import { PermissionsAndroid, Platform } from 'react-native'
import Geolocation from 'react-native-geolocation-service'
import Geocoder from 'react-native-geocoder'
import { FilterActions } from '@ReduxManager'
import { Location } from '@Models'

Geocoder.fallbackToGoogle('AIzaSyAggWpOMeJ0Hkz1IMNi--UbivONTjwuN34')

function hasLocationPermission() {
  return new Promise<boolean>((resolve, reject) => {
    if (Platform.OS === 'ios' || (Platform.OS === 'android' && Platform.Version < 23)) {
      resolve(true)
    }

    PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
      .then(hasPermission => {
        if (hasPermission) {
          resolve(true)
        } else {
          return
        }
      })
      .then(() => {
        return PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
      })
      .then(status => {
        if (status === PermissionsAndroid.RESULTS.GRANTED) resolve(true)

        if (status === PermissionsAndroid.RESULTS.DENIED) {
          resolve(false)
        } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
          resolve(false)
        }
      })
      .catch(error => reject(error))
  })
}

function startLocationUpdate() {
  return new Promise<Location>((resolve, reject) => {
    hasLocationPermission()
      .then(hasPermission => {
        if (hasPermission) {
          Geolocation.getCurrentPosition(
            (position: any) => {
              resolve(position.coords)
            },
            (error: any) => {
              reject(error)
            },
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
          )
        } else {
          reject({ message: 'Permission denied' })
        }
      })
      .catch(error => {
        reject(error)
      })
  })
}

function setCurrentUserLocation() {
  return new Promise((resolve, reject) => {
    startLocationUpdate()
      .then((location: any) => {
        if (location && location.latitude !== null && location.longitude !== null) {
          Geocoder.geocodePosition({
            lat: location.latitude,
            lng: location.longitude
          })
            .then((response: any) => {
              if (response && response.length > 0) {
                const locationDetails = response[0]
                const locationData = {
                  location,
                  address: locationDetails.locality + ', ' + locationDetails.adminArea
                }
                FilterActions.updateFilters('markets', locationData)
              }
            })
            .catch((error: any) => {
              reject(error)
            })
        } else {
          throw Error('Location not valid')
        }
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getCurrentUserLocation() {
  return new Promise<{ location: Location; address: string }>((resolve, reject) => {
    startLocationUpdate()
      .then((location: any) => {
        if (location && location.latitude !== null && location.longitude !== null) {
          Geocoder.geocodePosition({
            lat: location.latitude,
            lng: location.longitude
          })
            .then((response: any) => {
              if (response && response.length > 0) {
                const locationDetails = response[0]
                const locationData = {
                  location,
                  address: locationDetails.locality + ', ' + locationDetails.adminArea
                }
                resolve(locationData)
              }
            })
            .catch((error: any) => {
              reject(error)
            })
        } else {
          throw Error('Location not valid')
        }
      })
      .catch(error => {
        reject(error)
      })
  })
}

export default {
  startLocationUpdate,
  hasLocationPermission,
  setCurrentUserLocation,
  getCurrentUserLocation
}
