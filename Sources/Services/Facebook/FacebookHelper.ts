import { LoginManager, GraphRequest, GraphRequestManager, AccessToken } from 'react-native-fbsdk'
import { FacebookUserInfo } from '@Models'

function _handleFBLoginSuccess() {
  return new Promise<FacebookUserInfo>((resolve, reject) => {
    const infoRequest = new GraphRequest(
      '/me',
      {
        httpMethod: 'GET',
        version: 'v3.2',
        parameters: {
          fields: {
            string: 'email,first_name,last_name,picture.width(500).height(500)'
          }
        }
      },
      (error, result: any) => {
        let facebookUserInfo: FacebookUserInfo = { ...result }
        if (error) {
          // @ts-ignore
          reject(result.error)
          // @ts-ignore
        } else if (result.isCancelled) {
          // @ts-ignore
          reject(result.error)
        } else {
          AccessToken.getCurrentAccessToken()
            .then(data => {
              facebookUserInfo.token = data.accessToken.toString()
              resolve(facebookUserInfo)
            })
            .catch(error => {
              reject(error.message)
            })
        }
      }
    )

    new GraphRequestManager().addRequest(infoRequest).start()
  })
}

function loginAndGetUserData() {
  return new Promise<FacebookUserInfo>((resolve, reject) => {
    LoginManager.logOut()
    AccessToken.getCurrentAccessToken()
      .then(data => {
        LoginManager.logInWithPermissions(['public_profile'])
          .then(result => {
            if (result.isCancelled) {
              reject('Login cancelled')
            } else {
              return _handleFBLoginSuccess()
            }
          })
          .then(userinfo => {
            resolve(userinfo)
          })
          .catch(error => {
            reject(error.message)
          })
      })
      .catch(error => {
        reject(error.message)
      })
  })
}

function logOut() {
  LoginManager.logOut()
}
export default {
  loginAndGetUserData,
  logOut
}
