import Network from '../Network/Network'
import { RatingModelItem } from '@Models'
import qs from 'qs'

export type NetworkPromiseResponse<T> = Promise<T>

function login<T>(phone: string): NetworkPromiseResponse<T> {
  const body = { phone }
  return new Promise<T>((resolve, reject) => {
    Network.unAuthorizedRequest<T>('/verify-user', 'POST', body)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function extendToken<T>(token: string): NetworkPromiseResponse<T> {
  const body = { token }
  return new Promise<T>((resolve, reject) => {
    Network.unAuthorizedRequest<T>('/app/api/login', 'POST', body)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getItemAdditionalFee<T>(ids: Array<string | number>): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>(`/api/v2/item-additional-fees?ids=${ids}`, 'GET')
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getCoupons<T>(): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/coupons', 'GET')
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}
/**added by mahesh@fathom to get delivery promo codes 8 Nov 2019 */
function getDeliveryPromoCodes<T>(): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v2/promo-codes', 'GET')
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function getCouponDetailByName<T>(name: string): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>(`coupon-details-by-name?name=${name}`, 'GET')
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getUserCredits<T>(): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/user-credits', 'GET')
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getUserReviewSummary<T>(params: any) {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/user-review-summary', 'GET', null, params)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getItemReviewSummary<T>(params: any) {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v2/item-review-summary', 'GET', null, params)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getItemRenterRatings<T>(params: any) {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/item-renter-ratings', 'GET', null, params)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getItemListerRatings<T>(params: any) {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/item-lister-ratings', 'GET', null, params)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getItemDetails<T>(ids: Array<number>): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    const param = qs.stringify({ ids }, { arrayFormat: 'brackets' })
    console.log('**** param', param)
    Network.authorizedRequest<T>(`/api/v2/item-details?${param}`, 'GET')
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getRentalDetail<T>(id: string | number) {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>(`/api/v2/rental-details?id=${id}`, 'GET')
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getOtherUserDetail<T>(id: string | number) {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>(`/other-user-detail?id=${id}`, 'GET')
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

type RentalAmountForm = {
  item_ids: Array<string | number>
  credits: number
  coupon_code?: string
  dates_count?: number
}

function getRentalAmount<T>(body: RentalAmountForm) {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v2/rental-amount', 'POST', body)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}
type PromoCode = {
  code: string
}
function getDeliveryPromoCode<T>(body: PromoCode) {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v2/promo_codes', 'POST', body)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function createUserProfile<T>(body: any) {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/users', 'POST', body)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getUserProfile<T>(body: any) {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/users', 'POST', body)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function editUserProfile<T>(data: object) {
  const body = data
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/users', 'PATCH', body)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getNotificationSettings<T>() {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/notification-settings')
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function editNotificationSettings<T>(params: any) {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/notification-settings', 'PATCH', params)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function requestRental<T>(body: any): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    // Network.authorizedRequest<T>('/api/v2/rentals', 'POST', body)
    Network.authorizedRequest<T>('/api/v2/rentals-v3', 'POST', body)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function editRequestRental<T>(body): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    // Network.authorizedRequest<T>('/api/v2/rentals', 'PATCH', body)
    Network.authorizedRequest<T>('/api/v2/rentals-v3', 'PATCH', body)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function reportItem<T>(body: any): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/reports', 'POST', body)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function listerRating<T>(body: RatingModelItem): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v2/lister-ratings', 'POST', body)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function renterRating<T>(body: RatingModelItem): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/renter-ratings', 'POST', body)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getFavorites<T>(): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/favorites')
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function setFavorites<T>(item_id: any): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    const body = { item_id }
    Network.authorizedRequest<T>('/favorites', 'POST', body)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function deleteFavorites<T>(item_id: any): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>(`/favorites?item_id=${item_id}`, 'DELETE')
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function addItems<T>(item: any): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v2/items', 'POST', item)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function buyNowRequest<T>(item: any): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v2/purchase', 'POST', item)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function editBuyNowRequest<T>(item: any): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v2/purchase', 'PATCH', item)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function updateItems<T>(item: any): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v2/items', 'PATCH', item)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function deleteItems<T>(id: any): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>(`/api/v2/items?id=${id}`, 'DELETE')
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getItemList<T>(param?: any): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v2/items-list', 'POST', param)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getItemList_V3<T>(param?: any, nearBy?: boolean): NetworkPromiseResponse<T> {
  param.nearBy = nearBy
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v3/items-list', 'POST', param)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getUserDetail<T>(): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v1/users/details', 'GET')
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getItemSummary<T>(filterParam: any): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    // Network.authorizedRequest<T>('/api/v2/items-summary', 'POST', filterParam)
    Network.authorizedRequest<T>('/api/v3/items-summary', 'POST', filterParam)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}
//Method : GET
//http://localhost:3000/api/v2/purchases?rental_id=16
//------- Buy now approval get details api ---------------//

function getPurchaseDetails<T>(params: any): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>(`/api/v2/purchases?rental_id=${params}`, 'GET')
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getItemByUser<T>(params: any): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v2/items-by-user', 'GET', null, params)
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function canRemoveCard<T>(): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v1/users/can_remove_card', 'GET')
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function canDeleteBank<T>(): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v1/users/lender_can_remove_bank', 'GET')
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function returnItem<T>(rentalId, item_id): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>(`/api/v2/rentals/${rentalId}/return_item`, 'PATCH', { item_id })
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function applyAccessCode<T>(access_code: string): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v1/users/apply_access_code', 'PUT', { access_code })
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function createAccessCode<T>(code: string, end_date: string, max_used_count: number): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v2/access_codes', 'POST', { code, end_date, max_used_count })
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function addToWaitlist<T>(): NetworkPromiseResponse<T> {
  return new Promise<T>((resolve, reject) => {
    Network.authorizedRequest<T>('/api/v1/users/add_to_waitlist', 'PUT', null, null, {
      'Content-Type': 'application/json'
    })
      .then(response => {
        resolve(response as any)
      })
      .catch(error => {
        reject(error)
      })
  })
}

export default {
  login,
  extendToken,
  canRemoveCard,
  getItemDetails,
  getCoupons,
  getRentalAmount,
  getItemAdditionalFee,
  getUserReviewSummary,
  getItemReviewSummary,
  getItemRenterRatings,
  getItemListerRatings,
  getOtherUserDetail,
  getRentalDetail,
  addItems,
  buyNowRequest,
  updateItems,
  deleteItems,
  getItemList, getItemList_V3,
  getUserDetail,
  getItemSummary,
  getItemByUser,
  getFavorites,
  deleteFavorites,
  setFavorites,
  requestRental,
  reportItem,
  getUserCredits,
  listerRating,
  renterRating,
  getUserProfile,
  editUserProfile,
  createUserProfile,
  getNotificationSettings,
  editNotificationSettings,
  editRequestRental,
  getCouponDetailByName,
  canDeleteBank,
  returnItem,
  applyAccessCode,
  createAccessCode,
  addToWaitlist,
  // getDeliveryPromoCode,
  getPurchaseDetails,
  editBuyNowRequest,
  getDeliveryPromoCode, //promo code by code
  getDeliveryPromoCodes //get all codes
}
