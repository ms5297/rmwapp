import { GoogleAnalyticsSettings, GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';
import { AppConfig } from '@Utils';

const EVENT_CATEGORY = {
    MAIN: 'main'
}

const EVENT = {
    APP_LAUNCH: 'app_launch',
    LOGIN: 'login',
    REGISTER: 'register',
    PROFILE_COMPLETED: 'profile_completed',
    GEO_LOCATION_SHARED: 'geo_location_shared',
    RENTAL_ITEM_CREATED: 'rental_item_created',
    RENTAL_REQUESTED: 'rental_requested',
    RENTAL_ACCEPTED: 'rental_accepted',
    LENDER_REVIEW_CREATED: 'lender_review_created',
    RENTER_REVIEW_CREATED: 'renter_review_created',
    RENTAL_TRANSACTION_COMPLETED: 'rental_transaction_completed',
    PROMO_CODE_USED: 'promo_code_used',
    BUY_NOW_TRANSACTION_CREATED: 'buy_now_transaction_created',
    BUY_NOW_TRANSACTION_COMPLETED: 'buy_now_transaction_completed'
}

const SCREEN = {
    HOME: 'home',
    SEARCH: 'search',
    PROFILE: 'profile',
    FILTER: 'filter',
    ITEM_DESCRIPTION: 'item_description'
}

/**
setDryRun when enabled the native library prevents any data from being sent to Google Analytics.This allows you to test or debug the implementation, without your test data appearing in your Google Analytics reports.
*/
GoogleAnalyticsSettings.setDryRun(false);
/**
setDispatchInterval allows you to configure how often (in seconds) the batches are sent to your tracker.
*/
GoogleAnalyticsSettings.setDispatchInterval(30);
/**
Initialise the tracker based on the environment based tracker ID
*/
const tracker =
    //new GoogleAnalyticsTracker(Config.GA_TRACKER_ID, { CD_A: 1, CD_B: 2 });
    // staging tracking id belongs to powarvinayak@gmail.com
    AppConfig.isStaging ? new GoogleAnalyticsTracker('UA-153426799-1') : new GoogleAnalyticsTracker('UA-96456124-1');

function trackEvent(category, action, optionalValues = {}) {
    tracker.trackEvent(category, action, optionalValues)
}

function trackScreenView(screenName) {
    tracker.trackScreenView(screenName)
}

export default {
    EVENT_CATEGORY, EVENT, SCREEN,
    trackScreenView,
    trackEvent
}