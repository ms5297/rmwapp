import React from 'react'
import firebase from 'react-native-firebase'
import FirebaseHelper from './FirebaseHelper'
import { getStore, AppConfigurationActions, PermissionsActions } from '@ReduxManager'
import { Notification, NotificationOpen } from 'react-native-firebase/notifications'
import { UserProfile, AppConfiguration, NotificationData, ChatParam } from '@Models'
import { Navigator } from '@Navigation'
import { Platform, Alert } from 'react-native'

type Props = {}
type State = {
  userProfile?: UserProfile
  appConfiguration?: AppConfiguration
}

class FireBaseManager extends React.Component<Props, State> {
  private notificationListener: any
  private notificationOpenedListener: any
  private messageListener: any
  private tokenRefreshListener: any

  constructor(props: Props) {
    super(props)
    const store = getStore()
    const { appConfiguration, userProfile } = store.getState()
    this.state = { userProfile, appConfiguration }
  }

  private async getFirebaseToken() {
    try {
      let fcmToken = await firebase.messaging().getToken()
      if (fcmToken) {
        FirebaseHelper.updatePushTokenForUser(fcmToken)
      }
    } catch (error) { }
  }

  private checkPermission() {
    firebase
      .messaging()
      .hasPermission()
      .then(enabled => {
        if (enabled) {
          this.getFirebaseToken()
        } else {
          firebase
            .messaging()
            .requestPermission()
            .then(() => {
              PermissionsActions.saveNotificationPermission('authorized')
              this.getFirebaseToken()
            })
            .catch(error => {
              const { firstStart } = getStore().getState().appConfiguration
              if (firstStart) {
                Navigator.navTo('NotificationWarning')
              }
              console.warn('error while request permission', error)
            })
        }
      })
  }

  async createNotificationListeners() {
    firebase
      .notifications()
      .getInitialNotification()
      .then((notificationOpen: NotificationOpen) => {
        if (notificationOpen) {
          // App was opened by a notification
          // Get the action triggered by the notification being opened
          const action = notificationOpen.action
          // Get information about the notification that was opened
          const notification: Notification = notificationOpen.notification
        }
      })

    /*
     * Triggered when a particular notification has been received in foreground
     * */
    this.notificationListener = firebase.notifications().onNotification(notification => {
      console.log('createLocalNotification', notification)
      FirebaseHelper.createLocalNotificationFromNotification(notification)
      if (Platform.OS === 'android') {
        this.updateData(notification.data)
      }
    })

    /*
     * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
     * */
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened(notificationOpen => {
      console.log('onNotificationOpened', notificationOpen)
      const { title, body, data } = notificationOpen.notification
      this.handleUserTap(data)
    })

    /*
     * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
     * */
    const notificationOpen = await firebase.notifications().getInitialNotification()
    if (notificationOpen) {
      console.log('getInitialNotification', notificationOpen)
      const { title, body, data } = notificationOpen.notification
      this.handleUserTap(data)
    }

    /*
     * Triggered for data only payload in foreground
     * */
    this.messageListener = firebase.messaging().onMessage(response => {
      const { _data: data } = response
      const { message = '', title = '' } = data

      if (message === '') return
      console.log('createLocalNotificationFromMessage', title, message, data)
      FirebaseHelper.createLocalNotificationFromMessage(title, message, data)
      if (Platform.OS === 'ios') {
        this.updateData(data)
      }
    })

    this.tokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken => {
      FirebaseHelper.updatePushTokenForUser(fcmToken)
    })
  }

  private updateData = (data: NotificationData) => { }

  private handleUserTap = (data: NotificationData) => {
    const { type = '', rental_id, approval, statusChange, item, fromUser, toUser, chatID, isOwner, message, chat_id } = data
    switch (type) {
      case 'like':
        const item = { id: data.item }
        Navigator.push('Details', { item })
        break
      case 'chat':
        if (rental_id) {
          const param: ChatParam = {
            fromPush: true,
            statusChange: statusChange === 'true',
            rental_id: typeof rental_id === 'string' ? parseInt(rental_id) : rental_id,
            approval: approval === 'true',
            item: { id: typeof item === 'string' ? parseInt(item) : item },
            fromUser: typeof fromUser === 'string' ? parseInt(fromUser) : fromUser,
            toUser: typeof toUser === 'string' ? parseInt(toUser) : toUser,
            isOwner: isOwner === 'true',
            chatID,
            message
          }

          Navigator.navTo('Chat', { param })
        }
        break
      case 'admin_chat':
        if (rental_id) {
          const param: ChatParam = {
            rental_id: typeof rental_id === 'string' ? parseInt(rental_id) : rental_id,
            item: { id: '' },
            fromPush: true,
            fromUser: typeof fromUser === 'string' ? parseInt(fromUser) : fromUser,
            toUser: typeof toUser === 'string' ? parseInt(toUser) : toUser,
            isOwner: isOwner === 'true',
            chatID: chatID,
            message: message,
          }
          Navigator.navTo('AdminChat', { param })
        }

        break
      case 'should_add_bank_account':
        if (this.state.userProfile.should_add_bank_account) {
          Navigator.navTo('ForceUpdateBankAccount')
        }
        break
    }
  }

  componentWillMount() {
    this.checkPermission()
    this.createNotificationListeners()
  }

  componentWillUnmount() {
    this.notificationListener && this.notificationListener()
    this.notificationOpenedListener && this.notificationOpenedListener()
    this.messageListener && this.messageListener()
    this.tokenRefreshListener && this.tokenRefreshListener()
  }

  render(): any {
    return null
  }
}

export default FireBaseManager
