import firebase, { RNFirebase } from 'react-native-firebase'
import { Platform } from 'react-native'
import { UserProfile } from '@Models'
import { Utils, DateTime, Device } from '@Utils'
import { getStore } from '@ReduxManager'
import { DocumentReference } from 'react-native-firebase/firestore'
import branch, { BranchEvent } from 'react-native-branch'
import { AdminConfig } from '@Utils'

type QueryOperator = '=' | '==' | '>' | '>=' | '<' | '<=' | 'array-contains'

export type RNFirebaseUser = RNFirebase.User

export type RNFirebasePhoneAuthSnapshot = RNFirebase.PhoneAuthSnapshot

export type RNFirebaseConfirmationResult = RNFirebase.ConfirmationResult

export type RNDocumentReference = DocumentReference

export type LogEvents =
  | 'ProfileEdited'
  | 'PerformRentalAction'
  | 'ViewProfile'
  | 'Search_Items'
  | 'Search_Category_Items'
  | 'Favorite_Item'
  | 'ItemDeleted'
  | 'ItemEdited'
  | 'ItemAdded'
  | 'CardAdded'
  | 'CardRemoved'
  | 'ChangedDefaultCard'
  | 'PayoutMethodAdded'
  | 'PayoutMethodDeleted'

function get(collection: string, field: string, operation: QueryOperator, value: string | number) {
  return firebase
    .firestore()
    .collection(collection)
    .where(field, operation, value)
    .get()
}

async function getFirebaseToken() {
  try {
    let fcmToken = await firebase.messaging().getToken()
    if (fcmToken) {
      updatePushTokenForUser(fcmToken)
    }
  } catch (error) {
    console.log('getFirebaseToken error: ', error)
  }
}

function getAndUpdateToken() {
  firebase
    .messaging()
    .hasPermission()
    .then(enabled => {
      if (enabled) {
        getFirebaseToken()
      } else {
        firebase
          .messaging()
          .requestPermission()
          .then(() => {
            getFirebaseToken()
          })
          .catch(error => {
            console.warn('error while request permission', error)
          })
      }
    })
}

function updatePushTokenForUser(token: string | null, callback?: () => void) {
  //@ts-ignore
  const isAdminLogged = global.isAdminLogged
  console.log('***** onTokenRefresh isAdminLogged', isAdminLogged, token)
  if (isAdminLogged) {
    console.log('***** Admin logged no need to update device token')
    return
  }

  console.log('***** Update device token')

  const { userProfile, notificationSettings } = getStore().getState()

  if (!userProfile) return

  if (token == null) {
    firebase
      .firestore()
      .collection('pushTokens')
      .where('user', '==', userProfile.id)
      .get()
      .then(snapshot => {
        snapshot.forEach(tokenData => {
          tokenData.ref.delete()
        })
        if (typeof callback === 'function') {
          callback()
        }
      })
      .catch(error => {
        if (typeof callback === 'function') {
          callback()
        }
      })
  } else {
    firebase
      .firestore()
      .collection('pushTokens')
      .where('token', '==', token)
      .get()
      .then(snapshot => {
        snapshot.forEach(tokenData => {
          tokenData.ref.delete()
        })

        firebase
          .firestore()
          .collection('pushTokens')
          .where('user', '==', userProfile.id)
          .get()
          .then(snapshot => {
            snapshot.forEach(tokenData => {
              tokenData.ref.delete()
            })

            firebase
              .firestore()
              .collection('pushTokens')
              .add({
                user: userProfile.id,
                token: token,
                notificationDisabled: !notificationSettings.messages_push
              })
          })
      })
  }
}

async function createLocalNotificationFromMessage(title, body, data) {
  try {
    let local = new firebase.notifications.Notification()
      .setNotificationId('annoucementChannel')
      .setSound('default')
      .setTitle(title)
      .setBody(body)
      .setData(data)

    console.log(`createLocalNotificationFromMessage: title=${title} - message: ${body} - data=${data}`)
    if (Platform.OS === 'android') {
      const channel = new firebase.notifications.Android.Channel(
        'rmw-channel-id',
        'Rent My Wardobe Channel',
        firebase.notifications.Android.Importance.Max
      ).setDescription('Rent My Wardobe Channel')
      firebase.notifications().android.createChannel(channel)
      local.android
        .setChannelId('rmw-channel-id')
        // .android.setSmallIcon("app_icon")
        .android.setPriority(firebase.notifications.Android.Priority.Max)
        .setSound('default')
    }

    await firebase.notifications().displayNotification(local)
  } catch (error) {
    console.log('createLocalNotification error: ', error)
  }
}

// @ts-ignore
async function createLocalNotificationFromNotification(notification) {
  try {
    const { body, data, title, notificationId } = notification
    let local = new firebase.notifications.Notification()
      .setNotificationId(notificationId)
      .setSound('default')
      .setTitle(title)
      .setBody(body)
      .setData(data)

    if (Platform.OS === 'android') {
      const channel = new firebase.notifications.Android.Channel(
        'rmw-channel-id',
        'Rent My Wardobe Channel',
        firebase.notifications.Android.Importance.Max
      ).setDescription('Rent My Wardobe Channel')
      firebase.notifications().android.createChannel(channel)

      local.android
        .setChannelId('rmw-channel-id')
        // .android.setSmallIcon("app_icon")
        .android.setPriority(firebase.notifications.Android.Priority.Max)
        .setSound('default')
    }

    const result = await firebase.notifications().displayNotification(local)
  } catch (error) {
    console.log('createLocalNotification error: ', error)
  }
}

async function pushToken(token: string, userId: string | number, userName: string, role: string, photo: string = '') {
  try {
    const appVersion = Device.getAppVersion()
    const codePushVersion = Device.getCodePushVersion()

    console.log('**** Push token info: ', { userId, token, userName, role, photo, appVersion, codePushVersion })

    const tokens = await get('userTokens', 'token', '==', token)
    tokens.forEach(token => {
      token.ref.delete()
    })

    const users = await get('userTokens', 'userId', '==', userId)
    users.forEach(user => {
      user.ref.delete()
    })

    const lastTimeLogged = DateTime.getTimestamp()

    const result = await firebase
      .firestore()
      .collection('userTokens')
      .add({ userId, token, userName, photo, role, lastTimeLogged, appVersion, codePushVersion })

    console.log('Push token successfully: ', result)
  } catch (error) {
    console.log('Push token error: ', error)
  }
}

async function removeToken(userId: string) {
  try {
    console.log('Remove token of userId: ', userId)
    const tokens = await get('pushTokens', 'user', '==', userId)
    tokens.forEach(token => {
      token.ref.delete()
    })
  } catch (error) {
    console.log('Push token error: ', error)
  }
}

function setBadge(count: number) {
  firebase.notifications().setBadge(count)
}

function loginWithPhone(phone: string) {
  return firebase.auth().signInWithPhoneNumber(phone)
}

function verifyPhoneNumber(phone) {
  return firebase.auth().verifyPhoneNumber(phone)
}

function resendOTP(phoneNumber: string) {
  const phone = "+1" + phoneNumber
  return firebase.auth().signInWithPhoneNumber(phone, true)
}

function loginWithFacebook(accessToken: string) {
  const credential = firebase.auth.FacebookAuthProvider.credential(accessToken)
  return firebase.auth().signInWithCredential(credential)
}

const hasLoggedInUser = () => {
  return new Promise<RNFirebaseUser>((resolve, reject) => {
    const unsubscribeAuth = firebase.auth().onAuthStateChanged(user => {
      if (unsubscribeAuth) unsubscribeAuth()
      resolve(user)
    })
  })
}

function reAuthenticate(verificationID: string, code: string) {
  return new Promise<boolean>((resolve, reject) => {
    const user = getCurrentUser()
    const credential = firebase.auth.PhoneAuthProvider.credential(verificationID, code)
    user
      .reauthenticateWithCredential(credential)
      .then(response => {
        console.log('**** 3 re authenticate', response)
        const user = response.user
        if (user) {
          return resolve(true)
        } else {
          throw Error('User not valid')
        }
      })
      .catch(error => {
        console.log('**** error', error)
        reject(error)
      })
  })
}

function updatePhoneNumber(verificationID: string, code: string) {
  const user = getCurrentUser()
  const credential = firebase.auth.PhoneAuthProvider.credential(verificationID, code)
  return user.updatePhoneNumber(credential)
}

function uploadChatPhoto(path: string) {
  return new Promise<string>((resolve, reject) => {
    const uploadedURL = Utils.generateUUID() + '.jpeg'
    firebase
      .storage()
      .ref('chats')
      .child(uploadedURL)
      .putFile(path)
      .then(response => {
        resolve(response.downloadURL)
      })
      .catch(error => {
        console.log('***** error')
        reject(error)
      })
  })
}

function uploadItemPhoto(path: string) {
  return new Promise<string>((resolve, reject) => {
    const uploadedURL = Utils.generateUUID() + '.jpeg'
    firebase
      .storage()
      .ref('items')
      .child(uploadedURL)
      .putFile(path)
      .then(response => {
        resolve(response.downloadURL)
      })
      .catch(error => {
        console.log('***** error')
        reject(error)
      })
  })
}

function uploadUserProfilePhoto(path: string) {
  return new Promise<string>((resolve, reject) => {
    const uploadedURL = Utils.generateUUID() + '.jpeg'
    console.log('uploadUserProfilePhoto: path =  ', path)
    firebase
      .storage()
      .ref('userProfiles')
      .child(uploadedURL)
      .putFile(path)
      .then(response => {
        console.log('***** uploadUserProfilePhoto', response)
        resolve(response.downloadURL)
      })
      .catch(error => {
        console.log('***** error')
        reject(error)
      })
  })
}

function verifyOTP(request: any, otp: string) {
  request
    .confirm(otp)
    .then((user: RNFirebaseUser) => {
      if (user) {
      } else {
      }
    })
    .catch((error: any) => {
      console.warn('error', error)
    })
}

function getCurrentUser() {
  const currentUser = firebase.auth().currentUser
  return currentUser
}

function logOut() {
  return new Promise((resolve, reject) => {
    const currentUser = getCurrentUser()
    if (currentUser) {
      firebase
        .auth()
        .signOut()
        .then(() => resolve())
        .catch(error => reject(error))
    } else {
      resolve()
    }
  })
}

function pushHistories(data: any) {
  return firebase
    .firestore()
    .collection('pushHistory')
    .add(data)
}

function getItemDetail(userProfile: UserProfile, itemDetails: any) {
  return firebase
    .firestore()
    .collection('conversations')
    .where('fromUser.id', '==', userProfile.id)
    .where('toUser.id', '==', itemDetails.owner.id)
    .where('itemId', '==', itemDetails.id)
    .get()
}

function getAllUsers(limit = 50) {
  return firebase
    .firestore()
    .collection('userTokens')
    .orderBy('lastTimeLogged', 'desc')
    .limit(limit)
    .get()
}
/**added by mahesh@fathomable to get all admins */
function getAdminUsers(limit = 50) {
  return firebase
    .firestore()
    .collection('userTokens')
    .where('role', '==', 'admin')
    .orderBy('lastTimeLogged', 'desc')
    .limit(limit)
    .get()
}

function getMoreUsers(lastTime, limit = 50) {
  return firebase
    .firestore()
    .collection('userTokens')
    .orderBy('lastTimeLogged', 'desc')
    .limit(limit)
    .startAfter(lastTime)
    .get()
}

function getConversations() {
  return firebase.firestore().collection('conversations')
}
function getAdminConversations() {
  return firebase.firestore().collection('adminConversations')
}
function getAdminConversationsById(userProfile) {
  return firebase.firestore().collection('adminConversations')
    .where('fromUser.id', '==', userProfile.id)
}
function getAdminConversationsByAdminId() {
  return firebase.firestore().collection('adminConversations')
    .where('fromUser.id', '==', AdminConfig.adminChatConfig.id)
}
function getUserConversations(userProfile: UserProfile) {
  return firebase
    .firestore()
    .collection('conversations')
    .where('fromUser.id', '==', userProfile.id)
}

function setUser(userProfile: UserProfile) {
  firebase.analytics().setUserId(userProfile.id.toString())
  firebase.analytics().setUserProperty('firstname', userProfile.firstname)
  firebase.analytics().setUserProperty('lastname', userProfile.lastname)
  firebase.analytics().setUserProperty('email', userProfile.email)
  firebase.analytics().setUserProperty('phone', userProfile.phone)
}

function logEvent(eventName: LogEvents, params = {}) {
  firebase.analytics().logEvent(eventName, params)
  branch
    .createBranchUniversalObject(eventName, {
      locallyIndex: true,
      title: '',
      contentDescription: ''
    })
    .then(buo => {
      new BranchEvent(eventName, buo, params).logEvent()
    })
    .catch(error => {
      console.log('createBranchUniversalObject for event', eventName, error)
    })
}

function logCurrentScreen(screenName) {
  firebase.analytics().setCurrentScreen(screenName, screenName)
}

function enableCrashlyticsCollection() {
  firebase.crashlytics().enableCrashlyticsCollection()
}

function setupCrashlytics(userProfile: UserProfile) {
  firebase.crashlytics().setUserIdentifier(userProfile.id.toString())
  firebase.crashlytics().setUserEmail(userProfile.email)
  firebase.crashlytics().setUserName(Utils.getUserName(userProfile))
}

function findUserById(text: string) {
  return new Promise((resolve, reject) => {
    const searchId = parseInt(text)
    firebase
      .firestore()
      .collection('userTokens')
      .get()
      .then(async response => {
        let values = []

        await response.docs.forEach(value => {
          const data: any = value.data()
          if (!data) return false

          const lowerText = text.toLowerCase()
          const userName = `${data.userName}`.toLowerCase()
          const userId = `${data.userId}`

          if (isNaN(searchId)) {
            if (userName.includes(lowerText)) {
              values.push(data)
            }
          } else {
            if (userId == lowerText) {
              values.push(data)
            }
          }
        })
        resolve(values)
      })
      .catch(error => {
        console.log('**** error', error)
        reject(error)
      })
    // let promises = [
    //   firebase
    //     .firestore()
    //     .collection('userTokens')
    //     .where('userName', 'array-contains', value)
    //     .get()
    // ]
    // if (!isNaN(userId)) {
    //   console.log('**** userId', userId)
    //   promises.push(
    //     firebase
    //       .firestore()
    //       .collection('userTokens')
    //       .where('userId', '==', userId)
    //       .get()
    //   )
    // }
    // Promise.all(promises)
    //   .then(async values => {
    //     let data = null

    //     await values.forEach(value => {
    //       console.log('**** value', value)
    //       value.docs.forEach(doc => {
    //         const d = doc.data()
    //         data = d
    //         console.log('***** d', d)
    //       })
    //     })

    //     console.log('**** data', data)
    //   })
    //   .catch(error => {})
  })
}
export default {
  pushToken,
  createLocalNotificationFromNotification,
  removeToken,
  createLocalNotificationFromMessage,
  setBadge,
  loginWithFacebook,
  loginWithPhone,
  resendOTP,
  hasLoggedInUser,
  uploadChatPhoto,
  uploadItemPhoto,
  uploadUserProfilePhoto,
  verifyOTP,
  getCurrentUser,
  logOut,
  pushHistories,
  getConversations,
  getItemDetail,
  getUserConversations,
  setUser,
  updatePushTokenForUser,
  getAndUpdateToken,
  getAllUsers,
  setupCrashlytics,
  enableCrashlyticsCollection,
  updatePhoneNumber,
  reAuthenticate,
  verifyPhoneNumber,
  getMoreUsers,
  logEvent,
  logCurrentScreen,
  findUserById,
  getAdminConversations,
  getAdminConversationsById,
  getAdminUsers,
  getAdminConversationsByAdminId
}
