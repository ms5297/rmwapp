// @ts-ignore
import branch, { BranchEvent } from 'react-native-branch'
import { Navigator } from '@Navigation'
import { getStore } from '@ReduxManager'
import { BranchParams, UserProfile, CheckoutParam, ChatParam } from '@Models'
import Api from '../Api/Api'
import Permissions from '../Permissions/Permissions'

const dynamicLinkDomain = 'rentmywardrobe.com/links'

const INVITE_EVENT = 'INVITE'
const SHARE_ITEM_EVENT = 'SHARE_ITEM'
const SHARE_PROFILE_EVENT = 'SHARE_PROFILE'
const RENTAL_EVENT = 'RENTAL'

const fallbackUrl = 'https://itunes.apple.com/us/app/rent-my-wardrobe/id1375748670?ls=1&mt=8'

const subscribe = () => {
  return branch.subscribe(({ error, params }: { error: any; params: BranchParams }) => {
    try {
      if (error) {
        console.error('Error from Branch: ' + error)
        return
      }

      if (params['+non_branch_link']) {
        const nonBranchUrl = params['+non_branch_link']
        return
      }

      if (!params['+clicked_branch_link']) {
        return
      }
      const { type } = params
      console.log('**** handle branch deep link for type = ', type)
      switch (type) {
        case 'Share Item':
          const { item } = params
          if (item) {
            const itemObj = JSON.parse(item)
            Navigator.navTo('Details', { item: itemObj })
          }
          break
        case 'Share Profile':
          const { user } = params
          if (user) {
            const userObj = JSON.parse(user)
            Navigator.navTo('PublicProfile', { user: userObj })
          }
          break
        case 'rental_link':
          _processRentalLink(params)
          break
        case 'item_link':
          const { item_id: id } = params
          if (id) {
            Navigator.navTo('Details', { item: { id } })
          }
          break
        case 'notification_setting':
          Navigator.showLoading()
          Permissions.check('notification')
            .then(status => {
              Navigator.hideLoading()
              if (status !== 'authorized') {
                const param = { permissionType: 'notification' }
                Navigator.navTo('Permissions', { param })
              }
            })
            .catch(error => {
              console.log('error', error)
            })
          break
        default:
          console.log('No need handle for this type: ', type)
          break
      }
    } catch (error) {
      console.log('error: ', error)
    }
  })
}

const _processRentalLink = (params: BranchParams) => {
  const { rental_id, item_ids: _item_ids, item_id, statusChange, approved, user_ids } = params

  console.log('****** _processRentalLink', params)
  const { userProfile } = getStore().getState()
  if (!userProfile) {
    return
  }

  const currentUserId = userProfile.id

  if (!Array.isArray(user_ids) || currentUserId < 0 || !user_ids.includes(currentUserId)) {
    console.log("************* CAN'T ACCESS THIS DEEP LINK")
    return
  }

  const item_ids = Array.isArray(_item_ids) ? _item_ids : item_id ? [item_id] : []
  if (rental_id && item_ids.length) {
    if (approved) {
      Navigator.showLoading()

      Promise.all([Api.getRentalDetail(rental_id), Api.getItemDetails(item_ids)])
        .then((values: any) => {
          const { code: code1, message: message1, ...data } = values[0]
          const { code: code2, message: message2, items } = values[1]

          if (code1 !== 200) {
            throw Error(message1 || '(BH - 96) Internal Error')
          }

          if (code2 !== 200) {
            throw Error(message2 || '(BH - 100) Internal Error')
          }
          const selectedDates = data.dates
          const param: CheckoutParam = { items, selectedDates }

          Navigator.hideLoading()
          Navigator.navTo('Checkout', { param })
        })
        .catch(error => {
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
        })
    } else {
      const item = {
        id: rental_id
      }
      const param: ChatParam = {
        statusChange,
        rental_id,
        item
      }
      Navigator.navTo('Chat', { param })
    }
  }
}

function createSharedUserURL(user: UserProfile) {
  return new Promise<string>((resolve, reject) => {
    const linkProperties = {
      feature: 'share',
      campaign: 'referral'
    }
    const controlParams = {
      $ios_url: fallbackUrl,
      $fallback_url: fallbackUrl
    }
    let buoRef: any = null
    branch
      .createBranchUniversalObject(`shareProfile/${user.id}`, {
        locallyIndex: true,
        title: '',
        contentDescription: '',
        contentImageUrl: user.photo || '',
        contentMetadata: {
          customMetadata: {
            user: JSON.stringify(user),
            type: 'Share Profile'
          }
        }
      })
      .then((buo: any) => {
        const { id = '', firstname = '', lastname = '', email = '' } = user

        new BranchEvent(SHARE_PROFILE_EVENT, buo, {
          sharedBy: id,
          firstname,
          lastname,
          email
        }).logEvent()

        buoRef = buo
        return buo.generateShortUrl(linkProperties, controlParams)
      })
      .then((response: any) => {
        if (buoRef && typeof buoRef.release === 'function') {
          buoRef.release()
        }
        resolve(response.url)
      })
      .catch((error: any) => {
        console.warn('**** createSharedUserURL error: ', error)
        reject(error)
      })
  })
}

function createSharedAppURL(userProfile: UserProfile) {
  return new Promise<string>((resolve, reject) => {
    const linkProperties = {
      feature: 'share',
      campaign: 'referral'
    }
    const controlParams = {
      $ios_url: fallbackUrl,
      $fallback_url: fallbackUrl
    }

    const { id } = userProfile
    let buoRef: any = null
    branch
      .createBranchUniversalObject(
        `share/${id}`,
        {
          locallyIndex: true,
          title: '',
          contentDescription: ''
        },
        {
          contentMetadata: {
            customMetadata: {
              invitedBy: id,
              type: 'Share App'
            }
          }
        }
      )
      .then((buo: any) => {
        buoRef = buo
        const { id = '', firstname = '', lastname = '', email = '' } = userProfile
        buo.logEvent(BranchEvent.CompleteRegistration, {
          invitedBy: id,
          firstname,
          lastname,
          email
        })
        new BranchEvent(BranchEvent.CompleteRegistration, buo, {
          invitedBy: id,
          firstname,
          lastname,
          email
        }).logEvent()

        return buo.generateShortUrl(linkProperties, controlParams)
      })
      .then((response: any) => {
        if (buoRef && typeof buoRef.release === 'function') {
          buoRef.release()
        }
        resolve(response.url)
      })
      .catch((error: any) => {
        console.warn('**** createSharedAppURL error: ', error)
        reject(error)
      })
  })
}

function createSharedItemURL(fromUser: UserProfile, item: any) {
  return new Promise<string>((resolve, reject) => {
    const { name, description, photos, tags: tagObjects = [], id } = item
    let tags = []
    if (Array.isArray(tagObjects) && tagObjects.length > 0) {
      tags = tagObjects.map(obj => obj.tag)
    }
    const linkProperties = {
      feature: 'share',
      campaign: 'referral',
      tags
    }
    const controlParams = {
      $ios_url: fallbackUrl,
      $fallback_url: fallbackUrl
    }
    let buoRef: any = null
    branch
      .createBranchUniversalObject(`shareItem/${id}`, {
        locallyIndex: true,
        title: name,
        contentDescription: description,
        contentImageUrl: photos[0].url,
        contentMetadata: {
          customMetadata: {
            referrer: JSON.stringify(fromUser),
            item: JSON.stringify(item),
            type: 'Share Item'
          }
        }
      })
      .then((buo: any) => {
        const { id = '', firstname = '', lastname = '', email = '' } = fromUser

        new BranchEvent(SHARE_ITEM_EVENT, buo, {
          sharedBy: id,
          firstname,
          lastname,
          email,
          itemId: item.id,
          itemName: item.name
        }).logEvent()

        buoRef = buo
        return buo.generateShortUrl(linkProperties, controlParams)
      })
      .then((response: any) => {
        if (buoRef && typeof buoRef.release === 'function') {
          buoRef.release()
        }
        resolve(response.url)
      })
      .catch((error: any) => {
        console.log('createBranchUniversalObject error: ', error)
        reject(null)
      })
  })
}

function trackingRegister(user: UserProfile) {
  const { id = '', name = '' } = user
  branch.userCompletedAction(BranchEvent.CompleteRegistration, { id, name })
  branch.setIdentity(id)
}

function userLogout() {
  branch.logout()
}

function setUserId(id) {
  branch.setIdentity(id)
}

export default {
  subscribe,
  trackingRegister,
  userLogout,
  setUserId,
  createSharedUserURL,
  createSharedAppURL,
  createSharedItemURL
}
