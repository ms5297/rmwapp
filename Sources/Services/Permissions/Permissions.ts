import { Platform, PermissionsAndroid } from 'react-native'
import Permission from 'react-native-permissions'
import { Device } from '@Utils'
import { PermissionStatus } from '@Models'

type PermissionTypes = 'notification' | 'location' | 'camera' | 'photo'
const requestCameraPermission = async () => {
  if (Platform.OS === 'android') {
    const result = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA)
    return result === PermissionsAndroid.RESULTS.GRANTED || true
  }
  return true
}

function check(type: PermissionTypes) {
  return Permission.check(type)
}

function checkMultiple(types: Array<PermissionTypes>) {
  return Permission.checkMultiple(types)
}

function request(type: PermissionTypes) {
  return Permission.request(type)
}

function checkLocationAndNotification(types: Array<PermissionTypes>) {
  return new Promise<{ [key: string]: string }>((resolve, reject) => {
    if (Device.isAndroid()) {
      Permission.check('location')
        .then(location => {
          resolve({ location: location, notification: 'authorized' })
        })
        .catch(error => {
          console.warn('error: ', error)
        })
    } else {
      Permission.checkMultiple(['notification', 'location'])
        .then(status => {
          resolve(status)
        })
        .catch(error => {
          console.warn('error: ', error)
          reject(error)
        })
    }
  })
}

function openSettings(callback?: (status: any) => void) {
  Permission.canOpenSettings()
    .then(isCanOpen => {
      if (isCanOpen) {
        Permission.openSettings()
          .then(status => callback(status))
          .catch(error => {
            console.warn('openSettings error: ', error)
          })
      } else {
        console.warn("openSettings cann't open settings")
      }
    })
    .catch(error => {
      console.warn('openSettings error: ', error)
    })
}

export default {
  requestCameraPermission,
  checkLocationAndNotification,
  check,
  checkMultiple,
  openSettings,
  request
}
