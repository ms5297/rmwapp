import { Freshchat, FreshchatConfig, FreshchatUser, FreshchatMessage } from 'react-native-freshchat-sdk'
import { UserProfile } from '@Models'

function init() {
  const APP_ID = 'cd219e18-6ab5-4894-8c13-452870b23c59'
  const APP_KEY = 'ab63c62c-ccf1-4491-813e-da457ebb716e'
  const freshchatConfig = new FreshchatConfig(APP_ID, APP_KEY)
  Freshchat.init(freshchatConfig)
}

function setUser(user: UserProfile) {
  const { firstname, lastname, email, phone } = user
  var freshchatUser = new FreshchatUser()
  freshchatUser.firstName = firstname
  freshchatUser.lastName = lastname
  freshchatUser.email = email
  freshchatUser.phoneCountryCode = '+1'
  freshchatUser.phone = phone
  Freshchat.setUser(freshchatUser, (error: any) => {
    console.log('Freshchat.setUser error', error)
  })
}

function resetUserFreshchat() {
  Freshchat.resetUser()
}

function showConversations() {
  Freshchat.showConversations()
}

function showFAQs() {
  Freshchat.showFAQs()
}

function sendSupportMessage(message: string, tag: string) {
  let freshchatMessage = new FreshchatMessage()
  freshchatMessage.tag = tag
  freshchatMessage.message = message
  Freshchat.sendMessage(freshchatMessage)
}

function dismissFreshchatViews() {
  Freshchat.dismissFreshchatViews()
}
export default {
  init,
  setUser,
  resetUserFreshchat,
  showConversations,
  showFAQs,
  dismissFreshchatViews,
  sendSupportMessage
}
