import axios, { AxiosPromise } from 'axios'
import { AppConfig, Device } from '@Utils'

const BASE_URL = AppConfig.isStaging ? AppConfig.server_url_staging : AppConfig.server_url_prod

type RequestMethod = 'POST' | 'GET' | 'PUT' | 'PATCH' | 'DELETE'

class Network {
  private static instance = new Network()
  private token: string = ''
  private appVersion = '3.0.0.15'

  constructor() {
    if (Network.instance) {
      throw new Error('Error: Instantiation failed: Use Network.getInstance() instead of new.')
    }

    this.appVersion = Device.getAppVersion()
    Network.instance = this
  }
  public static getInstance(): Network {
    return Network.instance
  }

  setAppVersion(appVersion: string) {
    this.appVersion = appVersion
  }

  getBaseUrl(): string {
    return BASE_URL
  }

  setToken(token: string) {
    this.token = token
  }

  getToken(): string {
    return this.token
  }

  unAuthorizedRequest<T>(
    url: string,
    method: RequestMethod = 'GET',
    data?: object,
    params?: object,
    header?: object
  ): AxiosPromise<T> {
    const response: AxiosPromise<T> = axios({
      method: method,
      url: url,
      baseURL: BASE_URL,
      data: data,
      timeout: 60000,
      params: params,
      headers: {
        ...header,
        'Content-Type': 'application/json',
        accept: 'application/json',
        crossDomain: true,
        appVersion: this.appVersion
      }
    })
    return response
  }

  authorizedRequest<T>(
    url: string,
    method: RequestMethod = 'GET',
    data?: object,
    params?: object,
    header?: object
  ): AxiosPromise<T> {
    const response: AxiosPromise<T> = axios({
      method: method,
      url: url,
      baseURL: BASE_URL,
      data: data,
      timeout: 60000,
      params: params,
      headers: {
        ...header,
        'Content-Type': 'application/json',
        accept: 'application/json',
        crossDomain: true,
        token: this.token,
        appVersion: this.appVersion
      }
    })
    return response
  }
}

export const NetWorkError = {
  200: 'Success',
  404: 'Page not found',
  422: 'Invalid request',
  500: 'Internal errror'
}

export function getError(errorCode: number, fallback: string = 'Unknown Error') {
  let _fallback = 'Unknown Error'
  if (fallback && fallback !== '') {
    _fallback = fallback
  }

  const errorMessage = (NetWorkError as any)[errorCode] || _fallback

  return {
    errorCode,
    errorMessage
  }
}

axios.interceptors.request.use(
  config => {
    if (__DEV__) {
      const { url, method, data, params, baseURL, headers } = config
      const message = `👉👉👉
Request Info: ${baseURL || ''}${url}
  - Method : ${method}
  - Body   : ${JSON.stringify(data)}
  - Params : ${JSON.stringify(params)}
  - Headers: ${JSON.stringify(headers)}
  `
      console.log(message)
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

axios.interceptors.response.use(
  response => {
    if (__DEV__) {
      const { data: responseData, config } = response
      const { url, method, data, params } = config
      const message = `👉👉👉
Response info: ${url}
  - Method : ${method}
  - Body   : ${JSON.stringify(responseData)}
  - Params : ${JSON.stringify(params)}
  - Response Data: ${data}
  `
      console.log(message)
    }
    return response.data
  },
  error => {
    if (__DEV__) {
      const message = `👉👉👉
Response error: ${error}
  `
      console.log(message)
    }

    return Promise.reject(error)
  }
)

export default Network.getInstance()
