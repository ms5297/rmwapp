// @ts-ignore
import appsFlyer from 'react-native-appsflyer'
// import { Navigator } from '@Navigation'
// import { getStore } from '@ReduxManager'
// import { BranchParams, UserProfile, CheckoutParam, ChatParam } from '@Models'
import { UserProfile } from '@Models'
import Api from '../Api/Api'
import Permissions from '../Permissions/Permissions'
import { Platform } from 'react-native'
import { AppConfig } from '@Utils'

// const dynamicLinkDomain = 'rentmywardrobe.com/links'

// const INVITE_EVENT = 'INVITE'
// const SHARE_ITEM_EVENT = 'SHARE_ITEM'
// const SHARE_PROFILE_EVENT = 'SHARE_PROFILE'
// const RENTAL_EVENT = 'RENTAL'

// const fallbackUrl = 'https://itunes.apple.com/us/app/rent-my-wardrobe/id1375748670?ls=1&mt=8'

const EVENTS = {
    PROFILE_COMPLETED: 'profile_completed',
    GEO_LOCATION_SHARED: 'geo_location_shared',
    RENTAL_ITEM_CREATED: 'rental_item_created',
    RENTAL_REQUESTED: 'rental_requested',
    RENTAL_ACCEPTED: 'rental_accepted',
    LENDER_REVIEW_CREATED: 'lender_review_created',
    RENTER_REVIEW_CREATED: 'renter_review_created',
    RENTAL_TRANSACTION_COMPLETED: 'rental_transaction_completed',
    PROMO_CODE_USED: 'promo_code_used',
    BUY_NOW_TRANSACTION_CREATED: 'buy_now_transaction_created',
    BUY_NOW_TRANSACTION_COMPLETED: 'buy_now_transaction_completed'


    //     5. BUY NOW TRANSACTION CREATED. 
    //     6. LENDER REVIEW CREATED
    //     7. RENTER REVIEW CREATED. 
    //     8. BUY REVIEW CREATED. 
    //     9. RENTAL TRANSACTION COMPLETED. 
    //     10. BUY NOW TRANSACTION COMPLETED. 
    //     11. PROMO CODE USED


    // 5. buy now transaction created. 
    // 6. lender review created
    // 7. renter review created. 
    // 8. buy review created. 
    // 9. rental transaction completed. 
    // 10. buy now transaction completed. 
    // 11. promo code used
}

// staging dev_key belongs to vinayak@fathomable.com
// staging appId belongs to dev app
const options = {
    devKey: AppConfig.isStaging ? 'P8dsXexchvqfLbtm854xWH' : 'YgznjJYxmRmcNKr9cXJJK3',
    isDebug: AppConfig.isStaging,
    appId: ''
};

if (Platform.OS === 'ios') {
    options.appId = AppConfig.isStaging ? '1486143158' : '1375748670';
}



function init() {
    appsFlyer.initSdk(
        options,
        (result) => {
            //console.log(result);
        },
        (error) => {
            // console.error(error);
        }
    );
}

function trackingRegister(user: UserProfile) {
    const { id = '', name = '' } = user
    //branch.userCompletedAction(BranchEvent.CompleteRegistration, { id, name })
    //branch.setIdentity(id)

    const eventName = 'af_complete_registration';
    const eventValues = {
        af_user_id: id,
        af_name: name,
    };

    appsFlyer.trackEvent(
        eventName,
        eventValues,
        (result) => {
            //console.log(result);
        },
        (error) => {
            // console.error(error);
        }
    );

    appsFlyer.setCustomerUserId(id, (response) => {
        //..
    });
}

function trackLogin(user: UserProfile) {
    const { id = '', name = '' } = user
    const eventName = 'af_login';
    const eventValues = {
        af_user_id: id,
        af_name: name,
    };

    appsFlyer.trackEvent(
        eventName,
        eventValues,
        (result) => {
            //console.log(result);
        },
        (error) => {
            // console.error(error);
        }
    );

    appsFlyer.setCustomerUserId(id, (response) => {
        //..
    });
}

function setUserId(id) {
    appsFlyer.setCustomerUserId(id, (response) => {
        //..
    });
}

function trackAppLaunch() {
    appsFlyer.trackAppLaunch();
}

function trackEvent(eventName, userId, objectId) {
    const eventValues = {
        af_user_id: userId,
        object_id: objectId,
    };

    appsFlyer.trackEvent(
        eventName,
        eventValues,
        (result) => {
            //console.log(result);
        },
        (error) => {
            //console.error(error);
        }
    );
}

/*
const _processRentalLink = (params: BranchParams) => {
    const { rental_id, item_ids: _item_ids, item_id, statusChange, approved, user_ids } = params

    console.log('****** _processRentalLink', params)
    const { userProfile } = getStore().getState()
    if (!userProfile) {
        return
    }

    const currentUserId = userProfile.id

    if (!Array.isArray(user_ids) || currentUserId < 0 || !user_ids.includes(currentUserId)) {
        console.log("************* CAN'T ACCESS THIS DEEP LINK")
        return
    }

    const item_ids = Array.isArray(_item_ids) ? _item_ids : item_id ? [item_id] : []
    if (rental_id && item_ids.length) {
        if (approved) {
            Navigator.showLoading()

            Promise.all([Api.getRentalDetail(rental_id), Api.getItemDetails(item_ids)])
                .then((values: any) => {
                    const { code: code1, message: message1, ...data } = values[0]
                    const { code: code2, message: message2, items } = values[1]

                    if (code1 !== 200) {
                        throw Error(message1 || '(BH - 96) Internal Error')
                    }

                    if (code2 !== 200) {
                        throw Error(message2 || '(BH - 100) Internal Error')
                    }
                    const selectedDates = data.dates
                    const param: CheckoutParam = { items, selectedDates }

                    Navigator.hideLoading()
                    Navigator.navTo('Checkout', { param })
                })
                .catch(error => {
                    Navigator.hideLoading()
                    Navigator.showToast('Error', error.message, 'Error')
                })
        } else {
            const item = {
                id: rental_id
            }
            const param: ChatParam = {
                statusChange,
                rental_id,
                item
            }
            Navigator.navTo('Chat', { param })
        }
    }
}

function createSharedUserURL(user: UserProfile) {
    return new Promise<string>((resolve, reject) => {
        const linkProperties = {
            feature: 'share',
            campaign: 'referral'
        }
        const controlParams = {
            $ios_url: fallbackUrl,
            $fallback_url: fallbackUrl
        }
        let buoRef: any = null
        branch
            .createBranchUniversalObject(`shareProfile/${user.id}`, {
                locallyIndex: true,
                title: '',
                contentDescription: '',
                contentImageUrl: user.photo || '',
                contentMetadata: {
                    customMetadata: {
                        user: JSON.stringify(user),
                        type: 'Share Profile'
                    }
                }
            })
            .then((buo: any) => {
                const { id = '', firstname = '', lastname = '', email = '' } = user

                new BranchEvent(SHARE_PROFILE_EVENT, buo, {
                    sharedBy: id,
                    firstname,
                    lastname,
                    email
                }).logEvent()

                buoRef = buo
                return buo.generateShortUrl(linkProperties, controlParams)
            })
            .then((response: any) => {
                if (buoRef && typeof buoRef.release === 'function') {
                    buoRef.release()
                }
                resolve(response.url)
            })
            .catch((error: any) => {
                console.warn('**** createSharedUserURL error: ', error)
                reject(error)
            })
    })
}

function createSharedAppURL(userProfile: UserProfile) {
    return new Promise<string>((resolve, reject) => {
        const linkProperties = {
            feature: 'share',
            campaign: 'referral'
        }
        const controlParams = {
            $ios_url: fallbackUrl,
            $fallback_url: fallbackUrl
        }

        const { id } = userProfile
        let buoRef: any = null
        branch
            .createBranchUniversalObject(
                `share/${id}`,
                {
                    locallyIndex: true,
                    title: '',
                    contentDescription: ''
                },
                {
                    contentMetadata: {
                        customMetadata: {
                            invitedBy: id,
                            type: 'Share App'
                        }
                    }
                }
            )
            .then((buo: any) => {
                buoRef = buo
                const { id = '', firstname = '', lastname = '', email = '' } = userProfile
                buo.logEvent(BranchEvent.CompleteRegistration, {
                    invitedBy: id,
                    firstname,
                    lastname,
                    email
                })
                new BranchEvent(BranchEvent.CompleteRegistration, buo, {
                    invitedBy: id,
                    firstname,
                    lastname,
                    email
                }).logEvent()

                return buo.generateShortUrl(linkProperties, controlParams)
            })
            .then((response: any) => {
                if (buoRef && typeof buoRef.release === 'function') {
                    buoRef.release()
                }
                resolve(response.url)
            })
            .catch((error: any) => {
                console.warn('**** createSharedAppURL error: ', error)
                reject(error)
            })
    })
}

function createSharedItemURL(fromUser: UserProfile, item: any) {
    return new Promise<string>((resolve, reject) => {
        const { name, description, photos, tags: tagObjects = [], id } = item
        let tags = []
        if (Array.isArray(tagObjects) && tagObjects.length > 0) {
            tags = tagObjects.map(obj => obj.tag)
        }
        const linkProperties = {
            feature: 'share',
            campaign: 'referral',
            tags
        }
        const controlParams = {
            $ios_url: fallbackUrl,
            $fallback_url: fallbackUrl
        }
        let buoRef: any = null
        branch
            .createBranchUniversalObject(`shareItem/${id}`, {
                locallyIndex: true,
                title: name,
                contentDescription: description,
                contentImageUrl: photos[0].url,
                contentMetadata: {
                    customMetadata: {
                        referrer: JSON.stringify(fromUser),
                        item: JSON.stringify(item),
                        type: 'Share Item'
                    }
                }
            })
            .then((buo: any) => {
                const { id = '', firstname = '', lastname = '', email = '' } = fromUser

                new BranchEvent(SHARE_ITEM_EVENT, buo, {
                    sharedBy: id,
                    firstname,
                    lastname,
                    email,
                    itemId: item.id,
                    itemName: item.name
                }).logEvent()

                buoRef = buo
                return buo.generateShortUrl(linkProperties, controlParams)
            })
            .then((response: any) => {
                if (buoRef && typeof buoRef.release === 'function') {
                    buoRef.release()
                }
                resolve(response.url)
            })
            .catch((error: any) => {
                console.log('createBranchUniversalObject error: ', error)
                reject(null)
            })
    })
}

function trackingRegister(user: UserProfile) {
    const { id = '', name = '' } = user
    branch.userCompletedAction(BranchEvent.CompleteRegistration, { id, name })
    branch.setIdentity(id)
}

function userLogout() {
    branch.logout()
}

function setUserId(id) {
    branch.setIdentity(id)
}
*/

export default {
    init,
    trackingRegister,
    setUserId,
    trackLogin,
    trackAppLaunch,
    trackEvent,
    EVENTS
}
