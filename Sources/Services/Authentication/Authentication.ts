import Api from '../Api/Api'
import Network from '../Network/Network'
import { UserProfile } from '@Models'
import {
  UserTokenActions,
  getStore,
  UserProfileActions,
  UserItemActions,
  ItemSummaryActions,
  PaymentActions,
  FavoritesActions,
  ItemListActions,
  NotificationSettingsActions,
  PayoutActions,
  FilterActions,
  SearchHistoryActions
} from '@ReduxManager'

import FirebaseHelper, { RNFirebaseUser } from '../Firebase/FirebaseHelper'
import FacebookHelper from '../Facebook/FacebookHelper'
import StripeHelper from '../Stripe/StripeHelper'
import { Utils, AppConfig, Validator, Device } from '@Utils'

function logout(): Promise<boolean> {
  return new Promise((resolve, reject) => {
    FirebaseHelper.setBadge(0)
    FirebaseHelper.updatePushTokenForUser(null, () => {
      FirebaseHelper.logOut()
        .then(() => {
          UserTokenActions.deleteUserToken()
          ItemListActions.resetItemList()
          ItemSummaryActions.resetItemSummary()
          UserItemActions.resetUserItem()
          FavoritesActions.resetFavorites()
          NotificationSettingsActions.resetNotificationSettings()
          SearchHistoryActions.resetSearchHistory()
          FilterActions.resetFilters()
          PaymentActions.resetPayment()
          PayoutActions.resetPayout()
          UserProfileActions.resetUserProfile()
          FacebookHelper.logOut()
          resolve(true)
        })
        .catch(error => {
          reject(error)
        })
    })
  })
}

function loginAndCreateSession(user: RNFirebaseUser): Promise<boolean> {
  return new Promise((resolve, reject) => {
    const { filters } = getStore().getState()
    const { itemList, categories, userItems } = filters
    let filterItemListParams = itemList
    let filterCatoloriesParams = categories
    let userItemsParams = userItems

    checkIfUserExists(user)
      .then(userProfile => {
        console.log('***** checkIfUserExists userProfile: ', userProfile)
        if (!userProfile) {
          let error: any = Error('User not found')
          error.code = 404
          throw error
        }

        let newUser = { ...userProfile }

        const { id, longitude, latitude, payment_customer_id, payment_account_id, locality } = newUser

        if (typeof longitude === 'number' && typeof latitude === 'number') {
          filterItemListParams.filters['location'] = { longitude, latitude }
          filterItemListParams.filters['locality'] = locality
          if (typeof filterCatoloriesParams.filters === 'object') {
            filterCatoloriesParams.filters['location'] = { longitude, latitude }
          } else {
            filterCatoloriesParams.filters = {
              location: { longitude, latitude }
            }
          }
        }

        if (!filterItemListParams.filters.location) {
          filterItemListParams.filters.location = { longitude: AppConfig.longitude, latitude: AppConfig.latitude }
        }

        userItemsParams.id = id

        let promises = [
          Api.getItemByUser(userItemsParams),
          Api.getItemSummary(filterCatoloriesParams),
          Api.getNotificationSettings(),
          Api.getFavorites(),
          Api.getItemList(filterItemListParams)
        ]
        if (payment_customer_id !== null && payment_customer_id.length) {
          promises.push(StripeHelper.getCustomInfo(payment_customer_id))
        }

        if (payment_account_id !== null && payment_account_id.length) {
          promises.push(StripeHelper.getPayoutAccount(payment_account_id))
        }

        if (Validator.isURL(newUser.photo) == false) {
          newUser.photo = ''
        }

        UserProfileActions.saveUserProfile(newUser)

        return Promise.all(promises)
      })
      .then((values: any) => {
        const userItems = values[0] && values[0].code === 200 ? values[0].items : []
        const itemsSummary =
          values[1] && values[1].code === 200 && Array.isArray(values[1].categories) ? values[1].categories : []
        const settings = values[2] && values[2].code === 200 ? values[2].settings : null
        const favorites = values[3] && values[3].code === 200 ? values[3].favorites : null
        const itemList = values[4] && values[4].code === 200 && Array.isArray(values[4].items) ? values[4].items : []

        const paymentInfo = values[5]
        const payoutMethods = values[6] && Array.isArray(values[6].data) ? values[6].data : []
        UserItemActions.saveUserItems(userItems)
        ItemSummaryActions.saveItemSummary(itemsSummary)
        NotificationSettingsActions.saveNotificationSettings(settings)
        FavoritesActions.saveFavorites(favorites)
        ItemListActions.saveItemList(itemList)
        PayoutActions.saveAllPayoutMethods(payoutMethods)

        filterItemListParams.page = itemList.length ? 2 : 1
        userItemsParams.page = userItems.length ? 2 : 1

        FilterActions.updateFilters(filterItemListParams, filterCatoloriesParams, userItemsParams)

        console.log('***** paymentInfo', paymentInfo)
        if (paymentInfo) {
          PaymentActions.savePaymentInfo(paymentInfo)
          let paymentMethods = []
          if (paymentInfo.sources && Array.isArray(paymentInfo.sources.data) && paymentInfo.sources.total_count) {
            paymentMethods = paymentInfo.sources.data
            PaymentActions.saveAllPaymentMethods(paymentMethods)
          }
        }
        resolve(true)
      })
      .catch((error: any) => {
        console.log('***** error:', error, error.code)
        if (error && error.code === 404) {
          resolve(null)
        } else {
          reject(error)
        }
      })
  })
}

function _updateUserPushTokenInfo(user: UserProfile) {
  const userName = Utils.getUserName(user)
  const token = Network.getToken()
  const { id, photo, role } = user

  FirebaseHelper.pushToken(token, id, userName, role, photo)
    .then(response => {
      console.log('**** push token success: ', { token, id, userName, role, photo })
    })
    .catch(error => {
      console.log('**** push token error: ', error)
    })
}

function checkIfUserExists(user: RNFirebaseUser): Promise<UserProfile> {
  return new Promise((resolve, reject) => {
    let userProfile: UserProfile = null
    user
      .getIdToken()
      .then((token: string) => {
        Network.setToken(token)
        return true
      })
      .then(() => {
        return Api.getUserDetail<UserProfile>()
      })
      .then((response: any) => {
        const user = response.user
        if (!user) {
          let error: any = Error('User not found')
          error.code = 404
          throw error
        }
        userProfile = user

        console.log('**** user info detail: ', userProfile)
        _updateUserPushTokenInfo(user)
        return Utils.getAddress(user)
      })
      .then(address => {
        const name = Utils.getUserName(userProfile)
        const { payment_customer_id, phone, email } = userProfile
        const body = {
          phone,
          name,
          email,
          metadata: {
            Address: address.address,
            'App Version': Device.getAppVersion(),
            Device: Device.getDeviceInfo()
          }
        }

        if (payment_customer_id == null || payment_customer_id.length == 0) {
          console.log('******** createCustomerPaymentAccount', body)
          return StripeHelper.createCustomerPaymentAccount(email, name, phone, body.metadata)
        } else {
          console.log('******** updateCustomerPaymentAccount', body)
          return StripeHelper.updateCustomerPaymentAccount(payment_customer_id, body)
        }
      })
      .then((response: any) => {
        console.log('**** update or create successfully', response)
        if (!response.id) throw Error('Invalid Customer ID')
        return Api.editUserProfile({ payment_customer_id: response.id })
      })
      .then((response: any) => {
        const { code, user } = response
        if (code !== 200 || !user) {
          throw Error(response.message)
        }
        console.log('***** Edit profile success: ', response)
        user.is_in_waitlist = userProfile.is_in_waitlist // vinayak on 14 nov 2019
        resolve(user)
      })
      .catch(error => {
        if (error && error.code === 404) {
          resolve(null)
        } else {
          reject(error)
        }
      })
  })
}

function createSession(): Promise<boolean> {
  return new Promise((resolve, reject) => {
    FirebaseHelper.hasLoggedInUser()
      .then(user => {
        if (user !== null) {
          return loginAndCreateSession(user)
        } else {
          return false
        }
      })
      .then(isOK => {
        resolve(isOK)
      })
      .catch(error => {
        console.warn('***** error:', error)
        reject(error)
      })
  })
}

export default {
  logout,
  loginAndCreateSession,
  createSession
}
