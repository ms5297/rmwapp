import Axios from 'axios'
import qs from 'qs'
import { AppConfig } from '@Utils'

const stripe_url = 'https://api.stripe.com/v1'
const strike_key = AppConfig.isStaging ? AppConfig.stripe_key_staging : AppConfig.stripe_key_prod

const headers = {
  accept: 'application/json',
  'Content-Type': 'application/x-www-form-urlencoded',
  Authorization: `Bearer ${strike_key}`
}

function createCustomerPaymentAccount(email: string, name: string, phone: string, metadata?: object) {
  const body = qs.stringify({ email, name, phone, metadata })
  const url = `${stripe_url}/customers`
  return Axios.post(url, body, { headers })
}

function updateCustomerPaymentAccount(cus_id: string, data: object) {
  const url = `${stripe_url}/customers/${cus_id}`
  const body = qs.stringify(data)
  return Axios.post(url, body, { headers })
}

function createCustomerPayoutAccount(payload) {
  const url = `${stripe_url}/accounts`
  const body = qs.stringify(payload)
  return Axios.post(url, body, { headers })
}

function getBankAccounts(customerId: string) {
  const url = `${stripe_url}/customers/${customerId}/sources?object=bank_account&limit=3`
  return Axios.get(url, { headers })
}

function getPayoutAccount(accountId: string) {
  const url = `${stripe_url}/accounts/${accountId}/external_accounts?object=bank_account&limit=3`
  return Axios.get(url, { headers })
}

function createBankAccount(customerId, accountData) {
  const url = `${stripe_url}/customers/${customerId}/sources`
  const body = qs.stringify(accountData)
  return Axios.post(url, body, { headers })
}

function deleteBankAccount(accountId) {
  const url = `${stripe_url}/accounts/${accountId}`
  return Axios.delete(url, { headers })
}

function createCard(customerId: string, payload: object) {
  const body = qs.stringify(payload)
  const url = `${stripe_url}/customers/${customerId}/sources`
  return Axios.post(url, body, { headers })
}

function getCardList(customerId: string) {
  const url = `${stripe_url}/customers/${customerId}/sources?object=card&limit=10`
  return Axios.get(url, { headers })
}

function getCustomInfo(customerId: string) {
  const url = `${stripe_url}/customers/${customerId}`
  return Axios.get(url, { headers })
}

function setDefaultCard(customerId: string, default_source: string) {
  const body = qs.stringify({
    default_source
  })
  const url = `${stripe_url}/customers/${customerId}`
  return Axios.post(url, body, { headers })
}

function deleteCard(customerId: string, cardId: string) {
  const url = `${stripe_url}/customers/${customerId}/sources/${cardId}`
  return Axios.delete(url, { headers })
}

export default {
  updateCustomerPaymentAccount,
  createCustomerPaymentAccount,
  createCustomerPayoutAccount,
  getBankAccounts,
  getPayoutAccount,
  createBankAccount,
  deleteBankAccount,
  createCard,
  getCardList,
  getCustomInfo,
  setDefaultCard,
  deleteCard
}
