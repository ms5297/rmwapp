import { NavigationScreenProp, NavigationState } from 'react-navigation'
import { ViewProps, ImageSourcePropType, ImageStyle, ViewStyle, TextStyle, StyleProp, ImageProps } from 'react-native'
import { Item } from '@Models'
import { Style, IconProps } from 'rn-components'

export type NavigationProps = NavigationScreenProp<NavigationState>
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>

export type UploadImageViewProps = ViewProps & {
  editable?: boolean
  defaultImageSource?: ImageSourcePropType | null
  source?: ImageSourcePropType | null
  onSourceChange?: (source?: ImageSourcePropType) => void
  ContentComponent?: React.ReactElement | React.FunctionComponent
  cropperCircleOverlay?: boolean
  maxImageWidth?: number
  maxImageHeight?: number
  imageStyle?: ImageStyle
  loadingStyle?: ViewStyle
  editButtonStyle?: ViewStyle
  fallbackImageSource?: ImageSourcePropType | null
}

export type SectionProps = ViewProps & {
  onPress?: () => void
  title?: string
  titleContainerStyle?: StyleProp<ViewStyle>
  titleStyle?: StyleProp<TextStyle>
  rightContainerStyle?: StyleProp<ViewStyle>
  iconSource?: ImageSourcePropType
  iconStyle?: StyleProp<ImageStyle>
  value?: string
  valueStyle?: StyleProp<TextStyle>
}

export type TabProps = ViewProps & {
  iconStyle?: StyleProp<ImageStyle>
  iconSource?: ImageSourcePropType
  activeIconSource?: ImageSourcePropType
  text?: string
  textStyle?: StyleProp<TextStyle>

  onPress?: () => void
}

export type BottomTabBarProps = ViewProps & {
  tabs: TabProps[]
  textFontFamily?: string
  textFontSize?: number
  activeColor?: string
  inactiveColor?: string
  navigation?: NavigationProps
  jumpTo?: (key: string) => void
}

type ImageViewExtraProps = Pick<ImageProps, 'resizeMethod' | 'resizeMode' | 'source' | 'width' | 'height'>

export type AsyncImageProps = ImageViewExtraProps & {
  source?: ImageSourcePropType
  style?: StyleProp<ViewStyle>
  imageStyle?: StyleProp<ImageStyle>
  loadingStyle?: StyleProp<ViewStyle>
  fallbackImageSource?: ImageSourcePropType
  placeholderSource?: ImageSourcePropType
  placeholderStyle?: StyleProp<ImageStyle>
  acceptLocalImage?: boolean
}

export type EmptyViewProps = ViewProps & {
  title?: string
  titleImage?: ImageSourcePropType
  titleStyle?: StyleProp<TextStyle>
  subTitle?: string
  subTitleStyle?: StyleProp<TextStyle>
  info?: string
  infoStyle?: StyleProp<TextStyle>
  onPress?: () => void
  buttonText?: string
  buttonTextStyle?: StyleProp<TextStyle>
  buttonStyle?: StyleProp<ViewStyle>
}
export type EmptyViewTwoButtonProps = EmptyViewProps & {
  secondButtonText: string,
  onPressSecondButton?: () => void
}

export type HorizontalListPhotosProps = ViewProps & {
  photos?: Array<string>
}

export type TagInputProps = ViewProps & {
  horizontalTags?: Array<string>
  onTagsSelected?: (tags: Array<string>) => void
  onSubmitEditing?: () => void
}

export type ItemCellProps = ViewProps & {
  item: Item
  width: number
  height?: number
  editable?: boolean
  ownerNameVisible?: boolean
  hiddenRating?: boolean
  onPress?: (item: Item) => void
  onTapFavoriteItem?: (item: Item) => void
  onTapMoreOption?: (item: Item) => void
  shouldRefreshCategoryFavorite?: boolean
}

export type CameraButtonProps = IconProps & {
  onSourceChanged: (url: string) => void
  cropperCircleOverlay?: boolean
}

export type CollapsibleViewProps = ViewProps & {
  containerStyle?: StyleProp<ViewStyle>
  title?: string
  subTitle?: string
  highlightSubTitle?: string
  disabled?: boolean
  expanding?: boolean
  hasTopDivider?: boolean
  paddingBottom?: number
  RightSubComponent?: React.ReactElement | React.FunctionComponent
}
