import { StyleSheet } from 'react-native'
import Assets from '@Assets'

const Styles = StyleSheet.create({
  headerTitle: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    color: '#455154',
    alignSelf: 'center',
    textAlign: 'center'
  },
  nextAccessoryContainer: {
    paddingRight: 15,
    alignSelf: 'flex-end'
  },
  nextAccessoryText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    color: Assets.colors.appTheme
  },
  headerBack: {
    tintColor: Assets.colors.appTheme
  },
  headerLeft: {
    marginLeft: 15,
    justifyContent: 'center'
  },
  headerRight: {
    marginRight: 15,
    justifyContent: 'center'
  },
  textHeaderRight: {
    textAlign: 'right',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.appTheme,
    letterSpacing: -0.4,
    marginRight: 15
  }
})

export default Styles
