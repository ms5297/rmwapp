import { Platform } from 'react-native'

const isAndroid = Platform.OS === 'android'

const fonts = {
  text: {
    regular: isAndroid ? 'SFProText' : 'SFProText-Regular',
    medium: isAndroid ? 'SFProText_medium' : 'SFProText-Medium',
    semibold: isAndroid ? 'SFProText_semibold' : 'SFProText-Semibold',
    bold: isAndroid ? 'SFProText_bold' : 'SFProText-Bold',
    light: isAndroid ? 'SFProText_light' : 'SFProText-Light',
    heavy: isAndroid ? 'SFProText_heavy' : 'SFProText-Heavy'
  },
  display: {
    regular: isAndroid ? 'SFProDisplay' : 'SFProDisplay-Regular',
    medium: isAndroid ? 'SFProDisplay_medium' : 'SFProDisplay-Medium',
    semibold: isAndroid ? 'SFProDisplay_semibold' : 'SFProDisplay-Semibold',
    bold: isAndroid ? 'SFProDisplay_bold' : 'SFProDisplay-Bold',
    light: isAndroid ? 'SFProDisplay_light' : 'SFProDisplay-Light',
    heavy: isAndroid ? 'SFProDisplay_heavy' : 'SFProDisplay-Heavy'
  }
}
const colors = {
  appTheme: '#F06182', // Main theme of the application
  mainText: '#455154', // Text color for most of the controls like menu, options etc
  textLight: '#969FA2', // Light color for placeholder, version, etc
  homeTab: '#969FA2', // Tab control color
  placeholder: '#45515460',
  componentBg: 'white', // Background color for all screens
  componentHeaderBg: 'white', // Application header background color
  componentHeaderTitle: '#455154', // Application header foreground color
  headerSearchBarBg: '#F4F6F6', // Search bar background color
  inputBg: '#F4F6F6',
  borderColor: '#C4CACC',
  borderColorLight: '#C4CACC80',
  buttonColor: '#F06182',
  borderColorDark: '#45515430',
  codeColor: '#45515470',
  seperatorColor: '#E7EAEB',
  error: '#cc3232',
  statusBar: isAndroid ? '#F06182' : 'transparent',
  green: '#4CB359'
}

const images = {
  tryOn: require('./Images/tryOn.png'),
  apparel: require('./Images/apparel.png'),
  directExchange: require('./Images/directExchange.png'),
  chat_send: require('./Images/chat_send.png'),
  ob0: require('./Images/Tour.a.png'),
  ob1: require('./Images/Tour.b.png'),
  ob2: require('./Images/Tour.c.png'),
  ob3: require('./Images/Tour.d.png'),
  ob4: require('./Images/Tour.d.png'),
  addCircle: require('./Images/addCircle.png'),
  logoFacebook: require('./Images/logoFacebook.png'),
  menu_home: require('./Images/menu_search.png'),
  menu_favorites: require('./Images/menu_favorites.png'),
  menu_closet: require('./Images/menu_closet.png'),
  menu_inbox: require('./Images/menu_inbox.png'),
  menu_profile: require('./Images/menu_profile.png'),
  categoryDetailsArrow: require('./Images/categoryDetailsArrow.png'),
  app_bg: require('./Images/appBG.png'),
  user_photo_placeholder: require('./Images/user_photo_placeholder.png'),
  camera_button: require('./Images/camera_button.png'),
  filterIcon: require('./Images/filter.png'),
  button_bg: require('./Images/button_bg.png'),
  itemImagePlaceHolder: require('./Images/itemImagePlaceHolder.png'),
  ratingEmptyStar: require('./Images/ratingEmptyStar.png'),
  ratingFullStar: require('./Images/ratingFullStar.png'),
  ratingHalfStar: require('./Images/ratingHalfStar.png'),
  heartIcon: require('./Images/heartIcon.png'),
  heartOutline: require('./Images/heartOutline.png'),
  questionMark: require('./Images/questionmark.png'),
  questionMarkPink: require('./Images/questionmark-pink.png'),

  defaultProfile: require('./Images/defaultProfile.png'),
  tickmark_icon: require('./Images/tickmark_icon.png'),
  calendar: require('./Images/calendar.png'),
  user_comments_icon: require('./Images/user_comments_icon.png'),
  security_icon: require('./Images/security_icon.png'),
  shareIcon: require('./Images/shareIcon.png'),
  checkboxOn: require('./Images/checkboxOn.png'),
  trash: require('./Images/trash.png'),
  searchHistoryIcon: require('./Images/searchHistoryIcon.png'),
  searchIcon: require('./Images/searchIcon.png'),

  backIcon: require('./Images/backIcon.png'),
  closeIcon: require('./Images/closeIcon.png'),
  sold: require('./Images/sold.png'),
  moreIcon: require('./Images/edit.png'),
  profileEditDetailIcon: require('./Images/profileEditDetailIcon.png'),
  next_round: require('./Images/next_round.png'),
  chat_camera: require('./Images/chat_camera.png'),
  cartImagePlaceHolder: require('./Images/cartImagePlaceHolder.png'),
  returnPhotoEmpty: require('./Images/return_photo_empty.png'),
  returnPhotoCamera: require('./Images/return_photo_camera.png'),
  chatArchive: require('./Images/archive_chat.png'),
  profile_facebook: require('./Images/profile_facebook.png'),
  profile_instagram: require('./Images/profile_instagram.png'),
  profile_twitter: require('./Images/profile_twitter.png'),
  details_star: require('./Images/details_star.png'),
  profle_wardrobe_empty_icon: require('./Images/profle_wardrobe_empty_icon.png'),
  appIntro1: require('./Images/appIntro1.png'),
  appIntro2: require('./Images/appIntro2.png'),
  appIntro3: require('./Images/appIntro3.png'),
  appIntro4: require('./Images/appIntro4.png'),
  appIntro5: require('./Images/appIntro5.png'),
  appIntro6: require('./Images/appIntro6.png'),
  appIntro7: require('./Images/appIntro7.png'),
  appIntro8: require('./Images/appIntro8.png'),
  splashScreen: require('./Images/splashScreen.png'),
  chevronDown: require('./Images/chevronDown.png'),
  trashCircle: require('./Images/trashCircle.png'),
  rmw_logo: require('./Images/rmw_logo.png'),
  accessories: require('./Images/home-tab/accessories.png'),
  blazer: require('./Images/home-tab/blazer.png'),
  bottomDress: require('./Images/home-tab/bottomDress.png'),
  costume: require('./Images/home-tab/costume.png'),
  formalDress: require('./Images/home-tab/formalDress.png'),
  handbag: require('./Images/home-tab/handbag.png'),
  jewelery: require('./Images/home-tab/jewelery.png'),
  jumpSuit: require('./Images/home-tab/jumpSuit.png'),
  outerwear: require('./Images/home-tab/outerwear.png'),
  shoes: require('./Images/home-tab/shoes.png'),
  topDress: require('./Images/home-tab/topDress.png'),
  twoPiece: require('./Images/home-tab/twoPiece.png'),

  formal: require('./Images/home-tab/banner/formal.png'),
  dayTime: require('./Images/home-tab/banner/everyDay.png'),
  career: require('./Images/home-tab/banner/career.png'),
  nightOut: require('./Images/home-tab/banner/nightOut.png'),
  maternity: require('./Images/home-tab/banner/maternity.png'),
  ethnicWear: require('./Images/home-tab/banner/ethnicWear.png'),
  bannerShoes: require('./Images/home-tab/banner/shoes.png'),
  costumes: require('./Images/home-tab/banner/costumes.png'),
  bannerAccessories: require('./Images/home-tab/banner/accessories.png'),
  brideToBe: require('./Images/home-tab/banner/brideToBe.png'),
  curvy: require('./Images/home-tab/banner/curvy.png'),
  motherOfTheBride: require('./Images/home-tab/banner/motherOfTheBride.png'),
  travel: require('./Images/home-tab/banner/travel.png'),
  weddingGuest: require('./Images/home-tab/banner/weddingGuest.png'),

  bulbIcon: require('./Images/bulb1.png'),
  buttonCross: require('./Images/button_cross.png'),
  buttonBulb: require('./Images/button_bulb.png'),
  tryOn1: require('./Images/tryOn1.png'),
  tryOnBackground: require('./Images/tryOnBackground.png'),
  tryOnPagination: require('./Images/tryOnPagination.png')
  
}

export default {
  colors,
  fonts,
  images
}








// { value: 7, title: 'Purses', imgUrl: Assets.images.formalDress },





