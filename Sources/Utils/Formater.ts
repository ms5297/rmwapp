// @ts-ignore
import numeral from 'numeral'

function formatMoney(value: number | string) {
  return numeral(value).format('$0,0.00')
}

function convertToMoney(text: string | number, isInt = false) {
  let money = 0
  if (typeof text === 'string' && text !== '') {
    money = isInt ? parseInt(text) : parseFloat(text)
  }

  if (typeof text === 'number') {
    money = text
  }
  return money
}

function convertToMoneyText(value: string | number | null) {
  if (value === null) {
    return ''
  }
  if (typeof value === 'string') {
    return value
  }

  if (typeof value === 'number') {
    return value.toString()
  }

  return value
}

function convertToPercent(number) {
  return numeral(number).format('0.[00]%')
}

function convertStringToPercent(string) {
  const x = parseFloat(string)
  if (isNaN(x) || x < 0 || x > 100) {
    if (x > 100) {
      return '100'
    }
    return ''
  }

  return string
}

export default {
  formatMoney,
  convertToMoney,
  convertToMoneyText,
  convertToPercent,
  convertStringToPercent
}
