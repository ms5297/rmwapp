import { AppConfig } from "@Utils"
import Assets from "@Assets"

const appConfig = {
  isStaging: true,
  name: 'Rent My Wardrobe',
  copyright: '©copyright. All rights reserved.',
  privacy_url: 'https://rentmywardrobe.com/policies/privacy.html#privacy',
  terms_url: 'https://rentmywardrobe.com/policies/terms.html#terms',
  insurance_url: 'https://rentmywardrobe.com/policies/assurance.html#assurance',
  refund_url: 'https://rentmywardrobe.com/policies/refund.html#refund',
  help_email: 'help@rentmywardrobe.com',
  help_mail_subject: 'Help & Feedback',
  server_url_staging: 'http://18.215.115.205',
  stripe_key_staging: 'sk_test_1hsHrqSNJKWjMFSjQ27SFtjl',
  server_url_prod: 'http://54.144.159.33',
  stripe_key_prod: 'sk_live_M1Ll4upIHnnCjNKco0P1WKAl',
  load_chat_event: 'LOAD_CHAT',
  max_selection: 3,
  longitude: -96.7969879,
  latitude: 32.7766642,
  minimumRentDays: 7,
  supportMail: __DEV__ ? 'vinayak@fathomable.com' : 'info@rentmywardrobe.com',
  requestAccessCodeSubject: 'Early Access Code',
  filters: {
    sizes: null,
    priceRange: [0, 1001],
    market: null,
    locationRange: [100],
    availability: null,
    occasions: null,
    sleeves: null,
    hemlines: null,
    embellishments: null,
    necklines: null,
    styles: null,
    colors: null,
    fitProfiles: null,
    sortTypes: [{ title: 'Newest First', value: 1 }]
  },
  fieldType: {
    sizes: 1,
    market: 2,
    date: 3,
    sleeves: 4,
    hemlines: 5,
    embellishments: 6,
    necklines: 7,
    sortTypes: 8,
    fitProfiles: 9,
    conditions: 10,
    cleanings: 11,
    occasions: 12,
    refunds: 13,
    colors: 14,
    reportings: 15,
    styles: 16,
    shoeSizes: 17,
    heights: 18,
    braSizes: 19,
    backupSizes: 20
  },
  enumConfig: {
    sizes: [
      { value: 0, title: 'All', order: 0 },
      { value: 1, title: '0/XS', order: 3 },
      { value: 2, title: '4/S', order: 5 },
      { value: 3, title: '8/M', order: 7 },
      { value: 4, title: '12/L', order: 9 },
      { value: 5, title: '18/XL', order: 12 },
      { value: 6, title: '24/XXL', order: 15 },
      { value: 7, title: '0/XS', order: 3 },
      { value: 8, title: '2/XS', order: 4 },
      { value: 9, title: '2/XS', order: 4 },
      { value: 10, title: '4/S', order: 5 },
      { value: 11, title: '4/S', order: 5 },
      { value: 12, title: '6/S', order: 6 },
      { value: 13, title: '8/M', order: 7 },
      { value: 14, title: '10/M', order: 8 },
      { value: 15, title: '12/L', order: 9 },
      { value: 16, title: '14/L', order: 10 },
      { value: 17, title: '16/L', order: 11 },
      { value: 18, title: '18/XL', order: 12 },
      { value: 19, title: '20/XL', order: 13 },
      { value: 20, title: '22/XL', order: 14 },
      { value: 21, title: '24/XXL', order: 15 },
      { value: 22, title: 'Other', order: 17 },
      { value: 23, title: '00/XXS', order: 2 },
      { value: 24, title: 'One Size Fits All', order: 1 },
      { value: 25, title: '00/XXS', order: 2 },
      { value: 26, title: '0/XS', order: 3 },
      { value: 27, title: '00/XXS', order: 2 }
    ],
    fitProfiles: [
      { value: 0, title: 'True to size', order: 1 },
      { value: 1, title: 'Slightly smaller', order: 2 },
      { value: 2, title: 'Slightly larger', order: 3 },
      { value: 3, title: 'Significantly Smaller', order: 4 },
      { value: 4, title: 'Significantly Larger', order: 5 },
      { value: 5, title: 'All', order: 0 }
    ],
    cleanings: [
      {
        additionalKey: 'cleaning_fee',
        additionalTitle: 'Cleaning Fee',
        value: 1,
        title: "Drop off at cleaners and I'll pick up"
      },
      {
        additionalKey: 'cleaning_fee',
        additionalTitle: 'Cleaning Fee',
        value: 0,
        title: 'I will handle cleaning'
      },
      { value: 2, title: 'Return after cleaning' },
      { title: 'Return after dry cleaning', value: 3 }
    ],
    hemlines: [
      { value: 0, title: 'All' },
      { value: 1, title: 'Maxi (Floor)' },
      { value: 2, title: 'Tea' },
      { value: 3, title: 'Knee' },
      { value: 4, title: 'Midi' },
      { value: 5, title: 'Mini' },
      { value: 6, title: 'High / Low' },
      { value: 7, title: 'Asymmetrical' },
      { value: 8, title: 'Mermaid' },
      { value: 9, title: 'Other' }
    ],
    necklines: [
      { value: 0, title: 'Asymmetrical', order: 1 },
      { value: 1, title: 'Crew', order: 2 },
      { value: 2, title: 'Halter', order: 3 },
      { value: 3, title: 'High', order: 4 },
      { value: 4, title: 'Off Shoulder', order: 5 },
      { value: 5, title: 'Scoop', order: 6 },
      { value: 6, title: 'Straight', order: 7 },
      { value: 7, title: 'V Neck', order: 8 },
      { value: 8, title: 'Square', order: 9 },
      { value: 9, title: 'Boat', order: 10 },
      { value: 10, title: 'Cross Strapped', order: 11 },
      { value: 11, title: 'Illusion', order: 12 },
      { value: 12, title: 'Other', order: 13 },
      { value: 13, title: 'Plunging', order: 14 },
      { value: 15, title: 'Sweatheart', order: 15 },
      { value: 16, title: 'All', order: 0 }
    ],
    conditions: [
      { value: 0, title: 'New' },
      { value: 1, title: 'Like New' },
      { value: 2, title: 'Used' },
      { value: 3, title: 'Lightly Used' },
      { title: 'Heavily Used', value: 4 }
    ],
    sortTypes: [
      { value: 1, title: 'Newest First' },
      { value: 2, title: 'Price: low to high' },
      { value: 3, title: 'Price: high to low' }
    ],
    sleeves: [
      { value: 0, title: 'All' },
      { value: 1, title: 'Strapless' },
      { value: 2, title: 'Sleeveless' },
      { value: 3, title: 'Spaghetti' },
      { value: 4, title: 'Cap Sleeve' },
      { value: 5, title: 'Short' },
      { value: 6, title: '3/4' },
      { value: 7, title: 'Long' },
      { value: 8, title: 'Bell' },
      { value: 9, title: 'Off Shoulder' },
      { value: 10, title: 'Asymmetrical' },
      { value: 11, title: 'Other' }
    ],
    colors: [
      { value: 0, title: 'All' },
      { value: 1, title: 'White' },
      { value: 2, title: 'Black' },
      { value: 3, title: 'Red' },
      { value: 4, title: 'Orange' },
      { value: 5, title: 'Yellow' },
      { value: 6, title: 'Green' },
      { value: 7, title: 'Blue' },
      { value: 8, title: 'Purple' },
      { value: 9, title: 'Pink' },
      { value: 10, title: 'Navy' },
      { value: 11, title: 'Gray' },
      { value: 12, title: 'Brown' },
      { value: 13, title: 'Tan' },
      { value: 14, title: 'Cream/Off-White' },
      { value: 15, title: 'Gold' },
      { value: 16, title: 'Silver' },
      { value: 17, title: 'Pattern' },
      { value: 18, title: 'Print' },
      { value: 19, title: 'Multi-color' }
    ],
    occasions: [
      { value: -1, title: 'Recently added' },
      { value: -2, title: 'Nearby' },
      { value: 1, title: 'Formal' },
      { value: 2, title: 'Day time' },
      { value: 3, title: 'Career' },
      { value: 4, title: 'Night Out' },
      { value: 5, title: 'Maternity' },
      { value: 6, title: 'Ethnic Wear' },
      { value: 7, title: 'Purses' },
      { value: 8, title: 'Shoes' },
      { value: 9, title: 'Costumes' },
      { value: 10, title: 'Accessories' },
      { value: 11, title: 'Bride to Be' },
      { value: 12, title: 'Curvy' }
    ],
    styles: [
      { value: 0, title: 'All' },
      { value: 1, title: 'Romper/ Jumpsuit' },
      { value: 2, title: 'Two-Piece' },
      { value: 3, title: 'Maxie' },
      { value: 4, title: 'Shirtdress' },
      { value: 5, title: 'Sheath' },
      { value: 6, title: 'Shift' },
      { value: 7, title: 'Fit & Flare' },
      { value: 8, title: 'Formal' },
      { value: 9, title: 'A-line' },
      { value: 10, title: 'Body-Con' },
      { value: 11, title: 'Other' },
      { value: 12, title: 'Non-Garment' }
    ],
    markets: [{ value: 1, title: 'Dallax, TX' }, { title: 'Austin, TX', value: 2 }],
    embellishments: [
      { value: 0, title: 'All' },
      { value: 1, title: 'Cut Outs' },
      { value: 2, title: 'Lace' },
      { value: 3, title: 'Sequins' },
      { value: 4, title: 'Beaded' },
      { value: 5, title: 'Metallic' },
      { value: 6, title: 'Backless' },
      { value: 7, title: 'Peplum' },
      { value: 8, title: 'Scuba' },
      { value: 9, title: 'Other' }
    ],
    reportings: [
      { value: 1, title: 'Inappropriate content' },
      { value: 2, title: 'Dishonest or hateful content' },
      { value: 3, title: 'Inaccurate or incorrect' },
      { value: 4, title: "It's a scam" },
      { value: 5, title: "It's offensive" },
      { value: 6, title: 'Fake content' }
    ],
    refunds: [
      { value: 2, title: 'Strict: Sorry, No Refund' },
      {
        value: 1,
        title:
          'Flexible: Full refund for cancellations made before 3 days of the rental date. 80% refund for cancellations made within 72 hours of the rental date.'
      }
    ],
    shoeSizes: [
      { value: 1, title: '4.5/35' },
      { value: 2, title: '5/36' },
      { value: 3, title: '6/37.5' },
      { value: 4, title: '6.5/38' },
      { value: 5, title: '7/39' },
      { value: 6, title: '7.5/39.5' },
      { value: 7, title: '8/40' },
      { value: 8, title: '8.5/41' },
      { value: 9, title: '9/41.5' },
      { value: 10, title: '9/41.5' },
      { value: 11, title: '9.5/42' },
      { value: 12, title: '10/42.5' },
      { value: 13, title: '11/44' },
      { value: 14, title: '11.5/44.5' },
      { value: 14, title: '12/45' }
    ],
    heights: [
      ["1'", "2'", "3'", "4'", "5'", "6'", "7'", "8'", "9'", "10'"],
      ['1"', '2"', '3"', '4"', '5"', '6"', '7"', '8"', '9"', '10"', '11"']
    ],
    braSizes: [
      ['30', '32', '34', '36', '38', '40', '42', '44', '46', '48'],
      ['A', 'B', 'C', 'D', 'DD', 'DDD/E', 'F', 'G', 'H']
    ],
    itemTypes: [
      { value: 1, title: 'Dress', imgUrl: Assets.images.formalDress },
      { value: 2, title: 'Jumpsuit', imgUrl: Assets.images.jumpSuit },
      { value: 3, title: 'Top', imgUrl: Assets.images.topDress },
      { value: 4, title: 'Bottom', imgUrl: Assets.images.bottomDress },
      { value: 5, title: 'Blazer', imgUrl: Assets.images.blazer },
      { value: 6, title: 'Outer Wear', imgUrl: Assets.images.outerwear },
      { value: 7, title: 'Two Piece', imgUrl: Assets.images.twoPiece },
      { value: 8, title: 'Shoes', imgUrl: Assets.images.shoes },
      { value: 9, title: 'Costumes', imgUrl: Assets.images.costume },
      { value: 10, title: 'Accessories', imgUrl: Assets.images.accessories },
      { value: 11, title: 'Handbag', imgUrl: Assets.images.handbag },
      { value: 12, title: 'Jewelry', imgUrl: Assets.images.jewelery }

      // - Dress, Jumpsuit, Two - Piece, Top, Bottom, Blazer, Outerwear, Handbag, Jewelry, Accessories, Shoes
    ],
    priceList: [
      { value: 1, title: 35 },
      { value: 2, title: 55 },
      { value: 3, title: 75 },
      { value: 4, title: 95 },
      { value: 5, title: 105 },
      { value: 6, title: 125 }
    ],
    occasionsWithImage: [
      { value: 1, title: 'Formal', imgUrl: Assets.images.formal },
      { value: 2, title: 'Day time', imgUrl: Assets.images.dayTime },
      { value: 3, title: 'Career', imgUrl: Assets.images.career },
      { value: 4, title: 'Night Out', imgUrl: Assets.images.nightOut },
      { value: 5, title: 'Maternity', imgUrl: Assets.images.maternity },
      { value: 6, title: 'Ethnic Wear', imgUrl: Assets.images.ethnicWear },
      // { value: 7, title: 'Purses', imgUrl: Assets.images.bannerAccessories },
      { value: 8, title: 'Shoes', imgUrl: Assets.images.bannerShoes },
      { value: 9, title: 'Costumes', imgUrl: Assets.images.costumes },
      { value: 10, title: 'Accessories', imgUrl: Assets.images.bannerAccessories },
      { value: 11, title: 'Bride to Be', imgUrl: Assets.images.brideToBe },
      { value: 12, title: 'Curvy', imgUrl: Assets.images.curvy }

      // - Every Day - day time
      // - Hand Bags -  purses (rename?)
      // - Wedding Guest (new  category)
      // - Travel (non existing)
      // - Mother of the Bride

      // - Formal Every Day Career Collection Night Out Maternity Ethnic Wear Hand Bags Shoes Accessories Costumes Bride to Be Curvy Wedding Guest Travel Mother of the Bride





    ],
    topOccasionsWithImage: [
      { value: 11, title: 'Bride to Be', imgUrl: Assets.images.brideToBe },
      { value: 3, title: 'Career', imgUrl: Assets.images.career },
      { value: 6, title: 'Ethnic Wear', imgUrl: Assets.images.ethnicWear },
      { value: 9, title: 'Costumes', imgUrl: Assets.images.costumes },
      { value: 10, title: 'Accessories', imgUrl: Assets.images.bannerAccessories },
    ],
  }
}

export default appConfig

