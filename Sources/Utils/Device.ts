import { Dimensions, Platform, StatusBar } from 'react-native'
import { getStore } from '@ReduxManager'
import RNDeviceInfo from 'react-native-device-info'

const _isAndroid = Platform.OS == 'android'
const _version = Platform.Version
const { width, height } = Dimensions.get('window')

const vs = (height / 812) * (height <= 768 ? 0.8 : 1)

const hs = width / 375

function isAndroid(): boolean {
  return _isAndroid
}

function version(): string | number {
  return _version
}

function getScreenSize(): { width: number; height: number } {
  return { width, height }
}

function isIphoneX() {
  return Platform.OS === 'ios' && (height === 812 || width === 812 || (height === 896 || width === 896))
}

function ifIphoneX(iphoneXStyle: number, regularStyle: number) {
  if (isIphoneX()) {
    return iphoneXStyle
  }
  return regularStyle
}

function getStatusBarHeight(safe?: boolean) {
  return Platform.select({
    ios: ifIphoneX(safe ? 44 : 30, 20),
    android: StatusBar.currentHeight
  })
}

function getHeaderHeight() {
  const headerHeight = Platform.OS == 'android' ? 56 : 44
  return headerHeight
}

function getAppVersion() {
  const appVersion = RNDeviceInfo.getVersion()
  const appBuildNumber = RNDeviceInfo.getBuildNumber()
  const version = `${appVersion}.${appBuildNumber}`
  return version
}

function getCodePushVersion() {
  const { appConfiguration } = getStore().getState()
  const version = appConfiguration ? appConfiguration.codePushVersion : ''
  return version
}

function getDeviceUUID() {
  return RNDeviceInfo.getUniqueID()
}

function getDeviceInfo() {
  const version = RNDeviceInfo.getSystemVersion()
  const name = RNDeviceInfo.getDeviceName()
  return `${name} v${version}`
}
export default {
  vs,
  hs,
  getScreenSize,
  isAndroid,
  version,
  getStatusBarHeight,
  getHeaderHeight,
  width,
  height,
  isIphoneX,
  ifIphoneX,
  getAppVersion,
  getCodePushVersion,
  getDeviceUUID,
  getDeviceInfo
}
