import moment from 'moment'

function format(date: Date | string = new Date(), format = 'YYYY-MM-DD') {
  return moment(date).format(format)
}

function diffDays(from: Date | string, to: Date | string) {
  return moment(from).diff(to, 'days')
}

function calculateAge(birthday: Date | string) {
  const age = moment().diff(birthday, 'years')
  return age
}

function getTimestamp() {
  const now = new Date()
  const timestamp = now.getTime()
  return timestamp
}

export default {
  moment,
  format,
  diffDays,
  calculateAge,
  getTimestamp
}
