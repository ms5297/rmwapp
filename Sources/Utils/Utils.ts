import DateTime from './DateTime'
import AppConfig from './AppConfig'
import {
  EnumConfigKey,
  FirebaseValueObject,
  UserProfile,
  UpdateKeys,
  RentalStatus,
  ChatModelItem,
  RentalDetailModel,
  ChatUserModel,
  SortTypeModel,
  Item,
  AdditionalFeeModel,
  RentalDetailItem
} from '@Models'
import { FirebaseHelper, FreshChatHelper, BranchHelper, AppsFlyerHelper } from '@Services'
import Validator from './Validator'
import Geocoder from 'react-native-geocoder'
import adminConfig from './AdminConfig'
import Assets from '@Assets'

function generateUUID() {
  var d = new Date().getTime()
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (d + Math.random() * 16) % 16 | 0
    d = Math.floor(d / 16)
    return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16)
  })
}

function checkDue(endDate) {
  let due = null
  if (endDate) {
    let date = DateTime.moment(endDate).format('YYYY-MM-DD')
    let today = Date.now()
    let dayToday = DateTime.moment(today).format('YYYY-MM-DD')

    let now = DateTime.moment(dayToday)
    let then = DateTime.moment(date)
    let difference = DateTime.moment.duration(now.diff(then)).get('days')
    if (difference == 0) {
      due = 'Due Today!'
    } else if (difference > 0) {
      due = 'Due (' + difference + ' days)'
    }
  }
  return due
}

function getStatusText(statusValue: RentalStatus, order: RentalDetailModel) {
  switch (statusValue) {
    case 'inquiry':
      return 'Inquiry'
    case 'approved':
      return 'Approved'
    case 'request':
      return 'Pending'
    case 'declined':
      return 'Declined'
    case 'accepted':
      return 'Reserved'
    case 'returned':
      return 'Returned'
    case 'buy':
      return 'Buy'
    case 'purchased':
      return 'Purchased'
    case 'renting':
      return 'Renting'
    case 'cancel_without_penalty':
    case 'auto_cancel':
      return 'Cancelled(Inactivity)'
    case 'rental_complete':
    case 'reviewed':
      return 'Completed'

    case 'cancelled':
      let statusText = 'Cancelled'
      if (order) {
        let cancelledId = order.cancelled_by
        if (cancelledId == null || (typeof cancelledId == 'string' && cancelledId.length == 0)) {
        } else if (cancelledId == order.items[0].owner.id) {
          statusText = 'Cancelled (Lender)'
        } else {
          statusText = 'Cancelled (Renter)'
        }
      }
      return statusText
    default:
      return statusValue
  }
}

function checkStatus(data: ChatModelItem) {
  const { itemOwner, status } = data
  let showFooter = false
  let statusValue = status
  switch (status) {
    case 'inquiry':
      showFooter = itemOwner == true
      statusValue = 'inquiry'
      break
    case 'cancel_without_penalty':
      showFooter = itemOwner == false
      statusValue = 'cancel_without_penalty'
      break
    case 'renting':
      showFooter = itemOwner == false
      statusValue = 'renting'
      break
    case 'approved':
      showFooter = itemOwner == false
      statusValue = 'approved'
      break
    case 'request':
      showFooter = true
      statusValue = 'request'
      break
    case 'purchased':
      showFooter = true
      break
    case 'returned':
      showFooter = true
      statusValue = 'returned'
      break
    case 'cancelled':
    case 'accepted':
    case 'declined':
    case 'reviewed':
    case 'auto_cancel':
    case 'rental_complete':
      showFooter = false
      statusValue = status
      break
  }
  return { showFooter, statusValue }
}

function getDateStringFromDateRange(dateRange: Array<string>) {
  let valueText = ''
  if (Array.isArray(dateRange) && dateRange.length > 0) {
    if (dateRange.length > 1) {
      let firstDate = DateTime.moment(dateRange[0], 'YYYY-MM-DD').format('MMM DD')
      let lastDate = DateTime.moment(dateRange[dateRange.length - 1], 'YYYY-MM-DD').format('MMM DD')

      // Check same month
      if (
        DateTime.moment(dateRange[0], 'YYYY-MM-DD').format('MMMM') ===
        DateTime.moment(dateRange[dateRange.length - 1], 'YYYY-MM-DD').format('MMMM')
      ) {
        valueText = firstDate + ' - ' + DateTime.moment(dateRange[dateRange.length - 1], 'YYYY-MM-DD').format('DD')
      } else {
        valueText = firstDate + ' - ' + lastDate
      }
    } else {
      let firstDate = DateTime.moment(dateRange[0], 'YYYY-MM-DD').format('MMM DD')
      valueText = firstDate + ' - Onwards'
    }
  }
  return valueText
}

function getConfigWithValue(key: EnumConfigKey, value: number): FirebaseValueObject | null {
  let config = null
  const item = AppConfig.enumConfig[key]
  if (item) {
    const filtered = item.filter(element => {
      return element.value == value
    })

    if (filtered && Array.isArray(filtered) && filtered.length > 0) {
      config = filtered[0]
    }
  } else {
    console.warn("Can't get for key", key)
  }

  return config
}

function getConfigWithTitle(key: EnumConfigKey, title: string): FirebaseValueObject | null {
  let config = null
  const item = AppConfig.enumConfig[key]
  if (item) {
    const filtered = item.filter(element => {
      return element.title == title
    })

    if (filtered && Array.isArray(filtered) && filtered.length > 0) {
      config = filtered[0]
    }
  } else {
    console.warn("Can't get for key", key)
  }

  return config
}

function getTitleWithValue(key: EnumConfigKey, value: number) {
  const config = getConfigWithValue(key, value)
  if (config) {
    return config.title
  }
  return ''
}

function checkAndUpdateProfile(userProfile: UserProfile) {
  const {
    birthday,
    longitude,
    latitude,
    height,
    weight,
    dress_size_id,
    shoe_size_id,
    backup_size_id,
    bra_size
  } = userProfile

  const requiredFileds = {
    birthday,
    longitude,
    latitude,
    height,
    weight,
    dress_size_id,
    shoe_size_id,
    bra_size,
    backup_size_id
  }

  let missingFields: { [key in UpdateKeys]: boolean } = {
    backupSizes: false,
    braSizes: false,
    date: false,
    height: false,
    market: false,
    shoeSizes: false,
    sizes: false,
    weight: false
  }

  Object.keys(requiredFileds).forEach((key, index) => {
    if (requiredFileds[key] === null || requiredFileds[key] === '') {
      switch (key) {
        case 'birthday':
          missingFields['date'] = true
          break
        case 'longitude':
        case 'latitude':
          missingFields['market'] = true
          break
        case 'height':
          missingFields['height'] = true
          break
        case 'weight':
          missingFields['weight'] = true
          break
        case 'dress_size_id':
          missingFields['sizes'] = true
          break
        case 'shoe_size_id':
          missingFields['shoeSizes'] = true
          break
        case 'bra_size':
          missingFields['braSizes'] = true
          break
        case 'backup_size_id':
          missingFields['backupSizes'] = true
          break
      }
    }
  })
  let isNeedUpdate = false
  const missingFieldsLength = Object.values(missingFields).filter(Boolean).length
  if (missingFieldsLength > 0) {
    // if (missingFieldsLength === 1 && missingFields.backupSizes) {
    //   isNeedUpdate = false
    // }  else {
    //   isNeedUpdate = true
    // }
    isNeedUpdate = missingFields.date || missingFields.market
  }

  return { isNeedUpdate, missingFields }
}

function convertSortTypeToConfig(sortType: SortTypeModel) {
  let config = AppConfig.enumConfig.sortTypes[0]

  if (sortType.field === 'rental_fee') {
    const index = sortType.order === 'asc' ? 1 : 2
    config = AppConfig.enumConfig.sortTypes[index]
  }
  return config
}

function convertConfigToSortType(config: FirebaseValueObject) {
  let sort: SortTypeModel = {
    field: 'created_at',
    order: 'desc'
  }

  if (config.value === 2) {
    sort.field = 'rental_fee'
    sort.order = 'asc'
  } else if (config.value === 3) {
    sort.field = 'rental_fee'
  }
  return sort
}

function convertConfigToTitle(config: FirebaseValueObject | Array<FirebaseValueObject>, defaultText = '') {
  let title = defaultText
  if (config) {
    if (Array.isArray(config)) {
      if (config.length == 0) {
      } else if (config.length === 1) {
        title = config[0].title
      } else {
        const indexOf = config.findIndex(item => item.title === 'All')
        if (indexOf !== -1) {
          title = 'All'
        } else {
          title = `${config.length} selected`
        }
      }
    } else {
      title = config.title
    }
  }

  return title
}

function setUser(userProfile: UserProfile) {
  FirebaseHelper.setUser(userProfile)
  FirebaseHelper.setupCrashlytics(userProfile)
  FreshChatHelper.setUser(userProfile)
  BranchHelper.setUserId(userProfile.id.toString())
  AppsFlyerHelper.setUserId(userProfile.id.toString())
}

function getUserName(userProfile: UserProfile) {
  if (!userProfile) return ''
  const { user_name, name, firstname, lastname } = userProfile
  if (user_name && user_name.length > 0) {
    return user_name
  }
  if (name && name.length > 0) {
    return name
  }
  return firstname + ' ' + lastname
}

function checkIsInDallas(lat, lng) {
  return new Promise<boolean>((resolve, reject) => {
    Geocoder.geocodePosition({ lat, lng })
      .then(response => {
        console.log('**** geocodePosition', response)
        if (!Array.isArray(response) || !response.length) {
          throw Error('Invalid response')
        }
        const location = response[0]
        const address = location ? location.formattedAddress : ''
        const subAdminArea = location.subAdminArea
        const isDallas = subAdminArea === 'Dallas' || address.includes('Dallas')
        console.log('***** location', isDallas, location, address)
        resolve(isDallas)
      })
      .catch(error => {
        console.log('**** getAddress error', error)
        resolve(false)
      })
  })
}

function checkLocation(lat, lng) {
  Geocoder.geocodePosition({ lat, lng })
    .then(response => {
      console.log('**** geocodePosition', response)
      if (!Array.isArray(response) || !response.length) {
        throw Error('Invalid response')
      }
      const location = response[0]
      const address = location ? location.formattedAddress : ''
      console.log('***** location', location, address)
    })
    .catch(error => {
      console.log('**** getAddress error', error)
    })
}
type AddressType = {
  address: string
  locality: string
}
function getAddress(userProfile: UserProfile) {
  return new Promise<AddressType>((resolve, reject) => {
    const { longitude: lng, latitude: lat } = userProfile
    if (typeof lng !== 'number' || typeof lat !== 'number') {
      resolve({ address: '', locality: '' })
      return
    }
    Geocoder.geocodePosition({ lat, lng })
      .then(response => {
        console.log('**** geocodePosition', response)
        if (!Array.isArray(response) || !response.length) {
          throw Error('Invalid response')
        }
        const location = response[0]
        const address = location ? location.formattedAddress : ''
        const subAdminArea = location ? location.subAdminArea : ''

        const isDallas = subAdminArea === 'Dallas' || address.includes('Dallas')

        resolve({
          address,
          locality: isDallas ? 'Dallas, TX' : subAdminArea
        })
      })
      .catch(error => {
        console.log('**** getAddress error', error)
        resolve({ address: '', locality: '' })
      })
  })
}

function getItemName(items: Item[]) {
  let names = []
  items.forEach(item => {
    names.push(item.name)
  })

  return names.join(',')
}

function isClosedRental(statusValue: RentalStatus) {
  return (
    statusValue === 'returned' ||
    statusValue === 'rental_complete' ||
    statusValue === 'cancelled' ||
    statusValue === 'completed' ||
    statusValue === 'reviewed' ||
    statusValue === 'declined' ||
    statusValue === 'auto_cancel'
  )
}

function createChatUser(senderId: number, userProfile: UserProfile, otherUser: ChatUserModel) {
  const sender = senderId === userProfile.id ? userProfile : otherUser
  const name = senderId === userProfile.id ? getUserName(userProfile) : otherUser.name
  const user = {
    _id: sender.id,
    name,
    avatar: sender.id == adminConfig.adminChatConfig.id ? Assets.images.rmw_logo : sender.photo
  }
  return user
}

function createAdminChatUser(senderId: number, userProfile: UserProfile, otherUser: ChatUserModel) {
  let adminUser: ChatUserModel = {
    id: adminConfig.adminChatConfig.id,
    name: adminConfig.adminChatConfig.name,
    photo: adminConfig.adminChatConfig.photo
  }
  // const sender = senderId === userProfile.id ? userProfile : (senderId === otherUser.id ? otherUser : adminUser)
  // const name = senderId === userProfile.id ? getUserName(userProfile) : (senderId === otherUser.id ? otherUser.name : adminUser.name)

  const sender = senderId === otherUser.id ? otherUser : userProfile
  const name = senderId === otherUser.id ? otherUser.name : getUserName(userProfile)

  const user = {
    _id: sender.id,
    name,
    avatar: sender.photo
  }
  return user
}

function createChatPayload(
  userProfile: UserProfile,
  text: string,
  image: string | null,
  time: number,
  isCustom: boolean
) {
  const messageId = generateUUID()
  const sender = userProfile.id
  const isImage = image !== null
  const payload = { text, sender, messageId, isImage, time, image, isCustom }
  return payload
}

function getChatUserDetails(userId: number, userProfile: UserProfile, otherUser: ChatUserModel) {
  return userId == userProfile.id ? userProfile : otherUser
}

function getChatUserPhoto(userId: number, userProfile: UserProfile, otherUser: ChatUserModel) {
  const user = getChatUserDetails(userId, userProfile, otherUser)
  return user.photo
}

function getRentalAndReplacementCost(retail) {
  let _retail = typeof retail === 'string' ? parseFloat(retail) : retail
  if (isNaN(_retail)) {
    _retail = 0
  }
  let rentalPrice = 0
  let replacementPrice = 0

  if (_retail <= 39) {
  } else if (_retail >= 40 && _retail < 50) {
    rentalPrice = 12
  } else if (_retail >= 50 && _retail < 60) {
    rentalPrice = 15
  } else if (_retail >= 60 && _retail < 70) {
    rentalPrice = 17
  } else if (_retail >= 70 && _retail < 80) {
    rentalPrice = 20
  } else if (_retail >= 80 && _retail < 90) {
    rentalPrice = 23
  } else if (_retail >= 90 && _retail < 100) {
    rentalPrice = 26
  } else if (_retail >= 100 && _retail < 200) {
    rentalPrice = 28
  } else if (_retail >= 200 && _retail < 300) {
    rentalPrice = 34
  } else if (_retail >= 300 && _retail < 400) {
    rentalPrice = 36
  } else if (_retail >= 400 && _retail < 500) {
    rentalPrice = 44
  } else if (_retail >= 500 && _retail < 600) {
    rentalPrice = 50
  } else if (_retail >= 600 && _retail < 700) {
    rentalPrice = 60
  } else if (_retail >= 700 && _retail < 800) {
    rentalPrice = 63
  } else if (_retail >= 800 && _retail < 900) {
    rentalPrice = 64
  } else if (_retail >= 900 && _retail < 1000) {
    rentalPrice = 72
  } else if (_retail >= 1000 && _retail < 2000) {
    rentalPrice = 80
  } else if (_retail >= 2000 && _retail < 3000) {
    rentalPrice = 140
  } else if (_retail >= 3000 && _retail < 4000) {
    rentalPrice = 150
  } else if (_retail >= 4000 && _retail < 5000) {
    rentalPrice = 200
  } else if (_retail >= 5000 && _retail < 6000) {
    rentalPrice = 250
  } else if (_retail >= 6000 && _retail < 7000) {
    rentalPrice = 300
  } else if (_retail >= 7000 && _retail < 8000) {
    rentalPrice = 350
  } else if (_retail >= 8000 && _retail < 9000) {
    rentalPrice = 400
  } else if (_retail >= 9000 && _retail < 10000) {
    rentalPrice = 450
  } else if (_retail >= 10000 && _retail < 10001) {
    rentalPrice = 500
  }

  if (_retail !== 0) {
    replacementPrice = Math.round(_retail * 0.6 * 100) / 100
  }

  return { rentalPrice, replacementPrice, retail: _retail }
}

function calcAdditionalFees(fees: Array<AdditionalFeeModel>): AdditionalFeeModel {
  let insurance_fee = 0
  let rental_fee = 0
  let replacement_fee = 0
  let service_fee = 0
  let cleaning_fee = 0

  fees.forEach(fee => {
    insurance_fee += fee.insurance_fee
    rental_fee += fee.rental_fee
    replacement_fee += fee.replacement_fee
    service_fee += fee.service_fee
    cleaning_fee += fee.cleaning_fee
  })

  return {
    insurance_fee,
    rental_fee,
    replacement_fee,
    service_fee,
    cleaning_fee
  }
}

function getPhotoUrlFromList(photos, fallbackImage) {
  let photo = fallbackImage
  if (Array.isArray(photos) && photos.length) {
    for (let i = 0; i < photos.length; i++) {
      const _photo = photos[i]
      if (Validator.isURL(_photo.url)) {
        photo = _photo.url
        break
      }
    }
  }
  return photo
}

function combineItemsName(items: Array<RentalDetailItem>) {
  let names = []
  items.forEach(item => {
    if (item) {
      if (item.name) {
        names.push(item.name)
      } else {
        names.push(item.itemName)
      }

    }
  })

  const itemName = names.join(', ')
  return itemName
}

function combineItemsSize(sizeIds: Array<number>) {
  let names = []
  sizeIds.forEach(id => {
    const name = getConfigWithValue('sizes', id).title
    names.push(name)
  })

  const itemName = names.join(', ')
  return itemName
}

function getNameFromSizeIds(item: Item) {
  const sizeIds = getSafeSizeIds(item)
  if (sizeIds.length) {
    const name = sizeIds.length > 1 ? 'Multiple Sizes' : getConfigWithValue('sizes', sizeIds[0]).title
    return name
  }

  return ''
}

function getSafeSizeIds(item: Item) {
  if (!item) return []

  return Array.isArray(item.size_ids) && item.size_ids.length ? item.size_ids : [item.size_id]
}

function generateCodeName(length = 6) {
  var characters = '0123456789abcdefghijklmnopqrstuvwxyz'
  var otp = ''

  for (let i = 1; i <= length; i++) {
    var index = Math.floor(Math.random() * characters.length)
    otp = otp + characters[index]
  }

  return otp.toUpperCase()
}
function calculateServiceFees(rentalFee) {
  let serviceFee: number;
  serviceFee = (rentalFee * 3) / 100;
  // # Min service charge should be 1.50 USD if rental cost is < 50 USD
  if (serviceFee < 1.50) serviceFee = 1.50
  console.warn(serviceFee.toFixed(2))
  return serviceFee.toFixed(2)

}

export default {
  setUser,
  generateUUID,
  checkDue,
  getStatusText,
  getDateStringFromDateRange,
  getConfigWithValue,
  getTitleWithValue,
  checkAndUpdateProfile,
  getUserName,
  checkStatus,
  isClosedRental,
  createChatUser,
  createAdminChatUser,
  createChatPayload,
  getChatUserDetails,
  getChatUserPhoto,
  getConfigWithTitle,
  convertSortTypeToConfig,
  convertConfigToSortType,
  convertConfigToTitle,
  getRentalAndReplacementCost,
  getItemName,
  calcAdditionalFees,
  getPhotoUrlFromList,
  combineItemsName,
  getSafeSizeIds,
  combineItemsSize,
  getNameFromSizeIds,
  generateCodeName,
  getAddress,
  checkLocation,
  checkIsInDallas,
  calculateServiceFees
}
