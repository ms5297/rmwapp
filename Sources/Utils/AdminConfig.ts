import AppConfig from './AppConfig'

const adminConfig = {
    adminChatConfig: {
        id: AppConfig.isStaging ? 85 : 5230,
        name: 'RMW ADMIN',
        photo: ''
    }

}

export default adminConfig
