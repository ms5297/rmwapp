import { getStore } from '../Base/Store'
import { Item } from '@Models'

export const ActionTypes = {
  SAVE_ITEM_LIST: 'SAVE_ITEM_LIST',
  APPEND_ITEM_LIST: 'APPEND_ITEM_LIST',
  EDIT_ITEM_LIST: 'EDIT_ITEM_LIST',
  SAVE_ITEMS_LIST: 'SAVE_ITEMS_LIST',
  DELETE_ITEM_LIST: 'DELETE_ITEM_LIST',
  UPDATE_ITEM_LIST_FAVORITE: 'UPDATE_ITEM_LIST_FAVORITE',
  RESET_ITEM_LIST: 'RESET_ITEM_LIST'
}

const store = getStore()

function saveItemList(items: Array<Item>) {
  store.dispatch({
    type: ActionTypes.SAVE_ITEMS_LIST,
    payload: items
  })
}

function appendtemList(items: Array<Item>) {
  store.dispatch({
    type: ActionTypes.APPEND_ITEM_LIST,
    payload: items
  })
}

function deleteItemList() {
  store.dispatch({ type: ActionTypes.DELETE_ITEM_LIST })
}

function saveItem(item: Item) {
  store.dispatch({
    type: ActionTypes.SAVE_ITEM_LIST,
    payload: item
  })
}

function updateItemListFavorite(id: number, isFavorite: boolean) {
  store.dispatch({ type: ActionTypes.UPDATE_ITEM_LIST_FAVORITE, payload: { id, isFavorite } })
}

function editItem(item: Item) {
  store.dispatch({
    type: ActionTypes.EDIT_ITEM_LIST,
    payload: item
  })
}

function resetItemList() {
  store.dispatch({ type: ActionTypes.RESET_ITEM_LIST })
}

export default {
  saveItemList,
  appendtemList,
  deleteItemList,
  saveItem,
  editItem,
  updateItemListFavorite,
  resetItemList
}
