import { ItemList } from '@Models'
import { ActionTypes } from './ItemListActions'

type State = ItemList | []

const initialState: State = []

const reducer = (
  state: State = initialState,
  action: {
    type: keyof typeof ActionTypes
    payload?: any
  }
) => {
  switch (action.type) {
    case 'SAVE_ITEMS_LIST':
      if (Array.isArray(action.payload) && action.payload.length > 0) {
        return action.payload
      }
      return []
    case 'SAVE_ITEM_LIST':
      if (action.payload) {
        return [...state, action.payload]
      }
      return state
    case 'APPEND_ITEM_LIST':
      if (Array.isArray(action.payload) && action.payload.length > 0) {
        const newState = [...state, ...action.payload]
        return newState
      }
      return state
    case 'EDIT_ITEM_LIST':
      if (action.payload && Array.isArray(state) && state.length > 0) {
        //@ts-ignore
        const index = state.indexOf(action.payload)
        if (index !== -1) {
          let cloneState = [...state]
          cloneState[index] = action.payload
          return cloneState
        }
      }
      return state
    case 'UPDATE_ITEM_LIST_FAVORITE':
      if (action.payload) {
        const { id, isFavorite } = action.payload
        const index = state.findIndex(item => item.id === id)
        if (index !== -1) {
          let cloneState = [...state]
          cloneState[index].is_favorite = isFavorite
          return cloneState
        }
      }
      return state
    case 'DELETE_ITEM_LIST':
      return []
    case 'RESET_ITEM_LIST':
      return initialState
    default:
      return state
  }
}

export default { initialState, reducer }
