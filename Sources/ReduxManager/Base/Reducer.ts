import { combineReducers } from 'redux'
import { AsyncStorage } from 'react-native'
import { persistReducer } from 'redux-persist'
import AppConfigurationReducer from '../AppConfiguration/AppConfigurationReducer'
import UserProfileReducer from '../UserProfile/UserProfileReducer'
import UserTokenReducer from '../UserToken/UserTokenReducer'
import FirebaseReducer from '../Firebase/FirebaseReducer'
import UserItemReducer from '../UserItem/UserItemReducer'
import ItemSummaryReducer from '../ItemSummary/ItemSummaryReducer'

import PaymentReducer from '../Payment/PaymentReducer'
import PayoutReducer from '../Payout/PayoutReducer'
import FavoritesReducer from '../Favorites/FavoritesReducer'
import SearchHistoryReducer from '../SearchHistory/SearchHistoryReducer'
import ItemListReducer from '../ItemList/ItemListReducer'
import FilterReducer from '../Filter/FilterReducer'
import NotificationSettingsReducer from '../NotificationSettings/NotificationSettingsReducer'
import PermissionsReducer from '../Permissions/PermissionsReducer'
import BadgeCountReducer from '../BadgeCount/BadgeCountReducer'

export const initialState = {
  appConfiguration: AppConfigurationReducer.initialState,
  notificationSettings: NotificationSettingsReducer.initialState,
  permissions: PermissionsReducer.initialState,
  userProfile: UserProfileReducer.initialState,
  userToken: UserTokenReducer.initialState,
  userItems: UserItemReducer.initialState,
  categoryList: ItemSummaryReducer.initialState,
  itemList: ItemListReducer.initialState,
  payments: PaymentReducer.initialState,
  payouts: PayoutReducer.initialState,
  favorites: FavoritesReducer.initialState,
  searchHistories: SearchHistoryReducer.initialState,
  filters: FilterReducer.initialState,
  firebase: FirebaseReducer.initialState,
  badgeCount: BadgeCountReducer.initialState
}

const reducers = combineReducers<any>({
  appConfiguration: AppConfigurationReducer.reducer,
  notificationSettings: NotificationSettingsReducer.reducer,
  permissions: PermissionsReducer.reducer,
  userProfile: UserProfileReducer.reducer,
  userToken: UserTokenReducer.reducer,
  userItems: UserItemReducer.reducer,
  categoryList: ItemSummaryReducer.reducer,
  itemList: ItemListReducer.reducer,
  payments: PaymentReducer.reducer,
  payouts: PayoutReducer.reducer,
  favorites: FavoritesReducer.reducer,
  searchHistories: SearchHistoryReducer.reducer,
  filters: FilterReducer.reducer,
  firebase: FirebaseReducer.reducer,
  badgeCount: BadgeCountReducer.reducer
})

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['searchHistories', 'appConfiguration']
}

export const reducer = persistReducer(persistConfig, reducers)
