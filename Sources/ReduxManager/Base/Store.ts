import { createStore, applyMiddleware, Store } from 'redux'
import { persistStore, Persistor } from 'redux-persist'
import { middlewares } from './Middleware'
import { initialState, reducer } from './Reducer'
import {
  AppConfiguration,
  UserProfile,
  UserToken,
  UserItems,
  PaymentReduxState,
  SearchHistories,
  ItemList,
  FilterState,
  ItemsSummary,
  NotificationSettings,
  PermissionsState,
  BadgeCount
} from '@Models'

export type StoreState = Partial<{
  appConfiguration: AppConfiguration
  notificationSettings: NotificationSettings
  permissions: PermissionsState
  userProfile: UserProfile
  userToken: UserToken
  userItems: UserItems
  categoryList: ItemsSummary
  payments: PaymentReduxState
  payouts: any
  favorites: any
  searchHistories: SearchHistories
  itemList: ItemList
  filters: FilterState
  badgeCount: BadgeCount
}>

const store = createStore(reducer, applyMiddleware(...middlewares))
const persistor = persistStore(store)

export const configStore = () => {
  return { store, persistor }
}

export function getStore(): Store<StoreState> {
  return store
}

export function getPersistor(): Persistor {
  return persistor
}
