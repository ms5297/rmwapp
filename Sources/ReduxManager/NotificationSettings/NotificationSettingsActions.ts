import { getStore } from '../Base/Store'

const store = getStore()

export const ActionTypes = {
  SAVE_NOTIFICATION_SETTINGS: 'SAVE_NOTIFICATION_SETTINGS',
  RESET_NOTIFICATION_SETTINGS: 'RESET_NOTIFICATION_SETTINGS'
}

function saveNotificationSettings(settings: any) {
  store.dispatch({ type: ActionTypes.SAVE_NOTIFICATION_SETTINGS, payload: settings })
}

function resetNotificationSettings() {
  store.dispatch({ type: ActionTypes.RESET_NOTIFICATION_SETTINGS })
}

export default {
  saveNotificationSettings,
  resetNotificationSettings
}
