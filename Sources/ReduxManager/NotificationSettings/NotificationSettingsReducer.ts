import { ActionTypes } from './NotificationSettingsActions'
import { AppConfiguration } from '@Models'

type State = AppConfiguration

const initialState: State = null

function reducer(
  state: object = initialState,
  action: {
    type: keyof typeof ActionTypes
    payload?: State
  }
) {
  switch (action.type) {
    case 'SAVE_NOTIFICATION_SETTINGS':
      return { ...state, ...action.payload }
    case 'RESET_NOTIFICATION_SETTINGS':
      return initialState
    default:
      return state
  }
}

export default { initialState, reducer }
