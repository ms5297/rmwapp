import { getStore } from '../Base/Store'
import { UserItems, Item } from '@Models'

export const ActionTypes = {
  SAVE_USER_ITEMS: 'SAVE_USER_ITEMS',
  APPEND_USER_ITEMS: 'APPEND_USER_ITEMS',
  DELETE_USER_ITEMS: 'DELETE_USER_ITEMS',
  DELETE_USER_ITEM: 'DELETE_USER_ITEM',
  SAVE_USER_ITEM: 'SAVE_USER_ITEM',
  EDIT_USER_ITEM: 'EDIT_USER_ITEM',
  RESET_USER_ITEM: 'RESET_USER_ITEM'
}

const store = getStore()

function saveUserItems(items: UserItems) {
  store.dispatch({
    type: ActionTypes.SAVE_USER_ITEMS,
    payload: items
  })
}

function appendUserItems(items: UserItems) {
  store.dispatch({
    type: ActionTypes.APPEND_USER_ITEMS,
    payload: items
  })
}

function deleteUserItem(id: string) {
  store.dispatch({ type: ActionTypes.DELETE_USER_ITEM, payload: id })
}

function deleteUserItems() {
  store.dispatch({ type: ActionTypes.DELETE_USER_ITEMS })
}

function saveItem(item: Item) {
  store.dispatch({ type: ActionTypes.SAVE_USER_ITEM, payload: item })
}

function editItem(item: Item) {
  store.dispatch({ type: ActionTypes.EDIT_USER_ITEM, payload: item })
}

function resetUserItem() {
  store.dispatch({ type: ActionTypes.RESET_USER_ITEM })
}
export default {
  saveUserItems,
  deleteUserItems,
  deleteUserItem,
  saveItem,
  editItem,
  appendUserItems,
  resetUserItem
}
