import { UserItems } from '@Models'
import { ActionTypes } from './UserItemActions'

type State = UserItems | []

const initialState: State = []

const reducer = (
  state: State = initialState,
  action: {
    type: keyof typeof ActionTypes
    payload?: any
  }
) => {
  switch (action.type) {
    case 'SAVE_USER_ITEMS':
      return Array.isArray(action.payload) ? action.payload : []
    case 'APPEND_USER_ITEMS':
      if (Array.isArray(action.payload) && action.payload.length > 0) {
        const newState = [...state, ...action.payload]
        return newState
      }
      return state
    case 'DELETE_USER_ITEMS':
      return []
    case 'DELETE_USER_ITEM':
      if (action.payload && Array.isArray(state) && state.length > 0) {
        const index = state.findIndex(item => item.id === action.payload)
        if (index !== -1) {
          let cloneState = [...state]
          cloneState.splice(index, 1)
          return cloneState
        }
      }
      return state
    case 'SAVE_USER_ITEM':
      if (action.payload) {
        return [...state, action.payload]
      }
      return state
    case 'EDIT_USER_ITEM':
      if (action.payload && Array.isArray(state) && state.length > 0) {
        //@ts-ignore
        const index = state.findIndex(item => item.id === action.payload.id)

        if (index !== -1) {
          let cloneState = [...state]
          cloneState[index] = action.payload
          return cloneState
        }
      }
      return state
    case 'RESET_USER_ITEM':
      return initialState
    default:
      return state
  }
}

export default { initialState, reducer }
