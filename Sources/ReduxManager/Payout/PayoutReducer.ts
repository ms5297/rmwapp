import { StripePaymentMethods, StripePaymentInfo, StripePayoutMethods } from '@Models'
import { ActionTypes } from './PayoutActions'

type State = {
  payoutMethods: StripePayoutMethods
}

const initialState: State = {
  payoutMethods: []
}

const reducer = (
  state: State = initialState,
  action: {
    type: keyof typeof ActionTypes
    payload?: any
  }
) => {
  switch (action.type) {
    case 'SAVE_ALL_PAYOUT_METHODS':
      if (Array.isArray(action.payload) && action.payload.length > 0) {
        return { ...state, payoutMethods: action.payload }
      }
      return state
    case 'CHANGE_DEFAULT_PAYOUT_METHODS':
      if (action.payload) {
        const { data: payoutItem, isDefault: defaultStatus } = action.payload
        let payoutList = [...state.payoutMethods]
        for (let index = 0; index < payoutList.length; ++index) {
          payoutList[index].isDefault = false
        }
        const itemIndex = payoutList.indexOf(payoutItem)
        if (itemIndex !== -1) {
          payoutList[itemIndex].isDefault = defaultStatus
        }
        return { ...state, payoutMethods: payoutList }
      }

      return state
    case 'DELETE_PAYOUT_METHODS':
      return { ...state, payoutMethods: [] }
    case 'RESET_PAYOUT_METHODS':
      return initialState
    default:
      return state
  }
}

export default { initialState, reducer }
