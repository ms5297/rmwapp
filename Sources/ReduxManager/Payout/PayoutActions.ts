import { StripePaymentInfo, StripePayoutMethods, StripePayoutMethod } from '@Models'
import { getStore } from '../Base/Store'

const store = getStore()

export const ActionTypes = {
  SAVE_ALL_PAYOUT_METHODS: 'SAVE_ALL_PAYOUT_METHODS',
  CHANGE_DEFAULT_PAYOUT_METHODS: 'CHANGE_DEFAULT_PAYOUT_METHODS',
  DELETE_PAYOUT_METHODS: 'DELETE_PAYOUT_METHODS',
  RESET_PAYOUT_METHODS: 'RESET_PAYOUT_METHODS'
}

function saveAllPayoutMethods(data: StripePayoutMethods) {
  store.dispatch({ type: ActionTypes.SAVE_ALL_PAYOUT_METHODS, payload: data })
}

function changeDefaultPayoutMethods(data: StripePayoutMethod, isDefault: boolean) {
  store.dispatch({ type: ActionTypes.CHANGE_DEFAULT_PAYOUT_METHODS, payload: { data, isDefault } })
}

function deletePayoutMethod() {
  store.dispatch({ type: ActionTypes.DELETE_PAYOUT_METHODS })
}

function resetPayout() {
  store.dispatch({ type: ActionTypes.RESET_PAYOUT_METHODS })
}

export default {
  saveAllPayoutMethods,
  deletePayoutMethod,
  changeDefaultPayoutMethods,
  resetPayout
}
