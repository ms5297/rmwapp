import { getStore } from '../Base/Store'

const store = getStore()

export const ActionTypes = {
  SAVE_APP_VERSION: 'SAVE_APP_VERSION',
  SAVE_CODEPUSH_VERSION: 'SAVE_CODEPUSH_VERSION',
  SAVE_FIREBASE_TOKEN: 'SAVE_FIREBASE_TOKEN',
  DELETE_FIREBASE_TOKEN: 'DELETE_FIREBASE_TOKEN',
  SET_FIRST_START: 'SET_FIRST_START',
  SAVE_FRESHCHAT_ID: 'SAVE_FRESHCHAT_ID',
  SET_FIRST_REGISTER: 'SET_FIRST_REGISTER',
  SET_ADMIN_LOGIN: 'SET_ADMIN_LOGIN'
}

function saveFreshchatId(restoreId: string, externalId: string) {
  store.dispatch({
    type: ActionTypes.SAVE_FRESHCHAT_ID,
    payload: { restoreId, externalId }
  })
}

function saveAppVersion(appVersion: string) {
  store.dispatch({
    type: ActionTypes.SAVE_APP_VERSION,
    payload: appVersion
  })
}

function saveCodePushVersion(codePushVersion: string) {
  store.dispatch({
    type: ActionTypes.SAVE_CODEPUSH_VERSION,
    payload: codePushVersion
  })
}

function saveFirebaseToken(firebaseToken: string) {
  store.dispatch({
    type: ActionTypes.SAVE_FIREBASE_TOKEN,
    payload: firebaseToken
  })
}

function deleteFirebaseToken() {
  store.dispatch({ type: ActionTypes.DELETE_FIREBASE_TOKEN })
}

function setFirstStart(firstStart: boolean) {
  store.dispatch({ type: ActionTypes.SET_FIRST_START, payload: firstStart })
}

function setFirstRegister(firstRegister: boolean) {
  store.dispatch({ type: ActionTypes.SET_FIRST_REGISTER, payload: firstRegister })
}

function setAdminLogin(isAdmin: boolean) {
  store.dispatch({ type: ActionTypes.SET_ADMIN_LOGIN, payload: isAdmin })
}
export default {
  saveCodePushVersion,
  saveAppVersion,
  saveFirebaseToken,
  deleteFirebaseToken,
  setFirstStart,
  saveFreshchatId,
  setFirstRegister,
  setAdminLogin
}
