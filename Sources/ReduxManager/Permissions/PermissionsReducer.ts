import { PermissionsState } from '@Models'
import { ActionTypes } from './PermissionsActions'

type State = PermissionsState

const initialState: State = {
  location: 'undetermined',
  notification: 'undetermined'
}

const reducer = (
  state: State = initialState,
  action: {
    type: keyof typeof ActionTypes
    payload?: any
  }
) => {
  switch (action.type) {
    case 'SAVE_ALL_PERMISSION':
      return { ...state, ...action.payload }
    case 'SAVE_LOCATION_PERMISSION':
      return { ...state, location: action.payload }
    case 'SAVE_NOTIFICATION_PERMISSION':
      return { ...state, notification: action.payload }
    default:
      return state
  }
}

export default { initialState, reducer }
