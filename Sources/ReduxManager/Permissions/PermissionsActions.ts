import { PermissionStatus } from '@Models'
import { getStore } from '../Base/Store'

const store = getStore()

type PermissionStatusExtend = PermissionStatus | string
export const ActionTypes = {
  SAVE_ALL_PERMISSION: 'SAVE_ALL_PERMISSION',
  SAVE_NOTIFICATION_PERMISSION: 'SAVE_NOTIFICATION_PERMISSION',
  SAVE_LOCATION_PERMISSION: 'SAVE_LOCATION_PERMISSION'
}

function saveAllPermissons(notification: PermissionStatusExtend, location: PermissionStatusExtend) {
  store.dispatch({ type: ActionTypes.SAVE_ALL_PERMISSION, payload: { notification, location } })
}

function saveNotificationPermission(status: PermissionStatusExtend) {
  store.dispatch({ type: ActionTypes.SAVE_NOTIFICATION_PERMISSION, payload: { notification: status } })
}

function saveLocationPermission(status: PermissionStatusExtend) {
  store.dispatch({ type: ActionTypes.SAVE_LOCATION_PERMISSION, payload: { location: status } })
}

export default {
  saveAllPermissons,
  saveNotificationPermission,
  saveLocationPermission
}
