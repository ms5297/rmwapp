import { getStore } from '../Base/Store'

export const ActionTypes = {
  RESET_FILTER: 'RESET_FILTER',
  RESET_CATEGORY_FILTER: 'RESET_CATEGORY_FILTER',
  UPDATE_CATEGORY_FILTER: 'UPDATE_CATEGORY_FILTER',
  RESET_ITEM_LIST_FILTER: 'RESET_ITEM_LIST_FILTER',
  UPDATE_ITEM_LIST_FILTER: 'UPDATE_ITEM_LIST_FILTER',
  UPDATE_FILTER: 'UPDATE_FILTER',
  UPDATE_USER_ITEMS_FILTER: 'UPDATE_USER_ITEMS_FILTER',
  RESET_USER_ITEMS_FILTER: 'RESET_USER_ITEMS_FILTER'
}

const store = getStore()

function updateCategoryFilterParam(filterParama) {
  store.dispatch({ type: ActionTypes.UPDATE_CATEGORY_FILTER, payload: filterParama })
}

function resetCategoryFilterParam() {
  store.dispatch({ type: ActionTypes.RESET_CATEGORY_FILTER })
}

function updateItemListFilterParam(filterParama) {
  store.dispatch({ type: ActionTypes.UPDATE_ITEM_LIST_FILTER, payload: filterParama })
}

function resetItemListFilterParam() {
  store.dispatch({ type: ActionTypes.RESET_ITEM_LIST_FILTER })
}

function updateUserItemsFilterParam(filterParama) {
  store.dispatch({ type: ActionTypes.UPDATE_USER_ITEMS_FILTER, payload: filterParama })
}

function resetUserItemsFilterParam() {
  store.dispatch({ type: ActionTypes.RESET_USER_ITEMS_FILTER })
}

function resetFilters() {
  store.dispatch({ type: ActionTypes.RESET_FILTER })
}

function updateFilters(itemListfilterParama, catoloriesFilterParam, userItemsParam = null) {
  store.dispatch({
    type: ActionTypes.UPDATE_FILTER,
    payload: { itemListfilterParama, catoloriesFilterParam, userItemsParam }
  })
}

export default {
  resetFilters,
  updateFilters,
  updateCategoryFilterParam,
  resetCategoryFilterParam,
  updateItemListFilterParam,
  resetItemListFilterParam,
  updateUserItemsFilterParam,
  resetUserItemsFilterParam
}
