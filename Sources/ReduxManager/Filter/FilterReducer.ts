import { ActionTypes } from './FilterActions'
import { FilterState } from '@Models'

type State = FilterState

const _initialState: State = {
  categories: {
    page: 1,
    per_page: 20,
    sort: { field: 'created_at', order: 'desc' }
  },
  itemList: {
    page: 1,
    per_page: 20,
    sort: { field: 'created_at', order: 'desc' },
    filters: {
      location_range: 100,
      min_price: 0,
      max_price: 1000
    }
  },
  userItems: {
    page: 1,
    per_page: 20,
    id: -1
  }
}

const initialState = JSON.parse(JSON.stringify(_initialState))

const reducer = (
  state: State = initialState,
  action: {
    type: keyof typeof ActionTypes
    payload?: any
  }
) => {
  switch (action.type) {
    case 'RESET_FILTER':
      return _initialState
    case 'UPDATE_FILTER':
      return {
        itemList: action.payload.itemListfilterParama || state.itemList,
        categories: action.payload.catoloriesFilterParam || state.categories,
        userItems: action.payload.userItemsParam || state.userItems
      }

    case 'UPDATE_CATEGORY_FILTER':
      return { ...state, categories: action.payload }

    case 'RESET_CATEGORY_FILTER':
      return { ...state, categories: _initialState.categories }

    case 'UPDATE_ITEM_LIST_FILTER':
      if (action.payload.onlyLocation === undefined) {
        return { ...state, itemList: action.payload }
      } else {
        const newState = { ...state };
        newState.itemList.filters.location = action.payload.filters.location
        newState.itemList.filters.locality = action.payload.filters.locality
        return newState
      }


    case 'RESET_ITEM_LIST_FILTER':
      return { ...state, itemList: _initialState.itemList }

    case 'UPDATE_USER_ITEMS_FILTER':
      return { ...state, userItems: action.payload }

    case 'RESET_USER_ITEMS_FILTER':
      return { ...state, userItems: _initialState.userItems }

    default:
      return state
  }
}

export default { initialState, reducer }
