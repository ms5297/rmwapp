import { StripePaymentMethods, StripePaymentInfo } from '@Models'
import { getStore } from '../Base/Store'

const store = getStore()

export const ActionTypes = {
  SAVE_FAVORITES: 'SAVE_FAVORITES',
  DELETE_FAVORITE: 'DELETE_FAVORITE',
  ADD_FAVORITE: 'ADD_FAVORITE',
  RESET_FAVORITES: 'RESET_FAVORITES'
}

function saveFavorites(favorites: any) {
  store.dispatch({ type: ActionTypes.SAVE_FAVORITES, payload: favorites })
}

function resetFavorites() {
  store.dispatch({ type: ActionTypes.RESET_FAVORITES })
}

function deleteFavorite(itemId: number) {
  store.dispatch({ type: ActionTypes.DELETE_FAVORITE, payload: itemId })
}

function addFavorite(favoriteItem) {
  store.dispatch({ type: ActionTypes.ADD_FAVORITE, payload: favoriteItem })
}

export default {
  saveFavorites,
  resetFavorites,
  deleteFavorite,
  addFavorite
}
