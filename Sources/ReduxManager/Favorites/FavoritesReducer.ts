import { ActionTypes } from './FavoritesActions'

type State = Array<any>

const initialState: State = []

const reducer = (
  state: State = initialState,
  action: {
    type: keyof typeof ActionTypes
    payload?: any
  }
) => {
  switch (action.type) {
    case 'SAVE_FAVORITES':
      if (Array.isArray(action.payload) && action.payload.length > 0) {
        return action.payload
      }
      return state
    case 'DELETE_FAVORITE':
      if (Array.isArray(state) && state.length > 0) {
        const index = state.findIndex(item => item.id === action.payload)
        console.log('**** DELETE_FAVORITE', index)

        if (index !== -1) {
          let cloneState = [...state]
          cloneState.splice(index, 1)
          console.log('**** state', state)
          console.log('**** cloneState', cloneState)

          return cloneState
        }
      }
      return state
    case 'ADD_FAVORITE':
      if (Array.isArray(state) && action.payload.favoriteItem) {
        return state.concat(action.payload.favoriteItem)
      }
      return state
    case 'RESET_FAVORITES':
      return initialState
    default:
      return state
  }
}

export default { initialState, reducer }
