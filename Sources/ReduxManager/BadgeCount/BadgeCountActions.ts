import { getStore } from '../Base/Store'
import { BadgeCount } from '@Models'

const store = getStore()

export const ActionTypes = {
  SAVE_BADGE_COUNT: 'SAVE_BADGE_COUNT'
}

function saveBadgeCount(badgeCount: BadgeCount) {
  store.dispatch({ type: ActionTypes.SAVE_BADGE_COUNT, payload: badgeCount })
}


export default {
  saveBadgeCount
}
