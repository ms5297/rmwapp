import { BadgeCount } from '@Models'
import { ActionTypes } from './BadgeCountActions'


type State = BadgeCount

const initialState: State = null

function reducer(
  state: State = initialState,
  action: {
    type: keyof typeof ActionTypes
    payload?: State
  }
) {
  switch (action.type) {
    case 'SAVE_BADGE_COUNT':
      return { ...state, ...action.payload }
    default:
      return state
  }
}

export default { initialState, reducer }
