import { UserProfile } from '@Models'
import { getStore } from '../Base/Store'

const store = getStore()

export const ActionTypes = {
  SAVE_USER_PROFILE: 'SAVE_USER_PROFILE',
  DELETE_USER_PROFILE: 'DELETE_USER_PROFILE',
  RESET_USER_PROFILE: 'RESET_USER_PROFILE',
  SET_USER_IN_WAITLIST: 'SET_USER_IN_WAITLIST'
}

function saveUserProfile(userProfile: UserProfile) {
  store.dispatch({ type: ActionTypes.SAVE_USER_PROFILE, payload: userProfile })
}

function deleteUserProfile() {
  store.dispatch({ type: ActionTypes.DELETE_USER_PROFILE })
}

function resetUserProfile() {
  store.dispatch({ type: ActionTypes.RESET_USER_PROFILE })
}

function setUserInWaitlist() {
  store.dispatch({ type: ActionTypes.SET_USER_IN_WAITLIST })
}

export default {
  saveUserProfile,
  deleteUserProfile,
  resetUserProfile,
  setUserInWaitlist
}
