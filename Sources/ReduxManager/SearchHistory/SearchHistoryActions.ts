import { getStore } from '../Base/Store'

const store = getStore()

export const ActionTypes = {
  ADD_SEARCH_HISTORY: 'ADD_SEARCH_HISTORY',
  RESET_SEARCH_HISTORY: 'RESET_SEARCH_HISTORY'
}

function addSearchHistory(data: string) {
  store.dispatch({ type: ActionTypes.ADD_SEARCH_HISTORY, payload: data })
}

function resetSearchHistory() {
  store.dispatch({ type: ActionTypes.RESET_SEARCH_HISTORY })
}

export default {
  addSearchHistory,
  resetSearchHistory
}
