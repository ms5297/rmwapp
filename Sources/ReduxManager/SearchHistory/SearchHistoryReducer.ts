import { ActionTypes } from './SearchHistoryActions'

type State = {
  keywords: Array<string>
}

const initialState: State = {
  keywords: []
}

const reducer = (
  state: State = initialState,
  action: {
    type: keyof typeof ActionTypes
    payload?: State
  }
) => {
  switch (action.type) {
    case 'ADD_SEARCH_HISTORY':
      if (typeof action.payload === 'string') {
        let searchText = action.payload as string
        searchText = searchText.trim()

        let currentList = state.keywords.slice()
        if (
          !currentList.includes(searchText.toLowerCase()) &&
          !currentList.includes(searchText.toUpperCase()) &&
          !currentList.includes(searchText)
        ) {
          if (currentList.length > 20) {
            currentList.shift()
          }
          currentList.unshift(searchText)
        } else {
          const index = currentList.findIndex(item => item.toLowerCase() === searchText.toLowerCase())
          if (index !== -1) {
            currentList.splice(index, 1)
            currentList.unshift(searchText)
          }
        }
        return { ...state, keywords: currentList }
      }
      return state
    default:
      return state
  }
}

export default { initialState, reducer }
