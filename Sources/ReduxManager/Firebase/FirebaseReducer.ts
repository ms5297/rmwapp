import { FirebaseState } from '@Models'
import { ActionTypes } from './FirebaseActions'

type State = FirebaseState

const initialState: State = {
  chatInboxs: []
}

function reducer(
  state: State = initialState,
  action: {
    type: keyof typeof ActionTypes
    payload: State
  }
) {
  switch (action.type) {
    case 'SAVE_CHAT_INBOXS':
      return { ...state, chatInboxs: action.payload }
    default:
      return state
  }
}

export default { initialState, reducer }
