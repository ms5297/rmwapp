import { ChatModelItem } from '@Models'
import { getStore } from '../Base/Store'

const store = getStore()

export const ActionTypes = {
  SAVE_CHAT_INBOXS: 'SAVE_CHAT_INBOXS'
}

function saveChatInboxs(data: Array<ChatModelItem>) {
  store.dispatch({ type: ActionTypes.SAVE_CHAT_INBOXS, payload: data })
}

export default {
  saveChatInboxs
}
