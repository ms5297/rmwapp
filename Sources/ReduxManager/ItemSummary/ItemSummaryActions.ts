import { getStore } from '../Base/Store'
import { ItemsSummary } from '@Models'

export const ActionTypes = {
  SAVE_ITEM_SUMMARY: 'SAVE_ITEM_SUMMARY',
  DELETE_ITEM_SUMMARY: 'DELETE_ITEM_SUMMARY',
  UPDATE_ITEM_SUMARRY_FAVORITE: 'UPDATE_ITEM_SUMARRY_FAVORITE',
  RESET_ITEM_SUMMARY: 'RESET_ITEM_SUMMARY'
}

const store = getStore()

function saveItemSummary(items: ItemsSummary) {
  store.dispatch({
    type: ActionTypes.SAVE_ITEM_SUMMARY,
    payload: items
  })
}

function deleteItemSummary() {
  store.dispatch({ type: ActionTypes.DELETE_ITEM_SUMMARY })
}

function updateItemSummaryFavorite(id: number, isFavorite: boolean) {
  store.dispatch({ type: ActionTypes.UPDATE_ITEM_SUMARRY_FAVORITE, payload: { id, isFavorite } })
}

function resetItemSummary() {
  store.dispatch({ type: ActionTypes.DELETE_ITEM_SUMMARY })
}

export default {
  saveItemSummary,
  deleteItemSummary,
  updateItemSummaryFavorite,
  resetItemSummary
}
