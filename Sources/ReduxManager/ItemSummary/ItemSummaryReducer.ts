import { ItemsSummary } from '@Models'
import { ActionTypes } from './ItemSummaryActions'

type State = ItemsSummary | []

const initialState: State = []

const reducer = (
  state: State = initialState,
  action: {
    type: keyof typeof ActionTypes
    payload?: any
  }
) => {
  switch (action.type) {
    case 'SAVE_ITEM_SUMMARY':
      if (Array.isArray(action.payload)) {
        return action.payload
      }
      return state
    case 'UPDATE_ITEM_SUMARRY_FAVORITE':
      if (action.payload) {
        const { id, isFavorite } = action.payload
        // let subIndex = -1;

        let itemIndex = []
        state.forEach((item, index) => {
          if (Array.isArray(item.items) && item.items.length > 0) {
            const subIndex = item.items.findIndex(it => it.id === id && it.is_favorite !== isFavorite)
            // subIndex = index;
            // return index !== -1;
            if (subIndex != -1) {
              itemIndex.push({ index, subIndex })
            }
          }
        })
        if (itemIndex.length) {
          let cloneState = [...state]
          itemIndex.forEach((item, idx) => {
            const { index, subIndex } = item
            const oldItem = state[index].items[subIndex]

            if (oldItem) {
              cloneState[index].items[subIndex].is_favorite = isFavorite
            }
          })

          return cloneState
        } else {
          return state
        }
      }

      return state
    case 'DELETE_ITEM_SUMMARY':
      return []
    case 'RESET_ITEM_SUMMARY':
      return initialState
    default:
      return state
  }
}

export default { initialState, reducer }
