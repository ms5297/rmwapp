import { StripePaymentMethods, StripePaymentInfo } from '@Models'
import { getStore } from '../Base/Store'

const store = getStore()

export const ActionTypes = {
  SAVE_ALL_PAYMENT_METHODS: 'SAVE_ALL_PAYMENT_METHODS',
  SAVE_PAYMENT_INFO: 'SAVE_PAYMENT_INFO',
  DELETE_PAYMENT_METHOD: 'DELETE_PAYMENT_METHOD',
  ADD_PAYMENT_METHOD: 'ADD_PAYMENT_METHOD',
  RESET_PAYMENT_METHOD: 'RESET_PAYMENT_METHOD'
}

function saveAllPaymentMethods(data: any) {
  store.dispatch({ type: ActionTypes.SAVE_ALL_PAYMENT_METHODS, payload: data })
}

function savePaymentInfo(data: StripePaymentInfo) {
  store.dispatch({ type: ActionTypes.SAVE_PAYMENT_INFO, payload: data })
}

function deletePaymentMethod(card: any) {
  store.dispatch({ type: ActionTypes.DELETE_PAYMENT_METHOD, payload: card })
}

function addPaymentMethod(card: any) {
  store.dispatch({ type: ActionTypes.ADD_PAYMENT_METHOD, payload: card })
}

function resetPayment() {
  store.dispatch({ type: ActionTypes.RESET_PAYMENT_METHOD })
}

export default {
  saveAllPaymentMethods,
  savePaymentInfo,
  deletePaymentMethod,
  addPaymentMethod,
  resetPayment
}
