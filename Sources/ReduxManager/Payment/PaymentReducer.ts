import { StripePaymentMethods, StripePaymentInfo } from '@Models'
import { ActionTypes } from './PaymentActions'

type State = {
  paymentMethods: StripePaymentMethods | []
  paymentInfo: StripePaymentInfo | null
}

const initialState: State = {
  paymentMethods: [],
  paymentInfo: null
}

const reducer = (
  state: State = initialState,
  action: {
    type: keyof typeof ActionTypes
    payload
  }
) => {
  switch (action.type) {
    case 'SAVE_ALL_PAYMENT_METHODS':
      if (Array.isArray(action.payload)) {
        return { ...state, paymentMethods: action.payload }
      }
      return state
    case 'SAVE_PAYMENT_INFO':
      return { ...state, paymentInfo: action.payload }
    case 'DELETE_PAYMENT_METHOD':
      if (action.payload) {
        let paymentList = state.paymentMethods.slice()
        //@ts-ignore
        const itemIndex = state.paymentMethods.indexOf(action.payload)
        if (itemIndex !== -1) {
          paymentList.splice(itemIndex, 1)
        }

        if (state.paymentInfo && state.paymentInfo.id === action.payload.id) {
          return { ...state, paymentMethods: paymentList, paymentInfo: null }
        } else {
          return { ...state, paymentMethods: paymentList }
        }
      }
      return state
    case 'ADD_PAYMENT_METHOD':
      if (action.payload) {
        let paymentList = state.paymentMethods.slice()
        //@ts-ignore
        paymentList.splice(0, 0, action.payload)
        return { ...state, paymentMethods: paymentList }
      }
      return state
    case 'RESET_PAYMENT_METHOD':
      return initialState
    default:
      return state
  }
}

export default { initialState, reducer }
