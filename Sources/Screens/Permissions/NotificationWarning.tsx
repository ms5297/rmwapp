import React from 'react'
import { StyleSheet, View } from 'react-native'
import { Device } from '@Utils'
import { Permissions as AppPermissions } from '@Services'
import { Header, ScrollView, Text } from 'rn-components'
import { Button } from '@Components'
import { Navigator } from '@Navigation'
import Assets from '@Assets'

class NotificationWarning extends React.Component {
  _onCancel = () => Navigator.pop()

  _onOpenSettings = () => {
    AppPermissions.openSettings(() => {
      AppPermissions.check('notification')
        .then(status => {
          if (status === 'authorized') {
            Navigator.showLoading()
            setTimeout(() => Navigator.hideLoading(() => this._onCancel()), 300)
          }
        })
        .catch(error => {
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
        })
    })
  }

  render() {
    const title = 'Turn on\nnotifications'
    const info =
      'We can let you know when someone messages you or notify you about important account activities like rentals.'
    const buttonTitle = 'Open Settings'
    const message =
      'Don’t miss dollars! Turning your notifications off means you will not know if someone requests a rental!'

    return (
      <View style={StyleSheet.absoluteFill}>
        <Header title="" />
        <ScrollView contentContainerStyle={styles.contentContainer} style={styles.content}>
          <Text style={styles.helpText} text={title} />
          <Text style={styles.infoText} text={info} />
          <Text style={styles.infoText} text={message} />
          <Button
            style={styles.button}
            text={buttonTitle}
            textStyle={{ marginHorizontal: 40 }}
            onPress={this._onOpenSettings}
          />
          <Text
            containerStyle={{ marginTop: 20 }}
            style={styles.skipButtonText}
            text="Cancel"
            onPress={this._onCancel}
          />
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    marginHorizontal: 30,
    marginTop: 20
  },
  contentContainer: {
    flex: 1,
    alignItems: 'center'
  },
  button: {
    marginTop: 73 * Device.vs,
    marginHorizontal: 10
  },
  helpText: {
    marginTop: 25,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    width: '100%'
  },
  infoText: {
    marginTop: 15,
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    width: '100%',
    lineHeight: 22
  },

  skipButton: { marginBottom: 100 },

  skipButtonText: {
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    paddingHorizontal: 20
  }
})

export default NotificationWarning
