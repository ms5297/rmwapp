import React from 'react'
import { StyleSheet, View } from 'react-native'
import { AppConfig, Device } from '@Utils'
import { NavigationProps } from '@Types'
import { Permissions as AppPermissions, GoogleAnalyticsHelper, AppsFlyerHelper } from '@Services'
import { Header, ScrollView, Text } from 'rn-components'
import { Button } from '@Components'
import { Navigator } from '@Navigation'
import { PermissionsState, PermissionsParam } from '@Models'
import { StoreState, PermissionsActions } from '@ReduxManager'
import { connect } from 'react-redux'
import Assets from '@Assets'

type Props = {
  navigation: NavigationProps
  permissions: PermissionsState
}

class LocationPermissions extends React.Component<Props> {
  componentWillMount() {
    AppPermissions.checkLocationAndNotification(['notification', 'location'])
      .then(status => {
        const { notification, location } = status
        PermissionsActions.saveAllPermissons(notification, location)
      })
      .catch(error => {
        console.warn('error: ', error)
      })
  }

  _loadNextScreen = () => {
    const param: PermissionsParam = this.props.navigation.getParam('param')

    Navigator.pop()
    param.onDidClosed()
  }

  _onDone = () => {
    const param: PermissionsParam = this.props.navigation.getParam('param')
    const { permissions } = this.props

    if (param.permissionType === 'location') {
      if (permissions.location === 'denied' || permissions.location === 'restricted') {
        AppPermissions.openSettings(() => {
          AppPermissions.checkLocationAndNotification(['notification', 'location'])
            .then(status => {
              const { notification, location } = status
              PermissionsActions.saveAllPermissons(notification, location)
              if (location === 'authorized') {
                AppsFlyerHelper.trackEvent(AppsFlyerHelper.EVENTS.GEO_LOCATION_SHARED, 0, 0)
                GoogleAnalyticsHelper.trackEvent(GoogleAnalyticsHelper.EVENT_CATEGORY.MAIN, GoogleAnalyticsHelper.EVENT.GEO_LOCATION_SHARED)
                Navigator.showLoading()
                setTimeout(() => Navigator.hideLoading(() => this._loadNextScreen()), 300)
              }
            })
            .catch(error => {
              Navigator.hideLoading()
              Navigator.showToast('Error', error.message, 'Error')
            })
        })
      } else if (permissions.location === 'authorized') {
        AppsFlyerHelper.trackEvent(AppsFlyerHelper.EVENTS.GEO_LOCATION_SHARED, 0, 0)
        GoogleAnalyticsHelper.trackEvent(GoogleAnalyticsHelper.EVENT_CATEGORY.MAIN, GoogleAnalyticsHelper.EVENT.GEO_LOCATION_SHARED)
        this._loadNextScreen()
      } else {
        AppPermissions.request('location').then(response => {
          this._loadNextScreen()
        })
      }
    }
  }

  render() {
    let title = ''
    let info = ''
    let buttonTitle = ''
    const { permissions } = this.props

    if (permissions.location === 'denied' || permissions.location === 'restricted') {
      title = 'Location Services\ndisabled'
      buttonTitle = 'Open Settings'
      info = `${
        AppConfig.name
        } needs access to your location. Please turn on Location Services in your device settings.`
    } else {
      title = 'We need your\nlocation'
      buttonTitle = 'Yes, enable locations'
      info = 'We need your location to find your local market to show near by rentals and improve our service.'
    }

    return (
      <View style={StyleSheet.absoluteFill}>
        <Header title="" />
        <ScrollView contentContainerStyle={styles.contentContainer} style={styles.content}>
          <Text style={styles.helpText} text={title} />
          <Text style={styles.infoText} text={info} />
          <Button
            style={styles.button}
            text={buttonTitle}
            textStyle={{ marginHorizontal: 40 }}
            onPress={this._onDone}
          />
          <Text
            containerStyle={{ marginTop: 20 }}
            style={styles.skipButtonText}
            text="Skip"
            onPress={this._loadNextScreen}
          />
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    marginHorizontal: 30,
    marginTop: 20
  },
  contentContainer: {
    flex: 1,
    alignItems: 'center'
  },
  button: {
    marginTop: 73 * Device.vs,
    marginHorizontal: 10
  },
  helpText: {
    marginTop: 25,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    width: '100%'
  },
  infoText: {
    marginTop: 15,
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    width: '100%',
    lineHeight: 22
  },

  skipButton: { marginBottom: 100 },

  skipButtonText: {
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    paddingHorizontal: 20
  }
})

const mapStateToProps = (state: StoreState) => {
  return {
    permissions: state.permissions
  }
}
export default connect(mapStateToProps)(LocationPermissions)
