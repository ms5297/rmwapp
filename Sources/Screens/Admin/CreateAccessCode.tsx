import React from 'react'
import { View, StyleSheet, TouchableOpacity, Clipboard, Animated } from 'react-native'
import { Header, TextInput, Text, Row } from 'rn-components'
import { Button, BackButton, Section } from '@Components'
import Styles from '@Styles'
import Assets from '@Assets'
import { Device, DateTime } from '@Utils'
import DateTimePicker from 'react-native-modal-datetime-picker'
import { Navigator } from '@Navigation'
import { Api } from '@Services'

class CreateAccessCode extends React.Component {
  state = {
    isDateTimePickerVisible: false,
    opacityAnimated: new Animated.Value(0),
    endTime: DateTime.moment()
      .add(1, 'days')
      .format('MMM DD, YYYY'),
    success: false,
    accessCodeName: '',
    usedTimes: '',
    accessCodeCopy: ''
  }

  _inputRef = React.createRef<TextInput>()

  onCopy = () => {
    const { opacityAnimated, success } = this.state

    const code = this._inputRef.current.getText()
    Clipboard.setString(code)
    Animated.sequence([
      Animated.timing(opacityAnimated, {
        toValue: 1,
        duration: 250,
        useNativeDriver: true
      }),
      Animated.timing(opacityAnimated, {
        toValue: 1,
        duration: 2000,
        useNativeDriver: true
      }),
      Animated.timing(opacityAnimated, {
        toValue: 0,
        duration: 250,
        useNativeDriver: true
      })
    ]).start()
  }

  onCreateCode = () => {
    const { accessCodeName, endTime, usedTimes } = this.state
    if (accessCodeName.trim() === '') {
      Navigator.showToast('Error', 'Please enter an valid access code name.', 'Error')
      return
    }

    if (endTime.trim() === '') {
      Navigator.showToast('Error', 'Please select a valid time.', 'Error')
      return
    }

    if (usedTimes.trim() === '') {
      Navigator.showToast('Error', 'Please enter a valid used times.', 'Error')
      return
    }

    const endTimeFormated = DateTime.moment(endTime, 'MMM DD, YYYY').format('YYYY-MM-DD')
    const usedTimesFormated = parseInt(usedTimes)
    Navigator.showLoading()

    Api.createAccessCode(accessCodeName, endTimeFormated, usedTimesFormated)
      .then((response: any) => {
        const { code, message } = response
        if (code !== 200) throw Error(message)
        Navigator.hideLoading()
        Navigator.showToast('Success', 'The access code has been created successfully.', 'Success')
        this.setState({ success: true, accessCodeCopy: accessCodeName })
      })
      .catch(error => {
        this.setState({ success: false })
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message || 'Internal Error', 'Error')
      })
  }

  render() {
    const {
      opacityAnimated,
      endTime,
      isDateTimePickerVisible,
      success,
      accessCodeName,
      usedTimes,
      accessCodeCopy
    } = this.state
    const initialDate = DateTime.moment()
      .add(1, 'days')
      .toDate()
    const maxDate = DateTime.moment()
      .add(3, 'months')
      .toDate()

    return (
      <View style={StyleSheet.absoluteFill}>
        <Header title="Create Access Code" titleStyle={Styles.headerTitle} LeftComponent={<BackButton />} />
        <View style={{ flex: 1, marginHorizontal: 30, paddingTop: 40 }}>
          <TextInput
            ref={this._inputRef}
            autoCapitalize="characters"
            placeholder="Access code name"
            maxLength={7}
            value={accessCodeName}
            style={styles.inputContainer}
            inputStyle={styles.input}
            selectionColor={Assets.colors.mainText}
            placeholderTextColor={Assets.colors.placeholder}
            underlineWidth={0}
            onChangeText={text => this.setState({ accessCodeName: text.toUpperCase() })}
          />

          <TextInput
            ref={this._inputRef}
            defaultValue=""
            value={usedTimes}
            autoCapitalize="words"
            placeholder="How many times used ?"
            keyboardType="number-pad"
            style={styles.inputContainer}
            inputStyle={styles.input}
            maxLength={4}
            selectionColor={Assets.colors.mainText}
            placeholderTextColor={Assets.colors.placeholder}
            underlineWidth={0}
            onChangeText={text => this.setState({ usedTimes: text })}
          />

          <Section
            style={{ marginTop: 20 }}
            title="End time"
            value={endTime}
            valueStyle={{ color: Assets.colors.appTheme }}
            iconStyle={{ tintColor: Assets.colors.borderColor }}
            iconSource={Assets.images.calendar}
            onPress={() => this.setState({ isDateTimePickerVisible: true })}
          />

          <View style={{ height: 80, justifyContent: 'center', alignItems: 'center' }}>
            {success && (
              <View style={{ alignItems: 'center' }}>
                <Text text="Long press to copy" style={styles.holdText} />
                <View style={styles.accessCodeNameContainer}>
                  <Text selectable style={styles.accessCodeName} text={accessCodeCopy} />
                </View>
              </View>
            )}
          </View>
          <Button style={styles.button} textStyle={styles.buttonText} text="Create Code" onPress={this.onCreateCode} />
        </View>
        <DateTimePicker
          titleIOS="Select your birthday"
          date={initialDate}
          isVisible={isDateTimePickerVisible}
          onConfirm={date => {
            const endTime = DateTime.moment(date).format('MMM DD, YYYY')
            this.setState({ isDateTimePickerVisible: false, endTime })
          }}
          onCancel={() => this.setState({ isDateTimePickerVisible: false })}
          maximumDate={maxDate}
          minimumDate={initialDate}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    marginTop: 20,
    marginBottom: 10,
    backgroundColor: Assets.colors.inputBg,
    borderColor: 'transparent',
    borderRadius: 6
  },
  input: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    height: 44,
    marginLeft: 10,
    textAlignVertical: 'center',
    paddingTop: 4
  },
  button: {
    marginTop: 60 * Device.vs,

    alignSelf: 'center'
  },
  buttonText: {
    marginHorizontal: 40
  },
  copyContainer: {
    backgroundColor: '#CECDD2',
    borderRadius: 4,
    marginHorizontal: 8
  },
  copyText: {
    fontSize: 16,
    fontFamily: Assets.fonts.display.bold,
    color: '#43505D',
    marginHorizontal: 6,
    marginVertical: 4
  },
  copyToastContainer: {
    backgroundColor: 'black',
    borderRadius: 4,
    position: 'absolute',
    right: 0,
    top: -26
  },
  copyToastText: {
    fontSize: 16,
    fontFamily: Assets.fonts.display.bold,
    color: 'white',
    marginHorizontal: 6,
    marginVertical: 4
  },

  accessCodeNameContainer: {
    marginTop: 10,
    backgroundColor: Assets.colors.inputBg,
    borderRadius: 4
  },
  accessCodeName: {
    backgroundColor: Assets.colors.inputBg,
    fontSize: 16,
    fontFamily: Assets.fonts.display.bold,
    color: Assets.colors.mainText,
    marginHorizontal: 25,
    marginVertical: 8
  },
  holdText: {
    fontSize: 16,
    fontFamily: Assets.fonts.display.light,
    color: Assets.colors.textLight
  }
})

export default CreateAccessCode
