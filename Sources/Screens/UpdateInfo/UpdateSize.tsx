import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Button, Section } from '@Components'
import { Header, Text, TextInput, ScrollView } from 'rn-components'
import { UpdateSizeParam, SelectionListParam, EnumConfigKey, FirebaseValueObject, UpdateLocationParam, UpdateNotificationParam } from '@Models'
import { NavigationProps } from '@Types'
import { Navigator } from '@Navigation'
import { Device, AppConfig, Colors } from '@Utils'
import Styles from '@Styles'
import Assets from '@Assets'
import Picker from 'react-native-picker'
import { getStore } from '@ReduxManager'
import { Api } from '@Services'

const vs = Device.height / 812

const isIPhoneX = Device.isIphoneX()

type Props = {
  navigation: NavigationProps
}

type State = UpdateSizeParam & {
  height: string
  braSizes: string
  weight: string
  sizes: FirebaseValueObject | null
  backupSizes: FirebaseValueObject | null
  shoeSizes: FirebaseValueObject | null
}
class UpdateSize extends React.Component<Props, State> {
  _inputWeight = React.createRef<TextInput>()

  constructor(props: Props) {
    super(props)
    const param: UpdateSizeParam = this.props.navigation.getParam('param')
    this.state = {
      height: '',
      braSizes: '',
      weight: '',
      sizes: null,
      backupSizes: null,
      shoeSizes: null,
      ...param
    }
  }

  _pickerConfig = {
    pickerFontFamily: Assets.fonts.display.regular,
    pickerToolBarFontSize: 17,
    pickerFontSize: 17,
    pickerCancelBtnColor: Colors.hexToRgb('#ffffff'),
    pickerConfirmBtnColor: Colors.hexToRgb('#ffffff'),
    pickerTitleColor: Colors.hexToRgb('#ffffff'),
    pickerConfirmBtnText: 'Done',
    pickerCancelBtnText: 'Cancel',
    pickerToolBarBg: Colors.hexToRgb(Assets.colors.appTheme)
  }

  _onContinue = () => {
    try {
      const {
        braSizes: _braSize,
        sizes: _typicalDressSize,
        backupSizes: _backupDressSize,
        height: _height,
        shoeSizes: _shoeSize,
        weight,
        birthday
      } = this.state
      const braSize = _braSize.length > 0 ? _braSize.toString() : ''
      const typicalDressSize = _typicalDressSize ? _typicalDressSize.title : ''
      const shoeSize = _shoeSize ? _shoeSize.title : ''
      const height = _height.length > 0 ? _height.toString() : ''

      if (height === '') {
        // Navigator.showToast('Error', 'Please select Height', 'Error')
        // return
      }

      if (weight === '') {
        // Navigator.showToast('Error', 'Please enter your Weight', 'Error')
        // return
      }

      if (braSize === '') {
        // Navigator.showToast('Error', 'Please select Bra size', 'Error')
        // return
      }

      if (typicalDressSize === '') {
        // Navigator.showToast('Error', 'Please select Typical size', 'Error')
        // return
      }

      if (shoeSize === '') {
        // Navigator.showToast('Error', 'Please select Shoe size', 'Error')
        // return
      }

      // const param: UpdateLocationParam = {
      //   backup_size_id: _backupDressSize ? _backupDressSize.value : null,
      //   dress_size_id: _typicalDressSize ? _typicalDressSize.value : null,
      //   shoe_size_id: _shoeSize ? _shoeSize.value : null,
      //   bra_size: braSize,
      //   height: height,
      //   weight: weight,
      //   birthday: birthday
      // }
      const param: UpdateNotificationParam = {
        ...this.state,
        backup_size_id: _backupDressSize ? _backupDressSize.value : null,
        dress_size_id: _typicalDressSize ? _typicalDressSize.value : null,
        shoe_size_id: _shoeSize ? _shoeSize.value : null,
        bra_size: braSize,
        height: height,
        weight: weight,
        birthday: birthday
      }

      const { userProfile } = getStore().getState()
      const { ...other } = param
      const { isInDallas } = this.state

      let newUserProfile = {
        ...userProfile,
        ...other
      }

      if (isInDallas && !newUserProfile.locality) {
        newUserProfile.locality = 'Dallas, TX'
      }

      Api.editUserProfile(newUserProfile)

      // Navigator.navTo('UpdateLocation', { param })
      Navigator.navTo('TurnOnNotifications', { param })
    } catch (error) {
      Navigator.showToast('Error', error.message, 'Error')
    }
  }

  _showHeightPicker = () => {
    let selected = []
    if (this.state.height.length > 0) {
      selected = this.state.height.split(' ')
    }

    Picker.init({
      ...this._pickerConfig,
      pickerData: AppConfig.enumConfig.heights,
      pickerTitleText: 'Select Height',
      selectedValue: selected,
      onPickerConfirm: data => { },
      onPickerCancel: data => { },

      onPickerSelect: data => {
        const height = data.join(' ')
        this.setState({ height })
      }
    })
    Picker.show()
  }

  _showBraSizePicker = () => {
    let selected = []
    if (this.state.braSizes.length > 0) {
      selected = this.state.braSizes.split(' ')
    }

    Picker.init({
      ...this._pickerConfig,
      selectedValue: selected,
      pickerTitleText: 'Select Bra Size',
      pickerData: AppConfig.enumConfig.braSizes,
      onPickerConfirm: data => { },
      onPickerCancel: data => { },
      onPickerSelect: data => {
        const braSizes = data.join(' ')
        this.setState({ braSizes })
      }
    })
    Picker.show()
  }

  _renderSelectionView = (title: string, value: string, type: EnumConfigKey) => {
    let rightIcon = Assets.images.categoryDetailsArrow
    let titleExtensionStyle = null
    let valueExtensionStyle = null
    if (type == 'date') {
      rightIcon = Assets.images.calendar
      if (value !== '') {
        titleExtensionStyle = { color: Assets.colors.appTheme }
        title = value
        value = ''
      }
    } else {
      if (value !== 'All' && value !== 'Choose a location') {
        valueExtensionStyle = { color: Assets.colors.appTheme }
      }
    }

    const onSelected = () => {
      const mutiple = false
      if (type === 'braSizes') {
        this._showBraSizePicker()
      } else if (type === 'heights') {
        this._showHeightPicker()
      } else {
        const param: SelectionListParam = {
          onItemsSelected: items => {
            // @ts-ignore
            this.setState({ [type]: mutiple ? items : items[0] })
          },
          mutiple,
          title: title,
          selectedItems: mutiple ? this.state[type] : [this.state[type]],
          type,
          removedItemTitles: ['All', 'One Size Fits All', 'Other']
        }
        Navigator.navTo('SelectionList', { param })
      }
    }

    return (
      <Section
        title={title}
        titleStyle={titleExtensionStyle}
        valueStyle={valueExtensionStyle}
        value={value}
        iconSource={rightIcon}
        onPress={onSelected}
      />
    )
  }

  render() {
    const { height, braSizes, shoeSizes, sizes, backupSizes } = this.state

    return (
      <View style={StyleSheet.absoluteFill}>
        <Header title="2 of 4" titleStyle={Styles.headerTitle} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{ marginHorizontal: 30 }}>
            <Text style={styles.helpText} text="Create your size profile" />
            <Text
              style={styles.infoText}
              text="This information is optional but will curate better search results for you."
            />
            {this._renderSelectionView('Height', height.length ? height : '', 'heights')}
            <TextInput
              ref={this._inputWeight}
              onChangeText={text => this.setState({ weight: text })}
              style={styles.inputWeightContainer}
              inputStyle={{ paddingLeft: 14, height: 44, marginTop: 2 }}
              placeholder="Weight"
              keyboardType="numeric"
              underlineWidth={0}
              RightComponent={<Text style={styles.lbs} text="lbs" />}
            />
            {this._renderSelectionView('Bra size', braSizes.length ? braSizes : '', 'braSizes')}
            {this._renderSelectionView('Typical Size', sizes ? sizes.title : '', 'sizes')}
            {this._renderSelectionView('Backup Size (optional)', backupSizes ? backupSizes.title : '', 'backupSizes')}
            {this._renderSelectionView('Shoe Size', shoeSizes ? shoeSizes.title : '', 'shoeSizes')}

            <Button onPress={this._onContinue} style={styles.button} textStyle={styles.buttonText} text="Continue" />
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  input: {
    height: 44,
    width: '100%',
    marginBottom: 20 * vs,
    borderColor: Assets.colors.borderColor,
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 6,
    paddingLeft: 12
  },
  lbs: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.codeColor,
    marginLeft: 10
  },
  inputWeightContainer: {
    height: 44,

    width: '100%',
    marginBottom: 20 * vs,
    borderColor: Assets.colors.borderColor,
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 6,
    paddingRight: 12
  },

  inputButton: {
    height: 44,
    width: '100%',
    marginBottom: 20 * vs,
    borderColor: Assets.colors.borderColor,
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 6,
    paddingHorizontal: 12,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row'
  },
  text: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText
  },
  helpText: {
    marginTop: 15 * vs,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold
  },
  infoText: {
    marginTop: 28 * vs,
    marginBottom: 47 * vs,
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    lineHeight: 22
  },
  button: {
    marginHorizontal: 10,
    marginTop: isIPhoneX ? 60 * vs : 40 * vs,
    marginBottom: 40 * vs
  },
  buttonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: 'white'
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  value: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.appTheme,
    letterSpacing: -0.2,
    marginRight: 10,
    textAlign: 'right'
  }
})

export default UpdateSize
