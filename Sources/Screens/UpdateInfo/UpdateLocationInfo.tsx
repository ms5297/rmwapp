import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Button } from '@Components'
import { NavigationProps } from '@Types'
import { Navigator } from '@Navigation'
import { Api } from '@Services'
import { Text } from 'rn-components'
import { UpdateNotificationParam, UserProfile } from '@Models'
import { getStore, UserProfileActions, ItemSummaryActions, ItemListActions } from '@ReduxManager'
import { Device, AppConfig } from '@Utils'
import Assets from '@Assets'

type Props = {
  navigation: NavigationProps
}

type State = UpdateNotificationParam
class UpdateLocationInfo extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    const param: UpdateNotificationParam = this.props.navigation.getParam('param')
    this.state = param
  }

  _onContinue = () => {
    const { itemList, categories } = getStore().getState().filters
    let filterItemListParams = itemList
    let filterCatoloriesParams = categories
    try {
      Navigator.showLoading()
      const { userProfile } = getStore().getState()
      const newUserProfile = {
        ...userProfile,
        ...this.state
      }

      Api.editUserProfile(newUserProfile)
        .then((response: any) => {
          if (response.user == null) throw Error(response.message || '(UD - 39) Internal Error')
          UserProfileActions.saveUserProfile(response.user)

          const { dress_size_id, backup_size_id, longitude, latitude, locality } = response.user as UserProfile
          if (dress_size_id !== null) {
            filterItemListParams.filters['size'] = [dress_size_id]
            // if (typeof filterCatoloriesParams.filters === "object") {
            //   filterCatoloriesParams.filters["size"] = [dress_size_id];
            // } else {
            //   filterCatoloriesParams.filters = {
            //     size: [dress_size_id]
            //   };
            // }
          }

          if (backup_size_id !== null) {
            if (Array.isArray(filterItemListParams.filters['size'])) {
              filterItemListParams.filters['size'].push(backup_size_id)
            } else {
              filterItemListParams.filters['size'] = [backup_size_id]
            }

            // if (typeof filterCatoloriesParams.filters === "object") {
            //   if (Array.isArray(filterCatoloriesParams.filters["size"])) {
            //     filterCatoloriesParams.filters["size"].push(backup_size_id);
            //   } else {
            //     filterCatoloriesParams.filters["size"] = [backup_size_id];
            //   }
            // }
          }

          if (typeof longitude === 'number' && typeof latitude === 'number') {
            filterItemListParams.filters['location'] = { longitude, latitude }
            filterItemListParams.filters['locality'] = locality
            if (typeof filterCatoloriesParams.filters === 'object') {
              filterCatoloriesParams.filters['location'] = { longitude, latitude }
            } else {
              filterCatoloriesParams.filters = {
                location: { longitude, latitude }
              }
            }
          }

          if (!filterItemListParams.filters.location) {
            filterItemListParams.filters.location = { longitude: AppConfig.longitude, latitude: AppConfig.latitude }
          }

          return Promise.all([Api.getItemSummary(filterCatoloriesParams), Api.getItemList(filterItemListParams)])
        })
        .then((values: any) => {
          const itemsSummary =
            values[0] && values[0].code === 200 && Array.isArray(values[0].categories) ? values[0].categories : []
          const itemList = values[1] && values[1].code === 200 && Array.isArray(values[1].items) ? values[1].items : []

          ItemSummaryActions.saveItemSummary(itemsSummary)
          ItemListActions.saveItemList(itemList)

          filterItemListParams.page = itemList.length < 20 ? 1 : filterItemListParams.page + 1
          delete filterItemListParams.filters.size
          delete filterCatoloriesParams.filters.size

          return
        })
        .then(() => {
          Navigator.hideLoading()
          Navigator.navTo('Home')
          Navigator.navTo('Closet')
        })
        .catch(error => {
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
          return
        })
    } catch (error) {
      Navigator.hideLoading()
      Navigator.showToast('Error', error.message, 'Error')
    }
  }

  render() {
    const { locality } = this.state
    const title = `We are coming to ${locality} soon!`
    const message = `Want to cash in your closet sooner? Become a ${locality} brand advocate and get early access to inventory listings.`
    return (
      <View style={StyleSheet.absoluteFill}>
        <View style={{ flex: 1 }}>
          <View style={{ marginHorizontal: 30, flex: 1, marginTop: 54 }}>
            <Text style={styles.helpText} text={title} />
            <Text style={[styles.infoText, { marginTop: 24 }]} text={message} />
            <View style={{ flex: 1 }} />
            <Button
              style={styles.button}
              textStyle={styles.buttonText}
              text="Add Your Closet"
              onPress={this._onContinue}
            />
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  helpText: {
    marginTop: 25,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold
  },
  infoText: {
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    lineHeight: 22
  },
  button: {
    height: 48,
    marginHorizontal: 10,
    borderRadius: 6,
    marginBottom: Device.isIphoneX() ? 80 * Device.vs : 60 * Device.vs,
    backgroundColor: Assets.colors.buttonColor
  },
  buttonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: 'white'
  },
  location: {
    marginTop: 20,
    marginRight: 4,
    fontSize: 16,
    lineHeight: 22,
    fontFamily: Assets.fonts.display.bold,
    color: 'black'
  }
})

export default UpdateLocationInfo
