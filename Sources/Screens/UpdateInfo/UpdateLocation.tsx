import React from 'react'
import { View, StyleSheet } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { Section, Button } from '@Components'
import { Permissions, LocationHelper, Api } from '@Services'
import { Header, Text } from 'rn-components'
import { UpdateLocationParam, PlacePickerParam, UpdateNotificationParam, ComingSoonParam, UpdateSizeParam } from '@Models'
import { Navigator } from '@Navigation'
import { NavigationProps } from '@Types'
import { Device, AppConfig, Utils } from '@Utils'
import Styles from '@Styles'
import Assets from '@Assets'
import Axios from 'axios'
import { PermissionsActions, getStore, UserProfileActions, ItemSummaryActions, ItemListActions } from '@ReduxManager'
import { UpdateSize } from '@Screens'

type Props = {
  navigation: NavigationProps
}

type State = UpdateLocationParam & {
  longitude: number | null
  latitude: number | null
  locality: string | null
  isInDallas: boolean
}

class UpdateLocation extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    const param = this.props.navigation.getParam('param')
    this.state = {
      longitude: null,
      latitude: null,
      locality: null,
      isInDallas: false,
      ...param
    }
  }

  _onPress = () => {
    const param: PlacePickerParam = {
      title: 'Choose your location',
      onLocationSelected: (location, locality) => {
        console.log('***** onLocationSelected', location, locality)
        Navigator.showLoading()
        Utils.checkIsInDallas(location.latitude, location.longitude)
          .then(isInDallas => {
            Navigator.hideLoading()
            this.setState({ ...location, locality, isInDallas })
          })
          .catch(error => {
            Navigator.hideLoading()
            this.setState({ ...location, locality, isInDallas: false })
          })
      }
    }
    Navigator.navTo('PlacePicker', { param })
  }

  componentDidMount() {
    Permissions.request('location').then(status => {
      if (status === 'authorized') {
        PermissionsActions.saveLocationPermission('authorized')
        this._getCurrentLocation()
      }
    })
  }

  _getCurrentLocation = () => {
    Navigator.showLoading()
    LocationHelper.startLocationUpdate()
      .then(location => {
        const { latitude, longitude } = location
        Axios.get('https://maps.googleapis.com/maps/api/geocode/json', {
          params: {
            sensor: false,
            latlng: latitude + ',' + longitude,
            key: 'AIzaSyAggWpOMeJ0Hkz1IMNi--UbivONTjwuN34',
            language: 'en'
          }
        }).then(response => {
          let address = {
            city: '',
            state: ''
          }
          // @ts-ignore
          response.results[0].address_components.forEach(component => {
            if (component.types.includes('locality') || component.types.includes('sublocality')) {
              address.city = component.short_name
            }
            if (component.types.includes('neighborhood')) {
              address.city = component.short_name
            }
            if (component.types.includes('administrative_area_level_1')) {
              address.state = component.short_name
            }
            if (component.types.includes('administrative_area_level_2')) {
              address.state = component.short_name
            }
          })
          let addressString = ''
          if (address.city && address.city.length && address.state && address.state.length) {
            addressString = address.city + ', ' + address.state
          } else if (address.city && address.city.length) {
            addressString = address.city
          } else if (address.state && address.state.length) {
            addressString = address.state
          }

          Utils.checkIsInDallas(latitude, longitude)
            .then(isInDallas => {
              Navigator.hideLoading()
              this.setState({ latitude, longitude, locality: addressString, isInDallas })
            })
            .catch(error => {
              Navigator.hideLoading()
              this.setState({ latitude, longitude, locality: addressString, isInDallas: false })
            })
        })
      })
      .catch(error => {
        Navigator.hideLoading()
        console.log('error: ', error)
      })
  }

  _onCheckMyLocation = () => {
    Navigator.showLoading()

    const { userProfile } = getStore().getState()
    const newUserProfile = {
      ...userProfile,
      ...this.state
    }

    Api.editUserProfile(newUserProfile)
      .then((response: any) => {
        if (response.user == null) throw Error(response.message || '(UL - 71) Internal Error')
        UserProfileActions.saveUserProfile(response.user)
        Navigator.hideLoading()
        Navigator.reset('Start')
      })

      .catch(error => {
        console.log('**** error', error)
        Navigator.hideLoading()
        Navigator.reset('Start')
      })
  }

  _onSkip = () => {
    try {
      const { locality, isInDallas } = this.state
      // const notificationParam: UpdateNotificationParam = this.state
      const sizeParam: UpdateSizeParam = this.state
      Navigator.showLoading()
      if (locality) {
        const update = () => {
          Navigator.hideLoading()
          // Navigator.navTo('TurnOnNotifications', { param: notificationParam })

          try {
            AsyncStorage.setItem('USER_LONGITUDE', this.state.longitude.toString())
            AsyncStorage.setItem('USER_LATITUDE', this.state.latitude.toString())
            AsyncStorage.setItem('USER_LOCALITY', this.state.locality)
          } catch (e) {
            // saving error
          }

          Navigator.navTo('UpdateSize', { param: sizeParam })
        }
        update()
      } else {
        Navigator.hideLoading()
        Navigator.showToast('Error', 'Please select your location', 'Error')
      }
    } catch (error) {
      Navigator.hideLoading()
      Navigator.showToast('Error', error.message, 'Error')
    }
  }

  _onContinue = () => {
    try {
      const { locality, isInDallas } = this.state
      // const notificationParam: UpdateNotificationParam = this.state
      const sizeParam: UpdateSizeParam = this.state
      console.log('**** notificationParam', sizeParam, locality)
      Navigator.showLoading()
      if (locality) {
        const update = () => {
          Navigator.hideLoading()
          // if (isInDallas) {
          if (isInDallas || !isInDallas) {
            // Navigator.navTo('TurnOnNotifications', { param: notificationParam })
            try {
              AsyncStorage.setItem('USER_LONGITUDE', this.state.longitude.toString())
              AsyncStorage.setItem('USER_LATITUDE', this.state.latitude.toString())
              AsyncStorage.setItem('USER_LOCALITY', this.state.locality)
            } catch (e) {
              // saving error
            }

            Navigator.navTo('UpdateSize', { param: sizeParam })
          } else {
            const comingSoonParam1: ComingSoonParam = {
              title: 'Coming Soon!',
              message: `We aren't in your city yet, but we're working on it. Be the first to know as soon as we're open in your city.`,
              buttonText: 'Join the Waitlist',
              isHandleJoinWaitlist: true,
              onPressed: () => {
                const comingSoonParam2: ComingSoonParam = {
                  title: 'Your on the List',
                  message: `We will send an email when we are available in your city.`,
                  buttonText: 'Check My Location',
                  onPressed: this._onCheckMyLocation,
                  // onCodeAddedSuccess: () => Navigator.navTo('TurnOnNotifications', { param: notificationParam })
                  onCodeAddedSuccess: () => Navigator.navTo('UpdateSize', { param: sizeParam })
                }
                Navigator.push('ComingSoon', { param: comingSoonParam2 })
              },
              // onCodeAddedSuccess: () => Navigator.navTo('TurnOnNotifications', { param: notificationParam })
              onCodeAddedSuccess: () => Navigator.navTo('UpdateSize', { param: sizeParam })
            }
            Navigator.push('ComingSoon', { param: comingSoonParam1 })
          }
        }

        Permissions.check('location').then(status => {
          if (status === 'authorized') {
            update()
          } else if (status === 'denied' || status === 'restricted') {
            Permissions.openSettings(() => {
              Permissions.check('location')
                .then(status => {
                  if (status !== 'authorized') {
                    Navigator.hideLoading()
                    let error: any = Error('User denied location permission')
                    error.code = 404
                    throw error
                  }
                  update()
                })
                .catch(error => {
                  throw error
                })
            })
          } else {
            Permissions.request('location')
              .then(status => {
                if (status !== 'authorized') {
                  let error: any = Error('User denied location permission')
                  error.code = 404
                }

                this._getCurrentLocation()
                update()
              })
              .catch(error => {
                Navigator.hideLoading()
                throw error
              })
          }
        })
      } else {
        Navigator.hideLoading()
        Navigator.showToast('Error', 'Please select your location', 'Error')
      }
    } catch (error) {
      Navigator.hideLoading()
      Navigator.showToast('Error', error.message, 'Error')
    }
  }

  render() {
    const { locality = '' } = this.state
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header title="3 of 4" titleStyle={Styles.headerTitle} />
        <View style={{ marginHorizontal: 30 }}>
          <Text style={styles.helpText} text="Where are you located?" />
          <Text
            style={styles.infoText}
            text="We need your location to find your local market to show near by rentals and improve our service."
          />
          <Section
            style={{ marginTop: 44 }}
            title="Location"
            value={locality}
            valueStyle={{ color: Assets.colors.appTheme }}
            iconStyle={{ tintColor: Assets.colors.borderColor }}
            iconSource={Assets.images.categoryDetailsArrow}
            onPress={this._onPress}
          />
          <Button
            onPress={this._onContinue}
            style={styles.button}
            textStyle={styles.buttonText}
            text="Yes, enable locations"
          />
          <Text
            containerStyle={{ marginTop: 20 }}
            style={styles.skipButtonText}
            text="Continue without enabling locations"
            onPress={this._onSkip}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  helpText: {
    marginTop: 22 * Device.vs,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold
  },
  infoText: {
    marginTop: 28,
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    lineHeight: 22
  },
  button: {
    marginHorizontal: 10,
    marginTop: 99 * Device.vs
  },
  buttonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: 'white'
  },
  skipButtonText: {
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    textAlign: 'center'
  }
})

export default UpdateLocation
