import React from 'react'
import { View } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import { Button, Section } from '@Components'
import { Header, Text, StyleSheet } from 'rn-components'
import { DateTime, Device } from '@Utils'
import { NavigationProps } from '@Types'
import { Navigator } from '@Navigation'
import { UpdateSizeParam, UpdateLocationParam } from '@Models'
import Assets from '@Assets'
import DateTimePicker from 'react-native-modal-datetime-picker'

type Props = {
  navigation: NavigationProps
}

type State = {
  isDateTimePickerVisible: boolean
  birthday: string | null
}
class UpdateBirthday extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      birthday: null,
      isDateTimePickerVisible: false
    }
  }

  _onContinue = () => {
    try {
      const { birthday } = this.state
      if (birthday) {
        const age = DateTime.moment().diff(birthday, 'years')
        // const param: UpdateSizeParam = { birthday: DateTime.format(birthday) }
        const param: UpdateLocationParam = { birthday: DateTime.format(birthday) }
        if (age < 13) {
          Navigator.showAlert(
            'You must be over 13 years old',
            'Our platform is designed for users age 13 and up and requires parental consent for anyone under 18. Your parents must approve of you using this site to continue.',
            // () => Navigator.navTo('UpdateSize', { param }),
            () => Navigator.navTo('UpdateLocation', { param }),
            null,
            'Continue'
          )
        } else {
          // Navigator.navTo('UpdateSize', { param })

          try {
            AsyncStorage.setItem('USER_BIRTHDAY', DateTime.moment(birthday).format('MMM-DD-YYYY'))
          } catch (e) {
            // saving error
          }

          Navigator.navTo('UpdateLocation', { param })
        }
      } else {
        Navigator.showToast('Error', 'Please select your birthday', 'Error')
      }
    } catch (error) {
      Navigator.showToast('Error', error.message, 'Error')
    }
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true })

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false })

  _handleDatePicked = date => {
    this.setState({ birthday: date }, this._hideDateTimePicker)
  }

  render() {
    const { birthday, isDateTimePickerVisible } = this.state

    let birthdayString = ''
    if (birthday) {
      birthdayString = DateTime.moment(birthday).format('MMM-DD-YYYY')
    }
    const maxDate = DateTime.moment()
      .subtract(13, 'years')
      .toDate()
    const current = this.state.birthday ? DateTime.moment(this.state.birthday).toDate() : maxDate

    return (
      <View style={StyleSheet.absoluteFill}>
        <Header title="1 of 4" />
        <View style={{ marginHorizontal: 30, flex: 1 }}>
          <Text style={styles.helpText} text="When’s your birthday?" />
          <Text
            style={styles.infoText}
            text="Your birthday is private, we just want to celebrate your special day with you!"
          />
          <Section
            style={{ marginTop: 40 }}
            title="Birthday"
            value={birthdayString}
            valueStyle={{ color: Assets.colors.appTheme }}
            iconStyle={{ tintColor: Assets.colors.borderColor }}
            iconSource={Assets.images.calendar}
            onPress={this._showDateTimePicker}
          />
          <Text
            style={styles.infoText}
            text="You must be at least 13 years old to use the app. If you are under 18 you need your parents permission to use the app."
          />
          <View style={{ flex: 1 }} />
          <Button onPress={this._onContinue} style={styles.button} textStyle={styles.buttonText} text="Continue" />
        </View>
        <DateTimePicker
          titleIOS="Select your birthday"
          date={current}
          isVisible={isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
          maximumDate={maxDate}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  helpText: {
    marginTop: 25,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold
  },
  infoText: {
    marginTop: 25,
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    lineHeight: 22
  },
  button: {
    marginHorizontal: 10,
    marginBottom: Device.isIphoneX() ? 80 * Device.vs : 60 * Device.vs
  },
  buttonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: 'white'
  }
})

export default UpdateBirthday
