import React from 'react'
import { View, StyleSheet } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import { Api, Permissions } from '@Services'
import { Button } from '@Components'
import { Header, Text } from 'rn-components'
import { NavigationProps } from '@Types'
import { Navigator } from '@Navigation'
import { getStore, UserProfileActions, ItemSummaryActions, ItemListActions, PermissionsActions } from '@ReduxManager'
import { Device, AppConfig } from '@Utils'
import { UpdateNotificationParam, UserProfile } from '@Models'
import Assets from '@Assets'

type Props = {
  navigation: NavigationProps
}
type State = UpdateNotificationParam & {
  address: string | null
  location: object | null
}

class TurnOnNotifications extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    const param: UpdateNotificationParam = this.props.navigation.getParam('param')
    this.state = {
      location: null,
      address: null,
      ...param
    }
    console.log('**** state', this.state)
  }

  componentDidMount() {
    this.getAsyncStorageData()
  }

  getAsyncStorageData = async () => {
    try {
      const storedUserBirthday = await AsyncStorage.getItem('USER_BIRTHDAY')
      const storedUserLongitude = await AsyncStorage.getItem('USER_LONGITUDE')
      const storedUserLatitude = await AsyncStorage.getItem('USER_LATITUDE')
      const storedUserLocality = await AsyncStorage.getItem('USER_LOCALITY')

      if (storedUserBirthday !== null) {
        this.setState({ birthday: storedUserBirthday })
      }
      if (storedUserLongitude !== null && storedUserLatitude !== null) {
        this.setState({ location: { longitude: parseFloat(storedUserLongitude), latitude: parseFloat(storedUserLatitude) }, locality: storedUserLocality })
      }

    } catch (e) {
      // error reading value
    }
  }

  _onSkip = () => {
    Navigator.showAlert(
      'Are you sure?',
      'If you do not have notifications turned on you may miss out on rental opportunities and that means missing $$$ earning opportunities.',
      () => null,
      this._onEnableNotification,
      'Yes',
      'No'
    )
  }

  _onEnableNotification = () => {
    Navigator.showLoading()
    const { itemList, categories } = getStore().getState().filters
    let filterItemListParams = itemList
    let filterCatoloriesParams = categories

    Permissions.check('notification')
      .then(status => {
        if (status == 'denied') {
          Permissions.openSettings(() => {
            return Permissions.check('notification')
          })
        } else if (status == 'authorized') {
          return 'authorized'
        } else {
          return Permissions.request('notification')
        }
      })
      .then(status => {
        const { userProfile } = getStore().getState()
        const { isInDallas, ...other } = this.state

        let newUserProfile = {
          ...userProfile,
          ...other
        }

        if (isInDallas && !newUserProfile.locality) {
          newUserProfile.locality = 'Dallas, TX'
        }

        PermissionsActions.saveNotificationPermission(status)
        return Api.editUserProfile(newUserProfile)
      })
      .then((response: any) => {
        if (response.user == null) throw Error(response.message || '(TN - 71) Internal Error')
        UserProfileActions.saveUserProfile(response.user)

        const { dress_size_id, backup_size_id, longitude, latitude, locality } = response.user as UserProfile
        if (dress_size_id !== null) {
          filterItemListParams.filters['size'] = [dress_size_id]
          // if (typeof filterCatoloriesParams.filters === "object") {
          //   filterCatoloriesParams.filters["size"] = [dress_size_id];
          // } else {
          //   filterCatoloriesParams.filters = {
          //     size: [dress_size_id]
          //   };
          // }
        }

        if (backup_size_id !== null) {
          if (Array.isArray(filterItemListParams.filters['size'])) {
            filterItemListParams.filters['size'].push(backup_size_id)
          } else {
            filterItemListParams.filters['size'] = [backup_size_id]
          }

          // if (typeof filterCatoloriesParams.filters === "object") {
          //   if (Array.isArray(filterCatoloriesParams.filters["size"])) {
          //     filterCatoloriesParams.filters["size"].push(backup_size_id);
          //   } else {
          //     filterCatoloriesParams.filters["size"] = [backup_size_id];
          //   }
          // }
        }

        if (typeof longitude === 'number' && typeof latitude === 'number') {
          filterItemListParams.filters['location'] = { longitude, latitude }
          filterItemListParams.filters['locality'] = locality
          if (typeof filterCatoloriesParams.filters === 'object') {
            filterCatoloriesParams.filters['location'] = { longitude, latitude }
          } else {
            filterCatoloriesParams.filters = {
              location: { longitude, latitude }
            }
          }
        }

        if (!filterItemListParams.filters.location) {
          filterItemListParams.filters.location = { longitude: AppConfig.longitude, latitude: AppConfig.latitude }
        }

        return Promise.all([Api.getItemSummary(filterCatoloriesParams), Api.getItemList(filterItemListParams)])
      })
      .then((values: any) => {
        const itemsSummary =
          values[0] && values[0].code === 200 && Array.isArray(values[0].categories) ? values[0].categories : []
        const itemList = values[1] && values[1].code === 200 && Array.isArray(values[1].items) ? values[1].items : []

        ItemSummaryActions.saveItemSummary(itemsSummary)
        ItemListActions.saveItemList(itemList)
        filterItemListParams.page = itemList.length < 20 ? 1 : filterItemListParams.page + 1
        delete filterItemListParams.filters.size
        delete filterCatoloriesParams.filters.size

        return
      })
      .then(() => {
        Navigator.hideLoading()
        Navigator.navTo('Home')
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header title="4 of 4" />
        <View style={{ marginHorizontal: 30, flex: 1 }}>
          <Text style={styles.helpText} text="Turn on notifications" />
          <Text
            style={styles.infoText}
            text="We can let you know when someone messages you or notify you about important account activities like rentals."
          />

          <Button
            onPress={this._onEnableNotification}
            style={{ marginHorizontal: 10, marginTop: 61 }}
            textStyle={styles.buttonText}
            text="Yes, notify me"
          />
          <View style={{ flex: 1 }} />

          <Button
            onPress={this._onSkip}
            style={styles.button}
            textStyle={[styles.buttonText, { color: Assets.colors.appTheme }]}
            text="SKIP"
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  helpText: {
    marginTop: 22,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold
  },
  infoText: {
    marginTop: 28,
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    lineHeight: 22
  },
  buttonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: 'white'
  },
  button: {
    backgroundColor: 'transparent',
    marginBottom: Device.isIphoneX() ? 80 * Device.vs : 60 * Device.vs
  }
})

export default TurnOnNotifications
