import React from 'react'
import { Navigator } from '@Navigation'
import { Authentication } from '@Services'
import { ImageBackground, StatusBar, Platform } from 'react-native'
import { getStore, AppConfigurationActions } from '@ReduxManager'

import Assets from '@Assets'
import RNSplashScreen from 'react-native-splash-screen'

const isAndroid = Platform.OS === 'android'
const timeout = 500

class Start extends React.Component {
  timeoutHandler = null

  componentDidMount() {
    isAndroid && StatusBar.setHidden(true, 'none')
    this.start()
  }

  componentWillUnmount() {
    this.timeoutHandler && clearTimeout(this.timeoutHandler)
  }

  private start = () => {
    StatusBar.setBarStyle('light-content')
    Authentication.createSession()
      .then(isOK => {
        if (isOK) {
          RNSplashScreen.hide()
          const { userProfile } = getStore().getState()
          const isAdmin = userProfile.role === 'admin'
          AppConfigurationActions.setAdminLogin(isAdmin)

          console.log('****** isAdmin', isAdmin)

          if (isAdmin) {
            this.timeoutHandler = setTimeout(() => {
              Navigator.navTo('Admin')
              isAndroid && StatusBar.setHidden(false, 'slide')
            }, timeout)
          } else {
            this.timeoutHandler = setTimeout(() => {
              Navigator.navTo('Home')
              isAndroid && StatusBar.setHidden(false, 'slide')
            }, timeout)
          }
        } else {
          throw 'User not logged'
        }
      })
      .catch(error => {
        RNSplashScreen.hide()
        Authentication.logout()
        this.timeoutHandler = setTimeout(() => {
          Navigator.navTo('Authentication')
          isAndroid && StatusBar.setHidden(false, 'slide')
        }, timeout)
      })
  }

  render() {
    return (
      <ImageBackground
        style={{ width: '100%', height: '100%' }}
        resizeMode="cover"
        source={Assets.images.splashScreen}
      />
    )
  }
}

export default Start
