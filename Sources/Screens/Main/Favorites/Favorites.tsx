import React from 'react'
import { View, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { Header } from 'rn-components'
import { Navigator } from '@Navigation'
import { StoreState, FavoritesActions } from '@ReduxManager'
import { Api } from '@Services'
import { UserProfile } from '@Models'
import { LayoutAnimations } from '@Utils'
import Styles from '@Styles'
import CategoryGrid from '../Home/Category/CategoryGrid'

type Props = {
  favorites: any
  userProfile: UserProfile
}

type State = {
  pageIndex: number
}

class Favorites extends React.Component<Props, State> {
  _onRefresh = () => {
    return new Promise<boolean>((resolve, reject) => {
      Api.getFavorites()
        .then((response: any) => {
          const favorites = response && response.code === 200 ? response.favorites : []
          LayoutAnimations.setLayoutAnimation(LayoutAnimations.ListItem)
          FavoritesActions.saveFavorites(favorites)
          resolve(true)
        })
        .catch(error => {
          reject(error)
          console.warn('**** error', error)
        })
    })
  }

  render() {
    console.log('favorites', this.props.favorites)

    return (
      <View style={StyleSheet.absoluteFill}>
        <Header style={{ marginBottom: 20 }} title="Favorites" titleStyle={Styles.headerTitle} />
        <CategoryGrid
          style={{ marginBottom: 40 }}
          data={this.props.favorites}
          onRefresh={this._onRefresh}
          emptyViewProps={{
            title: 'No favorites yet',
            info: 'When you find a garment you love, make it a favorite and save it for later.',
            buttonText: 'Start Searching',
            onPress: () => Navigator.navTo('Home')
          }}
          itemCellProps={{ editable: false }}
        />
      </View>
    )
  }
}

const mapStateToProps = (state: StoreState) => {
  return {
    favorites: state.favorites || [],
    userProfile: state.userProfile
  }
}

export default connect(mapStateToProps)(Favorites)
