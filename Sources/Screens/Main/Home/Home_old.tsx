import React from 'react'
import { View, Image, Keyboard, StyleSheet, AppState, Platform } from 'react-native'
import { connect } from 'react-redux'
import { BranchHelper, FirebaseManager, Api, Permissions, AppsFlyerHelper, GoogleAnalyticsHelper } from '@Services'
import { KeyboardSpacer, Text, Icon, Row, Col, TextInput, StatusBar } from 'rn-components'
import {
  StoreState,
  ItemSummaryActions,
  SearchHistoryActions,
  ItemListActions,
  getStore,
  PermissionsActions,
  FilterActions
} from '@ReduxManager'
import {
  UserProfile,
  SearchHistories,
  FilterState,
  ItemList,
  ItemsSummary,
  ForceUpdateInfoParam,
  PermissionsParam,
  FilterParamsModel,
  EnumConfigKey,
  PlacePickerParam,
  Location
} from '@Models'
import { Navigator } from '@Navigation'
import { Device, LayoutAnimations, Utils, AppConfig } from '@Utils'
import { Button, Section, SectionLocation } from '@Components'
import { NavigationProps } from '@Types'
import Assets from '@Assets'
import CategorySectionList from './Category/CategorySectionList'
import SearchHistory from './SearchHistory'
import SearchResult from './SearchResult'

type Props = {
  categoryList: ItemsSummary
  itemList: ItemList
  userProfile: UserProfile
  searchHistories: SearchHistories
  filter: FilterState
  navigation: NavigationProps
}

type State = {
  searchType: 'None' | 'Searching' | 'Done'
  itemListPageIndex: number
  keyword: string
  appState: string,
  currentLocation: Location | null
  currentAddress: string | null
  refreshing: boolean
}
class Home extends React.Component<Props, State> {
  _unsubscribeFromBranch: any = null
  _searchInputRef = React.createRef<TextInput>()

  state: State = {
    searchType: 'None',
    itemListPageIndex: 1,
    keyword: '',
    appState: AppState.currentState,
    currentLocation: null,
    currentAddress: ''
  }


  componentWillMount() {
    Utils.setUser(this.props.userProfile)
    this._unsubscribeFromBranch = BranchHelper.subscribe()
    // AppsFlyerHelper.init();
  }

  componentWillUnmount() {
    this._unsubscribeFromBranch && this._unsubscribeFromBranch()
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      if (Platform.OS === 'ios') {
        AppsFlyerHelper.trackAppLaunch();

        GoogleAnalyticsHelper.trackEvent(GoogleAnalyticsHelper.EVENT_CATEGORY.MAIN, GoogleAnalyticsHelper.EVENT.APP_LAUNCH)
      }
    }
    this.setState({ appState: nextAppState });
  };

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);

    const { userProfile } = this.props
    this.setState({ currentLocation: { latitude: userProfile.latitude, longitude: userProfile.longitude }, currentAddress: userProfile.locality })

    const { isNeedUpdate, missingFields } = Utils.checkAndUpdateProfile(userProfile)
    if (isNeedUpdate) {
      const param: ForceUpdateInfoParam = {
        updateKeys: missingFields,
        onUpdateDone: userProfile => {
          console.log('****userProfile ', userProfile)
          this.setState({ currentLocation: { latitude: userProfile.latitude, longitude: userProfile.longitude }, currentAddress: userProfile.locality })

          if (userProfile.accessibility === false) {
            Navigator.navTo('ComingSoon', { onGoBack: this._onRefreshCategory })
          }
        }
      }
      Navigator.navTo('ForceUpdateInfo', { param })
    } else {
      if (userProfile.accessibility === false && userProfile.role != 'admin') {
        Navigator.navTo('ComingSoon', { onGoBack: this._onRefreshCategory })
        return
      }
      Permissions.checkLocationAndNotification(['notification', 'location'])
        .then(status => {
          const { notification, location } = status

          if (location !== 'authorized') {
            const param: PermissionsParam = {
              permissionType: 'location'
            }

            Navigator.navTo('Permissions', { param })
          } else if (notification !== 'authorized') {
            const param: PermissionsParam = {
              permissionType: 'notification'
            }

            Navigator.navTo('Permissions', { param })
          }
          console.log('**** saveAllPermissons', notification, location)
          PermissionsActions.saveAllPermissons(notification, location)
        })
        .catch(error => {
          console.warn('error: ', error)
        })
    }

    if (userProfile.should_add_bank_account) {
      Navigator.navTo('ForceUpdateBankAccount')
    }
  }

  _getFilterParams(keyword: string, reset: boolean) {
    const { userProfile } = this.props
    let filterParam: FilterParamsModel = {
      page: reset ? 1 : this.state.itemListPageIndex,
      per_page: 20,
      sort: {
        field: 'created_at',
        order: 'desc'
      },
      filters: {
        min_price: AppConfig.filters.priceRange[0],
        max_price: AppConfig.filters.priceRange[1],
        location_range: AppConfig.filters.locationRange[0],
        keyword
      }
    }

    if (typeof userProfile.longitude === 'number' && typeof userProfile.latitude === 'number') {
      filterParam.filters.location = {
        latitude: userProfile.latitude,
        longitude: userProfile.longitude
      }
    }

    // vinayak on 07 nov 2019
    filterParam.filters.locality = userProfile.locality;
    // end of changes by vinayak on 07 nov 2019
    filterParam.filters.location = this.state.currentLocation;
    filterParam.filters.locality = this.state.currentAddress;


    return filterParam
  }
  _searchForItems = (text: string, reset: boolean = false) => {
    Navigator.showLoading()
    this.setState({ refreshing: true })
    const filterParam = this._getFilterParams(text, reset)
    filterParam.page = 1
    Api.getItemList<any>(filterParam)
      .then(response => {
        const itemList = response && response.code === 200 && Array.isArray(response.items) ? response.items : []
        if (reset && itemList.length) {
          filterParam.page = 2
        }
        ItemListActions.saveItemList(itemList)
        text.trim().length && SearchHistoryActions.addSearchHistory(text)

        Navigator.hideLoading()
        this.setState({ refreshing: false })
        LayoutAnimations.setLayoutAnimation(LayoutAnimations.PresetSpring)
        this.setState({ searchType: 'Done', keyword: text, itemListPageIndex: filterParam.page })
      })
      .catch(error => {
        Navigator.hideLoading()
        this.setState({ refreshing: false })
        Navigator.showToast('Error', error.message, 'Error')
        LayoutAnimations.setLayoutAnimation(LayoutAnimations.PresetSpring)
        this.setState({ searchType: 'Done', keyword: text })
      })
  }

  _searchForItemsFromTextInput = () => {
    const text = this._searchInputRef.current.getText().trim()
    this._searchForItems(text, true)
    this._searchInputRef.current.clearText()
  }

  _onTapFilterButton = () => {
    const keyword = this._searchInputRef.current.getText()
    Navigator.navTo('Filters', { shouldSetDefault: true, keyword, onGoBack: this._onRefreshCategory })
  }

  _onTapSearchCancelButton = () => {
    Keyboard.dismiss()
    this.setState({ searchType: 'None', keyword: '' })
  }

  _onSearchHistoryFocused = () => {
    LayoutAnimations.setLayoutAnimation(LayoutAnimations.Spring)
    this.setState({ searchType: 'Searching' })
  }

  _renderHeader = () => {
    const { searchType } = this.state
    const marginTop = Device.isIphoneX() ? 34 : 20
    return (
      <View style={[styles.headerView, { marginTop }]}>
        <StatusBar barStyle="dark-content" backgroundColor={Assets.colors.statusBar} />

        <Row alignVertical="center" style={{ marginLeft: 13 }}>
          <TextInput
            ref={this._searchInputRef}
            LeftComponent={
              <Image
                style={{ marginHorizontal: 8, tintColor: Assets.colors.textLight }}
                source={Assets.images.searchIcon}
              />
            }
            placeholder="Search"
            style={styles.inputView}
            inputStyle={styles.inputText}
            underlineWidth={0}
            placeholderTextColor={Assets.colors.mainText}
            onFocus={this._onSearchHistoryFocused}
            returnKeyType="search"
            onSubmitEditing={this._searchForItemsFromTextInput}
          />
          {searchType !== 'None' ? (
            <Text style={styles.cancelText} text="CANCEL" onPress={this._onTapSearchCancelButton} />
          ) : (
              <Icon
                iconStyle={{ tintColor: Assets.colors.textLight }}
                iconSource={Assets.images.filterIcon}
                onPress={this._onTapFilterButton}
              />
            )}

        </Row>

      </View>

    )
  }

  _setGlobalFilter(filters: FilterState, currentLocation: Location, currentAddress: string) {
    let categoryFilterToUpdate = { ...filters.categories }
    categoryFilterToUpdate['filters'] = { 'location': currentLocation }

    let filtersToUpdate = { ...filters.itemList.filters }
    filtersToUpdate['location'] = currentLocation
    filtersToUpdate['locality'] = currentAddress

    let itemListToUpdate = { ...filters.itemList }
    itemListToUpdate['filters'] = filtersToUpdate

    FilterActions.updateCategoryFilterParam(categoryFilterToUpdate)
    FilterActions.updateItemListFilterParam(itemListToUpdate)

    this._onRefreshCategory();
  }


  // vinayak on 07 nov 2019
  _renderSelectionView = (title: string, value: string, type: EnumConfigKey) => {
    let rightIcon = Assets.images.categoryDetailsArrow
    let titleExtensionStyle = null
    let valueExtensionStyle = null

    let { filters } = getStore().getState()
    if (filters.itemList.filters.locality) {
      value = filters.itemList.filters.locality
    } else if (this.state.currentAddress) {
      value = this.state.currentAddress
      this._setGlobalFilter(filters, this.state.currentLocation, this.state.currentAddress)
    }


    if (type == 'date') {
      rightIcon = Assets.images.calendar
      if (value !== '') {
        titleExtensionStyle = { color: Assets.colors.appTheme }
        title = value
        value = ''
      }
    } else {
      if (value !== 'All' && value !== 'Choose a location') {
        valueExtensionStyle = { color: Assets.colors.appTheme }
      }
    }

    const onSelected = () => {
      if (type === 'market') {
        const param: PlacePickerParam = {
          title,
          onLocationSelected: (currentLocation, currentAddress) => {
            //let { filters } = getStore().getState()
            /* let categoryFilterToUpdate = { ...filters.categories }
            categoryFilterToUpdate['filters'] = { 'location': currentLocation }

            let filtersToUpdate = { ...filters.itemList.filters }
            filtersToUpdate['location'] = currentLocation
            filtersToUpdate['locality'] = currentAddress

            let itemListToUpdate = { ...filters.itemList }
            itemListToUpdate['filters'] = filtersToUpdate

            // FilterActions.updateCategoryFilterParam(filters.categories)
            // FilterActions.updateItemListFilterParam(filters.itemList)
            FilterActions.updateCategoryFilterParam(categoryFilterToUpdate)
            FilterActions.updateItemListFilterParam(itemListToUpdate)
            
            this._onRefreshCategory(); */
            this._setGlobalFilter(filters, currentLocation, currentAddress)
            this.setState({ currentLocation, currentAddress })
          }
        }
        Navigator.navTo('PlacePicker', { param })
      }
    }

    return (
      <SectionLocation
        style={{ marginBottom: 0, borderRadius: 6, marginHorizontal: 13, height: 28 }}
        title={title}
        titleStyle={titleExtensionStyle}
        valueStyle={valueExtensionStyle}
        value={value}
        iconSource={rightIcon}
        onPress={onSelected}
      />
    )
  }

  // vinayak on 07 nov 2019

  /* **************************** Category Section List **************************** */
  _onRefreshCategory = () => {
    Navigator.showLoading()
    this.setState({ refreshing: true })
    return new Promise<boolean>((resolve, reject) => {
      let { filters, userProfile } = getStore().getState()
      const { latitude, longitude } = userProfile
      if (!filters.categories.filters) {
        filters.categories['filters'] = { 'location': { latitude, longitude } }
      }

      Api.getItemSummary(filters.categories)
        .then((response: any) => {
          const itemsSummary = response && response.code === 200 ? response.categories : []

          ItemSummaryActions.saveItemSummary(itemsSummary)

          resolve(true)
          Navigator.hideLoading()
          this.setState({ refreshing: false })
        })
        .catch((error: any) => {
          Navigator.hideLoading()
          this.setState({ refreshing: false })
          reject(error)
        })
    })
  }

  _renderCategoryList = () => {
    const { categoryList, navigation } = this.props

    return (
      <CategorySectionList
        data={categoryList}
        onRefresh={this._onRefreshCategory}
        onTapFilterButton={this._onTapFilterButton}
        refreshing={this.state.refreshing}
      />
    )
  }

  /* **************************** END Category Section List **************************** */

  /* **************************** START Search Results **************************** */
  _onRefreshSearchResult = () => {
    return new Promise<boolean>((resolve, reject) => {
      const filterParam = this._getFilterParams(this.state.keyword, true)
      filterParam.page = 1
      Api.getItemList(filterParam)
        .then((response: any) => {
          const itemList = response && response.code === 200 && Array.isArray(response.items) ? response.items : []
          if (itemList.length) {
            filterParam.page = 2
          }
          ItemListActions.saveItemList(itemList)
          this.setState({ itemListPageIndex: filterParam.page })
          resolve(false)
        })
        .catch(error => {
          resolve(false)
        })
    })
  }

  _onLoadMoreSearchResult = () => {
    return new Promise<boolean>((resolve, reject) => {
      const filterParam = this._getFilterParams(this.state.keyword, false)
      Api.getItemList(filterParam)
        .then((response: any) => {
          const itemList = response && response.code === 200 && Array.isArray(response.items) ? response.items : []
          if (itemList.length) {
            this.setState({ itemListPageIndex: this.state.itemListPageIndex + 1 })
          }
          ItemListActions.appendtemList(itemList)
          resolve(true)
        })
        .catch(error => {
          resolve(true)
        })
    })
  }

  _renderSearchResult() {
    const { itemList } = this.props
    return (
      <SearchResult
        style={{ marginBottom: 60 }}
        data={itemList}
        onRefresh={this._onRefreshSearchResult}
        onLoadMore={this._onLoadMoreSearchResult}
        emptyViewProps={{
          title: 'Oh, No items available',
          info:
            'There are currently no items available using that search. Please change your search filters or location and try again.',
          buttonText: 'Adjust the Search',
          onPress: this._onTapFilterButton
        }}
      />
    )
    // return (
    //   <CategoryGrid
    //     style={{ marginBottom: 60 }}
    //     data={itemList}
    //     onRefresh={this._onRefreshSearchResult}
    //     onLoadMore={this._onLoadMoreSearchResult}
    //     emptyViewProps={{
    //       title: "Oh, No items available",
    //       info:
    //         "There are currently no items available using that search. Please change your search filters or location and try again.",
    //       buttonText: "Adjust the Search",
    //       onPress: this._onTapFilterButton
    //     }}
    //   />
    // );
  }

  /* **************************** Search History **************************** */
  _onTapSearchHistoryItem = (item: string) => {
    Keyboard.dismiss()
    this._searchForItems(item, true)
  }

  _onSearch = () => {
    Keyboard.dismiss()
    this._searchForItemsFromTextInput()
  }

  _renderSearchHistory = () => {
    const { searchHistories } = this.props
    return <SearchHistory searchHistories={searchHistories} onTapSearchHistoryItem={this._onTapSearchHistoryItem} />
  }

  render() {
    const { searchType } = this.state

    console.log('searchType', searchType, this.props.categoryList)

    return (
      <View style={StyleSheet.absoluteFill}>

        {this._renderHeader()}
        {searchType == 'None' && this._renderSelectionView('Location', this.state.currentAddress, 'market')}

        {searchType === 'None' && this._renderCategoryList()}
        {searchType === 'Searching' && this._renderSearchHistory()}
        {searchType === 'Done' && this._renderSearchResult()}
        {searchType === 'Searching' && <Button onPress={this._onSearch} style={{ borderRadius: 0 }} text="SEARCH" />}
        <KeyboardSpacer />
        <FirebaseManager />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  headerView: {
    height: 56,
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 0
  },
  inputView: {
    height: 40,
    backgroundColor: Assets.colors.headerSearchBarBg,
    borderRadius: 6,
    flex: 1
  },

  inputText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    marginTop: 4,
    height: 40
  },
  cancelText: {
    textAlign: 'center',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 12,
    color: Assets.colors.textLight,
    letterSpacing: -0.2,
    marginHorizontal: 10
  }
})

const mapStateToProps = (state: StoreState) => {
  return {
    categoryList: state.categoryList,
    itemList: state.itemList,
    userProfile: state.userProfile,
    searchHistories: state.searchHistories,
    filter: state.filters
  }
}

export default connect(mapStateToProps)(Home)
