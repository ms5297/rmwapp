import React from 'react'
import { StyleSheet, Modal, View, Keyboard } from 'react-native'
import { Header, Text, TextInput } from 'rn-components'
import { Navigator } from '@Navigation'
import { Api, AppsFlyerHelper, GoogleAnalyticsHelper } from '@Services'
import { BackButton, Button } from '@Components'
import { NavigationProps } from '@Types'
import { Constants, Utils } from '@Utils'
import { PromotionCodeAddParam } from '@Models'
import Assets from '@Assets'
import Styles from '@Styles'
import { getStore } from '@ReduxManager'

type Props = {
  navigation: NavigationProps
}

type State = PromotionCodeAddParam
class PromotionCodeAdd extends React.Component<Props, State> {
  _inputRef = React.createRef<TextInput>()
  constructor(props: Props) {
    super(props)
    const param: PromotionCodeAddParam = this.props.navigation.getParam('param')
    this.state = param
  }

  componentDidMount() {
    console.log('hi')
    setTimeout(() => this._inputRef.current.focus(), 300)
  }

  _onAddCode = () => {
    const { promoCodeList, credits, ids, onAddCodeSuccess, onDeliveryCodeSuccess, deliveryPoromoCodeList } = this.state
    const { userProfile } = getStore().getState()

    const promoCode = this._inputRef.current.getText()
    if (promoCode === '') {
      Navigator.showToast('Error', 'Please enter a promo code.', 'Error')
      return
    }

    if (typeof onAddCodeSuccess !== 'function' || promoCodeList.length == 0) {
      Navigator.showToast('Error', '(PRC - 39) Internal Error', 'Error')
      return
    }
    if (typeof onDeliveryCodeSuccess !== 'function') {
      Navigator.showToast('Error', '(PRC - 39) Internal Error', 'Error')
      return
    }

    const validPromoCode = promoCodeList.find(item => item.name.toLowerCase() === promoCode.toLowerCase())

    const validDeliveryPromoCode = deliveryPoromoCodeList.find(item => item.code.toLowerCase() === promoCode.toLowerCase())

    if (!validPromoCode && !validDeliveryPromoCode) {
      Navigator.showToast('Error', 'Promo code is not valid.', 'Error')
      return
    }

    Keyboard.dismiss()
    Navigator.showLoading()
    console.log('**** response')

    if (validPromoCode) {
      const name = validPromoCode.name
      let coupon = null
      Api.getCouponDetailByName<any>(name)
        .then(response => {
          if (response.code !== 200) {
            throw Error(response.message || 'Promo code is not valid.')
          }
          return response.coupon
        })
        .then(response => {
          coupon = response
          return Api.getRentalAmount<any>({
            item_ids: ids,
            credits: credits && credits > 0 ? 1 : 0,
            coupon_code: name
          })
        })
        .then(response => {
          const { code, message, coupon_amount, credits, payable_fee, items, service_fee } = response
          console.log('**** response', response)
          if (code !== 200 || !Array.isArray(items) || items.length == 0) throw Error('Promo code is not valid.')
          console.log('**** response', response)

          let fees: any = Utils.calcAdditionalFees(items)
          fees.payable_fee = payable_fee
          fees.credits = credits
          fees.coupon_amount = coupon_amount
          fees.service_fee = service_fee
          Navigator.hideLoading()
          Navigator.showToast(
            'Success',
            'Your code added successfully.',
            'Success',
            3000,
            () => {
              onAddCodeSuccess(coupon, fees)
            },
            () => {
              if (!validDeliveryPromoCode) {
                Navigator.back()
              }
            },
            true
          )
        })
        .catch(error => {
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
        })
    }
    if (validDeliveryPromoCode) {
      AppsFlyerHelper.trackEvent(AppsFlyerHelper.EVENTS.PROMO_CODE_USED, userProfile.id, promoCode)

      GoogleAnalyticsHelper.trackEvent(GoogleAnalyticsHelper.EVENT_CATEGORY.MAIN, GoogleAnalyticsHelper.EVENT.PROMO_CODE_USED, { user_id: userProfile.id, promo_code: promoCode })

      Navigator.hideLoading()
      Navigator.showAlert(
        'Code applied successfully',
        `This rental qualifies for free delivery!\nA Rent My Wardrobe team member will reach out to you to coordinate item drop off.`,
        () => {
          onDeliveryCodeSuccess(promoCode)
          Navigator.back()
        },
        null,
        'Continue'
      )

    }

  }

  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header title="Promotional Code" titleStyle={Styles.headerTitle} LeftComponent={<BackButton />} />
        <View style={{ alignItems: 'center', marginTop: 40, marginHorizontal: 20 }}>
          <Text style={styles.title} text="Enter your code" />
          <TextInput
            ref={this._inputRef}
            placeholder="Promo Code"
            placeholderTextColor={Assets.colors.placeholder}
            style={styles.inputContainer}
            inputStyle={styles.input}
          />
          <Button text="Add Code" style={styles.button} onPress={this._onAddCode} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 24,
    color: '#455154'
  },
  button: {
    marginHorizontal: 60,
    marginTop: Constants.WINDOW_HEIGHT / 4 - 48,
    alignSelf: 'stretch'
  },
  inputContainer: {
    alignSelf: 'stretch',
    marginTop: 40,
    backgroundColor: 'transparent',
    borderRadius: 4,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: Assets.colors.borderColor
  },
  input: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    height: 44,
    marginLeft: 10,
    textAlignVertical: 'center',
    paddingTop: 4
  }
})

export default PromotionCodeAdd
