import React from 'react'
import { View, Keyboard, StyleSheet, Animated } from 'react-native'
import { connect } from 'react-redux'
import { Header, ScrollView, Textarea, Text, Row, Col } from 'rn-components'
import { AsyncImage, BackButton, Button, RentalCollection } from '@Components'
import { StoreState } from '@ReduxManager'
import { Constants, Device, Formater, Utils, LayoutAnimations } from '@Utils'
import { NavigationProps } from '@Types'
import { Navigator } from '@Navigation'
import {
  StripePaymentMethods,
  StripePaymentInfo,
  CheckoutParam,
  CheckoutReviewParam,
  Item,
  AdditionalFeeModel
} from '@Models'
import { Api } from '@Services'
import Assets from '@Assets'
import Styles from '@Styles'

type Props = {
  navigation: NavigationProps
  paymentMethods: StripePaymentMethods
  paymentInfo: StripePaymentInfo
}

type State = CheckoutParam & {
  payableFee: number
  slideAnimatedValue: Animated.Value
}

const slideY = Device.getScreenSize().height
class Checkout extends React.Component<Props, State> {
  _textAreaRef = React.createRef<Textarea>()
  _scrollViewRef = React.createRef<ScrollView>()

  constructor(props: Props) {
    super(props)
    const param: CheckoutParam = this.props.navigation.getParam('param')
    const { confirming, items, selectedDates } = param
    const ids = items.map(item => item.id)
    this.state = {
      confirming,
      items,
      selectedDates,
      payableFee: 0,
      slideAnimatedValue: new Animated.Value(0)
    }
    LayoutAnimations.enableAndroidLayoutAnimation()
  }

  componentWillMount() {
    this._updateAmount(this.state.items)
  }

  _updateAmount = items => {
    Navigator.showLoading()
    const { selectedDates } = this.state
    const ids = items.map(item => item.id)
    Api.getRentalAmount({
      item_ids: ids,
      credits: 0,
      coupon_code: '',
      dates_count: selectedDates.length
    })
      .then((response: any) => {
        const { code, message, payable_fee, items: fees } = response
        if (code !== 200) throw message

        const newItems = items.map((item, index) => ({
          ...item,
          additional_fees: fees[index]
        }))
        this.setState({ payableFee: payable_fee, items: newItems })

        Navigator.hideLoading()
        Animated.spring(this.state.slideAnimatedValue, {
          toValue: 1,
          useNativeDriver: true,
          friction: 9,
          tension: 10
        }).start()
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  _onItemAdded = (item: Item) => {
    const clone = this.state.items.slice()
    clone.push(item)
    this._updateAmount(clone)
  }

  _onItemRemoved = (item: Item) => {
    const selectedItems = this.state.items.filter(it => it.id !== item.id)
    LayoutAnimations.setLayoutAnimation(LayoutAnimations.PresetLinear)
    this._updateAmount(selectedItems)
  }

  _onAddMore = () => {
    const { items, selectedDates } = this.state
    const user = {
      id: items[0].owner.id
    }

    Navigator.navTo('PublicProfile', {
      user,
      onItemAdded: this._onItemAdded,
      selectedItems: items,
      selectedDates,
      fromMultiItemScreen: true
    })
  }

  _onTapFooterButton = () => {
    Keyboard.dismiss()
    const { items } = this.state
    const { paymentMethods, paymentInfo } = this.props
    const message = this._textAreaRef.current.getText()

    const item = items[0]

    if (message.trim().length === 0) {
      Navigator.showToast('Error', 'Enter your message to ' + item.owner.name + '.', 'Error', 3000, () =>
        this._textAreaRef.current.focus()
      )
      return
    }

    if (paymentMethods.length === 0) {
      Navigator.navTo('AddCreditCard', {
        checkoutProcess: true,
        onCardAdded: card => {
          const param: CheckoutReviewParam = {
            message,
            card,

            ...this.state
          }
          setTimeout(() => Navigator.navTo('CheckoutReview', { param }), 500)
        }
      })
    } else {
      let paymentMethod = paymentMethods[0]
      const defaultPaymentMethod = paymentInfo.default_source
        ? paymentMethods.find(item => item.id === paymentInfo.default_source)
        : null
      if (defaultPaymentMethod) {
        if (Array.isArray(defaultPaymentMethod)) {
          if (defaultPaymentMethod.length) {
            paymentMethod = defaultPaymentMethod[0]
          }
        } else {
          paymentMethod = defaultPaymentMethod
        }
      }

      const param: CheckoutReviewParam = {
        message,
        card: paymentMethod,
        onItemRemoved: this._onItemRemoved,
        ...this.state
      }

      if (paymentMethod) {
        Navigator.navTo('CheckoutReview', { param })
      } else {
        Navigator.showToast('Error', '(CK - 76) Internal Error', 'Error')
      }
    }
  }

  _renderItemDetails() {
    const { items } = this.state

    const unit = items.length == 1 ? 'item' : 'items'
    const subTitle = `${items.length} ${unit}`

    console.log('**** items', items)
    return (
      <RentalCollection
        style={{ marginTop: 20 }}
        title="Rental Collection"
        subTitle={subTitle}
        items={items}
        showTotalPrice={false}
        onItemRemoved={this._onItemRemoved}
      />
    )
  }

  _renderRentalDates = () => {
    const { selectedDates } = this.state
    const selectedDateRange = Utils.getDateStringFromDateRange(selectedDates)
    return (
      <View style={{ flexDirection: 'column' }}>
        <Text style={[styles.itemTitleText, { marginLeft: 15, marginTop: 10, fontSize: 16 }]} text="Rental Dates" />
        <Text
          style={[styles.itemRetailPriceText, { marginLeft: 15, marginTop: 3, marginBottom: 15 }]}
          text={selectedDateRange}
        />
        <View style={{ flex: 1, height: 1, backgroundColor: '#E7EAEB' }} />
      </View>
    )
  }

  _renderAdditionalDetails = () => {
    const { items } = this.state
    const item = items[0]
    const title = `Tell ${item.owner.name} about your occasion`
    return (
      <View style={{ flexDirection: 'column', marginTop: 20, flex: 1 }}>
        <Text
          style={[styles.itemTitleText, { marginLeft: 15, marginTop: 10, fontSize: 16, lineHeight: 28 }]}
          text={title}
        />
        <Text
          style={[styles.itemRetailPriceText, { marginLeft: 15, marginTop: 12, marginBottom: 20 }]}
          text="Share a few details about your plans to help them prepare your garment."
        />

        <Textarea
          ref={this._textAreaRef}
          style={styles.inputContainer}
          onFocus={() => this._scrollViewRef.current.scrollToCurrentInput(135)}
          inputStyle={styles.input}
          placeholderTextColor={Assets.colors.placeholder}
          selectionColor={Assets.colors.mainText}
          placeholder="Write your message here"
          inputAccessoryViewID="_sendMessage"
          InputAccessoryComponent={
            <Text
              containerStyle={Styles.nextAccessoryContainer}
              style={Styles.nextAccessoryText}
              text="Review Details"
              onPress={this._onTapFooterButton}
            />
          }
        />
      </View>
    )
  }

  _getAnimationStyle = () => {
    const { slideAnimatedValue } = this.state
    const translateY = slideAnimatedValue.interpolate({ inputRange: [0, 1], outputRange: [slideY, 0] })

    return { transform: [{ translateY }] }
  }

  _renderFooter() {
    const { payableFee } = this.state
    const { paymentMethods } = this.props
    const animationStyle = this._getAnimationStyle()
    const buttonTitle = paymentMethods.length === 0 ? 'Add Payment' : 'Review Details'
    const totalFeeText = Formater.formatMoney(payableFee)
    return (
      <Animated.View style={animationStyle}>
        <Row style={styles.footer} alignHorizontal="space-between" alignVertical="center">
          <Col alignVertical="center">
            <Text numberOfLines={1} style={styles.itemRetailPriceText} text="Total" />
            <Text numberOfLines={1} style={styles.itemPriceText} text={totalFeeText} />
          </Col>
          {payableFee > 0 && (
            <Row style={{ marginLeft: 15 }} flex={1}>
              <Button style={{ flex: 1, marginRight: 6 }} type="outline" text="Add More" onPress={this._onAddMore} />
              <Button
                style={{ flex: 1, marginRight: 10, marginLeft: 6 }}
                text={buttonTitle}
                onPress={this._onTapFooterButton}
              />
            </Row>
          )}
        </Row>
      </Animated.View>
    )
  }

  render() {
    return (
      <View style={[StyleSheet.absoluteFill, { backgroundColor: Assets.colors.componentBg }]}>
        <Header title="Review Rental Details" titleStyle={Styles.headerTitle} LeftComponent={<BackButton />} />
        <ScrollView showsVerticalScrollIndicator={false} ref={this._scrollViewRef}>
          {this._renderItemDetails()}
          {this._renderRentalDates()}
          {this._renderAdditionalDetails()}
        </ScrollView>
        {this._renderFooter()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  footerView: {
    backgroundColor: '#F4F6F6',
    flexDirection: 'row',
    alignItems: 'center'
  },

  itemImage: {
    height: 100 * Constants.IMAGE_RATIO,
    width: 100,
    borderRadius: 6,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F4F6F6'
  },

  itemTitleText: {
    marginLeft: 20,
    marginTop: 8,
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: Assets.colors.mainText,
    lineHeight: 18
  },
  linkText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.appTheme,
    lineHeight: 18
  },
  itemPriceText: {
    marginTop: 1,
    marginLeft: 23,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 20,
    color: Assets.colors.appTheme,
    lineHeight: 22
  },

  itemRetailPriceText: {
    marginLeft: 23,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    letterSpacing: -0.2,
    lineHeight: 18,
    color: Assets.colors.textLight
  },

  availabilityView: {
    height: 48,
    width: Constants.WINDOW_WIDTH / 2 - 15,
    marginTop: 15,
    marginBottom: 15,
    marginRight: 15,
    borderRadius: 6,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Assets.colors.buttonColor
  },

  availabilityText: {
    textAlign: 'center',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: 'white'
  },
  inputContainer: {
    marginBottom: 40,
    marginLeft: 15,
    backgroundColor: Assets.colors.inputBg,
    borderRadius: 6,
    height: 112,
    marginHorizontal: 15
  },

  input: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 10,
    lineHeight: 22,
    paddingVertical: 0
  },

  itemHolderView: {
    // flex: 1,
    height: 44,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    alignItems: 'center'
  },

  tickMarkView: {
    width: 18,
    height: 18,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 15,
    borderWidth: 0.5,
    borderColor: Assets.colors.placeholder
  },
  footer: {
    paddingBottom: Device.isIphoneX() ? 30 : 10,
    backgroundColor: '#F4F6F6',
    paddingTop: 15
  }
})

const mapStateToProps = (state: StoreState) => {
  return {
    paymentMethods: state.payments.paymentMethods,
    paymentInfo: state.payments.paymentInfo
  }
}

export default connect(mapStateToProps)(Checkout)
