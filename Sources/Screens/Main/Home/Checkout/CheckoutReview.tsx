import React from 'react'
import { View, Image, TouchableOpacity, StyleSheet, Text as RNText, Animated, Modal, SafeAreaView } from 'react-native'
import { connect } from 'react-redux'
import { Header, Text, ScrollView, Row, Col, TextInput } from 'rn-components'
import { BackButton, Button, RentalCollection } from '@Components'
import { NavigationProps } from '@Types'
import { Api, FirebaseHelper, AppsFlyerHelper, GoogleAnalyticsHelper } from '@Services'
import { Navigator } from '@Navigation'
import { AppConfig, Constants, Formater, DateTime, Device, Utils, LayoutAnimations } from '@Utils'
import { StoreState } from '@ReduxManager'
import {
  UserProfile,
  CheckoutReviewParam,
  PromotionCodeParam,
  PromotionCodeAddParam,
  PromotionCodeModel,
  RequestSuccessParam,
  ChatModelItem,
  AdditionalFeeModel,
  Item,
  ChatModelOrderItem
} from '@Models'
import Assets from '@Assets'
import Styles from '@Styles'
import firebase from 'react-native-firebase'
import { Bubbles } from 'react-native-loader'

type Props = {
  navigation: NavigationProps
  userProfile: UserProfile
}

type State = CheckoutReviewParam & {
  agreeTerms: boolean
  promoCode: any
  promoCodeList: Array<PromotionCodeModel>
  fees: AdditionalFeeModel & {
    coupon_amount: number
    credits: number
    payable_fee: number
  }
  animatedBottom: Animated.Value
  loading: boolean,
  showPromoCodeModal: boolean,
  noDeliveryPromoCode: boolean,
  deliveryPromoCode: any,
  deliveryPoromoCodeList: Array<PromotionCodeModel>
}
class CheckoutReview extends React.Component<Props, State> {
  _currentChatId = null
  _currentRentalId = null
  _otherUser = null
  _chatDoc1 = null
  _chatDoc2 = null
  _conversationRef = firebase.firestore().collection('conversations')

  constructor(props) {
    super(props)

    const param: CheckoutReviewParam = this.props.navigation.getParam('param')

    this.state = {
      agreeTerms: false,
      promoCode: null,
      promoCodeList: [],
      fees: null,
      loading: false,
      animatedBottom: new Animated.Value(0),
      ...param,
      showPromoCodeModal: false,
      noDeliveryPromoCode: false,
      deliveryPromoCode: null,
      deliveryPoromoCodeList: []
    }

    LayoutAnimations.enableAndroidLayoutAnimation()
  }

  componentWillMount() {
    Navigator.showLoading()
    let promoCodeList = []
    let deliveryPoromoCodeList = []
    Promise.all([Api.getCoupons<any>(), Api.getUserCredits<any>(), Api.getDeliveryPromoCodes<any>()])
      .then(values => {
        const couponsResponse = values[0]
        const creditsResponse = values[1]
        const deliveryPromoCodesResponse = values[2]
        console.log(deliveryPromoCodesResponse);

        if (couponsResponse.code !== 200) {
          throw Error(couponsResponse.message || 'Fail to get credits')
        }

        if (creditsResponse.code !== 200) {
          throw Error(couponsResponse.message || 'Fail to get credits')
        }
        if (deliveryPromoCodesResponse.code !== 200) {
          throw Error(deliveryPromoCodesResponse.message || 'Fail to get delivery code')
        }

        return [couponsResponse.coupons, creditsResponse.credits, deliveryPromoCodesResponse.promo_codes]
      })
      .then(values => {
        const { items, selectedDates } = this.state
        const ids = items.map(item => item.id)
        const [coupons, credits, promo_codes] = values
        if (Array.isArray(coupons)) {
          promoCodeList = coupons
        }
        if (Array.isArray(promo_codes)) {
          deliveryPoromoCodeList = promo_codes
        }
        return Api.getRentalAmount<any>({
          item_ids: ids,
          credits: credits && credits > 0 ? 1 : 0,
          coupon_code: '',
          dates_count: selectedDates.length
        })
      })
      .then(response => {
        const { code, message, items, payable_fee, credits, coupon_amount, service_fee } = response
        if (code !== 200 || !Array.isArray(items) || items.length == 0)
          throw Error(message || '(CR - 143) Internal Error')

        let fees: any = Utils.calcAdditionalFees(items)
        fees.payable_fee = payable_fee
        fees.credits = credits
        fees.coupon_amount = coupon_amount
        fees.service_fee = service_fee

        console.log('**** fees', fees)
        console.log('**** delivery promo codes', deliveryPoromoCodeList)

        LayoutAnimations.setLayoutAnimation(LayoutAnimations.PresetEaseInOut)
        this.setState({ promoCodeList, fees, deliveryPoromoCodeList })

        Animated.spring(this.state.animatedBottom, {
          toValue: 1,
          useNativeDriver: true,
          friction: 9,
          tension: 10
        }).start()
        Navigator.hideLoading()
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error', 3000, () => null, () => Navigator.back(), true)
      })
  }
  _applyDeliveryPromoCode() {
    const { deliveryPromoCode } = this.state;
    if (!deliveryPromoCode) {
      Navigator.showToast('Error', 'Please enter promo code.', 'Error')
      return
    }
    // this.setState({ showPromoCodeModal: false });
    Navigator.showLoading()
    Api.getDeliveryPromoCode({ code: deliveryPromoCode })
      .then((response: any) => {
        Navigator.hideLoading()
        const { code, message, ...data } = response
        if (code !== 200) {
          throw Error(message)
        }
        this.setState({ showPromoCodeModal: false });
        Navigator.showAlert(
          'Code applied successfully',
          'This rental qualifies for free delivery! A member from our team will reach out to you to coordinate drop off.',
          () => {
            this._onTapFooterButton()
          },
          null,
          'Continue'
        )
        // Navigator.showToast('Success', 'Your promotional code applied successfully.', 'Success')
        // this._onTapFooterButton();
      })
      .catch(error => {
        this.setState({ deliveryPromoCode: null })
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error', 3000, () => null, () => null, true)
      })
  }

  _onTapFooterButton = () => {
    const { agreeTerms, noDeliveryPromoCode, deliveryPromoCode } = this.state
    if (!agreeTerms) {
      Navigator.showToast('Error', 'Please agree to our terms and conditions', 'Error')
      return
    }
    // if (!noDeliveryPromoCode && !deliveryPromoCode) {
    //   this.setState({ showPromoCodeModal: true });
    //   return
    // } else {
    //   this.setState({ showPromoCodeModal: false })
    // }

    this.setState({ loading: true })
    const { userProfile } = this.props
    const { selectedDates, items, message: _message } = this.state
    const item = items[0]
    //const { rental_fee } = item
    const sizeTitle = Utils.getTitleWithValue('sizes', item.size_id)
    const message = _message ? _message : ''
    //const rentalFee = `$${rental_fee}`

    let conversationData = {
      fromUser: {
        id: userProfile.id,
        name: Utils.getUserName(userProfile),
        photo: userProfile.photo
      },

      itemId: item.id,
      toUser: {
        id: item.owner.id,
        name: item.owner.name,
        photo: item.owner.photo
      },
      order: {
        itemId: item.id,
        itemName: item.name,
        itemSize: sizeTitle,
        rentalPrice: item.rental_fee,
        hasCleaning: item.cleaning_fee > 0,
        firstMessage: message,
        date: Utils.getDateStringFromDateRange(selectedDates),
        startDate: selectedDates && selectedDates.length ? selectedDates[0] : null,
        endDate: selectedDates && selectedDates.length ? selectedDates[selectedDates.length - 1] : null,
        status: 'request'
      },
      rental_id: null,
      size_ids: items.map(item => item.size_id),
      itemIds: items.map(item => item.id),
      orders: items.map(item => ({
        itemId: item.id,
        itemName: item.name,
        itemSize: sizeTitle,
        rentalPrice: item.rental_fee,
        hasCleaning: item.cleaning_fee > 0,
        firstMessage: message,
        date: Utils.getDateStringFromDateRange(selectedDates),
        startDate: selectedDates && selectedDates.length ? selectedDates[0] : null,
        endDate: selectedDates && selectedDates.length ? selectedDates[selectedDates.length - 1] : null,
        status: 'request'
      }))
    }

    this._otherUser = conversationData.toUser
    let dataConversation: ChatModelItem = null

    const conversationRefFiltered = this._conversationRef
      .where('fromUser.id', '==', conversationData.fromUser.id)
      .where('toUser.id', '==', conversationData.toUser.id)
      .where('itemId', '==', conversationData.itemId)

    conversationRefFiltered
      .where('order.startDate', '==', conversationData.order.startDate)
      .where('order.endDate', '==', conversationData.order.endDate)
      .get()
      .then(snapShot => {
        if (snapShot.size == 0) {
          return true
        }
        let conversations = []
        snapShot.docs.forEach(doc => {
          let data = doc.data() as ChatModelItem
          data.id = doc.id
          conversations.push(data)
        })

        conversations.sort((chat1, chat2) => {
          if (chat1.createdAt > chat2.createdAt) return -1
          if (chat1.createdAt < chat2.createdAt) return 1
        })

        dataConversation = conversations[0]
        const statusValue = dataConversation.status
        const isClosedRental = Utils.isClosedRental(statusValue)
        return isClosedRental
      })
      .then(shouldCreateRental => {
        if (shouldCreateRental) {
          conversationRefFiltered.get().then(snapShot1 => {
            let index = snapShot1.size + 1
            this._createRental(index, conversationData, selectedDates)
          })
        } else {
          const id1 = dataConversation.id
          const id2 = this._getSecondChatId(id1)
          this._currentChatId = id1
          this._chatDoc1 = this._conversationRef.doc(id1)
          this._chatDoc2 = this._conversationRef.doc(id2)

          conversationData.rental_id = dataConversation.rental_id

          this._currentRentalId = dataConversation.rental_id

          const doc = {
            status: 'request',
            order: {
              ...conversationData.order,
              status: 'request'
            },
            orders: conversationData.orders.map(doc => ({ ...doc, status: 'request' }))
          }
          this._chatDoc1.set(doc, { merge: true })
          this._chatDoc2.set(doc, { merge: true })

          this._requestItem(conversationData, true, error => {
            if (error) {
              throw error
            }
            this._sendMessage(conversationData.order.firstMessage, false, false, false, null, error => {
              if (error) {
                throw error
              }
              const param: RequestSuccessParam = {
                rentalId: dataConversation.rental_id,
                itemId: conversationData.itemId
              }
              this.setState({ loading: false })
              Navigator.navTo('RequestSuccess', { param })
            })
          })
        }
      })
      .catch(error => {
        this.setState({ loading: false })
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  _getSecondChatId(id1) {
    let idComponents = id1.split('_')

    return idComponents[1] + '_' + idComponents[0] + '_' + idComponents[2] + '_' + idComponents[3]
  }

  _createRental = (index, chat, selectedDates) => {
    const { promoCode, fees, message = '', noDeliveryPromoCode, deliveryPromoCode } = this.state
    const { userProfile } = this.props
    let conversationData = { ...chat }
    let freeDelivery = false;
    if (deliveryPromoCode) {
      freeDelivery = true;
    }
    const rentalDict = {
      item_ids: conversationData.itemIds,
      status: 'request',
      dates: selectedDates,
      coupon_code: promoCode ? promoCode.name : '',
      credits: fees && fees.credits > 0 ? 1 : 0,
      size_ids: conversationData.itemIds.map((itemId, index) => {
        return { item_id: itemId, size_id: conversationData.size_ids[index] }
      }),
      free_delivery: freeDelivery,
      delivery_promo_code: deliveryPromoCode
    }

    console.log('**** rentalDict', rentalDict)
    Api.requestRental<any>(rentalDict)
      .then(response => {
        if (response.code !== 200) {
          throw Error(response.message || '(CR - 143) Internal Error.')
        }
        if (!response.id) {
          throw Error('(Checkout Review-283) Invalid Rental Id! Please contact support.')
        }
        return response
      })
      .then(response => {
        conversationData.rental_id = response.id
        this._currentRentalId = response.id

        let fromUser = userProfile.id
        let toUser = conversationData.toUser.id
        let itemId = conversationData.itemId

        let id1 = fromUser + '_' + toUser + '_' + itemId + '_' + index
        let id2 = toUser + '_' + fromUser + '_' + itemId + '_' + index

        let time = DateTime.getTimestamp()

        conversationData.archived = false
        conversationData.createdAt = time
        conversationData.status = 'request'
        conversationData.statusDate = time
        conversationData.itemOwner = false
        this._currentChatId = id1
        this._chatDoc1 = this._conversationRef.doc(id1)
        this._chatDoc2 = this._conversationRef.doc(id2)

        const payloadDoc2 = {
          toUser: conversationData.fromUser,
          itemId: itemId,

          rental_id: conversationData.rental_id,
          fromUser: conversationData.toUser,
          archived: false,
          order: conversationData.order,
          itemOwner: true,
          createdAt: time,
          status: 'request',
          statusDate: time,
          itemIds: conversationData.itemIds,
          orders: conversationData.orders
        }

        return Promise.all([
          this._chatDoc1.set(conversationData, { merge: true }),
          this._chatDoc2.set(payloadDoc2, { merge: true })
        ])
      })
      .then(() => {
        this._requestItem(conversationData, false, error => {
          if (error) throw error
          this._sendMessage(message, false, false, false, null)
          this.setState({ loading: false })
          const param: RequestSuccessParam = {
            rentalId: conversationData.rental_id,
            itemId: conversationData.itemId
          }

          AppsFlyerHelper.trackEvent(AppsFlyerHelper.EVENTS.RENTAL_REQUESTED, this.props.userProfile.id, conversationData.rental_id)
          GoogleAnalyticsHelper.trackEvent(GoogleAnalyticsHelper.EVENT_CATEGORY.MAIN, GoogleAnalyticsHelper.EVENT.RENTAL_REQUESTED, { user_id: this.props.userProfile.id, rental_id: conversationData.rental_id })

          Navigator.navTo('RequestSuccess', { param })
        })
      })
      .catch(error => {
        this.setState({ loading: false })
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  _requestItem(chatData, shouldUpdate = true, callback: (error: Error) => void) {
    if (chatData.rental_id == null) {
      callback(Error('Invalid rental id'))
    } else {
      const update = () => {
        let itemName = chatData.order.itemName
        const itemDate = chatData.order.date

        if (Array.isArray(chatData.orders) && chatData.orders.length) {
          let names = []
          chatData.orders.forEach(item => {
            if (item) {
              names.push(item.itemName)
            }
          })
          itemName = names.join(', ')
        }

        this._sendMessage('Rental request for ' + itemName + ' for ' + itemDate, true, true, true, null, callback)
      }

      if (shouldUpdate) {
        this._changeRentalStatus(chatData.rental_id, 'request', error => {
          if (error) {
            Navigator.showToast('Error', '(Checkout Review - 354) ' + error.message, 'Error')
          } else {
            update()
          }
        })
      } else {
        update()
      }
    }
  }

  _sendMessage(text, isCustom, sendPush, statusChange, pushMessage, callback?: (error: Error) => void) {
    const time = DateTime.getTimestamp()
    this._chatDoc2
      .get()
      .then(snapShot => {
        const data = snapShot.data() as ChatModelItem
        const newUnRead = data.unread ? data.unread + 1 : 1

        let promisses: any = [
          this._sendMessageToCurrentUser(text, time, isCustom),
          this._sendMessageToFromUser(text, time, isCustom),
          this._updateChatDocumentForCurrentUser(text, time),
          this._updateChatDocumentForToUser(text, newUnRead, time)
        ]

        if (sendPush) {
          promisses.push(this._updatePushHistory(pushMessage ? pushMessage : text, statusChange))
        }
        return Promise.all(promisses)
      })
      .then(() => {
        if (typeof callback === 'function') {
          callback(null)
        }
      })
      .catch(error => {
        if (typeof callback === 'function') {
          callback(error)
        }
      })
  }

  _changeRentalStatus = (rentalId: number, status: string, callback?: (error: Error) => void) => {
    const { deliveryPromoCode } = this.state;
    let freeDelivery = false;
    if (deliveryPromoCode) {
      freeDelivery = true;
    }
    Api.editRequestRental({
      id: rentalId, status: status, free_delivery: freeDelivery,
      delivery_promo_code: deliveryPromoCode, pre_approved: true
    })
      .then((response: any) => {
        const { code, message, ...data } = response
        if (code !== 200) {
          throw Error(message || 'Fail to update rental status')
        }

        AppsFlyerHelper.trackEvent(AppsFlyerHelper.EVENTS.RENTAL_CREATED, this.props.userProfile.id, rentalId)

        if (typeof callback === 'function') {
          callback(null)
        }
      })
      .catch(error => {
        if (typeof callback === 'function') {
          callback(error)
        }
      })
  }

  // _changeMultiRentalStatus = (rentalIds: Array<number>, status: string, callback?: (error: Error) => void) => {
  //   let promises = [];
  //   rentalIds.forEach(id => {
  //     promises.push(Api.editRequestRental({ id, status: status }));
  //   });
  //   Promise.all(promises)
  //     .then(values => {
  //       if (typeof callback === "function") {
  //         callback(null);
  //       }
  //     })
  //     .catch(error => {
  //       if (typeof callback === "function") {
  //         callback(error);
  //       }
  //     });
  //   // Api.editRequestRental({ id: rentalId, status: status })
  //   //   .then((response: any) => {
  //   //     const { code, message, ...data } = response;
  //   //     if (code !== 200) {
  //   //       throw Error(message || "Fail to update rental status");
  //   //     }
  //   //     if (typeof callback === "function") {
  //   //       callback(null);
  //   //     }
  //   //   })
  //   //   .catch(error => {
  //   //     if (typeof callback === "function") {
  //   //       callback(error);
  //   //     }
  //   //   });
  // };

  _sendMessageToFromUser(text, time, isCustom) {
    const { userProfile } = this.props
    const message = {
      text: text,
      image: null,
      isImage: false,
      sender: userProfile.id,
      messageId: Utils.generateUUID(),
      time: time,
      isCustom: isCustom
    }
    return this._chatDoc1.collection('messages').add(message)
  }

  _sendMessageToCurrentUser(text, time, isCustom) {
    const { userProfile } = this.props
    const message = {
      text: text,
      sender: userProfile.id,
      messageId: Utils.generateUUID(),
      isImage: false,
      time: time,
      image: null,
      isCustom: isCustom
    }
    return this._chatDoc2.collection('messages').add(message)
  }

  _updateChatDocumentForCurrentUser(text, time) {
    const payload = {
      lastMessage: text,
      lastMessageTime: time,
      archived: false
    }
    return this._chatDoc1.set(payload, { merge: true })
  }

  _updateChatDocumentForToUser(text, unread, time) {
    const payload = {
      lastMessage: text,
      lastMessageTime: time,
      unread: unread,
      archived: false
    }

    return this._chatDoc2.set(payload, { merge: true })
  }

  _updatePushHistory(text, statusChange) {
    const { userProfile } = this.props
    const { items } = this.state
    const item = items[0]
    const name = Utils.getUserName(userProfile)

    const postData = {
      type: 'chat',
      toUser: this._otherUser.id,
      fromUser: userProfile.id,
      fromUserName: name,
      item: item.id,
      isOwner: false,
      chatID: this._currentChatId,
      rental_id: this._currentRentalId,
      statusChange: statusChange,
      approval: false,
      chat: { text },
      itemIds: items.map(item => item.id)
    }

    let data = { ...postData }
    data['action'] = 'request'
    FirebaseHelper.logEvent('PerformRentalAction', data)

    return firebase
      .firestore()
      .collection('pushHistory')
      .add(postData)
  }

  _showPromoCode = () => {
    const { promoCode, promoCodeList, fees, items, deliveryPoromoCodeList, deliveryPromoCode } = this.state

    if (promoCode) {
      const param: PromotionCodeParam = {
        onRemoveCode: this.onRemoveCode,
        promoCode,
      }
      Navigator.navTo('PromotionCode', { param })
    } else if (deliveryPromoCode) {
      let deliverCode: any;
      deliverCode = { name: deliveryPromoCode };
      const param: PromotionCodeParam = {
        onRemoveCode: () => this.setState({ deliveryPromoCode: null }),
        promoCode: deliverCode,
      }
      Navigator.navTo('PromotionCode', { param })
    }
    else {
      const credits = fees.credits
      const ids = items.map(item => item.id)
      const param: PromotionCodeAddParam = {
        promoCodeList,
        credits,
        ids,
        onAddCodeSuccess: (promoCode, fees) => this.setState({ promoCode, fees }),
        deliveryPoromoCodeList,
        onDeliveryCodeSuccess: (promoCode) => this.setState({ deliveryPromoCode: promoCode })
      }
      Navigator.navTo('PromotionCodeAdd', { param })
    }
  }

  _showPaymentMethods = () => {
    Navigator.navTo('PaymentMethods', {
      checkoutProcess: true,
      onSelectCard: card => {
        this.setState({ card })
      }
    })
  }

  onAddCodeSuccess = (promoCode, fees) => {
    this.setState({ promoCode, fees })
  }

  onRemoveCode = () => {
    const { fees, items } = this.state

    const ids = items.map(item => item.id)

    Navigator.showLoading()
    const { credits } = fees
    Api.getRentalAmount<any>({
      item_ids: ids,
      credits: credits && credits > 0 ? 1 : 0,
      coupon_code: ''
    })
      .then(response => {
        const { code, message, items, payable_fee, credits, coupon_amount, service_fee } = response
        if (code !== 200 || !Array.isArray(items) || items.length == 0)
          throw Error(message || '(CR - 143) Internal Error')

        let fees: any = Utils.calcAdditionalFees(items)
        fees.payable_fee = payable_fee
        fees.credits = credits
        fees.coupon_amount = coupon_amount
        fees.service_fee = service_fee

        console.log('**** fees', fees)

        LayoutAnimations.setLayoutAnimation(LayoutAnimations.PresetEaseInOut)
        this.setState({ promoCode: null, fees, deliveryPromoCode: null })

        Navigator.hideLoading()
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  _onRemoveRentalItem = (item: Item) => {
    const { items, onItemRemoved, promoCode, fees } = this.state
    const selectedItems = items.filter(it => it.id !== item.id)
    Navigator.showLoading()
    const ids = selectedItems.map(item => item.id)

    // Promise.all([
    //   Api.getItemAdditionalFee(ids),
    //   Api.getRentalAmount({
    //     item_ids: ids,
    //     credits: fees.credits,
    //     coupon_code: promoCode ? promoCode.name : ""
    //   })
    // ])
    //   .then((values: any) => {
    //     const { code1, message1, payable_fee, items: fees } = values[0];
    //     if (code1 !== 200) throw Error(message1);

    //     const { code2, message2, payable_fee, items: fees } = values[1];
    //     if (code2 !== 200) throw Error(message2);
    //   })
    //   .catch(error => {});

    Api.getRentalAmount({
      item_ids: ids,
      credits: fees.credits,
      coupon_code: promoCode ? promoCode.name : ''
    })
      .then((response: any) => {
        const { code, message, payable_fee, items: _fees, service_fee } = response
        if (code !== 200) throw message

        let fees = Utils.calcAdditionalFees(_fees)
        const newFees = {
          ...this.state.fees,
          ...fees,
          payable_fee,
          service_fee
        }

        console.log('**** newFees', newFees)
        if (typeof onItemRemoved === 'function') {
          onItemRemoved(item)
        }

        LayoutAnimations.setLayoutAnimation(LayoutAnimations.PresetLinear)
        this.setState({ fees: newFees, items: selectedItems })
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  _renderItemDetails() {
    const { items, selectedDates } = this.state
    const dates = Utils.getDateStringFromDateRange(selectedDates)

    if (Array.isArray(items) && items.length) {
      const unit = items.length == 1 ? 'item' : 'items'
      const subTitle = `${items.length} ${unit} \t Dates: ${dates}`
      return (
        <RentalCollection
          style={{ marginTop: 20 }}
          showTotalPrice={false}
          title="Rental Collection"
          subTitle={subTitle}
          items={items}
          onItemRemoved={this._onRemoveRentalItem}
        />
      )
    }

    return null

    // return (
    //   <RentalCollection
    //     style={{ marginTop: 20 }}
    //     title="Rental Collection"
    //     subTitle={`1 item \t Dates: ${dates}`}
    //     items={[item]}
    //     // onItemRemoved={item => {
    //     //   const selectedItems = this.state.selectedItems.filter(it => it.id !== item.id);
    //     //   LayoutAnimations.setLayoutAnimation(LayoutAnimations.PresetLinear);
    //     //   this.setState({ selectedItems });
    //     // }}
    //   />
    // );

    // let url = "";
    // if (item.photos == null) {
    //   url = item.photo.url;
    // } else {
    //   let images = item.photos.map(el => {
    //     return el.url;
    //   });
    //   url = images.length > 0 ? images[0] : "";
    // }

    // const itemSize = Utils.getTitleWithValue("sizes", item.size_id);
    // const imageWidth = 100;
    // const imageHeight = imageWidth * Constants.IMAGE_RATIO;
    // const size = `Size: ${itemSize}`;
    // const closet = `${item.owner.name}’s Closet`;

    // return (
    //   <View style={{ marginTop: 15, marginLeft: 15, marginRight: 15, flexDirection: "row" }}>
    //     <View style={styles.itemImage}>
    //       <AsyncImage
    //         source={{ uri: url }}
    //         width={imageWidth}
    //         height={imageHeight}
    //         placeholderSource={Assets.images.cartImagePlaceHolder}
    //         fallbackImageSource={Assets.images.cartImagePlaceHolder}
    //       />
    //     </View>
    //     <View style={{ flex: 1, flexDirection: "column" }}>
    //       <Text style={styles.itemTitleText}>{item.name}</Text>
    //       <Text style={[styles.itemRetailPriceText, { marginLeft: 20, marginTop: 25 }]} text={size} />
    //       <Text style={[styles.itemRetailPriceText, { marginLeft: 20, marginTop: 5 }]} text={closet} />
    //     </View>
    //   </View>
    // );
  }

  // _renderRentalDates() {
  //   const { selectedDates } = this.state;
  //   const dates = Utils.getDateStringFromDateRange(selectedDates);
  //   return (
  //     <View style={{ flexDirection: "column", marginTop: 18 }}>
  //       <Text style={[styles.itemTitleText, { marginLeft: 15, marginTop: 10, fontSize: 16 }]} text="Rental Dates" />
  //       <Text style={[styles.itemRetailPriceText, { marginLeft: 15, marginTop: 3, marginBottom: 15 }]} text={dates} />
  //       <View style={{ flex: 1, height: 1, backgroundColor: "#E7EAEB" }} />
  //     </View>
  //   );
  // }

  _renderCardDetails() {
    let cardInfo = ''
    const { card } = this.state
    if (card) {
      cardInfo = card.brand + ' **** ' + card.last4
    }

    return (
      <TouchableOpacity activeOpacity={0.7} onPress={this._showPaymentMethods}>
        <View style={{ flex: 1, flexDirection: 'column', marginTop: 7 }}>
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Text style={[styles.itemTitleText, { marginLeft: 15, marginTop: 10, fontSize: 16 }]} text="Payment" />
              <Text
                style={[styles.itemRetailPriceText, { marginLeft: 15, marginTop: 3, marginBottom: 15 }]}
                text={cardInfo}
              />
            </View>
            <Image
              source={Assets.images.profileEditDetailIcon}
              style={{ marginRight: 15, tintColor: Assets.colors.textLight }}
            />
          </View>

          <View style={{ flex: 1, height: 1, backgroundColor: '#E7EAEB' }} />
        </View>
      </TouchableOpacity>
    )
  }

  _renderPromoCode() {
    const { promoCodeList, promoCode, deliveryPoromoCodeList, deliveryPromoCode } = this.state
    if (promoCodeList.length === 0 && deliveryPoromoCodeList.length === 0) return null
    let promoCodeName = '';
    if (promoCode && promoCode.name) {
      promoCodeName = promoCode.name
    }
    if (deliveryPromoCode && !promoCode) {
      promoCodeName = deliveryPromoCode
    }

    return (
      <TouchableOpacity activeOpacity={0.7} onPress={this._showPromoCode}>
        <View style={{ flex: 1, flexDirection: 'column', marginTop: 7 }}>
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Text style={[styles.itemTitleText, { marginLeft: 15, marginTop: 10, fontSize: 16 }]} text="Promo Code" />
              <Text
                style={[styles.itemRetailPriceText, { marginLeft: 15, marginTop: 3, marginBottom: 15 }]}
                text={promoCodeName}
              />
            </View>
            <Image
              source={Assets.images.profileEditDetailIcon}
              style={{ marginRight: 15, tintColor: Assets.colors.textLight }}
            />
          </View>
          <View style={{ flex: 1, height: 1, backgroundColor: '#E7EAEB' }} />
        </View>
      </TouchableOpacity>
    )
  }

  _renderFeeDetails() {
    const { fees } = this.state
    if (!fees) return null
    const { cleaning_fee, coupon_amount, credits, insurance_fee, rental_fee, replacement_fee, service_fee } = fees

    return (
      <View style={{ marginTop: 15 }}>
        {this._renderFeeItem('Rental Fee Total', rental_fee, true)}
        {this._renderFeeItem('Cleaning Fee', cleaning_fee, false)}

        {insurance_fee > 0 && this._renderFeeItem('Apparel Assurance', insurance_fee, false, AppConfig.insurance_url)}
        {service_fee > 0 && this._renderFeeItem('Service Fee', service_fee, false)}
        {coupon_amount > 0 && this._renderFeeItem('Promo Code', -coupon_amount, false)}
        {credits > 0 && this._renderFeeItem('Credits', -credits, false)}
        <View style={styles.separator} />
      </View>
    )
  }

  _renderFeeItem(title, value, boldStaus, link = null) {
    const priceString = Formater.formatMoney(value)
    const color = link !== null ? Assets.colors.appTheme : Assets.colors.mainText
    const onPress = link ? () => Navigator.navTo('WebsiteView1', { url: link }) : null
    const valueStyle: any = {
      marginRight: 15,
      lineHeight: 28,
      fontSize: boldStaus ? 16 : 14,
      textAlign: 'right',
      marginLeft: 5,
      marginTop: 0
    }
    return (
      <Row alignVertical="center" onPress={onPress}>
        <Text style={[styles.itemRetailPriceText, { marginLeft: 30, lineHeight: 28, color, flex: 1 }]} text={title} />
        <Text style={[styles.itemTitleText, valueStyle]} text={priceString} />
      </Row>
    )
  }

  _renderRefundPolicy = () => {
    const { items } = this.state
    const item = items[0]
    if (!item.refund_option_id) {
      return null
    }

    var refundPolicy = ''
    if (item.refund_option_id == 2) {
      refundPolicy = 'No refund is offered you will be charged the total rental if you cancel.'
    } else if (item.refund_option_id == 1) {
      refundPolicy =
        'Refund will be issued 100% up to 3 days before your scheduled rental. If the rental is canceled within 3 days you will be charged a 20% cancellation fee.'
    } else {
      return null
    }

    return (
      <View style={{ flexDirection: 'column', marginTop: 15 }}>
        <Text style={[styles.itemTitleText, { marginLeft: 15, fontSize: 16 }]} text="Refund Policy" />
        <Text style={[styles.itemRetailPriceText, { marginLeft: 15, marginTop: 10, marginRight: 15 }]}>
          {refundPolicy}
          <RNText
            onPress={() => {
              Navigator.navTo('WebsiteView1', { url: AppConfig.refund_url })
            }}
            style={styles.linkText}>
            {' Learn More'}
          </RNText>
        </Text>
      </View>
    )
  }

  _renderAdditionalDetails = () => {
    const { items, message: _message } = this.state
    const item = items[0]
    const message = _message ? _message : ''

    return (
      <View style={{ flexDirection: 'column', marginTop: 25 }}>
        <Text
          style={[styles.itemTitleText, { marginLeft: 15, marginTop: 10, fontSize: 16, lineHeight: 28 }]}
          text={`Message to ${item.owner.name}`}
        />
        <Text
          style={[styles.itemRetailPriceText, { marginLeft: 15, marginTop: 10, marginBottom: 24, marginRight: 15 }]}
          text={message}
        />
      </View>
    )
  }

  _onToggleArgeeTerm = () => this.setState({ agreeTerms: !this.state.agreeTerms })

  _renderAgreeTermsView = () => {
    return (
      <View>
        <View style={styles.itemHolderView}>
          <TouchableOpacity activeOpacity={0.7} onPress={this._onToggleArgeeTerm}>
            <View style={styles.tickMarkView}>
              {this.state.agreeTerms && (
                <Image
                  source={Assets.images.checkboxOn}
                  resizeMode="contain"
                  style={{ tintColor: Assets.colors.appTheme, width: 14, height: 14 }}
                />
              )}
            </View>
          </TouchableOpacity>

          <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row' }}>
            <Text
              style={[styles.itemRetailPriceText, { marginLeft: 6, lineHeight: 18 }]}
              onPress={() => this.setState({ agreeTerms: !this.state.agreeTerms })}
              text="I agree to the"
            />
            <Text
              style={[styles.itemRetailPriceText, { marginLeft: 0, lineHeight: 18, color: Assets.colors.appTheme }]}
              onPress={() => Navigator.navTo('WebsiteView1', { url: AppConfig.terms_url })}
              text=" terms and conditions"
            />
          </View>
        </View>
      </View>
    )
  }

  _renderFooter = () => {
    const { fees, animatedBottom } = this.state
    const totalFee = fees ? fees.payable_fee : 0
    const totalFeeText = Formater.formatMoney(totalFee)
    const translateY = animatedBottom.interpolate({
      inputRange: [0, 1],
      outputRange: [Constants.WINDOW_HEIGHT + 120, 0]
    })
    const animationStyle = {
      transform: [{ translateY }]
    }
    return (
      <Col>
        {this._renderAgreeTermsView()}
        <Animated.View style={[styles.bottomRow, animationStyle]}>
          <View style={{ flex: 1 }}>
            <Text numberOfLines={1} style={styles.itemRetailPriceText} text="Total" />
            <Text numberOfLines={1} style={styles.itemPriceText} text={totalFeeText} />
          </View>
          {totalFee >= 0 && (
            <Button style={{ flex: 1, marginHorizontal: 15 }} onPress={this._onTapFooterButton} text="Request Rental" />
          )}
        </Animated.View>
      </Col>
    )
  }
  _dontHaveDeliveryCode() {
    this.setState({ showPromoCodeModal: false, noDeliveryPromoCode: true, deliveryPromoCode: null })
    // this.setState({  })
    // Navigator.hideLoading()
    setTimeout(() => {
      this._onTapFooterButton()
    }, 100);
  }
  _renderPromoCodeModal() {
    return (
      <Modal
        visible={this.state.showPromoCodeModal}
        animationType="none"
        transparent
        // presentationStyle={'overFullScreen'}
        onRequestClose={() => {
          this.setState({ showPromoCodeModal: false });
        }}>
        <SafeAreaView
          style={styles.modlaMainView}
        >
          <View style={{ width: 303, backgroundColor: '#fff', borderRadius: 5, shadowColor: '#000', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 3, marginLeft: 'auto', marginRight: 'auto', alignItems: 'center', justifyContent: 'center', alignSelf: 'center', }}>
            <View style={{ flexDirection: 'column', justifyContent: 'center', padding: 20 }}>
              <View style={{ flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start', marginBottom: 20 }}>
                <Text style={{
                  fontSize: 16,
                  fontWeight: "bold",
                  fontStyle: "normal",
                  color: '#454b54'
                }}
                > Have a promo code for free delivery? </Text>
              </View>
              <View style={{ flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start', marginBottom: 20 }}>
                <TextInput
                  // LeftComponent={<Text style={styles.codeText} text="+1" />}
                  clearButtonMode="while-editing"
                  onChangeText={text => this.setState({ deliveryPromoCode: text })}
                  // defaultValue={phoneString}
                  // keyboardType="phone-pad"
                  placeholder="Promo Code"
                  style={styles.modalInputContainer}
                  inputStyle={[styles.input]}
                  selectionColor={Assets.colors.mainText}
                  placeholderTextColor={Assets.colors.placeholder}
                />

              </View>
              <Row alignHorizontal="space-between" alignVertical="center">
                <Button
                  style={[{ marginRight: 10, flex: 1 }, styles.negativeButton]}
                  textStyle={styles.negativeButtonText}
                  text="Don't have code"
                  onPress={() => {
                    this._dontHaveDeliveryCode();
                  }
                  }
                //  onPress={onDecline}

                />
                <Button
                  style={[{ marginLeft: 10, flex: 1 }, styles.positiveButton]}
                  textStyle={styles.positiveButtonText}
                  text="Apply"
                  onPress={() => this._applyDeliveryPromoCode()}
                />
              </Row>

            </View>
          </View>
        </SafeAreaView>
      </Modal>
    )


  }
  render() {
    return (
      <View style={[StyleSheet.absoluteFill, { backgroundColor: Assets.colors.componentBg }]}>
        <Header title="Review Rental Details" titleStyle={Styles.headerTitle} LeftComponent={<BackButton />} />
        <ScrollView showsVerticalScrollIndicator={false}>
          {/* {this._renderPromoCodeModal()} */}
          {this._renderItemDetails()}
          {/* {this._renderRentalDates()} */}
          {this._renderCardDetails()}
          {this._renderPromoCode()}
          {this._renderFeeDetails()}
          {this._renderRefundPolicy()}
          {this._renderAdditionalDetails()}
        </ScrollView>
        {this._renderFooter()}

        <Modal visible={this.state.loading} transparent animationType="none">
          <View style={[StyleSheet.absoluteFill, { backgroundColor: 'rgba(0,0,0,0.2)' }]}>
            <View style={{ marginTop: 160, alignItems: 'center' }}>
              <Bubbles size={7} color="#F06182" />
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  itemImage: {
    height: 100 * Constants.IMAGE_RATIO,
    width: 100,
    borderRadius: 6,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F4F6F6'
  },

  itemTitleText: {
    marginLeft: 20,
    marginTop: 8,
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: Assets.colors.mainText,
    lineHeight: 18
  },
  linkText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.appTheme,
    lineHeight: 18
  },
  itemPriceText: {
    marginTop: 1,
    marginLeft: 23,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 20,
    color: Assets.colors.appTheme,
    lineHeight: 22
  },

  itemRetailPriceText: {
    marginLeft: 23,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    letterSpacing: -0.2,
    lineHeight: 18,
    color: Assets.colors.textLight
  },

  availabilityView: {
    height: 48,
    width: Constants.WINDOW_WIDTH / 2 - 15,
    marginTop: 15,
    marginBottom: 15,
    marginRight: 15,
    borderRadius: 6,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Assets.colors.buttonColor
  },

  availabilityText: {
    textAlign: 'center',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: 'white'
  },

  backgroundImage: {
    width: '100%',
    height: 48,
    alignItems: 'center',
    justifyContent: 'center'
  },

  inputContainer: {
    marginBottom: 40,
    marginLeft: 15,
    backgroundColor: Assets.colors.inputBg,
    borderRadius: 6,
    height: 112,
    width: Constants.WINDOW_WIDTH - 30
  },

  input: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 10,
    lineHeight: 22,
    paddingVertical: 0
  },

  itemHolderView: {
    // flex: 1,
    height: 44,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    alignItems: 'center'
  },

  tickMarkView: {
    width: 18,
    height: 18,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 15,
    borderWidth: 0.5,
    borderColor: Assets.colors.placeholder
  },
  bottomRow: {
    backgroundColor: Assets.colors.inputBg,
    paddingTop: 15,
    paddingBottom: Device.isIphoneX() ? 25 : 15,
    flexDirection: 'row',
    alignItems: 'center'
  },
  separator: {
    flex: 1,
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    height: 1,
    backgroundColor: '#E7EAEB'
  },

  modlaMainView: {
    borderRadius: 12,
    backgroundColor: "rgba(37, 43, 61, 0.5)",
    shadowColor: "rgba(43, 56, 68, 0.29)",
    shadowOffset: {
      width: 0,
      height: 18
    },
    shadowRadius: 29,
    shadowOpacity: 1,
    flex: 1,
    justifyContent: "center"
  },
  modalInputContainer: {
    marginBottom: 10,
    backgroundColor: 'white',
    borderColor: Assets.colors.borderColorLight,
    borderWidth: 0.5,
    borderRadius: 6,
    height: 44,
    width: '100%'
  },
  saveButton: {
    borderRadius: 0,
    marginBottom: Device.isIphoneX() ? 20 : 0
  },
  positiveButton: {},
  positiveButtonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: 'white',
    paddingVertical: 8
  },
  negativeButton: {
    backgroundColor: 'white',
    borderRadius: 6,
    borderWidth: 1,
    borderColor: Assets.colors.borderColor
  },
  negativeButtonText: {
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.display.regular,
    fontSize: 14,
    paddingVertical: 8
  }
})

const mapStateToProps = (state: StoreState) => {
  return {
    userProfile: state.userProfile
  }
}

export default connect(mapStateToProps)(CheckoutReview)
