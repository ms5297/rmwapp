import React from 'react'
import { Device } from '@Utils'
import { StyleSheet, View } from 'react-native'
import { Header, Text } from 'rn-components'
import { Navigator } from '@Navigation'
import { NavigationProps } from '@Types'
import { Button } from '@Components'
import { ChatParam, RequestSuccessParam } from '@Models'
import firebase from 'react-native-firebase'
import Assets from '@Assets'

const marginTop = Device.isAndroid ? 0 : 0
const fontSize = Device.isAndroid ? 32 : 36
const vs = Device.vs

type Props = {
  navigation: NavigationProps
}
type State = RequestSuccessParam & {}
class RequestSuccess extends React.Component<Props, State> {
  _chatDoc = null
  _chatDoc2 = null
  _otherUser = null
  _unsubscribe = null
  _currentChatId = null
  _conversationRef = firebase.firestore().collection('conversations')

  constructor(props: Props) {
    super(props)
    const param: RequestSuccessParam = this.props.navigation.getParam('param')
    this.state = param
  }

  _onDone = () => {
    const { itemId, rentalId } = this.state

    const param: ChatParam = {
      rental_id: rentalId,
      item: { id: itemId }
    }
    Navigator.pop(4, true)
    Navigator.navTo('Chat', { param })
  }

  _onSkip = () => Navigator.pop(4, false)

  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header title="" />
        <View style={styles.content}>
          <Text style={[styles.titleText, { marginTop, fontSize }]} text="Your rental request is pending" />
          <Text
            style={styles.infoText}
            text="Once the lender accepts the reservation dates, your reservation will be confirmed and your payment method will be charged."
          />
          <Text
            style={styles.infoText}
            text="You can message your lender and view reservation details in your inbox."
          />
          <Text
            style={styles.infoText}
            text="Remember a temporary hold will be placed on your card the first day of the rental and released once returned on check-in."
          />
          <Button style={styles.doneButton} type="solid" text="Go to inbox" onPress={this._onDone} />
          <Button style={styles.skipButton} type="outline" text="Skip" onPress={this._onSkip} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  titleText: {
    marginTop: 25,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    width: '100%'
  },
  subTitleText: {
    marginTop: 25,
    fontSize: 24,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.display.bold,
    width: '100%'
  },
  infoText: {
    marginTop: 15,
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    width: '100%',
    lineHeight: 22
  },

  content: {
    flex: 1,
    marginHorizontal: 30,
    marginTop: 20 * vs
  },
  doneButton: {
    width: '80%',
    marginTop: 73 * vs,
    alignSelf: 'center'
  },
  skipButton: {
    marginTop: 20,
    width: '80%',
    alignSelf: 'center'
  }
})

export default RequestSuccess
