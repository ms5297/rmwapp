import React from 'react'
import { View, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { Header, Text } from 'rn-components'
import { NavigationProps } from '@Types'
import { StoreState } from '@ReduxManager'
import { UserProfile, MessageSuccessParam, ChatModelItem, CheckoutParam } from '@Models'
import { Device, Utils, DateTime } from '@Utils'
import { Api, FirebaseHelper } from '@Services'
import { Navigator } from '@Navigation'
import { Button } from '@Components'
import Assets from '@Assets'
import firebase from 'react-native-firebase'

type Props = {
  navigation: NavigationProps
  userProfile: UserProfile
}

type State = MessageSuccessParam
class MessageSuccess extends React.Component<Props, State> {
  _rental_id = null
  _chatDoc = null
  _chatDoc2 = null
  _otherUser = null
  _unsubscribe = null
  _currentChatId = null
  _conversationRef = firebase.firestore().collection('conversations')

  constructor(props: Props) {
    super(props)
    const param: MessageSuccessParam = this.props.navigation.getParam('param')
    this.state = param
  }

  componentDidMount() {
    Navigator.showLoading()
    this._proceedWithItem()
    Navigator.hideLoading()
  }

  _proceedWithItem() {
    const { userProfile } = this.props
    const { items, selectedDates, message, date } = this.state

    const item = items[0]

    const chat = {
      fromUser: {
        id: userProfile.id,
        name: Utils.getUserName(userProfile),
        photo: userProfile.photo
      },
      itemId: item.id,
      order: {
        itemId: item.id,
        itemName: item.name,
        itemSize: Utils.getTitleWithValue('sizes', item.size_id),
        rentalPrice: item.rental_fee,
        hasCleaning: item.cleaning_fee > 0,
        firstMessage: message ? message : '',
        date: date ? date : '',
        startDate: selectedDates[0],
        endDate: selectedDates[selectedDates.length - 1],
        status: 'inquiry'
      },
      toUser: {
        id: item.owner.id,
        name: item.owner.name,
        photo: item.owner.photo
      },
      size_ids: items.map(it => it.size_id),
      itemIds: items.map(it => it.id),
      orders: items.map(it => ({
        itemId: it.id,
        itemName: it.name,
        itemSize: Utils.getTitleWithValue('sizes', it.size_id),
        rentalPrice:it.rental_fee,
        hasCleaning: it.cleaning_fee > 0,
        firstMessage: message ? message : '',
        date: date ? date : '',
        startDate: selectedDates[0],
        endDate: selectedDates[selectedDates.length - 1],
        status: 'inquiry'
      }))
    }

    this._otherUser = chat.toUser
    const fromUser = userProfile.id
    const toUser = chat.toUser.id
    const itemId = chat.itemId
    const order = chat.order

    firebase
      .firestore()
      .collection('conversations')
      .where('fromUser.id', '==', fromUser)
      .where('toUser.id', '==', toUser)
      .where('itemId', '==', itemId)
      .where('order.startDate', '==', order.startDate)
      .where('order.endDate', '==', order.endDate)
      .get()
      .then(snapShot => {
        if (snapShot.size == 0) {
          firebase
            .firestore()
            .collection('conversations')
            .where('fromUser.id', '==', fromUser)
            .where('toUser.id', '==', toUser)
            .where('itemId', '==', itemId)
            .get()
            .then(snapShot1 => {
              let index = snapShot1.size + 1
              this._createRental(index, chat)
            })
        } else {
          let conversations = []
          snapShot.docs.forEach(doc => {
            let data = doc.data() as ChatModelItem
            data.id = doc.id
            conversations.push(data)
          })

          conversations.sort((chat1, chat2) => {
            if (chat1.createdAt > chat2.createdAt) return -1
            if (chat1.createdAt < chat2.createdAt) return 1
          })

          const data = conversations[0]
          const id1 = data.id
          const id2 = this._getSecondChatId(id1)
          const statusValue = data.status
          const isClosedRental = Utils.isClosedRental(statusValue)
          if (isClosedRental) {
            const index = snapShot.size + 1
            this._createRental(index, chat)
          } else {
            this._chatDoc = this._conversationRef.doc(id1)
            this._chatDoc2 = this._conversationRef.doc(id2)
            this._sendMessage(message)
          }
        }
      })
  }

  _getSecondChatId(id1) {
    let idComponents = id1.split('_')

    return idComponents[1] + '_' + idComponents[0] + '_' + idComponents[2] + '_' + idComponents[3]
  }

  _createRental = (index, chat) => {
    const { selectedDates, message } = this.state
    let conversationData = { ...chat }
    const rentalDict = {
      item_ids: conversationData.itemIds,
      status: 'inquiry',
      dates: selectedDates,
      message,
      size_ids: conversationData.itemIds.map((itemId, index) => {
        return { item_id: itemId, size_id: conversationData.size_ids[index] }
      })
    }
    console.log('**** rentaldict', rentalDict)

    Api.requestRental<any>(rentalDict)
      .then(response => {
        if (response.code !== 200) throw Error(response.message || '(MS - 146) Internal Error')
        conversationData.rental_id = response.id
        this._rental_id = response.id
        const { userProfile } = this.props
        const status = 'inquiry'
        this._otherUser = conversationData.toUser
        const fromUser = userProfile.id
        let toUser = this._otherUser.id
        let itemId = conversationData.itemId

        let id1 = fromUser + '_' + toUser + '_' + itemId + '_' + index
        let id2 = toUser + '_' + fromUser + '_' + itemId + '_' + index

        let time = DateTime.getTimestamp()

        conversationData.archived = false
        conversationData.createdAt = time
        conversationData.status = status
        conversationData.order = conversationData.order
        conversationData.statusDate = time
        conversationData.itemOwner = false
        const payload = {
          toUser: conversationData.fromUser,
          rental_id: conversationData.rental_id,
          itemId: itemId,
          fromUser: conversationData.toUser,
          archived: false,
          order: conversationData.order,
          itemOwner: true,
          createdAt: time,
          status: status,
          statusDate: time,
          orders: conversationData.orders,
          itemIds: conversationData.itemIds
        }
        this._chatDoc = this._conversationRef.doc(id1)
        this._currentChatId = id1
        this._chatDoc.set(conversationData, { merge: true })
        this._chatDoc2 = this._conversationRef.doc(id2)
        this._chatDoc2.set(payload, { merge: true })
        this._sendMessage(message)
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  _sendMessage = (text, image = null, isCustom = false, sendPush = true, callback?: (error: Error) => void) => {
    const time = DateTime.getTimestamp()
    this._chatDoc2
      .get()
      .then(snapShot => {
        const data = snapShot.data() as ChatModelItem
        const newUnRead = data.unread ? data.unread + 1 : 1

        let promisses: any = [
          this._sendMessageToCurrentUser(text, image, time, isCustom),
          this._sendMessageToFromUser(text, image, time, isCustom),
          this._updateChatDocumentForCurrentUser(text, image, time),
          this._updateChatDocumentForToUser(text, image, newUnRead, time)
        ]
        if (sendPush) {
          promisses.push(this._updatePushHistory(text, image))
        }
        return Promise.all(promisses)
      })
      .then(() => {
        if (typeof callback === 'function') {
          callback(null)
        }
      })
      .catch(error => {
        if (typeof callback === 'function') {
          callback(error)
        }
      })
  }

  _updateChatDocumentForToUser(text, image, unread, time) {
    return this._chatDoc2.set(
      {
        lastMessage: image ? 'Image' : text,
        lastMessageTime: time,
        unread: unread,
        archived: false
      },
      { merge: true }
    )
  }

  _updatePushHistory(text, image) {
    const { items } = this.state
    const { userProfile } = this.props
    const name = Utils.getUserName(userProfile)

    const item = items[0]
    const itemName = Utils.getItemName(items)
    const postData = {
      type: 'chat',
      toUser: this._otherUser.id,
      fromUser: userProfile.id,
      fromUserName: name,
      item: item.id,
      isOwner: false,
      chatID: this._currentChatId,
      rental_id: this._rental_id,
      statusChange: true,
      chat: { text: 'New inquiry for ' + itemName },
      itemIds: items.map(item => item.id)
    }

    let data = { ...postData }
    data['action'] = 'inquiry'
    FirebaseHelper.logEvent('PerformRentalAction', data)

    return firebase
      .firestore()
      .collection('pushHistory')
      .add(postData)
  }

  _sendMessageToFromUser(text, image, time, isCustom) {
    const { userProfile } = this.props
    const message = {
      text: text,
      image: image,
      isImage: image != null,
      sender: userProfile.id,
      messageId: Utils.generateUUID(),
      time: time,
      isCustom: isCustom
    }
    return this._chatDoc.collection('messages').add(message)
  }

  _sendMessageToCurrentUser(text, image, time, isCustom) {
    const { userProfile } = this.props
    const message = {
      text: text,
      sender: userProfile.id,
      messageId: Utils.generateUUID(),
      isImage: image != null,
      time: time,
      image: image,
      isCustom: isCustom
    }
    return this._chatDoc2.collection('messages').add(message)
  }

  _updateChatDocumentForCurrentUser(text, image, time) {
    const payload = {
      lastMessage: image ? 'Image' : text,
      lastMessageTime: time,
      archived: false
    }
    return this._chatDoc.set(payload, { merge: true })
  }

  _onDone = () => Navigator.pop(3, false)

  _onRentNow = () => {
    const { selectedDates, items } = this.state
    const param: CheckoutParam = {
      selectedDates,
      items,
      confirming: true
    }
    Navigator.pop(3, true)
    Navigator.navTo('Checkout', { param })
  }

  render() {
    const { items } = this.state
    const item = items[0]
    const info = `You should get a response soon.\n\nDon't forget - you don't have to message ${
      item.owner.name
    } to rent this. You can rent it now for your dates.`
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header />
        <View style={styles.content}>
          <Text style={styles.titleText} text="Message sent!" />
          <Text style={styles.infoText} text={info} />

          <Button style={styles.button} type="solid" text="Rent now" onPress={this._onRentNow} />
          <Button style={styles.skipButton} type="outline" text="Done" onPress={this._onDone} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  titleText: {
    marginTop: 25,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    width: '100%'
  },
  subTitleText: {
    marginTop: 25,
    fontSize: 24,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.display.bold,
    width: '100%'
  },
  infoText: {
    marginTop: 15,
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    width: '100%',
    lineHeight: 22
  },

  content: {
    flex: 1,
    marginHorizontal: 30,
    marginTop: 20
  },

  button: {
    width: '70%',
    marginTop: 73 * Device.vs,
    alignSelf: 'center'
  },
  skipButton: {
    marginTop: 20,
    width: '70%',
    alignSelf: 'center'
  }
})

const mapStateToProps = (state: StoreState) => {
  return {
    userProfile: state.userProfile
  }
}
export default connect(mapStateToProps)(MessageSuccess)
