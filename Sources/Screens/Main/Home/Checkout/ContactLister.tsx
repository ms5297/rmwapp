import React from 'react'
import { View, Image, TouchableOpacity, Keyboard, StyleSheet, LayoutChangeEvent } from 'react-native'
import { Header, ScrollView, Text, Textarea } from 'rn-components'
import { AsyncImage, BackButton, Button, RentalCollection } from '@Components'
import { NavigationProps } from '@Types'
import { Navigator } from '@Navigation'
import { Constants, DateTime, Device, Utils, LayoutAnimations } from '@Utils'
import { CalendarParam, MessageSuccessParam, ContactListerParam, Item } from '@Models'
import Assets from '@Assets'
import Styles from '@Styles'

type Props = {
  navigation: NavigationProps
}

type State = ContactListerParam & {
  message: string
  selectedDates: Array<string>
}
class ContactLister extends React.Component<Props, State> {
  _scrollViewRef = React.createRef<ScrollView>()

  constructor(props: Props) {
    super(props)

    const param: ContactListerParam = this.props.navigation.getParam('param')

    this.state = {
      message: '',
      selectedDates: [],
      ...param
    }
  }

  _onTapDatesButton = () => {
    const { items, selectedDates } = this.state
    let unAvailableList = []

    items.forEach(item => {
      unAvailableList = unAvailableList.concat(item.unavailable_dates)
    })

    const blockedDates = unAvailableList.map(item => item.date)

    const param: CalendarParam = {
      mode: 'selecting',
      selectedDates,
      blockedDates,

      onDateSelected: selectedDates => this.setState({ selectedDates })
    }

    Navigator.navTo('AvailabilityCalendar', { param })
  }

  _onTapFooterButton = () => {
    Keyboard.dismiss()
    const { selectedDates, message, items, additionalFee } = this.state
    if (selectedDates.length === 0) {
      Navigator.showToast('Error', 'Please select dates.', 'Error')
      return
    }

    if (message.trim().length === 0) {
      Navigator.showToast('Error', 'Enter your message to ' + items[0].owner.name + '.', 'Error')
      return
    }

    const param: MessageSuccessParam = {
      items,
      selectedDates,
      date: Utils.getDateStringFromDateRange(selectedDates),
      additionalFee,
      message: message
    }
    console.log('**** param', param)
    this.props.navigation.navigate('MessageSuccess', { param })
  }

  _renderItemDetails = () => {
    const { items } = this.state

    const unit = items.length == 1 ? 'item' : 'items'
    const subTitle = `${items.length} ${unit}`
    return (
      <RentalCollection
        style={{ marginTop: 20 }}
        title="Rental Collection"
        subTitle={subTitle}
        items={items}
        onItemRemoved={this._onItemRemoved}
      />
    )

    // const { item } = this.state;
    // const images = item.photos.map(el => {
    //   return el.url;
    // });

    // const url = images.length > 0 ? images[0] : "";
    // const itemSize = Utils.getTitleWithValue("sizes", item.size_id);

    // const imageWidth = 100;
    // const imageHeight = imageWidth * Constants.IMAGE_RATIO;

    // const size = `Size: ${itemSize}`;
    // const closet = `${item.owner.name}’s Closet`;
    // return (
    //   <View style={{ marginTop: 15, marginLeft: 15, marginRight: 15, flexDirection: "row" }}>
    //     <View style={styles.itemImage}>
    //       <AsyncImage
    //         source={{ uri: url }}
    //         width={imageWidth}
    //         height={imageHeight}
    //         placeholderSource={Assets.images.cartImagePlaceHolder}
    //         fallbackImageSource={Assets.images.cartImagePlaceHolder}
    //       />
    //     </View>
    //     <View style={{ flex: 1, flexDirection: "column" }}>
    //       <Text style={styles.itemTitleText} text={item.name} />
    //       <Text style={[styles.itemRetailPriceText, { marginLeft: 20, marginTop: 25 }]} text={size} />
    //       <Text style={[styles.itemRetailPriceText, { marginLeft: 20, marginTop: 5 }]} text={closet} />
    //     </View>
    //   </View>
    // );
  }

  _renderRentalDates = () => {
    const { selectedDates } = this.state
    let selectedDateRange = Utils.getDateStringFromDateRange(selectedDates)
    if (selectedDateRange === '') {
      selectedDateRange = 'Please select least 1 week.'
    }

    return (
      <TouchableOpacity activeOpacity={0.7} onPress={this._onTapDatesButton}>
        <View style={{ flexDirection: 'column', marginTop: 35 }}>
          <Text style={[styles.itemTitleText, { marginLeft: 15, marginTop: 10, fontSize: 16 }]} text="Dates" />
          <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
            <Text
              style={[styles.itemRetailPriceText, { flex: 1, marginLeft: 15, marginTop: 3, marginBottom: 15 }]}
              text={selectedDateRange}
            />
            <Image source={Assets.images.calendar} style={{ marginRight: 15, tintColor: '#989FA2', opacity: 0.5 }} />
          </View>
          <View style={{ flex: 1, height: 1, backgroundColor: '#E7EAEB' }} />
        </View>
      </TouchableOpacity>
    )
  }

  _onItemAdded = (item: Item) => {
    const clone = this.state.items.slice()
    clone.push(item)
    LayoutAnimations.setLayoutAnimation(LayoutAnimations.PresetLinear)
    this.setState({ items: clone })
  }

  _onItemRemoved = (item: Item) => {
    const selectedItems = this.state.items.filter(it => it.id !== item.id)
    LayoutAnimations.setLayoutAnimation(LayoutAnimations.PresetLinear)
    this.setState({ items: selectedItems })
  }

  _onAddMore = () => {
    const { items } = this.state
    const user = {
      id: items[0].owner.id
    }

    Navigator.navTo('PublicProfile', { user, onItemAdded: this._onItemAdded, selectedItems: items })
  }

  _renderAdditionalDetails() {
    const { items } = this.state
    const title = `Tell ${items[0].owner.name} about your occasion`
    return (
      <View style={{ flexDirection: 'column', marginTop: 40, flex: 1 }}>
        <Text
          style={[styles.itemTitleText, { marginLeft: 15, marginTop: 10, fontSize: 16, lineHeight: 28 }]}
          text={title}
        />
        <Text
          style={[styles.itemRetailPriceText, { marginLeft: 15, marginTop: 12, marginBottom: 20 }]}
          text="Share a few details about your plans to help them prepare your garment."
        />

        <Textarea
          onChangeText={text => this.setState({ message: text })}
          onFocus={() => this._scrollViewRef.current.scrollToCurrentInput(135)}
          defaultValue={this.state.message}
          style={styles.inputContainer}
          inputStyle={styles.input}
          placeholderTextColor={Assets.colors.placeholder}
          selectionColor={Assets.colors.mainText}
          placeholder="Write your message here"
          inputAccessoryViewID="_sendMessage"
          InputAccessoryComponent={
            <Text
              containerStyle={Styles.nextAccessoryContainer}
              style={Styles.nextAccessoryText}
              text="Send message"
              onPress={this._onTapFooterButton}
            />
          }
        />
      </View>
    )
  }

  render() {
    return (
      <View style={[StyleSheet.absoluteFill, { backgroundColor: Assets.colors.componentBg }]}>
        <Header
          title="Contact"
          titleStyle={Styles.headerTitle}
          statusBarProps={{ barStyle: 'dark-content' }}
          LeftComponent={<BackButton />}
        />
        <ScrollView showsVerticalScrollIndicator={false} ref={this._scrollViewRef}>
          {this._renderItemDetails()}
          {this._renderRentalDates()}
          {this._renderAdditionalDetails()}
        </ScrollView>
        <View style={styles.buttonContainer}>
          {/* <Button
            type="outline"
            style={{ marginVertical: 15, marginLeft: 15, marginRight: 8, flex: 1 }}
            text="Add More"
            onPress={this._onAddMore}
          /> */}
          {/* <Button
            style={{ marginVertical: 15, marginRight: 15, marginLeft: 8, flex: 1 }}
            text="Send Message"
            onPress={this._onTapFooterButton}
          /> */}

          <Button
            style={{ marginVertical: 15, marginHorizontal: 15, flex: 1 }}
            text="Send Message"
            onPress={this._onTapFooterButton}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  itemImage: {
    height: 100 * Constants.IMAGE_RATIO,
    width: 100,
    borderRadius: 6,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Assets.colors.inputBg
  },

  itemTitleText: {
    marginLeft: 20,
    marginTop: 8,
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: Assets.colors.mainText,
    lineHeight: 18
  },
  linkText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.appTheme,
    lineHeight: 18
  },
  itemPriceText: {
    marginTop: 1,
    marginLeft: 23,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 20,
    color: Assets.colors.appTheme,
    lineHeight: 22
  },

  itemRetailPriceText: {
    marginLeft: 23,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    letterSpacing: -0.2,
    lineHeight: 18,
    color: Assets.colors.textLight
  },

  inputContainer: {
    marginBottom: 40,
    marginLeft: 15,
    backgroundColor: Assets.colors.inputBg,
    borderRadius: 6,
    height: 112,
    marginHorizontal: 15
  },

  input: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 10,
    lineHeight: 22,
    paddingVertical: 0
  },

  itemHolderView: {
    height: 44,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    alignItems: 'center'
  },

  buttonContainer: {
    backgroundColor: Assets.colors.inputBg,
    paddingBottom: Device.isIphoneX() ? 20 : 0,
    flexDirection: 'row'
  }
})

export default ContactLister
