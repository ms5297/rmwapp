import React from 'react'
import { View, Image, TouchableOpacity, StyleSheet } from 'react-native'
import { AppConfig, Device } from '@Utils'
import { NavigationProps } from '@Types'
import { Navigator } from '@Navigation'
import { FreshChatHelper, Api } from '@Services'
import { Header, Text } from 'rn-components'
import { BackButton, Button, FlatList } from '@Components'
import Assets from '@Assets'

type Props = {
  navigation: NavigationProps
}

type State = {
  selected: number
}
class Reporting extends React.Component<Props, State> {
  state: State = {
    selected: -1
  }

  _onTapReportButton = () => {
    if (this.state.selected === -1) {
      Navigator.showToast('Error', 'Please select reporting reason.', 'Error')
      return
    }

    let message = ''
    let tag = ''
    const { params } = this.props.navigation.state
    const { review, item } = params
    let param = {}
    if (review) {
      message =
        'I would like to report a review.' +
        '\nReason: ' +
        this._selectedItemText +
        '\n\nReview ID: ' +
        review.id +
        '\nUser ID: ' +
        review.reviewer.id +
        '\nUser Name: ' +
        review.reviewer.name +
        '\nReview Text: ' +
        review.description
      tag = 'report_rating'
      param = {
        kind_id: review.id,
        kind: 2,
        report_reason_id: this.state.selected
      }
    } else if (item) {
      message =
        'I would like to report an item.' +
        '\nReason: ' +
        this._selectedItemText +
        '\n\nItem ID: ' +
        item.id +
        '\nUser ID: ' +
        item.owner.id +
        '\nUser Name: ' +
        item.owner.name +
        '\nItem Name: ' +
        item.name
      tag = 'report_item'
      param = {
        kind_id: item.id,
        kind: 1,
        report_reason_id: this.state.selected
      }
    } else {
      Navigator.showToast('Error', '(RP - 73) Internal Error.', 'Error', 3000, () => null, () => Navigator.back(), true)
      return
    }

    Navigator.showLoading()
    FreshChatHelper.sendSupportMessage(message, tag)
    Api.reportItem<any>(param)
      .then(response => {
        if (response.code !== 200) {
          throw Error(response.message || '(RP - 90) Internal Error')
        }
        Navigator.hideLoading()
        Navigator.showToast(
          'Success',
          'Thank you for reporting this to us. Our team will investigate this and take the appropriate action.',
          'Success',
          3000,
          () => null,
          () => Navigator.back(),
          true
        )
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  _selectedItemText = ''

  didSelectReportItem(item) {
    this._selectedItemText = item.title
    this.setState({ selected: item.value })
  }

  _onBack = () => {
    const { state } = this.props.navigation
    const { fromReview } = state.params
    if (fromReview) {
      Navigator.back()
      Navigator.back()
    } else {
      Navigator.back()
    }
  }

  _renderReportHeader = () => {
    return (
      <View>
        <Text style={styles.viewTitleLabel} text="Why are you reporting this content?" />
        <Text style={styles.descriptionText} text="This won’t be shared with the lender." />
      </View>
    )
  }

  _renderReportItem = ({ item, index }) => {
    const { title, value } = item
    const { selected } = this.state
    const checked = selected === value
    return (
      <View style={{ flex: 1 }}>
        <TouchableOpacity activeOpacity={0.7} onPress={() => this.didSelectReportItem(item)}>
          <View style={styles.itemHolderView}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <Text style={styles.itemTitle} text={title} />
            </View>
            <View style={styles.tickMarkView}>
              {checked && <Image source={Assets.images.checkboxOn} style={{ tintColor: Assets.colors.appTheme }} />}
            </View>
          </View>
        </TouchableOpacity>
        <View style={styles.seperatorView} />
      </View>
    )
  }

  render() {
    const isIPhoneX = Device.isIphoneX()
    const marginBottom = isIPhoneX ? 20 : 0
    return (
      <View style={[StyleSheet.absoluteFill, { backgroundColor: Assets.colors.componentBg }]}>
        <Header
          statusBarProps={{ barStyle: 'dark-content' }}
          LeftComponent={<BackButton iconSource={Assets.images.closeIcon} />}
        />
        <FlatList
          style={{ flex: 1 }}
          data={AppConfig.enumConfig.reportings}
          ListHeaderComponent={this._renderReportHeader}
          renderItem={this._renderReportItem}
          showsVerticalScrollIndicator={false}
        />
        <Button style={{ borderRadius: 0, marginBottom }} text="Report" onPress={this._onTapReportButton} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  closeButton: {
    marginLeft: 6,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.appTheme,
    marginTop: 3,
    letterSpacing: -0.4
  },

  viewTitleLabel: {
    marginLeft: 15,
    marginRight: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 30,
    color: Assets.colors.mainText,
    letterSpacing: -0.2,
    lineHeight: 34
  },

  reportButton: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: 48,
    backgroundColor: Assets.colors.buttonColor
  },

  reportButtonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 18,
    textAlign: 'center',
    color: 'white'
  },

  descriptionText: {
    marginLeft: 15,
    marginRight: 15,
    marginTop: 16,
    marginBottom: 40,
    fontFamily: Assets.fonts.display.regular,
    fontSize: 14,
    lineHeight: 18,
    textAlign: 'left',
    color: Assets.colors.mainText
  },

  itemHolderView: {
    flex: 1,
    height: 44,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    alignItems: 'center'
  },

  tickMarkView: {
    width: 24,
    height: 24,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
    borderWidth: 0.5,
    borderColor: Assets.colors.placeholder
  },

  itemTitle: {
    marginLeft: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.4
  },

  seperatorView: {
    height: 1,
    backgroundColor: '#E7EAEB'
  }
})

export default Reporting
