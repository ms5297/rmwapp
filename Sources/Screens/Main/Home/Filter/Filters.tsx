import React from 'react'
import { View, Image, StyleSheet, ScrollView } from 'react-native'
import { Header, Text, Grid, Row } from 'rn-components'
import { BackButton, Button, Section, MultiSlider } from '@Components'
import { ItemSummaryActions, ItemListActions, getStore, FilterActions } from '@ReduxManager'
import {
  FirebaseValueObject,
  Location,
  SelectionListParam,
  PlacePickerParam,
  CalendarParam,
  EnumConfigKey,
  FilterKey,
  FilterParamsModel
} from '@Models'
import { Api, FirebaseHelper } from '@Services'
import { Navigator } from '@Navigation'
import { Device, AppConfig, Utils, DateTime } from '@Utils'
import { NavigationProps } from '@Types'
import Assets from '@Assets'
import Styles from '@Styles'

type Props = {
  navigation: NavigationProps
}

type State = {
  occasions: Array<FirebaseValueObject>
  selectedOccasions: Array<FirebaseValueObject>
  selectedDates: Array<string>
  location: Location | null
  address: string | null
  prices: Array<number>
  locationRange: Array<number>
  keyword: string
  sizes: Array<FirebaseValueObject> | null
  styles: Array<FirebaseValueObject> | null
  colors: Array<FirebaseValueObject> | null
  fitProfiles: Array<FirebaseValueObject> | null
  sleeves: Array<FirebaseValueObject> | null
  hemlines: Array<FirebaseValueObject> | null
  embellishments: Array<FirebaseValueObject> | null
  necklines: Array<FirebaseValueObject> | null
  sortTypes: FirebaseValueObject | null
  shouldSetDefault: boolean
}

class Filters extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    const { params } = props.navigation.state
    const { userProfile, filters } = getStore().getState()
    const { locality, latitude, longitude } = userProfile

    const { shouldSetDefault = false } = params

    let location = typeof longitude === 'number' && typeof latitude === 'number' ? { longitude, latitude } : null
    if (shouldSetDefault) {
      if (filters.itemList.filters && filters.itemList.filters.location) {
        location = filters.itemList.filters.location
      }
    }

    let address = ''
    if (location) {
      address = locality
    }

    if (shouldSetDefault) {
      if (filters.itemList.filters && filters.itemList.filters.locality && filters.itemList.filters.location) {
        address = filters.itemList.filters.locality
      }
    }
    let locationRange: any;
    locationRange = AppConfig.filters.locationRange
    if (filters.itemList.filters && typeof filters.itemList.filters.location_range == 'number') {
      if (filters.itemList.filters.location_range > 100) {
        locationRange = Math.trunc(filters.itemList.filters.location_range / 1609.34)
        locationRange = [locationRange]
      } else {
        locationRange = [filters.itemList.filters.location_range]
      }
    }

    let selectedDates = []
    let prices = AppConfig.filters.priceRange
    let sortTypes = Utils.getConfigWithValue('sortTypes', 1)

    if (shouldSetDefault) {
      if (filters.itemList.filters) {
        if (typeof filters.itemList.filters.availability_start === 'string') {
          const date = DateTime.moment(filters.itemList.filters.availability_start)
          if (date.isValid) {
            selectedDates.push(date)
          }
        }

        if (typeof filters.itemList.filters.availability_end === 'string') {
          const date = DateTime.moment(filters.itemList.filters.availability_end)
          if (date.isValid) {
            selectedDates.push(date)
          }
        }

        if (
          typeof filters.itemList.filters.min_price === 'number' &&
          typeof filters.itemList.filters.max_price === 'number'
        ) {
          prices = [filters.itemList.filters.min_price, filters.itemList.filters.max_price]
        }
      }

      if (filters.itemList.sort) {
        sortTypes = Utils.convertSortTypeToConfig(filters.itemList.sort)
      }
    }

    const sizes = this._getConfigFromValue('sizes', shouldSetDefault)
    const selectedOccasions = this._getConfigFromValue('occasions', shouldSetDefault)
    const styles = this._getConfigFromValue('styles', shouldSetDefault)
    const colors = this._getConfigFromValue('colors', shouldSetDefault)
    const fitProfiles = this._getConfigFromValue('fitProfiles', shouldSetDefault)
    const sleeves = this._getConfigFromValue('sleeves', shouldSetDefault)
    const hemlines = this._getConfigFromValue('hemlines', shouldSetDefault)
    const embellishments = this._getConfigFromValue('embellishments', shouldSetDefault)
    const necklines = this._getConfigFromValue('necklines', shouldSetDefault)

    this.state = {
      selectedOccasions: selectedOccasions ? selectedOccasions : [],
      selectedDates: [],
      sizes,
      prices,
      location: location,
      occasions: AppConfig.enumConfig.occasions,
      address,
      locationRange,
      styles,
      colors,
      fitProfiles,
      sleeves,
      hemlines,
      embellishments,
      necklines,
      sortTypes,
      keyword: params && params.keyword ? params.keyword : '',
      shouldSetDefault
    }
  }

  _getConfigFromValue(type: EnumConfigKey, shouldSetDefault: boolean) {
    let configValue = []
    if (shouldSetDefault) {
      const { filters } = getStore().getState()
      if (filters.itemList && filters.itemList.filters) {
        const defaultFilters = filters.itemList.filters
        let ids = []
        switch (type) {
          case 'sizes':
            ids = Array.isArray(defaultFilters.size) ? defaultFilters.size : []
            break
          case 'occasions':
            ids = Array.isArray(defaultFilters.occasion) ? defaultFilters.occasion : []
            break
          case 'styles':
            ids = Array.isArray(defaultFilters.style) ? defaultFilters.style : []
            break
          case 'colors':
            ids = Array.isArray(defaultFilters.color) ? defaultFilters.color : []
            break
          case 'fitProfiles':
            ids = Array.isArray(defaultFilters.v) ? defaultFilters.v : []
            break
          case 'sleeves':
            ids = Array.isArray(defaultFilters.sleeve) ? defaultFilters.sleeve : []
            break
          case 'hemlines':
            ids = Array.isArray(defaultFilters.hemline) ? defaultFilters.hemline : []
            break
          case 'embellishments':
            ids = Array.isArray(defaultFilters.embellishment) ? defaultFilters.embellishment : []
            break
          case 'necklines':
            ids = Array.isArray(defaultFilters.neckline) ? defaultFilters.neckline : []
            break
        }

        ids.forEach(id => {
          const config = Utils.getConfigWithValue(type, id)
          if (config) {
            configValue.push(config)
          }
        })
      }
    } else {
      const config = Utils.getConfigWithTitle(type, 'All')
      configValue = config ? [config] : []
    }

    return configValue.length ? configValue : null
  }

  _convertConfigToParams(type: EnumConfigKey, values: Array<FirebaseValueObject>, shouldSetDefault: boolean) {
    const { location, sortTypes, prices, selectedDates } = this.state
    let filterParam: FilterParamsModel = {
      page: 1,
      per_page: 20,
      filters: {}
    }

    if (sortTypes) {
      filterParam.sort = Utils.convertConfigToSortType(sortTypes)
    }

    if (shouldSetDefault) {
      const filters = getStore().getState().filters.itemList
      if (filters) {
        filterParam.page = filters.page
      }
    }

    if (location) {
      filterParam.filters.location = location
    }

    if (prices && prices.length == 2) {
      filterParam.filters.min_price = prices[0]
      filterParam.filters.max_price = prices[1]
    }

    if (selectedDates && selectedDates.length) {
      if (selectedDates.length === 1) {
        filterParam.filters.availability_start = selectedDates[0]
      } else {
        filterParam.filters.availability_end = selectedDates[selectedDates.length - 1]
      }
    }

    let shouldAdd = false
    if (values && values.length) {
      shouldAdd = values.findIndex(value => value.title !== 'All') === -1
    }

    switch (type) {
      case 'sizes':
        if (shouldAdd) {
          filterParam.filters['size'] = values.map(value => value.value)
        }
        break
      case 'occasions':
        if (shouldAdd) {
          filterParam.filters['occasion'] = values.map(value => value.value)
        }
        break
      case 'styles':
        if (shouldAdd) {
          filterParam.filters['style'] = values.map(value => value.value)
        }
        break
      case 'colors':
        if (shouldAdd) {
          filterParam.filters['color'] = values.map(value => value.value)
        }
        break
      case 'fitProfiles':
        if (shouldAdd) {
          filterParam.filters['v'] = values.map(value => value.value)
        }
        break
      case 'sleeves':
        if (shouldAdd) {
          filterParam.filters['sleeve'] = values.map(value => value.value)
        }
        break
      case 'hemlines':
        if (shouldAdd) {
          filterParam.filters['hemline'] = values.map(value => value.value)
        }
        break
      case 'embellishments':
        if (shouldAdd) {
          filterParam.filters['embellishment'] = values.map(value => value.value)
        }
        break
      case 'necklines':
        if (shouldAdd) {
          filterParam.filters['neckline'] = values.map(value => value.value)
        }
        break
    }
  }
  _onPriceValueChanged = (prices: Array<number>) => this.setState({ prices })

  _onLocationRangeValueChanged = (locationRange: Array<number>) => this.setState({ locationRange })

  _onTapFilterButton = () => {
    Navigator.showLoading()
    const {
      location,
      sizes,
      keyword,
      sleeves,
      hemlines,
      embellishments,
      colors,
      necklines,
      sortTypes,
      styles,
      fitProfiles,
      selectedOccasions,
      prices,
      locationRange,
      selectedDates,
      shouldSetDefault,
      address
    } = this.state

    let filterParam: FilterParamsModel = {
      page: 1,
      per_page: 20,
      filters: {}
    }

    if (sortTypes) {
      filterParam.sort = Utils.convertConfigToSortType(sortTypes)
    }

    if (location) {
      filterParam.filters.location = location
    } else {
      filterParam.filters.location = {
        longitude: AppConfig.longitude,
        latitude: AppConfig.latitude
      }
    }

    if (prices && prices.length == 2) {
      filterParam.filters.min_price = prices[0]
      filterParam.filters.max_price = prices[1]
    }

    if (locationRange && locationRange.length) {
      const rangeInMile = locationRange[0] * 1609.34;
      filterParam.filters.location_range = rangeInMile
    }

    if (selectedDates && selectedDates.length) {
      if (selectedDates.length === 1) {
        filterParam.filters.availability_start = selectedDates[0]
      } else {
        filterParam.filters.availability_end = selectedDates[selectedDates.length - 1]
      }
    }

    if (shouldSetDefault) {
      if (Array.isArray(sizes) && sizes.length) {
        const shouldAdd = sizes.findIndex(value => value.title === 'All') === -1
        if (shouldAdd) {
          filterParam.filters['size'] = sizes.map(value => value.value)
        }
      }

      if (Array.isArray(selectedOccasions) && selectedOccasions.length) {
        filterParam.filters['occasion'] = selectedOccasions.map(value => value.value)
      }

      if (Array.isArray(styles) && styles.length) {
        const shouldAdd = styles.findIndex(value => value.title === 'All') === -1
        if (shouldAdd) {
          filterParam.filters['style'] = styles.map(value => value.value)
        }
      }

      if (Array.isArray(colors) && colors.length) {
        const shouldAdd = colors.findIndex(value => value.title === 'All') === -1
        if (shouldAdd) {
          filterParam.filters['color'] = colors.map(value => value.value)
        }
      }

      if (Array.isArray(fitProfiles) && fitProfiles.length) {
        const shouldAdd = fitProfiles.findIndex(value => value.title === 'All') === -1
        if (shouldAdd) {
          filterParam.filters['v'] = fitProfiles.map(value => value.value)
        }
      }

      if (Array.isArray(sleeves) && sleeves.length) {
        const shouldAdd = sleeves.findIndex(value => value.title === 'All') === -1
        if (shouldAdd) {
          filterParam.filters['sleeve'] = sleeves.map(value => value.value)
        }
      }

      if (Array.isArray(hemlines) && hemlines.length) {
        const shouldAdd = hemlines.findIndex(value => value.title === 'All') === -1
        if (shouldAdd) {
          filterParam.filters['hemline'] = hemlines.map(value => value.value)
        }
      }

      if (Array.isArray(embellishments) && embellishments.length) {
        const shouldAdd = embellishments.findIndex(value => value.title === 'All') === -1
        if (shouldAdd) {
          filterParam.filters['embellishment'] = embellishments.map(value => value.value)
        }
      }

      if (Array.isArray(necklines) && necklines.length) {
        const shouldAdd = necklines.findIndex(value => value.title === 'All') === -1
        if (shouldAdd) {
          filterParam.filters['neckline'] = necklines.map(value => value.value)
        }
      }
    }

    if (keyword && keyword !== '') {
      filterParam.filters['keyword'] = keyword
    }

    if (Array.isArray(selectedDates) && selectedDates.length > 0) {
      filterParam.filters['availability_start'] = selectedDates[0]
      if (selectedDates.length == 2) {
        filterParam.filters['availability_end'] = selectedDates[1]
      }
    }

    filterParam.filters.locality = address

    const categoryFilterParam = {
      page: 1,
      per_page: 20,
      filters: {
        location: filterParam.filters.location
      }
    }

    Promise.all([Api.getItemSummary(categoryFilterParam), Api.getItemList(filterParam)])
      .then((values: any[]) => {
        const itemsSummary =
          values[0] && values[0].code === 200 && Array.isArray(values[0].categories) ? values[0].categories : []
        const itemList = values[1] && values[1].code === 200 && Array.isArray(values[1].items) ? values[1].items : []
        let itemListFilter = { ...filterParam }
        if (itemList.length) {
          itemListFilter.page += 1
        }

        // itemListFilter.filters['locality'] = address
        FilterActions.updateItemListFilterParam(itemListFilter)
        ItemSummaryActions.saveItemSummary(itemsSummary)
        ItemListActions.saveItemList(itemList)

        const onGoBack = this.props.navigation.getParam('onGoBack')
        Navigator.hideLoading(() => {
          Navigator.back()
          Navigator.navTo('CategoryItemList', { onGoBack: onGoBack })
        })
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })

    FirebaseHelper.logEvent('Search_Items', filterParam)
  }

  _onClear = () => {
    this.setState({
      selectedOccasions: [],
      occasions: AppConfig.enumConfig.occasions,
      selectedDates: [],
      // location: null,
      // address: '',
      sizes: null,
      prices: AppConfig.filters.priceRange,
      locationRange: AppConfig.filters.locationRange,
      styles: null,
      colors: null,
      fitProfiles: null,
      sleeves: null,
      hemlines: null,
      embellishments: null,
      necklines: null,
      sortTypes: null,
      keyword: ''
    })
    FilterActions.resetFilters()
  }

  _renderOccasions = () => {
    const { occasions, selectedOccasions: _selectedItems } = this.state

    return (
      <Grid numCols={2}>
        {occasions.map((item, index) => {
          if (item.value <= 0) {
            return
          }
          const selected = _selectedItems.includes(item)
          const selectedStyle = selected ? stylesheet.occasionSelectedHolder : null
          const onPress = () => {
            const idx = _selectedItems.indexOf(item)
            let selectedItems = _selectedItems
            if (idx === -1) {
              if (selectedItems.length === 6) return
              selectedItems.push(item)
            } else {
              selectedItems.splice(idx, 1)
            }
            this.setState({ selectedOccasions: selectedItems })
          }
          const marginRight = index % 2 == 0 ? 15 : 0
          return (
            <Row key={index} onPress={onPress} style={[stylesheet.occasionItemHolder, selectedStyle, { marginRight }]}>
              <View style={stylesheet.occasionItemContent}>
                <Text style={stylesheet.title} text={item.title} />
                {selected && <Image style={stylesheet.occasionItemIcon} source={Assets.images.tickmark_icon} />}
              </View>
            </Row>
          )
        })}
      </Grid>
    )
  }

  _renderSectionHeader = (title: string, marginTop = 20) => {
    return (
      <View style={[stylesheet.sectionView, { marginTop: marginTop }]}>
        <Text style={stylesheet.sectionTitle} text={title} />
      </View>
    )
  }

  _renderSelectionView = (title: string, value: string, type: EnumConfigKey) => {
    let rightIcon = Assets.images.categoryDetailsArrow
    let titleExtensionStyle = null
    let valueExtensionStyle = null
    if (type == 'date') {
      rightIcon = Assets.images.calendar
      if (value !== '') {
        titleExtensionStyle = { color: Assets.colors.appTheme }
        title = value
        value = ''
      }
    } else {
      if (value !== 'All' && value !== 'Choose a location') {
        valueExtensionStyle = { color: Assets.colors.appTheme }
      }
    }

    const onSelected = () => {
      if (type === 'market') {
        const param: PlacePickerParam = {
          title,
          onLocationSelected: (location, address) => {
            this.setState({ location, address })
          }
        }
        Navigator.navTo('PlacePicker', { param })
      } else if (type === 'date') {
        const param: CalendarParam = {
          mode: 'selecting',
          onDateSelected: selectedDates => this.setState({ selectedDates })
        }
        Navigator.navTo('AvailabilityCalendar', { param })
      } else {
        const mutiple = type !== 'sortTypes'
        const param: SelectionListParam = {
          onItemsSelected: items => {
            // @ts-ignore
            this.setState({ [type]: mutiple ? items : items[0] })
          },
          mutiple,
          title: title,
          selectedItems: mutiple ? this.state[type] : [this.state[type]],
          type
        }
        Navigator.navTo('SelectionList', { param })
      }
    }

    return (
      <Section
        title={title}
        titleStyle={titleExtensionStyle}
        valueStyle={valueExtensionStyle}
        value={value}
        iconSource={rightIcon}
        onPress={onSelected}
      />
    )
  }

  render() {
    const {
      sizes,
      prices,
      address,
      locationRange,
      selectedDates,
      styles,
      colors,
      fitProfiles,
      sleeves,
      hemlines,
      embellishments,
      necklines,
      sortTypes
    } = this.state

    console.log('***** locationRange', locationRange)
    const isIphoneX = Device.isIphoneX()
    const marginBottom = isIphoneX ? 20 : 0

    const dateString = Utils.getDateStringFromDateRange(selectedDates)

    return (
      <View style={[{ backgroundColor: Assets.colors.componentBg }, StyleSheet.absoluteFill]}>
        <Header
          title="Filters"
          titleStyle={Styles.headerTitle}
          statusBarProps={{ barStyle: 'dark-content' }}
          LeftComponent={<BackButton iconSource={Assets.images.closeIcon} />}
          RightComponent={<Text onPress={this._onClear} text="Clear All" style={Styles.textHeaderRight} />}
        />
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingHorizontal: 20 }}>
          {this._renderSectionHeader('Size')}
          {this._renderSelectionView('Sizes', Utils.convertConfigToTitle(sizes, 'All'), 'sizes')}

          {this._renderSectionHeader('Rental Price Range')}
          <MultiSlider values={prices} min={0} max={1001} step={10} onRangeValueChange={this._onPriceValueChanged} />
          {/* {this._renderSectionHeader('Location')}
          {this._renderSelectionView('Search Location', address, 'market')} */}
          {this._renderSectionHeader('Distance from Location')}
          <MultiSlider
            values={locationRange}
            min={1}
            max={100}
            step={1}
            onRangeValueChange={this._onLocationRangeValueChanged}
          />

          {this._renderSectionHeader('Availability')}
          {this._renderSelectionView('All Dates', dateString, 'date')}
          {this._renderOccasions()}
          {this._renderSectionHeader('Item Details')}
          {this._renderSelectionView('Style', Utils.convertConfigToTitle(styles, 'All'), 'styles')}
          {this._renderSelectionView('Color', Utils.convertConfigToTitle(colors, 'All'), 'colors')}
          {this._renderSelectionView('Fit', Utils.convertConfigToTitle(fitProfiles, 'All'), 'fitProfiles')}
          {this._renderSelectionView('Sleeves', Utils.convertConfigToTitle(sleeves, 'All'), 'sleeves')}
          {this._renderSelectionView('Hemline', Utils.convertConfigToTitle(hemlines, 'All'), 'hemlines')}
          {this._renderSelectionView(
            'Embellishment',
            Utils.convertConfigToTitle(embellishments, 'All'),
            'embellishments'
          )}
          {this._renderSelectionView('Neckline', Utils.convertConfigToTitle(necklines, 'All'), 'necklines')}
          {this._renderSectionHeader('Sort by')}
          {this._renderSelectionView('Sort', Utils.convertConfigToTitle(sortTypes, 'All'), 'sortTypes')}
        </ScrollView>
        <Button
          style={{ borderRadius: 0, marginBottom }}
          textStyle={{ fontSize: 18 }}
          text="Apply Filters"
          onPress={this._onTapFilterButton}
        />
      </View>
    )
  }
}

const stylesheet = StyleSheet.create({
  sectionView: {
    height: 50,
    justifyContent: 'center',
    marginTop: 20
  },

  sectionTitle: {
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 18,
    color: Assets.colors.mainText,
    letterSpacing: -0.2
  },

  title: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.4,
    paddingVertical: 12
  },

  value: {
    marginRight: 10,
    textAlign: 'right',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2
  },

  occasionItemHolder: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#F4F6F6',
    borderRadius: 6,
    marginTop: 10,
    borderWidth: 1,
    borderColor: '#F4F6F6',
    height: 44
  },

  occasionSelectedHolder: {
    backgroundColor: 'white',
    borderColor: Assets.colors.appTheme
  },
  occasionItemContent: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    paddingLeft: 15
  },
  occasionItemIcon: {
    position: 'absolute',
    right: 15,
    alignSelf: 'center'
  }
})

export default Filters
