import React from 'react'
import {
  View,
  Image,
  TouchableOpacity,
  FlatList,
  Animated,
  StyleSheet,
  Dimensions,
  Text as RNText,
  Platform,
  DeviceEventEmitter
} from 'react-native'

import StarRating from 'react-native-star-rating'
import PageControl from 'react-native-page-control'
import { AsyncImage, BackButton, Button, Section, CollapsibleView } from '@Components'
import { NavigationProps } from '@Types'
import {
  UserProfile,
  Item,
  CalendarParam,
  CheckoutParam,
  ContactListerParam,
  ChatParam,
  RequestSizeParam
} from '@Models'
import { Navigator } from '@Navigation'
import { Api, FirebaseHelper } from '@Services'
import { LayoutAnimations, Formater, Device, Constants, AppConfig, DateTime, Utils, Validator } from '@Utils'
import { Text, Row, Icon } from 'rn-components'
import { StoreState, ItemSummaryActions, FavoritesActions, ItemListActions } from '@ReduxManager'
import { connect } from 'react-redux'
import Assets from '@Assets'
import branch, { BranchEvent } from 'react-native-branch'
import { Alert } from 'rn-notifier'

const WindowWidth = Dimensions.get('window').width
const ImageRatio = 1008 / 761
const isIPhoneX = Device.isIphoneX()
const isAndroid = Device.isAndroid()

const TOP_OFFSET = isIPhoneX ? 35 : isAndroid ? 20 : 10
const HEADER_HEIGHT = 54 + TOP_OFFSET
const SCROLL_DISTANCE = HEADER_HEIGHT

type Props = {
  navigation: NavigationProps
  userProfile: UserProfile
}

type State = {
  showCalendar: boolean
  imageIndex: number
  statusPending: boolean
  isValidForAvailabilityCheck: boolean
  itemDetails: Item
  additional_fee: any
  rental_id: 0
  scrollY: Animated.Value
  recentChatStatus: string | null
  markedDates: string[]
  animatedBottom: Animated.Value
  onItemAdded: (item) => void
  isMultiRental: boolean
  selectedSizeId: number
  availableSizes: Array<number>
}

class Details extends React.Component<Props, State> {
  _scrollViewRef = React.createRef<any>()
  _timeoutLoading = null

  constructor(props: Props) {
    super(props)

    const itemDetails = this.props.navigation.getParam('item', null)
    const onItemAdded = this.props.navigation.getParam('onItemAdded', null)
    const isMultiRental = typeof onItemAdded === 'function'

    this.state = {
      showCalendar: false,
      imageIndex: 0,
      statusPending: true,
      isValidForAvailabilityCheck: false,
      itemDetails,
      additional_fee: null,
      rental_id: 0,
      scrollY: new Animated.Value(0),
      animatedBottom: new Animated.Value(0),
      recentChatStatus: null,
      markedDates: [],
      onItemAdded,
      isMultiRental,
      selectedSizeId: -1,
      availableSizes: []
    }

    LayoutAnimations.enableAndroidLayoutAnimation()
  }

  componentWillMount() {
    Navigator.showLoading()
    const { itemDetails, selectedSizeId, availableSizes } = this.state

    const { userProfile } = this.props
    let newItemDetails: any = null
    let additional_fee: any = null
    let sizeId = selectedSizeId
    let sizes = availableSizes

    const id = itemDetails.id

    Api.getItemDetails<any>([id])
      .then((response: any) => {
        const { code, message, items } = response
        if (code !== 200 || !Array.isArray(items) || items.length == 0) throw Error(message)
        return response.items[0]
      })
      .then(itemDetails => {
        newItemDetails = itemDetails

        return Api.getItemAdditionalFee<any>([id])
      })
      .then(response => {
        const { code, message, items } = response
        if (code !== 200 || !Array.isArray(items) || items.length == 0) throw message
        additional_fee = items[0]

        newItemDetails.additional_fee = additional_fee

        sizes = Utils.getSafeSizeIds(newItemDetails)
        if (sizes.length) {
          sizeId = sizes[0]
        }

        return true
      })
      .then(() => {
        return this._checkForStatus(userProfile, newItemDetails)
      })
      .then(() => {
        Navigator.hideLoading()

        console.log('will mount', itemDetails)

        this.setState(
          { additional_fee, itemDetails: newItemDetails, selectedSizeId: sizeId, availableSizes: sizes },
          () => {
            Animated.spring(this.state.animatedBottom, {
              toValue: 1,
              useNativeDriver: true,
              friction: 9,
              tension: 10
            }).start()
          }
        )
      })
      .catch(error => {
        console.warn('***** error', error)
        Navigator.hideLoading()
      })
  }

  componentWillUnmount() {
    this._timeoutLoading && clearTimeout(this._timeoutLoading)
  }

  _checkForStatus = (userProfile: UserProfile, itemDetails: any) => {
    return new Promise((resolve, reject) => {
      FirebaseHelper.getItemDetail(userProfile, itemDetails)
        .then(snapshoot => {
          console.log("snapshoot.size", snapshoot)

          // let conversations: any[] = []
          // snapshoot.docs.forEach(doc => {
          //   const data = doc.data()
          //   conversations.push(data)
          // })
          // const data = conversations[0]
          // if (data.order.status == "request") {
          //   if (snapshoot.size === 0) {
          //     snapshoot.size = 1
          //   }
          // }
          console.log("this.state", this.state)
          if (snapshoot.size === 0) {
            this.setState({
              isValidForAvailabilityCheck: true,
              statusPending: false
            })
            resolve()
          } else {
            let conversations: any[] = []
            snapshoot.docs.forEach(doc => {
              const data = doc.data()
              conversations.push(data)
            })

            conversations.sort((chat1, chat2) => {
              if (chat1.createdAt > chat2.createdAt) return -1
              if (chat1.createdAt < chat2.createdAt) return 1
            })

            const data = conversations[0]
            if (data.order.status == "request") {
              if (data.status != "accepted") {
                data.status = "accepted"
              }
            }
            const status = data.status
            const rental_id = data.rental_id

            if (rental_id === null || rental_id === undefined) {
              console.warn('(Item Detail-158) Invalid Rental Id! Please contact support.')
              reject('Invalid Rental Id! Please contact support.')
            }

            if (
              status === 'inquiry' ||
              status === 'returned' ||
              status === 'reviewed' ||
              status === 'rental_complete' ||
              status === 'cancelled' ||
              status === 'declined' ||
              status === 'completed' ||
              status === 'auto_cancel'
            ) {
              this.setState({
                isValidForAvailabilityCheck: true,
                statusPending: false,
                recentChatStatus: status,
                rental_id
              })
            } else {
              this.setState({
                isValidForAvailabilityCheck: false,
                statusPending: false,
                rental_id: data.rental_id,
                recentChatStatus: status
              })
            }
            resolve()
          }
        })
        .catch(error => {
          reject(error)
        })
    })
  }

  _didTapOptionButton = (type: number) => {
    const { additional_fee, isValidForAvailabilityCheck, itemDetails } = this.state
    if (type == 1) {
      if (!isValidForAvailabilityCheck) {
        return
      }
      this._showAvailabilityCalendar()
    } else {
      const param: ContactListerParam = {
        items: [itemDetails],
        additionalFee: additional_fee
      }

      Navigator.navTo('ContactLister', { param })
    }
  }

  _showAvailabilityCalendar = () => {
    const {
      additional_fee,
      itemDetails,
      isValidForAvailabilityCheck,
      markedDates,
      isMultiRental,
      onItemAdded,
      selectedSizeId,
      availableSizes
    } = this.state

    console.log('***** availableSizes', availableSizes)
    const multiSize = availableSizes.length > 1

    if (isMultiRental) {
      const handleAddMore = item => {
        Navigator.back()
        Navigator.back()
        console.log('***** item', item)
        onItemAdded(item)
      }

      if (multiSize) {
        const requestSizeParam: RequestSizeParam = {
          fromMultiItemScreen: true,
          item: itemDetails,
          onSizeSelected: selectedSizeId => {
            const newItem = { ...itemDetails, size_id: selectedSizeId }
            console.log('**** selectedSizeId', selectedSizeId)
            Navigator.back()
            handleAddMore(newItem)
          }
        }
        Navigator.navTo('RequestSize', { param: requestSizeParam })
      } else {
        handleAddMore(itemDetails)
      }

      return
    }
    if (isValidForAvailabilityCheck) {
      const handleSelectDates = selectedSizeId => {
        const blockedDates = this._getBlockedDates()
        const selectedDates = Array.isArray(markedDates) && markedDates.length > 0 ? markedDates : [DateTime.format()]
        const { multi_week_discount, monthly_discount, auto_smart_pricing } = itemDetails
        itemDetails.size_id = selectedSizeId

        const param: CalendarParam = {
          mode: 'selecting',
          selectedDates,
          blockedDates,
          autoSmartPricing: auto_smart_pricing,
          monthlyDiscount: monthly_discount,
          multiWeekDiscount: multi_week_discount,
          onDateSelected: markedDates => {
            this.setState({ markedDates }, () => {
              const checkoutParam: CheckoutParam = {
                selectedDates: markedDates,
                items: [itemDetails],
                selectedSizeId
              }
              Navigator.showLoading()
              this._timeoutLoading = setTimeout(() => {
                Navigator.hideLoading()
                Navigator.navTo('Checkout', { param: checkoutParam })
              }, 1000)
            })
          }
        }
        Navigator.navTo('AvailabilityCalendar', { param })
      }

      console.log('**** multiSize', multiSize)

      if (multiSize) {
        const requestSizeParam: RequestSizeParam = {
          item: itemDetails,
          onSizeSelected: selectedSizeId => {
            handleSelectDates(selectedSizeId)
          }
        }
        Navigator.navTo('RequestSize', { param: requestSizeParam })
      } else {
        handleSelectDates(this.state.selectedSizeId)
      }
    } else {
      const { rental_id, itemDetails } = this.state
      if (rental_id === null || rental_id === undefined) {
        Navigator.showToast('Error', 'Invalid Rental Id! Please contact support.', 'Error')
      } else {
        const param: ChatParam = {
          item: itemDetails,
          rental_id: rental_id
        }

        Navigator.navTo('Chat', { param })
      }
    }
  }

  _getBlockedDates = () => {
    const { itemDetails } = this.state

    const unAvailableList = itemDetails && itemDetails.unavailable_dates ? itemDetails.unavailable_dates : []
    let blockedDates = []
    for (let index = 0; index < unAvailableList.length; ++index) {
      blockedDates.push(unAvailableList[index].date)
    }
    return blockedDates
  }

  _onTapReportReviewButton = (review: any) => {
    const { params } = this.props.navigation.state
    const fromReview = params ? params.fromReview : false

    Navigator.navTo('Reporting', { review: review, fromReview })
  }

  _onTapReportItemButton = () => {
    const { params } = this.props.navigation.state
    const fromReview = params ? params.fromReview : false

    Navigator.navTo('Reporting', {
      item: this.state.itemDetails,
      fromReview
    })
  }

  _onTapReadAllReviews = () => {
    Navigator.navTo('Reviews', { item: this.state.itemDetails })
  }

  _didTapPhotoButton = () => {
    const { itemDetails } = this.state
    if (!itemDetails || !itemDetails.photos) {
      return
    }

    const photosList = itemDetails.photos
    console.log('**** photosList', photosList)
    Navigator.navTo('PhotoViewer', {
      photos: photosList.map((image: any) => {
        return {
          photo: image.url
        }
      }),
      initialIndex: this.state.imageIndex,
      name: itemDetails.name,
      description: itemDetails.description
    })
  }

  _onTapFavoriteItem = () => {
    const { itemDetails } = this.state
    const { id, is_favorite, owner, name } = itemDetails
    if (is_favorite) {
      Navigator.showAlert(
        'Remove favorite item',
        'Are you sure you want to remove this favorite item?',
        () => null,
        () => {
          Navigator.showLoading()
          Api.deleteFavorites<any>(id)
            .then(response => {
              if (response.code !== 200) {
                throw Error(response.message || '(DT - 343) Internal Error')
              }

              ItemSummaryActions.updateItemSummaryFavorite(id, false)
              ItemListActions.updateItemListFavorite(id, false)
              FavoritesActions.deleteFavorite(id)

              return true
            })
            .then(() => {
              const newItemDetails = { ...itemDetails, is_favorite: false }
              this.setState({ itemDetails: newItemDetails })

              Navigator.hideLoading()
              Navigator.showToast('Success', 'Your favorite item has been removed successfully.', 'Success')

              DeviceEventEmitter.emit(Constants.NEED_UPDATE_FAVORITE_ITEM_EVENT)
            })
            .catch(error => {
              Navigator.hideLoading()
              Navigator.showToast('Error', error.message, 'Error')
            })
        },
        'CANCEL',
        'OK'
      )
    } else {
      Navigator.showLoading()
      Api.setFavorites(id)
        .then(response => {
          const { userProfile } = this.props
          const { user_name, lastname, firstname } = userProfile
          let username = ''
          if (user_name && user_name.length > 0) {
            username = user_name
          } else {
            username = firstname + ' ' + lastname
          }

          let postData = {
            type: 'like',
            item: id,
            toUser: owner.id,
            fromUserName: username,
            text: username + ' just liked your rental ' + name
          }

          ItemSummaryActions.updateItemSummaryFavorite(id, true)
          ItemListActions.updateItemListFavorite(id, true)

          return Promise.all([Api.getFavorites(), FirebaseHelper.pushHistories(postData)])
        })
        .then((response: any) => {
          console.log('Set favoruite success', response)

          const favorites = response[0] && response[0].code === 200 ? response[0].favorites : null
          if (favorites) {
            FavoritesActions.saveFavorites(favorites)
          }

          const newItemDetails = { ...itemDetails, is_favorite: true }
          this.setState({ itemDetails: newItemDetails })

          Navigator.hideLoading()
          Navigator.showToast('Success', 'Add favorite successfully', 'Success')
          DeviceEventEmitter.emit(Constants.NEED_UPDATE_FAVORITE_ITEM_EVENT)
        })
        .catch(error => {
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
        })
    }
  }

  _onTapShareItem = async () => {
    Navigator.showLoading()
    const { itemDetails } = this.state
    const { userProfile } = this.props
    const { id, firstname, lastname, photo, email } = userProfile
    const url = `shareItem/${itemDetails.id}/fromUser/${userProfile.id}/`

    const validPhoto = itemDetails.photos.find(photo => Validator.isURL(photo.url))

    const buo = await branch.createBranchUniversalObject(url, {
      locallyIndex: true,
      canonicalUrl: '',
      title: itemDetails.name,
      contentDescription: itemDetails.description,
      contentImageUrl: validPhoto.url,
      contentMetadata: {
        customMetadata: {
          referrer: JSON.stringify(userProfile),
          item: JSON.stringify(itemDetails),
          type: 'Share Item'
        }
      }
    })

    new BranchEvent('RMW_Share_Item', buo, {
      sharedBy: id,
      firstname,
      lastname,
      email,
      itemId: itemDetails.id,
      itemName: itemDetails.name
    }).logEvent()

    const { channel, completed, error } = await buo.showShareSheet(
      {
        emailSubject: '',
        messageBody: '',
        messageHeader: 'Rent this look!'
      },
      {
        feature: 'share',
        channel: 'RMW_Share_Item'
      },
      {
        $desktop_url: 'https://itunes.apple.com/us/app/rent-my-wardrobe/id1375748670?ls=1&mt=8',
        $ios_deepview: 'branch_default'
      }
    )

    Navigator.hideLoading()
    if (error) {
      console.error('Error sharing via Branch: ' + error)
      return
    }

    console.log('Share to ' + channel + ' completed: ' + completed)
  }

  _showPublicProfile = (user: any) => {
    Navigator.navTo('PublicProfile', { user })
  }

  _isLoggedUsersItem() {
    const { itemDetails } = this.state
    const { userProfile } = this.props
    const isLoggedUsersItem = itemDetails && itemDetails.owner && userProfile.id === itemDetails.owner.id
    return isLoggedUsersItem
  }

  _keyExtractor = (item: any, index: number) => '' + (item ? item.id : index)

  _renderHeaderListEmpty = () => {
    return (
      <View style={styles.headerListEmpty}>
        <Image style={{ alignSelf: 'center' }} source={Assets.images.itemImagePlaceHolder} />
      </View>
    )
  }

  _renderHeader() {
    const { itemDetails } = this.state
    const photos = itemDetails && itemDetails.photos ? itemDetails.photos : []
    return (
      <View style={styles.detailsHeader}>
        <FlatList
          horizontal={true}
          pagingEnabled={true}
          style={{ flex: 1, backgroundColor: 'transparent' }}
          data={photos}
          renderItem={this._renderImageItem}
          keyExtractor={this._keyExtractor}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          onMomentumScrollEnd={this._handleImageScroll}
          ListEmptyComponent={this._renderHeaderListEmpty}
        />
        {this._renderHeaderTopBar()}
        {this._renderHeaderBottomBar()}
      </View>
    )
  }

  _handleImageScroll = (event: any) => {
    var e = event.nativeEvent
    var scrollViewWidth = e.layoutMeasurement.width
    var innerScrollViewWidth = e.contentSize.width
    var scrollX = e.contentOffset.x
    var totalHorizontalPages = Math.floor(innerScrollViewWidth / scrollViewWidth + 0.5)
    var currentHorizontalPage = Math.min(
      Math.max(Math.floor(scrollX / scrollViewWidth + 0.5) + 1, 0),
      totalHorizontalPages
    )
    this.setState({ imageIndex: currentHorizontalPage - 1 })
  }

  _renderImageItem = (item: any) => {
    const imageHeight = Constants.WINDOW_WIDTH * Constants.IMAGE_RATIO
    return (
      <View
        style={{
          width: Constants.WINDOW_WIDTH,
          height: imageHeight,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#F4F6F6'
        }}>
        <TouchableOpacity onPress={this._didTapPhotoButton} activeOpacity={0.7}>
          <AsyncImage
            source={{ uri: item.item ? item.item.url : '' }}
            width={WindowWidth}
            height={imageHeight}
            placeholderSource={Assets.images.itemImagePlaceHolder}
            fallbackImageSource={Assets.images.itemImagePlaceHolder}
          />
        </TouchableOpacity>
      </View>
    )
  }

  _renderHeaderTopBar = (title = '') => {
    const { itemDetails } = this.state
    if (!itemDetails) return null
    const isLoggedUsersItem = this._isLoggedUsersItem()

    return (
      <Row style={styles.headerTopView} alignHorizontal="space-between" alignVertical="center">
        <BackButton
          size={16}
          style={{ marginLeft: 15, backgroundColor: 'rgba(0,0,0,0.3)' }}
          iconStyle={{ height: 15, width: 15, tintColor: 'white' }}
        />

        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text ellipsizeMode="middle" numberOfLines={2} style={styles.title} text={title} />
        </View>

        <Row>
          <Icon
            onPress={this._onTapShareItem}
            style={{ marginRight: isLoggedUsersItem ? 0 : 10, backgroundColor: 'rgba(0,0,0,0.3)' }}
            size={16}
            iconSource={Assets.images.shareIcon}
            iconStyle={{ height: 15, width: 15, tintColor: 'white' }}
          />
          {isLoggedUsersItem == false && (
            <Icon
              onPress={this._onTapFavoriteItem}
              style={{ backgroundColor: itemDetails.is_favorite ? Assets.colors.appTheme : 'rgba(0,0,0,0.3)' }}
              size={16}
              iconSource={itemDetails.is_favorite ? Assets.images.heartIcon : Assets.images.heartOutline}
              iconStyle={{ height: 15, width: 15, tintColor: 'white' }}
            />
          )}
        </Row>
      </Row>
    )
  }

  _renderHeaderBottomBar = () => {
    const { itemDetails } = this.state
    if (!itemDetails) return null

    const { occasions } = itemDetails
    let categoryList = ''
    if (occasions && occasions.length > 0) {
      for (let index = 0; index < occasions.length; ++index) {
        categoryList = categoryList + Utils.getTitleWithValue('occasions', occasions[index]).toUpperCase()
        if (index < occasions.length - 1) {
          categoryList = categoryList + '\n'
        }
      }
    }

    const photos = itemDetails && itemDetails.photos ? itemDetails.photos : []
    return (
      <View style={styles.headerBottomView}>
        {/* <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "white",
            borderRadius: 6,
            minHeight: 22
          }}
        >
          <Text style={styles.categoryTitle}>{categoryList}</Text>
        </View> */}
        <View style={{ flex: 1 }} />
        <View style={{ justifyContent: 'flex-end', height: 10 }}>
          <PageControl
            numberOfPages={photos.length}
            currentPage={this.state.imageIndex}
            hidesForSinglePage
            pageIndicatorTintColor={Assets.colors.borderColor}
            currentPageIndicatorTintColor={Assets.colors.appTheme}
            indicatorStyle={{ borderRadius: 5, marginLeft: 3, marginRight: 3 }}
            currentIndicatorStyle={{
              borderRadius: 5,
              marginLeft: 3,
              marginRight: 3
            }}
            indicatorSize={{ width: 10, height: 10 }}
          />
        </View>
      </View>
    )
  }

  _renderContent = () => {
    const { itemDetails, isMultiRental } = this.state
    const ownerName = itemDetails && itemDetails.owner ? itemDetails.owner.name : ''
    const name = itemDetails ? itemDetails.name : ''
    const isLoggedUsersItem = this._isLoggedUsersItem()
    return (
      <View>
        <Text style={styles.itemTitle} text={name} />
        {this._renderOwnerDetails()}
        {this._renderItemDetails()}
        {this._renderItemDescription()}
        {this._renderCleaningPreference()}
        {this._renderAdditionalPrices()}
        {this._renderRefundPolicy()}
        {this._renderReview()}

        {!isLoggedUsersItem && isMultiRental === false && (
          <View>
            {this._renderOptionButton('Availability Calendar', Assets.images.calendar, 1)}
            {this._renderOptionButton('Contact ' + ownerName, Assets.images.user_comments_icon, 2)}
          </View>
        )}
        <View style={{ alignItems: 'center', marginHorizontal: 45, marginTop: 35, marginBottom: 10 }}>
          <Image source={Assets.images.tryOn} />
          <Text style={[styles.title, { marginTop: 10 }]} text="Try On" />
          <Text
            style={[styles.detailText, { textAlign: 'center', marginTop: 8 }]}
            text="Don’t worry about renting an item that doesn’t fit. Message your lender to try on the item before renting."
          />
          <Image style={{ marginTop: 30 }} source={Assets.images.apparel} />
          <Text style={[styles.title, { marginTop: 10 }]} text="Apparel Assurance" />
          <Text
            style={[styles.detailText, { textAlign: 'center', marginTop: 8 }]}
            text="We got you covered! We automatically include Apparel Assurance on each rental so you can enjoy your items worry free."
          />
          <Image style={{ marginTop: 30 }} source={Assets.images.directExchange} />
          <Text style={[styles.title, { marginTop: 10 }]} text="Direct Exchange" />
          <Text
            style={[styles.detailText, { textAlign: 'center', marginTop: 8 }]}
            text="No need to wait for shipping, arrange the exchange with your lender locally to receive your fashion sustainably faster."
          />
        </View>

        {!isLoggedUsersItem ? this._renderSecurity() : <View style={{ height: 20 }} />}
      </View>
    )
  }

  _renderOwnerDetails = () => {
    const { itemDetails } = this.state
    if (!itemDetails || !itemDetails.owner) {
      return null
    }

    const onPress = () => this._showPublicProfile(itemDetails.owner)

    return (
      <Row style={styles.userContainer} alignHorizontal="space-between" alignVertical="center" onPress={onPress}>
        <View>
          <Text style={styles.ownerName} text={`${itemDetails.owner.name}'s Closet`} />
          <Text style={styles.location} text={itemDetails.owner.locality ? itemDetails.owner.locality : ''} />
        </View>
        <AsyncImage
          style={{ marginRight: 15, borderRadius: 20, overflow: 'hidden' }}
          width={40}
          height={40}
          placeholderSource={Assets.images.defaultProfile}
          source={{ uri: itemDetails.owner ? itemDetails.owner.photo : '' }}
          fallbackImageSource={Assets.images.defaultProfile}
        />
      </Row>
    )
  }

  _renderItemDetails = () => {
    const { itemDetails } = this.state
    if (!itemDetails) return null

    const {
      rental_fee,
      embellishments,
      style_id,
      size_id,
      condition_id,
      fit_id,
      color_id,
      sleeve_id,
      hemline_id,
      neckline_id,
      retail_price
    } = itemDetails

    const rentalFee = Formater.formatMoney(rental_fee)
    const retailFee = Formater.formatMoney(retail_price)

    let embellishmentsList = ''
    if (embellishments && embellishments.length > 0) {
      for (let index = 0; index < embellishments.length; ++index) {
        embellishmentsList = embellishmentsList + Utils.getTitleWithValue('embellishments', embellishments[index])
        if (index < embellishments.length - 1) {
          embellishmentsList = embellishmentsList + ', '
        }
      }
    }

    const sizes = Utils.getSafeSizeIds(itemDetails)
    return (
      <View style={[styles.itemDetailsView, { marginTop: 10, marginBottom: 25 }]}>
        {this._renderSingleItemDetails('Style:', Utils.getTitleWithValue('styles', style_id))}
        {this._renderSingleItemDetails('Size:', Utils.combineItemsSize(sizes))}
        {this._renderSingleItemDetails('Condition:', Utils.getTitleWithValue('conditions', condition_id))}
        {this._renderSingleItemDetails('Fit:', Utils.getTitleWithValue('fitProfiles', fit_id))}
        {this._renderSingleItemDetails('Color:', Utils.getTitleWithValue('colors', color_id))}
        {this._renderSingleItemDetails('Sleeve:', Utils.getTitleWithValue('sleeves', sleeve_id))}
        {this._renderSingleItemDetails('Hemline:', Utils.getTitleWithValue('hemlines', hemline_id))}
        {this._renderSingleItemDetails('Embellishments:', embellishmentsList)}
        {this._renderSingleItemDetails('Neckline:', Utils.getTitleWithValue('necklines', neckline_id))}

        <Row style={{ marginTop: 8 }}>
          <Row>
            <Text style={[styles.detailCaptionText]} text="Retail Price:" />
            <Text style={styles.priceValueText} text={retailFee} />
            <View style={styles.lineThrough} />
          </Row>

          <View style={{ flex: 1 }} />
        </Row>

        <Row style={{ marginTop: 8 }}>
          <Text style={[styles.detailCaptionText, { color: Assets.colors.appTheme }]} text="Rental Price:" />
          <Text style={[styles.priceValueText, { color: Assets.colors.appTheme }]} text={rentalFee} />
        </Row>
      </View>
    )
  }

  _renderSingleItemDetails = (title: string, value: string) => {
    if (!value || value === '') {
      return null
    }

    return (
      <View style={[styles.itemDetailsRow, { marginTop: 8 }]}>
        <Text style={styles.detailCaptionText} text={title} />
        <Text style={styles.detailValueText} text={value} />
      </View>
    )
  }

  _renderItemDescription = () => {
    const description = this.state.itemDetails ? this.state.itemDetails.description : ''
    if(!description) return
    return (
      <CollapsibleView title="Description" hasTopDivider>
        <Text style={styles.detailText} text={description} />
      </CollapsibleView>
    )
  }

  _renderCleaningPreference() {
    const { itemDetails } = this.state
    if (!itemDetails || (!itemDetails.cleaning_option_id && itemDetails.cleaning_option_id !== 0)) {
      return null
    }
    return (
      <CollapsibleView title="Cleaning Preferences">
        <Text style={styles.detailText} text={Utils.getTitleWithValue('cleanings', itemDetails.cleaning_option_id)} />
      </CollapsibleView>
    )
  }

  _renderAdditionalPrices = () => {
    const { itemDetails, additional_fee } = this.state
    if (!itemDetails) {
      return null
    }

    const { cleaning_fee, replacement_fee } = itemDetails

    const cleaningFee = Formater.formatMoney(cleaning_fee)
    const replacementFee = Formater.formatMoney(replacement_fee)

    return (
      <CollapsibleView title="Additional Prices" paddingBottom={40}>
        <Row style={{ marginTop: 8 }}>
          <Text style={styles.detailCaptionText}>Cleaning Fee:</Text>
          <Text style={styles.priceValueText} text={cleaningFee} />
        </Row>
        {additional_fee && additional_fee.insurance > 0 && (
          <Row style={{ marginTop: 8 }}>
            <Text style={styles.detailCaptionText} text="Apparel Assurance:" />
            <Text style={styles.priceValueText} text={Formater.formatMoney(additional_fee.insurance)} />
          </Row>
        )}
        {additional_fee && additional_fee.service_fee > 0 && (
          <Row style={{ marginTop: 8 }}>
            <Text style={styles.detailCaptionText} text="Service Fee:" />
            <Text style={styles.priceValueText} text={Formater.formatMoney(additional_fee.service_fee)} />
          </Row>
        )}
        {itemDetails.replacement_fee > 0 && (
          <Row style={{ marginTop: 8 }}>
            <Text style={styles.detailCaptionText} text="Replacement Costs:" />
            <Text style={styles.priceValueText} text={replacementFee} />
          </Row>
        )}
      </CollapsibleView>
    )
  }

  _onViewPolicy = () => {
    Navigator.navTo('WebsiteView1', { url: AppConfig.refund_url })
  }

  _renderRefundPolicy = () => {
    const { itemDetails } = this.state
    if (!itemDetails || !itemDetails.refund_option_id) {
      return null
    }

    var refundPolicy = ''
    if (this.state.itemDetails.refund_option_id == 2) {
      refundPolicy = 'No refund is offered you will be charged the total rental if you cancel.'
    } else if (this.state.itemDetails.refund_option_id == 1) {
      refundPolicy =
        'Refund will be issued 100% up to 3 days before your scheduled rental. If the rental is canceled within 3 days you will be charged a 20% cancellation fee.'
    } else {
      return null
    }

    return (
      <CollapsibleView title="Refund Policy">
        <Text style={styles.detailText}>
          {refundPolicy}
          <RNText onPress={this._onViewPolicy} style={styles.linkText}>
            {' '}
            Learn More
          </RNText>
        </Text>
      </CollapsibleView>
    )
  }

  _renderOptionButton = (title: string, image: any, type: number) => {
    const marginTop = type == 1 ? 25 : 10
    var disableStyle = null
    if (type == 1 && !this.state.isValidForAvailabilityCheck) {
      disableStyle = { opacity: 0.5 }
    }
    const onPress = () => this._didTapOptionButton(type)
    return (
      <View style={[styles.itemDetailsView, { marginTop: marginTop }]}>
        <TouchableOpacity onPress={onPress}>
          <View style={styles.optionButtonContainer}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <Text style={[styles.optionButtonTitle, disableStyle]} text={title} />
            </View>
            <Image source={image} style={[{ marginRight: 15, tintColor: '#989FA2' }, disableStyle]} />
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  _renderReview() {
    const { itemDetails } = this.state
    if (!itemDetails) return null

    console.log('**** itemDetails', itemDetails)
    if (!itemDetails.review || itemDetails.avg_review === 0 || itemDetails.reviews_count === 0) {
      return null
    }

    return (
      <CollapsibleView
        title="Reviews"
        RightSubComponent={
          <Row alignVertical="center">
            <StarRating
              disabled={true}
              maxStars={5}
              rating={itemDetails.avg_review}
              emptyStar={Assets.images.ratingEmptyStar}
              fullStar={Assets.images.ratingFullStar}
              halfStar={Assets.images.ratingHalfStar}
              halfStarEnabled={true}
              starSize={12}
            />
            <Text
              style={[styles.linkText, { fontFamily: Assets.fonts.display.bold, marginLeft: 4 }]}
              text={itemDetails.reviews_count}
            />
          </Row>
        }>
        <View style={{ marginTop: 15 }}>
          <Text numberOfLines={3} ellipsizeMode="tail" style={styles.mainText}>
            {itemDetails.review.description}
          </Text>
          <Row style={{ marginTop: 15 }}>
            <TouchableOpacity activeOpacity={0.7} onPress={() => this._showPublicProfile(itemDetails.review.reviewer)}>
              <AsyncImage
                placeholderSource={Assets.images.defaultProfile}
                source={{ uri: itemDetails.review.reviewer.photo }}
                width={40}
                height={40}
                style={{ borderRadius: 20 }}
                fallbackImageSource={Assets.images.defaultProfile}
              />
            </TouchableOpacity>
            <View style={{ marginLeft: 10 }}>
              <Text
                numberOfLines={1}
                style={[styles.title, { marginHorizontal: 0 }]}
                text={itemDetails.review.reviewer.name}
              />
              <Text
                numberOfLines={1}
                style={styles.detailText}
                text={DateTime.moment(itemDetails.review.created_at).format('MMM-DD-YYYY')}
              />
            </View>
          </Row>
          <TouchableOpacity activeOpacity={0.7} onPress={this._onTapReadAllReviews}>
            <Row alignVertical="center" alignHorizontal="space-between" style={styles.readAllReviews}>
              <Text style={styles.mainText} text="Read All Reviews" />
              <Image
                source={Assets.images.categoryDetailsArrow}
                style={{ tintColor: Assets.colors.borderColor, marginLeft: 10 }}
              />
            </Row>
          </TouchableOpacity>
        </View>
      </CollapsibleView>
    )
  }

  _renderSecurity() {
    return (
      <View style={styles.itemDetailsView}>
        <View style={styles.seperator} />
        <Row style={{ marginTop: 15 }} alignHorizontal="space-between" alignVertical="flex-start">
          <Text
            style={[styles.detailText, { flex: 1, marginTop: 0 }]}
            text="To protect your payment, never transfer money or communicate outside of the Rent My Wardrobe app."
          />
          <View style={styles.securityIcon}>
            <Image source={Assets.images.security_icon} />
          </View>
        </Row>
        <Section
          style={{
            backgroundColor: 'white',
            borderColor: Assets.colors.borderColor,
            marginVertical: 20,
            borderWidth: 1
          }}
          titleStyle={{ color: Assets.colors.mainText }}
          title="Report Item"
          iconSource={Assets.images.categoryDetailsArrow}
          iconStyle={{ tintColor: Assets.colors.borderColor }}
          onPress={this._onTapReportItemButton}
        />
      </View>
    )
  }

  _renderFooter = () => {
    const { itemDetails, additional_fee, isValidForAvailabilityCheck, statusPending } = this.state
    if (!itemDetails || !additional_fee) return null
    const { rental_fee, cleaning_fee, retail_price } = itemDetails
    let totalFee = rental_fee
    if (cleaning_fee) {
      totalFee += cleaning_fee
    }

    const { insurance, service_fee } = additional_fee
    if (additional_fee) {
      totalFee += insurance + service_fee
    }

    const totalFeeString = Formater.formatMoney(totalFee)
    const retailFeeString = Formater.formatMoney(retail_price)
    const isLoggedUsersItem = this._isLoggedUsersItem()
    const animationStyle = {
      opacity: this.state.animatedBottom,
      transform: [
        {
          translateY: this.state.animatedBottom.interpolate({
            inputRange: [0, 1],
            outputRange: [Constants.WINDOW_HEIGHT + 160, 0]
          })
        }
      ]
    }

    const { isMultiRental } = this.state
    console.log("isMultiRental", isMultiRental)
    console.log("isValidForAvailabilityCheck", isValidForAvailabilityCheck)
    
    return (
      <Animated.View style={[styles.bottomRow, animationStyle]}>
        <View style={{ flex: 1, paddingLeft: 23 }}>
          <Text style={styles.itemRetailPriceText} text="Total Cost" />
          <Text style={styles.itemPriceText} text={totalFeeString} />
        </View>

        {!statusPending && !isLoggedUsersItem && (
          <Button
            style={{ flex: 1, marginHorizontal: 15 }}
            onPress={this._showAvailabilityCalendar}
            text={isValidForAvailabilityCheck ? (isMultiRental ? 'Add to Rental' : 'Request Rental') : 'Contact'}
          />
        )}
      </Animated.View>
    )
  }

  render() {
    const translateY = this.state.scrollY.interpolate({
      inputRange: [0, SCROLL_DISTANCE],
      outputRange: [-SCROLL_DISTANCE, 0],
      extrapolate: 'clamp'
    })

    const opacity = this.state.scrollY.interpolate({
      inputRange: [0, SCROLL_DISTANCE],
      outputRange: [0, 1],
      extrapolate: 'clamp'
    })

    const animationStyle = {
      transform: [{ translateY }],
      opacity
    }
    const name = this.state.itemDetails ? this.state.itemDetails.name : ''
    return (
      <View style={StyleSheet.absoluteFill}>
        <Animated.ScrollView
          ref={this._scrollViewRef}
          style={{ flex: 1 }}
          automaticallyAdjustContentInsets={false}
          scrollEventThrottle={16}
          onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }], {
            useNativeDriver: true
          })}
          showsVerticalScrollIndicator={false}>
          {this._renderHeader()}
          {this._renderContent()}
        </Animated.ScrollView>
        <Animated.View style={[styles.animation, { height: SCROLL_DISTANCE }, animationStyle]}>
          {this._renderHeaderTopBar(name)}
        </Animated.View>
        {this._renderFooter()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  detailsHeader: {
    width: WindowWidth,
    height: WindowWidth * ImageRatio,
    backgroundColor: '#F4F6F6'
  },
  title: {
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.display.bold,
    marginHorizontal: 10
  },
  headerTopView: {
    position: 'absolute',
    top: 0,
    height: HEADER_HEIGHT,
    left: 0,
    width: Device.width,
    paddingRight: 15,
    paddingTop: TOP_OFFSET
  },
  linkText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.appTheme,
    lineHeight: 18,
    letterSpacing: -0.2
  },
  headerBottomView: {
    position: 'absolute',
    bottom: 15,
    left: 15,
    width: WindowWidth - 30,
    minHeight: 22,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    alignItems: 'flex-end',
    justifyContent: 'flex-end'
  },

  categoryTitle: {
    marginLeft: 6,
    marginRight: 6,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.heavy,
    fontSize: 12,
    color: Assets.colors.mainText,
    letterSpacing: 1,
    lineHeight: 18
  },

  headerOptionContainer: {
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  },

  itemTitle: {
    marginLeft: 15,
    marginRight: 15,
    marginTop: 15,
    marginBottom: 22,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 24,
    color: Assets.colors.mainText,
    letterSpacing: -0.2,
    lineHeight: 28
  },

  location: {
    marginLeft: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 12,
    color: Assets.colors.textLight,
    letterSpacing: -0.2
  },

  ownerName: {
    marginLeft: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    marginTop: 5,
    letterSpacing: -0.4
  },

  profileView: {
    height: 40,
    width: 40,
    borderRadius: 20,
    overflow: 'hidden'
  },

  itemDetailsView: {
    marginTop: 25,
    marginLeft: 15,
    marginRight: 15,
    flexDirection: 'column'
  },

  itemDetailsRow: {
    flex: 1,
    flexDirection: 'row'
  },

  itemDetailsColumn: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row'
  },

  detailCaptionText: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2,
    lineHeight: 18
  },

  detailValueText: {
    marginLeft: 5,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: Assets.colors.mainText,
    lineHeight: 18
  },

  priceValueText: {
    marginLeft: 5,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: Assets.colors.mainText,
    lineHeight: 18
  },

  subHeadingText: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.medium,
    fontSize: 12,
    color: Assets.colors.mainText,
    lineHeight: 28
  },

  seperator: {
    height: 1,
    backgroundColor: '#E7EAEB'
  },

  detailText: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2,
    lineHeight: 18
  },

  optionButtonTitle: {
    marginLeft: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    color: Assets.colors.mainText,
    lineHeight: 20
  },

  itemPriceText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 20,
    color: Assets.colors.appTheme,
    lineHeight: 22
  },

  itemRetailPriceText: {
    fontFamily: Assets.fonts.text.bold,
    fontSize: 14,
    letterSpacing: -0.2,
    lineHeight: 18,
    color: Assets.colors.mainText
  },

  animation: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: 'white',
    alignItems: 'center',
    ...Platform.select({
      android: {
        elevation: 4
      },
      ios: {
        zIndex: 2,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        shadowRadius: 6
      }
    })
  },
  bottomRow: {
    backgroundColor: Assets.colors.inputBg,
    paddingTop: 15,
    paddingBottom: isIPhoneX ? 25 : 15,
    flexDirection: 'row',
    alignItems: 'center'
  },
  reviewContainer: {
    backgroundColor: '#F4F6F6',
    borderRadius: 6
    // marginHorizontal: 15,
    // paddingHorizontal: 15
  },
  optionButtonContainer: {
    flex: 1,
    height: 44,
    flexDirection: 'row',
    backgroundColor: '#F4F6F6',
    alignItems: 'center',
    borderRadius: 6
  },
  securityIcon: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerListEmpty: {
    alignItems: 'center',
    justifyContent: 'center',
    width: WindowWidth
  },
  userContainer: {
    marginHorizontal: 15,
    borderRadius: 6,
    backgroundColor: 'rgba(240, 97, 130, 0.05)',
    paddingVertical: 15,
    borderColor: 'rgba(240, 97, 130, 0.2)',
    borderWidth: 1
  },
  mainText: {
    fontSize: 16,
    fontFamily: Assets.fonts.display.regular,
    color: Assets.colors.mainText
  },
  readAllReviews: {
    marginTop: 20,
    paddingHorizontal: 10,
    borderRadius: 6,
    borderWidth: 1,
    borderColor: Assets.colors.borderColorLight,
    height: 46
  },
  lineThrough: {
    position: 'absolute',
    left: 0,
    right: 0,
    alignSelf: 'center',
    height: 1,
    backgroundColor: Assets.colors.appTheme
  }
})

const mapStateToProps = (state: StoreState) => {
  return {
    userProfile: state.userProfile
  }
}

export default connect(mapStateToProps)(Details)
