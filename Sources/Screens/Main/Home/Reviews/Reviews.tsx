import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Api } from '@Services'
import { NavigationProps } from '@Types'
import { Navigator } from '@Navigation'
import { Header, Text } from 'rn-components'
import { BackButton, FlatList } from '@Components'
import { Item, UserProfile } from '@Models'
import Assets from '@Assets'
import StarRating from 'react-native-star-rating'
import ReviewItem from './ReviewItem'

type Props = {
  navigation: NavigationProps
}

type State = {
  reviewSummary: any
  dataSource: any[]
  pageIndex: number
  item: Item
  user: UserProfile
}
class Reviews extends React.Component<Props, State> {
  constructor(props) {
    super(props)
    const { params } = this.props.navigation.state
    const { item = null, user = null } = params
    this.state = {
      reviewSummary: null,
      dataSource: [],
      pageIndex: 1,
      item,
      user
    }
  }

  componentWillMount() {
    this._refreshData(true)
  }

  _refreshData(resetFlag) {
    const { user, item, pageIndex } = this.state
    Navigator.showLoading()
    if (resetFlag) {
      if (user) {
        const param = {
          user_id: user.id,
          page: resetFlag ? 1 : pageIndex + 1,
          per_page: 20
        }
        Api.getUserReviewSummary<any>(param)
          .then(response => {
            if (response.code !== 200) {
              throw Error(response.message || '(RV - 55) Internal Error')
            }

            this.setState({ reviewSummary: response.data }, () => {
              this._downloadReviewList(resetFlag)
            })
          })
          .catch(error => {
            Navigator.showToast('Error', error.message, 'Error')
            Navigator.hideLoading()
          })
        return
      }

      if (item) {
        const param = {
          item_id: item.id,
          page: resetFlag ? 1 : pageIndex + 1,
          per_page: 20
        }
        Api.getItemReviewSummary<any>(param)
          .then(response => {
            if (response.code !== 200) {
              throw Error(response.message || '(RV - 55) Internal Error')
            }

            this.setState({ reviewSummary: response.data }, () => {
              this._downloadReviewList(resetFlag)
            })
          })
          .catch(error => {
            Navigator.showToast('Error', error.message, 'Error')
            Navigator.hideLoading()
          })
        return
      }
    } else {
      this._downloadReviewList(resetFlag)
    }
  }

  _downloadReviewList(resetFlag) {
    const { user, item, pageIndex, dataSource } = this.state

    if (user) {
      const param = {
        user_id: user.id,
        page: resetFlag ? 1 : pageIndex + 1,
        per_page: 20
      }

      Api.getItemRenterRatings<any>(param)
        .then(response => {
          if (response.code !== 200) {
            throw Error(response.message || '(RV - 55) Internal Error')
          }

          const reviewsList = response.ratings ? response.ratings : []
          let fullReviewsList = []
          if (param.page > 1) {
            fullReviewsList = dataSource.slice()
          }
          fullReviewsList = fullReviewsList.concat(reviewsList)

          this.setState({ dataSource: fullReviewsList, pageIndex: param.page })
          Navigator.hideLoading()
        })
        .catch(error => {
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
        })
    } else {
      const param = {
        item_id: item.id,
        page: resetFlag ? 1 : pageIndex + 1,
        per_page: 20
      }

      Api.getItemListerRatings<any>(param)
        .then(response => {
          if (response.code !== 200) {
            throw Error(response.message || '(RV - 55) Internal Error')
          }

          const reviewsList = response.ratings ? response.ratings : []
          let fullReviewsList = []
          if (param.page > 1) {
            fullReviewsList = this.state.dataSource.slice()
          }
          fullReviewsList = fullReviewsList.concat(reviewsList)

          this.setState({ dataSource: fullReviewsList, pageIndex: param.page })
          Navigator.hideLoading()
        })
        .catch(error => {
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
        })
    }
  }

  _onTapReportButton = review => {
    Navigator.navTo('Reporting', { review, fromReview: true })
  }

  _onRefresh = () => {
    return new Promise<boolean>((resolve, reject) => {
      this._refreshData(true)
      resolve(true)
    })
  }

  _onLoadMore = () => {
    return new Promise<boolean>((resolve, reject) => {
      this._refreshData(false)
      resolve(true)
    })
  }

  render() {
    return (
      <View style={{ backgroundColor: Assets.colors.componentBg }}>
        <Header
          statusBarProps={{ barStyle: 'dark-content' }}
          title=""
          LeftComponent={<BackButton iconSource={Assets.images.closeIcon} />}
        />
        <FlatList
          data={this.state.dataSource}
          ListHeaderComponent={this._renderReviewHeader}
          renderItem={this._renderReviewItem}
          onRefresh={this._onRefresh}
          onLoadMore={this._onLoadMore}
          showsVerticalScrollIndicator={false}
        />
      </View>
    )
  }

  _renderItemReview() {
    return (
      <View style={styles.ratingHolderView}>
        {this._renderAverageRating('Accurate Description', this.state.reviewSummary.accuracy)}
        {this._renderAverageRating('Communication & Promptness', this.state.reviewSummary.communication)}
        {this._renderAverageRating(
          'Ease of Scheduling Fitting & Exchange',
          this.state.reviewSummary.ease_of_scheduling
        )}
        {this._renderAverageRating('Fitting & Exchange Experience', this.state.reviewSummary.exchange)}
        {this._renderAverageRating('Reasonable Pricing', this.state.reviewSummary.reasonable_pricing)}
      </View>
    )
  }

  _renderUserReview() {
    return (
      <View style={styles.ratingHolderView}>
        {this._renderAverageRating('Accuracy of Profile', this.state.reviewSummary.accuracy)}
        {this._renderAverageRating('Communication & Promptness', this.state.reviewSummary.communication)}
        {this._renderAverageRating('Fitting/Exchange Punctuality', this.state.reviewSummary.exchange)}
        {this._renderAverageRating('Returned in proper condition', this.state.reviewSummary.return_condition)}
        {this._renderAverageRating('Returned on time', this.state.reviewSummary.return_on_time)}
      </View>
    )
  }

  _renderReviewHeader = () => {
    const { reviewSummary, item, user } = this.state
    if (!reviewSummary) {
      return null
    }
    const { count } = reviewSummary
    return (
      <View>
        <Text numberOfLines={1} style={styles.reviewCount} text={`${count} Reviews`} />
        <View style={styles.averageRatingView}>
          <StarRating
            disabled={true}
            maxStars={5}
            rating={this.state.reviewSummary.avg_rating}
            emptyStar={Assets.images.ratingEmptyStar}
            fullStar={Assets.images.ratingFullStar}
            halfStar={Assets.images.ratingHalfStar}
            halfStarEnabled={true}
            starSize={20}
          />
          <Text style={styles.averageText} text="Avg." />
        </View>
        {user != null && this._renderUserReview()}
        {item != null && this._renderItemReview()}
        <View style={styles.seperator} />
      </View>
    )
  }

  _renderAverageRating = (text, value) => {
    return (
      <View style={{ flex: 1, flexDirection: 'row', marginBottom: 5, height: 18 }}>
        <View style={{ flex: 1 }}>
          <Text numberOfLines={1} style={styles.ratingCaption} text={text} />
        </View>
        <View style={{ width: 85 }}>
          <StarRating
            disabled={true}
            maxStars={5}
            rating={value}
            emptyStar={Assets.images.ratingEmptyStar}
            fullStar={Assets.images.ratingFullStar}
            halfStar={Assets.images.ratingHalfStar}
            halfStarEnabled={true}
            starSize={18}
          />
        </View>
      </View>
    )
  }

  _showPublicProfile = user => {
    Navigator.navTo('PublicProfile', { user })
  }

  _renderReviewItem = ({ item, index }) => {
    return (
      <ReviewItem
        review={item}
        onTapReportButton={this._onTapReportButton}
        onTapProfileButton={this._showPublicProfile}
      />
    )
  }
}

const styles = StyleSheet.create({
  reviewCount: {
    marginLeft: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 36,
    color: Assets.colors.mainText,
    letterSpacing: -0.2,
    lineHeight: 40
  },

  averageRatingView: {
    flexDirection: 'row',
    marginLeft: 15,
    marginTop: 10,
    alignItems: 'center'
  },

  averageText: {
    marginLeft: 10,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.2,
    lineHeight: 22
  },

  ratingHolderView: {
    marginLeft: 15,
    marginRight: 15,
    marginTop: 15,
    marginBottom: 15
  },

  ratingCaption: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2,
    lineHeight: 18
  },

  seperator: {
    flex: 1,
    height: 1,
    backgroundColor: '#E7EAEB'
  }
})

export default Reviews
