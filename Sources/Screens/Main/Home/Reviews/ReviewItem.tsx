import React from 'react'
import { View, TouchableOpacity, Platform, StyleSheet } from 'react-native'
import { Text } from 'rn-components'
import { AsyncImage } from '@Components'
import { DateTime } from '@Utils'
import Assets from '@Assets'

const ReviewItem = props => {
  const { review } = props
  const createAt = DateTime.moment(review.created_at).format('MMM YYYY')
  const { onTapProfileButton } = props
  const onPress = () => onTapProfileButton(review.reviewer)
  return (
    <View style={styles.reviewHolder}>
      <Text style={styles.reviewText}>{review.description}</Text>
      <View style={styles.userView}>
        <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
          <View style={styles.profileView}>
            <AsyncImage
              placeholderSource={Assets.images.defaultProfile}
              source={{ uri: review.reviewer.photo }}
              width={40}
              height={40}
              fallbackImageSource={Assets.images.defaultProfile}
            />
          </View>
        </TouchableOpacity>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
            <Text numberOfLines={1} style={styles.userName}>
              {review.reviewer.name}
            </Text>
          </TouchableOpacity>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Text style={[styles.dateDetails, { marginTop: 3, flex: 1 }]} text={createAt} />
            <TouchableOpacity activeOpacity={0.7} onPress={() => props.onTapReportButton(review)}>
              <View style={{ width: 80 }}>
                <Text style={[styles.dateDetails, { marginTop: 3, textAlign: 'right' }]} text="Report" />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      {!props.hideSeperator && <View style={[styles.seperator, { marginTop: 20 }]} />}
    </View>
  )
}

const styles = StyleSheet.create({
  closeButton: {
    marginLeft: 6,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.appTheme,
    marginTop: Platform.OS === 'android' ? 0 : 3,
    letterSpacing: -0.4
  },

  reviewCount: {
    marginLeft: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 36,
    color: Assets.colors.mainText,
    letterSpacing: -0.2,
    lineHeight: 40
  },

  averageRatingView: {
    flexDirection: 'row',
    marginLeft: 15,
    marginTop: 10,
    alignItems: 'center'
  },

  averageText: {
    marginLeft: 10,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.2,
    lineHeight: 22
  },

  ratingHolderView: {
    marginLeft: 15,
    marginRight: 15,
    marginTop: 15,
    marginBottom: 15
  },

  ratingCaption: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2,
    lineHeight: 18
  },

  seperator: {
    flex: 1,
    height: 1,
    backgroundColor: '#E7EAEB'
  },

  reviewHolder: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 20
  },

  reviewText: {
    marginLeft: 15,
    marginRight: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.4,
    lineHeight: 22
  },

  userView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 15,
    marginRight: 15,
    marginTop: 15
  },

  profileView: {
    height: 40,
    width: 40,
    borderRadius: 20,
    overflow: 'hidden'
  },

  userName: {
    marginLeft: 10,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    color: Assets.colors.mainText,
    lineHeight: 20
  },

  dateDetails: {
    marginLeft: 10,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 12,
    color: Assets.colors.textLight,
    letterSpacing: -0.2,
    lineHeight: 16
  }
})

export default ReviewItem
