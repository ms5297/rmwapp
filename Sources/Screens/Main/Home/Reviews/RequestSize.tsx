import React from 'react'
import { View, StyleSheet, Animated, Platform, FlatList, Image } from 'react-native'
import { Text, Header, Row } from 'rn-components'
import { Constants, Formater, Device, Utils } from '@Utils'
import { NavigationProps } from '@Types'
import { Item, RequestSizeParam } from '@Models'
import { Button, RentalItem, BackButton } from '@Components'

import Assets from '@Assets'
import Styles from '@Styles'

type Props = {
  navigation: NavigationProps
}

type State = RequestSizeParam & {
  animatedBottom: Animated.Value
  selectedSizeId: number
}

class RequestSize extends React.Component<Props, State> {
  constructor(props) {
    super(props)
    const param: RequestSizeParam = this.props.navigation.getParam('param', {})

    const data = Utils.getSafeSizeIds(param.item)
    const selectedSizeId = data.length ? data[0] : -1
    this.state = {
      ...param,
      animatedBottom: new Animated.Value(0),
      selectedSizeId
    }
  }

  componentDidMount() {
    const { item, animatedBottom } = this.state
    if (item) {
      Animated.spring(animatedBottom, {
        toValue: 1,
        useNativeDriver: true,
        friction: 9,
        tension: 10,
        delay: 250
      }).start()
    }
  }

  handleRequestSize = () => {
    const { onSizeSelected, selectedSizeId } = this.state
    if (typeof onSizeSelected === 'function') {
      onSizeSelected(selectedSizeId)
    }
  }

  onSizeSelected = sizeId => {
    console.log('***** onSizeSelected', sizeId)
    this.setState({ selectedSizeId: sizeId })
  }

  renderItem = ({ item, index }) => {
    const selected = this.state.selectedSizeId === item
    console.log('**** item', item)
    const size = Utils.getConfigWithValue('sizes', item)
    const onPress = () => this.onSizeSelected(item)
    return (
      <Row onPress={onPress} style={[styles.itemHolder, selected && styles.itemHolderSelected]}>
        <View style={styles.itemContent}>
          <Text style={styles.itemTitle} text={size.title} />
          {selected && <Image style={styles.itemIcon} source={Assets.images.tickmark_icon} />}
        </View>
      </Row>
    )
  }

  render() {
    const { item, selectedSizeId, fromMultiItemScreen } = this.state
    if (!item) return null
    const { rental_fee, cleaning_fee, retail_price, additional_fee } = item
    let totalFee = rental_fee
    if (cleaning_fee) {
      totalFee += cleaning_fee
    }

    const { insurance, service_fee } = additional_fee
    if (additional_fee) {
      totalFee += insurance + service_fee
    }

    const totalFeeString = Formater.formatMoney(totalFee)

    const animationStyle = {
      opacity: this.state.animatedBottom,
      transform: [
        {
          translateY: this.state.animatedBottom.interpolate({
            inputRange: [0, 1],
            outputRange: [Constants.WINDOW_HEIGHT + 160, 0]
          })
        }
      ]
    }

    const data = Utils.getSafeSizeIds(item)

    console.log('**** data', data)
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header
          titleStyle={Styles.headerTitle}
          title="Request Size"
          LeftComponent={
            <BackButton iconSource={fromMultiItemScreen ? Assets.images.closeIcon : Assets.images.backIcon} />
          }
        />

        <FlatList
          contentContainerStyle={{ marginTop: 20 }}
          data={data}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index.toString()}
          extraData={selectedSizeId}
          ListHeaderComponent={
            <View>
              <RentalItem {...item} size_id={selectedSizeId} />
              <Text style={styles.title} text="Choose a size" />
            </View>
          }
          ListFooterComponent={<View style={styles.separator} />}
        />
        <Animated.View style={[styles.bottomRow, animationStyle]}>
          <View style={{ flex: 1, paddingLeft: 23 }}>
            <Text style={styles.itemRetailPriceText} text="Total Cost" />
            <Text style={styles.itemPriceText} text={totalFeeString} />
          </View>

          <Button style={{ flex: 1, marginHorizontal: 15 }} onPress={this.handleRequestSize} text="Request Size" />
        </Animated.View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  itemPriceText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 20,
    color: Assets.colors.appTheme,
    lineHeight: 22
  },

  itemRetailPriceText: {
    fontFamily: Assets.fonts.text.bold,
    fontSize: 14,
    letterSpacing: -0.2,
    lineHeight: 18,
    color: Assets.colors.mainText
  },

  animation: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: 'white',
    alignItems: 'center',
    ...Platform.select({
      android: {
        elevation: 4
      },
      ios: {
        zIndex: 2,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        shadowRadius: 6
      }
    })
  },
  bottomRow: {
    backgroundColor: Assets.colors.inputBg,
    paddingTop: 15,
    paddingBottom: Device.isIphoneX() ? 25 : 15,
    flexDirection: 'row',
    alignItems: 'center'
  },
  separator: {
    height: 1,
    backgroundColor: Assets.colors.borderColor,
    marginVertical: 25
  },
  itemTitle: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.4,
    paddingVertical: 12
  },
  itemHolder: {
    flexDirection: 'row',
    backgroundColor: '#F4F6F6',
    borderRadius: 6,
    marginTop: 10,
    borderWidth: 1,
    borderColor: '#F4F6F6',
    height: 44,
    marginHorizontal: 16
  },
  itemHolderSelected: {
    backgroundColor: 'white',
    borderColor: Assets.colors.appTheme
  },
  itemContent: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    paddingLeft: 15
  },
  itemIcon: {
    position: 'absolute',
    right: 15,
    alignSelf: 'center'
  },
  title: {
    marginTop: 25,
    marginBottom: 10,
    fontSize: 16,
    fontFamily: Assets.fonts.display.bold,
    color: Assets.colors.mainText,
    marginHorizontal: 16
  }
})

export default RequestSize
