import React from 'react'
import { TouchableOpacity, View, Image, StyleSheet } from 'react-native'
import { Text, Header } from 'rn-components'
import { NavigationProps } from '@Types'
import { BackButton } from '@Components'
import { Formater } from '@Utils'
import { Navigator } from '@Navigation'
import { PromotionCodeParam } from '@Models'
import Assets from '@Assets'
import Styles from '@Styles'

type Props = {
  navigation: NavigationProps
}
type State = PromotionCodeParam

class PromotionCodeInfo extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    const param: PromotionCodeParam = this.props.navigation.getParam('param')
    this.state = param
  }

  _onViewDetail = () => Navigator.navTo('PromotionCodeDetail', { param: this.state })

  _onRemoveCode = () => {
    const { onRemoveCode } = this.state
    if (typeof onRemoveCode === 'function') {
      onRemoveCode()
      Navigator.back()
    }
  }

  render() {
    const { promoCode } = this.state
    const { name, amount_off, percent_off, metadata } = promoCode
    let amountText = ''
    if (amount_off !== null) {
      amountText = Formater.formatMoney(amount_off / 100) + ''
    } else {
      amountText = percent_off + '%'
    }
    const description = (metadata && metadata.description) || ''

    return (
      <View style={StyleSheet.absoluteFill}>
        <Header title="Promotional Code" titleStyle={Styles.headerTitle} LeftComponent={<BackButton />} />
        <View style={{ marginTop: 40, marginHorizontal: 20 }}>
          <TouchableOpacity activeOpacity={0.7} onPress={this._onViewDetail}>
            <View style={styles.itemHolderView}>
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <Text style={styles.itemTitle}>{name}</Text>
              </View>
              <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'flex-end' }} />
              <Text style={styles.itemDescription}>{amountText}</Text>
              <Image
                source={Assets.images.categoryDetailsArrow}
                style={{ tintColor: Assets.colors.borderColor, marginRight: 15 }}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  itemDescription: {
    marginRight: 10,
    textAlign: 'right',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2
  },
  itemHolderView: {
    height: 44,
    flexDirection: 'row',
    backgroundColor: '#F4F6F6',
    alignItems: 'center',
    borderRadius: 6
  },
  itemTitle: {
    marginLeft: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.4
  }
})

export default PromotionCodeInfo
