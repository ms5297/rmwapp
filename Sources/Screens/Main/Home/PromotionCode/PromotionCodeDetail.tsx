import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Header, Text } from 'rn-components'
import { BackButton, Button } from '@Components'
import { Navigator } from '@Navigation'
import { NavigationProps } from '@Types'
import { Formater, Constants } from '@Utils'
import { PromotionCodeParam } from '@Models'
import Assets from '@Assets'
import Styles from '@Styles'

type Props = {
  navigation: NavigationProps
}

type State = PromotionCodeParam
class PromotionCodeDetail extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    const param: PromotionCodeParam = this.props.navigation.getParam('param')
    this.state = param
  }
  _onPress = () => {
    const { onRemoveCode } = this.state
    if (typeof onRemoveCode === 'function') {
      onRemoveCode()
      Navigator.back()
      Navigator.back()
    }
  }

  render() {
    const { promoCode } = this.state
    const { name, amount_off, percent_off, metadata } = promoCode
    let amountText = ''
    if (amount_off !== null) {
      amountText = Formater.formatMoney(amount_off / 100)
    } else {
      amountText = percent_off + '%'
    }
    const description = (metadata && metadata.description) || ''
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header title="Promotional Code" titleStyle={Styles.headerTitle} LeftComponent={<BackButton />} />
        <View style={{ marginTop: 60, paddingLeft: 15 }}>
          <Text style={styles.title} text={name} />
          <Text style={styles.money} text={amountText} />
          <Text style={styles.description} text={description} />
          <Button
            text="Remove Code"
            style={styles.removebutton}
            textStyle={styles.removeText}
            onPress={this._onPress}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    fontFamily: Assets.fonts.text.bold,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.2
  },
  money: {
    marginTop: 10,
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2
  },
  description: {
    marginTop: 10,
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2
  },
  removebutton: {
    alignSelf: 'flex-end',
    marginTop: Constants.WINDOW_HEIGHT / 4 - 40,
    backgroundColor: 'transparent'
  },
  removeText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.appTheme,
    lineHeight: 18,
    paddingHorizontal: 20
  }
})

export default PromotionCodeDetail
