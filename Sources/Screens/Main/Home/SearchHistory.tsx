import React from 'react'
import { RecyclerListView, DataProvider, LayoutProvider } from 'recyclerlistview'
import { SearchHistories } from '@Models'
import { Constants } from '@Utils'
import { Keyboard, Image, View, TouchableOpacity } from 'react-native'
import { Row, StyleSheet, Text } from 'rn-components'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler'
import Assets from '@Assets'

type Props = {
  searchHistories: SearchHistories
  onTapSearchHistoryItem: (item: string) => void
}

type State = {
  dataProvider: DataProvider
}

class SearchHistory extends React.PureComponent<Props, State> {
  _layoutProvider: LayoutProvider = null

  constructor(props) {
    super(props)

    const { searchHistories } = this.props

    const dataRow: DataProvider = new DataProvider((r1, r2) => r1 !== r2)
    const dataProvider = dataRow.cloneWithRows(searchHistories.keywords)

    this._layoutProvider = new LayoutProvider(
      index => 1,
      (type, dim) => {
        dim.width = Constants.WINDOW_WIDTH
        dim.height = 40
      }
    )

    this.state = {
      dataProvider
    }
  }
  _dismissKeyboard = () => Keyboard.dismiss()

  _onTapSearchHistoryItem = (item: string) => {
    Keyboard.dismiss()
    this.props.onTapSearchHistoryItem(item)
  }

  _renderSearchListHeader = () => {
    const { searchHistories } = this.props
    if (Array.isArray(searchHistories.keywords) && searchHistories.keywords.length > 0) {
      return (
        <Row style={styles.searchHistoryHeader}>
          <Text style={styles.searchHeaderText} text="RECENT SEARCHES" />
        </Row>
      )
    }
    return null
  }

  _renderSearchItem = (type, data, index) => {
    const onPress = () => this._onTapSearchHistoryItem(data)
    return (
      <Row style={styles.renderSearchItem} alignHorizontal="space-between" alignVertical="center" onPress={onPress}>
        <Row style={{ marginVertical: 8 }}>
          <Image source={Assets.images.searchHistoryIcon} style={{ marginHorizontal: 15 }} />
          <Text numberOfLines={1} style={styles.searchHistoryText} text={data} />
        </Row>
        <Image source={Assets.images.searchIcon} style={{ marginRight: 15, tintColor: Assets.colors.borderColor }} />
      </Row>
    )
  }

  render() {
    const { searchHistories } = this.props
    const { dataProvider } = this.state
    if (Array.isArray(searchHistories.keywords) && searchHistories.keywords.length > 0) {
      return (
        <View style={{ flex: 1 }}>
          {this._renderSearchListHeader()}
          <RecyclerListView
            dataProvider={dataProvider}
            scrollViewProps={{
              keyboardShouldPersistTaps: 'handled',
              showsVerticalScrollIndicator: false,
              showsHorizontalScrollIndicator: false,
              decelerationRate: 'normal'
            }}
            layoutProvider={this._layoutProvider}
            rowRenderer={this._renderSearchItem}
          />
        </View>
      )
    }
    return <TouchableOpacity activeOpacity={1} style={{ flex: 1 }} onPress={this._dismissKeyboard} />
  }
}

const styles = StyleSheet.create({
  searchHeaderText: {
    marginLeft: 15,
    marginTop: 28,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 12,
    color: Assets.colors.textLight,
    letterSpacing: 0.2
  },
  searchHistoryHeader: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: Assets.colors.seperatorColor,
    paddingBottom: 5
  },
  renderSearchItem: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: Assets.colors.borderColor
  },
  searchHistoryText: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.4
  }
})

export default SearchHistory
