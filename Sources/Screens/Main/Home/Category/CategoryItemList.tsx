import React from 'react'
import { View, Image, Keyboard, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { Row, TextInput, Icon, Text, StatusBar, KeyboardSpacer, Col } from 'rn-components'
import { NavigationProps } from '@Types'
import { Navigator } from '@Navigation'
import { BackButton, FlatList, Button, Section, SectionLocation } from '@Components'
import { StoreState, SearchHistoryActions, ItemListActions, getStore, FilterActions } from '@ReduxManager'
import { UserProfile, SearchHistories, ItemList, PlacePickerParam, EnumConfigKey, FilterState, FilterParamsModel } from '@Models'
import { LayoutAnimations, Device, Utils } from '@Utils'
import { Api } from '@Services'
import Assets from '@Assets'
import CategoryGrid from './CategoryGrid'
import SearchResult from '../SearchResult'

type Props = {
  navigation: NavigationProps
  itemList: ItemList
  userProfile: UserProfile
  searchHistories: SearchHistories
  filters: FilterState
}

type State = {
  searchType: 'None' | 'Searching' | 'Done'
  refreshing: boolean
}
class CategoryItemList extends React.Component<Props, State> {
  _searchInputRef = React.createRef<TextInput>()
  _currentLocality = ''
  state: State = {
    searchType: 'None',
    refreshing: false
  }
  _focusListener: any = null

  componentWillMount() {
    const occasionId = this.props.navigation.getParam('occasionId', null)
    const itemTypeId = this.props.navigation.getParam('itemTypeId', null)
    if (occasionId !== null || itemTypeId !== null) {
      console.log('**** fetch', occasionId)
      Navigator.showLoading()
      this.setState({ refreshing: true })
      this._onRefresh()
        .then(() => {
          Navigator.hideLoading()
          this.setState({ refreshing: false })
        })
        .catch(() => {
          Navigator.hideLoading()
          this.setState({ refreshing: false })
        })
    } else {
      let { filters, userProfile } = getStore().getState()
      const { locality } = userProfile
      this._currentLocality = locality
      if (filters.itemList.filters.locality) {
        this._currentLocality = filters.itemList.filters.locality
      }
    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    this._focusListener = navigation.addListener('didFocus', () => {
      const latestUserProfile = getStore().getState().userProfile
      const latestMissingFields = Utils.checkAndUpdateProfile(latestUserProfile).missingFields
      const _missingFieldsLength = Object.values(latestMissingFields).filter(Boolean).length
      if (_missingFieldsLength > 0) {
        Navigator.showToast("Incomplete size profile", "Update your sizing profile for better search results.", "Warn")
      }

    });
  }

  componentWillUnmount() {
    this._focusListener.remove();
  }

  _onTapFilterButton = () => {
    const keyword = this._searchInputRef.current.getText()

    const onGoBack = this.props.navigation.getParam('onGoBack')

    Navigator.navTo('Filters', { shouldSetDefault: true, keyword, onGoBack: onGoBack })
  }

  _onTapItemCell = (item: any) => {
    Keyboard.dismiss()
    Navigator.navTo('Details', { item })
  }

  _onTapSearchCancelButton = () => {
    Keyboard.dismiss()
    LayoutAnimations.setLayoutAnimation(LayoutAnimations.Spring)
    this.setState({ searchType: 'None' })
    this._searchInputRef.current.clearText()
  }

  _onSearchHistoryFocused = () => {
    LayoutAnimations.setLayoutAnimation(LayoutAnimations.Spring)
    this.setState({ searchType: 'Searching' })
  }

  _searchForItems = (text: string, reset: boolean = false, occasionId = -1) => {
    Navigator.showLoading()
    this.setState({ refreshing: true })
    let { filters } = getStore().getState()
    filters.itemList.filters['keyword'] = text

    if (occasionId !== -1) {
      filters.itemList.filters['occasion'] = [occasionId]
    }

    if (reset) {
      filters.itemList.page = 1
    }

    Api.getItemList<any>(filters.itemList)
      .then(response => {
        const itemList = response && response.code === 200 && Array.isArray(response.items) ? response.items : []

        if (itemList.length > 0) {
          filters.itemList.page += 1
        }

        // filters.itemList.filters["keyword"] = "";
        text.trim().length && SearchHistoryActions.addSearchHistory(text)
        ItemListActions.saveItemList(itemList)
        FilterActions.updateItemListFilterParam(filters.itemList)
        this.setState({ searchType: 'Done' })
        Navigator.hideLoading()
        this.setState({ refreshing: false })
      })
      .catch(error => {
        Navigator.hideLoading()
        this.setState({ refreshing: false })
        Navigator.showToast('Error', error.message, 'Error')
        this.setState({ searchType: 'Done' })
      })
  }

  _searchForItemsFromTextInput = () => {
    const text = this._searchInputRef.current.getText()
    this._searchForItems(text, true)
  }

  _renderHeader = () => {
    const marginTop = Device.isIphoneX() ? 34 : 20
    return (
      <View style={[styles.headerView, { marginTop }]}>
        <StatusBar barStyle="dark-content" />

        <Row alignVertical="center">
          <BackButton />
          <TextInput
            ref={this._searchInputRef}
            LeftComponent={
              <Image
                style={{ marginHorizontal: 8, tintColor: Assets.colors.textLight }}
                source={Assets.images.searchIcon}
              />
            }
            placeholder="Search"
            style={styles.inputView}
            inputStyle={styles.inputText}
            underlineWidth={0}
            placeholderTextColor={Assets.colors.mainText}
            onFocus={this._onSearchHistoryFocused}
            returnKeyType="search"
            onSubmitEditing={this._searchForItemsFromTextInput}
          />
          <Icon
            iconStyle={{ tintColor: Assets.colors.appTheme }}
            iconSource={Assets.images.filterIcon}
            onPress={this._onTapFilterButton}
          />
        </Row>
      </View>
    )
  }

  // vinayak on 07 nov 2019
  _renderSelectionView = (title: string, value: string, type: EnumConfigKey) => {
    let rightIcon = Assets.images.categoryDetailsArrow
    let titleExtensionStyle = null
    let valueExtensionStyle = null
    let { filters } = getStore().getState()

    if (type == 'date') {
      rightIcon = Assets.images.calendar
      if (value !== '') {
        titleExtensionStyle = { color: Assets.colors.appTheme }
        title = value
        value = ''
      }
    } else {
      if (value !== 'All' && value !== 'Choose a location') {
        valueExtensionStyle = { color: Assets.colors.appTheme }
      }
    }

    const onSelected = () => {
      if (type === 'market') {
        const param: PlacePickerParam = {
          title,
          onLocationSelected: (currentLocation, currentAddress) => {
            let categoryFilterToUpdate = { ...filters.categories }
            categoryFilterToUpdate['filters'] = { 'location': currentLocation }
            // let itemListToUpdate: FilterParamsModel = {
            //   filters: {}
            // }
            let filtersToUpdate = { ...filters.itemList.filters }
            filtersToUpdate['location'] = currentLocation
            filtersToUpdate['locality'] = currentAddress

            let itemListToUpdate = { ...filters.itemList }
            itemListToUpdate['filters'] = filtersToUpdate
            // itemListToUpdate['filters']['location'] = currentLocation
            // itemListToUpdate['filters']['locality'] = currentAddress
            // itemListToUpdate['filters'] = { 'location': currentLocation }
            // itemListToUpdate['filters'] = { 'locality': currentAddress }

            // FilterActions.updateCategoryFilterParam(filters.categories)
            // FilterActions.updateItemListFilterParam(filters.itemList)
            FilterActions.updateCategoryFilterParam(categoryFilterToUpdate)
            FilterActions.updateItemListFilterParam(itemListToUpdate)
            Navigator.showLoading()
            this.setState({ refreshing: true })
            this._onRefresh()
              .then(() => {
                Navigator.hideLoading()
                this.setState({ refreshing: false })
              })
              .catch(() => {
                Navigator.hideLoading()
                this.setState({ refreshing: false })
              })
            const onGoBack = this.props.navigation.getParam('onGoBack')
            if (typeof onGoBack === 'function') {
              onGoBack()
            }
          }
        }
        Navigator.navTo('PlacePicker', { param })
      }
    }

    return (
      <SectionLocation
        style={{ marginBottom: 0, borderRadius: 6, marginHorizontal: 13, height: 28 }}
        title={title}
        titleStyle={titleExtensionStyle}
        valueStyle={valueExtensionStyle}
        value={value}
        iconSource={rightIcon}
        onPress={onSelected}
      />
    )
  }

  // vinayak on 07 nov 2019

  /* **************************** Search History **************************** */
  _onTapSearchHistoryItem = (item: string) => {
    Keyboard.dismiss()
    this._searchForItems(item, true)
  }

  _onSearch = () => {
    Keyboard.dismiss()
    this._searchForItemsFromTextInput()
  }

  _renderSearchHistory = () => {
    const { searchHistories } = this.props
    if (Array.isArray(searchHistories.keywords) && searchHistories.keywords.length > 0) {
      return (
        <FlatList
          data={searchHistories.keywords}
          keyboardShouldPersistTaps="handled"
          ListHeaderComponent={this._renderSearchListHeader}
          renderItem={this._renderSearchItem}
          showsVerticalScrollIndicator={false}
        />
      )
    }
    return <View style={{ flex: 1 }} />
  }

  _renderSearchListHeader = () => {
    const { searchHistories } = this.props
    if (Array.isArray(searchHistories.keywords) && searchHistories.keywords.length > 0) {
      return (
        <Row style={styles.searchHistoryHeader}>
          <Text style={styles.searchHeaderText} text="RECENT SEARCHES" />
        </Row>
      )
    }
    return null
  }

  _renderSearchItem = ({ item }: any) => {
    const onPress = () => this._onTapSearchHistoryItem(item)
    return (
      <Row style={styles.renderSearchItem} alignHorizontal="space-between" alignVertical="center" onPress={onPress}>
        <Row>
          <Image source={Assets.images.searchHistoryIcon} style={{ marginHorizontal: 15 }} />
          <Text numberOfLines={1} style={styles.searchHistoryText} text={item} />
        </Row>
        <Image source={Assets.images.searchIcon} style={{ marginRight: 15, tintColor: Assets.colors.borderColor }} />
      </Row>
    )
  }

  /* **************************** Main **************************** */
  _onRefresh = () => {
    return new Promise<boolean>((resolve, reject) => {
      let { filters, userProfile } = getStore().getState()
      const { locality, latitude, longitude } = userProfile
      this._currentLocality = locality
      let location = typeof longitude === 'number' && typeof latitude === 'number' ? { longitude, latitude } : null

      const occasionId = this.props.navigation.getParam('occasionId', null)
      if (occasionId !== null && occasionId > 0) {
        let filtersToUpdate = { ...filters.itemList.filters }
        filtersToUpdate['occasion'] = [occasionId]
        filters.itemList['filters'] = filtersToUpdate
        // filters.itemList.filters['occasion'] = [occasionId]
      }
      else {
        if (filters.itemList.filters['occasion']) {
          filters.itemList.filters['occasion'] = []
        }
      }
      const itemTypeId = this.props.navigation.getParam('itemTypeId', null)
      if (itemTypeId !== null && itemTypeId > 0) {
        let filtersToUpdate = { ...filters.itemList.filters }
        filtersToUpdate['item_type'] = itemTypeId
        filters.itemList['filters'] = filtersToUpdate
      }
      else {
        if (filters.itemList.filters['item_type']) {
          filters.itemList.filters['item_type'] = null
        }
      }

      if (!filters.itemList.filters.location) {
        if (location) {
          filters.itemList.filters['location'] = location
          filters.itemList.filters['locality'] = locality
        }

      }
      // console.warn(filters.itemList.filters.location_range);

      if (!filters.itemList.filters.location_range || filters.itemList.filters.location_range == 100) {
        const rangeInMile = 100 * 1609.34;
        filters.itemList.filters['location_range'] = rangeInMile
      }


      filters.itemList.page = 1

      let getItemListFunction: any = null
      if (occasionId == -2 || (itemTypeId !== null && itemTypeId > 0)) {
        getItemListFunction = Api.getItemList_V3<any>(filters.itemList, true)
      } else {
        getItemListFunction = Api.getItemList<any>(filters.itemList)
      }

      // Api.getItemList(filters.itemList)
      getItemListFunction
        .then((response: any) => {
          const itemList = response && response.code === 200 && Array.isArray(response.items) ? response.items : []
          if (itemList.length) {
            filters.itemList.page = 2
          }

          ItemListActions.saveItemList(itemList)
          FilterActions.updateItemListFilterParam(filters.itemList)
          resolve(false)
        })
        .catch(error => {
          resolve(false)
          console.warn('**** error', error)
        })
    })
  }

  _onLoadMore = () => {
    return new Promise<boolean>((resolve, reject) => {
      let { filters } = getStore().getState()
      const occasionId = this.props.navigation.getParam('occasionId', null)
      if (occasionId !== null) {
        filters.itemList.filters['occasion'] = [occasionId]
      }
      const itemTypeId = this.props.navigation.getParam('itemTypeId', null)
      if (itemTypeId !== null && itemTypeId > 0) {
        filters.itemList.filters['item_type'] = itemTypeId
      }
      let getItemListFunction: any = null
      if (occasionId == -2 || (itemTypeId !== null && itemTypeId > 0)) {
        getItemListFunction = Api.getItemList_V3<any>(filters.itemList, true)
      } else {
        getItemListFunction = Api.getItemList<any>(filters.itemList)
      }
      // Api.getItemList(filters.itemList)
      getItemListFunction
        .then((response: any) => {
          const itemList = response && response.code === 200 && Array.isArray(response.items) ? response.items : []
          if (itemList.length > 0) {
            filters.itemList.page += 1
          }

          ItemListActions.appendtemList(itemList)
          FilterActions.updateItemListFilterParam(filters.itemList)
          resolve(true)
        })
        .catch(error => {
          resolve(true)
          console.warn('**** error', error)
        })
    })
  }

  _renderItemList = () => {
    const { itemList } = this.props

    return (
      <SearchResult
        style={{ marginBottom: 60 }}
        data={itemList}
        emptyViewProps={{
          title: 'Oh, No items available',
          info:
            'There are currently no items available using that search. Please change your search filters or location and try again.',
          buttonText: 'Adjust the Search',
          onPress: this._onTapFilterButton
        }}
        itemCellProps={{
          editable: false
        }}
        onRefresh={this._onRefresh}
        onLoadMore={this._onLoadMore}
        refreshing={this.state.refreshing}
      />
    )

    // return (
    //   <CategoryGrid
    //     style={{ marginBottom: 60 }}
    //     data={itemList}
    //     emptyViewProps={{
    //       title: "Oh, No items available",
    //       info:
    //         "There are currently no items available using that search. Please change your search filters or location and try again.",
    //       buttonText: "Adjust the Search",
    //       onPress: this._onTapFilterButton
    //     }}
    //     onRefresh={this._onRefresh}
    //     onLoadMore={this._onLoadMore}
    //   />
    // );
  }

  render() {
    const { searchType } = this.state
    return (
      <View style={[{ backgroundColor: Assets.colors.componentBg }, StyleSheet.absoluteFill]}>
        {this._renderHeader()}
        {searchType !== 'Searching' && this._renderSelectionView('Location', this.props.filters.itemList.filters.locality, 'market')}
        {searchType !== 'Searching' && this._renderItemList()}
        {searchType === 'Searching' && this._renderSearchHistory()}
        {searchType === 'Searching' && <Button onPress={this._onSearch} style={{ borderRadius: 0 }} text="SEARCH" />}
        <KeyboardSpacer />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  headerView: {
    height: 56,
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 0
  },

  inputView: {
    flex: 1,
    height: 36,
    backgroundColor: Assets.colors.headerSearchBarBg,
    borderRadius: 6
  },
  inputText: {
    marginRight: 10,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    marginTop: 3,
    height: 36,
    paddingVertical: 0
  },

  filterButton: {
    height: 36,
    marginTop: 20,
    width: 70,
    alignItems: 'center'
  },

  searchHeaderText: {
    marginLeft: 15,
    marginTop: 28,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 12,
    color: Assets.colors.textLight,
    letterSpacing: 0.2
  },

  searchHistoryText: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.4
  },

  renderSearchItem: {
    paddingVertical: 8,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: Assets.colors.borderColor
  },
  searchHistoryHeader: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: Assets.colors.seperatorColor,
    paddingBottom: 5
  }
})

const mapStateToProps = (state: StoreState) => {
  return {
    itemList: state.itemList,
    userProfile: state.userProfile,
    searchHistories: state.searchHistories,
    filters: state.filters
  }
}

export default connect(mapStateToProps)(CategoryItemList)
