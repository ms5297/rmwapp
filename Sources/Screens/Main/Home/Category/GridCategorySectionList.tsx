import React from 'react'
import { View, DeviceEventEmitter, StyleSheet, Image } from 'react-native'
import { Navigator, getRouteName } from '@Navigation'
import { DataProvider, RecyclerListView, LayoutProvider } from 'recyclerlistview'
import { Constants } from '@Utils'
import { ItemCell, EmptyView, RefreshControl } from '@Components'
import { ItemsSummary, Item, ItemTypes, } from '@Models'
import CategoryHeader from './CategoryHeader'

import isEqual from 'lodash.isequal'
import { NavigationProps } from '@Types'
import { withNavigation } from 'react-navigation'
import { Text } from 'rn-components'
import Assets from '@Assets'
import { TouchableOpacity } from 'react-native-gesture-handler'

type Props = {
    data: ItemTypes
    onRefresh?: () => Promise<boolean>
    onTapFilterButton?: () => void
    refreshing: boolean
}

type State = {
    refreshing: boolean
    loadingMore: boolean
    dataProvider: Array<ItemsSummary>
    dataChildProvider: Array<Item>
    ids: Array<number>
}

class GridCategorySectionList extends React.PureComponent<Props, State> {
    _layoutProvider: LayoutProvider = null
    _layoutChildProvider: LayoutProvider = null
    _subscription = null
    _currentScrollViewPositionY = 0
    _scrollView = null

    constructor(props) {
        super(props)

        const { data } = this.props

        const dataRow: any = new DataProvider((r1, r2) => r1 !== r2)
        const dataProvider = dataRow.cloneWithRows(data)

        let dataChildProvider = []
        let ids = []
        // data.forEach(item => {
        //     const provider: any = new DataProvider((r1, r2) => r1 !== r2)
        //     const childProvider = provider.cloneWithRows(item.items)
        //     dataChildProvider.push(childProvider)
        //     ids.push(item.id)
        // })

        this._layoutProvider = new LayoutProvider(
            index => 1,
            (type, dim) => {
                dim.width = Constants.WINDOW_WIDTH
                dim.height = Constants.TOTAL_CELL_HEIGHT
            }
        )

        this._layoutChildProvider = new LayoutProvider(
            index => 1,
            (type, dim) => {
                // dim.width = Constants.CELL_WIDTH - 70
                // dim.height = Constants.TOTAL_CELL_HEIGHT
                dim.width = 114
                dim.height = 114
            }
        )

        this.state = {
            refreshing: false,
            loadingMore: false,
            dataProvider,
            dataChildProvider,
            ids
        }
    }


    componentDidMount() {
        // this._subscription = DeviceEventEmitter.addListener(
        //     Constants.NEED_UPDATE_FAVORITE_ITEM_EVENT,
        //     this._handleUpdateFavoriteItems
        // )
    }

    componentWillUnmount() {
        // DeviceEventEmitter.removeListener(Constants.NEED_UPDATE_FAVORITE_ITEM_EVENT, this._handleUpdateFavoriteItems)
    }



    // _onRefresh = () => {
    //     const { onRefresh } = this.props
    //     if (typeof onRefresh === 'function') {
    //         if (this.state.refreshing) return
    //         Navigator.showLoading()
    //         this.setState({ refreshing: true })
    //         onRefresh()
    //             .then(() => {
    //                 this.setState({ refreshing: false })
    //             })
    //             .catch(error => {
    //                 this.setState({ refreshing: false })
    //             })
    //     }
    // }

    _onTapCategoryHeader = (occasionId: number) => {
        // Navigator.navTo('CategoryItemList', { occasionId, onGoBack: this._onRefresh })
    }
    onPress(value) {
        const itemTypeId = value
        Navigator.navTo('CategoryItemList', { itemTypeId, onGoBack: this.props.onRefresh })
    }
    _keyExtrator = (item, index) => index.toString()

    _renderItem = (type, data, index) => {
        // const dataChildProvider: any = this.state.dataChildProvider
        // const provider = data[index]
        const provider = data
        // const occasionId = this.state.ids[index]
        // const headerHeight = Constants.TOTAL_CELL_HEIGHT - Constants.CELL_HEIGHT
        const scrollEnabled = data.length > 2

        return (
            <View style={{ width: Constants.WINDOW_WIDTH, height: Constants.TOTAL_CELL_HEIGHT }}>
                <Text>Sub</Text>
                {/* <CategoryHeader
                    style={{ height: headerHeight }}
                    occasionId={occasionId}
                    onTapCategoryHeader={this._onTapCategoryHeader}
                /> */}
                {/* <RecyclerListView
                    isHorizontal
                    scrollViewProps={{
                        showsVerticalScrollIndicator: false,
                        showsHorizontalScrollIndicator: false,
                        contentContainerStyle: { marginLeft: 13 },
                        scrollEnabled,
                        decelerationRate: 'normal'
                    }}
                    dataProvider={provider}
                    layoutProvider={this._layoutChildProvider}
                    rowRenderer={this._renderChildItem}
                /> */}
            </View>
        )
    }

    _renderChildItem = (type, data, index) => {
        const { title, value, imgUrl } = data
        return (
            <TouchableOpacity style={styles.mainView} onPress={() => this.onPress(value)}>
                <View style={styles.imageView}>
                    <Image
                        style={styles.imageStyle}
                        source={imgUrl}
                        resizeMode={'stretch'}
                    />
                </View>
                <Text>{title}</Text>
            </TouchableOpacity>
        )
    }

    render() {
        if (this.props.data && this.props.data.length) {
            const dataProvider: any = this.state.dataProvider
            const scrollEnabled = this.props.data.length > 2
            return (
                <View style={{ height: 160, marginBottom: 0, paddingLeft: 13 }}>
                    <RecyclerListView
                        isHorizontal
                        ref={r => (this._scrollView = r)}
                        // layoutProvider={this._layoutProvider}
                        layoutProvider={this._layoutChildProvider}
                        scrollViewProps={{
                            showsVerticalScrollIndicator: false,
                            showsHorizontalScrollIndicator: false,
                            contentContainerStyle: { margin: 0 },
                            scrollEnabled,
                            decelerationRate: 'normal'
                        }}
                        dataProvider={dataProvider}
                        rowRenderer={this._renderChildItem}
                    // style={{ margingHorizontal: 5 }}
                    />
                </View>
            )
        }
        return (
            <EmptyView
                title="Oh, No items available!"
                info="We currently are not available in this city but are working on it. Please select a different location."
                buttonText="Try a Different Location"
            // onPress={this.props.onTapFilterButton}
            />
        )
    }
}

const styles = StyleSheet.create({
    mainView: {
        height: 106,
        width: 106,
        marginHorizontal: 0,
        marginVertical: 5,
        borderRadius: 4,
        backgroundColor: "#F8F7F7",
        borderWidth: 1,
        borderColor: '#E5DEE0',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        padding: 0
    },
    imageView: {
        // height: 66,
        // width: 66,
        justifyContent: 'center',
        alignContent: 'center',
        marginBottom: 20
    },
    imageStyle: {
        height: 38,
        // width: 28,
    },
    inputText: {
        fontFamily: Assets.fonts.text.regular,
        fontSize: 16,
        fontWeight: '500',
        color: Assets.colors.mainText,
        // marginTop: 4,
        height: 40,
        letterSpacing: -0.5
    },
})
export default GridCategorySectionList
