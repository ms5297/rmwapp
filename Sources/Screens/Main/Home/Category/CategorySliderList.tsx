import React from 'react'
import { View, DeviceEventEmitter, StyleSheet, Image, TouchableOpacity, Alert, Dimensions } from 'react-native'
import { Navigator, getRouteName } from '@Navigation'
import { DataProvider, RecyclerListView, LayoutProvider } from 'recyclerlistview'
import { Constants } from '@Utils'
import { ItemCell, EmptyView, RefreshControl } from '@Components'
import { ItemsSummary, Item, ItemTypes, CategoryLists, } from '@Models'
import CategoryHeader from './CategoryHeader'

import isEqual from 'lodash.isequal'
import { NavigationProps } from '@Types'
import { withNavigation } from 'react-navigation'
import { Text } from 'rn-components'
import Assets from '@Assets'
// import Carousel from 'react-native-smart-carousel';
import Carousel, { Pagination } from 'react-native-snap-carousel';
const { width: screenWidth } = Dimensions.get('window')


type Props = {
    data: CategoryLists
    onRefresh?: () => Promise<boolean>
    onTapFilterButton?: () => void
    refreshing: boolean
}

type State = {
    refreshing: boolean
    loadingMore: boolean
    datacarousel: any
    activeSlide: number

}

class CategorySliderList extends React.PureComponent<Props, State> {
    _layoutProvider: LayoutProvider = null
    _layoutChildProvider: LayoutProvider = null
    _subscription = null
    _currentScrollViewPositionY = 0
    _scrollView = null

    constructor(props) {
        super(props)
        const { data } = this.props
        let datacarousel = []
        // this.onPress = this.onPress.bind(this);
        // this._renderItem = this._renderItem.bind(this);

        if (data) {
            data.forEach(item => {
                const { title, value, imgUrl } = item
                const imgData = { id: value, imagePath: imgUrl }
                datacarousel.push(imgData)
            })
        }

        this.state = {
            refreshing: false,
            loadingMore: false,
            datacarousel,
            activeSlide: 0
        }
    }


    componentDidMount() {
        // Alert.alert(screenWidth.toString());
        // this._subscription = DeviceEventEmitter.addListener(
        //     Constants.NEED_UPDATE_FAVORITE_ITEM_EVENT,
        //     this._handleUpdateFavoriteItems
        // )
    }

    componentWillUnmount() {
        // DeviceEventEmitter.removeListener(Constants.NEED_UPDATE_FAVORITE_ITEM_EVENT, this._handleUpdateFavoriteItems)
    }



    // _onRefresh = () => {
    //     const { onRefresh } = this.props
    //     if (typeof onRefresh === 'function') {
    //         if (this.state.refreshing) return
    //         Navigator.showLoading()
    //         this.setState({ refreshing: true })
    //         onRefresh()
    //             .then(() => {
    //                 this.setState({ refreshing: false })
    //             })
    //             .catch(error => {
    //                 this.setState({ refreshing: false })
    //             })
    //     }
    // }
    _onRefresh = () => {
        const { onRefresh } = this.props
        if (typeof onRefresh === 'function') {
            if (this.state.refreshing) return
            Navigator.showLoading()
            this.setState({ refreshing: true })
            onRefresh()
                .then(() => {
                    this.setState({ refreshing: false })
                })
                .catch(error => {
                    this.setState({ refreshing: false })
                })
        }
    }
    _onTapCategoryHeader = (occasionId: number) => {
        // Navigator.navTo('CategoryItemList', { occasionId, onGoBack: this._onRefresh })
    }
    onPress(item) {
        const occasionId = item.id
        Navigator.navTo('CategoryItemList', { occasionId, onGoBack: this._onRefresh })
    }
    get pagination() {
        const { datacarousel, activeSlide } = this.state;
        return (
            <Pagination
                dotsLength={datacarousel.length}
                activeDotIndex={activeSlide}
                containerStyle={{ paddingTop: 16, paddingBottom: 30 }}

                dotContainerStyle={{
                    marginHorizontal: 4
                }}
                dotStyle={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    backgroundColor: '#2F282A'
                    // marginHorizontal: 8,
                    // backgroundColor: 'rgba(255, 255, 255, 0.92)'
                }}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                    backgroundColor: '#E5DEE0'
                }}
                inactiveDotOpacity={0.4}
                inactiveDotScale={1}
            />
        );
    }
    renderPropertyItem = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => this.onPress(item)} >
                <Image source={item.imagePath} />
            </TouchableOpacity>
        );
    }
    render() {
        if (this.props.data && this.props.data.length) {
            const datacarousel: any = this.state.datacarousel
            //const itemWidth = screenWidth - 50
            const itemWidth = screenWidth * 8
            return (
                <View style={{ marginBottom: 0 }}>
                    <Carousel
                        data={datacarousel}
                        renderItem={this.renderPropertyItem}
                        sliderWidth={screenWidth}
                        //itemWidth={itemWidth}
                        removeClippedSubviews={false}
                        itemWidth={327}
                        slideStyle={{ padding: 0 }}
                        onSnapToItem={(index) => this.setState({ activeSlide: index })}
                        // containerCustomStyle={{ backgroundColor: 'red', marginRight: 48 }}
                        //contentContainerCustomStyle={{ marginLeft: -11 }}
                        contentContainerCustomStyle={{ paddingLeft: 13 }}
                    />
                    {this.pagination}
                </View>
            )
        }
        return (
            <EmptyView
                title="Oh, No items available!"
                info="We currently are not available in this city but are working on it. Please select a different location."
                buttonText="Try a Different Location"
            // onPress={this.props.onTapFilterButton}
            />
        )
    }
}

const styles = StyleSheet.create({
    sliderView: {
        height: 160,
        width: 335,
        opacity: 0.25,
        backgroundColor: '#F47495'
    },
    textStyle: {
        color: '#2F282A',
        fontFamily: Assets.fonts.text.regular,
        fontSize: 28,
        fontWeight: 'bold',
        lineHeight: 34
    }
})
export default CategorySliderList
