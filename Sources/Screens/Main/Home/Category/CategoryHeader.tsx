import React from 'react'
import { Image, StyleSheet, ViewProps } from 'react-native'
import { Text, Row } from 'rn-components'
import { Utils } from '@Utils'
import Assets from '@Assets'

type Props = ViewProps & {
  occasionId: number
  onTapCategoryHeader: (occasionId: number) => void
}

const CategoryHeader: React.SFC<Props> = props => {
  const { occasionId, onTapCategoryHeader, style } = props
  const onPress = () => onTapCategoryHeader(occasionId)
  const title = Utils.getTitleWithValue('occasions', occasionId)
  return (
    <Row style={[styles.row, style]} alignHorizontal="space-between" alignVertical="center">
      <Text numberOfLines={1} style={styles.categoryName} text={title} />
      <Row alignVertical="center" onPress={onPress}>
        <Text style={styles.seeAllLink} text="See all" />
        <Image source={Assets.images.categoryDetailsArrow} style={{ marginLeft: 6 }} />
      </Row>
    </Row>
  )
}

const styles = StyleSheet.create({
  row: {
    marginRight: 15,
    marginTop: 6,
    marginBottom: 10
  },

  categoryName: {
    marginLeft: 15,
    marginRight: 10,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 24,
    letterSpacing: -0.2,
    color: Assets.colors.mainText
  },

  seeAllLink: {
    textAlign: 'right',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    color: Assets.colors.appTheme
  }
})

export default CategoryHeader
