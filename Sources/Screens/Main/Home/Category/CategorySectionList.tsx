import React from 'react'
import { View, DeviceEventEmitter } from 'react-native'
import { Navigator, getRouteName } from '@Navigation'
import { DataProvider, RecyclerListView, LayoutProvider } from 'recyclerlistview'
import { Constants } from '@Utils'
import { ItemCell, EmptyView, RefreshControl } from '@Components'
import { ItemsSummary, Item } from '@Models'
import CategoryHeader from './CategoryHeader'

import isEqual from 'lodash.isequal'
import { NavigationProps } from '@Types'
import { withNavigation } from 'react-navigation'

type Props = {
  data: ItemsSummary
  onRefresh?: () => Promise<boolean>
  onTapFilterButton?: () => void
  refreshing: boolean
}

type State = {
  refreshing: boolean
  loadingMore: boolean
  dataProvider: Array<ItemsSummary>
  dataChildProvider: Array<Item>
  ids: Array<number>
}

class CategorySectionList extends React.PureComponent<Props, State> {
  _layoutProvider: LayoutProvider = null
  _layoutChildProvider: LayoutProvider = null
  _subscription = null
  _currentScrollViewPositionY = 0
  _scrollView = null

  constructor(props) {
    super(props)

    const { data } = this.props

    const dataRow: any = new DataProvider((r1, r2) => r1 !== r2)
    const dataProvider = dataRow.cloneWithRows(data)

    let dataChildProvider = []
    let ids = []
    data.forEach(item => {
      const provider: any = new DataProvider((r1, r2) => r1 !== r2)
      const childProvider = provider.cloneWithRows(item.items)
      dataChildProvider.push(childProvider)
      ids.push(item.id)
    })

    this._layoutProvider = new LayoutProvider(
      index => 1,
      (type, dim) => {
        dim.width = Constants.WINDOW_WIDTH
        dim.height = Constants.TOTAL_CELL_HEIGHT
      }
    )

    this._layoutChildProvider = new LayoutProvider(
      index => 1,
      (type, dim) => {
        dim.width = Constants.CELL_WIDTH + 15
        dim.height = Constants.TOTAL_CELL_HEIGHT
      }
    )

    this.state = {
      refreshing: false,
      loadingMore: false,
      dataProvider,
      dataChildProvider,
      ids
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (Array.isArray(nextProps.data) && !isEqual(prevState.data, nextProps.data)) {
      let dataChildProvider = []
      let ids = []
      nextProps.data.forEach(item => {
        const provider: any = new DataProvider((r1, r2) => r1 !== r2)
        const childProvider = provider.cloneWithRows(item.items)
        dataChildProvider.push(childProvider)
        ids.push(item.id)
      })
      const dataRow: any = new DataProvider((r1, r2) => r1 !== r2)
      const dataProvider = dataRow.cloneWithRows(nextProps.data)

      return {
        ...prevState,
        dataChildProvider,
        ids,
        dataProvider
      }
    }

    return null
  }

  componentDidMount() {
    this._subscription = DeviceEventEmitter.addListener(
      Constants.NEED_UPDATE_FAVORITE_ITEM_EVENT,
      this._handleUpdateFavoriteItems
    )
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(Constants.NEED_UPDATE_FAVORITE_ITEM_EVENT, this._handleUpdateFavoriteItems)
  }

  _handleUpdateFavoriteItems = data => {
    if (data && data.shouldRefresh) {
      this._onRefresh()
    } else {
      if (this._scrollView && this._scrollView._scrollComponent && this._scrollView._scrollComponent._scrollViewRef) {
        const scrollViewRef = this._scrollView._scrollComponent._scrollViewRef
        if (scrollViewRef && typeof scrollViewRef.scrollResponderScrollTo === 'function') {
          const contentHeight = scrollViewRef.props ? scrollViewRef.props.contentHeight : 0

          scrollViewRef.scrollResponderScrollTo({ x: 0, y: 0, animated: false })
          scrollViewRef.scrollResponderScrollTo({ x: 0, y: contentHeight, animated: false })
          scrollViewRef.scrollResponderScrollTo({ x: 0, y: this._currentScrollViewPositionY, animated: false })
        }
      }
    }
  }

  _onRefresh = () => {
    const { onRefresh } = this.props
    if (typeof onRefresh === 'function') {
      if (this.state.refreshing) return
      Navigator.showLoading()
      this.setState({ refreshing: true })
      onRefresh()
        .then(() => {
          this.setState({ refreshing: false })
        })
        .catch(error => {
          this.setState({ refreshing: false })
        })
    }
  }

  _onTapCategoryHeader = (occasionId: number) => {
    Navigator.navTo('CategoryItemList', { occasionId, onGoBack: this._onRefresh })
  }

  _keyExtrator = (item, index) => index.toString()

  _renderItem = (type, data, index) => {
    const dataChildProvider: any = this.state.dataChildProvider
    const provider = dataChildProvider[index]
    const occasionId = this.state.ids[index]
    const headerHeight = Constants.TOTAL_CELL_HEIGHT - Constants.CELL_HEIGHT
    const scrollEnabled = data.items.length > 2

    return (
      <View style={{ width: Constants.WINDOW_WIDTH, height: Constants.TOTAL_CELL_HEIGHT }}>
        <CategoryHeader
          style={{ height: headerHeight }}
          occasionId={occasionId}
          onTapCategoryHeader={this._onTapCategoryHeader}
        />
        <RecyclerListView
          isHorizontal
          scrollViewProps={{
            showsVerticalScrollIndicator: false,
            showsHorizontalScrollIndicator: false,
            contentContainerStyle: { marginLeft: 13 },
            scrollEnabled,
            decelerationRate: 'normal'
          }}
          dataProvider={provider}
          layoutProvider={this._layoutChildProvider}
          rowRenderer={this._renderChildItem}
        />
      </View>
    )
  }

  _renderChildItem = (type, data, index) => {
    return (
      <ItemCell
        shouldRefreshCategoryFavorite
        editable={false}
        style={{ height: Constants.CELL_HEIGHT }}
        item={data}
        width={Constants.CELL_WIDTH}
      />
    )
  }

  render() {
    if (this.props.data.length) {
      const dataProvider: any = this.state.dataProvider
      return (
        <RecyclerListView
          ref={r => (this._scrollView = r)}
          layoutProvider={this._layoutProvider}
          scrollViewProps={{
            showsVerticalScrollIndicator: false,
            showsHorizontalScrollIndicator: false,
            contentContainerStyle: {
              paddingBottom: 60
            },
            decelerationRate: 'normal',
            refreshControl: <RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh} />,
            onMomentumScrollEnd: e => {
              this._currentScrollViewPositionY = e.nativeEvent.contentOffset.y
            }
          }}
          dataProvider={dataProvider}
          rowRenderer={this._renderItem}
        />
      )
    }

    if (this.props.refreshing) {
      return (
        <EmptyView
          title="Hold Please. Summoning the fashionistas."
        />
      )
    }
    return (
      <EmptyView
        title="Oh, No items available!"
        info="We currently are not available in this city but are working on it. Please select a different location."
        buttonText="Try a Different Location"
      // onPress={this.props.onTapFilterButton}
      />
    )
  }
}

export default CategorySectionList
