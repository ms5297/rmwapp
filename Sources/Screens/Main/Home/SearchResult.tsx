import React from 'react'
import { ItemList, Item } from '@Models'
import { DataProvider, RecyclerListView, LayoutProvider } from 'recyclerlistview'
import { Navigator } from '@Navigation'
import { RefreshControl, View, ActivityIndicator, ImageSourcePropType, ViewProps, StyleSheet } from 'react-native'
import { Constants, Device } from '@Utils'
import { ItemCell, EmptyView } from '@Components'
import { ItemCellProps } from '@Types'

import isEqual from 'lodash.isequal'
import { Text } from 'rn-components'
import Assets from '@Assets'

const LOAD_MORE_CONTROL_OFFSET = Device.isAndroid() ? -20 : -15
type Props = ViewProps & {
  data: ItemList
  onRefresh?: () => Promise<boolean>
  onLoadMore?: () => Promise<boolean>
  emptyViewProps?: {
    titleImage?: ImageSourcePropType
    title?: string
    info?: string
    buttonText?: string
    onPress?: () => void
  }
  itemHeight?: number
  loadOnMount?: boolean
  showEmptyView?: boolean
  itemCellProps?: Partial<ItemCellProps>
  onItemSelected?: (item: Item) => void
  refreshing: boolean
}

type State = {
  refreshing: boolean
  loadingMore: boolean
  dataProvider: DataProvider
  loadable: boolean
  data: ItemList
}

class SearchResult extends React.Component<Props, State> {
  _layoutProvider: LayoutProvider = null

  constructor(props) {
    super(props)

    const { data = [], itemHeight = Constants.CELL_HEIGHT, loadOnMount = false } = this.props

    const dataRow: any = new DataProvider((r1, r2) => r1 !== r2)
    const dataProvider = dataRow.cloneWithRows(data)

    this._layoutProvider = new LayoutProvider(
      index => 1,
      (type, dim) => {
        dim.width = Constants.WINDOW_WIDTH / 2 - Constants.OFFSET_WIDTH
        dim.height = itemHeight
      }
    )

    this.state = {
      dataProvider,
      loadable: false,
      loadingMore: false,
      refreshing: loadOnMount,
      data
    }
  }

  static getDerivedStateFromProps(props: Props, state: State) {
    if (Array.isArray(props.data)) {
      const dataProvider = state.dataProvider.cloneWithRows(props.data)
      return { ...state, dataProvider }
    }

    return state
  }

  /*   shouldComponentUpdate(props: Props, state: State) {
      const shouldUpdate =
        state.refreshing !== this.state.refreshing ||
        state.loadingMore !== this.state.loadingMore ||
        !isEqual(props.data, this.state.data)
  
      return shouldUpdate
    } */

  _onRefresh = () => {
    const { onRefresh } = this.props
    if (typeof onRefresh === 'function') {
      if (this.state.refreshing) return
      Navigator.showLoading()
      this.setState({ refreshing: true })
      onRefresh()
        .then(() => {
          this.setState({ refreshing: false })
          Navigator.hideLoading()
        })
        .catch(error => {
          this.setState({ refreshing: false })
          Navigator.hideLoading()
        })
    }
  }

  _onLoadMore = () => {
    const { onLoadMore } = this.props
    if (typeof onLoadMore === 'function' && this.state.loadable) {
      if (this.state.loadingMore) return
      Navigator.showLoading()
      this.setState({ loadingMore: true })
      onLoadMore()
        .then(() => {
          this.setState({ loadingMore: false })
          Navigator.hideLoading()
        })
        .catch(error => {
          this.setState({ loadingMore: false })
          Navigator.hideLoading()
        })
    }
  }

  _onMomentumScrollBegin = () => this.setState({ loadable: true })

  _onMomentumScrollEnd = () => this.setState({ loadable: false })

  _renderFooter = () => {
    return (
      this.state.loadingMore && (
        <View style={{ marginBottom: 20, marginTop: LOAD_MORE_CONTROL_OFFSET }}>
          <ActivityIndicator />
        </View>
      )
    )
  }

  _renderItem = (type, data, index) => {
    const paddingLeft = index % 2 ? 0 : 8
    const paddingRight = 8 - paddingLeft

    const { onItemSelected } = this.props
    const onPress = typeof onItemSelected === 'function' ? () => onItemSelected(data) : null
    return (
      <View style={{ width: Constants.WINDOW_WIDTH / 2, alignItems: 'center', paddingLeft, paddingRight }}>
        <ItemCell {...this.props.itemCellProps} item={data} width={Constants.CELL_WIDTH} onPress={onPress} />
      </View>
    )
  }

  render() {
    const { dataProvider, refreshing } = this.state
    const { data, emptyViewProps, showEmptyView = true, style } = this.props
    if (data.length) {
      return (
        <View style={[{ flex: 1 }, style]}>
          <Text style={styles.header} text="Search Results" />
          <RecyclerListView
            scrollViewProps={{
              showsVerticalScrollIndicator: false,
              showsHorizontalScrollIndicator: false,
              decelerationRate: 'normal',
              refreshControl: <RefreshControl refreshing={refreshing} onRefresh={this._onRefresh} />,
              onMomentumScrollBegin: this._onMomentumScrollBegin,
              onMomentumScrollEnd: this._onMomentumScrollEnd
            }}
            dataProvider={dataProvider}
            layoutProvider={this._layoutProvider}
            rowRenderer={this._renderItem}
            onEndReachedThreshold={1}
            onEndReached={this._onLoadMore}
            renderFooter={this._renderFooter}
            extraData={dataProvider}
          />
        </View>
      )
    }

    if (this.props.refreshing) {
      return (
        showEmptyView && (
          <EmptyView
            title="Hold Please. Summoning the fashionistas."
          />
        )
      )
    }
    return (
      showEmptyView && (
        <EmptyView
          title="Oh, No items available"
          info="There are currently no items available using that search. Please change your search filters or location and try again."
          buttonText="Adjust the Search"
          {...emptyViewProps}
        />
      )
    )
  }
}

const styles = StyleSheet.create({
  header: {
    marginLeft: 15,
    marginBottom: 25,
    fontSize: 32,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.display.bold
  }
})
export default SearchResult
