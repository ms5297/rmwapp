import React from 'react'
import firebase, { Firebase } from 'react-native-firebase'
import { connect } from 'react-redux'
import { View, StyleSheet, Platform, EmitterSubscription, StatusBar, Modal } from 'react-native'
import {
  StripePaymentMethods,
  UserProfile,
  Item,
  RentalStatus,
  ChatParam,
  ChatModelItem,
  ChatUserModel,
  MessageModelItem,
  CheckoutParam,
  RentalDetailModel,
  ReturnItemParam,
  ReturnPhotoModel,
  RentalRatingParam,
  RentalItemDetailsParam,
  RentalDetailItem,
  ChatModelOrderItem,
  RentalRatingDetailParam
} from '@Models'
import { Navigator } from '@Navigation'
import { Api, FirebaseHelper, RNDocumentReference, AppsFlyerHelper, GoogleAnalyticsHelper } from '@Services'
import { NavigationProps } from '@Types'
import { DateTime, Utils, LayoutAnimations } from '@Utils'
import { BackButton, Button, GiftedChat } from '@Components'
import { Header, Icon, Row, Text } from 'rn-components'
import { Bubbles } from 'react-native-loader'
import { StoreState } from '@ReduxManager'
import Assets from '@Assets'
import Styles from '@Styles'
import { Alert } from 'rn-notifier'
import { any } from 'prop-types'

type Props = {
  paymentMethods: StripePaymentMethods
  userProfile: UserProfile
  navigation: NavigationProps
}

type State = ChatParam & {
  user: string
  loadEarlier: boolean
  isLoadingEarlier: boolean
  lastMessageTime: number | null
  firstMessageTime: number | null
  messages: Array<any>
  showFooter: boolean
  statusValue: RentalStatus
  checkBuyNowStatus: boolean
  archived: boolean
  loading: boolean
  purchaseData: any
  isRequested:boolean
  isRequestedproduct:boolean
  

}
class Chat extends React.Component<Props, State> {
  [x: string]: any
  _chatDoc: RNDocumentReference = null
  _chatDoc2: RNDocumentReference = null
  _unsubscribe = null
  _unsubscribeChatData = null
  _conversationRef = firebase.firestore().collection('conversations')
  _loadChatListener: EmitterSubscription = null
  _otherUser: ChatUserModel = null
  _rentalDetails: RentalDetailModel | null
  _currentChatData: ChatModelItem | null
  _timeoutHandler: any = null
  checkMesg: string = null

  constructor(props: Props) {
    super(props)
    const param: ChatParam = this.props.navigation.getParam('param')
    // let buy_now = '';
    this.state = {
      user: '',
      loadEarlier: false,
      isLoadingEarlier: false,
      lastMessageTime: null,
      firstMessageTime: null,
      messages: [],
      showFooter: false,
      statusValue: '',
      checkBuyNowStatus: false,
      message: null,
      date: null,
      fromPush: false,
      item: null,
      rental_id: null,
      selectedDates: null,
      statusChange: null,
      archived: false,
      loading: false,
      purchaseData:null,
      isRequested: false,
      isRequestedproduct: false,
     
      ...param
    }

    LayoutAnimations.enableAndroidLayoutAnimation()
  }

  componentWillReceiveProps(props: Props, state: State) {
    if (props.navigation && this.state.chatID != null) {
      const param: ChatParam = props.navigation.getParam('param', null)
      if (param && param.fromPush && param.chatID !== this.state.chatID) {
        this._chatDoc = null
        this._chatDoc2 = null
        this._otherUser = null
        this._currentChatData = null
        this._rentalDetails = null
        this._unsubscribe && this._unsubscribe()
        this._unsubscribeChatData && this._unsubscribeChatData()
        this.setState(
          {
            user: '',
            archived: false,
            loadEarlier: false,
            isLoadingEarlier: false,
            lastMessageTime: null,
            firstMessageTime: null,
            messages: [],
            showFooter: false,
            ...param
          },
          this._populateValuesFromChat
        )
      }
      const { rental_id } = this.state
      this.getPurchaseDetails(rental_id);
    }
  }

  getPurchaseDetails = (rental_id:number) =>{
    Api.getPurchaseDetails(rental_id)
    .then((response: any) => {
          console.log(response);
          const { purchses } = response
          let purchasedCount = 0; 
          purchses.map((item) =>{
            if(item.status == "request"){
              purchasedCount++;
              this.setState({
                isRequested : true,
                purchaseData : purchses
              })
            }
          })
          if(purchasedCount == 0)
          {
            this.setState({
              isRequested : false,
              purchaseData : null
            })
          }
    })
    .catch(error => {
      Navigator.hideLoading()
      console.log('**** error: ', error)
    })
  }

  componentWillMount() {
    StatusBar.setBarStyle('dark-content')
    this._populateValuesFromChat()
    this.props.navigation.setParams(this._currentChatData)
    
    //----- This is for buy now approval---------
    const { rental_id } = this.state
    this.getPurchaseDetails(rental_id);
  }

  componentWillUnmount() {
    this._unsubscribe && this._unsubscribe()
    this._unsubscribeChatData && this._unsubscribeChatData()
    this._loadChatListener && this._loadChatListener.remove()
    this._timeoutHandler && clearTimeout(this._timeoutHandler)
  }

  _updateItemReturnStatusRetry = (chatData: ChatModelItem) => {
    return new Promise<void>((resolve, reject) => {
      const data = chatData.orders
      const returnedItem = data.filter(item => item.status === 'returned')
      const totalReturnedItem = Array.isArray(returnedItem) ? returnedItem.length : 0
      const isComplete = totalReturnedItem === data.length
      if (isComplete && chatData.status !== 'returned') {
        Navigator.showLoading()
        Promise.all([
          this._changeRentalStatus(this._currentChatData.rental_id, 'return'),

          this._chatDoc.set({ status: 'returned' }, { merge: true }),
          this._chatDoc2.set({ status: 'returned' }, { merge: true })
        ])
          .then(values => {
            console.log('***** _updateItemReturnStatusRetry response', values)
            Navigator.hideLoading()
            resolve()
          })
          .catch(error => {
            console.log('***** _updateItemReturnStatusRetry error', error)
            Navigator.hideLoading()
            resolve()
          })
      }
    })
  }
  /**added by mahesh purchased status */
  _updateItemPurchasedStatus = () => {
    return new Promise<void>((resolve, reject) => {
      const chatData = this._currentChatData;
      const data = chatData.orders
      // const returnedItem = data.filter(item => item.status === 'returned')
      // const totalReturnedItem = Array.isArray(returnedItem) ? returnedItem.length : 0
      // const isComplete = totalReturnedItem === data.length
      if (chatData.status !== 'returned') {
        Navigator.showLoading()
        Promise.all([
          this._changeRentalStatus(this._currentChatData.rental_id, 'purchased'),

          this._chatDoc.set({ status: 'purchased' }, { merge: true }),
          this._chatDoc2.set({ status: 'purchased' }, { merge: true })
        ])
          .then(values => {
            console.log('***** _updateItemReturnStatusRetry response', values)
            Navigator.hideLoading()
            resolve()
          })
          .catch(error => {
            console.log('***** _updateItemReturnStatusRetry error', error)
            Navigator.hideLoading()
            resolve()
          })
      }
    })
  }
  _updateItemPurchasedStatusPartial() {
    return new Promise<void>((resolve, reject) => {
      const chatData = this._currentChatData;
      const data = chatData.orders
      // const returnedItem = data.filter(item => item.status === 'returned')
      // const totalReturnedItem = Array.isArray(returnedItem) ? returnedItem.length : 0
      // const isComplete = totalReturnedItem === data.length
      if (chatData.status !== 'returned') {
        Navigator.showLoading()
        Promise.all([
          this._changeRentalStatus(this._currentChatData.rental_id, 'returned'),

          this._chatDoc.set({ status: 'returned' }, { merge: true }),
          this._chatDoc2.set({ status: 'returned' }, { merge: true })
        ])
          .then(values => {
            console.log('***** _updateItemReturnStatusRetry response', values)
            Navigator.hideLoading()
            resolve()
          })
          .catch(error => {
            console.log('***** _updateItemReturnStatusRetry error', error)
            Navigator.hideLoading()
            resolve()
          })
      }
    })
  }
  _updateItemReturnStatus = (returnItem: ChatModelOrderItem, isComplete = false) => {
    let _orders = []
    let _isComplete = isComplete

    console.log('**** _updateItemReturnStatus isComplete', isComplete)

    return new Promise<ChatModelItem>((resolve, reject) => {
      Promise.all([this._chatDoc.get(), this._chatDoc2.get()])
        .then(values => {
          const data1: any = values[0].data()

          if (data1 && Array.isArray(data1.orders) && data1.orders.length) {
            let orders: Array<ChatModelOrderItem> = data1.orders
            const index = orders.findIndex(item => item.itemId == returnItem.itemId)
            if (index !== -1) {
              orders[index].status = 'returned'
              _orders.push(orders)
            }
          }

          const data2: any = values[1].data()
          if (data2 && Array.isArray(data2.orders) && data2.orders.length) {
            let orders: Array<ChatModelOrderItem> = data2.orders
            const index = orders.findIndex(item => item.itemId == returnItem.itemId)
            if (index !== -1) {
              orders[index].status = 'returned'
              _orders.push(orders)
            }
          }

          if (_orders.length == 2) {
            if (isComplete) {
              return Promise.all([
                this._chatDoc.update({ orders: _orders[0], order: _orders[0][0], status: 'returned' }),
                this._chatDoc2.update({ orders: _orders[1], order: _orders[1][0], status: 'returned' })
              ])
            }
            return Promise.all([
              this._chatDoc.update({ orders: _orders[0], order: _orders[0][0] }),
              this._chatDoc2.update({ orders: _orders[1], order: _orders[1][0] })
            ])
          } else {
            throw Error('Fail to update')
          }
        })
        .then(values => {
          console.log('*** udpate docs', values.length)

          return Api.returnItem(this._currentChatData.rental_id, returnItem.itemId)
        })
        .then((response: any) => {
          console.log('returnItem response: ', response)
          const { code, message } = response
          if (code !== 200) throw Error(message || 'Internal Error.')
          console.log('**** update return status success', response, _isComplete)

          // if (_isComplete) {
          //   const itemName = Utils.combineItemsName(this._rentalDetails.items);
          //   const verb = this._rentalDetails.items.length ? "were" : "was";
          //   const date = DateTime.moment().format("MMM DD, YYYY");
          //   this._changeRentalStatus(this._currentChatData.rental_id, "return");
          //   this._sendMessage(
          //     `${itemName} ${verb} returned on ${date}`,
          //     null,
          //     true,
          //     true,
          //     false,
          //     "Success! Your rental is checked in! Rate your experience",
          //     false
          //   );
          // }

          this._chatDoc
            .get()
            .then(doc => resolve(doc.data() as ChatModelItem))
            .catch(error => {
              throw error
            })
        })
        .catch(error => {
          console.log('***** error', error)
          reject(error)
        })
    })
  }

  _updateItemReviewStatusRetry = (chatData: ChatModelItem) => {
    return new Promise<void>((resolve, reject) => {
      const data = chatData.orders
      const reviewedItem = data.filter(item => item.status === 'reviewed')
      const totalReviewedItem = Array.isArray(reviewedItem) ? reviewedItem.length : 0
      const isComplete = totalReviewedItem === data.length
      if (isComplete && chatData.status !== 'reviewed') {
        Navigator.showLoading()
        Promise.all([
          this._changeRentalStatus(chatData.rental_id, 'review'),
          this._chatDoc.set({ status: 'reviewed' }, { merge: true })
          // this._chatDoc2.set({ status: "reviewed" }, { merge: true })
        ])
          .then(values => {
            console.log('***** _updateItemReviewStatusRetry response', values)

            Navigator.hideLoading()
            resolve()
          })
          .catch(error => {
            console.log('***** _updateItemReviewStatusRetry error', error)
            Navigator.hideLoading()
            resolve()
          })
      } else {
        resolve()
      }
    })
  }

  _updateItemReviewStatus = (returnItem: ChatModelOrderItem, isComplete = false) => {
    return new Promise<ChatModelItem>((resolve, reject) => {
      this.setState({ loading: true })
      let _isComplete = isComplete
      this._chatDoc
        .get()
        .then(response => {
          const data: any = response.data()
          if (data && Array.isArray(data.orders) && data.orders.length) {
            let orders: Array<ChatModelOrderItem> = data.orders
            const index = orders.findIndex(item => item.itemId == returnItem.itemId)
            if (index !== -1) {
              orders[index].status = 'reviewed'
              orders[index].rating = returnItem.rating || 0
              console.log('**** review item')
              if (isComplete) {
                return this._chatDoc.update({ orders: orders, order: orders[0], status: 'reviewed' })
              } else {
                return this._chatDoc.update({ orders: orders, order: orders[0] })
              }
            } else {
              throw Error("can't found")
            }
          } else {
            throw Error('order is empty')
          }
        })
        .then(() => {
          this._changeRentalStatus(this._currentChatData.rental_id, 'review', error => {
            if (error) throw error
          })
          this._chatDoc
            .get()
            .then(doc => {
              resolve(doc.data() as ChatModelItem)
              this.setState({ loading: false })
            })
            .catch(error => {
              throw error
            })
        })
        .catch(error => {
          console.log('***** error', error)
          reject(error)
          this.setState({ loading: false })
          Navigator.showToast('Error', error.message, 'Error')
        })
    })

    // console.log("**** _updateItemReviewStatus isComplete", isComplete, returnItem);
    // return new Promise<ChatModelItem>((resolve, reject) => {
    //   Promise.all([this._chatDoc.get(), this._chatDoc2.get()])
    //     .then(values => {
    //       const data1: any = values[0].data();

    //       if (data1 && Array.isArray(data1.orders) && data1.orders.length) {
    //         let orders: Array<ChatModelOrderItem> = data1.orders;
    //         const index = orders.findIndex(item => item.itemId == returnItem.itemId);
    //         if (index !== -1) {
    //           orders[index].status = "reviewed";
    //           orders[index].rating = returnItem.rating || 0;
    //           _orders.push(orders);
    //         }
    //       }

    //       const data2: any = values[1].data();
    //       if (data2 && Array.isArray(data2.orders) && data2.orders.length) {
    //         let orders: Array<ChatModelOrderItem> = data2.orders;
    //         const index = orders.findIndex(item => item.itemId == returnItem.itemId);
    //         if (index !== -1) {
    //           orders[index].status = "reviewed";
    //           orders[index].rating = returnItem.rating || 0;
    //           _orders.push(orders);
    //         }
    //       }

    //       if (_orders.length == 2) {
    //         if (isComplete) {
    //           return Promise.all([
    //             this._chatDoc.update({ orders: _orders[0], order: _orders[0][0], status: "reviewed" }),
    //             this._chatDoc2.update({ orders: _orders[1], order: _orders[1][0], status: "reviewed" })
    //           ]);
    //         }
    //         return Promise.all([
    //           this._chatDoc.update({ orders: _orders[0], order: _orders[0][0] }),
    //           this._chatDoc2.update({ orders: _orders[1], order: _orders[1][0] })
    //         ]);
    //       } else {
    //         throw Error("Fail to update");
    //       }
    //     })
    //     .then((response: any) => {
    //       if (_isComplete) {
    //         this._changeRentalStatus(this._currentChatData.rental_id, "review", error => {
    //           if (error) throw error;
    //         });
    //       }

    //       this._chatDoc
    //         .get()
    //         .then(doc => {
    //           resolve(doc.data() as ChatModelItem);
    //           this.setState({ loading: false });
    //         })
    //         .catch(error => {
    //           throw error;
    //         });
    //     })
    //     .catch(error => {
    //       console.log("***** error", error);
    //       reject(error);
    //       this.setState({ loading: false });
    //       Navigator.showToast("Error", error.message, "Error");
    //     });
    // });
  }

  _subscribeForChatDataChange = () => {
    this._unsubscribeChatData && this._unsubscribeChatData()
    this._unsubscribeChatData = this._chatDoc.onSnapshot(snapShot => {
      let data = snapShot.data() as ChatModelItem
      data.id = snapShot.id
      this._currentChatData = data

      console.log('****** _subscribeForChatDataChange', data)

      const { showFooter, statusValue } = Utils.checkStatus(data)
      if (showFooter !== this.state.showFooter || statusValue !== this.state.statusValue) {
        this.state.loading == false && Navigator.showLoading()
        LayoutAnimations.setLayoutAnimation(LayoutAnimations.PresetEaseInOut)
        this.setState({ showFooter, statusValue })

        this._timeoutHandler = setTimeout(() => Navigator.hideLoading(), 300)
      }
    })
  }

  /******************************** START Perform action ******************************************/
  _handleCancelAction = () => {
    const { statusValue } = this.state
    const { userProfile } = this.props
    let message = 'Are you sure that you want to cancel this rental?'
    let shouldCharge = false
    if (statusValue === 'accepted') {
      let startDate = this._rentalDetails.dates[0]
      let date = DateTime.moment(startDate).format('YYYY-MM-DD')
      let today = Date.now()

      let dayRental = DateTime.moment(date).startOf('day')
      let dayToday = DateTime.moment(today).startOf('day')

      let difference = dayRental.diff(dayToday, 'days')
      let isOwner = false;
      if(this._currentChatData.itemOwner){
         isOwner = this._currentChatData.itemOwner
      }else{
         isOwner = this._currentChatData.itemOwner
      }
      // let isOwner = this._currentChatData.itemOwner

      if (difference >= 3) {
        message = isOwner
          ? 'Are you sure that you want to cancel this rental? The renter will be refunded the full rental total.'
          : 'Are you sure that you want to cancel this rental? You will be refunded the full rental total.'
      } else {
        if (isOwner) {
          shouldCharge = true
          message =
            'Are you sure that you want to cancel this rental? You will be charged $25 to cancel within 72 hours of the rental date.'
        } else {
          if (this._rentalDetails.items[0].refund_option_id == 1) {
            message =
              'Are you sure that you want to cancel this rental? You will be refunded 80% of the rental fee because you are within 72 hours of the rental date.'
          } else {
            message = 'Are you sure that you want to cancel this rental? There will be no refund.'
          }
        }
      }
    }
    Navigator.showAlert(
      'Cancel rental',
      message,
      () => null,
      () => {
        Navigator.showLoading()
        const proceedWithCancel = () => {
          this._changeRentalStatus(this._currentChatData.rental_id, 'cancel', error => {
            if (error) {
              Navigator.hideLoading()
              Navigator.showToast('Error', '(Chat - 325) Fail to update rental status', 'Error')
            } else {
              Promise.all([
                this._chatDoc.set({ status: 'cancelled' }, { merge: true }),
                this._chatDoc2.set({ status: 'cancelled' }, { merge: true })
              ])
                .then(() => {
                  this._sendMessage(
                    'The request was cancelled on ' +
                    DateTime.moment(new Date()).format('MMM DD, YYYY') +
                    '. Any refunds will be processed according to the refund policy.',
                    null,
                    true,
                    true,
                    false,
                    Utils.getUserName(userProfile) + ' cancelled a scheduled rental.',
                    false
                  )
                  Navigator.hideLoading()
                })
                .catch(error => {
                  console.log('**** error', error)
                  Navigator.hideLoading()
                  Navigator.showToast('Error', error.message, 'Error')
                })
            }
          })
        }
        if (shouldCharge) {
          if (this.props.paymentMethods.length === 0) {
            Navigator.navTo('AddCreditCard', {
              checkoutProcess: true,
              onCardAdded: card => {
                proceedWithCancel()
              }
            })
          } else {
            proceedWithCancel()
          }
        } else {
          proceedWithCancel()
        }
      },
      'No',
      'Yes'
    )
  }

  _handleRentAction = () => {
    Navigator.showLoading()

    const rental_id = this._currentChatData.rental_id
    const itemIds = this._currentChatData.itemIds

    Promise.all([Api.getRentalDetail(rental_id), Api.getItemDetails(itemIds)])
      .then((values: any) => {
        const { code: code1, message: message1, ...data } = values[0]
        const { code: code2, message: message2, items } = values[1]

        if (code1 !== 200) {
          throw Error(message1 || '(CH - 253) Internal Error')
        }

        if (code2 !== 200) {
          throw Error(message2 || '(CH - 259) Internal Error')
        }
        const selectedDates = data.dates
        const param: CheckoutParam = { items, selectedDates }

        console.log('***** navTo checkout with param', param)
        Navigator.hideLoading()
        Navigator.navTo('Checkout', { param })
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }
  _handleBuyNowActions = (purchasedItems: any, isCompleted: boolean = false, isPartial: boolean = false) => {
    const { rental_id } = this.state
   
    // if (isPartial) {
    //     return new Promise<void>((resolve, reject) => {
    //     const message = `Request for purchase item(s) ${purchasedItems} on ${DateTime.moment().format('MMM DD, YYYY')}`;
    //     Promise.all([
    //       isCompleted ? '' : this._populateValuesFromChat(),
    //       // isCompleted ? this._updateItemPurchasedStatus() : '',
    //       this._sendMessage(message, null, true, true, true, null, true)

    //     ])
    //       .then(values => {
    //         console.log('***** _updateItemReturnStatusRetry response', values)
    //         Navigator.hideLoading()
    //         resolve()
    //       })
    //       .catch(error => {
    //         console.log('***** _updateItemReturnStatusRetry error', error)
    //         Navigator.hideLoading()
    //         resolve()
    //       })

    //   })
    // } else {
      this.getPurchaseDetails(rental_id)
      this.setState({
        isRequested : true
      })
      return new Promise<void>((resolve, reject) => {
      const message = `Request for purchase item(s) ${purchasedItems} on ${DateTime.moment().format('MMM DD, YYYY')}`;
      Promise.all([
        isCompleted ? '' : this._populateValuesFromChat(),
        // isCompleted ? this._updateItemPurchasedStatusPartial() : '',
        this._sendMessage(message, null, true, true, true, null, true)

      ])
        .then(values => {
          console.log('***** _updateItemReturnStatusRetry response', values)
          Navigator.hideLoading()
          resolve()
          this._populateValuesFromChat()
        })
        .catch(error => {
          console.log('***** _updateItemReturnStatusRetry error', error)
          Navigator.hideLoading()
          resolve()
        })

    })
    // }
    
    
  }

  _handleApproveAction = () => {
    if (this._rentalDetails && this._currentChatData) {
      Navigator.showLoading()
      this._changeRentalStatus(this._currentChatData.rental_id, 'approved', error => {
        if (error) {
          Navigator.hideLoading()
          Navigator.showToast('Error', '(CH - 289) ' + error.message, 'Error')
        } else {
          const owner = this._rentalDetails.items[0].owner.name
          const itemName = Utils.combineItemsName(this._rentalDetails.items)
          const date = this._currentChatData.order.date
          const message = owner + ' pre-approved rental of ' + itemName + ' for ' + date

          Promise.all([
            this._chatDoc.set({ status: 'approved' }, { merge: true }),
            this._chatDoc2.set({ status: 'approved' }, { merge: true })
          ])
            .then(() => {
              this._sendMessage(message, null, true, true, true, null, true)
              Navigator.hideLoading()
            })
            .catch(error => {
              Navigator.hideLoading()
              Navigator.showToast('Error', error.message, 'Error')
            })
        }
      })
    } else {
      Navigator.showToast('Error', '(CH - 309) Internal Error', 'Error')
    }
  }

  _handleReviewAction = () => {
    // if (this._rentalDetails && this._currentChatData) {
    //   Navigator.showLoading();
    //   this._changeRentalStatus(this._currentChatData.rental_id, "review", error => {
    //     if (error) {
    //       Navigator.hideLoading();
    //       Navigator.showToast("Error", "(Chat - 321) " + error.message, "Error");
    //     } else {
    //       this._chatDoc
    //         .set({ status: "rental_complete" }, { merge: true })
    //         .then(() => {
    //           Navigator.hideLoading();
    //         })
    //         .catch(error => {
    //           Navigator.hideLoading();
    //           Navigator.showToast("Error", error.message, "Error");
    //         });
    //     }
    //   });
    // } else {
    //   Navigator.showToast("Error", "(CH - 333) Internal Error", "Error");
    // }

    if (this._rentalDetails && this._currentChatData) {
      
      if (this._currentChatData.itemOwner) {
        const param: RentalRatingDetailParam = {
          chatData: this._currentChatData,
          rentalDetail: this._rentalDetails,
          title: '',
          item: null,
          onReviewed: this._handleReviewedAction
        }
        Navigator.navTo('ListerRentalRating', { param })
      } else {
        const param: RentalRatingParam = {
          chatData: this._currentChatData,
          rentalDetail: this._rentalDetails,
          onReviewItem: this._updateItemReviewStatus,
          onRetryUpdateReviewItem: this._updateItemReviewStatusRetry
        }
        Navigator.navTo('RentalRating', { param })
      }
    } else {
      Navigator.showToast('Error', '(CH - 333) Internal Error', 'Error')
    }
  }

  _handleReviewedAction = () => {
    Navigator.showLoading()
    this._changeRentalStatus(this._currentChatData.rental_id, 'review', error => {
      if (error) {
        Navigator.hideLoading()
        Navigator.showToast('Error', '(Chat - 343) ' + error.message, 'Error')
      } else {
        this._chatDoc
          .set({ status: 'reviewed' }, { merge: true })
          .then(() => {
            Navigator.hideLoading()
            Navigator.pop()
            Navigator.navTo('Inbox')
          })
          .catch(error => {
            Navigator.hideLoading()
            Navigator.showToast('Error', error.message, 'Error')
          })
      }
    })
  }

  _handleCancelWithoutPenalty = () => {
    this.setState({ loading: true })
    const { userProfile } = this.props
    this._changeRentalStatus(this._currentChatData.rental_id, 'cancel', error => {
      if (error) {
        this.setState({ loading: false })
        Navigator.showToast('Error', '(Chat - 365) ' + error.message, 'Error')
      } else {
        Promise.all([
          this._chatDoc.set({ status: 'cancelled' }, { merge: true }),
          this._chatDoc2.set({ status: 'cancelled' }, { merge: true })
        ])
          .then(() => {
            this._sendMessage(
              'The request was cancelled on ' +
              DateTime.moment().format('MMM DD, YYYY') +
              '. Any refunds will be processed according to the refund policy.',
              null,
              true,
              true,
              false,
              Utils.getUserName(userProfile) + ' cancelled a scheduled rental.',
              false
            )
            this.setState({ loading: false })
          })
          .catch(error => {
            this.setState({ loading: false })
            Navigator.showToast('Error', error.message, 'Error')
          })
      }
    })
  }

  _handleAcceptBuyNowAction = () => {
    this.setState({ loading: true })

    let isParial : boolean = false
    const { items } = this._rentalDetails;
    const chatData = this._currentChatData;
    const orders = chatData.orders
    
    let notActiveItem = []
    let returnItem = []

    
    const { purchaseData } = this.state
  if(purchaseData){
      let purhaseID = null;
      let allPurchase: any = null;
      purchaseData.map((item) => {
        if(item.status == "request"){
          purhaseID = item.id;
          allPurchase = item
        }
      })
      if(items.length > 0 && items[0].buy_now == true || items[0].buy_now == false){
        items.map((a_it) =>{
          if(!a_it.active){
              notActiveItem.push(a_it)
          }
        })
      }
        

      orders.map((it) =>{
          if(it.status == "returned"){
              returnItem.push(it)
          }
      })
      let purchasedProduct = []
      allPurchase.items.map((item) =>{
        purchasedProduct.push(item.item)
      })


      this._changeBuyNowStatus(purhaseID, 'confirm', error => {
        if (error) {
          Navigator.hideLoading()
          Navigator.showToast('Error', '(Chat - 491) ' + error.message, 'Error')
          this.setState({ loading:false })
          return false
        } else {
          this.checkMesg = "confirm"
          
         this._populateValuesFromChat()
              const { userProfile } = this.props
              Navigator.hideLoading()
              this.setState({ loading: false })
              this.setState({
                isRequested: false
              })
              AppsFlyerHelper.trackEvent(AppsFlyerHelper.EVENTS.BUY_NOW_TRANSACTION_COMPLETED, userProfile.id, purhaseID)
              
              GoogleAnalyticsHelper.trackEvent(GoogleAnalyticsHelper.EVENT_CATEGORY.MAIN, GoogleAnalyticsHelper.EVENT.BUY_NOW_TRANSACTION_COMPLETED,purhaseID)
              if(allPurchase.items.length === items.length){
                isParial = true;
                returnItem.length = 0
             }else{
                isParial = items.length === (returnItem.length + allPurchase.items.length + notActiveItem.length)
              }
              if(isParial){
                  if(returnItem.length > 0){
                      this._changeRentalStatus(purhaseID, 'returned', error => {
                      if (error) {
                          this.setState({ loading: false })
                          Navigator.showToast('Error', '(CH - 502) ' + error.message, 'Error')
                      } else {
                          Promise.all([
                          this._chatDoc.set({ status: 'returned' }, { merge: true }),
                          this._chatDoc2.set({ status: 'returned' }, { merge: true })
                          ])
                          .then(() => {
                              const itemName = Utils.combineItemsName(purchasedProduct)
                              this._sendMessage(
                              'The purchase request was accepted on ' + DateTime.moment(new Date()).format('MMM DD, YYYY'),
                              null,
                              true,
                              true,
                              false,
                              'Your request to purchase ' + itemName + ' for ' + this._currentChatData.order.date + ' is confirmed!',
                              false
                              )
                              this.setState({ loading: false })
                              this._closerReturnItems()
                          })
                          .catch(error => {
                              this.setState({ loading: false })
                              Navigator.showToast('Error', error.message, 'Error')
                          })
                          Navigator.hideLoading()
                      }
                      })
                  }else{
                      this._changeRentalStatus(this._currentChatData.rental_id, 'purchased', error => {
                      if (error) {
                          this.setState({ loading: false })
                          Navigator.showToast('Error', '(CH - 502) ' + error.message, 'Error')
                      } else {
                          Promise.all([
                          this._chatDoc.set({ status: 'purchased' }, { merge: true }),
                          this._chatDoc2.set({ status: 'purchased' }, { merge: true })
                          ])
                          .then(() => {
                              const itemName = Utils.combineItemsName(purchasedProduct)
                              this._sendMessage(
                              'The purchase request was accepted on ' + DateTime.moment(new Date()).format('MMM DD, YYYY'),
                              null,
                              true,
                              true,
                              false,
                              'Your request to purchase ' + itemName + ' for ' + this._currentChatData.order.date + ' is confirmed!',
                              false
                              )
                              this.setState({ loading: false })
                              this._closerReturnItems()
                          })
                          .catch(error => {
                              this.setState({ loading: false })
                              Navigator.showToast('Error', error.message, 'Error')
                          })
                      }
                      })
                  }
          
              }else{
                  const itemName = Utils.combineItemsName(purchasedProduct)
                  this._sendMessage(
                      'The purchase request was accepted on ' + DateTime.moment(new Date()).format('MMM DD, YYYY'),
                      null,
                      true,
                      true,
                      false,
                      'Your request to purchase item(s)' + itemName + ' for ' + this._currentChatData.order.date + ' is confirmed!',
                      false
                  )
                  this._closerReturnItems()
                  Navigator.hideLoading()
                  this.setState({
                    isRequested: false,
                    purchaseData: null
                  })
              }
        }
      })
   }else{
    Navigator.hideLoading()
    this.setState({ loading:false })
    Navigator.showToast('Error', "Could not do the action.", 'Error')
   }

  }

  _handleAcceptRental = () => {
    this.setState({ loading: true })
    this._changeRentalStatus(this._currentChatData.rental_id, 'confirm', error => {
      if (error) {
        this.setState({ loading: false })
        Navigator.showToast('Error', '(CH - 502) ' + error.message, 'Error')
      } else {
        AppsFlyerHelper.trackEvent(AppsFlyerHelper.EVENTS.RENTAL_ACCEPTED, this.props.userProfile.id, this._currentChatData.rental_id)
        GoogleAnalyticsHelper.trackEvent(GoogleAnalyticsHelper.EVENT_CATEGORY.MAIN, GoogleAnalyticsHelper.EVENT.RENTAL_ACCEPTED, { user_id: this.props.userProfile.id, rental_id: this._currentChatData.rental_id })
        Promise.all([
          this._chatDoc.set({ status: 'accepted' }, { merge: true }),
          this._chatDoc2.set({ status: 'accepted' }, { merge: true })
        ])
          .then(() => {
            const itemName = Utils.combineItemsName(this._rentalDetails.items)
            this._sendMessage(
              'The request was accepted on ' + DateTime.moment(new Date()).format('MMM DD, YYYY'),
              null,
              true,
              true,
              false,
              'Your request to rent ' + itemName + ' for ' + this._currentChatData.order.date + ' is confirmed!',
              false
            )
            this.setState({ loading: false })
            if (this._rentalDetails.free_delivery) {
              Navigator.showAlert(
                'Free Delivery Applied',
                'This rental qualifies for free delivery!\nA Rent My Wardrobe team member will reach out to you to coordinate item pick up.',
                () => {
                  // this._handleAcceptRental()
                },
                null,
                'Continue'
              )
            }
          })
          .catch(error => {
            this.setState({ loading: false })
            Navigator.showToast('Error', error.message, 'Error')
          })
      }
    })
  }

  _handleAcceptedAction = () => {
    const { userProfile } = this.props
    /**start comment: added by Mahesh 20 Sep 2019 to remove bank validation. */
    /*   if (userProfile.payment_account_id == null || userProfile.payment_account_id == '') {
        Navigator.showAlert(
          'Add a bank account',
          'To receive money for your rentals you will need to add a bank account.',
          () => {
            Navigator.navTo('ForceUpdateBankAccount')
            return
          },
          () => null,
          'Add Now',
          'Later'
        )

        return
      } else {
        this._handleAcceptRental()
      } */
    /**end comment: added by Mahesh 20 Sep 2019 to remove bank validation. */
    this._handleAcceptRental()
  }

  _handleApprovedAction = () => {
    if (this._rentalDetails && this._currentChatData) {

      this.setState({ loading: true })
      this._changeRentalStatus(this._currentChatData.rental_id, 'approved', error => {
        if (error) {
          this.setState({ loading: false })
          Navigator.showToast('Error', '(Chat - 434) ' + error.message, 'Error')
        } else {
          Promise.all([
            this._chatDoc.set({ status: 'approved' }, { merge: true }),
            this._chatDoc2.set({ status: 'approved' }, { merge: true })
          ])
            .then(() => {
              const owner = this._rentalDetails.items[0].owner.name
              const date = this._currentChatData.order.date
              const itemName = Utils.combineItemsName(this._rentalDetails.items)
              const message = owner + ' pre-approved rental of ' + itemName + ' for ' + date
              this._sendMessage(message, null, true, true, true, null, true)
              this.setState({ loading: false })
            })
            .catch(error => {
              this.setState({ loading: false })
              Navigator.showToast('Error', error.message, 'Error')
            })
        }
      })
    } else {
      Navigator.showToast('Error', 'Invalid Chat Data', 'Error')
    }
  }

  _onMessageLender = message => {
    this.setState({ loading: true })
    return new Promise<void>((resolve, reject) => {
      this._sendMessage(message, null, false, false, false, null, false, error => {
        if (error) {
          reject(error)
          return
        }
        this._changeRentalStatus(this._currentChatData.rental_id, 'return', error => {
          if (error) {
            reject(error)
            return
          }
        })
        return Promise.all([
          this._chatDoc.set({ status: 'returned' }, { merge: true }),
          this._chatDoc2.set({ status: 'returned' }, { merge: true })
        ])
          .then(() => {
            let newItems = []
            this._currentChatData.orders.map((it) => {
              if (it.status == "returned") {
                newItems.push(it)
              }
            })
            // const itemName = Utils.combineItemsName(this._rentalDetails.items)
            const itemName = Utils.combineItemsName(newItems)
            const verb = newItems.length ? 'were' : 'was'
            const date = DateTime.moment().format('MMM DD, YYYY')
            const itemLength = newItems.length ? 'items' : 'item'
            this._changeRentalStatus(this._currentChatData.rental_id, 'return')
            this._sendMessage(
              `${itemName} ${verb} returned on ${date}`,
              null,
              true,
              true,
              false,
              `Your ${itemLength} ${itemName} ${verb} just returned! `,
              false
            )
            resolve()
            this.setState({ loading: false })
          })
          .catch(error => {
            console.log('*** error', error)
            reject(error)
            this.setState({ loading: false })
          })
      })
    })
  }

  _handleReturnedAction = () => {
    if (this._currentChatData) {
      const getNonPurchasedChatData = this.getNonPurchasedChatData()
      console.log(getNonPurchasedChatData);
      const param: ReturnItemParam = {
        // chatData: this._currentChatData,
        chatData: getNonPurchasedChatData,
        onReturnItem: this._handlePhotoReturnSuccess,
        onRetryUpdateReturnItem: this._updateItemReturnStatusRetry,
        onMessageLender: this._onMessageLender,
        onReviewItem: this._updateItemReviewStatus,
        onRetryUpdateReviewItem: this._updateItemReviewStatusRetry,
        onCloseAction: this._closerReturnItems,
      }
      Navigator.navTo('ReturnItem', { param })
    } else {
      Navigator.showToast('Error', 'Invalid Chat Data', 'Error')
    }
  }
  _closerReturnItems = () => {
    this._currentChatData = this._currentChatData
    this._rentalDetails = this._rentalDetails
    this._populateValuesFromChat();
  }
  getNonPurchasedChatData = () => {
    const { items } = this._rentalDetails;
    const { purchaseData } = this.state;
    let chatData = this._currentChatData;
    let purchasedItems = [];
    let nonPurchasedItems = [];
    items.map((item, index) => {
      if(!purchaseData){
        if (!item.active && item.buy_now) {
          purchasedItems.push(item);
        }
      }else{
      let isRequestedItem = false;
       let requestedProd = purchaseData.find(req_item => req_item.status === "request" || req_item.status === "confirm")
       requestedProd.items.map((item_req) =>{
         if(item_req.item.id == item.item_id){
          isRequestedItem = true;
         }
       })  
      if (!item.active && item.buy_now && !isRequestedItem) {
        purchasedItems.push(item);
      }else{
        if (isRequestedItem) {
          purchasedItems.push(item);
        }
      }
    }
    })
    // console.log(this._rentalDetails.items)
    if (purchasedItems.length > 0) {
      this._currentChatData.orders.map((item, index) => {
        const np = purchasedItems.find(pItem => pItem.item_id === item.itemId)
        if (!np) {
          nonPurchasedItems.push(item);
        }
      }
      )
      if (nonPurchasedItems && nonPurchasedItems.length > 0) {
        chatData.orders = nonPurchasedItems;
      }
    }
    return chatData;
  }

  _handleBuyNowAction = () => {
    if (this._currentChatData) {
      const param: RentalItemDetailsParam = {
        chatData: this._currentChatData,
        rentalDetail: this._rentalDetails,
        onAction: this._performAction,
        purchaseAction: this._handleBuyNowActions
      }
      Navigator.navTo('CheckoutBuyNow', { param })
    } else {
      Navigator.showToast('Error', 'Invalid Chat Data', 'Error')
    }
  }

  

  _handleDeclinedAction = () => {
    Navigator.showAlert(
      'Decline Rental?',
      'Are you sure that you want to decline this rental?',
      () => null,
      () => {
        Navigator.showLoading()
        this._changeRentalStatus(this._currentChatData.rental_id, 'decline', error => {
          if (error) {
            Navigator.hideLoading()
            Navigator.showToast('Error', '(Chat - 491) ' + error.message, 'Error')
          } else {
            Promise.all([
              this._chatDoc.set({ status: 'declined' }, { merge: true }),
              this._chatDoc2.set({ status: 'declined' }, { merge: true })
            ])
              .then(() => {
                const { userProfile } = this.props
                this._sendMessage(
                  'The request was declined on ' + DateTime.moment().format('MMM DD, YYYY'),
                  null,
                  true,
                  true,
                  false,
                  Utils.getUserName(userProfile) + ' declined your rental request.',
                  false
                )
                Navigator.hideLoading()
              })
              .catch(error => {
                Navigator.hideLoading()
                Navigator.showToast('Error', error.message, 'Error')
              })
          }
        })
      },
      'CANCEL',
      'OK'
    )
  }

  _handleBuynowDeclinedAction = () => {
    const { purchaseData } = this.state
    if(purchaseData){
      Navigator.showAlert(
        'Decline Request?',
        'Are you sure that you want to decline this purchase request?',
        () => null,
        () => {
          Navigator.showLoading()
          let purhaseID = null;
          console.log("purchaseData",purchaseData)
          purchaseData.map((item) => {
            if(item.status == "request"){
              purhaseID = item.id;
            }
          })
          // Api.checkDetailsOfProduct
          // Api.checkDetailsOfBuyNowProduct(purhaseID)
          // .then((response: any) => {
          //   const { code, message, ...data } = response
          //   if (code !== 200) {
          //     throw Error(message || 'Fail to update decline status')
          //     Navigator.hideLoading()
          //   }
    
            
        
          this._changeBuyNowStatus(purhaseID, 'decline', error => {
            if (error) {
              Navigator.hideLoading()
              Navigator.showToast('Error', '(Chat - 491) ' + error.message, 'Error')
              // throw Error('Error', '(Chat - 491) ' + error.message, 'Error')
              return
            } else {
              // Promise.all([
              //   this._chatDoc.set({ status: 'declined' }, { merge: true }),
              //   this._chatDoc2.set({ status: 'declined' }, { merge: true })
              // ])
                // .then(() => {
                  this.checkMesg = "decline" 
                  const { userProfile } = this.props
                  this._sendMessage(
                    'The purchase request was declined on ' + DateTime.moment().format('MMM DD, YYYY'),
                    null,
                    true,
                    true,
                    false,
                    Utils.getUserName(userProfile) + ' declined your purchase request.',
                    false
                  )
                  Navigator.hideLoading()
                  this.setState({
                    isRequested:false,
                    isRequestedproduct: false
                  })
                // })
                // .catch(error => {
                //   Navigator.hideLoading()
                //   Navigator.showToast('Error', error.message, 'Error')
                // })
            }
          })
        // })
        },
        'CANCEL',
        'OK'
      )
    }else{
      Navigator.hideLoading()
      Navigator.showToast('Error', "Could not do the action.", 'Error')
     }
  }

  _handleCancelBuyNowAction = () => {
    const { purchaseData  } = this.state
    if(purchaseData){
      Navigator.showAlert(
        'Cancel Request?',
        'Are you sure that you want to cancel this purchase request?',
        () => null,
        () => {
          Navigator.showLoading()
          
          let purhaseID = null;
          
          purchaseData.map((item) => {
            if(item.status == "request"){
              purhaseID = item.id;
            }
          })
          
          this._changeBuyNowStatus(purhaseID, 'cancel', error => {
            if (error) {
              Navigator.hideLoading()
              Navigator.showToast('Error', '(Chat - 491) ' + error.message, 'Error')
              return
            } else {
              // Promise.all([
              //   this._chatDoc.set({ status: 'declined' }, { merge: true }),
              //   this._chatDoc2.set({ status: 'declined' }, { merge: true })
              // ])
                // .then(() => {
                  

                  this.checkMesg = "cancel"   
                  const { userProfile } = this.props
                  this._sendMessage(
                    'The purchase request was cancelled on ' + DateTime.moment().format('MMM DD, YYYY'),
                    null,
                    true,
                    true,
                    false,
                    Utils.getUserName(userProfile) + ' cancelled your purchase request.',
                    false
                  )
                  Navigator.hideLoading()
                  purchaseData.map((item) => {
                    if(item.status == "request"){
                      item.status = "cancel"
                    }
                  })
                  this._populateValuesFromChat()
                  // const { rental_id } = this.state
                  // this.getPurchaseDetails(rental_id);
                  this.setState({
                    isRequested: false,
                    isRequestedproduct: false,
                    purchaseData:null
                  })
                  

                // })
                // .catch(error => {
                //   Navigator.hideLoading()
                //   Navigator.showToast('Error', error.message, 'Error')
                // })
            }
          })
        },
        'CANCEL',
        'OK'
      )
    }else{
      Navigator.hideLoading()
      Navigator.showToast('Error', "Could not do the action.", 'Error')
     }
  }

  
  /******************************** END Perform action ******************************************/

  /******************************** START Helper functions ******************************************/
  _sendMessageToFromUser(text: string, image: string | null, time: number, isCustom: boolean) {
    const payload = Utils.createChatPayload(this.props.userProfile, text, image, time, isCustom)
    return this._chatDoc.collection('messages').add(payload)
  }

  _sendMessageToCurrentUser = (text: string, image: string | null, time: number, isCustom: boolean) => {
    const payload = Utils.createChatPayload(this.props.userProfile, text, image, time, isCustom)
    return this._chatDoc2.collection('messages').add(payload)
  }

  _updateChatDocumentForCurrentUser = (text: string, image: string | null, time: number) => {
    const payload = {
      lastMessage: image ? 'Image' : text,
      lastMessageTime: time,
      archived: false
    }
    return this._chatDoc.set(payload, { merge: true })
  }

  _updateChatDocumentForToUser = (text: string, image: string | null, unread: number, time: number) => {
    const payload = {
      lastMessage: image ? 'Image' : text,
      lastMessageTime: time,
      unread: unread,
      archived: false
    }
    return this._chatDoc2.set(payload, { merge: true })
  }

  _updatePushHistory = (text: string, image: string | null, statusChange: boolean, approval: boolean) => {
    const { userProfile } = this.props
    const { item } = this.state
    const name = Utils.getUserName(userProfile)

    const postData = {
      type: 'chat',
      toUser: this._otherUser.id,
      fromUser: userProfile.id,
      fromUserName: name,
      item: item.id,
      isOwner: this._currentChatData ? this._currentChatData.itemOwner : false,
      chatID: this._currentChatData ? this._currentChatData.id : '',
      rental_id: this._currentChatData.rental_id,
      statusChange: statusChange,
      approval: approval,
      chat: {
        text: image ? '📎Image' : text
      },
      itemIds: this._rentalDetails.items.map(item => item.id)
    }

    console.log('****** _updatePushHistory postData', postData)

    return firebase
      .firestore()
      .collection('pushHistory')
      .add(postData)
  }

  _sendMessage(
    text,
    image,
    isCustom,
    sendPush,
    statusChange,
    pushMessage,
    approval = false,
    callback?: (error: Error) => void
  ) {
    // const { rental_id } = this.state;
    // this.getPurchaseDetails(rental_id)
    const time = DateTime.getTimestamp()
    this._chatDoc2
      .get()
      .then(snapShot => {
        const data = snapShot.data() as ChatModelItem
        const newUnRead = data.unread ? data.unread + 1 : 1

        let promisses: any = [
          this._sendMessageToCurrentUser(text, image, time, isCustom),
          this._sendMessageToFromUser(text, image, time, isCustom),
          this._updateChatDocumentForCurrentUser(text, image, time),
          this._updateChatDocumentForToUser(text, image, newUnRead, time)
        ]
        if (sendPush) {
          promisses.push(this._updatePushHistory(pushMessage ? pushMessage : text, image, statusChange, approval))
        }
        return Promise.all(promisses)
      })
      .then(() => {
        if (typeof callback === 'function') {
          callback(null)
        }
      })
      .catch(error => {
        if (typeof callback === 'function') {
          callback(error)
        }
      })
  }

  /******************************** START Helper functions ******************************************/
  _onLoadEarlier = () => {
    const { firstMessageTime } = this.state
    this.setState({ isLoadingEarlier: true })

    this._chatDoc
      .collection('messages')
      .orderBy('time', 'desc')
      .startAt(firstMessageTime)
      .limit(20)
      .get()
      .then(snapShot => {
        let messages = []
        let firstMessageTime = null
        snapShot.forEach(child => {
          let data = child.data() as MessageModelItem

          if (data.time === this.state.firstMessageTime) return
          firstMessageTime = data.time

          const isImage = data.isImage
          const text = data.text
          const image = isImage ? data.image : null
          const user = Utils.createChatUser(data.sender, this.props.userProfile, this._otherUser)
          const message = {
            _id: data.messageId,
            text: text,
            image: image,
            createdAt: data.time,
            isCustom: data.isCustom,
            user
          }
          messages.push(message)
        })
        this.setState({
          loadEarlier: snapShot.size >= 20,
          isLoadingEarlier: false,
          firstMessageTime: firstMessageTime,
          messages: GiftedChat.prepend(this.state.messages, messages)
        })
      })
  }

  _loadMessages = (shouldSendFirstMessage: boolean) => {
    
    this._chatDoc
      .collection('messages')
      .orderBy('time', 'desc')
      .limit(20)
      .get()
      .then(snapShot => {
        let messages = []
        let lastMessageTime = null
        let firstMessageTime = null

        snapShot.forEach(child => {
          const data = child.data() as MessageModelItem

          if (lastMessageTime == null) {
            lastMessageTime = data.time
          }
          firstMessageTime = data.time
          const isImage = data.isImage
          const text = data.text
          const image = isImage ? data.image : null
          const user = Utils.createChatUser(data.sender, this.props.userProfile, this._otherUser)

          messages.push({
            _id: data.messageId,
            text: text,
            image: image,
            createdAt: data.time,
            isCustom: data.isCustom,
            user
          })
        })
        this.setState({
          messages:[]
        })
        // this.getPurchaseDetails(rental_id)

        const newState = {
          user: this._otherUser.name,
          loadEarlier: messages.length >= 20,
          lastMessageTime: lastMessageTime,
          firstMessageTime: firstMessageTime,
          messages: GiftedChat.append(this.state.messages, messages)
        }

        LayoutAnimations.setLayoutAnimation(LayoutAnimations.Spring)
        this.setState(newState, () => this._loadNewMessages(shouldSendFirstMessage))
      })
      .catch(error => { })
  }

  _loadNewMessages(shouldSendFirstMessage: boolean) {
    
    this._chatDoc.set({ unread: 0 }, { merge: true })
    const { message, rental_id } = this.state
    if (typeof message === 'string' && message.length > 0 && shouldSendFirstMessage) {
      this._sendMessage(message, null, false, false, false, null, false)
    }
    this._unsubscribe = this._chatDoc
      .collection('messages')
      .orderBy('time')
      .startAt(this.state.lastMessageTime)
      .onSnapshot(snapShot => {
        let lastMessageTime = this.state.lastMessageTime
        let messages = []
        // this.getPurchaseDetails(rental_id)
        snapShot.docChanges.forEach(change => {
          if (change.type !== 'added') return

          let data = change.doc.data() as MessageModelItem

          if (data.time == this.state.lastMessageTime) return

          lastMessageTime = data.time
          const isImage = data.isImage
          const text = data.text
          const image = isImage ? data.image : null
          const user = Utils.createChatUser(data.sender, this.props.userProfile, this._otherUser)

          // The purchase request

          /**check message regarding purchase request */
          const checkText = "The purchase request"
          const checkText_1 = "Request for purchase"
          const checkText_accept = "The purchase request was accepted"
          const textIndex = text ? text.indexOf(checkText) : 0;
          const textIndex_1 = text ? text.indexOf(checkText_1) : 0;
          const textIndex_A = text ? text.indexOf(checkText_accept) : 0;
          console.log("****Purchase index")
          console.log(textIndex)
          console.log(textIndex_1)
          if(textIndex >= 0 || textIndex_1 >= 0){
              this.getPurchaseDetails(rental_id)
          }
          if(textIndex_A >= 0){
            this._populateValuesFromChat()
          }

          messages.push({
            _id: data.messageId,
            text: text,
            image: image,
            createdAt: data.time,
            isCustom: data.isCustom,
            user
          })
        })

        if (messages.length > 0) {
          this._chatDoc.set({ unread: 0 }, { merge: true })
        }

        const newMessgaes = GiftedChat.append(this.state.messages, messages)

        
        
        this.setState({ lastMessageTime, messages: newMessgaes })
      })
  }

  _getSecondChatId(id1) {
    let idComponents = id1.split('_')

    return idComponents[1] + '_' + idComponents[0] + '_' + idComponents[2] + '_' + idComponents[3]
  }

  _populateValuesFromChat = () => {
    const { rental_id } = this.state
    const { userProfile } = this.props

    if (rental_id) {
      this.getPurchaseDetails(rental_id)
      Navigator.showLoading()
      this._conversationRef
        .where('rental_id', '==', rental_id)
        .get()
        .then(snapshot => {
          if (snapshot.size < 2) {
            throw Error('(CH - 761) Internal Error')
          }

          const chatDoc1 = snapshot.docs[0]
          const chatDoc2 = snapshot.docs[1]

          const docData1 = chatDoc1.data() as ChatModelItem
          const docData2 = chatDoc2.data() as ChatModelItem

          if (docData1.fromUser.id == userProfile.id) {
            this._otherUser = docData1.toUser
            this._chatDoc = chatDoc1.ref
            this._chatDoc2 = chatDoc2.ref
          } else {
            this._otherUser = docData2.toUser
            this._chatDoc = chatDoc2.ref
            this._chatDoc2 = chatDoc1.ref
          }

          this._subscribeForChatDataChange()
          this._loadMessages(false)
          return
        })
        .then(() => {
          return Api.getRentalDetail(rental_id)
        })
        .then((response: any) => {
          console.log(response);
          const { code, message, ...data } = response
          if (code !== 200) {
            throw Error(message || '(CH - 787) Internal Error.')
          }

          console.log('************* getRentalDetail response', response)
          console.log('************* getRentalDetail _currentChatData', this._currentChatData)
          this._rentalDetails = data
          this.checkBuyNowStatus = response.items[0].buy_now;
          console.log(this.checkBuyNowStatus);
          console.log(this._rentalDetails)
          Navigator.hideLoading()

          if (this.state.statusChange) {
            this._viewDetails()
          }
        })
        .catch(error => {
          Navigator.hideLoading()
          console.log('**** error: ', error)
        })
    } else {
      Navigator.showToast('Error', 'Invalid Rental Id', 'Error', 2000, () => null, () => Navigator.back(), true)
    }
  }

  _uploadReturnPhoto = (sourceURL, callback) => {
    FirebaseHelper.uploadChatPhoto(sourceURL)
      .then(url => {
        callback(url)
      })
      .catch(error => {
        callback(sourceURL)
      })
  }

  _uploadReturnPhotos(photos: Array<ReturnPhotoModel>, passedIndex: number, completion: () => void) {
    if (photos.length > passedIndex) {
      this._uploadReturnPhoto(photos[passedIndex].image, url => {
        this._sendMessage(photos[passedIndex].title, url, false, false, false, null, false)
        this._uploadReturnPhotos(photos, passedIndex + 1, completion)
      })
    } else {
      completion()
    }
  }

  _changeRentalStatus = (rentalId: number, status: string, callback?: (error: Error) => void) => {
    Api.editRequestRental({ id: rentalId, status: status })
      .then((response: any) => {
        const { code, message, ...data } = response
        if (code !== 200) {
          throw Error(message || 'Fail to update rental status')
        }

        this._rentalDetails = data

        if (typeof callback === 'function') {
          callback(null)
        }
      })
      .catch(error => {
        if (typeof callback === 'function') {
          callback(error)
        }
      })
  }

  _changeBuyNowStatus = (id: number, status: string, callback?: (error: Error) => void) => {
    Api.editBuyNowRequest({ id: id, status: status })
      .then((response: any) => {
        console.log("response", response)
        const { code, message } = response
        if (code !== 200) {
          const { rental_id } = this.state
          this.setState({ loading:false })
          // this.getPurchaseDetails(rental_id)
          throw Error(message || 'Fail to update rental status')
        }

        // this._rentalDetails = data

        if (typeof callback === 'function') {
          callback(null)
        }
        Navigator.hideLoading()
      })
      .catch(error => {
        if (typeof callback === 'function') {
          callback(error)
        }
      })
  }

  _onSendMessage = messages => this._sendMessage(messages[0].text, null, false, true, false, null, false)

  _showDeleteConfirmation = () => {
    const { archived } = this.state
    const title = archived ? 'Unarchive this chat?' : 'Archive this chat?'
    const message = archived
      ? 'Are you sure you want to unarchive this chat?'
      : 'Are you sure you want to archive this chat?'
    Navigator.showAlert(
      title,
      message,
      () => null,
      () => {
        Navigator.showLoading()
        this._chatDoc.set({ archived: !archived }, { merge: true }).then(() =>
          this.setState(() => {
            Navigator.hideLoading(() => Navigator.back())
          })
        )
      },
      'CANCEL',
      'OK'
    )
  }

  _viewDetails = () => {
    console.log('**** this._currentChatData', this._currentChatData)
    console.log('**** this._rentalDetails: ', this._rentalDetails)
    if (this._currentChatData && this._rentalDetails) {
      const param: RentalItemDetailsParam = {
        chatData: this._currentChatData,
        rentalDetail: this._rentalDetails,
        onAction: this._performAction
      }
      Navigator.navTo('RentalItemDetails', param)
    } else {
      Navigator.showToast('Error', '(CH - 883) Internal Error', 'Error')
    }
  }

  _onUploadPhoto = url => {
    Navigator.showLoading()
    FirebaseHelper.uploadChatPhoto(url)
      .then(uploadedUrl => {
        this._sendMessage(null, uploadedUrl, false, true, false, null, false, error => {
          if (error) {
            throw error
          }
          Navigator.hideLoading()
        })
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  /******************************* START Render components *******************************/
  _performAction = (action: RentalStatus) => {
    console.log('**** _performAction', action)
    if(action == 'buynow_accepted'){
      this._populateValuesFromChat()
    }

    if (this._currentChatData) {
      let _anData = this._currentChatData
      if (Object.isFrozen(_anData)) {
        Object.freeze(_anData)
      }
      let anData = Object.assign({}, _anData)
      anData['action'] = action
      FirebaseHelper.logEvent('PerformRentalAction', anData)
    }

    switch (action) {
      case 'rent':
        this._handleRentAction()
        break
      case 'declined':
        this._handleDeclinedAction()
        break
      case 'cancelled':
        this._handleCancelAction()
        break
      case 'approved':
        this._handleApprovedAction()
        break
      case 'returned':
        this._handleReturnedAction()
        break
      case 'buy':
        this._handleBuyNowAction()
        break
      case 'approve':
        this._handleApproveAction()
        break
      case 'review':
        this._handleReviewAction()
        break
      case 'reviewed':
        this._handleReviewedAction()
        break
      case 'cancel_without_penalty':
        this._handleCancelWithoutPenalty()
        break
      case 'accepted':
        this._handleAcceptedAction()
        break
      case 'buynow_declined':
        this._handleBuynowDeclinedAction()
        break
      case 'buynow_accepted':
        this._handleAcceptBuyNowAction()
        break
      case 'buynow_cancel':
        this._handleCancelBuyNowAction()
        break
    }
  }

  // _handlePhotoReturnSuccess = (photos: ReturnPhotoModel[], message: string) => {
  //   // Navigator.pop(2, false);
  //   // this.setState({ loading: true });
  //   this._changeRentalStatus(this._currentChatData.rental_id, "return", error => {
  //     if (error) {
  //       Navigator.showToast("Error", "(Chat - 1069) " + error.message, "Error");
  //     } else {
  //       this._sendMessage(
  //         "The item is being returned. Current condition: " + message,
  //         null,
  //         false,
  //         false,
  //         false,
  //         null,
  //         false,
  //         () => {
  //           this._uploadReturnPhotos(photos, 0, () => {
  //             Promise.all([
  //               this._chatDoc.set({ status: "returned" }, { merge: true }),
  //               this._chatDoc2.set({ status: "returned" }, { merge: true })
  //             ])
  //               .then(() => {
  //                 // this.setState({ loading: false });
  //                 this._sendMessage(
  //                   "Item was returned on " + DateTime.moment().format("MMM DD, YYYY"),
  //                   null,
  //                   true,
  //                   true,
  //                   false,
  //                   "Sucess! Your rental is checked in! Rate your experience",
  //                   false,
  //                   error => {
  //                     if (error) {
  //                       throw error;
  //                     }
  //                     // this.setState({ loading: false });
  //                     // Navigator.hideLoading();
  //                     // Navigator.navTo("ActionFinal", {
  //                     //   title: "Your rental is complete",
  //                     //   description:
  //                     //     "Thanks for returning the item, we’ll notify the lender. Now, leave a review on the item so others may know about your experience.",
  //                     //   buttonTitle: "Leave a Review",
  //                     //   onActionButton: () => {
  //                     //     const param: RentalRatingParam = {
  //                     //       type: "rentalFinal",
  //                     //       itemOwner: this._currentChatData.itemOwner,
  //                     //       data: this._currentChatData,
  //                     //       order: this._rentalDetails,
  //                     //       onCompletion: () => {
  //                     //         this._performAction("reviewed");
  //                     //         Navigator.back();
  //                     //         Navigator.navTo("Inbox");
  //                     //       }
  //                     //     };
  //                     //     Navigator.back();
  //                     //     Navigator.navTo("RentalRating", { param });
  //                     //   }
  //                     // });
  //                   }
  //                 );
  //               })
  //               .catch(error => {
  //                 this.setState({ loading: false });
  //                 Navigator.showToast("Error", error.message, "Error");
  //               });
  //           });
  //         }
  //       );
  //     }
  //   });
  // };

  _handlePhotoReturnSuccess = (
    photos: ReturnPhotoModel[],
    message: string,
    item: ChatModelOrderItem,
    isComplete = false
  ) => {
    return new Promise<ChatModelItem>((resolve, reject) => {
      this.setState({ loading: true })
      this._sendMessage(
        `The item ${item.itemName} is being returned. Current condition: ${message}`,
        null,
        false,
        false,
        false,
        null,
        false,
        error => {
          if (error) {
            reject(error)
          }
          this._uploadReturnPhotos(photos, 0, () => {
            this._updateItemReturnStatus(item, isComplete)
              .then(data => {
                resolve(data)
                this.setState({ loading: false })
              })
              .catch(error => {
                this.setState({ loading: false })
                reject(error)
              })
          })
        }
      )
    })
  }

  _renderFooterForRenting = () => {
    if(this._currentChatData){
      if (this._currentChatData == null || this._currentChatData.itemOwner) {
        return null
      }
    }

    return (
      <Button
        style={styles.positiveButton}
        textStyle={styles.positiveButtonText}
        text="Return Item"
        onPress={this._handleReturnedAction}
      />
    )
  }

  _renderFooterForBuyNow = () => {
    if(this._currentChatData){
      if (this._currentChatData == null || this._currentChatData.itemOwner) {
        return null
      }
    }
    // if (this._currentChatData == null || this._currentChatData.itemOwner) {
    //   return null
    // }

    return (
      <Button
        style={styles.positiveButtonBuyNow}
        textStyle={styles.positiveButtonText}
        text="Request to Buy"
        onPress={this._handleBuyNowAction}
      />
    )
  }

  // _renderFooterForCancelBuyNowRequest = () => {
  //   if (this._currentChatData == null || this._currentChatData.itemOwner) {
  //     return null
  //   }

  //   return (
  //     <Button
  //       style={styles.negativeButton}
  //       text="Cancel Request"
  //       textStyle={styles.negativeButtonText}
  //       onPress={this._handleBuyNowCancelAction}
  //     />
  //   )
  // }

  _renderFooterForAccepted() {
    if(this._currentChatData){
      if (this._currentChatData == null || this._currentChatData.itemOwner) {
        return null
      }
    }
    // if (this._currentChatData == null || this._currentChatData.itemOwner) {
    //   return null
    // }

    return (
      <Button
        style={styles.positiveButton}
        textStyle={styles.positiveButtonText}
        text="Return Item"
        onPress={this._handleReturnedAction}
      />
    )
  }

  _renderFooterForPreApprove = () => {
    const onPress = () => this._performAction('approve')
    return (
      <Button
        style={styles.positiveButton}
        textStyle={styles.positiveButtonText}
        text="Pre-approve Rental"
        onPress={onPress}
      />
    )
  }

  _renderFooterForApproved = () => (
    <Button
      style={styles.positiveButton}
      textStyle={styles.positiveButtonText}
      text="Rent Now"
      onPress={this._handleRentAction}
    />
  )

  _renderFooterForReview = () => {
    // const onPress = () => {
    //   console.log("this._currentChatData.itemOwner", this._currentChatData.itemOwner);
    //   const param: RentalRatingParam = {
    //     itemOwner: this._currentChatData.itemOwner,
    //     chatData: this._currentChatData,
    //     rentalDetail: this._rentalDetails,
    //     onCompletion: () => {
    //       this._performAction("reviewed");
    //       Navigator.navTo("Inbox");
    //     }
    //   };
    //   Navigator.navTo("RentalRating", { param });
    // };

    const onPress = () => this._performAction('review')

    return (
      <Button
        style={styles.positiveButton}
        textStyle={styles.positiveButtonText}
        text="Write Review"
        onPress={onPress}
      />
    )
  }

  _renderFooterForReturn() {
    const onPress = () => this._performAction('returned')
    return (
      <Button
        style={styles.positiveButton}
        textStyle={styles.positiveButtonText}
        text="Return Item"
        onPress={onPress}
      />
    )
  }

  _renderFooterBuyNow() {
    const onPress = () => this._performAction('buy')
    return (
      <Button
        style={styles.positiveButton}
        textStyle={styles.positiveButtonText}
        text="Request to Buy"
        onPress={onPress}
      />
    )
  }



  _renderFooterForDeclined() {
    return null
  }

  _renderFooterForCancelRental() {
    if(this._currentChatData){
      if (this._currentChatData == null || this._currentChatData.itemOwner) {
        return null
      }
    }
    // if (this._currentChatData == null || this._currentChatData.itemOwner) {
    //   return null
    // }
    const onPress = () => this._performAction('cancel_without_penalty')
    return (
      <Button
        style={styles.negativeButton}
        textStyle={styles.negativeButtonText}
        text="Cancel Rental"
        onPress={onPress}
      />
    )
  }

  _renderFooterForRequest() {
    if (this._currentChatData == null) {
      return null
    }
    const isItemOwner = this._currentChatData.itemOwner

    if (isItemOwner) {
      const onDecline = () => this._performAction('declined')
      const onAccept = () => this._performAction('accepted')
      return (
        <Row alignHorizontal="space-between" alignVertical="center">
          <Button
            style={[{ marginRight: 10, flex: 1 }, styles.negativeButton]}
            textStyle={styles.negativeButtonText}
            text="Decline"
            onPress={onDecline}
          />
          <Button
            style={[{ marginLeft: 10, flex: 1 }, styles.positiveButton]}
            textStyle={styles.positiveButtonText}
            text="Accept"
            onPress={onAccept}
          />
        </Row>
      )
    }

    return (
      <Button
        style={styles.negativeButton}
        text="Cancel Request"
        textStyle={styles.negativeButtonText}
        onPress={this._handleCancelAction}
      />
    )
  }

  _renderFooterForBuyNowRequest() {
    if (this._currentChatData == null) {
      return null
    }
    const isItemOwner = this._currentChatData.itemOwner

    if (isItemOwner) {
      //const onDecline = () => this._performAction('declined')
      //const onAccept = () => this._performAction('accepted')

      const onDecline = () => this._performAction('buynow_declined')
      const onAccept = () => this._performAction('buynow_accepted')
       return (
        <Row alignHorizontal="space-between" alignVertical="center">
          <Button
            style={[{ marginRight: 10, flex: 1 }, styles.negativeButton]}
            textStyle={styles.negativeButtonText}
            text="Decline"
            onPress={onDecline}
          />
          <Button
            style={[{ marginLeft: 10, flex: 1 }, styles.positiveButton]}
            textStyle={styles.positiveButtonText}
            text="Accept"
            onPress={onAccept}
          />
        </Row>
       )
    } 
    const onCancel = () => this._performAction('buynow_cancel')
      return (
        <Button
          style={[{marginTop:10}, styles.negativeButton]}
          text="Cancel Request"
          textStyle={styles.negativeButtonText}
          onPress={onCancel}
        />
      )
  }

  // _renderFooterForBuyNowRequestOwner = () => {
  //   const onDecline = () => this._performAction('declined')
  //     const onAccept = () => this._performAction('accepted')
  //     return (
  //       <Row alignHorizontal="space-between" alignVertical="center">
  //         <Button
  //           style={[{ marginRight: 10, flex: 1 }, styles.negativeButton]}
  //           textStyle={styles.negativeButtonText}
  //           text="Decline"
  //           onPress={onDecline}
  //         />
  //         <Button
  //           style={[{ marginLeft: 10, flex: 1 }, styles.positiveButton]}
  //           textStyle={styles.positiveButtonText}
  //           text="Accept"
  //           onPress={onAccept}
  //         />
  //       </Row>
  //     )
  // }

  

  _renderChatFooter = () => {
    let { showFooter, statusValue, isRequested, purchaseData,rental_id,isRequestedproduct } = this.state
    //  this.getPurchaseDetails(rental_id);
    let purchaseStatus = ''
    if(this._currentChatData == null){
      return false
    }
    

    if (statusValue === 'request' ) {
      showFooter = this._currentChatData.itemOwner
      console.log("showFooter", showFooter)
    }else{
      if (statusValue === 'renting' && showFooter === false) {
        showFooter = this._currentChatData.itemOwner
        console.log("showFooter", showFooter)
      }
    }
    let requestedProdList = ''
    let showReturnButton = true;
    if(purchaseData){
      if (this._rentalDetails) {
        const { items } = this._rentalDetails;
        if(items.length == 1){
          showReturnButton = false;
        }
      }
      
      // showReturnButton = false;
      // requestedProdList = purchaseData.find(req_item => req_item.status === "request")

    }else{
      showReturnButton = true;
    }

    // if(this.checkMesg === "cancel"){
    //     if(isRequested){
    //       isRequested = false
    //     }
    // }else if(this.checkMesg === "decline"){
    //   if(isRequested){
    //     isRequested = false
    //   }

    // }else if(this.checkMesg === "confirm"){
    //   if(isRequested){
    //     isRequested = false
    //   }
    // }
    console.log(isRequested)
    //if(statusValue === "" && showFooter === false && isRequested){
      //{this._renderFooterForBuyNowRequestOwner()}
    // }
    let buy_now: boolean = false;
    // let isRequestedproduct: boolean = false
    let itemLength = 0
    const { orders } = this._currentChatData
    if (this._rentalDetails) {
      const { items } = this._rentalDetails;
       itemLength = items.length
      // buy_now = items.find(item => item.buy_now && item.active)
      items.map((item: any) => {
        console.log(item)
        if (item.buy_now && item.active) {
          const return_item = orders.find(or_item => or_item.itemId == item.item_id && or_item.status == "returned")
          if(return_item){
            
          }else{
            buy_now = true
          }
            
          // return
          
        }
      }
      )
    }
    console.log("Buynow", buy_now)
    // if(!purchaseData){
    //   if(isRequested){
    //     isRequested = false
    //   }else{
    //     isRequested = true
    //   }
    // }
    // if(requestedProdList){
    //   if(requestedProdList.items.length === itemLength){
    //     isRequestedproduct = true
       
    //   }
    // }

    if (showFooter) {
      return (
        <View style={styles.footerContainer}>
          {statusValue === 'cancel_without_penalty' && this._renderFooterForCancelRental()}
          {statusValue === 'request' && this._renderFooterForRequest()}
          {statusValue === 'approved' && this._renderFooterForApproved()}
          {statusValue === 'accepted' && this._renderFooterForAccepted()}
          {statusValue === 'renting' &&  !purchaseData && this._renderFooterForRenting()}
          {/* {statusValue === 'renting' && buy_now === true && this._renderFooterForBuyNow()}  && !isRequested */}
          {(statusValue === 'renting' && buy_now && !purchaseData) && this._renderFooterForBuyNow()}
          {(statusValue === 'renting' && isRequested) && this._renderFooterForBuyNowRequest()}
          
          {statusValue === 'returned' && this._renderFooterForReview()}
          {statusValue === 'purchased' && this._renderFooterForReview()}
          {statusValue === 'inquiry' && this._renderFooterForPreApprove()}
        </View>
      )
    }

    return null
  }

  render() {
    const { user, statusValue, archived,rental_id } = this.state
    const { userProfile } = this.props
    let imageSource = archived ? Assets.images.trash : Assets.images.chatArchive
    let color = Assets.colors.textLight
    let statusText = Utils.getStatusText(statusValue, this._rentalDetails)

    if (statusValue === 'renting' && this._currentChatData && this._currentChatData.order) {
      let due = Utils.checkDue(this._currentChatData.order.endDate)

      if (due) {
        color = Assets.colors.appTheme
        statusText = due
      }
    }

    //  this.getPurchaseDetails(rental_id);

    return (
      <View style={[StyleSheet.absoluteFill, { backgroundColor: Assets.colors.componentBg }]}>
        <Header
          style={styles.headerView}
          statusBarProps={{ barStyle: 'dark-content' }}
          LeftComponent={<BackButton />}
          title={user}
          titleStyle={Styles.headerTitle}
          rightContainerStyle={{ alignItems: 'flex-end' }}
          RightComponent={
            <Icon
              iconSource={imageSource}
              iconStyle={{ tintColor: Assets.colors.borderColor, width: 24, height: 24 }}
              onPress={this._showDeleteConfirmation}
            />
          }>
          <Row style={styles.titleView} alignHorizontal="space-between" alignVertical="center">
            <Text style={[styles.statusText, { color }]} text={`Status: ${statusText}`} />
            <Text style={styles.detailsButtonText} text="Details" onPress={this._viewDetails} />
          </Row>
        </Header>
        <GiftedChat
          messages={this.state.messages}
          onSend={this._onSendMessage}
          user={{ _id: userProfile.id }}
          onLoadEarlier={this._onLoadEarlier}
          loadEarlier={this.state.loadEarlier}
          isLoadingEarlier={this.state.isLoadingEarlier}
          onUploadPhoto={this._onUploadPhoto}
          renderChatFooter={this._renderChatFooter}
        />

        <Modal visible={this.state.loading} transparent animationType="none">
          <View style={[StyleSheet.absoluteFill, { backgroundColor: 'rgba(0,0,0,0.2)' }]}>
            <View style={{ marginTop: 160, alignItems: 'center' }}>
              <Bubbles size={7} color="#F06182" />
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  statusText: {
    marginLeft: 15,
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.textLight
  },
  detailsButtonText: {
    marginRight: 15,
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.appTheme
  },
  headerView: {
    backgroundColor: 'white',
    ...Platform.select({
      android: {
        elevation: 4
      },
      ios: {
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.15,
        shadowRadius: 3
      }
    })
  },
  titleView: {
    height: 44,
    marginBottom: 5
  },

  positiveButtonBuyNow: {
    // margin: 10,
    marginTop: 10
  },
  positiveButton: {},
  positiveButtonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: 'white',
    paddingVertical: 8
  },
  negativeButton: {
    backgroundColor: 'white',
    borderRadius: 6,
    borderWidth: 1,
    borderColor: Assets.colors.borderColor
  },
  negativeButtonText: {
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.display.regular,
    fontSize: 14,
    paddingVertical: 8
  },
  footerContainer: {
    borderTopColor: Assets.colors.borderColor,
    borderTopWidth: 1,
    paddingVertical: 15,
    paddingHorizontal: 20
  }
})

const mapStateToProps = (state: StoreState) => {
  return {
    userProfile: state.userProfile,
    paymentMethods: state.payments.paymentMethods
  }
}

export default connect(mapStateToProps)(Chat)
