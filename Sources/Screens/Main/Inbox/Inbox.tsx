import React from 'react'
import { StyleSheet, View, StatusBar } from 'react-native'
import { Header, Icon } from 'rn-components'
import { Navigator, getRouteName } from '@Navigation'
import { StoreState } from '@ReduxManager'
import { connect } from 'react-redux'
import { NavigationProps } from '@Types'
import { UserProfile, ChatModelItem, ChatParam } from '@Models'
import { EmptyView, RefreshControl, EmptyViewTwoButton } from '@Components'
import { FirebaseHelper } from '@Services'
import { LayoutAnimations, Constants } from '@Utils'
import InboxItem from './InboxItem'
import { LayoutProvider, DataProvider, RecyclerListView } from 'recyclerlistview'
import Assets from '@Assets'
import Styles from '@Styles'

type Props = {
  navigation: NavigationProps
  userProfile: UserProfile
}

type State = {
  items: any[]
  showingArchive: boolean
  loading: boolean
  dataProvider: DataProvider
  pageIndex: 1
  refreshing: boolean
}
class Inbox extends React.Component<Props, State> {
  _dataSource = []
  _unsubscribe = null
  _layoutProvider: LayoutProvider = null
  _timerHandler = null
  _adminTimerHandler = null
  _adminUnsubscribe = null
  constructor(props) {
    super(props)

    const dataRow: any = new DataProvider((r1, r2) => r1 !== r2)
    const dataProvider = dataRow.cloneWithRows([])

    this._layoutProvider = new LayoutProvider(
      index => 1,
      (type, dim) => {
        dim.width = Constants.WINDOW_WIDTH
        dim.height = Constants.INBOX_ITEM_HEIGHT
      }
    )

    LayoutAnimations.enableAndroidLayoutAnimation()

    this.state = {
      items: [],
      showingArchive: false,
      loading: true,
      dataProvider,
      pageIndex: 1,
      refreshing: false
    }
  }

  componentWillMount() {
    const { userProfile } = this.props
    // userProfile.role != 'admin'
    StatusBar.setBarStyle('dark-content')
    this._loadMessages(false, () => userProfile.role != 'admin' && this._loadAdminMessages(false))
  }

  componentWillUnmount() {
    this._unsubscribe && this._unsubscribe()
    this._adminUnsubscribe && this._adminUnsubscribe()
    this._timerHandler && clearTimeout(this._timerHandler)
    this._adminTimerHandler && clearTimeout(this._adminTimerHandler)
  }

  _onRefresh = () => {
    Navigator.showLoading()
    const { dataProvider, showingArchive } = this.state
    const { userProfile } = this.props

    this.setState({ refreshing: true })
    FirebaseHelper.getConversations()
      .where('archived', '==', showingArchive)
      .where('fromUser.id', '==', userProfile.id)
      // .limit(300)
      .get()
      .then(snapshoot => {
        let items: ChatModelItem[] = []
        snapshoot.forEach(doc => {
          const item = doc.data() as ChatModelItem
          items.push(item)
        })
        items.sort((a, b) => b.lastMessageTime - a.lastMessageTime)

        this.setState({ refreshing: false, items, dataProvider: dataProvider.cloneWithRows(items) })
        Navigator.hideLoading()
      }).then(() => {
        if (userProfile.role != 'admin') {
          this._loadAdminOnRefresh()
        }
      })
      .catch(error => {
        this.setState({ refreshing: false })
        Navigator.hideLoading()
      })
  }
  /**fetch admin conversation on refresh added by mahesh@fathomable on 14 Nov 19 */
  _loadAdminOnRefresh() {
    // Navigator.showLoading()
    const { dataProvider, showingArchive } = this.state
    const { userProfile } = this.props
    this.setState({ refreshing: true })
    let items = [...this.state.items]
    FirebaseHelper.getAdminConversations()
      .where('archived', '==', showingArchive)
      .where('fromUser.id', '==', userProfile.id)
      // .limit(300)
      .get()
      .then(snapshoot => {
        // let items: ChatModelItem[] = []
        snapshoot.forEach(doc => {
          const item = doc.data() as ChatModelItem
          items.push(item)
        })
        items.sort((a, b) => b.lastMessageTime - a.lastMessageTime)

        this.setState({ refreshing: false, items, dataProvider: dataProvider.cloneWithRows(items) })
        Navigator.hideLoading()
      })
      .catch(error => {
        this.setState({ refreshing: false })
        Navigator.hideLoading()
      })
  }

  _loadMessages(archived: boolean, callback?: () => void) {
    const currentRouteName = getRouteName(this.props.navigation)
    currentRouteName == 'Inbox' && Navigator.showLoading()
    const { userProfile } = this.props

    let items = [...this.state.items]
    this._unsubscribe && this._unsubscribe()
    this._unsubscribe = FirebaseHelper.getConversations()
      .where('archived', '==', archived)
      .where('fromUser.id', '==', userProfile.id)
      .onSnapshot(snapshot => {
        snapshot.docChanges.forEach(change => {
          let data: any = change.doc.data()

          if (data == null) return

          data.id = change.doc.id

          if (change.type === 'added') {
            items.push(data)
          } else if (change.type === 'modified') {
            const index = items.findIndex(item => item.id === change.doc.id)
            items.splice(index, 1, data)
          } else if (change.type === 'removed') {
            const index = items.findIndex(item => item.id === change.doc.id)
            items.splice(index, 1)
          }
        })

        items.sort((chat1, chat2) => {
          if (chat1.lastMessageTime == null) return -1
          if (chat2.lastMessageTime == null) return -1
          if (chat1.lastMessageTime > chat2.lastMessageTime) return -1
          if (chat1.lastMessageTime < chat2.lastMessageTime) return 1
        })

        currentRouteName == 'Inbox' && LayoutAnimations.setLayoutAnimation(LayoutAnimations.ListItem)
        this._timerHandler = setTimeout(() => {
          this.setState({ items, dataProvider: this.state.dataProvider.cloneWithRows(items), loading: false })
          currentRouteName == 'Inbox' && Navigator.hideLoading()
          if (typeof callback === 'function') {
            callback()
          }
        }, 300)
      })
  }
  /**load admin message on component mount added by mahesh@fathomable on 14 nov 19 */
  _loadAdminMessages(archived: boolean, callback?: () => void) {
    const currentRouteName = getRouteName(this.props.navigation)
    // currentRouteName == 'Inbox' && Navigator.showLoading()
    const { userProfile } = this.props

    let items = [...this.state.items]
    this._adminUnsubscribe && this._adminUnsubscribe()
    this._adminUnsubscribe = FirebaseHelper.getAdminConversations()
      .where('archived', '==', archived)
      .where('fromUser.id', '==', userProfile.id)
      .onSnapshot(snapshot => {
        snapshot.docChanges.forEach(change => {
          let data: any = change.doc.data()

          if (data == null) return

          data.id = change.doc.id

          if (change.type === 'added') {
            items.push(data)
          } else if (change.type === 'modified') {
            const index = items.findIndex(item => item.id === change.doc.id)
            items.splice(index, 1, data)
          } else if (change.type === 'removed') {
            const index = items.findIndex(item => item.id === change.doc.id)
            items.splice(index, 1)
          }
        })

        items.sort((chat1, chat2) => {
          if (chat1.lastMessageTime == null) return -1
          if (chat2.lastMessageTime == null) return -1
          if (chat1.lastMessageTime > chat2.lastMessageTime) return -1
          if (chat1.lastMessageTime < chat2.lastMessageTime) return 1
        })

        currentRouteName == 'Inbox' && LayoutAnimations.setLayoutAnimation(LayoutAnimations.ListItem)
        this._adminTimerHandler = setTimeout(() => {
          this.setState({ items, dataProvider: this.state.dataProvider.cloneWithRows(items), loading: false })
          currentRouteName == 'Inbox' && Navigator.hideLoading()
          if (typeof callback === 'function') {
            // callback()
          }
        }, 300)
      })
  }

  _showDeleteConfirmation = (item: ChatModelItem, index) => {
    return new Promise<boolean>((resolve, reject) => {
      const archive = item.archived
      const title = archive ? 'Unarchive this chat?' : 'Archive this chat?'
      const message = archive
        ? 'Are you sure you want to unarchive this chat?'
        : 'Are you sure you want to archive this chat?'
      Navigator.showAlert(
        title,
        message,
        () => null,
        () => {
          Navigator.showLoading()
          this.setState({ loading: true })
          FirebaseHelper.getConversations()
            .doc(item.id)
            .set({ archived: !archive }, { merge: true })
            .then(() => {
              resolve(true)
              this.setState({ loading: false })
              Navigator.hideLoading()
            })
            .catch(error => {
              resolve(true)
              this.setState({ loading: false })
              Navigator.hideLoading()
            })
        },
        'CANCEL',
        'OK'
      )
    })
  }

  _onTapSwipeItem = (item, index) => {
    return this._showDeleteConfirmation(item, index)
  }

  _onTapAvatar = user => Navigator.navTo('PublicProfile', { user }) //user: { id: user.id }

  _onTapChatItem = item => {
    if ((item.rental_id === null || item.rental_id === undefined) && (item.chat_id === null || item.chat_id === undefined)) {
      Navigator.showToast('Error', '(Inbox-147) Invalid Rental Id! Please contact support.', 'Error')
      return
    } else if (item.rental_id) {
      const param: ChatParam = {
        rental_id: item.rental_id,
        item: { id: item.itemId },
        fromPush: false,
        fromUser: item.fromUser.id,
        toUser: item.toUser.id,
        isOwner: item.itemOwner,
        chatID: item.id,
        message: item.lastMessage
      }
      Navigator.navTo('Chat', { param })
    }
    else {
      this._onTapAdminChatItem(item)
    }
  }
  /**added by mahehs@fathomable to redirect on admin chat */
  _onTapAdminChatItem = item => {
    if (item.chat_id === null || item.chat_id === undefined) {
      Navigator.showToast('Error', '(Inbox-147) Invalid Rental Id! Please contact support.', 'Error')
      return
    } else {
      const param: ChatParam = {
        rental_id: item.chat_id,
        item: { id: item.itemId },
        fromPush: false,
        fromUser: item.fromUser.id,
        toUser: item.toUser.id,
        isOwner: item.itemOwner,
        chatID: item.id,
        message: item.lastMessage
      }
      Navigator.navTo('AdminChat', { param })
    }
  }

  _onArchive = () => {
    const { showingArchive, dataProvider } = this.state
    const { userProfile } = this.props
    this.setState(
      { dataProvider: dataProvider.cloneWithRows([]), items: [], showingArchive: !showingArchive, loading: true },
      () => {
        if (!showingArchive) {
          this._loadMessages(!showingArchive)
        } else {
          this._loadMessages(!showingArchive, () => userProfile && userProfile.role != 'admin' && this._loadAdminMessages(false))
        }
      }
    )
  }

  _renderItem = (type, data, index) => {
    return (
      <View style={{ height: Constants.INBOX_ITEM_HEIGHT, width: Constants.WINDOW_WIDTH }}>
        <InboxItem
          onTapSwipeItem={this._onTapSwipeItem}
          onTapAvatar={this._onTapAvatar}
          onTapChatItem={this._onTapChatItem}
          isArchived={this.state.showingArchive}
          item={data}
          index={index}
        />
      </View>
    )
  }

  _renderConntent = () => {
    const { dataProvider, loading, refreshing, items, showingArchive } = this.state

    // let title = 'No messages yet'
    let title = null
    let message =
      // 'When you find a garmet you love, connect with the lender. Tell them a bit about yourself and the occasion.'
      'Your inbox is empty. there’s only one way to fix that....'
    let buttonText = 'List Items To Rent'
    let secondButtonText = 'Find Rentals To Wear'
    if (showingArchive) {
      title = 'No archived messages'
      message = 'You can archive a message by left swiping a chat item, or tapping more button in chat screen.'
    }
    // const onPress = showingArchive ? null : () => Navigator.navTo('Home')
    const { userProfile } = this.props
    const onPress = showingArchive ? null : userProfile && userProfile.accessibility === false ? () => Navigator.navTo('ComingSoon', { fromCloset: true }) : () => Navigator.navTo('AddItem')
    const onPressSecondButton = () => Navigator.navTo('Home')

    if (Array.isArray(items) && items.length) {
      return (
        <RecyclerListView
          scrollViewProps={{
            showsVerticalScrollIndicator: false,
            showsHorizontalScrollIndicator: false,
            decelerationRate: 'normal',
            refreshControl: <RefreshControl refreshing={refreshing} onRefresh={this._onRefresh} />,
            contentContainerStyle: { paddingBottom: 40 }
          }}
          dataProvider={dataProvider}
          layoutProvider={this._layoutProvider}
          rowRenderer={this._renderItem}
        />
      )
    }
    if (loading) return null
    if (!showingArchive) {
      return <EmptyViewTwoButton title={title} info={message} buttonText={buttonText} onPress={onPress} secondButtonText={secondButtonText} onPressSecondButton={onPressSecondButton} />
    }

    return <EmptyView title={title} info={message} buttonText={buttonText} onPress={onPress} />
  }

  render() {
    const { showingArchive } = this.state
    const headerTitle = showingArchive ? 'Archive' : 'Inbox'
    const tintColor = showingArchive ? Assets.colors.appTheme : Assets.colors.borderColor

    return (
      <View style={StyleSheet.absoluteFill}>
        <Header
          title={headerTitle}
          titleStyle={Styles.headerTitle}
          rightContainerStyle={{ alignItems: 'flex-end' }}
          statusBarProps={{ barStyle: 'dark-content' }}
          RightComponent={
            <Icon iconStyle={{ tintColor }} iconSource={Assets.images.chatArchive} onPress={this._onArchive} />
          }
        />
        <View style={{ flex: 1, marginBottom: 40, marginTop: 30 }}>{this._renderConntent()}</View>
      </View>
    )
  }
}

const mapPropsToState = (state: StoreState) => {
  return {
    userProfile: state.userProfile
  }
}
export default connect(mapPropsToState)(Inbox)
