import React from 'react'
import { View, Image, Text as RNText, TouchableOpacity, StyleSheet, ScrollView } from 'react-native'
import { AsyncImage, BackButton, Button, RentalCollection } from '@Components'
import { NavigationProps } from '@Types'
import { Api, AppsFlyerHelper } from '@Services'
import { Navigator } from '@Navigation'
import { Device, AppConfig, Constants, Formater, Utils } from '@Utils'
import { RentalStatus, RentalItemDetailsParam } from '@Models'
import { Header, Text, Row } from 'rn-components'
import StarRating from 'react-native-star-rating'
import Assets from '@Assets'
import Styles from '@Styles'
import { getStore } from '@ReduxManager'

type Props = {
  navigation: NavigationProps
}

type State = RentalItemDetailsParam & {
  showFooter: boolean
  statusValue: RentalStatus
  isRequested:boolean
  purchaseData:any
}
class RentalItemDetails extends React.Component<Props, State> {
  constructor(props) {
    super(props)
    const rentalDetail = this.props.navigation.getParam('rentalDetail', null)
    const chatData = this.props.navigation.getParam('chatData', null)
    const onAction = this.props.navigation.getParam('onAction', null)
    this.state = {
      rentalDetail,
      chatData,
      onAction,
      showFooter: false,
      statusValue: '',
      isRequested:false,
      purchaseData:null
    }
  }

    getPurchaseDetails = (rental_id:number) =>{
    Api.getPurchaseDetails(rental_id)
    .then((response: any) => {
          console.log(response);
          const { purchses } = response
          let purchasedCount = 0; 
          purchses.map((item) =>{
            if(item.status == "request"){
              purchasedCount++;
              this.setState({
                isRequested : true,
                purchaseData : purchses
              })
            }
          })
          if(purchasedCount == 0)
          {
            this.setState({
              isRequested : false,
              purchaseData : null
            })
          }
    })
    .catch(error => {
      Navigator.hideLoading()
      console.log('**** error: ', error)
    })
  }

  componentWillMount() {
    Navigator.showLoading()
    const { chatData, rentalDetail } = this.state
    this.getPurchaseDetails(chatData.rental_id);
    let { statusValue, showFooter } = Utils.checkStatus(chatData)

    if (statusValue === 'accepted' || statusValue === 'renting') {
      showFooter = true
    }

    if (rentalDetail) {
      Navigator.hideLoading()
      this.setState({ statusValue, showFooter })
    } else {
      Api.getRentalDetail(chatData.rental_id)
        .then((response: any) => {
          const { code, message, ...data } = response
          if (code !== 200) {
            throw Error(message)
          }
          this.setState({ rentalDetail: data, statusValue, showFooter })
          Navigator.hideLoading()
        })
        .catch(error => {
          this.setState({ statusValue, showFooter })
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error', 3000, () => null, () => Navigator.back(), true)
        })
    }
  }

  _onViewInsurance = () => {
    this.props.navigation.navigate('WebsiteView1', {
      url: AppConfig.insurance_url
    })
  }

  _onViewRefund = () => {
    this.props.navigation.navigate('WebsiteView1', {
      url: AppConfig.refund_url
    })
  }

  _renderRefundPolicyForOwner() {
    let refundPolicy =
      'Feel confident in this rental. We will place a credit card hold at the start of the rental and we’ve got you covered with our'
    return (
      <View>
        <View style={{ flexDirection: 'row', marginTop: 15 }}>
          <Image style={{ marginLeft: 28 }} source={Assets.images.details_star} />
          <View style={{ flex: 1 }}>
            <Text style={[styles.itemRetailPriceText, { marginRight: 15, marginLeft: 15 }]}>
              {refundPolicy}
              <RNText onPress={this._onViewInsurance} style={styles.linkText}>
                {' apparel assurance.'}
              </RNText>
            </Text>
          </View>
        </View>
        <Text style={[styles.itemTitleText, { marginTop: 10, marginLeft: 15, fontSize: 16 }]} text="Refund Policy" />
        <Text style={[styles.itemRetailPriceText, { marginHorizontal: 15, marginTop: 10 }]}>
          {
            'Refund will be issued 100% up to 3 days before your scheduled rental. If the rental is canceled within 3 days you will be charged $20 as cancellation fee.'
          }
          <RNText onPress={this._onViewRefund} style={styles.linkText}>
            {' Learn More'}
          </RNText>
        </Text>
      </View>
    )
  }

  _renderRefundPolicy = () => {
    const { rentalDetail } = this.state

    

    if (rentalDetail === null || !Array.isArray(rentalDetail.items) || rentalDetail.items.length == 0) return null

    const item = rentalDetail.items[0]

    let refundPolicy = ''
    if (item.refund_option_id == 2) {
      refundPolicy = 'No refund is offered you will be charged the total rental if you cancel.'
    } else if (item.refund_option_id == 1) {
      refundPolicy =
        'Refund will be issued 100% up to 3 days before your scheduled rental. If the rental is canceled within 3 days you will be charged a 20% cancellation fee.'
    } else {
      return null
    }

    return (
      <View style={{ flexDirection: 'column', marginTop: 15 }}>
        <Text style={[styles.itemTitleText, { marginLeft: 15, fontSize: 16 }]} text="Refund Policy" />
        <Text style={[styles.itemRetailPriceText, { marginLeft: 15, marginTop: 10 }]}>
          {refundPolicy}
          <RNText onPress={this._onViewRefund} style={styles.linkText}>
            {' Learn More'}
          </RNText>
        </Text>
      </View>
    )
  }

  _renderHoldTextNotification = () => {
    const { rentalDetail, chatData } = this.state
    if (rentalDetail == null || chatData == null || chatData.itemOwner == true) return null

    const holdText = Formater.formatMoney(rentalDetail.hold_amount)

    const text = `There will be a hold of ${holdText} on the credit card until the item is returned.`
    return <Text style={styles.holdTextNotification} text={text} />
  }

  _renderItemDetails = () => {
    const { rentalDetail, chatData } = this.state

    if (rentalDetail === null || !Array.isArray(rentalDetail.items) || rentalDetail.items.length == 0) return null

    let statusText = Utils.getStatusText(chatData.status, rentalDetail)
    if (chatData.status === 'renting') {
      let endDate = rentalDetail.dates[rentalDetail.dates.length - 1]
      let due = Utils.checkDue(endDate)

      if (due) {
        statusText = due
      }
    }

    const items: any = rentalDetail.items
    const unit = items.length == 1 ? 'item' : 'items'
    const dates = Utils.getDateStringFromDateRange(rentalDetail.dates)
    const subTitle = `${items.length} ${unit} \t Dates: ${dates}`

    return (
      <RentalCollection
        style={{ marginTop: 15 }}
        title="Rental Collection"
        items={items}
        subTitle={subTitle}
        highlightSubTitle={statusText}
      />
    )
  }

  _showPublicProfile = () => {
    const { chatData } = this.state
    Navigator.navTo('PublicProfile', { user: chatData.toUser })
  }
  _renderUserDetails() {
    const { rentalDetail, chatData } = this.state
    if (rentalDetail == null || chatData == null) return null

    const name = chatData.toUser.name

    console.log
    const rating = chatData.itemOwner
      ? rentalDetail.renter
        ? rentalDetail.renter.rating
        : 0
      : rentalDetail.lister
        ? rentalDetail.lister.rating
        : 0
    return (
      <View style={{ flex: 1, marginTop: 15 }}>
        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flex: 1, marginLeft: 15 }}>
            <Text
              style={[styles.itemTitleText, { marginLeft: 0, marginTop: 15, fontSize: 16, lineHeight: 20 }]}
              text={name}
            />
            <View style={{ flexDirection: 'row', marginTop: 14, marginBottom: 32, alignItems: 'center' }}>
              <View style={{ width: 100 }}>
                <StarRating
                  disabled={true}
                  maxStars={5}
                  rating={rating}
                  emptyStar={Assets.images.ratingEmptyStar}
                  fullStar={Assets.images.ratingFullStar}
                  halfStar={Assets.images.ratingHalfStar}
                  halfStarEnabled={true}
                  starSize={20}
                />
              </View>
              <Text style={styles.averageText} text="Avg." />
            </View>
          </View>

          <View style={{ marginRight: 15, width: 50 }}>
            <TouchableOpacity activeOpacity={0.7} onPress={this._showPublicProfile}>
              <View style={styles.profileView}>
                <AsyncImage
                  placeholderSource={Assets.images.defaultProfile}
                  source={{ uri: chatData.toUser.photo }}
                  width={50}
                  height={50}
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ flex: 1, height: 1, backgroundColor: '#E7EAEB' }} />
      </View>
    )
  }

  _renderListerFeeDetails() {
    const { rentalDetail } = this.state
    if (rentalDetail == null || !Array.isArray(rentalDetail.items) || rentalDetail.items.length == 0) return null

    const { rental_fee = 0, cleaning_fee = 0, service_fee = 0, total_payout = 0, insurance_fee = 0 } = rentalDetail.fees
    let payoutAmount = total_payout - insurance_fee
    payoutAmount = payoutAmount > 0 ? payoutAmount : 0;

    return (
      <View style={{ marginTop: 30 }}>
        {this._renderFeeItem('Rental Fee Total', rental_fee, false)}
        {this._renderFeeItem('Cleaning Fee', cleaning_fee, false)}
        {this._renderFeeItem('Service Fee', 0 - service_fee, false)}
        {/* {this._renderFeeItem('Total Payout', total_payout, true)} */}
        {this._renderFeeItem('Total Payout', payoutAmount, true)}
        <View
          style={{
            marginTop: 15,
            marginHorizontal: 15,
            flex: 1,
            height: 1,
            backgroundColor: '#E7EAEB'
          }}
        />
      </View>
    )
  }

  _renderFeeDetails = () => {
    const { rentalDetail } = this.state
    if (rentalDetail == null) return null

    const {
      insurance_fee = 0,
      rental_fee = 0,
      replacement_fee = 0,
      service_fee = 0,
      cleaning_fee = 0,
      total_payout = 0,
      coupon_amount = 0,
      credits = 0
    } = rentalDetail.fees

    return (
      <View style={{ marginTop: 30 }}>
        {this._renderFeeItem('Rental Fee Total', rental_fee, false)}
        {this._renderFeeItem('Cleaning Fee', cleaning_fee ? cleaning_fee : 0, false)}
        {this._renderFeeItem('Apparel Assurance', insurance_fee, false)}
        {this._renderFeeItem('Service Fee', service_fee, false)}
        {coupon_amount > 0 && this._renderFeeItem('Promo Code', -coupon_amount, false)}
        {credits > 0 && this._renderFeeItem('Credits', -credits, false)}
        {this._renderFeeItem('Rental Total', total_payout, true)}
        <View style={{ marginTop: 15, marginHorizontal: 15, flex: 1, height: 1, backgroundColor: '#E7EAEB' }} />
      </View>
    )
  }

  _renderFeeItem = (title, value, boldStaus) => {
    if (value == null) return null
    const valueString = Formater.formatMoney(value)
    const valueStyle: any = {
      marginRight: 15,
      lineHeight: 28,
      fontSize: boldStaus ? 16 : 14,
      textAlign: 'right',
      marginLeft: 5,
      marginTop: 0
    }
    const titleStyle = { marginLeft: 30, lineHeight: 28, color: Assets.colors.mainText, flex: 1 }

    return (
      <View style={{ flexDirection: 'row' }}>
        <Text style={[styles.itemRetailPriceText, titleStyle]} text={title} />
        <Text style={[styles.itemTitleText, valueStyle]} text={valueString} />
      </View>
    )
  }

  _renderAdditionalDetails = () => {
    const { rentalDetail, chatData } = this.state
    if (
      rentalDetail === null ||
      chatData == null ||
      chatData.order.firstMessage == null ||
      chatData.order.firstMessage.length == 0
    )
      return null

    let message = ''

    if (chatData.itemOwner) {
      message = 'Message to ' + chatData.fromUser.name
    } else {
      message = 'Message from ' + chatData.fromUser.name
    }
    return (
      <View style={{ flexDirection: 'column', marginTop: 25 }}>
        <Text
          style={[styles.itemTitleText, { marginLeft: 15, marginTop: 10, fontSize: 16, lineHeight: 28 }]}
          text={message}
        />
        <Text
          style={[styles.itemRetailPriceText, { marginLeft: 15, marginTop: 10, marginBottom: 24, marginRight: 15 }]}
          text={chatData.order.firstMessage}
        />
      </View>
    )
  }

  /******************************** START Perform action ******************************************/
  _onTapFooterButton = action => {
    const { chatData } = this.state

    if (action == 'review') {
      const { userProfile } = getStore().getState()
      // AppsFlyerHelper.trackEvent(AppsFlyerHelper.EVENTS.RENTAL_TRANSACTION_COMPLETED, userProfile.id,chatData.rental_id)
    }
    if(action == 'buy'){
      this.setState({
        // purchaseData:null,
        isRequested: true
      })
    }
    if(action == 'buynow_cancel'){
      this.setState({
        purchaseData:null,
        isRequested: false
      })
      this.getPurchaseDetails(chatData.rental_id);
    }
    if(action == 'buynow_accepted'){
      this.setState({
        purchaseData:null,
        isRequested: false
      })
      this.getPurchaseDetails(chatData.rental_id);
    }
    if(action == 'buynow_declined'){
      this.setState({
        purchaseData:null,
        isRequested: false
      })
      this.getPurchaseDetails(chatData.rental_id);
    }
    const { onAction } = this.state
    Navigator.back()
    onAction(action)
  }

  _renderCancelWithoutPenalty = () => {
    const { chatData } = this.state
    if (chatData && chatData.itemOwner === false) {
      const onPress = () => this._onTapFooterButton('cancel_without_penalty')
      return (
        <Button
          style={styles.negativeButton}
          textStyle={styles.negativeButtonText}
          text="Cancel Rental"
          onPress={onPress}
        />
      )
    }
    return null
  }

  _renderFooterForPreApprove = () => {
    const { chatData } = this.state
    if (chatData && chatData.itemOwner) {
      const onPress = () => this._onTapFooterButton('approve')
      return (
        <Button
          style={styles.positiveButton}
          textStyle={styles.positiveButtonText}
          text="Pre-approve Rental"
          onPress={onPress}
        />
      )
    }

    return null
  }

  _renderFooterForReview = () => {
    const onPress = () => this._onTapFooterButton('review')

    return (
      <Button
        style={styles.positiveButton}
        textStyle={styles.positiveButtonText}
        text="Write Review"
        onPress={onPress}
      />
    )
  }

  _renderFooterForRequest = () => {
    const { chatData } = this.state
    if (chatData && chatData.itemOwner == false) {
      const onPress = () => this._onTapFooterButton('cancelled')
      return (
        <Button
          style={styles.negativeButton}
          textStyle={styles.negativeButtonText}
          text="Cancel Request"
          onPress={onPress}
        />
      )
    }
    const onDecline = () => this._onTapFooterButton('declined')
    const onAccept = () => this._onTapFooterButton('accepted')
    return (
      <Row alignHorizontal="space-between" alignVertical="center">
        <Button
          style={[{ marginRight: 10, flex: 1 }, styles.negativeButton]}
          textStyle={styles.negativeButtonText}
          text="Decline"
          onPress={onDecline}
        />
        <Button
          style={[{ marginLeft: 10, flex: 1 }, styles.positiveButton]}
          textStyle={styles.positiveButtonText}
          text="Accept"
          onPress={onAccept}
        />
      </Row>
    )
  }

  _renderFooterForRenting = () => {
    const { chatData } = this.state
    if (chatData && chatData.itemOwner == false) {
      const onPress = () => this._onTapFooterButton('returned')
      return (
        <Button style={styles.positiveButton} textStyle={styles.positiveButtonText} text="Return" onPress={onPress} />
      )
    }
  }
  _renderFooterForBuyNow = () => {
    const { chatData } = this.state
    if (chatData && chatData.itemOwner == false) {
      const onPress = () => this._onTapFooterButton('buy')
    
      return (
        <Button style={[styles.positiveButton,{ marginTop:5}]} textStyle={styles.positiveButtonText} text="Request to Buy" onPress={onPress} 
        />
      )
    }
  }
  _renderFooterForBuyNowRequest() {
    const { chatData } = this.state

    if (chatData == null) {
      return null
    }
    const isItemOwner = chatData.itemOwner

    if (isItemOwner) {
      //const onDecline = () => this._performAction('declined')
      //const onAccept = () => this._performAction('accepted')

      const onDecline = () => this._onTapFooterButton('buynow_declined') //this._performAction('buynow_declined')
      const onAccept = () => this._onTapFooterButton('buynow_accepted') //this._performAction('buynow_accepted')
       return (
        <Row alignHorizontal="space-between" alignVertical="center">
          <Button
            style={[{ marginRight: 10, flex: 1 }, styles.negativeButton]}
            textStyle={styles.negativeButtonText}
            text="Decline"
            onPress={onDecline}
          />
          <Button
            style={[{ marginLeft: 10, flex: 1 }, styles.positiveButton]}
            textStyle={styles.positiveButtonText}
            text="Accept"
            onPress={onAccept}
          />
        </Row>
       )
    } 
    const onCancel = () => this._onTapFooterButton('buynow_cancel') //this._performAction('buynow_cancel')
      return (
        <Button
          style={[{marginTop:10}, styles.negativeButton]}
          text="Cancel Request"
          textStyle={styles.negativeButtonText}
          onPress={onCancel}
        />
      )
  }

  _renderFooterForApproved = () => {
    const onPress = () => this._onTapFooterButton('rent')
    return (
      <Button style={styles.positiveButton} textStyle={styles.positiveButtonText} text="Rent Now" onPress={onPress} />
    )
  }

  _renderFooterForAccepted = () => {
    const onPress = () => this._onTapFooterButton('cancelled')
    return (
      <Button
        style={styles.negativeButton}
        textStyle={styles.negativeButtonText}
        text="Cancel Request"
        onPress={onPress}
      />
    )
  }

  _renderFooter = () => {
    const { showFooter, statusValue, purchaseData, rentalDetail, isRequested } = this.state
    let buy_now: boolean = false;
    if (rentalDetail) {
      const { items } = rentalDetail;
      items.map((item: any) => {
        console.log(item)
        if (item.buy_now && item.active) {
          buy_now = true
          // return
          
        }
      }
      )
    }

    if (showFooter) {
      const backgroundColor =
        statusValue === 'request' || statusValue === 'approved' || statusValue === 'accepted'
          ? 'white'
          : Assets.colors.inputBg
      return (
        <View style={[styles.footerContainer, { backgroundColor }]}>
          {statusValue === 'cancel_without_penalty' && this._renderCancelWithoutPenalty()}
          {statusValue === 'request' && this._renderFooterForRequest()}
          {statusValue === 'renting' && !purchaseData && this._renderFooterForRenting()}
          {(statusValue === 'renting' && buy_now && !purchaseData) && this._renderFooterForBuyNow()}
          {(statusValue === 'renting' && isRequested) && this._renderFooterForBuyNowRequest()}
          {statusValue === 'approved' && this._renderFooterForApproved()}
          {statusValue === 'accepted' && this._renderFooterForAccepted()}
          {statusValue === 'returned' && this._renderFooterForReview()}
          {statusValue === 'purchased' && this._renderFooterForReview()}
          {statusValue === 'inquiry' && this._renderFooterForPreApprove()}
          
        </View>
      )
    }

    return null
  }
  /******************************** START Perform action ******************************************/
  render() {
    const { chatData } = this.state
    return (
      <View style={[StyleSheet.absoluteFill, { backgroundColor: Assets.colors.componentBg }]}>
        <Header
          title="Details"
          titleStyle={Styles.headerTitle}
          LeftComponent={<BackButton />}
          statusBarProps={{ barStyle: 'dark-content' }}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          {this._renderItemDetails()}
          {this._renderUserDetails()}
          {chatData && chatData.itemOwner == true && this._renderListerFeeDetails()}
          {chatData && chatData.itemOwner == false && this._renderFeeDetails()}
          {chatData && chatData.itemOwner == false && this._renderRefundPolicy()}
          {chatData && chatData.itemOwner == true && this._renderRefundPolicyForOwner()}
          {this._renderAdditionalDetails()}
          {this._renderHoldTextNotification()}
        </ScrollView>
        {this._renderFooter()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  linkText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    letterSpacing: -0.2,
    lineHeight: 18,
    color: Assets.colors.appTheme
  },
  itemImage: {
    height: 100 * Constants.IMAGE_RATIO,
    width: 100,
    borderRadius: 6,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F4F6F6'
  },
  statusText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight
  },
  itemTitleText: {
    marginLeft: 20,
    marginTop: 8,
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: Assets.colors.mainText,
    lineHeight: 18
  },

  profileView: {
    height: 50,
    width: 50,
    borderRadius: 25,
    overflow: 'hidden'
  },
  itemRetailPriceText: {
    marginLeft: 23,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    letterSpacing: -0.2,
    lineHeight: 18,
    color: Assets.colors.textLight
  },

  averageText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    textAlign: 'left',
    letterSpacing: -0.4,
    lineHeight: 22,
    color: Assets.colors.mainText,
    marginLeft: 10
  },
  holdTextNotification: {
    fontSize: 14,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    color: Assets.colors.textLight,
    marginLeft: 15,
    marginTop: 10,
    marginBottom: 15,
    marginRight: 15
  },
  footerContainer: {
    borderTopColor: Assets.colors.borderColor,
    borderTopWidth: 1,
    backgroundColor: Assets.colors.inputBg,
    paddingTop: 10,
    paddingHorizontal: 20,
    paddingBottom: Device.isIphoneX() ? 20 : 10
  },

  positiveButton: {},
  positiveButtonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: 'white',
    paddingVertical: 8
  },
  negativeButton: {
    backgroundColor: 'white',
    borderRadius: 6,
    borderWidth: 1,
    borderColor: Assets.colors.borderColor
  },
  negativeButtonText: {
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.display.regular,
    fontSize: 14,
    paddingVertical: 8
  }
})

export default RentalItemDetails
