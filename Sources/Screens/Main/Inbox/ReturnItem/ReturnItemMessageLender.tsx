import React from 'react'
import { View, StyleSheet, TouchableOpacity, Image, Keyboard } from 'react-native'
import { Header, Text, Textarea, Row } from 'rn-components'
import { Button, BackButton } from '@Components'
import { Api } from '@Services'
import { Utils, Device } from '@Utils'
import Assets from '@Assets'
import Styles from '@Styles'
import { Navigator } from '@Navigation'
import { ReturnItemMessageLenderParam, ReturnItemMessageSentParam } from '@Models'
import { NavigationProps } from '@Types'

const isIphoneX = Device.isIphoneX()

type Props = {
  navigation: NavigationProps
}

type State = ReturnItemMessageLenderParam & {
  agreeTerms: boolean
}

class ReturnItemMessageLender extends React.Component<Props, State> {
  _textArea = React.createRef<Textarea>()

  constructor(props) {
    super(props)
    const param: ReturnItemMessageLenderParam = this.props.navigation.getParam('param')
    this.state = {
      ...param,
      lender: param.lender || 'lender',
      agreeTerms: false
    }
  }

  _onToggleArgeeTerm = () => this.setState({ agreeTerms: !this.state.agreeTerms })

  _onSendMessage = () => {
    const message = this._textArea.current.getText()
    const trimMessage = message.trim()
    const { agreeTerms, lender, onMessageLender, onReviewItem, items, chatData, onRetryUpdateReviewItem } = this.state
    if (trimMessage.length === 0) {
      Navigator.showToast('Error', 'Enter your message to ' + lender + '.', 'Error')
      return
    }

    if (agreeTerms == false) {
      Navigator.showToast('Error', 'Please make sure all your items are documented.', 'Error')
      return
    }

    Keyboard.dismiss()

    onMessageLender(message)
      .then(() => {
        const param: ReturnItemMessageSentParam = {
          onReviewItem,
          chatData,
          items,
          onRetryUpdateReviewItem
        }

        Navigator.navTo('ReturnItemMessageSent', { param })
      })
      .catch(error => {
        console.log('**** error', error)
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  render() {
    return (
      <View style={[StyleSheet.absoluteFill, { backgroundColor: Assets.colors.componentBg }]}>
        <Header titleStyle={Styles.headerTitle} title="Rental Return" LeftComponent={<BackButton />} />
        <View style={{ marginHorizontal: 20, marginTop: 25 }}>
          <Text style={styles.viewTitleLabel} text="Message the lender to setup a time & place for the return." />
          <Text
            style={[styles.descriptionText, { marginTop: 10 }]}
            text="Message your lender to arrange a time and location to return your rentals."
          />
        </View>
        <View style={styles.divider} />
        <View style={{ marginHorizontal: 20 }}>
          <Text style={[styles.inputHelper]} text={`Message ${this.state.lender}`} />
          <Textarea
            ref={this._textArea}
            style={styles.inputContainer}
            placeholder="Your message"
            placeholderTextColor={Assets.colors.placeholder}
            inputStyle={styles.input}
            inputAccessoryViewID="snapPhotos"
            InputAccessoryComponent={
              <Text
                containerStyle={Styles.nextAccessoryContainer}
                style={Styles.nextAccessoryText}
                text="DONE"
                onPress={() => {
                  Keyboard.dismiss()
                  // this._onSendMessage()
                }}
              />
            }
          />

          <Row style={{ marginTop: 20 }} alignVertical="center">
            <TouchableOpacity activeOpacity={0.7} onPress={this._onToggleArgeeTerm}>
              <View style={styles.tickMarkView}>
                {this.state.agreeTerms && (
                  <Image
                    source={Assets.images.checkboxOn}
                    resizeMode="contain"
                    style={{ tintColor: Assets.colors.appTheme, width: 14, height: 14 }}
                  />
                )}
              </View>
            </TouchableOpacity>
            <Text style={[styles.descriptionText, { marginLeft: 10, color: Assets.colors.appTheme }]}>
              Yes <Text style={{ color: Assets.colors.textLight }} text="I've documented the items for return." />
            </Text>
          </Row>
        </View>
        <View
          style={{ flex: 1 }}
          onStartShouldSetResponder={() => {
            Keyboard.dismiss()
            return true
          }}
        />

        <View style={styles.buttonContainer}>
          <Button text="SEND MESSAGE" onPress={this._onSendMessage} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  viewTitleLabel: {
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 24,
    color: Assets.colors.mainText,
    letterSpacing: -0.2,
    lineHeight: 34
  },
  descriptionText: {
    fontFamily: Assets.fonts.display.regular,
    fontSize: 16,
    lineHeight: 18,
    textAlign: 'left',
    color: Assets.colors.textLight
  },
  divider: {
    backgroundColor: Assets.colors.borderColorLight,
    height: 1,
    marginTop: 20,
    marginBottom: 25
  },
  buttonContainer: {
    borderTopColor: Assets.colors.borderColorLight,
    borderTopWidth: 1,
    paddingTop: 15,
    paddingBottom: isIphoneX ? 35 : 20,
    paddingHorizontal: 15
  },
  buttonDescription: {
    fontFamily: Assets.fonts.display.regular,
    fontSize: 16,
    color: Assets.colors.textLight,
    marginRight: 20
  },
  inputHelper: {
    fontFamily: Assets.fonts.display.regular,
    fontSize: 18,
    lineHeight: 18,
    textAlign: 'left',
    color: Assets.colors.mainText
  },
  inputContainer: {
    marginTop: 15,
    backgroundColor: Assets.colors.inputBg,
    borderRadius: 6,
    height: 112,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 6,
    lineHeight: 22
  },

  input: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    padding: 10,
    lineHeight: 22,
    height: 112
  },
  tickMarkView: {
    width: 18,
    height: 18,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0.5,
    borderColor: Assets.colors.placeholder
  }
})

export default ReturnItemMessageLender
