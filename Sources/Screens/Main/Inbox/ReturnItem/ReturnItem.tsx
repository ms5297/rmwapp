import React from 'react'
import { View, StyleSheet, FlatList, Image } from 'react-native'
import { Navigator } from '@Navigation'
import { NavigationProps } from '@Types'
import { BackButton, AsyncImage, Button } from '@Components'
import { Header, Text, Textarea, Icon, Row, Col } from 'rn-components'
import { ReturnItemParam, ChatModelOrderItem, ReturnItemDetailParam, Item, ReturnItemMessageLenderParam } from '@Models'
import { Device, Utils } from '@Utils'
import { Api } from '@Services'
import Assets from '@Assets'
import Styles from '@Styles'

type Props = {
  navigation: NavigationProps
}

type State = ReturnItemParam & {
  items: Array<Item>,
  purchasedItems: Array<Item>,
  onCloseAction
  // originalChatData: Array<Item>
}

const isIphoneX = Device.isIphoneX()
const footerHeight = isIphoneX ? 100 : 80

class ReturnItem extends React.Component<Props, State> {
  _textArea = React.createRef<Textarea>()
  _currentItemIndex = 0
  _lender = ''

  constructor(props: Props) {
    super(props)
    const param: ReturnItemParam = this.props.navigation.getParam('param')
    let chatData = param.chatData
    if (!chatData.orders) {
      chatData.orders = [chatData.order].map(chat => ({
        ...chat,
        status: chatData.status,
        rental_id: chatData.rental_id
      }))
    }
    this.state = {
      ...param,
      items: [],
      purchasedItems: [],

      // originalChatData: []
    }
    console.log('constructore')
    // const orignalChatData = this.state.chatData;
    // this.setState({ originalChatData: orignalChatData })
  }

  componentDidMount() {
    Navigator.showLoading()
    const ids = this.state.chatData.orders.map(item => item.itemId)
    if (ids) {
      Api.getItemDetails(ids)
        .then((response: any) => {
          const { code, message, items } = response
          if (code !== 200) throw Error(message)
          let purchasedItems = [];
          let newItems = [];
          items.map((item: any) => {
            if (item.buy_now && !item.active) {
              purchasedItems.push(item);
            } else {
              newItems.push(item)
            }
          })
          let newOrder = [];
          let nonPurchasedItems = [];
          this.state.chatData.orders.map((item, index) => {
            const np = purchasedItems.find(pItem => pItem.id != item.itemId)
            if (np) {
              nonPurchasedItems.push(item);
            }
          }
          )
          // console.log(nonPurchasedItems);
          if (nonPurchasedItems && nonPurchasedItems.length > 0) {
            this.state.chatData.orders = nonPurchasedItems;
          }

          console.log(this.state.chatData.orders)
          this.setState({ items: newItems })
          Navigator.hideLoading()
        })
        .catch(error => {
          Navigator.hideLoading()
          console.log('**** error', error)
        })
    }

  }

  _onTapNextButton = () => {
   // let item = this.state.chatData.orders[this._currentItemIndex]
    let newChatData = []
    let { onReturnItem, items, chatData, purchasedItems } = this.state
    chatData.orders.map((item, index) => {
      const isProduct = items.find(i_item => i_item.id == item.itemId)
      if (isProduct) {
        newChatData.push(item);
      }
    }),
      
    chatData.orders = newChatData;

    let item = chatData.orders[this._currentItemIndex]

    const title = `Item ${this._currentItemIndex + 1} of ${this.state.chatData.orders.length}`
    const isComplete = this._currentItemIndex === this.state.chatData.orders.length - 1

    let seqItem = null;
    items.map((it) => {
      if (it.id === item.itemId) {
        seqItem = it
      }
    })
    if (seqItem) { 

    

    // if (!seqItem) {
    //   item = this.state.chatData.orders[this._currentItemIndex + 1]
    //   if (item.status == "returned") {
        
    //   }
    // }
    items.map((it) => {
      if (it.id === item.itemId) {
        seqItem = it
      }
    })
    // item = seqItem;
    const param: ReturnItemDetailParam = {
      order: item,
      onReturnItem: (photos, message) => {
        Navigator.pop(2, false)
        onReturnItem(photos, message, item, isComplete).then(chatData => {
          console.log('***** onSuccess chatData', chatData)
          let originalChatData = chatData;
          let nonPurchasedItems = [];
          chatData.orders.map((item, index) => {
            const np = purchasedItems.find(pItem => pItem.id != item.itemId)
            if (np) {
              nonPurchasedItems.push(item);
            }
          }
          )
          if (nonPurchasedItems && nonPurchasedItems.length > 0) {
            originalChatData.orders = nonPurchasedItems
            this.setState({ chatData: originalChatData })
          } else {
            this.setState({ chatData })
          }
        })
      },
      item: items.length ? items[this._currentItemIndex] : null,
      title
    }
    param.item = seqItem
      Navigator.navTo('ReturnItemDetail', { param })
      }
  }

  _renderHeader = (isComplete = false) => {
    const totalItem = this.state.chatData.orders.length
    const numItems = totalItem === 1 ? '1 Item' : `${totalItem} Items`

    const color = isComplete ? Assets.colors.green : Assets.colors.mainText
    return (
      <View style={styles.header}>
        <Text
          style={[styles.viewTitleLabel, { color }]}
          text={
            isComplete ? 'Thanks for Documenting all your items for return!' : "Document the items you're returning."
          }
        />
        {isComplete == false && (
          <Text
            style={styles.descriptionText}
            text="Prior to drop-off, tell us about the current condition, provide photos, and a photo of the dry cleaning receipt if you dropped your item at the cleaner so your lender can pick it up."
          />
        )}

        {/* {isComplete == false && (
          <Row style={{ marginTop: 25 }}>
            <Text style={styles.name} text={numItems} />
            <Text style={[styles.name, { color: Assets.colors.appTheme, marginLeft: 10 }]} text="Due Today" />
          </Row>
        )} */}
        {isComplete == false && <Text style={styles.name} text={`${numItems} Due`} />}
      </View>
    )
  }

  // _returnItem = (item: ChatModelOrderItem, index: number) => {
  //   const { onSuccess, items, chatData } = this.state;
  //   const param: ReturnItemDetailParam = {
  //     order: item,
  //     onSuccess: (photos, message) => {
  //       Navigator.pop(2, false);
  //       Navigator.showLoading();
  //       onSuccess(photos, message, item).then(chatData => {
  //         this.setState({ chatData });
  //       });
  //       // Api.editRequestRental({ id: item.rental_id, status: item.status })
  //       //   .then((response: any) => {
  //       //     const { code, message, ...data } = response;
  //       //     if (code !== 200) {Once
  //       //       throw Error(message || "Fail to update rental status");
  //       //     }

  //       //     Navigator.hideLoading();
  //       //   })
  //       //   .catch(error => {
  //       //     Navigator.hideLoading();
  //       //   });
  //     },
  //     item: items.length ? items[index] : null
  //   };
  //   Navigator.navTo("ReturnItemDetail", { param });
  // };

  _messageToLender = () => {
    const {
      onMessageLender,
      items,
      chatData,
      onReviewItem,
      onRetryUpdateReturnItem,
      onRetryUpdateReviewItem
    } = this.state

    const param: ReturnItemMessageLenderParam = {
      onMessageLender,
      lender: this._lender,
      chatData,
      items,
      onReviewItem,
      onRetryUpdateReturnItem,
      onRetryUpdateReviewItem
    }
    Navigator.navTo('ReturnItemMessageLender', { param })
  }

  _renderItem = ({ item, index }: { item: ChatModelOrderItem; index: number }) => {
    let { items } = this.state
    let activeItem = []
    console.log(item);
    console.log(items)

    items.map((itemact: any) => {
      if (itemact.active && itemact.id == item.itemId) {
        activeItem.push(itemact)
      }
    })
    items = activeItem

    
    
    const returned = item.status === 'returned'
    const status = returned ? 'Documented' : 'Pending Documentation'
    const itemDetail = items.length ? items[0] : null
    const itemImage = itemDetail ? Utils.getPhotoUrlFromList(itemDetail.photos, '') : ''
    console.log("itemDetail", itemDetail)
    
    
    
    const color = returned ? Assets.colors.green : Assets.colors.textLight
    const backgroundColor = returned ? Assets.colors.green : 'transparent'
    this._lender = itemDetail ? itemDetail.owner.name : ''

    return (
      <Row key={index} style={styles.rowItem} alignHorizontal="space-between" alignVertical="center">
        <View style={styles.imageContainer}>
          <AsyncImage
            style={styles.image}
            source={{ uri: itemImage }}
            fallbackImageSource={Assets.images.cartImagePlaceHolder}
          />
        </View>
        <Col style={{ marginHorizontal: 15 }} flex={1} alignVertical="center">
          <Text style={styles.name} text={item.itemName} />
          <Text style={[styles.status, { color }]} text={status} />
        </Col>
        <View style={[styles.iconContainer, { backgroundColor }]}>
          {returned && <Image style={{ tintColor: 'white' }} source={Assets.images.checkboxOn} />}
        </View>
      </Row>
    )
  }
  componentWillUnmount() {
    const { onCloseAction } = this.state
    onCloseAction()
  }

  _keyExtractor = (item, index) => index + ''

  render() {
    let { chatData, items } = this.state
    let data = chatData.orders

    let BuyProds = []
    let notBuyProds = []
    let setData = []

    console.log("data", data)
    console.log("chatData", chatData)
    console.log("items", items)
    items.map((it: any) => {
      if (!it.active) {
        BuyProds.push(it)
      } else {
        notBuyProds.push(it)
      }
    })
    items = notBuyProds;

    data.forEach(obj => {
      if (notBuyProds.some(o => o.id === obj.itemId)) {
        setData.push(obj);
      }
    });
    data = setData;
    console.log("data", data)

    const returnedItem = data.filter(item => item.status === 'returned')
    const totalReturnedItem = Array.isArray(returnedItem) ? returnedItem.length : 0
    const isComplete = totalReturnedItem === data.length
    this._currentItemIndex = totalReturnedItem

    return (
      <View style={[StyleSheet.absoluteFill, { backgroundColor: Assets.colors.componentBg }]}>
        <Header
          titleStyle={Styles.headerTitle}
          title="Rental Return"
          LeftComponent={<BackButton iconSource={Assets.images.closeIcon} />}
        />
        <FlatList
          style={{ flex: 1 }}
          ListHeaderComponent={this._renderHeader(isComplete)}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          extraData={items}
          data={data}
        />
        {isComplete == false && (
          <View style={{ height: footerHeight, marginRight: 25 }}>
            <Row alignVertical="center" alignHorizontal="flex-end">
              <Text
                style={styles.buttonDescription}
                text={`Document: Item ${totalReturnedItem + 1} of ${data.length}`}
              />
              <Icon
                iconStyle={styles.nextButtonImage}
                iconSource={Assets.images.next_round}
                onPress={this._onTapNextButton}
              />
            </Row>
          </View>
        )}
        {isComplete && (
          <View style={styles.buttonContainer}>
            <Button text="Message the Lender" onPress={this._messageToLender} />
          </View>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  viewTitleLabel: {
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 30,
    color: Assets.colors.mainText,
    letterSpacing: -0.2,
    lineHeight: 34
  },
  rowItem: {
    paddingLeft: 25,
    paddingRight: 20,
    paddingVertical: 10,
    borderBottomColor: Assets.colors.borderColorLight,
    borderBottomWidth: 1
  },
  imageContainer: {
    width: 50,
    height: 60,
    backgroundColor: Assets.colors.inputBg,
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    width: 50,
    height: 60,
    backgroundColor: Assets.colors.inputBg
  },
  descriptionText: {
    marginTop: 28,
    fontFamily: Assets.fonts.display.regular,
    fontSize: 14,
    lineHeight: 18,
    textAlign: 'left',
    color: Assets.colors.mainText
  },

  header: {
    marginTop: 25,
    paddingHorizontal: 25,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: Assets.colors.borderColorLight
  },
  nextButtonImage: {
    height: 64,
    width: 64
  },
  iconContainer: {
    width: 32,
    height: 32,
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center'
  },
  name: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    color: Assets.colors.mainText
  },
  status: {
    marginTop: 3,
    fontFamily: Assets.fonts.display.regular,
    fontSize: 14,
    color: Assets.colors.textLight
  },
  buttonContainer: {
    borderTopColor: Assets.colors.borderColorLight,
    borderTopWidth: 1,
    paddingTop: 15,
    paddingBottom: isIphoneX ? 35 : 20,
    paddingHorizontal: 15
  },
  buttonDescription: {
    fontFamily: Assets.fonts.display.regular,
    fontSize: 16,
    color: Assets.colors.textLight,
    marginRight: 20
  }
})

export default ReturnItem
