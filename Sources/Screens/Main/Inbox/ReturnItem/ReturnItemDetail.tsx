import React from 'react'
import { View, StyleSheet, Keyboard } from 'react-native'
import { Navigator } from '@Navigation'
import { NavigationProps } from '@Types'
import { BackButton, RentalItem } from '@Components'
import { Header, Text, Textarea, Icon, ScrollView, Row } from 'rn-components'
import { ReturnPhotoParam, ReturnItemDetailParam } from '@Models'
import { Api } from '@Services'
import { Utils, Device } from '@Utils'
import Assets from '@Assets'
import Styles from '@Styles'

type Props = {
  navigation: NavigationProps
}

type State = ReturnItemDetailParam

const footerHeight = Device.isIphoneX() ? 100 : 80

class ReturnItemDetail extends React.Component<Props, State> {
  _textArea = React.createRef<Textarea>()

  constructor(props: Props) {
    super(props)
    const param: ReturnItemDetailParam = this.props.navigation.getParam('param')
    this.state = param
  }

  componentDidMount() {
    Navigator.showLoading()
    this._textArea.current.focus()
    const id = this.state.order.itemId
    const { order, item } = this.state
    Api.getRentalAmount({
      credits: 0,
      coupon_code: '',
      item_ids: [id]
    })
      .then((response: any) => {
        const { code, message, coupon_amount, credits, payable_fee, items, service_fee } = response

        if (code !== 200 || !Array.isArray(items) || items.length == 0) throw Error(message || 'Internal Error.')

        console.log('**** response', response)

        let fees: any = Utils.calcAdditionalFees(items)
        fees.payable_fee = payable_fee
        fees.credits = credits
        fees.coupon_amount = coupon_amount
        fees.service_fee = service_fee
        const newItem = { ...item, additional_fees: fees }
        this.setState({ item: newItem })
        Navigator.hideLoading()
      })
      .catch(error => {
        console.log('*** error', error)
        Navigator.hideLoading()
      })
  }

  _onTapNextButton = () => {
    const message = this._textArea.current.getText()
    const trimMessage = message.trim()
    if (trimMessage.length === 0) {
      Navigator.showToast('Error', 'Please fill current condition of the rental.', 'Error')
      return
    }
    Keyboard.dismiss()
    const { onReturnItem, order } = this.state
    const param: ReturnPhotoParam = {
      order,
      onReturnItem: data => onReturnItem(data, trimMessage)
    }

    Navigator.navTo('ReturnPhoto', { param })
  }

  render() {
    const { item, title } = this.state
    
    return (
      <View style={[StyleSheet.absoluteFill, { backgroundColor: Assets.colors.componentBg }]}>
        <Header
          titleStyle={[Styles.headerTitle, { color: Assets.colors.appTheme }]}
          title={title ? title : 'Return Item'}
          LeftComponent={<BackButton />}
        />
        <ScrollView style={{ flex: 1, marginTop: 15 }} keyboardDismissMode="interactive">
          {item != null && item['additional_fees'] !== null && <RentalItem style={styles.itemContainer} {...item} />}
          <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
            <Text style={styles.descriptionText} text="Note any damage of imperfections:" />
            <Textarea
              ref={this._textArea}
              style={styles.inputContainer}
              placeholder="Item’s current condition, was it dry cleaned, etc.sss"
              placeholderTextColor={Assets.colors.placeholder}
              inputStyle={styles.input}
              inputAccessoryViewID="snapPhotos"
              InputAccessoryComponent={
                <Text
                  containerStyle={Styles.nextAccessoryContainer}
                  style={Styles.nextAccessoryText}
                  text="Next: Snap a few photos"
                  onPress={() => {
                    Keyboard.dismiss()
                    this._onTapNextButton()
                  }}
                />
              }
            />
          </View>
        </ScrollView>

        <View style={{ height: footerHeight, marginRight: 25 }}>
          <Row alignVertical="center" alignHorizontal="flex-end">
            <Text style={styles.buttonDescription} text="Next: Snap a few photos" />
            <Icon
              iconStyle={styles.nextButtonImage}
              iconSource={Assets.images.next_round}
              onPress={this._onTapNextButton}
            />
          </Row>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    marginTop: 15,
    paddingLeft: 10,
    marginBottom: 20,
    paddingBottom: 36,
    borderBottomWidth: 1,
    borderBottomColor: Assets.colors.borderColorLight
  },
  descriptionText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    lineHeight: 18,
    textAlign: 'left',
    color: Assets.colors.mainText
  },

  inputContainer: {
    marginTop: 15,
    backgroundColor: Assets.colors.inputBg,
    borderRadius: 6,
    height: 112,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 6,
    lineHeight: 22
  },

  input: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    padding: 10,
    lineHeight: 22,
    height: 112
  },

  nextButtonImage: {
    height: 64,
    width: 64
  },
  buttonDescription: {
    fontFamily: Assets.fonts.display.regular,
    fontSize: 16,
    color: Assets.colors.textLight,
    marginRight: 20
  }
})

export default ReturnItemDetail
