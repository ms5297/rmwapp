import React from 'react'
import { Image, View, TouchableOpacity, StyleSheet } from 'react-native'
import { RNCamera } from 'react-native-camera'
import { NavigationProps } from '@Types'
import { Header, Text, Icon, Row } from 'rn-components'
import { BackButton } from '@Components'
import { Device, Constants, LayoutAnimations } from '@Utils'
import { ReturnPhotoParam, ReturnPhotoModel } from '@Models'
import Assets from '@Assets'
import { Navigator } from '@Navigation'
import CameraPendingView from './CameraPendingView'

type Props = {
  navigation: NavigationProps
}

type State = ReturnPhotoParam & {
  images: Array<string>
  currentPhoto: string
  progress: number
}

class ReturnPhoto extends React.Component<Props, State> {
  _titles = ['Front of item', 'Back of item', 'Dry cleaning receipt']
  _subTitles = [
    'Take your photo in a well lit place.',
    'Take your photo in a well lit place.',
    'Make sure the full receipt is captured and legible.'
  ]
  _cameraRef = React.createRef<RNCamera>()

  constructor(props) {
    super(props)
    const param: ReturnPhotoParam = this.props.navigation.getParam('param')
    let images = ['', '', '']
    if (param.order.hasCleaning == false) {
      this._titles = ['Front of item', 'Back of item']
      this._subTitles = ['Take your photo in a well lit place.', 'Take your photo in a well lit place.']
      images = ['', '']
    }
    this.state = {
      images,
      currentPhoto: '',
      progress: 0,
      ...param
    }

    LayoutAnimations.enableAndroidLayoutAnimation()
  }

  _canShowDone = () => {
    const { order, images } = this.state
    const hasCleaning = order.hasCleaning
    if (hasCleaning) {
      return images[0].length > 0 && images[1].length > 0 && images[2].length > 0
    }
    return images[0].length > 0 && images[1].length > 0
  }

  _onPressDoneButton = () => {
    const { images, onReturnItem } = this.state

    if (typeof onReturnItem === 'function') {
      let photos: Array<ReturnPhotoModel> = []
      for (let index = 0; index < this._titles.length; ++index) {
        photos.push({
          title: this._titles[index],
          image: images[index]
        })
      }
      onReturnItem(photos)
    } else {
      Navigator.showToast('Error', '(RE - 70) Internal Error', 'Error')
    }
  }

  _renderImageItem = image => {
    const { images, progress } = this.state
    const retake = () => {
      let array = [...images]
      array.splice(progress, 1, '')
      this.setState({ images: array })
    }
    const usePhoto = () => {
      if (progress + 1 == this._titles.length) {
        this._onPressDoneButton()
      } else {
        this.setState({
          progress: progress + 1
        })
      }
    }
    return (
      <View style={styles.imageContainer}>
        <Image style={styles.image} resizeMode={'contain'} source={{ uri: image }} />
        <Row style={{ marginTop: 20 }} alignHorizontal="space-between">
          <Text style={[styles.photoOptionsText, { paddingLeft: 25 }]} text={`Retake\nPhoto`} onPress={retake} />
          <Text style={[styles.photoOptionsText, { paddingRight: 25 }]} text={`Use\nPhoto`} onPress={usePhoto} />
        </Row>
      </View>
    )
  }

  _takePicture = async () => {
    if (this._cameraRef.current) {
      const options = { width: 375, pauseAfterCapture: true }
      const data = await this._cameraRef.current.takePictureAsync(options)
      var array = this.state.images
      array.splice(this.state.progress, 1, data.uri)
      LayoutAnimations.setLayoutAnimation(LayoutAnimations.PresetEaseInOut)
      this.setState({ images: array })
    }
  }

  render() {
    const { images, progress } = this.state
    const isIPhoneX = Device.isIphoneX()
    const subTitle = images[progress].length == 0 ? this._subTitles[progress] : ' '
    const title = this._titles[progress]

    return (
      <View style={[StyleSheet.absoluteFill, { backgroundColor: 'black' }]}>
        <Header
          backgroundColor="black"
          statusBarProps={{ barStyle: 'light-content', backgroundColor: 'black' }}
          title=""
          LeftComponent={<BackButton iconSource={Assets.images.closeIcon} />}
        />
        <View style={{ flex: 1, marginHorizontal: 5, paddingBottom: isIPhoneX ? 20 : 0 }}>
          <Text style={styles.titleText} text={title} />
          <Text style={styles.subTitleText} text={subTitle} />
          {images[progress].length == 0 && (
            <View style={{ flex: 1 }}>
              <RNCamera
                ref={this._cameraRef}
                captureAudio={false}
                style={styles.cameraContainer}
                type={RNCamera.Constants.Type.back}
                flashMode={RNCamera.Constants.FlashMode.off}
                androidCameraPermissionOptions={{
                  title: 'Permission to use camera',
                  message: 'We need your permission to use your camera.'
                }}>
                {({ camera, status }) => {
                  if (status !== 'READY')
                    return (
                      <CameraPendingView status={status} onEnabled={() => this._cameraRef.current.resumePreview()} />
                    )
                }}
              </RNCamera>
              <Icon
                style={styles.cameraButton}
                iconStyle={{ height: 100, width: 100 }}
                iconSource={Assets.images.returnPhotoCamera}
                onPress={this._takePicture}
              />
            </View>
          )}
          {images[progress].length > 0 && this._renderImageItem(images[progress])}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  titleText: {
    marginTop: 9,
    marginLeft: 15,
    fontFamily: Assets.fonts.display.bold,
    fontSize: 30,
    color: 'white'
  },
  imageContainer: {
    width: Constants.WINDOW_WIDTH,
    height: 475 * (Constants.WINDOW_WIDTH / 375) - 80 - 20,
    marginTop: 20
  },
  image: {
    width: Constants.WINDOW_WIDTH,
    height: 475 * (Constants.WINDOW_WIDTH / 375) - 80 - 20
  },

  cameraContainer: {
    height: 475 * (Constants.WINDOW_WIDTH / 375) - 80 - 20,
    backgroundColor: '#F6F6F6',
    marginTop: 20,
    justifyContent: 'flex-end'
  },
  cameraButton: {
    marginTop: 40,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center'
  },
  photoOptionsText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: Assets.fonts.display.regular,
    fontSize: 16
  },
  subTitleText: {
    marginTop: 30,
    marginLeft: 15,
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: 'white'
  }
})

export default ReturnPhoto
