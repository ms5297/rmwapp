import React from 'react'
import { View } from 'react-native'
import { Text, StyleSheet } from 'rn-components'
import Assets from '@Assets'
import { Button } from '@Components'
import { AppConfig } from '@Utils'
import { Bubbles } from 'react-native-loader'
import { Permissions } from '@Services'

type Props = {
  status: 'PENDING_AUTHORIZATION' | 'NOT_AUTHORIZED'
  onEnabled: () => void
}
const CameraPendingView: React.SFC<Props> = props => {
  const { status } = props
  const onPress = () => {
    Permissions.openSettings(() => {
      Permissions.check('camera')
        .then(status => {
          console.log('status: ', status)
        })
        .catch(error => {
          console.log('error: ', error)
        })
    })
  }

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      {status === 'NOT_AUTHORIZED' ? (
        <View style={{ marginHorizontal: 30 }}>
          <Text style={styles.helpText} text={`Camera Services\ndisabled`} />
          <Text
            style={styles.infoText}
            text={`${
              AppConfig.name
            } needs access to your camera. Please turn on Camera Services in your device settings.`}
          />
          <Button style={{ marginTop: 20, marginHorizontal: 60 }} text="Open Settings" onPress={onPress} />
        </View>
      ) : (
        <View style={{ marginTop: 120 }}>
          <Bubbles size={7} color="white" />
        </View>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  helpText: {
    marginTop: 25,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    flex: 1
  },
  infoText: {
    marginTop: 15,
    fontSize: 16,
    color: 'white',
    fontFamily: Assets.fonts.text.regular,
    lineHeight: 22,
    flex: 1
  },

  textLink: {
    color: 'white',
    fontSize: 16,
    fontFamily: Assets.fonts.display.bold
  }
})

export default CameraPendingView
