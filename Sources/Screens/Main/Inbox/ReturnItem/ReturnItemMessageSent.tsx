import React from 'react'
import { View, StyleSheet, Image, Platform } from 'react-native'
import { Header, Text } from 'rn-components'
import { BackButton, Button } from '@Components'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Device } from '@Utils'
import { Navigator } from '@Navigation'
import { NavigationProps } from '@Types'
import Styles from '@Styles'
import Assets from '@Assets'

const isIphoneX = Device.isIphoneX()
type Props = {
  navigation: NavigationProps
}
class ReturnItemMessageSent extends React.Component<Props> {
  _onClose = (immediate = false) => {
    Navigator.popToTop()
    Navigator.pop()
  }

  _onRateAndReview = () => {
    this._onClose(true)
    const param = this.props.navigation.getParam('param')

    Navigator.navTo('RentalRating', { param })
  }

  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header
          titleStyle={Styles.headerTitle}
          title="Rental Return"
          LeftComponent={<BackButton iconSource={Assets.images.closeIcon} onPress={this._onClose} />}
        />
        <View style={styles.container}>
          <TouchableOpacity activeOpacity={0.7} style={styles.imageContainer}>
            <Image style={styles.image} source={Assets.images.checkboxOn} />
          </TouchableOpacity>
          <Text style={styles.text} text="Message Sent!" />
        </View>
        <View style={styles.buttonContainer}>
          <Button text="Rate & Review this Rental" onPress={this._onRateAndReview} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  imageContainer: {
    width: 100,
    height: 100,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Assets.colors.appTheme
  },
  image: {
    transform: [{ scale: 2 }],
    tintColor: 'white'
  },
  buttonContainer: {
    borderTopColor: Assets.colors.borderColorLight,
    borderTopWidth: 1,
    paddingTop: 15,
    paddingBottom: isIphoneX ? 35 : 20,
    paddingHorizontal: 15
  },
  text: {
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    marginTop: 30
  }
})

export default ReturnItemMessageSent
