import React from 'react'
import { View, StyleSheet, FlatList, Image, ScrollView, SafeAreaView, TouchableHighlight } from 'react-native'
import { Navigator } from '@Navigation'
import { NavigationProps } from '@Types'
import { BackButton, AsyncImage, Button } from '@Components'
import { Header, Text, Textarea, Icon, Row, Col } from 'rn-components'
import { getStore, ItemSummaryActions, FavoritesActions, ItemListActions, configStore } from '@ReduxManager'
import { ReturnItemParam, ChatModelOrderItem, ReturnItemDetailParam, Item, ReturnItemMessageLenderParam, UserProfile } from '@Models'
import { Device, Utils } from '@Utils'
import { Api, AppsFlyerHelper, GoogleAnalyticsHelper } from '@Services'
import Assets from '@Assets'
import Styles from '@Styles'
import { utils } from 'react-native-gifted-chat'
import Tooltip from 'react-native-walkthrough-tooltip';


type Props = {
  navigation: NavigationProps
}

type State = ReturnItemDetailParam & {
  items: Array<Item>,
  buy_now: number
  rental_price: number
  cleaning_discount: number
  service_fee: number
  total_price: number
  checked: boolean
  status: any
  owner_id: any
  userProfile: UserProfile
  rental_id: any,
  isSuccess: boolean,
  onAction,
  toolTipVisible:boolean
  // rentalDetail: any

}

const isIphoneX = Device.isIphoneX()
const footerHeight = isIphoneX ? 100 : 80

class CheckoutBuyNow extends React.Component<Props, State> {
  _textArea = React.createRef<Textarea>()
  _currentItemIndex = 0
  _lender = ''
  additional_fee: 0
  buy_now: number = 0
  rental_price: number = 0
  cleaning_discount: number = 0
  service_fee: number = 0
  servicefee: number = 0
  total_price: number = 0
  newItems = []
  owner_id: any
  status_buynow: any
  rental_idbuynow: number
  buyNowFlagsOnProducts: any = []




  constructor(props: Props) {
    super(props)
    const param: ReturnItemDetailParam = this.props.navigation.getParam('param')
    console.log(param);

    const onAction = param.purchaseAction
    let additional_fee: any[] = []
    let newItems: any[] = []
    this.state = {
      ...param,
      items: [],
      buy_now: 0,
      rental_price: 0,
      cleaning_discount: 0,
      service_fee: 0,
      total_price: 0,
      checked: false,
      status: null,
      owner_id: null,
      userProfile: null,
      rental_id: null,
      // servicefee: 0,
      isSuccess: false,
      onAction,
      toolTipVisible:false
      // rentalDetail: param
    }
  }

  componentDidMount() {
    const { items } = this.state.rentalDetail
    const { orders } = this.state.chatData
    if (items) {
      let newItem = []
      items.map((item: any) => {
        item['checked'] = false;
        newItem.push(item)
        if(item.buy_now && item.active){
          let ord_ites = orders.find(oItem => oItem.itemId == item.item_id && oItem.status == "request")

          if(ord_ites){
          this.buyNowFlagsOnProducts.push(item)}
        }
      })
      this.setState({ items: newItem })
        if(this.buyNowFlagsOnProducts.length === 1){
          this._calculateFeeDetails(this.buyNowFlagsOnProducts[0], 0,'')
        }
      }
  }
  _renderHeader = (isComplete = false) => {
    const color = isComplete ? Assets.colors.green : Assets.colors.mainText
    const { items } = this.state.rentalDetail
    return (
      <View style={styles.header}>
        <Text
          style={[styles.viewTitleLabel, { color }]}
          text="Buy Now"
        />
        {isComplete == false && this.buyNowFlagsOnProducts.length > 1 && (
          <Text
            style={styles.descriptionText}
            text="Select item you would like to purchase."
          />
        )}
      </View>
    )
  }
  _renderSectionHeader = (title: string, marginTop = 0, marginBottom = 10, marginLeft = 20) => {
    return (
      <View style={[styles.sectionView, { marginTop, marginBottom, marginLeft }]}>
        <Text style={styles.sectionTitle} text={title} />
      </View>
    )
  }
  _calculateFeeDetails(data, index,check) {
    let isSingleProduct:boolean = false;
    if(check == "onPress"){
      if(this.buyNowFlagsOnProducts.length === 1){
        isSingleProduct = true
      }
    }
    if(!isSingleProduct){
    const { service_fee, items } = this.state;

    let vals = data
    console.log(vals);
    let newArray = [...items];
    if(!newArray || newArray.length === 0){
      newArray.push(data)
    }
    let serviceFee: number;
    if (data.checked) {
      this.newItems.map((item: any, index: number) => {
        if (item) {
          if (item === vals.item_id) {
            this.newItems.splice(index, 1)
          }
        }
      })
      this.buy_now -= vals ? vals.replacement_fee : 0
      this.rental_price += vals ? vals.rental_fee : 0
      this.cleaning_discount += vals ? vals.cleaning_fee : 0
      this.total_price = this.buy_now + this.rental_price + this.cleaning_discount;

      serviceFee = this.total_price <= 0 ? 0 : parseFloat(Utils.calculateServiceFees(this.total_price));
      this.total_price = this.total_price + serviceFee

      newArray[index]['checked'] = false
    } else {
      this.newItems.push(vals.item_id)
      this.buy_now += vals ? vals.replacement_fee : 0
      this.rental_price -= vals ? vals.rental_fee : 0
      this.cleaning_discount -= vals ? vals.cleaning_fee : 0
      newArray[index]['checked'] = true
      this.total_price = this.buy_now + this.rental_price + this.cleaning_discount;
      serviceFee = this.total_price <= 0 ? 0 : parseFloat(Utils.calculateServiceFees(this.total_price));
      this.total_price = this.total_price + serviceFee
    }
    this.service_fee = service_fee
    this.setState({ buy_now: this.buy_now, rental_price: this.rental_price, cleaning_discount: this.cleaning_discount, service_fee: serviceFee, total_price: this.total_price, items: newArray });
  }
  }

  _renderFeeDetails() {
    const valueStyle: any = {
      marginRight: 30,
      lineHeight: 28,
      fontSize: 16,
      textAlign: 'right',
      marginLeft: 5,
      marginTop: 0
    }
    return (

      <View style={{ marginTop: 0, alignContent: 'center' }}>
        <Row alignVertical="center">
          <Text style={[styles.itemRetailPriceText, { marginLeft: 30, lineHeight: 28, flex: 1 }]} text="Buy Now Price" />
          <Text style={[styles.itemTitleText, valueStyle]} text={'$' + this.state.buy_now} />
        </Row>
        <Row alignVertical="center">
          <Text style={[styles.itemRetailPriceText, { marginLeft: 30, lineHeight: 28, flex: 1 }]} text="Rental Price" />
          <Text style={[styles.itemTitleText, valueStyle]} text={'-$' + Math.abs(this.state.rental_price)} />
        </Row>
        <Row alignVertical="center">
          <Text style={[styles.itemRetailPriceText, { marginLeft: 30, lineHeight: 28, flex: 1 }]} text="Cleaning Fee Discount" />
          <Text style={[styles.itemTitleText, valueStyle]} text={'-$' + Math.abs(this.cleaning_discount)} />
        </Row>
        <Row alignVertical="center" >
          <Text style={[styles.itemRetailPriceText, { marginLeft: 30, lineHeight: 28, flex: 1 }]} text="Service Fee" />
          <Text style={[styles.itemTitleText, valueStyle]} text={'$' + this.state.service_fee} />
        </Row>
        <Row style={{
          borderBottomColor: '#ddd',
          borderBottomWidth: 1, width: '85%', marginLeft: 30
        }}></Row>
        <Row alignVertical="center" >
          <Text style={[styles.itemRetailPriceText, { marginLeft: 30, lineHeight: 28, flex: 1 }]} text="Total Price" />
          <Text style={[styles.itemTitleText, valueStyle]} text={'$' + this.state.total_price.toFixed(2)} />
        </Row>
      </View>
    )
  }

  _renderPriceSection = () => {
    return (
      <Row>
        <View>
          {this._renderSectionHeader('Prices & Fees', 0, 0)}
        </View>
      </Row>
    )
  }
  _renderRenterMessage = () => {
    // const { items } = this.state
    // const item = items[0]
    // if (!item.refund_option_id) {
    //   return null
    // }

    let renterMessage = 'Rent My Wardrobe charges no additional service fees should you elect to buy this item.  A standard 3% Credit Card processing fee will still apply.'
  
    

    return (
      <View style={{ flexDirection: 'column', marginTop: 0 }}>
        <Text style={[styles.itemTitleText, { marginLeft: 15, fontSize: 16 }]} text="Note" />
        <Text style={[styles.itemRetailPriceText, { marginLeft: 15, marginTop: 10, marginRight: 15 }]}>
          {renterMessage}
        </Text>
      </View>
    )
  }

  _renderCheckoutBtn = () => {
    return (
      <View>
        <Button
          style={[styles.positiveButtonChekout, { width: '85%', marginLeft: 30, marginTop: 10 }]}
          textStyle={styles.positiveButtonText}
          text="Checkout"
          onPress={this._handleBuyNowAction}
        />
      </View>

    )
  }

  _handleBuyNowAction = () => {
    const { items } = this.state;

    const { userProfile } = getStore().getState()
    const totalPayment = this.total_price.toFixed(2)
    const payload = {
      "rental_id": this.rental_idbuynow,
      "user_id": userProfile.id,
      "owner_id": this.owner_id,
      "modified_by": userProfile.id,
      "buy_now_amount": this.state.buy_now,
      "rental_fee": this.state.rental_price,
      "cleaning_fee": this.state.cleaning_discount,
      "service_fee": this.state.service_fee,
      "wallet_amount": 0,
      "total_payment": totalPayment,
      "item_ids": this.newItems,
      "status": "request"
    }
    const purchasePayload = { "purchase": payload }
    console.log(purchasePayload);
    if (this.newItems.length > 0) {
      Navigator.showLoading()
      Api.buyNowRequest<any>(purchasePayload)
        .then(response => {
          Navigator.hideLoading()
          if (response.code !== 200) {
            throw Error(response.message || '(IC - 63) Internal Error')
          }

          this.sendPurchasedMessage()
          this.setState({ isSuccess: true })
          AppsFlyerHelper.trackEvent(AppsFlyerHelper.EVENTS.BUY_NOW_TRANSACTION_CREATED, userProfile.id, this.rental_idbuynow)
          GoogleAnalyticsHelper.trackEvent(GoogleAnalyticsHelper.EVENT_CATEGORY.MAIN, GoogleAnalyticsHelper.EVENT.BUY_NOW_TRANSACTION_CREATED, { user_id: userProfile.id, buynow_rental_id: this.rental_idbuynow })
          // Navigator.hideLoading()
          // Navigator.showToast('Error', response.message, 'Error')


        }).catch(error => {
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
        })
    } else {
      Navigator.hideLoading()
      Navigator.showToast('Error', "Please select at least one item to continue", 'Error')
    }
  }

  sendPurchasedMessage() {
    // const { items, chatData } = this.state

    const { onAction, items, chatData } = this.state
    //const isCompleted = items.length == this.newItems.length;
    let nonActive = []
    let returnItem = []
    if (items.length === chatData.orders.length) {
      chatData.orders.map((item) => {
        if (item.status == "returned") {
          returnItem.push(item)
        }
      })
      items.map((it) => {
        if (!it.active) {
          nonActive.push(it)
        }
      })
    }
    let usedItem = nonActive.length + returnItem.length + this.newItems.length
    const isCompleted = items.length == usedItem;

    let newArray = [];
    let isPartial = false;
    this.newItems.map((val, index) => {
      const findItem = items.find((item: any) => item.item_id == val);
      newArray.push(findItem)
    })
    const purchasedItems = Utils.getItemName(newArray);
    if (isCompleted) {
      if (returnItem.length > 0) {
        isPartial = false
        onAction(purchasedItems, isCompleted, isPartial);
      } else {
        isPartial = false
        onAction(purchasedItems, isCompleted, isPartial);
      }
    } else {
      isPartial = false
      onAction(purchasedItems, isCompleted, isPartial);
    }

  }

  _renderUnchecked = () => {
    return (
      <View style={{
        borderColor: '#7ae015',
        borderWidth: 1, width: 38, height: 38, borderRadius: 25, padding: 2.9
      }}>
      </View>
    )
  }
  _renderChecked = () => {
    return (
      <View style={{
        borderColor: '#7ae015',
        borderWidth: 1, width: 38, height: 38, borderRadius: 25, padding: 2.9
      }}>
        <View style={{ borderRadius: 25, backgroundColor: '#7ae015', padding: 7, marginRight: 10, width: 30, height: 30 }}>
          <Image style={{ tintColor: 'white' }} source={Assets.images.checkboxOn} />
        </View>
      </View>
    )
  }

  _renderItem = ({ item, index }) => {
    const { items, chatData } = this.state
    const currentItem = items[index];

    this.rental_idbuynow = chatData['rental_id']
    const returned = item.status === 'returned'
    console.log("items",items)

    this.owner_id = item && item.owner ? item.owner.id : ''
    const itemImage = item && item.photo ? item.photo.url : ''
    const color = returned ? Assets.colors.green : Assets.colors.textLight
    const backgroundColor = returned ? Assets.colors.green : 'transparent'
    const rental_fee = item ? item.rental_fee : ''
    const sizes = Utils.getSafeSizeIds(item)
    let prodStatus = null;

    chatData.orders.map((it) => {
      if (it.itemId === item.item_id) {
        prodStatus = it
      }
    })

    if (!item.buy_now || !item.active || prodStatus.status == "returned") {
      return (
        <View></View>
      )
    }
    
    return (
      <View>
        <Row key={index} style={[styles.rowItem, {}]} alignHorizontal="space-between" alignVertical="center" onPress={() => this._calculateFeeDetails(item, index, "onPress")}>

          {items.length > 1 ? <View style={[styles.iconContainer, { backgroundColor }]}>

            {item.checked ? <View style={{
              borderColor: '#59cb90',
              borderWidth: 1, width: 38, height: 38, borderRadius: 25, padding: 2.9
            }}>
              <View style={{ borderRadius: 25, backgroundColor: '#59cb90', padding: 7, marginRight: 10, width: 30, height: 30 }}>
                <Image style={{ tintColor: 'white' }} source={Assets.images.checkboxOn} />
              </View>
            </View> : <View style={{
              borderColor: '#59cb90',
              borderWidth: 1, width: 38, height: 38, borderRadius: 25, padding: 2.9
            }}>
              </View>
            }

          </View> : <View></View>}
          <View style={styles.imageContainer}>
            <AsyncImage
              style={styles.image}
              source={{ uri: itemImage }}
              fallbackImageSource={Assets.images.cartImagePlaceHolder}
            />
          </View>
          <Col style={{ marginHorizontal: 15 }} flex={1} alignVertical="center">
            <Text style={styles.name} text={item.name} />

            <Text style={styles.status}>
              Size: <Text style={[styles.status, { color }]} text={Utils.combineItemsSize(sizes)} />
            </Text>
            <Text style={styles.status}>
              Rental Price: <Text style={[styles.status, { color }]} text={'$' + rental_fee} />
            </Text>
          </Col>
        </Row>

      </View>

    )
  }
  _keyExtractor = (item, index) => index + ''

  _renderPurchaseItem = () => {
    const { chatData, items } = this.state
    const data = chatData.orders

    const returnedItem = data.filter(item => item.status === 'returned')
    const totalReturnedItem = Array.isArray(returnedItem) ? returnedItem.length : 0
    const isComplete = totalReturnedItem === data.length
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ flex: 4 }}>
          <FlatList
            // style={{ flex: 6 }}
            ListHeaderComponent={this._renderHeader(isComplete)}
            stickyHeaderIndices={[0]}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            extraData={items}
            data={items}
          />
        </View>

        <View style={{ flex: 6 }}>
          {this._renderPriceSection()}
          {this._renderFeeDetails()}
          {this._renderRenterMessage()}
          {this._renderCheckoutBtn()}
        </View>
      </SafeAreaView>
    )
  }
  _renderSuccessImage = () => {
    return (
      <View style={{
        borderColor: '#59cb90',
        borderWidth: 3, width: 105, height: 105, borderRadius: 50, padding: 3.9
      }}>
        <View style={{ borderRadius: 50, backgroundColor: '#59cb90', padding: 15, marginRight: 10, width: 92, height: 92, justifyContent: 'center', alignContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          <Image style={{ tintColor: 'white', width: 45, height: 45 }} source={Assets.images.checkboxOn} />
        </View>
      </View>
    )
  }
  _renderPurchaseSuccess = () => {
    return (
      <View style={{ alignContent: 'center', justifyContent: 'center', flex: 1 }}>
        <View style={{ alignContent: 'center', justifyContent: 'center', flexDirection: 'row' }}>
          {this._renderSuccessImage()}
        </View>
        <View style={{ alignContent: 'center', justifyContent: 'center', flexDirection: 'column', padding: 10, marginTop: 25 }}>
          <Text style={[styles.sectionTitle, { alignSelf: 'center', justifyContent: 'center', marginBottom: 18, textAlign: 'center' }]} text='Nicely done!' />
          {/* <Text style={{ alignSelf: 'center', justifyContent: 'center', textAlign: 'center' }} text='Your purchase is complete and your rental has been closed out.' /> */}

          <Text style={{ alignSelf: 'center', justifyContent: 'center', textAlign: 'center' }} text='Purchase request sent successfully !!!' />
        </View>
        <View style={{ alignContent: 'center', justifyContent: 'center', flexDirection: 'column', marginTop: 50, padding: 10 }}>
          <Button
            style={[styles.positiveButtonChekout]}
            textStyle={styles.positiveButtonText}
            text="Done"
            onPress={() => Navigator.back()}
          />
        </View>

      </View>
    )
  }

  //_keyExtractor = (item, index) => index + ''

  render() {
    const { chatData, items, isSuccess } = this.state

    // const data = chatData.orders


    // const returnedItem = data.filter(item => item.status === 'returned')
    // const totalReturnedItem = Array.isArray(returnedItem) ? returnedItem.length : 0
    // const isComplete = totalReturnedItem === data.length


    return (
      <View style={[StyleSheet.absoluteFill, { backgroundColor: Assets.colors.componentBg }]}>
        <Header
          titleStyle={Styles.headerTitle}
          title="Item Checkout"
          LeftComponent={<BackButton iconSource={Assets.images.closeIcon} />}
        />
       <Tooltip
          isVisible={this.state.toolTipVisible}
          content={<Text style={{paddingVertical:20, paddingHorizontal:20,backgroundColor:'red'}}>Check this out!</Text>}
          placement="top"
          onClose={() => this.setState({ toolTipVisible: false })}
        >
          {/* <TouchableHighlight>
            <Text>Press me</Text>
          </TouchableHighlight> */}
        </Tooltip>
        {!isSuccess ? this._renderPurchaseItem() : this._renderPurchaseSuccess()}
        
      </View>
    )
  }
}

const styles = StyleSheet.create({
  viewTitleLabel: {
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 30,
    color: Assets.colors.mainText,
    letterSpacing: -0.2,
    lineHeight: 34
  },
  rowItem: {
    paddingLeft: 25,
    paddingRight: 20,
    paddingVertical: 10,
    borderBottomColor: Assets.colors.borderColorLight,
    borderBottomWidth: 1
  },
  imageContainer: {
    width: 50,
    height: 60,
    backgroundColor: Assets.colors.inputBg,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10
  },
  image: {
    width: 50,
    height: 60,
    backgroundColor: Assets.colors.inputBg
  },
  descriptionText: {
    marginTop: 28,
    fontFamily: Assets.fonts.display.regular,
    fontSize: 14,
    lineHeight: 18,
    textAlign: 'left',
    color: Assets.colors.mainText
  },

  header: {
    // marginTop: 25,
    paddingHorizontal: 25,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: Assets.colors.borderColorLight,
    backgroundColor: 'white'
  },
  nextButtonImage: {
    height: 64,
    width: 64
  },
  iconContainer: {
    width: 32,
    height: 32,
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center'
  },
  name: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    color: Assets.colors.mainText
  },
  sectionView: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: 10
  },
  sectionTitle: {
    // textAlign: 'center',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 24,
    color: Assets.colors.mainText,
    letterSpacing: -0.2,
  },
  status: {
    marginTop: 3,
    fontFamily: Assets.fonts.display.regular,
    fontSize: 14,
    color: Assets.colors.textLight
  },
  buttonContainer: {
    borderTopColor: Assets.colors.borderColorLight,
    borderTopWidth: 1,
    paddingTop: 15,
    paddingBottom: isIphoneX ? 35 : 20,
    paddingHorizontal: 15
  },
  buttonDescription: {
    fontFamily: Assets.fonts.display.regular,
    fontSize: 16,
    color: Assets.colors.textLight,
    marginRight: 20
  }, helper: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.4,
    marginBottom: 10
  }, itemRetailPriceText: {
    marginLeft: 23,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    letterSpacing: -0.2,
    lineHeight: 18,
    color: Assets.colors.textLight
  },
  itemTitleText: {
    // marginLeft: 5,
    marginTop: 8,
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: Assets.colors.mainText,
    lineHeight: 18
  },
  positiveButtonChekout: {

  },
  positiveButtonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: 'white',
    paddingVertical: 8
  },
})

export default CheckoutBuyNow
