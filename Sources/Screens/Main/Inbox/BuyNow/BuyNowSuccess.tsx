import React from 'react'
import { View, StyleSheet, FlatList, Image } from 'react-native'
import { BackButton, AsyncImage, Button } from '@Components'
import { Header, Text, Textarea, Icon, Row, Col, Device } from 'rn-components'
import Assets from '@Assets'
import Styles from '@Styles'
import { Navigator } from '@Navigation'



// const isIphoneX = Device.isIphoneX()
// const footerHeight = isIphoneX ? 100 : 80

class CheckoutBuyNow extends React.Component {




  constructor(props) {
    super(props)
  }


  // _renderHeader = () => {

  //     return (
  //       <View style={styles.header}>
  //         <Text
  //           style={[styles.viewTitleLabel, { color:"#000" }]}
  //           text="Buy Now"
  //         />
  //       </View>
  //     )
  // }


  _renderSectionHeader = (title: string, marginTop = 20, marginBottom = 10, marginLeft = 20) => {
    return (
      <View style={[styles.sectionView, { marginTop, marginBottom, marginLeft }]}>
        <Text style={styles.sectionTitle} text={title} />
      </View>
    )
  }

  _renderChecked = () => {
    return (
      <View style={{
        borderColor: '#7ae015',
        borderWidth: 1, width: 60, height: 60, borderRadius: 30, padding: 3.9
      }}>
        <View style={{ borderRadius: 25, backgroundColor: '#7ae015', padding: 15, marginRight: 10, width: 50, height: 50 }}>
          <Image style={{ tintColor: 'white' }} source={Assets.images.checkboxOn} />
        </View>
      </View>
    )
  }


  _renderItem = ({ item, index }) => {


  }

  //   _keyExtractor = (item, index) => index + ''

  render() {

    return (
      <View style={[StyleSheet.absoluteFill, { backgroundColor: Assets.colors.componentBg }]}>
        <Header
          titleStyle={Styles.headerTitle}
          title="Item Checkout"
        //   LeftComponent={<BackButton iconSource={Assets.images.closeIcon} />}
        />
        <View style={{ marginLeft: 185, marginTop: 160, alignContent: 'center' }}>
          {this._renderChecked()}
        </View>
        <Text style={[styles.sectionTitle, {}]} text='Nicely done!' />
        <Text style={{ marginTop: 30, width: '85%', marginLeft: 30 }} text='Your purchase is complete and your rental has been closed out.' />

        <Button
          style={[styles.positiveButtonChekout, { width: '85%', marginLeft: 30, marginTop: 170 }]}
          textStyle={styles.positiveButtonText}
          text="Done"
          onPress={() => Navigator.navTo('Inbox')}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  viewTitleLabel: {
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 30,
    color: Assets.colors.mainText,
    letterSpacing: -0.2,
    lineHeight: 34
  },
  rowItem: {
    paddingLeft: 25,
    paddingRight: 20,
    paddingVertical: 10,
    borderBottomColor: Assets.colors.borderColorLight,
    borderBottomWidth: 1
  },
  imageContainer: {
    width: 50,
    height: 60,
    backgroundColor: Assets.colors.inputBg,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10
  },
  image: {
    width: 50,
    height: 60,
    backgroundColor: Assets.colors.inputBg
  },
  descriptionText: {
    marginTop: 28,
    fontFamily: Assets.fonts.display.regular,
    fontSize: 14,
    lineHeight: 18,
    textAlign: 'left',
    color: Assets.colors.mainText
  },

  header: {
    marginTop: 25,
    paddingHorizontal: 25,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: Assets.colors.borderColorLight
  },
  nextButtonImage: {
    height: 64,
    width: 64
  },
  iconContainer: {
    width: 32,
    height: 32,
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center'
  },
  name: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    color: Assets.colors.mainText
  },
  sectionView: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: 10
  },
  sectionTitle: {
    textAlign: 'center',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 25,
    color: Assets.colors.mainText,
    letterSpacing: -0.2
  },
  status: {
    marginTop: 3,
    fontFamily: Assets.fonts.display.regular,
    fontSize: 14,
    color: Assets.colors.textLight
  },
  buttonContainer: {
    borderTopColor: Assets.colors.borderColorLight,
    borderTopWidth: 1,
    paddingTop: 15,
    // paddingBottom: isIphoneX ? 35 : 20,
    paddingHorizontal: 15
  },
  buttonDescription: {
    fontFamily: Assets.fonts.display.regular,
    fontSize: 16,
    color: Assets.colors.textLight,
    marginRight: 20
  }, helper: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.4,
    marginBottom: 10
  }, itemRetailPriceText: {
    marginLeft: 23,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    letterSpacing: -0.2,
    lineHeight: 18,
    color: Assets.colors.textLight
  },
  itemTitleText: {
    // marginLeft: 5,
    marginTop: 8,
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: Assets.colors.mainText,
    lineHeight: 18
  },
  positiveButtonChekout: {

  },
  positiveButtonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: 'white',
    paddingVertical: 8
  },
})

export default CheckoutBuyNow
