import React from 'react'
import { View, StyleSheet, FlatList, Image } from 'react-native'
import { Navigator } from '@Navigation'
import { NavigationProps } from '@Types'
import { BackButton, AsyncImage, Button } from '@Components'
import { Header, Text, Textarea, Icon, Row, Col } from 'rn-components'
import {
  ChatModelOrderItem,
  Item,
  ReturnItemMessageLenderParam,
  RentalRatingParam,
  RentalRatingDetailParam
} from '@Models'
import { Device, Utils } from '@Utils'
import { Api, AppsFlyerHelper, GoogleAnalyticsHelper } from '@Services'
import Assets from '@Assets'
import Styles from '@Styles'
import StarRating from 'react-native-star-rating'
import { getStore } from '@ReduxManager'

type Props = {
  navigation: NavigationProps
}

type State = RentalRatingParam & {
  items: Array<Item>
}

const isIphoneX = Device.isIphoneX()
const footerHeight = isIphoneX ? 100 : 80

class RentalRating extends React.Component<Props, State> {
  _textArea = React.createRef<Textarea>()
  _currentItemIndex = 0
  _lender = ''

  constructor(props: Props) {
    super(props)
    const param: RentalRatingParam = this.props.navigation.getParam('param')
    let chatData = param.chatData
    if (!chatData.orders) {
      chatData.orders = [chatData.order].map(chat => ({
        ...chat,
        status: chatData.status,
        rental_id: chatData.rental_id
      }))
    }
    this.state = {
      ...param,
      items: param.items || []
    }
  }

  componentDidMount() {
    if (this.state.items.length == 0) {
      Navigator.showLoading()
      const ids = this.state.chatData.orders.map(item => item.itemId)
      Api.getItemDetails(ids)
        .then((response: any) => {
          const { code, message, items } = response
          if (code !== 200) throw Error(message)

          this.setState({ items })
          Navigator.hideLoading()
        })
        .catch(error => {
          Navigator.hideLoading()
          console.log('**** error', error)
        })
    }
  }

  _onTapNextButton = () => {
    const { onReviewItem, items, chatData, rentalDetail } = this.state
    const { orders } = chatData
    const item = orders[this._currentItemIndex]
    if (orders.length != items.length) {
      const ids = this.state.chatData.orders.map(item => item.itemId)
      Api.getItemDetails(ids)
        .then((response: any) => {
          const { code, message, items } = response
          if (code !== 200) throw Error(message)

          this.setState({ items })
          Navigator.hideLoading()
        })
        .catch(error => {
          Navigator.hideLoading()
          console.log('**** error', error)
        })
    }
    // const itemDetail = items[this._currentItemIndex]

    let itemDetail = null
    items.map((itemR, index) => {
      if (itemR.id === item.itemId) {
        itemDetail = itemR
      }
    })
    const title = `Item ${this._currentItemIndex + 1} of ${orders.length}`
    const isComplete = this._currentItemIndex + 1 === orders.length

    const param: RentalRatingDetailParam = {
      chatData,
      item: itemDetail,
      rentalDetail,
      onReviewed: avgCount => {
        Navigator.pop(2, false)
        // Api.getItemDetails([itemId])
        //   .then((response: any) => {
        //     const { code, message, items } = response;
        //     if (code !== 200) throw Error(message);

        //   const index = this.state.items.findIndex(it => it.id === items[0].id);
        //   if (index != -1) {
        //     const cloneItems = this.state.items.slice();
        //     cloneItems[index] = items[0];
        //     this.setState({ items: cloneItems });
        //   }
        // })
        // .catch(error => {
        //   console.log("**** getItemDetails error", error);
        // });

        const newOrder = {
          ...item,
          rating: avgCount
        }

        onReviewItem(newOrder, isComplete)
          .then(chatData => {
            console.log('***** onSuccess chatData', chatData)

            this.setState({ chatData })
          })
          .catch(error => {
            console.log('**** onReviewItem error', error)
          })
        // Promise.all([Api.getItemDetails([itemId]), onReviewItem(item, isComplete)]).then((values: any) => {
        //   const { code, message, items } = values[0];
        //   const chatData = values[1];
        //   if (code !== 200) throw Error(message);
        //   if (!chatData) throw Error("Invalid data");

        //   const index = this.state.items.findIndex(it => it.id === items[0].id);
        //   if (index != -1) {
        //     const cloneItems = this.state.items.slice();
        //     cloneItems[index] = items[0];
        //     this.setState({ items: cloneItems, chatData });
        //   } else {
        //     this.setState({ chatData });
        //   }
        // });
      },
      title
    }
    Navigator.navTo('RentalRatingDetail', { param })
  }

  _renderHeader = (isComplete = false) => {
    const totalItem = this.state.chatData.orders.length
    const numItems = totalItem === 1 ? 'item' : `items`
    const color = isComplete ? Assets.colors.appTheme : Assets.colors.mainText
    return (
      <View style={styles.header}>
        <Text
          style={[styles.viewTitleLabel, { color }]}
          text={
            isComplete
              ? 'Thanks for your rating & review of this rental'
              : `Please Rate and Review the ${numItems} you rented and your experience with the lender`
          }
        />

        {isComplete == false && (
          <Text
            style={styles.descriptionText}
            text="Include some sub copy about this process for the users to maybe better understand."
          />
        )}
      </View>
    )
  }

  _onDone = () => {
    const { chatData, onRetryUpdateReviewItem } = this.state
    onRetryUpdateReviewItem(chatData)
      .then(() => {
        const { userProfile } = getStore().getState()
        AppsFlyerHelper.trackEvent(AppsFlyerHelper.EVENTS.RENTAL_TRANSACTION_COMPLETED, userProfile.id, chatData.rental_id)

        GoogleAnalyticsHelper.trackEvent(GoogleAnalyticsHelper.EVENT_CATEGORY.MAIN, GoogleAnalyticsHelper.EVENT.RENTAL_TRANSACTION_COMPLETED, { user_id: userProfile.id, rental_id: chatData.rental_id })

        Navigator.back()
      })
      .catch(error => {
        console.log('**** errror', error)
        Navigator.back()
      })
  }

  _renderItem = ({ item, index }: { item: ChatModelOrderItem; index: number }) => {
    let { items } = this.state.rentalDetail ? this.state.rentalDetail : this.state;
    // let products = []
    // if (this.state.rentalDetail) { 
    //   products = this.state.rentalDetail.items
    // } else {
    //   products = this.state.items
    // }
    // let items = products
    // this.setState({
    //   items:products
    // })
    // if (this.state.rentalDetail) {
    // let { items } = this.state
    // }
    console.log(items)
    const reviewed = item.status === 'reviewed'
    const status = reviewed ? '' : 'Rate this item    '
    // const itemDetail = items.length ? items[index] : null
    let itemDetail = null
    let newItems = []
    items.map((itemR, index) => {
      let itemId;
      if (itemR.item_id) {
        itemId = itemR.item_id
      } else {
        itemId = itemR.id
      }
      if (itemId === item.itemId) {
        itemDetail = itemR
        newItems.push(itemR)
      }
    })
    items = newItems

    // let itemImage = null
    // // if (itemDetail.item_id == item.itemId) {
    //    itemImage = itemDetail ? Utils.getPhotoUrlFromList(itemDetail.photo, '') : ''
    // // }

    //const itemImage = itemDetail ? itemDetail.photo['url'] : ''

    let itemImage = itemDetail ? Utils.getPhotoUrlFromList(itemDetail.photos, '') : ''
    if (itemImage == "") {
      let photo = itemDetail ? itemDetail.photo.url : ''
      // let photo1 = itemDetail.photo['url']
      itemImage = photo ? photo : ''
    }
    const color = reviewed ? Assets.colors.appTheme : Assets.colors.textLight
    const backgroundColor = reviewed ? Assets.colors.appTheme : 'transparent'
    this._lender = itemDetail ? itemDetail.owner.name : ''


    return (
      <Row key={index} style={styles.rowItem} alignHorizontal="space-between" alignVertical="center">
        <View style={styles.imageContainer}>
          <AsyncImage style={styles.image} source={{ uri: itemImage }} />
        </View>
        <Col style={{ marginHorizontal: 15 }} flex={1} alignVertical="center">
          <Text style={styles.name} text={item.itemName} />
          <Row style={{ marginTop: 5 }} alignVertical="center">
            <Text style={[styles.status, { color }]} text={status} />
            <StarRating
              disabled
              maxStars={5}
              rating={item.rating || 0}
              emptyStar={Assets.images.ratingEmptyStar}
              fullStar={Assets.images.ratingFullStar}
              halfStar={Assets.images.ratingHalfStar}
              halfStarEnabled={true}
              starSize={12}
            />
          </Row>
        </Col>

        <View style={[styles.iconContainer, { backgroundColor }]}>
          {reviewed && <Image style={{ tintColor: 'white' }} source={Assets.images.checkboxOn} />}
        </View>
      </Row>
    )
  }

  _keyExtractor = (item, index) => index + ''

  render() {
    const { chatData, items } = this.state

    if (items == undefined || items.length < 1) {
      return <View></View>
    }

    const data = chatData.orders

    const reviewedItem = data.filter(item => item.status === 'reviewed')
    const totalReviewedItem = Array.isArray(reviewedItem) ? reviewedItem.length : 0

    const isComplete = totalReviewedItem === data.length

    this._currentItemIndex = totalReviewedItem

    return (
      <View style={[StyleSheet.absoluteFill, { backgroundColor: Assets.colors.componentBg }]}>
        <Header
          titleStyle={Styles.headerTitle}
          title="Rate & Review"
          LeftComponent={<BackButton iconSource={Assets.images.closeIcon} />}
        />
        <FlatList
          style={{ flex: 1 }}
          ListHeaderComponent={this._renderHeader(isComplete)}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          extraData={items}
          data={data}
        />

        {isComplete == false && (
          <View style={{ height: footerHeight, marginRight: 25 }}>
            <Row alignVertical="center" alignHorizontal="flex-end">
              <Text
                style={styles.buttonDescription}
                text={`Rate & Review: Item ${totalReviewedItem + 1} of ${data.length}`}
              />
              <Icon
                iconStyle={styles.nextButtonImage}
                iconSource={Assets.images.next_round}
                onPress={this._onTapNextButton}
              />
            </Row>
          </View>
        )}
        {isComplete && (
          <View style={styles.buttonContainer}>
            <Button text="Complete this rental" onPress={this._onDone} />
          </View>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  viewTitleLabel: {
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 26,
    color: Assets.colors.mainText,
    letterSpacing: -0.2,
    lineHeight: 34
  },
  rowItem: {
    paddingLeft: 25,
    paddingRight: 20,
    paddingVertical: 10,
    borderBottomColor: Assets.colors.borderColorLight,
    borderBottomWidth: 1
  },
  iconContainer: {
    width: 32,
    height: 32,
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageContainer: {
    width: 50,
    height: 60,
    backgroundColor: Assets.colors.inputBg,
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    width: 50,
    height: 60,
    backgroundColor: Assets.colors.inputBg
  },
  descriptionText: {
    marginTop: 20,
    marginBottom: 20,
    fontFamily: Assets.fonts.display.regular,
    fontSize: 14,
    lineHeight: 18,
    textAlign: 'left',
    color: Assets.colors.mainText
  },

  header: {
    marginTop: 25,
    paddingHorizontal: 25,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: Assets.colors.borderColorLight
  },
  nextButtonImage: {
    height: 64,
    width: 64
  },
  name: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    color: Assets.colors.mainText
  },
  status: {
    fontFamily: Assets.fonts.display.regular,
    fontSize: 14,
    color: Assets.colors.textLight
  },
  buttonContainer: {
    borderTopColor: Assets.colors.borderColorLight,
    borderTopWidth: 1,
    paddingTop: 15,
    paddingBottom: isIphoneX ? 35 : 20,
    paddingHorizontal: 15
  },
  buttonDescription: {
    fontFamily: Assets.fonts.display.regular,
    fontSize: 16,
    color: Assets.colors.textLight,
    marginRight: 20
  }
})

export default RentalRating
