import React from 'react'
import { View, StyleSheet, Image, Platform } from 'react-native'
import { Row, Icon, Text } from 'rn-components'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Device } from '@Utils'
import Styles from '@Styles'
import Assets from '@Assets'

const isIphoneX = Device.isIphoneX()
const footerHeight = isIphoneX ? 100 : 80

class RentalRatingComplete extends React.Component {
  _onClose = () => {}

  _onRateAndReview = () => {}

  _onTapNextButton = () => {}

  render() {
    const totalReturnedItem = 1
    const totalItem = 4
    return (
      <View style={StyleSheet.absoluteFill}>
        <View style={styles.container}>
          <Text style={styles.viewTitleLabel} text="Thanks for your ranting & review of this item!" />
          <Text
            style={styles.descriptionText}
            text={`Item ${totalReturnedItem + 1} of ${totalItem} is now rated and reviewed.`}
          />
        </View>
        <View style={{ height: footerHeight, marginRight: 25 }}>
          <Row alignVertical="center" alignHorizontal="flex-end">
            <Text
              style={styles.buttonDescription}
              text={`Rate & Review: Item ${totalReturnedItem + 1} of ${totalItem}`}
            />
            <Icon
              iconStyle={styles.nextButtonImage}
              iconSource={Assets.images.next_round}
              onPress={this._onTapNextButton}
            />
          </Row>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  viewTitleLabel: {
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 30,
    color: Assets.colors.appTheme,
    letterSpacing: -0.2,
    lineHeight: 34
  },

  descriptionText: {
    marginTop: 28,
    fontFamily: Assets.fonts.display.regular,
    fontSize: 14,
    lineHeight: 18,
    textAlign: 'left',
    color: Assets.colors.mainText
  },

  container: {
    flex: 1
  },

  nextButtonImage: {
    height: 64,
    width: 64
  },
  buttonDescription: {
    fontFamily: Assets.fonts.display.regular,
    fontSize: 16,
    color: Assets.colors.textLight,
    marginRight: 20
  }
})

export default RentalRatingComplete
