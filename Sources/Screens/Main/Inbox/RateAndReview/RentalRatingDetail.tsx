import React from 'react'
import { View, Keyboard, StyleSheet } from 'react-native'
import { AsyncImage, BackButton, HorizontalListPhotos, Button, RentalItem } from '@Components'
import { Constants, Device, Utils, LayoutAnimations } from '@Utils'
import { Navigator } from '@Navigation'
import { NavigationProps } from '@Types'
import { Api, AppsFlyerHelper, GoogleAnalyticsHelper } from '@Services'
import { Header, Text, ScrollView, Textarea } from 'rn-components'
import { Item, RentalRatingDetailParam } from '@Models'
import Assets from '@Assets'
import StarRating from 'react-native-star-rating'
import { getStore } from '@ReduxManager'

type Props = {
  navigation: NavigationProps
}
type State = RentalRatingDetailParam & {
  accuracyRating: number
  cummunicationRating: number
  qualityRating: number
  exchangeRating: number
  valueRating: number
  reviewText: string
}

class RentalRatingDetail extends React.Component<Props, State> {
  _horizontalListPhotos = null

  constructor(props: Props) {
    super(props)
    const param = this.props.navigation.getParam('param')
    this.state = {
      accuracyRating: 0,
      cummunicationRating: 0,
      qualityRating: 0,
      exchangeRating: 0,
      valueRating: 0,
      reviewText: '',

      ...param
    }
    LayoutAnimations.enableAndroidLayoutAnimation()
  }

  _errorMessageForRenterRating = {
    accuracyRating: "Rate the overall accuracy of the renter's profile",
    cummunicationRating: 'Rate the communication with the other person',
    qualityRating: 'Rate the punctuality of the fitting or exchange',
    exchangeRating: 'Rate the condition of the item when returned',
    valueRating: 'Rate the promptness of the returned',
    reviewText: 'Please add review message for the renter'
  }

  _errorMessageForListerRating = {
    accuracyRating: 'Rate the overall accuracy of the rental description',
    cummunicationRating: 'Rate the communication and promptness of the lender',
    qualityRating: 'Rate how exchanging with the lender went',
    exchangeRating: 'Rate your experience with the lender',
    valueRating: 'Please add review message for this rental',
    reviewText: 'Please add review message for the lender'
  }

  componentWillMount() {
    const { chatData, item } = this.state
    if (chatData.itemOwner === false && item == null) {
      Navigator.showLoading()
      Api.getItemDetails([this.state.chatData.itemId])
        .then((response: any) => {
          const { code, message, error, items } = response
          if (code !== 200 || !Array.isArray(items) || items.length == 0) {
            throw Error(message ? message : error)
          }
          Navigator.hideLoading()
          LayoutAnimations.setLayoutAnimation(LayoutAnimations.Dropdown)
          this.setState({ item: items[0] })
        })
        .catch(error => {
          Navigator.hideLoading()
          console.log('getItemDetails error: ', error)
        })
    }
  }
  _getErrorMessage(state) {
    return this.state.chatData.itemOwner
      ? this._errorMessageForRenterRating[state]
      : this._errorMessageForListerRating[state]
  }

  _onTapReviewButton = () => {
    const { userProfile } = getStore().getState()

    Keyboard.dismiss()
    if (this.state.accuracyRating === 0) {
      Navigator.showToast('Error', this._getErrorMessage('accuracyRating'), 'Error')
      return
    }

    if (this.state.cummunicationRating === 0) {
      Navigator.showToast('Error', this._getErrorMessage('cummunicationRating'), 'Error')
      return
    }

    if (this.state.qualityRating === 0) {
      Navigator.showToast('Error', this._getErrorMessage('qualityRating'), 'Error')
      return
    }

    if (this.state.exchangeRating === 0) {
      Navigator.showToast('Error', this._getErrorMessage('exchangeRating'), 'Error')
      return
    }

    if (this.state.valueRating === 0) {
      Navigator.showToast('Error', this._getErrorMessage('valueRating'), 'Error')
      return
    }

    if (this.state.reviewText.trim().length === 0) {
      Navigator.showToast('Error', this._getErrorMessage('reviewText'), 'Error')
      return
    }

    Navigator.showLoading()
    if (this.state.chatData.itemOwner) {
      const data = {
        rental_id: this.state.chatData.rental_id,
        user_id: this.state.chatData.toUser.id,
        accuracy: this.state.accuracyRating,
        communication: this.state.cummunicationRating,
        exchange: this.state.qualityRating,
        return_condition: this.state.exchangeRating,
        return_on_time: this.state.valueRating,
        description: this.state.reviewText
      }
      Api.renterRating(data)
        .then((response: any) => {
          console.log('**** response owner', response)
          console.log(response.rating)
          // console.log(response.rating.reviewer)
          // console.log(response.rating.reviewer.id)
          const { code, message } = response
          if (code !== 200) throw Error(message || 'Internal Error.')
          Navigator.hideLoading()
          if (typeof this.state.onReviewed === 'function') {
            this.state.onReviewed(null)
          }
          AppsFlyerHelper.trackEvent(AppsFlyerHelper.EVENTS.LENDER_REVIEW_CREATED, userProfile.id, response.rating.reviewer.id)

          GoogleAnalyticsHelper.trackEvent(GoogleAnalyticsHelper.EVENT_CATEGORY.MAIN, GoogleAnalyticsHelper.EVENT.LENDER_REVIEW_CREATED, { user_id: userProfile.id, review_id: response.rating.reviewer.id })
        })
        .catch(error => {
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
        })
    } else {
      const images = this._horizontalListPhotos ? this._horizontalListPhotos.current.getPhotos() : []
      const data = {
        rental_id: this.state.chatData.rental_id,
        user_id: this.state.chatData.toUser.id,
        accuracy: this.state.accuracyRating,
        communication: this.state.cummunicationRating,
        ease_of_scheduling: this.state.qualityRating,
        exchange: this.state.exchangeRating,
        reasonable_pricing: this.state.valueRating,
        description: this.state.reviewText,
        item_id: this.state.item.id,
        images
      }

      Api.listerRating(data)
        .then((response: any) => {
          console.log('**** response renter', response)
          const { code, message, rating } = response
          if (code === 200 || code === 201) {
            const { accuracy, communication, ease_of_scheduling, reasonable_pricing, exchange } = rating
            const avgRating = (accuracy + communication + ease_of_scheduling + reasonable_pricing + exchange) / 5
            console.log('&&&&&& avgRating', avgRating)
            Navigator.hideLoading()
            if (typeof this.state.onReviewed === 'function') {
              Navigator.pop()
              this.state.onReviewed(avgRating)
            }
            AppsFlyerHelper.trackEvent(AppsFlyerHelper.EVENTS.RENTER_REVIEW_CREATED, userProfile.id, response.rating.reviewer.id)

            GoogleAnalyticsHelper.trackEvent(GoogleAnalyticsHelper.EVENT_CATEGORY.MAIN, GoogleAnalyticsHelper.EVENT.RENTER_REVIEW_CREATED, { user_id: userProfile.id, review_id: response.rating.reviewer.id })

          } else {
            throw Error(message || 'Internal Error.')
          }
        })
        .catch(error => {
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
        })
    }
  }

  render() {
    const { itemOwner } = this.state.chatData
    const title = itemOwner ? 'Rate & review the renter' : 'Rate & review this rental item'
    return (
      <View style={[StyleSheet.absoluteFill, { backgroundColor: Assets.colors.componentBg }]}>
        <Header
          title=""
          LeftComponent={<BackButton iconSource={itemOwner ? Assets.images.closeIcon : Assets.images.backIcon} />}
        />
        <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
          <Text style={styles.viewTitleLabel} text={title} />

          {itemOwner ? this._renderUserDetails() : this._renderItemDetails()}
          <View style={styles.divider} />
          {itemOwner ? this._renderUserRatings() : this._renderItemRatings()}

          {this._renderReviewMessage()}
          {this._renderUploadPhotos()}
        </ScrollView>
        <Button style={styles.button} text="Write Review" onPress={this._onTapReviewButton} />
      </View>
    )
  }

  _renderUploadPhotos = () => {
    if (this.state.chatData.itemOwner == true) {
      return <View style={{ width: 10, height: 45 }} />
    }

    return (
      <View style={{ marginTop: 16, marginLeft: 15, marginBottom: 45 }}>
        <Text style={styles.ratingTitleText} text="Add a photo" />
        <Text
          style={[styles.ratingDescriptionText, { marginTop: 4 }]}
          text="Help others to see the way this garment fit"
        />
        <View style={{ marginTop: 8, width: '100%' }}>
          <HorizontalListPhotos ref={this._horizontalListPhotos} />
        </View>
      </View>
    )
  }

  _renderUserDetails() {
    const { chatData } = this.state
    return (
      <View style={{ marginHorizontal: 20 }}>
        <View style={{ marginTop: 35, flexDirection: 'row' }}>
          <View style={styles.userImage}>
            <AsyncImage
              style={{ borderRadius: 30, overflow: 'hidden' }}
              source={{ uri: chatData.toUser.photo }}
              width={60}
              height={60}
              placeholderSource={Assets.images.user_photo_placeholder}
              fallbackImageSource={Assets.images.defaultProfile}
            />
          </View>
          <View style={{ flex: 1, flexDirection: 'column' }}>
            <Text style={styles.itemTitleText} text={chatData.toUser.name} />
            <Text style={[styles.itemTitleText, { marginLeft: 20, marginTop: 25 }]} text={chatData.order.itemName} />
            <Text style={[styles.itemRetailPriceText, { marginLeft: 20, marginTop: 5 }]} text={chatData.order.date} />
          </View>
        </View>
      </View>
    )
  }

  _renderItemDetails() {
    const { item: itemDetail } = this.state
    if (!itemDetail) return null

    return <RentalItem style={{ marginHorizontal: 20, marginTop: 20 }} {...itemDetail} />
  }

  _renderUserRatings() {
    return (
      <View>
        {this._renderRating(
          'Accuracy of Profile/Apperanace',
          "Rate the overall accuracy of the renter's profile",
          this.state.accuracyRating,
          1
        )}
        {this._renderRating(
          'Communication & Promptness',
          'Rate the communication with the other person',
          this.state.cummunicationRating,
          2
        )}
        {this._renderRating(
          'Fitting/Exchange Punctuality',
          'Rate the punctuality of the fitting or exchange',
          this.state.qualityRating,
          3
        )}
        {this._renderRating(
          'Item returned in proper condition',
          'Rate the condition of the item when returned',
          this.state.exchangeRating,
          4
        )}
        {this._renderRating('Returned on time', 'Rate the promptness of the returned', this.state.valueRating, 5)}
      </View>
    )
  }

  _renderItemRatings() {
    return (
      <View>
        {this._renderRating(
          'Accurate Description',
          'Rate the overall accuracy of the rental description',
          this.state.accuracyRating,
          1
        )}
        {this._renderRating(
          'Communication & Promptness',
          'Rate the communication and promptness of the lender',
          this.state.cummunicationRating,
          2
        )}
        {this._renderRating(
          'Ease of Scheduling Fitting & Exchange',
          'Rate how exchanging with the lender went',
          this.state.qualityRating,
          3
        )}
        {this._renderRating(
          'Fitting & Exchange Experience',
          'Rate your experience with the lender',
          this.state.exchangeRating,
          4
        )}
        {this._renderRating(
          'Reasonable Pricing',
          'How was the overall value including pricing for rental, replacement and dry cleaning fees',
          this.state.valueRating,
          5
        )}
      </View>
    )
  }

  _renderRating(title, description, rating, type) {
    return (
      <View style={styles.ratingRow}>
        <Text style={styles.ratingTitleText}>{title}</Text>
        <Text style={styles.ratingDescriptionText}>{description}</Text>
        <View style={{ marginTop: 8, width: 33 * 5 }}>
          <StarRating
            maxStars={5}
            rating={rating}
            emptyStar={Assets.images.ratingEmptyStar}
            fullStar={Assets.images.ratingFullStar}
            halfStar={Assets.images.ratingHalfStar}
            halfStarEnabled={true}
            starSize={33}
            selectedStar={rating => this.handleRatingSelection(type, rating)}
          />
        </View>
      </View>
    )
  }

  handleRatingSelection(type, rating) {
    switch (type) {
      case 1:
        this.setState({ accuracyRating: rating })
        break
      case 2:
        this.setState({ cummunicationRating: rating })
        break
      case 3:
        this.setState({ qualityRating: rating })
        break
      case 4:
        this.setState({ exchangeRating: rating })
        break
      case 5:
        this.setState({ valueRating: rating })
        break
      default:
        break
    }
  }

  _renderReviewMessage() {
    const placeholder = this.state.chatData.itemOwner
      ? 'How was your experience with this renter?'
      : 'Tell us how the garment fit and your overall experience.'
    return (
      <View style={styles.inputContainer}>
        <Textarea
          onChangeText={text => this.setState({ reviewText: text })}
          defaultValue={this.state.reviewText}
          style={styles.input}
          inputContainerStyle={{ backgroundColor: Assets.colors.inputBg }}
          placeholderTextColor={Assets.colors.placeholder}
          selectionColor={Assets.colors.mainText}
          placeholder={placeholder}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  viewTitleLabel: {
    marginTop: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 30,
    color: Assets.colors.appTheme,
    letterSpacing: -0.2,
    lineHeight: 34,
    marginHorizontal: 20
  },
  divider: {
    height: 1,
    backgroundColor: Assets.colors.borderColorLight,
    marginTop: 10
  },
  ratingRow: {
    marginTop: 20,
    paddingBottom: 20,
    borderBottomColor: Assets.colors.borderColorLight,
    borderBottomWidth: 1,
    paddingHorizontal: 20
  },
  userImage: {
    height: 60,
    width: 60,
    borderRadius: 30,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F4F6F6'
  },

  itemImage: {
    height: 85 * Constants.IMAGE_RATIO,
    width: 85,
    borderRadius: 6,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F4F6F6'
  },

  itemTitleText: {
    marginLeft: 20,
    marginTop: 8,
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: Assets.colors.mainText,
    lineHeight: 18
  },

  itemRetailPriceText: {
    marginLeft: 23,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    letterSpacing: -0.2,
    lineHeight: 18,
    color: Assets.colors.textLight
  },

  ratingTitleText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    color: Assets.colors.mainText,
    lineHeight: 20
  },

  ratingDescriptionText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    lineHeight: 18,
    letterSpacing: -0.2,
    marginTop: 8
  },

  inputContainer: {
    marginLeft: 15,
    marginRight: 15,
    marginTop: 25,
    backgroundColor: Assets.colors.inputBg,
    borderRadius: 6,
    height: 112
  },

  input: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 10,
    lineHeight: 22,
    paddingVertical: 0
  },

  button: {
    backgroundColor: Assets.colors.buttonColor,
    marginBottom: Device.isIphoneX() ? 20 : 0,
    borderRadius: 0
  }
})

export default RentalRatingDetail
