import React from 'react'
import { View, ScrollView } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import {
  ForceUpdateInfoParam,
  FirebaseValueObject,
  Location,
  PlacePickerParam,
  SelectionListParam,
  UserProfile,
  UpdateKeys,
  PermissionsState,
  PermissionsParam
} from '@Models'
import { NavigationProps } from '@Types'
import { Utils, Colors, AppConfig, DateTime, Device } from '@Utils'
import { Button, BackButton, Section } from '@Components'
import { Header, Text, StyleSheet, TextInput } from 'rn-components'
import { Navigator } from '@Navigation'
import { getStore, UserProfileActions } from '@ReduxManager'
import { Api, FirebaseHelper, StripeHelper } from '@Services'
import Assets from '@Assets'
import Picker from 'react-native-picker'
import Styles from '@Styles'
import DateTimePicker from 'react-native-modal-datetime-picker'
import moment from 'moment'

type State = ForceUpdateInfoParam & {
  sizes: FirebaseValueObject | null
  shoeSizes: FirebaseValueObject | null
  braSizes: string | null
  location: Location | null
  locality: string | null
  backupSizes: FirebaseValueObject | null
  birthdate: string | null
  isDateTimePickerVisible: boolean
  height: string | null
  weight: string | null
  userProfile: UserProfile
  permissions: PermissionsState
  showBirthdate: boolean
  showLocation: boolean
}

type Props = {
  navigation: NavigationProps
}

class ForceUpdateInfo extends React.Component<Props, State> {
  constructor(props) {
    super(props)
    const param: ForceUpdateInfoParam = this.props.navigation.getParam('param')
    const { userProfile, permissions } = getStore().getState()
    const {
      birthday,
      dress_size_id,
      backup_size_id,
      shoe_size_id,
      bra_size,
      locality: _locality,
      latitude,
      longitude,
      height,
      weight
    } = userProfile
    const sizes = Utils.getConfigWithValue('sizes', dress_size_id)
    const backupSizes = Utils.getConfigWithValue('sizes', backup_size_id)
    const shoeSizes = Utils.getConfigWithValue('shoeSizes', shoe_size_id)
    const location = typeof longitude === 'number' && typeof latitude === 'number' ? { longitude, latitude } : null
    const locality = location ? _locality : null

    this.state = {
      isDateTimePickerVisible: false,
      birthdate: birthday ? birthday : null,
      backupSizes,
      sizes,
      braSizes: bra_size ? bra_size : null,
      locality,
      location,
      shoeSizes,
      height,
      weight,
      userProfile,
      permissions,
      showBirthdate: false,
      showLocation: false,
      ...param
    }
  }

  _titleForKeys: { [key in UpdateKeys]: string } = {
    date: 'Birthday',
    braSizes: 'Bra sizes',
    height: 'Height',
    market: 'Choose a location',
    sizes: 'Typical size',
    backupSizes: 'Backup Size (optional)',
    shoeSizes: 'Shoe Size',
    weight: 'Weight'
  }

  _pickerConfig = {
    pickerFontFamily: Assets.fonts.display.regular,
    pickerToolBarFontSize: 17,
    pickerFontSize: 17,
    pickerCancelBtnColor: Colors.hexToRgb('#ffffff'),
    pickerConfirmBtnColor: Colors.hexToRgb('#ffffff'),
    pickerTitleColor: Colors.hexToRgb('#ffffff'),
    pickerConfirmBtnText: 'Done',
    pickerCancelBtnText: 'Cancel',
    pickerToolBarBg: Colors.hexToRgb(Assets.colors.appTheme)
  }

  componentDidMount() {
    this.getAsyncStorageData()
  }

  getAsyncStorageData = async () => {
    try {
      const storedUserBirthday = await AsyncStorage.getItem('USER_BIRTHDAY')
      const storedUserLongitude = await AsyncStorage.getItem('USER_LONGITUDE')
      const storedUserLatitude = await AsyncStorage.getItem('USER_LATITUDE')
      const storedUserLocality = await AsyncStorage.getItem('USER_LOCALITY')

      if (storedUserBirthday !== null && this.state.birthdate === null) {
        this.setState({ birthdate: storedUserBirthday, showBirthdate: false })
      } else {
        this.setState({ showBirthdate: true })
      }
      if (this.state.location === null && storedUserLongitude !== null && storedUserLatitude !== null) {
        this.setState({ showLocation: false, location: { longitude: parseFloat(storedUserLongitude), latitude: parseFloat(storedUserLatitude) }, locality: storedUserLocality })
      } else {
        this.setState({ showLocation: true })
      }

    } catch (e) {
      // error reading value
    }
  }

  _showHeightPicker = () => {
    let selected: string[] = []
    const { height } = this.state
    if (height && height.length > 0) {
      selected = height.split(' ')
    }

    Picker.init({
      pickerData: AppConfig.enumConfig.heights,
      selectedValue: selected,
      pickerTitleText: 'Select Height',
      ...this._pickerConfig,
      onPickerConfirm: data => { },
      onPickerCancel: data => { },
      onPickerSelect: data => {
        this.setState({ height: data.join(' ') })
      }
    })
    Picker.show()
  }

  _showBraSizePicker = () => {
    let selected: string[] = []
    const { braSizes } = this.state
    if (braSizes && braSizes.length > 0) {
      selected = braSizes.split(' ')
    }

    Picker.init({
      pickerData: AppConfig.enumConfig.braSizes,
      selectedValue: selected,
      pickerTitleText: 'Select Bra Size',
      ...this._pickerConfig,
      onPickerConfirm: data => { },
      onPickerCancel: data => { },

      onPickerSelect: data => {
        this.setState({ braSizes: data.join(' ') })
      }
    })
    Picker.show()
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true })

  _hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false }, () => {
      const age = DateTime.calculateAge(this.state.birthdate)

      if (age < 18) {
        Navigator.showAlert(
          'Your parents must approve',
          'Our platform is designed for users age 13 and up and requires parental consent for anyone under 18. Your parents must approve of you using this site to continue.',
          () => null,
          null,
          'Continue'
        )
      }
    })
  }

  _handleDatePicked = birthdate => {
    this.setState({ birthdate })
    this._hideDateTimePicker()
  }

  _renderSelectionView = (title, value, type: UpdateKeys) => {
    let rightIcon = Assets.images.categoryDetailsArrow
    let titleExtensionStyle = null
    let valueExtensionStyle = null
    if (type == 'date') {
      rightIcon = Assets.images.calendar
      if (value !== '') {
        valueExtensionStyle = { color: Assets.colors.appTheme }
      }
    } else {
      if (value !== 'All' && value !== 'Choose a location') {
        valueExtensionStyle = { color: Assets.colors.appTheme }
      }
    }

    const onSelected = () => {
      if (type === 'market') {
        const param: PlacePickerParam = {
          title,
          onLocationSelected: (location, locality) => this.setState({ location, locality })
        }
        Navigator.navTo('PlacePicker', { param })
      } else if (type === 'date') {
        this._showDateTimePicker()
      } else if (type === 'braSizes') {
        this._showBraSizePicker()
      } else if (type === 'height') {
        this._showHeightPicker()
      } else {
        const mutiple = false
        const param: SelectionListParam = {
          onItemsSelected: items => {
            // @ts-ignore
            this.setState({ [type]: mutiple ? items : items[0] })
          },
          mutiple: false,
          title,
          // @ts-ignore
          selectedItems: [this.state[type]],
          // @ts-ignore
          type,
          removedItemTitles: ['All', 'One Size Fits All', 'Other']
        }
        Navigator.navTo('SelectionList', { param })
      }
    }
    return (
      <Section
        title={title}
        titleStyle={titleExtensionStyle}
        valueStyle={valueExtensionStyle}
        value={value}
        iconSource={rightIcon}
        onPress={onSelected}
      />
    )
  }

  _renderSectionHeader(title, marginTop = 20) {
    return (
      <View style={[styles.sectionView, { marginTop: marginTop }]}>
        <Text style={styles.sectionTitle} text={title} />
      </View>
    )
  }

  _validateInputs() {
    const { updateKeys, birthdate, location, height, weight, sizes, shoeSizes, braSizes } = this.state
    if (updateKeys.date && (birthdate === null || birthdate.length === 0)) {
      // Navigator.showToast(
      //   'Error',
      //   'Please select your birthday\nOur platform is designed for users age 13 and up and requires parental consent for anyone under 18. Your parents must approve of you using this site to continue.',
      //   'Error',
      //   10000
      // )
      this.setState({ showBirthdate: true })
      Navigator.showAlert(
        'Select your birthday',
        'You must be at least 13 years old to use the app. If you are under 18 you need your parents permission to use the app.',
        this._showDateTimePicker
      )
      return false
    }

    if (updateKeys.market && location === null) {
      this.setState({ showLocation: true })
      Navigator.showToast('Error', 'Please select your location', 'Error')
      return false
    }

    if (updateKeys.height && (height === null || height.length === 0)) {
      // Navigator.showToast('Error', 'Please select your height', 'Error')
      // return false
    }
    if (updateKeys.weight && (weight === null || weight.length === 0)) {
      // Navigator.showToast('Error', 'Please select your weight', 'Error')
      // return false
    }

    if (updateKeys.sizes && sizes === null) {
      // Navigator.showToast('Error', 'Please select your dress size (typical size).', 'Error')
      // return false
    }
    if (updateKeys.shoeSizes && shoeSizes === null) {
      // Navigator.showToast('Error', 'Please select shoe size', 'Error')
      // return false
    }

    if (updateKeys.braSizes && (braSizes === null || braSizes.length === 0)) {
      // Navigator.showToast('Error', 'Please select your bra size', 'Error')
      // return false
    }
    return true
  }

  _onPressDoneButton = () => {
    if (this._validateInputs() == false) return
    Navigator.showLoading()

    const {
      userProfile,
      backupSizes,
      braSizes,
      shoeSizes,
      weight,
      height,
      locality,
      birthdate,
      location,
      sizes,
      permissions,
      onUpdateDone
    } = this.state

    let profiledata = userProfile
    if (backupSizes) {
      profiledata.backup_size_id = backupSizes.value
    }

    if (braSizes) {
      profiledata.bra_size = braSizes
    }

    if (shoeSizes) {
      profiledata.shoe_size_id = shoeSizes.value
    }

    if (weight) {
      profiledata.weight = weight
    }

    if (height) {
      profiledata.height = height
    }

    if (locality && locality.length) {
      profiledata.locality = locality
    }

    if (birthdate) {
      profiledata.birthday = birthdate
    }

    if (location) {
      profiledata.longitude = location.longitude
      profiledata.latitude = location.latitude
    }

    if (sizes) {
      profiledata.dress_size_id = sizes.value
    }

    let user: UserProfile = null
    Api.editUserProfile(profiledata)
      .then((response: any) => {
        FirebaseHelper.logEvent('ProfileEdited')
        if (response.code !== 200 || !response.user) {
          throw Error(response.message)
        }
        user = response.user

        return Utils.getAddress(user)
      })
      .then(address => {
        const name = Utils.getUserName(user)
        const { payment_customer_id, phone, email } = user
        const body = {
          phone,
          name,
          email,
          metadata: {
            Address: address.address,
            'App Version': Device.getAppVersion(),
            'Code Push Version': Device.getCodePushVersion(),
            Device: Device.getDeviceInfo()
          }
          // address
        }
        console.log('**** updateCustomerPaymentAccount body', body)
        return StripeHelper.updateCustomerPaymentAccount(payment_customer_id, body)
      })
      .then(response => {
        Navigator.hideLoading()
        UserProfileActions.saveUserProfile(user)
        Navigator.back()

        console.log('******* permissions', permissions)
        if (permissions.location) {
          const param: PermissionsParam = {
            permissionType: 'location'
          }
          Navigator.navTo('Permissions', { param })
        } else if (permissions.notification) {
          const param: PermissionsParam = {
            permissionType: 'notification'
          }
          Navigator.navTo('Permissions', { param })
        }

        typeof onUpdateDone === 'function' && onUpdateDone(user)
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  _onBack = () => {
    Navigator.back()
    const { onUpdateDone, userProfile } = this.state
    typeof onUpdateDone === 'function' && onUpdateDone(userProfile)
  }

  render() {
    const { updateKeys, birthdate: _birthdate, locality, sizes, backupSizes, braSizes, shoeSizes, height } = this.state
    //const current = _birthdate ? DateTime.moment(_birthdate, 'MM-DD-YYYY') : DateTime.moment(new Date(), 'MM-DD-YYYY')
    const maxDate = DateTime.moment()
      .subtract(13, 'years')
      .toDate()
    const birthdate = _birthdate ? DateTime.format(_birthdate, 'MMM-DD-YYYY') : ''

    return (
      <View style={StyleSheet.absoluteFill}>
        <Header
          title="Edit Profile"
          titleStyle={Styles.headerTitle}
          // LeftComponent={<BackButton iconSource={Assets.images.closeIcon} onPress={this._onBack} />}
          RightComponent={<Text style={Styles.textHeaderRight} text="Save" onPress={this._onPressDoneButton} />}
        />
        <ScrollView showsVerticalScrollIndicator={false} style={{ marginTop: 20, marginHorizontal: 30 }}>
          <Text style={styles.helpText} text="Create your size profile" />
          <Text
            style={styles.infoText}
            text="This information is optional but will curate better search results for you."
          />
          {updateKeys.date && this.state.showBirthdate && this._renderSelectionView('Birthday', birthdate, 'date')}
          {updateKeys.market && this.state.showLocation && this._renderSelectionView('Location', locality ? locality : '', 'market')}
          {updateKeys.height && this._renderSelectionView('Height', height ? height : '', 'height')}
          {updateKeys.weight && (
            <TextInput
              onChangeText={text => this.setState({ weight: text })}
              style={styles.inputContainer}
              inputStyle={{ paddingLeft: 14, height: 44, marginTop: 2 }}
              placeholder="Weight"
              keyboardType="numeric"
              underlineWidth={0}
              RightComponent={<Text style={styles.codeText} text="lbs" />}
            />
          )}
          {updateKeys.sizes && this._renderSelectionView('Sizes', sizes ? sizes.title : '', 'sizes')}
          {updateKeys.backupSizes &&
            this._renderSelectionView('Backup Size (optional)', backupSizes ? backupSizes.title : '', 'backupSizes')}
          {updateKeys.shoeSizes &&
            this._renderSelectionView('Shoe Size', shoeSizes ? shoeSizes.title : '', 'shoeSizes')}
          {updateKeys.braSizes && this._renderSelectionView('Bra Size', braSizes ? braSizes : '', 'braSizes')}
        </ScrollView>
        <Button
          style={styles.button}
          textStyle={{ fontSize: 18 }}
          text="Save Profile"
          onPress={this._onPressDoneButton}
        />
        <DateTimePicker
          titleIOS={'Select your birthday'}
          date={this.state.birthdate ? DateTime.moment(this.state.birthdate, 'MMM-DD-YYYY').toDate() : maxDate}
          maximumDate={maxDate}
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    marginBottom: 10,
    backgroundColor: 'white',
    borderColor: Assets.colors.borderColorLight,
    borderWidth: 0.5,
    borderRadius: 6,
    height: 44,
    paddingRight: 10
  },

  sectionView: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: 20
  },

  sectionTitle: {
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 18,
    color: Assets.colors.mainText,
    letterSpacing: -0.2
  },

  title: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.4
  },

  value: {
    marginRight: 10,
    textAlign: 'right',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2
  },
  selectionView: {
    marginTop: 10,
    height: 44,
    backgroundColor: '#F4F6F6',
    flexDirection: 'row',
    borderRadius: 6,
    alignItems: 'center'
  },
  codeText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: '#45515470'
  },
  input: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    height: 44,
    marginLeft: 10,
    paddingVertical: 0,
    marginVertical: 0
  },

  loginButtonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 18,
    color: Assets.colors.appTheme
  },
  rightButton: {
    textAlign: 'right',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.appTheme,
    marginTop: 3,
    letterSpacing: -0.4
  },
  joinButtonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    textAlign: 'center',
    color: 'white'
  },
  helpText: {
    marginRight: 60,
    marginTop: 15,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold
  },
  infoText: {
    marginTop: 28,
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    lineHeight: 22,
    marginBottom: 39
  },
  button: {
    marginBottom: Device.isIphoneX() ? 20 : 0,
    borderRadius: 0
  }
})

export default ForceUpdateInfo
