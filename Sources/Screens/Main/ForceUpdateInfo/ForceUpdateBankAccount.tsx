import React from 'react'
import { View, StyleSheet } from 'react-native'
import { ScrollView, Header, Text } from 'rn-components'
import { Button } from '@Components'
import { Device } from '@Utils'
import { Navigator } from '@Navigation'
import Assets from '@Assets'

class ForceUpdateBankAccount extends React.Component {
  _onDone = () => {
    Navigator.navTo('PayoutMethodsInfo', { isForceUpdate: true })
  }

  _onSkip = () => {
    Navigator.pop()
  }

  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header title="" />
        <ScrollView showsVerticalScrollIndicator={false} style={styles.content}>
          <Text style={styles.titleText} text="Get paid, add a bank account" />
          <Text style={styles.infoText} text="To receive money for your rentals you will need to add a bank account." />

          <Button style={styles.button} type="solid" text="Add Payout Method" onPress={this._onDone} />
          <Text containerStyle={styles.skipButton} style={styles.skipButtonText} text="Skip" onPress={this._onSkip} />
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  titleText: {
    marginTop: 25,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    width: '100%'
  },
  infoText: {
    marginTop: 25,
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    width: '100%',
    lineHeight: 22
  },
  content: {
    flex: 1,
    marginHorizontal: 30,
    paddingTop: 20
  },
  button: {
    marginTop: 160 * Device.vs
  },
  skipButton: {
    marginTop: 30 * Device.vs
  },
  skipButtonText: {
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    paddingHorizontal: 20,
    textAlign: 'center'
  }
})

export default ForceUpdateBankAccount
