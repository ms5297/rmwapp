import React from 'react'
import { View, StyleSheet, Keyboard } from 'react-native'
import { connect } from 'react-redux'
import { Header, TextInput, Text, Textarea, InputGroup, ScrollView, Row } from 'rn-components'
import { BackButton, HorizontalListPhotos, Button, Section, TagInput, RightHeaderButton, Switch } from '@Components'
import { Navigator } from '@Navigation'
import { NavigationProps } from '@Types'
import { Api, FirebaseHelper, AppsFlyerHelper, GoogleAnalyticsHelper } from '@Services'
import { StoreState, UserItemActions, getStore, ItemSummaryActions } from '@ReduxManager'
import { Validator, Device, AppConfig, Formater, LayoutAnimations, Utils } from '@Utils'
import {
  UserProfile,
  FirebaseValueObject,
  SelectionListParam,
  PlacePickerParam,
  Location,
  CalendarParam,
  EnumConfigKey,
  Item
} from '@Models'
import Styles from '@Styles'
import Assets from '@Assets'
import debounce from 'lodash.debounce'

type Props = {
  navigation: NavigationProps
  userProfile: UserProfile
}
type State = {
  itemTitle: string
  hasCleaningFee: boolean
  cleaning_fee: string | null
  itemDescription: string
  rentalFee: string | null
  replacement: string | null
  retail: string | null
  availability: Array<string>
  locality: string | null
  text: string | null
  tags: Array<string>
  location: Location | null

  embellishments: Array<FirebaseValueObject> | null
  occasions: Array<FirebaseValueObject> | null
  refunds: FirebaseValueObject | null
  conditions: FirebaseValueObject | null
  cleanings: FirebaseValueObject | null
  sizes: Array<FirebaseValueObject> | null
  styles: FirebaseValueObject | null
  colors: FirebaseValueObject | null
  fitProfiles: FirebaseValueObject | null
  sleeves: FirebaseValueObject | null
  hemlines: FirebaseValueObject | null
  necklines: FirebaseValueObject | null

  autoSmartPricing: boolean
  autoSmartPricingErrorMsg: string
  itemDetails: any
  item: any
  photos: Array<string>

  autoSmartBuyNow: boolean

  multi_week_discount: string
  monthly_discount: string
  placeholderName: string
}
class AddItem extends React.Component<Props, State> {
  _horizontalListPhotos = React.createRef<HorizontalListPhotos>()
  _inputCleaningRef = React.createRef<TextInput>()
  _textAreaRef = React.createRef<Textarea>()
  _priceInputGroup = React.createRef<InputGroup>()
  _scrollViewRef = React.createRef<ScrollView>()
  _retailRef = React.createRef<TextInput>()
  _monthlyDiscountRef = React.createRef<TextInput>()
  _multiWeekDiscountRef = React.createRef<TextInput>()

  constructor(props: Props) {
    super(props)
    const item = props.navigation.getParam('item', null)
    const { userProfile } = props
    const { longitude, latitude, locality = '' } = userProfile

    this.state = {
      itemTitle: '',
      hasCleaningFee: false,
      cleaning_fee: '',
      itemDescription: '',
      sizes: null,
      fitProfiles: null,
      conditions: null,
      cleanings: null,
      refunds: null,
      rentalFee: null,
      replacement: null,
      retail: null,
      colors: null,
      sleeves: null,
      hemlines: null,
      styles: null,
      necklines: null,
      locality,
      text: '',
      location: longitude !== null && latitude !== null ? { longitude, latitude } : null,
      occasions: [],
      availability: [],
      embellishments: [],
      tags: [],
      itemDetails: null,
      item,
      photos: [],
      autoSmartPricing: true,
      autoSmartBuyNow: false,
      placeholderName: 'Replacement Cost [Required]',
      autoSmartPricingErrorMsg: '',
      multi_week_discount: '',
      monthly_discount: ''
    }
    LayoutAnimations.enableAndroidLayoutAnimation()
  }

  componentWillMount() {
    const { locality: defaultLocality, location: defaultLocation } = this.state
    const item = this.props.navigation.getParam('item', null)
    if (item) {
      Navigator.showLoading()
      Api.getItemDetails<any>([item.id])
        .then(response => {
          const { code, message, items } = response
          if (code !== 200 || !Array.isArray(items) || items.length == 0) throw Error(message)

          const itemDetails = items[0]
          const {
            latitude: _latitude,
            longitude: _longitude,
            locality: _locality,
            name: itemTitle,
            description: itemDescription,
            tags: _tags,
            occasions: _occasions,
            embellishments: _embellishments,
            photos: _photos,
            size_id,
            fit_id,
            condition_id,
            cleaning_option_id,
            refund_option_id,
            rental_fee,
            replacement_fee,
            retail_price,
            unavailable_dates,
            color_id,
            sleeve_id,
            hemline_id,
            style_id,
            neckline_id,
            cleaning_fee,
            auto_smart_pricing: autoSmartPricing,
            buy_now: autoSmartBuyNow,
            multi_week_discount: multiWeekDiscount,
            monthly_discount: monthlyDiscount,
            size_ids
          } = itemDetails

          const locality = _locality === null || _locality === '' ? defaultLocality : _locality
          const location =
            _latitude !== null && _longitude !== null ? { longitude: _longitude, latitude: _latitude } : null
          const occasions = Array.isArray(_occasions)
            ? AppConfig.enumConfig.occasions.filter(item => _occasions.includes(item.value))
            : null
          const embellishments = Array.isArray(_embellishments)
            ? AppConfig.enumConfig.embellishments.filter(item => _embellishments.includes(item.value))
            : null
          const photos = Array.isArray(_photos) ? _photos.map(item => item.url) : []
          const tags = _tags.map(el => el.tag)

          let sizes = null

          if (Array.isArray(size_ids) && size_ids.length) {
            sizes = AppConfig.enumConfig.sizes.filter(item => size_ids.includes(item.value))
          } else {
            sizes = [Utils.getConfigWithValue('sizes', size_id)]
          }

          const fitProfiles = Utils.getConfigWithValue('fitProfiles', fit_id)
          const conditions = Utils.getConfigWithValue('conditions', condition_id)
          const cleanings = Utils.getConfigWithValue('cleanings', cleaning_option_id)
          const hasCleaningFee = 'additionalKey' in cleanings
          const availability = unavailable_dates.map(item => item.date)
          const refunds = Utils.getConfigWithValue('refunds', refund_option_id)
          const colors = Utils.getConfigWithValue('colors', color_id)
          const sleeves = Utils.getConfigWithValue('sleeves', sleeve_id)
          const hemlines = Utils.getConfigWithValue('hemlines', hemline_id)
          const styles = Utils.getConfigWithValue('styles', style_id)
          const necklines = Utils.getConfigWithValue('necklines', neckline_id)

          const multi_week_discount =
            multiWeekDiscount > 0 ? Formater.convertToPercent(multiWeekDiscount).replace('%', '') : ''
          const monthly_discount =
            monthlyDiscount > 0 ? Formater.convertToPercent(monthlyDiscount).replace('%', '') : ''

          let sizeIds = []
          if (Array.isArray(size_ids) && size_ids.length) {
            sizeIds = size_ids
          } else {
            if (size_id != null) {
              sizeIds = [size_id]
            }
          }
          this.setState({
            tags,
            itemTitle,
            itemDescription,
            sizes,
            fitProfiles,
            conditions,
            cleanings,
            hasCleaningFee,
            occasions,
            availability,
            refunds,
            rentalFee: rental_fee,
            replacement: replacement_fee,
            retail: retail_price,
            colors,
            sleeves,
            hemlines,
            styles,
            embellishments: embellishments,
            necklines,
            locality,
            location,
            cleaning_fee,
            photos,
            autoSmartPricing,
            autoSmartBuyNow,
            monthly_discount,
            multi_week_discount
          })
          Navigator.hideLoading()
        })
        .catch(error => {
          console.log('***** error', error)
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
        })
    }
  }

  _getFilterParams(keyword: string = '', refresh: boolean = false, currentPageIndex: number = 1) {
    const { userProfile } = this.props
    const { latitude, longitude } = userProfile
    const param = {
      filters: {
        keyword,
        location: latitude !== null && longitude !== null ? { longitude, latitude } : null,
        location_range: 100
      },
      sort: {
        field: 'created_at',
        order: 'desc'
      },
      page: refresh ? 1 : currentPageIndex,
      per_page: 20
    }
    return param
  }

  _validateInputHelper = () => {
    const photos = this._horizontalListPhotos.current.getPhotos()

    if (photos.length == 0) {
      return 'Add at least one photo'
    }

    if (this.state.itemTitle.length == 0) {
      return 'Enter a catchy title'
    }

    if (this.state.itemDescription.length == 0) {
      return 'Enter a description for the item'
    }

    if (this.state.sizes == null) {
      return 'Select the size'
    }

    if (this.state.occasions == null || this.state.occasions.length == 0) {
      return 'Select at least one occasion'
    }

    if (this.state.autoSmartPricingErrorMsg !== '') {
      return this.state.autoSmartPricingErrorMsg
    }

    if (this.state.rentalFee == null) {
      return 'Enter the rental price'
    }

    if (this.state.replacement == null) {
      return 'Enter a replacement price'
    }

    if (this.state.retail == null) {
      return 'Enter the retail price'
    }

    if (Formater.convertToMoney(this.state.rentalFee) > Formater.convertToMoney(this.state.replacement) || Formater.convertToMoney(this.state.rentalFee) == Formater.convertToMoney(this.state.replacement)) {
      if (this.state.autoSmartBuyNow) {
        return 'The rental fee should be less than buying price'
      } else {
        return 'The rental fee should be less than replacement fee'
      }
    }

    if (Formater.convertToMoney(this.state.rentalFee) > Formater.convertToMoney(this.state.retail)) {
      return 'The rental fee should be less than retail price'
    }

    if (this.state.locality == null || this.state.locality.length == 0 || this.state.location == null) {
      return 'Select the location for the listing'
    }

    if (this.state.cleanings == null) {
      return 'Select a cleaning preference'
    }

    if (this.state.hasCleaningFee && Formater.convertToMoney(this.state.cleaning_fee) > 0) {
      let maxCleaning = Formater.convertToMoney(this.state.rentalFee) * 0.2
      if (maxCleaning < 8) {
        maxCleaning = 8
      }

      const cleaningFee = Formater.convertToMoney(this.state.cleaning_fee)
      if (cleaningFee > maxCleaning) {
        const message = 'Cleaning fee cannot be more than $' + maxCleaning
        this.setState({ cleaning_fee: `${maxCleaning}` })
        return message
      }
    }

    // NOTE: Remove refund option
    // if (this.state.refunds == null) {
    //   return "Select a refund option";
    // }

    return null
  }

  _validateInput() {
    const errorMessage = this._validateInputHelper()
    if (errorMessage) {
      Navigator.showToast('Error', errorMessage, 'Error')
      return false
    }
    return true
  }

  _getOptionalValue(key) {
    let object = this.state[key]
    if (object) {
      return object.value
    }
    return null
  }

  _preparePayload = (itemId = null) => {
    const photos = this._horizontalListPhotos.current.getPhotos()
    const {
      occasions,
      embellishments,
      tags,
      itemTitle,
      itemDescription,
      sizes,
      availability,
      locality,
      location,
      cleanings,
      rentalFee,
      replacement,
      refunds,
      retail,
      fitProfiles,
      cleaning_fee,
      colors,
      conditions,
      hasCleaningFee,
      hemlines,
      necklines,
      styles,
      sleeves,
      autoSmartPricing,
      autoSmartBuyNow,
      multi_week_discount,
      monthly_discount
    } = this.state

    let payload = {
      tags,
      photos,
      name: itemTitle,
      description: itemDescription,
      size_ids: sizes.map(el => el.value),
      unavailable_dates: availability,
      occasions: occasions.map(el => el.value),
      embellishments: embellishments.map(el => el.value),
      locality,
      latitude: location ? location.latitude : null,
      longitude: location ? location.longitude : null,
      rental_fee: rentalFee,
      replacement_fee: replacement,
      retail_price: retail,
      cleaning_option_id: cleanings.value,
      cleaning_fee: hasCleaningFee ? cleaning_fee : 0,
      // NOTE: Remove refund option
      // refund_option_id: refunds.value,
      refund_option_id: 1,
      fit_id: fitProfiles ? fitProfiles.value : null,
      condition_id: conditions ? conditions.value : null,
      color_id: colors ? colors.value : null,
      sleeve_id: sleeves ? sleeves.value : null,
      hemline_id: hemlines ? hemlines.value : null,
      style_id: styles ? styles.value : null,
      neckline_id: necklines ? necklines.value : null,
      auto_smart_pricing: autoSmartPricing,
      buy_now: autoSmartBuyNow,
      monthly_discount: monthly_discount !== '' ? parseFloat(monthly_discount) / 100 : 0,
      multi_week_discount: multi_week_discount !== '' ? parseFloat(multi_week_discount) / 100 : 0
    }

    console.log('**** payload: ', payload)

    if (itemId !== null) {
      payload['id'] = itemId
    }
    return payload
  }

  _editItem = itemId => {
    Navigator.showLoading()
    this._uploadPhotos(urls => {
      let item = this._preparePayload(itemId)
      console.log('_preparePayload', item)
      item['photos'] = urls
      Api.updateItems<any>(item)
        .then(response => {
          if (response.code !== 200) {
            throw Error(response.message || '(AI - 381) Internal Error')
          }
          FirebaseHelper.logEvent('ItemEdited', { id: itemId })
          return Api.getItemDetails([itemId])
        })
        .then((response: any) => {
          const { code, message, items } = response
          if (code !== 200 || !Array.isArray(items) || items.length == 0)
            throw Error(message || '(AI - 421) Internal Error')

          const itemDetails = items[0]

          return itemDetails
        })
        .then(item => {
          UserItemActions.editItem(item)
          Navigator.hideLoading()
          Navigator.showToast(
            'Success',
            'Item has been updated successfully',
            'Success',
            3000,
            () => null,
            () => {
              Navigator.back()
            },
            true
          )
        })
        .catch(error => {
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
        })
    })
  }

  _addItem = () => {
    Navigator.showLoading()
    this._uploadPhotos(urls => {
      let item = this._preparePayload()
      // if (item.buy_now && item.replacement_fee > item.rental_fee) {

      // }
      item['photos'] = urls
      Api.addItems<any>(item)
        .then(response => {
          if (response.code !== 200) {
            throw Error(response.message || '(AI - 421) Internal Error.')
          }

          const { payment_account_id } = this.props.userProfile
          UserItemActions.saveItem(response.item)
          FirebaseHelper.logEvent('ItemAdded', { id: response.item })
          AppsFlyerHelper.trackEvent(AppsFlyerHelper.EVENTS.RENTAL_ITEM_CREATED, this.props.userProfile.id, response.item)
          GoogleAnalyticsHelper.trackEvent(GoogleAnalyticsHelper.EVENT_CATEGORY.MAIN, GoogleAnalyticsHelper.EVENT.RENTAL_ITEM_CREATED, { item_id: response.item })

          if (payment_account_id == null || payment_account_id.length == 0) {
            let { filters } = getStore().getState()
            Api.getItemSummary(filters.categories)
              .then((response: any) => {
                const itemsSummary = response && response.code === 200 ? response.categories : []

                ItemSummaryActions.saveItemSummary(itemsSummary)

                Navigator.hideLoading()
                Navigator.navTo('PayoutReminder')
              })
              .catch((error: any) => {
                Navigator.hideLoading()
                Navigator.navTo('PayoutReminder')
              })
          } else {
            let { filters } = getStore().getState()
            Api.getItemSummary(filters.categories)
              .then((response: any) => {
                const itemsSummary = response && response.code === 200 ? response.categories : []

                ItemSummaryActions.saveItemSummary(itemsSummary)

                Navigator.hideLoading()
                Navigator.back()
              })
              .catch((error: any) => {
                Navigator.hideLoading()
                Navigator.back()
              })
          }
        })
        .catch(error => {
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
        })
    })
  }

  _onChangeDescription = description => {
    this.setState({ itemDescription: description })
  }

  _onToggleSmartPricing = autoSmartPricing => {
    LayoutAnimations.setLayoutAnimation(LayoutAnimations.PresetLinear)
    this.setState({ autoSmartPricing })
    this._handleSmartPricing(this.state.retail)
  }
  _onToggleSmartBuyNow = autoSmartBuyNow => {
    LayoutAnimations.setLayoutAnimation(LayoutAnimations.PresetLinear)
    this.setState({ autoSmartBuyNow })
    if (this.state.autoSmartBuyNow) {
      this.setState({
        placeholderName: 'Replacement Cost [Required]'
      })
    } else {
      this.setState({
        placeholderName: 'Buying Cost [Required]'
      })
    }
    //this._handleSmartPricing(this.state.retail)
  }

  _onInputGroupSubmit = () => this._inputCleaningRef.current.focus()

  _handleMultiWeekDiscount = debounce(text => { })

  _handleMonthlyDiscount = debounce(text => { })

  _handleSmartPricing = debounce(price => {
    const { rentalPrice, replacementPrice, retail } = Utils.getRentalAndReplacementCost(price)
    let autoSmartPricingErrorMsg = ''
    if (this.state.autoSmartPricing) {
      if (retail < 40) {
        autoSmartPricingErrorMsg = 'Please add a price greater than or equal to $40 for smart pricing.'
      } else if (retail > 10000) {
        autoSmartPricingErrorMsg = 'Please add a price lower than or equal to $10,000 for smart pricing.'
      }
      this.setState({
        retail: price,
        replacement: replacementPrice === 0 ? null : `${replacementPrice}`,
        rentalFee: rentalPrice === 0 ? null : `${rentalPrice}`,
        autoSmartPricingErrorMsg
      })
    } else {
      this.setState({
        retail: price,
        autoSmartPricingErrorMsg,
        replacement: null,
        rentalFee: null
      })
    }

    console.log('rentalPrice, replacementPrice ', rentalPrice, replacementPrice)
  }, 1000)

  _onRetailPriceChanged = (price: string) => {
    this._handleSmartPricing(price)
  }

  _renderPriceSection() {
    const {
      rentalFee,
      replacement,
      retail,
      hasCleaningFee,
      cleaning_fee,
      cleanings,
      refunds,
      autoSmartPricing,
      autoSmartBuyNow,
      placeholderName,
      autoSmartPricingErrorMsg,
      multi_week_discount,
      monthly_discount
    } = this.state

    const rentalFeeBorderColor = rentalFee === null ? Assets.colors.appTheme : Assets.colors.borderColorLight
    const replacementBorderColor = replacement === null ? Assets.colors.appTheme : Assets.colors.borderColorLight
    const retailBorderColor = retail === null ? Assets.colors.appTheme : Assets.colors.borderColorLight

    const rentalDefaultText = Formater.convertToMoneyText(rentalFee)
    const replacementDefaultText = Formater.convertToMoneyText(replacement)
    const retailDefaultText = Formater.convertToMoneyText(retail)
    const cleaningDefaultText = Formater.convertToMoneyText(cleaning_fee)

    const multiWeekDiscount = Formater.convertStringToPercent(multi_week_discount)
    const monthlyDiscount = Formater.convertStringToPercent(monthly_discount)

    return (
      <View>
        {this._renderSectionHeader('Prices & Fees', 20, 0)}
        <Row style={{ marginBottom: 4 }} alignHorizontal="space-between" alignVertical="center">
          <Text style={styles.title} text="Automatic Pricing" />
          <Switch value={autoSmartPricing} onValueChange={this._onToggleSmartPricing} />
        </Row>
        <Text
          style={styles.helper}
          text="Boost rentals and use smart pricing to populate the recommended rental rate. Simply enter the retail price and the rest is taken care of."
        />
        {typeof autoSmartPricingErrorMsg == 'string' && autoSmartPricingErrorMsg !== '' && (
          <Text style={styles.smartPricingError} text={autoSmartPricingErrorMsg} />
        )}
        <TextInput
          ref={this._retailRef}
          onChangeText={this._onRetailPriceChanged}
          defaultValue={retailDefaultText}
          keyboardType="decimal-pad"
          style={[styles.inputContainer, { borderColor: retailBorderColor }]}
          inputStyle={[styles.input, { marginLeft: 5 }]}
          placeholderTextColor={Assets.colors.placeholder}
          selectionColor={Assets.colors.mainText}
          autoCapitalize="none"
          placeholder="Retail Price [Required]"
          LeftComponent={<Text style={styles.prefixText} text="$" />}
          underlineWidth={0}
          underlineColor="transparent"
          returnKeyType="next"
          inputAccessoryViewID="_retailFee"
          InputAccessoryComponent={
            <Text
              containerStyle={Styles.nextAccessoryContainer}
              style={Styles.nextAccessoryText}
              text="Next"
              onPress={() => {
                if (autoSmartPricing) {
                  Keyboard.dismiss()
                } else {
                  if (this._priceInputGroup.current) {
                    this._priceInputGroup.current.focus(0)
                  }
                }
              }}
            />
          }
        />

        {autoSmartPricing == false && (
          <View>
            <View
              style={{
                height: 1,
                backgroundColor: Assets.colors.borderColorDark,
                marginTop: 6,
                marginBottom: 10
              }}
            />
            <InputGroup ref={this._priceInputGroup} scrollViewRef={this._scrollViewRef}>
              {/* <TextInput
                onChangeText={text => this.setState({ replacement: text })}
                defaultValue={replacementDefaultText}
                style={[styles.inputContainer, { borderColor: replacementBorderColor }]}
                inputStyle={[styles.input, { marginLeft: 5 }]}
                placeholderTextColor={Assets.colors.placeholder}
                keyboardType="decimal-pad"
                autoCapitalize="words"
                placeholder="Replacement Cost [Required]"
                LeftComponent={<Text style={styles.prefixText} text="$" />}
                underlineWidth={0}
                underlineColor="transparent"
                returnKeyType="next"
                inputAccessoryViewID="_replacementFee"
                InputAccessoryComponent={
                  <Text
                    containerStyle={Styles.nextAccessoryContainer}
                    style={Styles.nextAccessoryText}
                    text="Next"
                    onPress={() => {
                      this._priceInputGroup.current.focus(1)
                    }}
                  />
                }
              /> */}
              <TextInput
                onChangeText={text => this.setState({ rentalFee: text })}
                defaultValue={rentalDefaultText}
                style={[styles.inputContainer, { borderColor: rentalFeeBorderColor }]}
                inputStyle={[styles.input, { marginLeft: 5 }]}
                placeholderTextColor={Assets.colors.placeholder}
                keyboardType="decimal-pad"
                autoCapitalize="words"
                placeholder="Rental Fee (1 Week) [Required]"
                LeftComponent={<Text style={styles.prefixText} text="$" />}
                underlineWidth={0}
                underlineColor="transparent"
                returnKeyType="next"
                inputAccessoryViewID="_rentalFee"
                InputAccessoryComponent={
                  <Text
                    containerStyle={Styles.nextAccessoryContainer}
                    style={Styles.nextAccessoryText}
                    text="Next"
                    onPress={() => {
                      Keyboard.dismiss()
                    }}
                  />
                }
              />
            </InputGroup>
          </View>
        )}
        {autoSmartPricing == true && this.state.rentalFee && (
          <View>
            <Row>
            <Text
              style={styles.helper}
              text="Calculated rental fee (1 week): $"
            />
            <Text
              style={styles.helper}
              text={this.state.rentalFee}
            />
            </Row>
          </View>
        )}
        <Row style={{ marginBottom: 4 }} alignHorizontal="space-between" alignVertical="center">
          <Text style={styles.title} text="Buy Now" />
          <Switch value={autoSmartBuyNow} onValueChange={this._onToggleSmartBuyNow} />
        </Row>
        <Text
          style={styles.helper}
          text="Turn on Buy Now to provide returns with the opportunity to purchase your item during their rental."
        />
        {autoSmartBuyNow && (<Text
          style={styles.helper}
          text="Rent My Wardrobe does not withdraw a service fee from your Buy Now Earnings."
        />)}
        

        {/* <View>
          <View
              style={{
                height: 1,
                backgroundColor: Assets.colors.borderColorDark,
                marginTop: 6,
                marginBottom: 10
              }}
            /> */}
        <TextInput

          onChangeText={text => this.setState({ replacement: text })}
          defaultValue={replacementDefaultText}
          style={[styles.inputContainer, { borderColor: replacementBorderColor }]}
          inputStyle={[styles.input, { marginLeft: 5 }]}
          placeholderTextColor={Assets.colors.placeholder}
          keyboardType="decimal-pad"
          autoCapitalize="words"
          placeholder={this.state.placeholderName}
          LeftComponent={<Text style={styles.prefixText} text="$" />}
          underlineWidth={0}
          underlineColor="transparent"
          returnKeyType="next"
          inputAccessoryViewID="_replacementFee"
          InputAccessoryComponent={
            <Text
              containerStyle={Styles.nextAccessoryContainer}
              style={Styles.nextAccessoryText}
              text="Next"
              onPress={() => {
                // this._priceInputGroup.current.focus(1)
                Keyboard.dismiss()
              }}
            />
          }
        />
        {/* </View> */}

        <Text style={[styles.title, { marginTop: 20 }]} text="Length-of-rental Discount" />
        <Text
          style={[styles.title, { marginTop: 8, marginBottom: 10, fontSize: 13 }]}
          text="Encourage renters to rent longer by offering a discount for a week or more."
        />
        <TextInput
          ref={this._multiWeekDiscountRef}
          defaultValue={multiWeekDiscount}
          onChangeText={text => {
            this.setState({ multi_week_discount: text })
          }}
          keyboardType="decimal-pad"
          maxLength={5}
          // value={multiWeekDiscount}
          style={styles.inputContainer}
          inputStyle={[styles.input, { marginLeft: 5 }]}
          placeholderTextColor={Assets.colors.placeholder}
          selectionColor={Assets.colors.mainText}
          autoCapitalize="none"
          placeholder="Multi-week"
          RightComponent={<Text style={[styles.prefixText, { marginRight: 10 }]} text="2 weeks or more" />}
          underlineWidth={0}
          underlineColor="transparent"
          inputAccessoryViewID="_multiWeekDiscount"
          InputAccessoryComponent={
            <Text
              containerStyle={Styles.nextAccessoryContainer}
              style={Styles.nextAccessoryText}
              text="Done"
              onPress={() => Keyboard.dismiss()}
            />
          }
        />
        <Text
          style={[styles.title, { fontSize: 14, color: Assets.colors.textLight, marginBottom: 12 }]}
          text="Suggested: 15%"
        />
        <TextInput
          ref={this._monthlyDiscountRef}
          // value={monthlyDiscount}
          defaultValue={monthlyDiscount}
          onChangeText={text => this.setState({ monthly_discount: text })}
          keyboardType="decimal-pad"
          style={styles.inputContainer}
          inputStyle={[styles.input, { marginLeft: 5 }]}
          placeholderTextColor={Assets.colors.placeholder}
          selectionColor={Assets.colors.mainText}
          maxLength={5}
          autoCapitalize="none"
          placeholder="Monthly"
          RightComponent={<Text style={[styles.prefixText, { marginRight: 10 }]} text="4 weeks or more" />}
          underlineWidth={0}
          underlineColor="transparent"
          inputAccessoryViewID="_monthlyDiscount"
          InputAccessoryComponent={
            <Text
              containerStyle={Styles.nextAccessoryContainer}
              style={Styles.nextAccessoryText}
              text="Done"
              onPress={() => Keyboard.dismiss()}
            />
          }
        />
        <Text
          style={[styles.title, { fontSize: 14, color: Assets.colors.textLight, marginBottom: 20 }]}
          text="Suggested: 25%"
        />

        {this._renderSelectionView('Cleaning Preference', cleanings ? cleanings.title : '', 'cleanings', true)}
        {hasCleaningFee && (
          <TextInput
            ref={this._inputCleaningRef}
            onChangeText={text => this.setState({ cleaning_fee: text })}
            defaultValue={cleaningDefaultText}
            keyboardType="decimal-pad"
            style={[styles.inputContainer, { borderColor: retailBorderColor }]}
            inputStyle={[styles.input, { marginLeft: 5 }]}
            placeholderTextColor={Assets.colors.placeholder}
            selectionColor={Assets.colors.mainText}
            autoCapitalize="none"
            placeholder="Cleaning Fee"
            LeftComponent={<Text style={styles.prefixText} text="$" />}
            underlineWidth={0}
            underlineColor="transparent"
            inputAccessoryViewID="_cleaningFee"
            InputAccessoryComponent={
              <Text
                containerStyle={Styles.nextAccessoryContainer}
                style={Styles.nextAccessoryText}
                text="Done"
                onPress={() => Keyboard.dismiss()}
              />
            }
          />
        )}

        {/* {this._renderSelectionView("Refund Option", refunds ? refunds.title : "", "refunds", true)} */}
      </View>
    )
  }

  _onTagsSelected = tags => this.setState({ tags })

  _renderTagsSection() {
    console.log('this.state.tags', this.state.tags)
    return (
      <View>
        {this._renderSectionHeader('#Tags')}
        <View style={[styles.inputContainer, { height: 50 }]}>
          <TagInput
            horizontalTags={this.state.tags}
            style={[styles.inputContainer, { height: 50 }]}
            onTagsSelected={this._onTagsSelected}
            onSubmitEditing={() => {
              if (this._retailRef.current) {
                this._retailRef.current.focus()
              }
            }}
          />
        </View>
      </View>
    )
  }

  _renderOtherSection = () => {
    const { colors, fitProfiles, conditions, sleeves, hemlines, styles, embellishments, necklines } = this.state
    const totalEmbellishments = embellishments.length
    const embellishmentsText = totalEmbellishments
      ? totalEmbellishments === 1
        ? embellishments[0].title
        : `${totalEmbellishments} items`
      : ''
    return (
      <View>
        {this._renderSectionHeader('Further Details')}
        {this._renderSelectionView('Color', colors ? colors.title : '', 'colors')}
        {this._renderSelectionView('Fit', fitProfiles ? fitProfiles.title : '', 'fitProfiles')}
        {this._renderSelectionView('Condition', conditions ? conditions.title : '', 'conditions')}
        {this._renderSelectionView('Sleeve', sleeves ? sleeves.title : '', 'sleeves')}
        {this._renderSelectionView('Hemline', hemlines ? hemlines.title : '', 'hemlines')}
        {this._renderSelectionView('Style', styles ? styles.title : '', 'styles')}
        {this._renderSelectionView('Embellishments', embellishmentsText, 'embellishments')}
        {this._renderSelectionView('Neckline', necklines ? necklines.title : '', 'necklines')}
      </View>
    )
  }

  _renderTitleSection = () => {
    const { itemDescription, itemTitle, sizes, availability, occasions, locality } = this.state

    const descriptionBorderColor = itemDescription.length ? Assets.colors.borderColorLight : Assets.colors.appTheme
    const titleBorderColor = itemTitle.length ? Assets.colors.borderColorLight : Assets.colors.appTheme
    const totalBlockedDates = availability ? availability.length : 0
    const totalOccasion = occasions ? occasions.length : 0
    const dateString = totalBlockedDates ? `${totalBlockedDates} dates are blocked` : ''
    const occasionString = totalOccasion ? (totalOccasion === 1 ? occasions[0].title : `${totalOccasion} items`) : ''

    const totalSize = sizes ? sizes.length : 0
    const sizesString = totalSize ? (totalSize === 1 ? sizes[0].title : `${totalSize} items`) : ''
    return (
      <View>
        {this._renderSectionHeader('Name & Description')}
        <TextInput
          onChangeText={text => this.setState({ itemTitle: text })}
          onSubmitEditing={() => this._textAreaRef.current.focus()}
          defaultValue={itemTitle}
          inputStyle={styles.input}
          style={[styles.inputContainer, { borderColor: titleBorderColor }]}
          placeholderTextColor={Assets.colors.placeholder}
          selectionColor={Assets.colors.mainText}
          autoCapitalize="words"
          placeholder="Title [Required]"
          underlineColor="transparent"
          underlineWidth={0}
          returnKeyType="next"
        />

        <Textarea
          ref={this._textAreaRef}
          onChangeText={this._onChangeDescription}
          defaultValue={itemDescription}
          style={[styles.inputContainer, { borderColor: descriptionBorderColor, height: 85 }]}
          inputStyle={[styles.input, { height: 85 }]}
          selectionColor={Assets.colors.mainText}
          placeholderTextColor={Assets.colors.placeholder}
          placeholder="Description [Required]"
          returnKeyType="next"
          inputAccessoryViewID="_textArea"
          InputAccessoryComponent={
            <Text
              containerStyle={Styles.nextAccessoryContainer}
              style={Styles.nextAccessoryText}
              text="Done"
              onPress={() => Keyboard.dismiss()}
            />
          }
        />

        {this._renderSelectionView('Size', sizesString, 'sizes', true)}
        {this._renderSelectionView('Availability', dateString, 'date')}
        {this._renderSelectionView('Occasion', occasionString, 'occasions', true)}
        {this._renderSelectionView('Location', locality ? locality : '', 'market')}
      </View>
    )
  }

  _uploadPhotos(callback: (urls: Array<string>) => void) {
    const photos = this._horizontalListPhotos.current.getPhotos()
    let promises: any = []
    let httpUrls: string[] = []
    photos.forEach(photo => {
      if (photo.length > 0) {
        if (Validator.isURL(photo) == false) {
          promises.push(FirebaseHelper.uploadItemPhoto(photo))
        } else {
          httpUrls.push(photo)
        }
      }
    })
    Promise.all(promises)
      .then((urls: any) => {
        const newUrls = [...urls, ...httpUrls]
        callback(newUrls)
      })
      .catch(error => {
        callback(photos)
      })
  }

  _renderSelectionView = (title: string, value: string, type: EnumConfigKey, required = false) => {
    let rightIcon = Assets.images.categoryDetailsArrow
    let titleExtensionStyle = null
    let valueExtensionStyle = null
    let borderColor = type in AppConfig.fieldType ? Assets.colors.inputBg : Assets.colors.borderColorLight
    let _value = value
    if (type == 'date') {
      rightIcon = Assets.images.calendar
      if (value !== '') {
        titleExtensionStyle = { color: Assets.colors.appTheme }
        title = value
        _value = ''
      }
    } else {
      if (value !== 'All' && value !== 'Choose a location') {
        if (value.length === 0) {
          _value = required ? 'Required' : value
        } else {
          _value = value
          valueExtensionStyle = { color: Assets.colors.appTheme }
          if (type === 'cleanings') {
            const hasCleaningFee = 'additionalKey' in this.state.cleanings
            if (this.state.hasCleaningFee !== hasCleaningFee) {
              LayoutAnimations.setLayoutAnimation(LayoutAnimations.Dropdown)
              this.setState({ hasCleaningFee })
            }
          }
        }
      }
    }

    if (required && value.length == 0) {
      borderColor = Assets.colors.appTheme
    }
    const onSelected = () => {
      if (type === 'market') {
        const param: PlacePickerParam = {
          title,
          onLocationSelected: (location, locality) => this.setState({ location, locality })
        }
        Navigator.navTo('PlacePicker', { param })
      } else if (type === 'date') {
        const param: CalendarParam = {
          mode: 'blocking',
          onDateSelected: availability => {
            this.setState({ availability })
          },
          blockedDates: this.state.availability
        }
        Navigator.navTo('AvailabilityCalendar', { param })
      } else {
        const mutiple = type === 'embellishments' || type === 'occasions' || type === 'sizes'
        const param: SelectionListParam = {
          onItemsSelected: items => {
            // @ts-ignore
            this.setState({ [type]: mutiple ? items : items[0] })
          },
          mutiple,
          title: title,
          selectedItems: mutiple ? this.state[type] : [this.state[type]],
          type,
          removedItemTitles: ['All']
        }
        Navigator.navTo('SelectionList', { param })
      }
    }
    return (
      <Section
        titleContainerStyle={{ width: '50%' }}
        rightContainerStyle={{ width: '50%', paddingRight: 10 }}
        style={{ borderColor, borderWidth: 1 }}
        title={title}
        titleStyle={[{ flex: 1 }, titleExtensionStyle]}
        valueStyle={[{ flex: 1 }, valueExtensionStyle]}
        value={_value}
        iconSource={rightIcon}
        onPress={onSelected}
      />
    )
  }

  _renderSectionHeader = (title: string, marginTop = 20, marginBottom = 10) => {
    return (
      <View style={[styles.sectionView, { marginTop, marginBottom }]}>
        <Text style={styles.sectionTitle} text={title} />
      </View>
    )
  }

  _deleteClosetItem = () => {
    const { params } = this.props.navigation.state
    Navigator.showAlert(
      'Delete item',
      'Delete this item? This cannot be undone.',
      () => null,
      () => {
        Navigator.showLoading()
        Api.deleteItems<any>(params.item.id)
          .then(response => {
            if (response.code !== 200) {
              throw Error(response.message)
            }
            FirebaseHelper.logEvent('ItemDeleted', { id: params.item.id })
            UserItemActions.deleteUserItem(params.item.id)

            Navigator.showToast(
              'Success',
              'Item has been deleted successfully',
              'Success',
              3000,
              () => null,
              () => {
                let { filters } = getStore().getState()
                Api.getItemSummary(filters.categories)
                  .then((response: any) => {
                    const itemsSummary = response && response.code === 200 ? response.categories : []

                    ItemSummaryActions.saveItemSummary(itemsSummary)

                    Navigator.hideLoading()
                    Navigator.back()
                  })
                  .catch((error: any) => {
                    Navigator.hideLoading()
                    Navigator.back()
                  })
              },
              true
            )
          })
          .catch(error => {
            Navigator.hideLoading()
            Navigator.showToast('Error', error.message, 'Error')
          })
      },
      'CANCEL',
      'OK'
    )
  }

  _onSave = () => {
    const { item } = this.state
    if (this._validateInput()) {
      if (item) {
        this._editItem(item.id)
      } else {
        this._addItem()
      }
    }
  }

  render() {
    const { item, photos } = this.state
    const isEditing = item !== null
    const title = isEditing ? 'Edit Item' : 'New Item'
    const marginBottom = Device.isIphoneX() ? 20 : 0
    const buttonText = isEditing ? 'Save Changes' : 'List Item'

    return (
      <View style={StyleSheet.absoluteFill}>
        <Header
          title={title}
          titleStyle={Styles.headerTitle}
          LeftComponent={<BackButton />}
          RightComponent={
            isEditing && (
              <RightHeaderButton
                size={16}
                iconSource={Assets.images.trash}
                onPress={this._deleteClosetItem}
                iconStyle={{
                  tintColor: Assets.colors.appTheme,
                  height: 20,
                  width: 20
                }}
              />
            )
          }
        />
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={styles.content}
          contentContainerStyle={{ paddingBottom: 30 }}
          keyboardShouldPersistTaps="handled"
          autoScrollNextInput
          ref={this._scrollViewRef}>
          <HorizontalListPhotos ref={this._horizontalListPhotos} photos={photos} />
          {this._renderTitleSection()}
          {this._renderTagsSection()}
          {this._renderPriceSection()}
          {this._renderOtherSection()}
        </ScrollView>
        <Button
          style={{ borderRadius: 0, marginBottom }}
          textStyle={{ fontSize: 18 }}
          text={buttonText}
          onPress={this._onSave}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    marginHorizontal: 20,
    paddingTop: 20
  },

  prefixText: {
    color: Assets.colors.placeholder,
    fontFamily: Assets.fonts.text.regular,
    fontSize: 15,
    marginLeft: 5
  },

  inputContainer: {
    marginBottom: 10,
    backgroundColor: 'white',
    borderColor: Assets.colors.borderColorLight,
    borderWidth: 0.5,
    borderRadius: 6,
    height: 44
  },

  input: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    height: 44,
    marginLeft: 10,
    width: '100%',
    marginTop: 2
  },

  selectionView: {
    marginLeft: 10,
    marginTop: 10,
    height: 44,
    backgroundColor: '#F4F6F6',
    borderColor: Assets.colors.appTheme,
    flexDirection: 'row',
    borderRadius: 6,
    alignItems: 'center'
  },
  sectionView: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: 10
  },

  sectionTitle: {
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 18,
    color: Assets.colors.mainText,
    letterSpacing: -0.2
  },
  title: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.4
  },
  helper: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.4,
    marginBottom: 10
  },
  smartPricingError: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: '#e62117',
    letterSpacing: -0.4,
    marginBottom: 10
  },
  value: {
    marginRight: 10,
    textAlign: 'right',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2
  }
})

const mapStateToProps = (state: StoreState) => {
  return {
    userProfile: state.userProfile
  }
}

export default connect(mapStateToProps)(AddItem)
