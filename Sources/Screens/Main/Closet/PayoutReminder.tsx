import React from 'react'
import { View, StyleSheet } from 'react-native'
import { ScrollView, Header, Text } from 'rn-components'
import { Button } from '@Components'
import { Navigator } from '@Navigation'
import Assets from '@Assets'

class PayoutReminder extends React.Component {
  _onDone = () => {
    Navigator.pop(2, false)
    Navigator.navTo('PayoutMethodsInfo', {})
  }

  _onSkip = () => {
    Navigator.back()
    Navigator.back()
  }

  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header title="" />
        <ScrollView showsVerticalScrollIndicator={false} style={styles.content}>
          <Text style={styles.titleText} text="Thanks for listing your item" />
          <Text style={styles.subTitleText} text="Get paid, add a bank account" />
          <Text
            style={styles.infoText}
            text="To receieve money for your rentals you will need to add a bank account."
          />

          <Button style={{ marginTop: 73 }} type="solid" text="Add Payout Method" onPress={this._onDone} />
          <Text onPress={this._onSkip} style={styles.skipButtonText} text="Skip now, Go to My Closet" />
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  titleText: {
    marginTop: 25,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    width: '100%'
  },
  subTitleText: {
    marginTop: 25,
    fontSize: 24,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.display.bold,
    width: '100%'
  },
  infoText: {
    marginTop: 15,
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    width: '100%',
    lineHeight: 22
  },
  content: {
    flex: 1,
    marginHorizontal: 30,
    paddingTop: 20
  },
  skipButtonText: {
    marginTop: 30,
    alignSelf: 'center',
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14
  }
})

export default PayoutReminder
