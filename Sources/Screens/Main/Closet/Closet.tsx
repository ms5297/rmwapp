import React from 'react'
import { View } from 'react-native'
import { Header, StyleSheet, Text } from 'rn-components'
import { connect } from 'react-redux'
import { ItemCell } from '@Components'
import { Navigator } from '@Navigation'
import { StoreState, UserItemActions, getStore, FilterActions } from '@ReduxManager'
import { UserItems, UserProfile, Item } from '@Models'
import { Api } from '@Services'
import { Constants } from '@Utils'
import Styles from '@Styles'
import CategoryGrid from '../Home/Category/CategoryGrid'

type Props = {
  userItems: UserItems
  userProfile: UserProfile
}

class Closet extends React.Component<Props> {

  componentDidMount() {

  }

  _onRefresh = () => {
    return new Promise<boolean>((resolve, reject) => {
      let param = getStore().getState().filters.userItems
      param.page = 1
      Api.getItemByUser(param)
        .then((response: any) => {
          const userItems = response && response.code === 200 ? response.items : []
          if (userItems.length) {
            param.page = 2
          }
          FilterActions.updateUserItemsFilterParam(param)
          UserItemActions.saveUserItems(userItems)

          resolve(true)
        })
        .catch(error => {
          resolve(false)
        })
    })
  }

  _onLoadMore = () => {
    return new Promise<boolean>((resolve, reject) => {
      let param = getStore().getState().filters.userItems

      Api.getItemByUser(param)
        .then((response: any) => {
          const userItems = response && response.code === 200 ? response.items : []
          if (userItems.length > 0) {
            param.page += 1
          }
          FilterActions.updateUserItemsFilterParam(param)
          UserItemActions.appendUserItems(userItems)

          resolve(true)
        })
        .catch(error => {
          resolve(false)
        })
    })
  }

  _triggerEditItem = (item: Item) => {
    if (item.editable) {
      Navigator.navTo('AddItem', { item })
    } else {
      Navigator.showToast(
        'Error',
        'There is a rental in progress, you can not edit this item until the rental is canceled or complete.',
        'Error'
      )
    }
  }

  _triggerAddItem = () => {
    const { userProfile } = this.props
    if (userProfile.accessibility === false) {
      Navigator.navTo('ComingSoon', { fromCloset: true })
    } else {
      Navigator.navTo('AddItem')
    }

  }

  _keyExtractor = (item: any, index: number) => item.id + index.toString()

  _renderListItem = ({ item, index }: { item: Item; index: number }) => {
    const marginLeft = index % 2 === 0 ? 15 : 13
    return (
      <ItemCell
        style={{ marginLeft }}
        editable
        ownerNameVisible={false}
        item={item}
        width={Constants.CELL_WIDTH}
        onTapMoreOption={this._triggerEditItem}
      />
    )
  }

  render() {
    const { userItems } = this.props
    console.log('*** userItems', userItems)
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header
          style={{ marginBottom: 20 }}
          title="My Closet"
          titleStyle={Styles.headerTitle}
          RightComponent={<Text style={[Styles.textHeaderRight]} text="Add" onPress={this._triggerAddItem} />}
        />
        <CategoryGrid
          style={{ marginBottom: 70 }}
          itemHeight={Constants.CELL_HEIGHT - 35}
          data={userItems}
          emptyViewProps={{
            title: 'Earn Money as a Rent My Wardrobe Lender',
            info:
              'Have some great pieces of clothes that others would love to rent? Rent My Wardrobe makes it easy to earn extra money reaching fashionistas looking for unique pieces, just like yours.',
            buttonText: 'Get Started',
            onPress: this._triggerAddItem
          }}
          itemCellProps={{
            editable: true,
            ownerNameVisible: false,
            onTapMoreOption: this._triggerEditItem
          }}
          onRefresh={this._onRefresh}
          onLoadMore={this._onLoadMore}
        />
      </View>
    )
  }
}

const mapStateToProps = (state: StoreState) => {
  return {
    userItems: state.userItems || [],
    userProfile: state.userProfile
  }
}

export default connect(mapStateToProps)(Closet)
