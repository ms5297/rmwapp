import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Header } from 'rn-components'
import { BackButton } from '@Components'
import { NavigationProps } from '@Types'
import { WebView } from 'react-native-webview'
import Styles from '@Styles'

type Props = {
  navigation: NavigationProps
}
class WebsiteView extends React.Component<Props> {
  render() {
    const { state } = this.props.navigation
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header
          statusBarProps={{ barStyle: 'dark-content' }}
          title="rentmywardrobe.com"
          titleStyle={Styles.headerTitle}
          LeftComponent={<BackButton />}
        />
        <View style={{ flex: 1 }}>
          <View style={{ height: 1, backgroundColor: '#979797' }} />
          <WebView style={{ flex: 1 }} source={{ uri: state.params.url }} />
        </View>
      </View>
    )
  }
}

export default WebsiteView
