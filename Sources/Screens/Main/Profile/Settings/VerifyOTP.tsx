import React from 'react'
import { View, Keyboard } from 'react-native'
import { Header, Text, Row, StyleSheet } from 'rn-components'
import { Button, BackButton } from '@Components'
import { NavigationProps } from '@Types'
import { Navigator } from '@Navigation'
import { FirebaseHelper, Api } from '@Services'
import { VerifyOTPParam } from '@Models'
import { getStore, UserProfileActions } from '@ReduxManager'
import Assets from '@Assets'
import CodeInput from 'react-native-confirmation-code-field'
import { Validator } from '@Utils'

type Props = {
  navigation: NavigationProps
}

type State = VerifyOTPParam & {
  code: string
}

class VerifyOTP extends React.Component<Props, State> {
  _inputRef = React.createRef<CodeInput>()

  constructor(props: Props) {
    super(props)
    const param: VerifyOTPParam = this.props.navigation.getParam('param')
    this.state = {
      code: '',
      ...param
    }
  }

  _onFulfill = (code: string) => {
    this.setState({ code })
    this._verifyOTP(code)
  }

  _onVerifyOTP = () => {
    const { code } = this.state
    this._verifyOTP(code)
  }

  _verifyOTP = (code: string) => {
    Keyboard.dismiss()
    const { onSuccess, phone } = this.state

    if (typeof onSuccess !== 'function' && phone !== null) {
      Navigator.showToast('Error', '(VOT - 49) Internal Error', 'Error')
      return
    }

    onSuccess(code)
    Navigator.back()
  }

  _resendCode = () => {
    const { phone } = this.state

    if (phone === null) {
      Navigator.showToast('Error', '(VOT - 61) Internal Error', 'Error')
      return
    }
    Navigator.showLoading()
    FirebaseHelper.verifyPhoneNumber(phone)
      .then(response => {
        Navigator.hideLoading()
        Navigator.showToast('Success', 'Your OTP code is sent.', 'Success')
      })
      .catch(error => {
        console.warn('**** error', error)
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  render() {
    const { phone } = this.state

    return (
      <View style={StyleSheet.absoluteFill}>
        <Header LeftComponent={<BackButton />} />
        <View style={styles.content}>
          <Text style={styles.helpText} text={`Enter code to\nverify phone`} />
          <Text style={styles.infoText} text={`We just texted you a 6 digit code, just\nenter it below.`} />
          <CodeInput
            containerProps={{ style: { flex: 0, marginTop: 20 } }}
            ref={this._inputRef}
            autoFocus
            cellBorderWidth={1}
            codeLength={6}
            activeColor={Assets.colors.appTheme}
            inactiveColor={Assets.colors.borderColorDark}
            onFulfill={this._onFulfill}
            cellProps={{ style: styles.otpText }}
            size={50}
            ratio={0.9}
          />
          <Button style={{ marginTop: 60 }} type="solid" text="Verify Phone" onPress={this._onVerifyOTP} />
          <Row style={{ marginTop: 21 }} alignHorizontal="space-between" alignVertical="center">
            <Text style={styles.phoneInfoText} text={`sent to ${phone}`} />
            <Text style={styles.resendButtonText} text="Resend Code" onPress={this._resendCode} />
          </Row>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    marginBottom: 20
  },
  helpText: {
    marginTop: 25,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    width: '100%'
  },
  infoText: {
    marginTop: 15,
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    width: '100%'
  },
  otpText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 24,
    textAlign: 'center',
    color: Assets.colors.mainText,
    borderRadius: 4
  },
  content: {
    flex: 1,
    marginHorizontal: 30,
    marginTop: 20
  },

  verifyButton: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: 48,
    marginHorizontal: 10,
    marginTop: 10,
    borderRadius: 6,
    overflow: 'hidden',
    backgroundColor: Assets.colors.buttonColor
  },

  verifyButtonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: 'white'
  },
  phoneInfoText: {
    color: Assets.colors.textLight,
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14
  },
  resendView: {
    marginTop: 21,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  resendButtonText: {
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14
  }
})

export default VerifyOTP
