import React from 'react'
import { View, Keyboard } from 'react-native'
import { StyleSheet, Header, TextInput, Text } from 'rn-components'
import { Button, BackButton } from '@Components'
import { getStore, UserProfileActions } from '@ReduxManager'
import { Validator } from '@Utils'
import { Navigator } from '@Navigation'
import Styles from '@Styles'
import Assets from '@Assets'
import { FirebaseHelper, RNFirebasePhoneAuthSnapshot, Api } from '@Services'
import { VerifyOTPParam } from '@Models'

type Props = {}
type State = {
  oldPhone: string
}

class ChangePhoneNumber extends React.Component<Props, State> {
  _phoneInputRef = React.createRef<TextInput>()

  constructor(props: Props) {
    super(props)
    let oldPhone = getStore().getState().userProfile.phone
    oldPhone = oldPhone.replace('+1', '')
    this.state = { oldPhone }
  }
  componentDidMount() {
    this._phoneInputRef.current.focus()
  }

  _handleVerifyOTPSuccess = (snapshot: RNFirebasePhoneAuthSnapshot, code: string) => {
    Navigator.showLoading()
    FirebaseHelper.updatePhoneNumber(snapshot.verificationId, code)
      .then(response => {
        const { userProfile } = getStore().getState()
        const newPhone = this._phoneInputRef.current.getText()
        const newProfile = {
          ...userProfile,
          phone: '+1' + newPhone
        }
        return Api.editUserProfile(newProfile)
      })
      .then((response: any) => {
        if (response.code !== 200) {
          throw Error(response.message || '(CPN - 45) Internal Error.')
        }
        UserProfileActions.saveUserProfile(response.user)
        Navigator.hideLoading()
        Navigator.showToast(
          'Success',
          'Your phone number has been updated.',
          'Success',
          3000,
          () => null,
          () => {
            Navigator.pop(2, false)
          },
          true
        )
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  _onLogin = () => {
    const phoneNumber = this._phoneInputRef.current.getText()
    const { oldPhone } = this.state
    console.log('**** oldPhone , phoneNumber', oldPhone, phoneNumber)
    if (oldPhone == phoneNumber) {
      Navigator.showToast('Error', 'New phone must be different with your current phone', 'Error')
      return
    }
    if (Validator.validatePhoneNumber(phoneNumber) == false) {
      Navigator.showToast('Error', 'Please enter a valid phone number.', 'Error')
      return
    }
    Keyboard.dismiss()
    Navigator.showLoading()
    const phone = '+1' + phoneNumber
    FirebaseHelper.verifyPhoneNumber(phone)
      .then(snapshot => {
        console.log('snapshot', snapshot)
        if (snapshot.error) {
          throw Error(snapshot.error.message)
        }
        const param: VerifyOTPParam = {
          phone,
          onSuccess: (code: string) => this._handleVerifyOTPSuccess(snapshot, code)
        }
        Navigator.hideLoading()
        Navigator.navTo('VerifyOTP', { param })
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header LeftComponent={<BackButton />} title="Change Phone Number" titleStyle={Styles.headerTitle} />
        <View style={styles.content}>
          <TextInput
            style={styles.inputContainer}
            inputStyle={styles.input}
            ref={this._phoneInputRef}
            LeftComponent={<Text style={styles.codeText} text="+1" />}
            clearButtonMode="while-editing"
            placeholderTextColor={Assets.colors.placeholder}
            keyboardType="phone-pad"
            placeholder="Phone Number"
            underlineWidth={0}
            underlineColor="transparent"
            onSubmitEditing={this._onLogin}
          />
          <Button type="solid" onPress={this._onLogin} style={{ marginTop: 95 }} text="Submit" />
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  content: {
    flex: 1,
    marginHorizontal: 30,
    marginTop: 20
  },
  codeText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: '#45515470',
    marginHorizontal: 10,
    marginTop: 1
  },
  orText: {
    marginTop: 15,
    color: Assets.colors.textLight,
    fontSize: 14,
    fontFamily: Assets.fonts.text.regular,
    alignSelf: 'center',
    textAlign: 'center'
  },
  inputContainer: {
    marginTop: 64,
    backgroundColor: Assets.colors.inputBg,
    borderColor: 'transparent',
    borderRadius: 6,
    alignItems: 'center'
  },
  input: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    height: 44,
    marginTop: 4,
    textAlignVertical: 'center'
  },

  loginInfoText: {
    marginTop: 10,
    width: '100%',
    color: Assets.colors.mainText,
    fontSize: 14,
    fontFamily: Assets.fonts.text.regular
  }
})

export default ChangePhoneNumber
