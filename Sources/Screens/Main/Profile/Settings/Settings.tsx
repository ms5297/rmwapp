import React from 'react'
import { FlatList, View, StyleSheet } from 'react-native'
import { Header } from 'rn-components'
import { BackButton, Section } from '@Components'
import { Authentication, FirebaseHelper, RNFirebasePhoneAuthSnapshot, RNFirebaseConfirmationResult } from '@Services'
import { AppConfig } from '@Utils'
import { Navigator } from '@Navigation'
import { StoreState, getStore } from '@ReduxManager'
import { connect } from 'react-redux'
import Assets from '@Assets'
import Styles from '@Styles'
import { VerifyOTPParam } from '@Models'
import AsyncStorage from '@react-native-community/async-storage'

type Props = {}
class Settings extends React.Component<Props> {
  _timerHandler = null

  _dataSource = [
    { title: 'Notifications', key: '1', order: 2 },
    { title: 'Payout Methods', key: '2', order: 3 },
    { title: 'Terms of Service', key: '3', order: 4 },
    { title: 'Privacy Policy', key: '4', order: 5 },
    { title: 'Change Phone Number', key: '5', order: 6 },
    { title: 'Logout', key: '6', order: 7 }
  ]
  constructor(props: Props) {
    super(props)
    const { userProfile } = getStore().getState()
    if (userProfile.role === 'admin') {
      this._dataSource.push({ title: 'Create Access Code', key: '-1', order: 0 })
      this._dataSource.push({ title: 'Change User', key: '0', order: 1 })
      this._dataSource.sort((a, b) => a.order - b.order)
    }

    /**added by Mahesh Fathom to check if user logged in by admin 20 Sep 2019*/
    // @ts-ignore
    if (global.isAdminLogged && userProfile.role !== 'admin') {
      // this._dataSource.push({ title: 'Create Access Code', key: '-1', order: 0 })
      this._dataSource.push({ title: 'Change User', key: '0', order: 1 })
      this._dataSource.sort((a, b) => a.order - b.order)
    }
    /**added by Mahesh Fathom to check if user logged in by admin 20 Sep 2019*/
  }

  componentWillUnmount() {
    this._timerHandler && clearTimeout(this._timerHandler)
  }

  _handleVerifyOTPSuccess = (confirmationResult: RNFirebaseConfirmationResult, code: string) => {
    Navigator.showLoading()
    confirmationResult
      .confirm(code)
      .then(user => {
        return FirebaseHelper.reAuthenticate(confirmationResult.verificationId, code)
      })
      .then(() => {
        Navigator.hideLoading()
        Navigator.navTo('ChangePhoneNumber')
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  _handleReAuth = () => {
    const { userProfile } = getStore().getState()
    const phone = userProfile.phone
    Navigator.showLoading()
    FirebaseHelper.loginWithPhone(phone)
      .then(confirmationResult => {
        Navigator.hideLoading()
        const param: VerifyOTPParam = {
          phone,
          onSuccess: (code: string) => this._handleVerifyOTPSuccess(confirmationResult, code)
        }
        Navigator.navTo('VerifyOTP', { param })
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  didSelectOptionItem = (key: string) => {
    switch (key) {
      case '1':
        Navigator.navTo('Notifications')
        break
      case '2':
        Navigator.navTo('PayoutMethods')
        break
      case '3':
        Navigator.navTo('WebsiteView', {
          url: AppConfig.terms_url
        })
        break
      case '4':
        Navigator.navTo('WebsiteView', {
          url: AppConfig.privacy_url
        })
        break
      case '5':
        Navigator.showAlert(
          'Change phone number',
          'This operation is sensitive and requires re-authentication.\n\nAre you sure you want to change your current phone number?',
          () => null,
          this._handleReAuth,
          'CANCEL',
          'OK'
        )
        break
      case '6':
        // @ts-ignore
        if (!global.isAdminLogged) {
          Navigator.showAlert(
            'Logout',
            'Are you sure you want to logout?',
            () => null,
            () => {
              Navigator.showLoading()
              this._timerHandler = setTimeout(() => {
                Authentication.logout()
                  .then(() => {
                    AsyncStorage.multiRemove(['USER_BIRTHDAY', 'USER_LONGITUDE', 'USER_LATITUDE', 'USER_LOCALITY'])

                    Navigator.hideLoading(() => Navigator.reset('Start'))
                  })
                  .catch(error => {
                    Navigator.hideLoading()
                    Navigator.showToast('Error', error.message, 'Error')
                  })
              }, 1000)
            },
            'CANCEL',
            'OK'
          )
        }
        break
      case '0':
        Navigator.navTo('Admin')
        break
      case '-1':
        Navigator.navTo('CreateAccessCode')
        break
      default:
        break
    }
  }

  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header LeftComponent={<BackButton />} title="Settings" titleStyle={Styles.headerTitle} />
        <FlatList
          style={{ flex: 1, marginTop: 30 }}
          data={this._dataSource}
          renderItem={this._renderListItem}
          keyExtractor={this._keyExtractor}
          showsVerticalScrollIndicator={false}
          scrollEnabled={false}
        />
      </View>
    )
  }

  _keyExtractor = (item: any, index: number) => item.key + index

  _renderListItem = ({ item }: any) => {
    const { title, key } = item
    const source = key === '6' ? null : Assets.images.categoryDetailsArrow
    const onPress = () => this.didSelectOptionItem(key)
    return <Section style={{ marginHorizontal: 20 }} title={title} iconSource={source} onPress={onPress} />
  }
}

const mapStateToProps = (state: StoreState) => {
  return {
    permissions: state.permissions
  }
}

export default connect(mapStateToProps)(Settings)
