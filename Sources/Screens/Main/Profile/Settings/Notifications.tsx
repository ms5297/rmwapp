import React from 'react'
import { View, StyleSheet, ScrollView, Text as RNText } from 'react-native'
import { StoreState, NotificationSettingsActions, PermissionsActions } from '@ReduxManager'
import { connect } from 'react-redux'
import { NotificationSettings, PermissionsState } from '@Models'
import { Navigator } from '@Navigation'
import { Api, Permissions, FirebaseHelper } from '@Services'
import { BackButton, Switch } from '@Components'
import { Header, Row, Text } from 'rn-components'
import { Device } from '@Utils'
import Styles from '@Styles'
import Assets from '@Assets'

type Props = {
  permissions: PermissionsState
  notificationSettings: NotificationSettings
}

type State = {
  settings: {
    messages_email: boolean
    messages_push: boolean
    promotion_email: boolean
    promotion_push: boolean
    account_email: boolean
  }
}

const isAndroid = Device.isAndroid()
class Notifications extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    const { notificationSettings } = props
    const defaultSettings = {
      messages_email: true,
      messages_push: false,
      promotion_email: true,
      promotion_push: false,
      account_email: true
    }
    const settings = notificationSettings ? notificationSettings : defaultSettings
    this.state = {
      settings
    }
  }

  _onOpenSettings = () => {
    Permissions.openSettings(status => {
      Permissions.checkLocationAndNotification(['notification', 'location'])
        .then(status => {
          const { notification, location } = status
          PermissionsActions.saveAllPermissons(notification, location)
        })
        .catch(error => {
          console.warn('error: ', error)
        })
    })
  }

  _onTapBackButton = () => {
    const { settings } = this.state
    const { notificationSettings } = this.props
    const needUpdate = JSON.stringify(notificationSettings) !== JSON.stringify(settings)
    if (needUpdate) {
      Navigator.showLoading()
      Api.editNotificationSettings(settings)
        .then(response => {
          Navigator.hideLoading()
          NotificationSettingsActions.saveNotificationSettings(settings)

          Navigator.showToast(
            'Success',
            'Edit settings successfully',
            'Success',
            2000,
            () => {
              FirebaseHelper.getAndUpdateToken()
            },
            () => Navigator.back(),
            true
          )
        })
        .catch(error => {
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
        })
    } else {
      Navigator.back()
    }
  }

  _didSelectOptionItem = (type: number) => {
    let settings = Object.assign({}, this.state.settings)
    switch (type) {
      case 1:
        settings.messages_email = !settings.messages_email
        break
      case 2:
        settings.messages_push = !settings.messages_push
        break
      case 3:
        settings.promotion_email = !settings.promotion_email
        break
      case 4:
        settings.promotion_push = !settings.promotion_push
        break
      case 5:
        settings.account_email = !settings.account_email
        break
      default:
        break
    }
    this.setState({ settings })
  }

  render() {
    const { promotion_email, account_email, messages_email, messages_push, promotion_push } = this.state.settings
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header
          title="Notifications"
          titleStyle={Styles.headerTitle}
          LeftComponent={<BackButton onPress={this._onTapBackButton} />}
        />
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingBottom: 20 }}>
          <Text style={styles.mainHeaderText} text="Updates from Rent My Wardrobe" />
          {this._renderSectionTitle(
            'Promotion and Tips',
            'Receive coupons, promotions, and product updates from Rent My Wardrobe and it’s partners.'
          )}
          {this._renderListItem(3, 'Email', promotion_email)}
          {this._renderListItem(4, 'Push Notification', promotion_push)}
          {this._renderSectionTitle('Messages', 'Receive messages from renters and lenders including rental requests.')}
          {this._renderListItem(1, 'Email', messages_email)}
          {this._renderMessageNotification()}
          {this._renderSectionTitle(
            'Account Support',
            'We may need to send you messages regarding your account. For security reasons you cannot disable email notifications.'
          )}
          {this._renderListItem(5, 'Email', account_email, false)}
        </ScrollView>
      </View>
    )
  }

  _renderSectionTitle = (title: string, description: string) => {
    return (
      <View>
        <Text style={styles.sectionHeaderText}>{title}</Text>
        <Text style={styles.sectionDescriptionText}>{description}</Text>
      </View>
    )
  }

  _renderMessageNotification = () => {
    const { messages_push } = this.state.settings
    const { permissions } = this.props
    const denied = permissions.notification === 'denied'
    const valueText = messages_push ? 'On ' : 'Off'
    const onValueChange = () => this._didSelectOptionItem(2)
    return (
      <View style={styles.listItem}>
        <Row alignHorizontal="space-between" alignVertical="center">
          <Text style={styles.itemTitle} text="Push Notification" />
          {denied ? (
            <Text style={styles.sliderValueText} text="Off" />
          ) : (
            <Row alignVertical="center">
              <Text style={styles.sliderValueText} text={valueText} />
              <Switch value={messages_push} onValueChange={onValueChange} />
            </Row>
          )}
        </Row>
        {denied && (
          <Text style={[styles.sectionDescriptionText, { marginBottom: 0, marginTop: 10 }]}>
            To enable notifications, go to{' '}
            <RNText onPress={this._onOpenSettings} style={{ color: Assets.colors.appTheme }}>
              settings
            </RNText>
          </Text>
        )}
      </View>
    )
  }

  _renderListItem = (type: number, title: string, value: boolean, switchEnable = true) => {
    const valueText = value ? 'On ' : 'Off'
    const onValueChange = () => this._didSelectOptionItem(type)
    return (
      <Row style={styles.listItem} alignHorizontal="space-between" alignVertical="center">
        <Text style={styles.itemTitle} text={title} />
        <Row alignVertical="center">
          <Text style={styles.sliderValueText} text={valueText} />
          <Switch value={value} disabled={!switchEnable} onValueChange={onValueChange} />
        </Row>
      </Row>
    )
  }
}

const styles = StyleSheet.create({
  mainHeaderText: {
    marginTop: 30,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 20,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 24,
    color: Assets.colors.mainText,
    letterSpacing: -0.2,
    lineHeight: 28
  },

  sectionHeaderText: {
    marginTop: 20,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 10,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    color: Assets.colors.mainText,
    lineHeight: 20
  },

  sectionDescriptionText: {
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 20,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2,
    lineHeight: 18
  },

  itemHolderView: {
    flex: 1,
    height: 64,
    alignItems: 'center',
    marginLeft: 15,
    marginRight: 15,
    flexDirection: 'column'
  },

  itemTitle: {
    marginLeft: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.4
  },

  sliderValueText: {
    marginRight: 10,
    textAlign: 'right',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2
  },
  listItem: {
    marginHorizontal: 15,
    paddingVertical: 15,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: Assets.colors.seperatorColor
  }
})

const mapStateToProps = (state: StoreState) => {
  return {
    notificationSettings: state.notificationSettings,
    permissions: state.permissions
  }
}

export default connect(mapStateToProps)(Notifications)
