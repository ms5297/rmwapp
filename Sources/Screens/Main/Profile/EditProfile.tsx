import React from 'react'
import { View, StyleSheet, ImageSourcePropType } from 'react-native'
import { Header, Text, TextInput, InputGroup, ScrollView } from 'rn-components'
import { Colors, DateTime, Validator, Device, AppConfig, Utils } from '@Utils'
import { connect } from 'react-redux'
import { BackButton, Button, Avatar, Section } from '@Components'
import { StoreState, UserProfileActions } from '@ReduxManager'
import { NavigationProps } from '@Types'
import {
  UserProfile,
  FirebaseValueObject,
  Location,
  PlacePickerParam,
  SelectionListParam,
  EnumConfigKey
} from '@Models'
import { FirebaseHelper, Api, StripeHelper } from '@Services'
import { Navigator } from '@Navigation'
import Styles from '@Styles'
import Assets from '@Assets'
import DateTimePicker from 'react-native-modal-datetime-picker'
import Picker from 'react-native-picker'

type Props = {
  navigation: NavigationProps
  userProfile: UserProfile
}

type State = {
  firstName: string
  lastName: string
  username: string
  birthDate: string
  about: string
  email: string
  imageUrl: string
  location: Location | null
  locality: string
  phoneNumber: string
  height: string
  weight: string

  website: string
  facebook: string
  instagram: string
  twitter: string
  isDateTimePickerVisible: boolean
  source: ImageSourcePropType

  backupSizes: FirebaseValueObject | null
  sizes: FirebaseValueObject | null
  braSizes: string
  shoeSizes: FirebaseValueObject | null
  isInDallas: boolean
}
class EditProfile extends React.Component<Props, State> {
  _infoInputGroupRef = React.createRef<InputGroup>()
  _socialInputGroupRef = React.createRef<InputGroup>()
  _privateInputGroupRef = React.createRef<InputGroup>()

  _hasUserName = false

  _pickerConfig = {
    pickerFontFamily: Assets.fonts.display.regular,
    pickerToolBarFontSize: 17,
    pickerFontSize: 17,
    pickerCancelBtnColor: Colors.hexToRgb('#ffffff'),
    pickerConfirmBtnColor: Colors.hexToRgb('#ffffff'),
    pickerTitleColor: Colors.hexToRgb('#ffffff'),
    pickerConfirmBtnText: 'Done',
    pickerCancelBtnText: 'Cancel',
    pickerToolBarBg: Colors.hexToRgb(Assets.colors.appTheme)
  }

  constructor(props: Props) {
    super(props)

    const {
      location_id,
      dress_size_id,
      shoe_size_id,
      user_name,
      latitude,
      longitude,
      lastname,
      firstname,
      photo,
      phone,
      height,
      about_me,
      website,
      facebook,
      bra_size,
      birthday,
      locality,
      email,
      instagram,
      twitter,
      weight,
      backup_size_id
    } = this.props.userProfile

    console.log('User profile: ', this.props.userProfile)
    const sizeValue = Utils.getConfigWithValue('sizes', dress_size_id)
    const showSizeValue = Utils.getConfigWithValue('shoeSizes', shoe_size_id)
    const backupSizeValue = Utils.getConfigWithValue('sizes', backup_size_id)
    if (user_name.length > 0) {
      this._hasUserName = true
    }

    const source = typeof photo === 'string' && photo.length > 0 ? { uri: photo } : null

    this.state = {
      firstName: firstname ? firstname : '',
      lastName: lastname ? lastname : '',
      birthDate: birthday ? birthday : '',
      email: email ? email : '',
      username: user_name ? user_name : '',
      imageUrl: photo ? photo : '',
      locality: locality,
      phoneNumber: phone,
      height: height ? height : '',
      weight: weight ? weight : '',
      about: about_me ? about_me : '',
      website: website ? website : '',
      facebook: facebook ? facebook : '',
      instagram: instagram ? instagram : '',
      twitter: twitter ? twitter : '',
      sizes: sizeValue,
      shoeSizes: showSizeValue,
      braSizes: bra_size || '',
      backupSizes: backupSizeValue,
      isDateTimePickerVisible: false,
      location: latitude !== null && longitude !== null ? { latitude, longitude } : null,
      source,
      isInDallas: false
    }
  }

  _showHeightPicker = () => {
    let selected: string[] = []
    if (this.state.height.length > 0) {
      selected = this.state.height.split(' ')
    }

    Picker.init({
      pickerData: AppConfig.enumConfig.heights,
      selectedValue: selected,
      pickerTitleText: 'Select Height',
      ...this._pickerConfig,
      onPickerConfirm: data => { },
      onPickerCancel: data => { },
      onPickerSelect: data => {
        this.setState({ height: data.join(' ') })
      }
    })
    Picker.show()
  }

  _showBraSizePicker = () => {
    let selected: string[] = []
    if (this.state.braSizes.length > 0) {
      selected = this.state.braSizes.split(' ')
    }

    Picker.init({
      pickerData: AppConfig.enumConfig.braSizes,
      selectedValue: selected,
      pickerTitleText: 'Select Bra Size',
      ...this._pickerConfig,
      onPickerConfirm: data => {
        console.log(data)
      },
      onPickerCancel: data => {
        console.log(data)
      },

      onPickerSelect: data => {
        this.setState({
          braSizes: data.join(' ')
        })
      }
    })
    Picker.show()
  }

  _onSourceChange = source => this.setState({ source })

  _onSocialInputFocus = () => this._socialInputGroupRef.current.focus(0)

  _onPrivateInputFocus = () => this._privateInputGroupRef.current.focus(0)

  _renderSelectionView = (title: string, value: string, type: EnumConfigKey) => {
    let rightIcon = Assets.images.categoryDetailsArrow
    let titleExtensionStyle = null
    let valueExtensionStyle = null
    if (type == 'date') {
      rightIcon = Assets.images.calendar
      if (value !== '') {
        valueExtensionStyle = { color: Assets.colors.appTheme }
      }
    } else {
      if (value !== 'All' && value !== 'Choose a location') {
        valueExtensionStyle = { color: Assets.colors.appTheme }
      }
    }

    const onSelected = () => {
      if (type === 'market') {
        const param: PlacePickerParam = {
          title,
          onLocationSelected: (location, locality) => {
            Navigator.showLoading()
            Utils.checkIsInDallas(location.latitude, location.longitude)
              .then(isInDallas => {
                Navigator.hideLoading()
                this.setState({ location, locality, isInDallas })
              })
              .catch(error => {
                Navigator.hideLoading()
                this.setState({ location, locality, isInDallas: false })
              })
          }
        }
        Navigator.navTo('PlacePicker', { param })
      } else if (type === 'heights') {
        this._showHeightPicker()
      } else if (type === 'braSizes') {
        this._showBraSizePicker()
      } else if (type === 'date') {
        this._showDateTimePicker()
      } else {
        const mutiple = type === 'sortTypes'
        const param: SelectionListParam = {
          onItemsSelected: items => {
            // @ts-ignore
            this.setState({ [type]: mutiple ? items : items[0] })
          },
          removedItemTitles: ['All', 'One Size Fits All', 'Other'],
          mutiple,
          title: title,
          selectedItems: [this.state[type]],
          type
        }
        Navigator.navTo('SelectionList', { param })
      }
    }

    return (
      <Section
        title={title}
        titleStyle={titleExtensionStyle}
        valueStyle={valueExtensionStyle}
        value={value}
        iconSource={rightIcon}
        onPress={onSelected}
      />
    )
  }

  _renderContent = () => {
    const { username, firstName, lastName, about, website, source: _defaultSource } = this.state
    console.log('**** ', username, firstName, lastName, about, website, _defaultSource)
    const source = _defaultSource || Assets.images.defaultProfile
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.content}
        contentContainerStyle={{ paddingBottom: 20 }}
        autoScrollNextInput>
        <Avatar
          cropperCircleOverlay
          loadingStyle={{ borderRadius: 35, width: 70, height: 70 }}
          style={{
            width: 70,
            height: 70,
            borderRadius: 35,
            alignSelf: 'center'
          }}
          imageStyle={{ width: 70, height: 70, borderRadius: 35 }}
          defaultImageSource={source}
          onSourceChange={this._onSourceChange}
        />
        <InputGroup ref={this._infoInputGroupRef} style={{ marginTop: 20 }} onInputSubmit={this._onSocialInputFocus}>
          <TextInput
            clearButtonMode="while-editing"
            onChangeText={text => this.setState({ firstName: text })}
            defaultValue={firstName}
            style={styles.inputContainer}
            inputStyle={styles.input}
            placeholderTextColor={Assets.colors.placeholder}
            selectionColor={Assets.colors.mainText}
            autoCapitalize="words"
            placeholder="First Name"
          />
          <TextInput
            clearButtonMode="while-editing"
            onChangeText={text => this.setState({ lastName: text })}
            defaultValue={lastName}
            autoCapitalize="words"
            placeholder="Last Name"
            style={styles.inputContainer}
            inputStyle={styles.input}
            selectionColor={Assets.colors.mainText}
            placeholderTextColor={Assets.colors.placeholder}
          />
          <TextInput
            onChangeText={text => this.setState({ about: text })}
            defaultValue={about}
            style={[styles.inputContainer, { height: 85 }]}
            inputStyle={[styles.input, { marginLeft: 5, height: 85 }]}
            selectionColor={Assets.colors.mainText}
            placeholderTextColor={Assets.colors.placeholder}
            placeholder="About me"
            multiline
            scrollEnabled
          />
          <TextInput
            clearButtonMode="while-editing"
            onChangeText={text => this.setState({ username: text })}
            defaultValue={username}
            autoCapitalize="none"
            placeholder="Username"
            style={styles.inputContainer}
            inputStyle={styles.input}
            selectionColor={Assets.colors.mainText}
            placeholderTextColor={Assets.colors.placeholder}
          />
          <TextInput
            clearButtonMode="while-editing"
            onChangeText={text => this.setState({ website: text })}
            defaultValue={website}
            autoCapitalize="none"
            placeholder="Website"
            keyboardType={'url'}
            style={styles.inputContainer}
            inputStyle={styles.input}
            selectionColor={Assets.colors.mainText}
            placeholderTextColor={Assets.colors.placeholder}
            returnKeyType="next"
          />
        </InputGroup>
        {this._renderSocialMediaAccounts()}
        {this._renderPriviteInfo()}
      </ScrollView>
    )
  }

  _renderSocialMediaAccounts = () => {
    return (
      <View>
        {this._renderSectionHeader('Social Media Accounts')}
        <InputGroup ref={this._socialInputGroupRef} onInputSubmit={this._onPrivateInputFocus}>
          <TextInput
            clearButtonMode="while-editing"
            onChangeText={text => this.setState({ facebook: text })}
            defaultValue={this.state.facebook}
            style={styles.inputContainer}
            inputStyle={styles.input}
            autoCapitalize="none"
            placeholder="Facebook URL"
            selectionColor={Assets.colors.mainText}
            placeholderTextColor={Assets.colors.placeholder}
          />

          <TextInput
            clearButtonMode="while-editing"
            onChangeText={text => this.setState({ instagram: text })}
            defaultValue={this.state.instagram}
            style={styles.inputContainer}
            inputStyle={styles.input}
            autoCapitalize="none"
            placeholder="Instagram Username"
            selectionColor={Assets.colors.mainText}
            placeholderTextColor={Assets.colors.placeholder}
          />

          <TextInput
            clearButtonMode="while-editing"
            onChangeText={text => this.setState({ twitter: text })}
            defaultValue={this.state.twitter}
            style={styles.inputContainer}
            inputStyle={styles.input}
            autoCapitalize="none"
            placeholder="Twitter Username"
            selectionColor={Assets.colors.mainText}
            placeholderTextColor={Assets.colors.placeholder}
            returnKeyType="next"
          />
        </InputGroup>
      </View>
    )
  }

  _renderPriviteInfo = () => {
    const {
      height,
      weight,
      backupSizes,
      shoeSizes,
      sizes,
      braSizes,
      email,
      phoneNumber,
      locality,
      birthDate: _birthDate
    } = this.state
    const birthday = _birthDate ? DateTime.moment(_birthDate).format('MMM DD, YYYY') : ''

    let phoneString = phoneNumber
    const indexPhoneCode = phoneString.indexOf('+1')
    console.log('**** indexPhoneCode', indexPhoneCode)

    if (indexPhoneCode != -1) {
      phoneString = phoneString.replace('+1', '')
    }

    return (
      <View>
        {this._renderSectionHeader('Private Information')}
        <InputGroup ref={this._privateInputGroupRef}>
          <TextInput
            clearButtonMode="while-editing"
            onChangeText={text => this.setState({ email: text })}
            defaultValue={email}
            style={styles.inputContainer}
            inputStyle={styles.input}
            autoCapitalize="none"
            keyboardType="email-address"
            placeholder="Email Address"
            selectionColor={Assets.colors.mainText}
            placeholderTextColor={Assets.colors.placeholder}
          />
          <TextInput
            LeftComponent={<Text style={styles.codeText} text="+1" />}
            clearButtonMode="while-editing"
            onChangeText={text => this.setState({ phoneNumber: text })}
            defaultValue={phoneString}
            keyboardType="phone-pad"
            placeholder="Mobile Number"
            style={styles.inputContainer}
            inputStyle={[styles.input]}
            selectionColor={Assets.colors.mainText}
            placeholderTextColor={Assets.colors.placeholder}
          />
          <TextInput
            clearButtonMode="while-editing"
            keyboardType="decimal-pad"
            onChangeText={text => this.setState({ weight: text })}
            style={[styles.inputContainer, { marginTop: 10 }]}
            defaultValue={weight}
            inputStyle={[styles.input]}
            placeholder="Weight"
            selectionColor={Assets.colors.mainText}
            placeholderTextColor={Assets.colors.placeholder}
            RightComponent={<Text style={[styles.codeText, { marginRight: 10 }]} text="lbs" />}
            returnKeyType="next"
          />
        </InputGroup>
        {this._renderSelectionView('Height', height.length ? height : '', 'heights')}
        {this._renderSelectionView('Birthday', birthday, 'date')}
        {this._renderSelectionView('Location', locality.length ? locality : '', 'market')}
        {this._renderSelectionView('Typical Size', sizes && sizes.title !== 'All' ? sizes.title : '', 'sizes')}
        {this._renderSelectionView(
          'Backup Size',
          backupSizes && backupSizes.title !== 'All' ? backupSizes.title : '',
          'backupSizes'
        )}
        {this._renderSelectionView(
          'Shoe Size',
          shoeSizes && shoeSizes.title !== 'All' ? shoeSizes.title : '',
          'shoeSizes'
        )}
        {this._renderSelectionView('Bra Size', braSizes.length ? braSizes : '', 'braSizes')}
      </View>
    )
  }
  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true })

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false })

  _handleDatePicked = (date: Date) => {
    this.setState({ birthDate: date.toDateString() })
    this._hideDateTimePicker()
  }

  _renderSectionHeader = (title: string, marginTop = 20) => {
    return (
      <View style={[styles.sectionView, { marginTop }]}>
        <Text style={styles.sectionTitle} text={title} />
      </View>
    )
  }

  _uploadPhoto(callback: (url: string) => void) {
    const { imageUrl, source } = this.state
    const uploadedURL = source === null ? '' : typeof source === 'object' && 'uri' in source ? source.uri : imageUrl

    if (uploadedURL.length > 0 && Validator.isURL(uploadedURL) == false) {
      FirebaseHelper.uploadUserProfilePhoto(uploadedURL)
        .then(url => {
          callback(url)
        })
        .catch(() => {
          callback(uploadedURL)
        })
    } else {
      callback(uploadedURL)
    }
  }

  _onSave = () => {
    const {
      firstName,
      lastName,
      birthDate,
      email,
      imageUrl,
      phoneNumber,
      website,
      height,
      about,
      username,
      weight,
      locality,
      location,
      backupSizes,
      facebook,
      twitter,
      braSizes,
      instagram,
      shoeSizes,
      sizes,
      source,
      isInDallas
    } = this.state
    console.log('**** current state', this.state)
    if (firstName.length == 0) {
      Navigator.showToast('Error', 'Enter your first name.', 'Error')
      return
    }
    if (lastName.length == 0) {
      Navigator.showToast('Error', 'Enter your last name.', 'Error')
      return
    }
    if (Validator.validateEmail(email) == false) {
      Navigator.showToast('Error', 'Please enter a valid email address.', 'Error')
      return
    }
    if (Validator.validatePhoneNumber(phoneNumber) == false) {
      Navigator.showToast('Error', 'Please enter a valid phone number.', 'Error')
      return
    }

    let profiledata = {
      ...this.props.userProfile,
      firstname: firstName,
      lastname: lastName,
      birthday: birthDate,
      email: email,
      photo: imageUrl,
      locality: locality,
      latitude: location !== null ? location.latitude : null,
      longitude: location !== null ? location.longitude : null,
      phone: phoneNumber,
      height,
      weight,
      about_me: about,
      user_name: username,
      website,
      dress_size_id: sizes ? sizes.value : null,
      backup_size_id: backupSizes ? backupSizes.value : null,
      facebook,
      instagram,
      twitter,
      shoe_size_id: shoeSizes ? shoeSizes.value : null,
      bra_size: braSizes
    }

    if (isInDallas && !profiledata.locality) {
      profiledata.locality = 'Dallas, TX'
    }

    Navigator.showLoading()
    this._uploadPhoto(url => {
      console.log('************* url', url)

      if (Validator.isURL(url)) {
        profiledata.photo = url
      }

      let user: UserProfile = null
      Api.editUserProfile(profiledata)
        .then((response: any) => {
          FirebaseHelper.logEvent('ProfileEdited')
          if (response.code !== 200 || !response.user) {
            throw Error(response.message)
          }
          user = response.user

          return Utils.getAddress(user)
        })
        .then(address => {
          const name = Utils.getUserName(user)
          const { payment_customer_id, phone, email } = user
          const body = {
            phone,
            name,
            email,
            metadata: {
              Address: address.address,
              'App Version': Device.getAppVersion(),
              'Code Push Version': Device.getCodePushVersion(),
              Device: Device.getDeviceInfo()
            }
            // address
          }
          console.log('**** updateCustomerPaymentAccount body', body)
          return StripeHelper.updateCustomerPaymentAccount(payment_customer_id, body)
        })
        .then(response => {
          FirebaseHelper.logEvent('ProfileEdited')
          UserProfileActions.saveUserProfile(user)
          Navigator.hideLoading()
          Navigator.showToast(
            'Success',
            'Your profile has been updated',
            'Success',
            3000,
            () => null,
            () => {
              Navigator.back()
            },
            true
          )
        })
        .catch(error => {
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
        })
    })
  }

  render() {
    let current = new Date()
    const { birthDate } = this.state
    if (typeof birthDate === 'string' && birthDate !== '') {
      current = new Date(birthDate)
    }

    const maxDate = DateTime.moment()
      .subtract(13, 'years')
      .toDate()
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header
          titleStyle={Styles.headerTitle}
          statusBarProps={{ barStyle: 'dark-content' }}
          backgroundColor="white"
          title="Edit Profile"
          LeftComponent={
            <BackButton iconStyle={{ tintColor: Assets.colors.appTheme }} iconSource={Assets.images.closeIcon} />
          }
          RightComponent={<Text style={Styles.textHeaderRight} text="Save" onPress={this._onSave} />}
        />
        {this._renderContent()}
        <Button style={styles.saveButton} text="Save" onPress={this._onSave} />

        <DateTimePicker
          titleIOS="Select your birthday"
          date={current}
          maximumDate={maxDate}
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    marginBottom: 10,
    backgroundColor: 'white',
    borderColor: Assets.colors.borderColorLight,
    borderWidth: 0.5,
    borderRadius: 6,
    height: 44,
    width: '100%'
  },
  input: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    height: 44,
    marginLeft: 10,
    marginTop: 2
  },
  sectionView: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: 20
  },

  sectionTitle: {
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 18,
    color: Assets.colors.mainText,
    letterSpacing: -0.2
  },

  titleView: {
    width: 120,
    height: 44,
    marginLeft: 15,
    justifyContent: 'center'
  },

  value: {
    marginRight: 10,
    textAlign: 'right',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2
  },

  codeText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.codeColor,
    marginLeft: 10
  },

  content: {
    marginHorizontal: 15,
    paddingTop: 20
  },

  saveButton: {
    borderRadius: 0,
    marginBottom: Device.isIphoneX() ? 20 : 0
  }
})

const mapStateToProps = (state: StoreState) => {
  return {
    userProfile: state.userProfile
  }
}

export default connect(mapStateToProps)(EditProfile)
