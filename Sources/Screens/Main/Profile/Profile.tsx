import React from 'react'
import { FlatList, View, Image, TouchableOpacity, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { Header, Text, Row, Col } from 'rn-components'
import { Version, Avatar, RentalCollection } from '@Components'
import { Constants, Validator } from '@Utils'
import { Navigator } from '@Navigation'
import { StoreState, getStore } from '@ReduxManager'
import { StripePaymentMethods, UserProfile, BadgeCount } from '@Models'
import { FreshChatHelper, FirebaseHelper, GoogleAnalyticsHelper, AppsFlyerHelper } from '@Services'
import Assets from '@Assets'
import Styles from '@Styles'

import branch, { BranchEvent } from 'react-native-branch'

type Props = {
  paymentMethods: StripePaymentMethods
  userProfile: UserProfile
  badgeCount: BadgeCount
}
class Profile extends React.Component<Props> {
  _dataSource = [
    { title: 'List your wardrobe', key: '1' },
    { title: 'Share your wardrobe', key: '6' },
    { title: 'Chat', key: '7' },
    { title: 'Payments', key: '2' },
    { title: 'Settings', key: '3' },
    { title: 'Invite a Friend', key: '4' },
    { title: 'Help & Feedback', key: '5' }
  ]
  state = {
    displayName: '',
    imageUrl: '',
    adminUnread: 0
  }

  _handlePendingSteps = () => {
    const { userProfile, paymentMethods } = this.props
    if (userProfile.birthday === null || userProfile.birthday === '') {
      this._onEditProfile()
      return
    }

    if (paymentMethods.length == 0) {
      Navigator.navTo('AddCreditCard', { parentView: 'Profile' })
      return
    }

    this._shareUsersWardrobe()
  }

  _shareUsersWardrobe = async () => {
    // Navigator.showLoading();
    // const { userProfile } = this.props;
    // BranchHelper.createSharedUserURL(userProfile)
    //   .then(url => {

    //     Navigator.hideLoading();
    //     Share.open({
    //       title: "Did you know you can Rent My Wardrobe?",
    //       message: "",
    //       subject: "",
    //       url: url
    //     });
    //   })
    //   .catch(error => {
    //     Navigator.hideLoading();
    //     Navigator.showToast("Error", error.message, "Error");
    //   });

    Navigator.showLoading()
    const { userProfile } = this.props
    const { id = '', firstname = '', lastname = '', email = '', photo = '' } = userProfile
    const url = `shareProfile/${id}`
    const buo = await branch.createBranchUniversalObject(url, {
      locallyIndex: true,
      canonicalUrl: '',
      title: 'Did you know you can Rent My Wardrobe?',
      contentImageUrl: Validator.isURL(photo) ? photo : '',
      contentMetadata: {
        customMetadata: {
          user: JSON.stringify(userProfile),
          type: 'Share Profile'
        }
      }
    })

    new BranchEvent('RMW_Share_Profile', buo, { sharedBy: id, firstname, lastname, email }).logEvent()

    const { channel, completed, error } = await buo.showShareSheet(
      {
        emailSubject: '',
        messageBody: '',
        messageHeader: 'Did you know you can Rent My Wardrobe?'
      },
      {
        feature: 'share',
        channel: 'RMW_Share_Profile'
      },
      {
        $desktop_url: 'https://itunes.apple.com/us/app/rent-my-wardrobe/id1375748670?ls=1&mt=8',
        $ios_deepview: 'branch_default'
      }
    )

    Navigator.hideLoading()
    if (error) {
      console.error('Error sharing via Branch: ' + error)
      return
    }

    console.log('Share to ' + channel + ' completed: ' + completed)
  }

  _shareAppDetails = async () => {
    // Navigator.showLoading();
    // const { userProfile } = this.props;
    // BranchHelper.createSharedAppURL(userProfile)
    //   .then(url => {
    //     Navigator.hideLoading();
    //     Share.open({
    //       title: "This is what I’ve been telling you about!",
    //       message: "",
    //       subject: "",
    //       url: url
    //     });
    //   })
    //   .catch(error => {
    //     Navigator.hideLoading();
    //     Navigator.showToast("Error", error.message, "Error");
    //   });

    Navigator.showLoading()
    const { userProfile } = this.props
    const { id, firstname, email, lastname } = userProfile
    const url = `shareApp/fromUser/${id}/`
    const buo = await branch.createBranchUniversalObject(url, {
      locallyIndex: true,
      canonicalUrl: '',
      title: 'This is what I’ve been telling you about!',
      contentMetadata: {
        customMetadata: {
          invitedBy: id,
          type: 'Share App'
        }
      }
    })

    new BranchEvent('RMW_Share_App', buo, {
      invitedBy: id,
      firstname,
      lastname,
      email
    }).logEvent()

    const { channel, completed, error } = await buo.showShareSheet(
      {
        emailSubject: '',
        messageBody: '',
        messageHeader: 'This is what I’ve been telling you about!'
      },
      {
        feature: 'share',
        channel: 'RMW_Share_App'
      },
      {
        $desktop_url: 'https://itunes.apple.com/us/app/rent-my-wardrobe/id1375748670?ls=1&mt=8',
        $ios_deepview: 'branch_default'
      }
    )

    Navigator.hideLoading()
    if (error) {
      console.error('Error sharing via Branch: ' + error)
      return
    }

    console.log('Share to ' + channel + ' completed: ' + completed)
  }

  _didSelectOptionItem = (item: any) => {
    switch (item.key) {
      case '1':
        Navigator.navTo('Closet')
        break
      case '2':
        Navigator.navTo('PaymentMethods')
        break
      case '3':
        Navigator.navTo('Settings')
        break
      case '4':
        this._shareAppDetails()
        break
      case '5':
        FreshChatHelper.showFAQs()
        break
      case '6':
        this._shareUsersWardrobe()
        break
      case '7':
        Navigator.navTo('AdminInbox')
        break
      default:
        break
    }
  }

  _onEditProfile = () => Navigator.navTo('EditProfile')

  _keyExtractor = (item: any, index: number) => item.key + index

  _renderProfileDetails = () => {
    const { userProfile } = this.props
    if (!userProfile) return null

    let displayName = ''
    let source = Assets.images.defaultProfile
    const { firstname, lastname, photo } = userProfile
    displayName = firstname + ' ' + lastname
    if (typeof photo === 'string' && photo.length > 0) {
      source = { uri: photo }
    }

    return (
      <Col style={{ marginTop: 12, marginBottom: 24 }}>
        <Row style={{ paddingHorizontal: 15 }} onPress={this._onEditProfile}>
          <Avatar
            loadingStyle={{ borderRadius: 35, width: 70, height: 70 }}
            style={{ width: 70, height: 70, borderRadius: 35 }}
            imageStyle={{ width: 70, height: 70, borderRadius: 35 }}
            defaultImageSource={source}
            editable={false}
          />

          <Col>
            <Text numberOfLines={1} style={styles.profileName} text={displayName} />
            <Text numberOfLines={1} style={styles.viewAndEditText} text="View and edit profile" />
          </Col>
        </Row>
        {this._renderPendingSteps()}
      </Col>
    )
  }

  _renderPendingSteps = () => {
    let pendingSteps = 0
    const { userProfile, paymentMethods } = this.props
    if (userProfile.birthday === '') {
      ++pendingSteps
    }
    if (paymentMethods.length == 0) {
      ++pendingSteps
    }

    let pendingStepsMessage = pendingSteps === 1 ? '1 step left' : pendingSteps + ' steps left'
    let detailMessage = 'Complete your profile and add important details before listing or renting your wardrobe.'
    if (pendingSteps == 0) {
      pendingStepsMessage = '100% Complete'
      detailMessage = "Great job, your profile is complete! Now it's time to share your wardrobe."

      AppsFlyerHelper.trackEvent(AppsFlyerHelper.EVENTS.PROFILE_COMPLETED, userProfile.id, paymentMethods)

      GoogleAnalyticsHelper.trackEvent(GoogleAnalyticsHelper.EVENT_CATEGORY.MAIN, GoogleAnalyticsHelper.EVENT.PROFILE_COMPLETED, { user_id: userProfile.id })
    }

    const maxSteps = 6
    let indicatorWidth = (Constants.WINDOW_WIDTH - 30) * ((maxSteps - pendingSteps) / maxSteps)

    return (
      <Col>
        <Text style={styles.stepsText} text={pendingStepsMessage} />
        <View style={{ height: 40, marginTop: 6, marginBottom: 15 }}>
          <View style={styles.progressHolderView} />
          <View style={[styles.progressIndicatorView, { width: indicatorWidth }]} />
        </View>
        <Row alignHorizontal="space-between" alignVertical="center" onPress={this._handlePendingSteps}>
          <Text style={styles.completeProfileText} text={detailMessage} />
          <Image
            source={Assets.images.profileEditDetailIcon}
            style={{ marginHorizontal: 30, tintColor: Assets.colors.borderColor }}
          />
        </Row>
      </Col>
    )
  }
  _renderBagde = () => {
    const { badgeCount } = this.props
    let adminUnread = 0;
    if (badgeCount) {
      adminUnread = badgeCount.adminBadgeCount
    }
    if (adminUnread) {
      const unreadText = adminUnread > 10 ? '10+' : adminUnread
      return (
        <View style={styles.badgeContainer}>
          <Text style={styles.badgeText}>{unreadText}</Text>
        </View>
      )
    }
    return null
  }
  _renderListItem = ({ item }: any) => {
    const { userProfile } = this.props
    let itemImage = Assets.images.categoryDetailsArrow

    if (item.key === '7' && userProfile && userProfile.role != 'admin') {
      return
    }
    if (item.key === '4') {
      itemImage = Assets.images.shareIcon
    } else if (item.key === '6') {
      itemImage = Assets.images.menu_closet
    } else if (item.key === '7') {
      itemImage = Assets.images.chat_send
    }

    const onPress = () => this._didSelectOptionItem(item)
    return (
      <TouchableOpacity onPress={onPress}>
        <View style={styles.itemHolderView}>
          <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row' }}>
            <Text style={styles.itemTitle}>{item.title}</Text>
            {item.key === '7' ? this._renderBagde() : null}
          </View>
          <Image
            source={itemImage}
            style={{
              marginLeft: 6,
              marginRight: 15,
              tintColor: Assets.colors.borderColor
            }}
          />
        </View>

      </TouchableOpacity>
    )
  }

  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header
          title="Profile"
          titleStyle={Styles.headerTitle}
          statusBarProps={{ barStyle: 'dark-content' }}
          backgroundColor="white"
        />

        <FlatList
          style={{ flex: 1 }}
          data={this._dataSource}
          extraData={this}
          ListHeaderComponent={this._renderProfileDetails}
          renderItem={this._renderListItem}
          keyExtractor={this._keyExtractor}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
        />
        <Version style={{ marginBottom: 60 }} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    marginHorizontal: 20,
    marginTop: 20
  },

  profileName: {
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    color: Assets.colors.mainText,
    lineHeight: 24,
    marginTop: 11,
    marginLeft: 17,
    marginRight: 15
  },

  viewAndEditText: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.appTheme,
    letterSpacing: -0.2,
    lineHeight: 18,
    marginTop: 6,
    marginLeft: 17,
    marginRight: 15
  },

  itemHolderView: {
    flex: 1,
    height: 44,
    flexDirection: 'row',
    backgroundColor: '#F4F6F6',
    alignItems: 'center',
    borderRadius: 6,
    marginBottom: 10,
    marginLeft: 15,
    marginRight: 15
  },

  itemTitle: {
    marginLeft: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.4
  },

  stepsText: {
    marginRight: 15,
    textAlign: 'right',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2,
    lineHeight: 18
  },

  progressHolderView: {
    marginLeft: 16,
    marginRight: 16,
    borderWidth: 1,
    borderColor: Assets.colors.borderColor,
    borderRadius: 6,
    flexDirection: 'row',
    marginTop: 1,
    height: 38,
    overflow: 'hidden'
  },

  progressIndicatorView: {
    marginLeft: 15,
    marginRight: 15,
    height: 40,
    borderRadius: 6,
    overflow: 'hidden',
    backgroundColor: Assets.colors.appTheme,
    position: 'absolute',
    top: 0,
    left: 0
  },

  completeProfileText: {
    marginLeft: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2,
    lineHeight: 18,
    flex: 1
  },
  badgeContainer: {
    // position: 'absolute',
    // left: 25,
    // top: 5,
    marginLeft: 10,
    borderRadius: 9,
    height: 22,
    width: 22,
    backgroundColor: Assets.colors.appTheme,
    justifyContent: 'center',
    alignItems: 'center'
  },
  badgeText: {
    textAlign: 'center',
    fontFamily: Assets.fonts.display.regular,
    fontSize: 11,
    color: 'white'
  }
})

const mapStateToProps = (state: StoreState) => {
  return {
    paymentMethods: state.payments ? state.payments.paymentMethods : [],
    userProfile: state.userProfile,
    badgeCount: state.badgeCount
  }
}

export default connect(mapStateToProps)(Profile)
