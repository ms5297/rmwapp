import React from 'react'
import { StyleSheet, View, StatusBar, Image } from 'react-native'
import { Header, Icon } from 'rn-components'
import { Navigator, getRouteName } from '@Navigation'
import { StoreState } from '@ReduxManager'
import { connect } from 'react-redux'
import { NavigationProps } from '@Types'
import { UserProfile, ChatModelItem, ChatParam } from '@Models'
import { EmptyView, RefreshControl, BackButton } from '@Components'
import { FirebaseHelper } from '@Services'
import { LayoutAnimations, Constants } from '@Utils'
// import InboxItem from './InboxItem'
import { LayoutProvider, DataProvider, RecyclerListView } from 'recyclerlistview'
import Assets from '@Assets'
import Styles from '@Styles'
import { AdminInboxItem } from '@Screens'
import { AdminConfig } from '@Utils'

type Props = {
  navigation: NavigationProps
  userProfile: UserProfile
}

type State = {
  items: any[]
  showingArchive: boolean
  loading: boolean
  dataProvider: DataProvider
  pageIndex: 1
  refreshing: boolean
}
class AdminInbox extends React.Component<Props, State> {
  _dataSource = []
  _unsubscribe = null
  _layoutProvider: LayoutProvider = null
  _timerHandler = null

  constructor(props) {
    super(props)
    const dataRow: any = new DataProvider((r1, r2) => r1 !== r2)
    const dataProvider = dataRow.cloneWithRows([])

    this._layoutProvider = new LayoutProvider(
      index => 1,
      (type, dim) => {
        dim.width = Constants.WINDOW_WIDTH
        dim.height = Constants.INBOX_ITEM_HEIGHT
        dim.height = dim.height - 20
      }
    )

    LayoutAnimations.enableAndroidLayoutAnimation()

    this.state = {
      items: [],
      showingArchive: false,
      loading: true,
      dataProvider,
      pageIndex: 1,
      refreshing: false
    }
  }

  componentWillMount() {
    StatusBar.setBarStyle('dark-content')
    this._loadMessages(false)
  }

  componentWillUnmount() {
    this._unsubscribe && this._unsubscribe()
    this._timerHandler && clearTimeout(this._timerHandler)
  }

  _onRefresh = () => {
    Navigator.showLoading()
    const { dataProvider, showingArchive } = this.state
    const { userProfile } = this.props
    const { id, name, photo } = AdminConfig.adminChatConfig

    this.setState({ refreshing: true })
    FirebaseHelper.getAdminConversations()
      .where('archived', '==', showingArchive)
      .where('fromUser.id', '==', id)
      // .limit(300)
      .get()
      .then(snapshoot => {
        let items: ChatModelItem[] = []
        snapshoot.forEach(doc => {
          const item = doc.data() as ChatModelItem
          items.push(item)
        })
        items.sort((a, b) => b.lastMessageTime - a.lastMessageTime)

        this.setState({ refreshing: false, items, dataProvider: dataProvider.cloneWithRows(items) })
        Navigator.hideLoading()
      })
      .catch(error => {
        this.setState({ refreshing: false })
        Navigator.hideLoading()
      })
  }
  goToUserList = () => {
    Navigator.navTo('UserList')
  }

  _loadMessages(archived: boolean, callback?: () => void) {
    const currentRouteName = getRouteName(this.props.navigation)
    currentRouteName == 'AdminInbox' && Navigator.showLoading()
    const { userProfile } = this.props
    const { id, name, photo } = AdminConfig.adminChatConfig

    let items = [...this.state.items]
    this._unsubscribe && this._unsubscribe()
    this._unsubscribe = FirebaseHelper.getAdminConversations()
      .where('archived', '==', archived)
      .where('fromUser.id', '==', id)
      .onSnapshot(snapshot => {
        snapshot.docChanges.forEach(change => {
          let data: any = change.doc.data()

          if (data == null) return

          data.id = change.doc.id

          if (change.type === 'added') {
            items.push(data)
          } else if (change.type === 'modified') {
            const index = items.findIndex(item => item.id === change.doc.id)
            items.splice(index, 1, data)
          } else if (change.type === 'removed') {
            const index = items.findIndex(item => item.id === change.doc.id)
            items.splice(index, 1)
          }
        })

        items.sort((chat1, chat2) => {
          if (chat1.lastMessageTime == null) return -1
          if (chat2.lastMessageTime == null) return -1
          if (chat1.lastMessageTime > chat2.lastMessageTime) return -1
          if (chat1.lastMessageTime < chat2.lastMessageTime) return 1
        })

        currentRouteName == 'AdminInbox' && LayoutAnimations.setLayoutAnimation(LayoutAnimations.ListItem)
        this._timerHandler = setTimeout(() => {
          this.setState({ items, dataProvider: this.state.dataProvider.cloneWithRows(items), loading: false })
          currentRouteName == 'AdminInbox' && Navigator.hideLoading()
          if (typeof callback === 'function') {
            callback()
          }
        }, 300)
      })
  }

  _showDeleteConfirmation = (item: ChatModelItem, index) => {
    return new Promise<boolean>((resolve, reject) => {
      const archive = item.archived
      const title = archive ? 'Unarchive this chat?' : 'Archive this chat?'
      const message = archive
        ? 'Are you sure you want to unarchive this chat?'
        : 'Are you sure you want to archive this chat?'
      Navigator.showAlert(
        title,
        message,
        () => null,
        () => {
          null
          // Navigator.showLoading()
          // this.setState({ loading: true })
          // FirebaseHelper.getAdminConversations()
          //   .doc(item.id)
          //   .set({ archived: !archive }, { merge: true })
          //   .then(() => {
          //     resolve(true)
          //     this.setState({ loading: false })
          //     Navigator.hideLoading()
          //   })
          //   .catch(error => {
          //     resolve(true)
          //     this.setState({ loading: false })
          //     Navigator.hideLoading()
          //   })
        },
        'CANCEL',
        'OK'
      )
    })
  }

  _onTapSwipeItem = (item, index) => {
    return this._showDeleteConfirmation(item, index)
  }

  _onTapAvatar = user => Navigator.navTo('PublicProfile', { user }) //user: { id: user.id }

  _onTapChatItem = item => {
    if (item.chat_id === null || item.chat_id === undefined) {
      Navigator.showToast('Error', '(Inbox-147) Invalid Rental Id! Please contact support.', 'Error')
      return
    } else {
      const param: ChatParam = {
        rental_id: item.chat_id,
        item: { id: item.itemId },
        fromPush: false,
        fromUser: item.fromUser.id,
        toUser: item.toUser.id,
        isOwner: item.itemOwner,
        chatID: item.id,
        message: item.lastMessage
      }
      Navigator.navTo('AdminChat', { param })
    }
  }

  _onArchive = () => {
    const { showingArchive, dataProvider } = this.state

    this.setState(
      { dataProvider: dataProvider.cloneWithRows([]), items: [], showingArchive: !showingArchive, loading: true },
      () => {
        this._loadMessages(!showingArchive)
      }
    )
  }

  _renderItem = (type, data, index) => {
    return (
      <View style={{ height: Constants.INBOX_ITEM_HEIGHT, width: Constants.WINDOW_WIDTH }}>
        <AdminInboxItem
          onTapSwipeItem={this._onTapSwipeItem}
          onTapAvatar={this._onTapAvatar}
          onTapChatItem={this._onTapChatItem}
          isArchived={this.state.showingArchive}
          item={data}
          index={index}
        />
      </View>
    )
  }

  _renderConntent = () => {
    const { dataProvider, loading, refreshing, items, showingArchive } = this.state

    let title = 'No messages yet'
    // let message =
    //   'When you find a garmet you love, connect with the lender. Tell them a bit about yourself and the occasion.'
    let message = 'As the users start communicating, messages will be listed here.'
    let buttonText = 'Start Searching'

    if (showingArchive) {
      title = 'No archived messages'
      message = 'You can archive a message by left swiping a chat item, or tapping more button in chat screen.'
    }
    const onPress = showingArchive ? null : () => this.goToUserList()

    if (Array.isArray(items) && items.length) {
      return (
        <RecyclerListView
          scrollViewProps={{
            showsVerticalScrollIndicator: false,
            showsHorizontalScrollIndicator: false,
            decelerationRate: 'normal',
            refreshControl: <RefreshControl refreshing={refreshing} onRefresh={this._onRefresh} />,
            contentContainerStyle: { paddingBottom: 40 }
          }}
          dataProvider={dataProvider}
          layoutProvider={this._layoutProvider}
          rowRenderer={this._renderItem}
        />
      )
    }
    if (loading) return null
    return <EmptyView title={title} info={message} buttonText={buttonText} onPress={onPress} />
  }

  render() {
    const { showingArchive } = this.state
    // const headerTitle = showingArchive ? 'Archive' : 'Inbox'
    const headerTitle = 'Admin Inbox'
    // const tintColor = showingArchive ? Assets.colors.appTheme : Assets.colors.borderColor

    return (
      <View style={StyleSheet.absoluteFill}>
        <Header
          title={headerTitle}
          titleStyle={Styles.headerTitle}
          rightContainerStyle={{ alignItems: 'flex-end' }}
          statusBarProps={{ barStyle: 'dark-content' }}
          LeftComponent={<BackButton />}
          RightComponent={
            // <Icon iconStyle={{ tintColor }} iconSource={Assets.images.chatArchive} onPress={this._onArchive} />
            <Icon iconStyle={{ width: 30, height: 30, borderColor: "#ccc", borderWidth: 2, borderRadius: 50 }} iconSource={Assets.images.user_photo_placeholder} onPress={this.goToUserList} />
          }
        />
        <View style={{ flex: 1, marginBottom: 40, marginTop: 30 }}>{this._renderConntent()}</View>
        {/* {this._renderFooterUserListButton()} */}
      </View>
    )
  }
}

const mapPropsToState = (state: StoreState) => {
  return {
    userProfile: state.userProfile
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  myButton: {
    padding: 5,
    height: 90,
    width: 90,  //The Width must be the same as the height
    borderRadius: 400, //Then Make the Border Radius twice the size of width or Height   
    backgroundColor: 'rgb(195, 125, 198)',

  }
});
export default connect(mapPropsToState)(AdminInbox)

