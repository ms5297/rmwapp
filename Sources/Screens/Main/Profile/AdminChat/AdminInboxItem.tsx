import React from 'react'
import { TouchableOpacity, View, StyleSheet, Animated } from 'react-native'
import Swipeable from 'react-native-gesture-handler/Swipeable'
import { GiftedAvatar } from 'react-native-gifted-chat'
import { Utils, Constants, DateTime, Validator } from '@Utils'
import { InboxItemProps } from '@Models'
import { Row, Text, Touchable } from 'rn-components'

import TimeAgo from 'react-native-timeago'
import Assets from '@Assets'
import Styles from '@Styles'

class AdminInboxItem extends React.PureComponent<InboxItemProps> {
  _swipableRef = React.createRef<Swipeable>()
  _onTapSwipeItem = (item, index) => {
    const { onTapSwipeItem } = this.props
    onTapSwipeItem(item, index).then(close => {
      if (close) {
        this._swipableRef.current.close()
      }
    })
  }

  _renderRightAction = progress => {
    const { item, index } = this.props
    const translateX = progress.interpolate({
      inputRange: [0, 1],
      outputRange: [64, 0]
    })

    const containerAnimationStyle = {
      transform: [{ translateX }]
    }
    const scale = progress.interpolate({
      inputRange: [0, 1],
      outputRange: [0.2, 1]
    })

    const iconAnimationStyle = {
      transform: [{ scale }]
    }
    const onPress = () => this._onTapSwipeItem(item, index)
    return (
      <Animated.View style={[styles.item, containerAnimationStyle]}>
        <TouchableOpacity onPress={onPress} style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
          <Animated.Image
            source={this.props.isArchived ? Assets.images.trash : Assets.images.chatArchive}
            style={[Styles.headerBack, { tintColor: 'white' }, iconAnimationStyle]}
          />
        </TouchableOpacity>
      </Animated.View>
    )
  }

  _renderStatus = () => {
    let color = Assets.colors.mainText
    let actionButtonText = 'Pending'
    const { item } = this.props
    const { status, order } = item
    switch (status) {
      case 'request':
        actionButtonText = 'Pending'
        break
      case 'auto_cancel':
        actionButtonText = 'Cancelled'
        break
      case 'renting':
        actionButtonText = 'Renting'
        if (order) {
          const due = Utils.checkDue(order.endDate)
          if (due) {
            color = Assets.colors.appTheme
            actionButtonText = due
          }
        }
        break
      case 'reviewed':
        actionButtonText = 'Completed'
        break
      case 'approved':
        actionButtonText = 'Approved'
        break
      case 'inquiry':
        actionButtonText = 'Inquiry'
        break
      case 'accepted':
        actionButtonText = 'Reserved'
        break
      case 'returned':
        actionButtonText = 'Returned'
        break
      case 'declined':
        actionButtonText = 'Declined'
        break
      case 'cancelled':
        actionButtonText = 'Cancelled'
        break
    }
    return (
      <Row>
        <Text style={styles.statusText} text="Status: " />
        <Text style={[styles.actionButtonText, { color }]} text={actionButtonText} />
      </Row>
    )
  }

  _renderBadge = () => {
    const { unread } = this.props.item
    if (unread) {
      const unreadText = unread > 10 ? '10+' : unread
      return (
        <View style={styles.badgeContainer}>
          <Text style={styles.badgeText} text={unreadText} />
        </View>
      )
    }
    return null
  }

  render() {
    const { item, onTapChatItem, onTapAvatar, index } = this.props
    const { order, lastMessage, lastMessageTime, toUser, unread } = item
    // const itemName = order.itemName
    // const itemDate = DateTime.format(order.startDate, 'MMM DD, YYYY')

    // const subTitle = itemDate && itemDate.length > 0 ? itemName + ' ∙ ' + itemDate : itemName
    const _onTapChatItem = () => onTapChatItem(item, index)
    const _onTapAvatar = () => onTapAvatar(item.toUser, index)

    const avatar = Validator.isURL(toUser.photo) ? toUser.photo : null
    return (
      <Swipeable
        ref={this._swipableRef}
        overshootRight={false}
        renderRightActions={this._renderRightAction}
        rightThreshold={40}>
        <Touchable onPress={_onTapChatItem}>
          <Row style={styles.rowContainer}>
            <TouchableOpacity activeOpacity={0.7} onPress={_onTapAvatar} style={styles.avatarContainer}>
              <GiftedAvatar user={{ _id: toUser.id, name: toUser.name, avatar }} />
            </TouchableOpacity>
            <View style={{ flex: 1, marginLeft: 10, marginRight: 15, alignSelf: 'center' }}>
              <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                <Text style={styles.userNameText}>{toUser.name}</Text>
                <View style={{ flex: 1 }} />
                <TimeAgo style={styles.timeText} time={lastMessageTime} />
              </View>
              <View style={{ marginTop: 7, flexDirection: 'row', justifyContent: 'center' }}>
                <Text numberOfLines={1} style={[styles.messageText, { flex: 1 }]} text={lastMessage} />
                {this._renderBadge()}
              </View>
              {/* <View style={{ marginTop: 9, flexDirection: 'row' }}>
                <View style={{ width: '60%' }}>
                  <Text numberOfLines={2} ellipsizeMode="tail" style={styles.dressInfoText} text={subTitle} />
                </View>
                <View style={{ flex: 1 }} />
                {this._renderStatus()}
              </View> */}
            </View>
          </Row>
        </Touchable>
      </Swipeable>
    )
  }
}

const styles = StyleSheet.create({
  rowContainer: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: Assets.colors.borderColor,
    height: Constants.INBOX_ITEM_HEIGHT
  },
  userNameText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: Assets.colors.mainText
  },

  timeText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 12,
    color: Assets.colors.textLight
  },
  messageText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    marginRight: 80,
    color: Assets.colors.textLight,
    textAlign: 'left'
  },
  dressInfoText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 12,
    color: Assets.colors.mainText
  },
  actionButtonText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 12,
    color: Assets.colors.mainText
  },
  statusText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 12,
    color: Assets.colors.textLight
  },
  badgeContainer: {
    borderRadius: 12,
    height: 24,
    width: 24,
    backgroundColor: Assets.colors.appTheme,
    justifyContent: 'center',
    alignItems: 'center'
  },
  badgeText: {
    textAlign: 'center',
    fontFamily: Assets.fonts.display.regular,
    fontSize: 12,
    color: 'white'
  },
  rightAction: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center'
  },
  item: {
    width: 64,
    backgroundColor: Assets.colors.error,
    justifyContent: 'center'
  },
  avatarContainer: {
    paddingTop: 30,
    paddingLeft: 15,
    alignSelf: 'stretch',
    alignItems: 'center',
    width: 65
  }
})
export default AdminInboxItem
