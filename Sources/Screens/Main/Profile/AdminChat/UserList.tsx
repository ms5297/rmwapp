import React from 'react'
import { View, StyleSheet, Image, ActivityIndicator } from 'react-native'
import { Header, Row, Col, Text, TextInput } from 'rn-components'
import { Navigator } from '@Navigation'
import { FirebaseHelper, Network, Api, StripeHelper } from '@Services'
import { FirebaseUser, UserProfile } from '@Models'
import { Avatar, Button, RefreshControl, Version, BackButton } from '@Components'
import Styles from '@Styles'
import Assets from '@Assets'
import {
  getStore,
  UserProfileActions,
  UserItemActions,
  ItemSummaryActions,
  NotificationSettingsActions,
  FavoritesActions,
  PaymentActions,
  ItemListActions,
  PayoutActions,
  FilterActions
} from '@ReduxManager'
import { Device, Constants, LayoutAnimations, Validator, Utils } from '@Utils'
import { RecyclerListView, DataProvider, LayoutProvider } from 'recyclerlistview'
import TimeAgo from 'react-native-timeago'
import debounce from 'lodash.debounce'
import { Directions } from 'react-native-gesture-handler'

type Props = {}

type State = {
  users: Array<FirebaseUser>
  searchUsers: Array<FirebaseUser>
  refreshing: boolean
  dataProvider: DataProvider
  loadable: boolean
  isSearchMode: boolean
  searching: boolean
  adminUser: FirebaseUser    //added by Mahesh
}

class UserList extends React.Component<Props, State> {
  _layoutProvider = null
  _inputRef = React.createRef<TextInput>()

  constructor(props) {
    super(props)
    console.log("In");

    const dataRow: any = new DataProvider((r1, r2) => r1 !== r2)
    const dataProvider = dataRow.cloneWithRows([])

    this._layoutProvider = new LayoutProvider(
      index => 1,
      (type, dim) => {
        dim.width = Constants.WINDOW_WIDTH
        dim.height = Constants.USER_ITEM_HEIGHT
        dim.height = dim.height - 20
      }
    )

    LayoutAnimations.enableAndroidLayoutAnimation()

    this.state = {
      users: [],
      searchUsers: [],
      refreshing: false,
      dataProvider,
      loadable: false,
      searching: false,
      isSearchMode: false,
      adminUser: null
    }
  }

  componentWillMount() {
    this.getAdminUser() //added by mahesh to set admin details.
    this._onRefresh()
  }
  /** added by mahesh to get/set admin details, to make working return to admin button. */
  getAdminUser() {
    const { userProfile } = getStore().getState()
    FirebaseHelper.findUserById(userProfile.id.toString())
      .then(snapshot => {
        const adminUser = snapshot[0] as FirebaseUser
        this.setState({
          adminUser
        })
      })
      .catch(error => {
        Navigator.showToast('Error', error.message, 'Error')
      })
  }


  _updateUser = () => { }

  _onPress = (user: FirebaseUser, returnToAdmin = false) => {
    const param = {
      user: user
    }
    Navigator.navTo('AdminChat', { param })

    // Navigator.showLoading()
    // Network.setToken(user.token)
    // const { itemList, categories, userItems } = getStore().getState().filters
    // let filterItemListParams = itemList
    // let filterCatoloriesParams = categories
    // let userItemsParams = userItems
    // let userProfile: UserProfile = null
    // let locality = ''
    // // @ts-ignore
    // returnToAdmin ? global.isAdminLogged = false : global.isAdminLogged = true;



    // Api.getUserDetail<UserProfile>()
    //   .then((response: any) => {
    //     const user = response.user
    //     if (!user) {
    //       throw Error('User not found')
    //     }
    //     userProfile = user
    //     return Utils.getAddress(user)
    //   })
    //   .then(address => {
    //     const { payment_customer_id, id, phone, email } = userProfile
    //     locality = address.locality
    //     const name = Utils.getUserName(userProfile)

    //     const body = {
    //       phone,
    //       name,
    //       email,
    //       metadata: {
    //         Address: address.address,
    //         'App Version': Device.getAppVersion(),
    //         'Code Push Version': Device.getCodePushVersion(),
    //         Device: Device.getDeviceInfo()
    //       }
    //     }

    //     if (payment_customer_id == null || payment_customer_id.length == 0) {
    //       return StripeHelper.createCustomerPaymentAccount(email, name, phone, body.metadata)
    //     } else {
    //       return StripeHelper.updateCustomerPaymentAccount(payment_customer_id, body)
    //     }
    //   })
    //   .then((paymentInfo: any) => {
    //     if (paymentInfo) {
    //       userProfile.payment_customer_id = paymentInfo.id
    //       PaymentActions.savePaymentInfo(paymentInfo)
    //       let paymentMethods = []
    //       if (paymentInfo.sources && Array.isArray(paymentInfo.sources.data) && paymentInfo.sources.total_count) {
    //         paymentMethods = paymentInfo.sources.data
    //         PaymentActions.saveAllPaymentMethods(paymentMethods)
    //       }
    //     }

    //     if (Validator.isURL(userProfile.photo) == false) {
    //       userProfile.photo = ''
    //     }

    //     UserProfileActions.saveUserProfile(userProfile)
    //     const {
    //       dress_size_id = null,
    //       id,
    //       longitude,
    //       latitude,
    //       payment_customer_id,
    //       payment_account_id,
    //       backup_size_id
    //     } = userProfile

    //     if (dress_size_id !== null) {
    //       filterItemListParams.filters['size'] = [dress_size_id]
    //     }

    //     if (typeof longitude === 'number' && typeof latitude === 'number') {
    //       filterItemListParams.filters['location'] = { longitude, latitude }
    //       if (typeof filterCatoloriesParams.filters === 'object') {
    //         filterCatoloriesParams.filters['location'] = { longitude, latitude }
    //       } else {
    //         filterCatoloriesParams.filters = {
    //           location: { longitude, latitude }
    //         }
    //       }
    //     }

    //     if (backup_size_id !== null) {
    //       if (Array.isArray(filterItemListParams.filters['size'])) {
    //         filterItemListParams.filters['size'].push(backup_size_id)
    //       } else {
    //         filterItemListParams.filters['size'] = [backup_size_id]
    //       }
    //     }

    //     userItemsParams.id = id
    //     userItemsParams.page = 1
    //     let promises = [
    //       Api.getItemByUser(userItemsParams),
    //       Api.getItemSummary(filterCatoloriesParams),
    //       Api.getNotificationSettings(),
    //       Api.getFavorites(),
    //       Api.getItemList(filterItemListParams)
    //     ]

    //     if (payment_account_id !== null && payment_account_id.length) {
    //       promises.push(StripeHelper.getPayoutAccount(payment_account_id))
    //     }

    //     return Promise.all(promises)
    //   })
    //   .then((values: any) => {
    //     const userItems = values[0] && values[0].code === 200 ? values[0].items : []
    //     const itemsSummary =
    //       values[1] && values[1].code === 200 && Array.isArray(values[1].categories) ? values[1].categories : []
    //     const settings = values[2] && values[2].code === 200 ? values[2].settings : null
    //     const favorites = values[3] && values[3].code === 200 ? values[3].favorites : null
    //     const itemList = values[4] && values[4].code === 200 && Array.isArray(values[4].items) ? values[4].items : []
    //     const payoutMethods = values[5] && Array.isArray(values[5].data) ? values[5].data : []
    //     UserItemActions.saveUserItems(userItems)
    //     ItemSummaryActions.saveItemSummary(itemsSummary)
    //     NotificationSettingsActions.saveNotificationSettings(settings)
    //     FavoritesActions.saveFavorites(favorites)
    //     ItemListActions.saveItemList(itemList)
    //     PayoutActions.saveAllPayoutMethods(payoutMethods)

    //     filterItemListParams.page = itemList.length ? 2 : 1
    //     userItemsParams.page = userItems.length ? 2 : 1

    //     FilterActions.updateFilters(filterItemListParams, filterCatoloriesParams, userItemsParams)

    //     Navigator.hideLoading()
    //     Navigator.navTo('Home')
    //   })
    //   .catch((error: any) => {
    //     Navigator.hideLoading()
    //     Navigator.showToast('Error', error.message, 'Error')
    //   })
  }

  _onLoadMore = () => {
    let { users, loadable, isSearchMode } = this.state

    if (isSearchMode) return

    if (loadable) {
      Navigator.showLoading()
      users = users.slice()
      const lastUser = users[users.length - 1]
      FirebaseHelper.getMoreUsers(lastUser.lastTimeLogged)
        .then(snapshot => {
          snapshot.docs.forEach(doc => {
            const data = doc.data() as FirebaseUser

            if (data.role !== 'admin') {
              users.push(data)
            }
          })
          const { dataProvider } = this.state

          this.setState({
            users,
            searchUsers: users,
            refreshing: false,
            dataProvider: dataProvider.cloneWithRows(users)
          })

          Navigator.hideLoading()
        })
        .catch(error => {
          this.setState({ refreshing: false })
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
        })
    }
  }

  _onRefresh = () => {
    const { isSearchMode } = this.state
    if (isSearchMode) return

    const text = this._inputRef.current ? this._inputRef.current.getText() : ''
    console.log('***** text', text)
    if (text !== '') {
      this._onChangeText(text)
    } else {
      this.setState({ refreshing: true })
      Navigator.showLoading()
      FirebaseHelper.getAllUsers()
        .then(snapshot => {
          let users = []
          snapshot.docs.forEach(doc => {
            const data = doc.data() as FirebaseUser

            if (data.role !== 'admin') {
              users.push(data)
            }
          })
          const { dataProvider } = this.state

          this.setState({
            users,
            searchUsers: users,
            refreshing: false,
            dataProvider: dataProvider.cloneWithRows(users)
          })

          Navigator.hideLoading()
        })
        .catch(error => {
          this.setState({ refreshing: false })
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
        })
    }
  }

  _searchDebounce = debounce((text: string) => {
    this.setState({ searching: true })
    FirebaseHelper.findUserById(text)
      .then(async (searchUsers: any) => {

        let users = []
        searchUsers.forEach(doc => {

          if (doc.role !== 'admin') {
            users.push(doc)
          }
        })
        const { dataProvider } = this.state

        this.setState({
          searchUsers: users,
          searching: false,
          dataProvider: dataProvider.cloneWithRows(users)
        })

        // this.setState({
        //   searchUsers,
        //   dataProvider: this.state.dataProvider.cloneWithRows(searchUsers),
        //   searching: false
        // })
      })
      .catch(error => {
        this.setState({ searchUsers: [], dataProvider: this.state.dataProvider.cloneWithRows([]), searching: false })
      })
    // const searchUsers = this.state.users.filter(item => {
  }, 300)

  _onChangeText = (text: string) => this._searchDebounce(text)

  _onMomentumScrollBegin = () => this.setState({ loadable: true })

  _onMomentumScrollEnd = () => this.setState({ loadable: false })

  _renderItem = (type, data, index) => {
    const { photo, userName, userId, lastTimeLogged, appVersion = 'Unknown', codePushVersion = 'Unknown' } = data
    const onPress = () => this._onPress(data)
    const source =
      typeof photo === 'string' && photo.length > 0 && Validator.isURL(photo)
        ? { uri: photo }
        : Assets.images.defaultProfile
    return (
      <View>
        <Row style={styles.rowItem} alignVertical="center" onPress={onPress}>
          <Avatar
            loadingStyle={{ borderRadius: 25, width: 50, height: 50 }}
            style={{
              width: 50,
              height: 50,
              borderRadius: 25
            }}
            imageStyle={{
              width: 50,
              height: 50,
              borderRadius: 25,
              borderColor: Assets.colors.borderColorLight,
              borderWidth: 1
            }}
            defaultImageSource={source}
            editable={false}
          />
          <Col flex={1} alignHorizontal="flex-start">

            <Row style={styles.row} alignHorizontal="space-between" alignVertical="center">
              <Text style={styles.title} text={userName} />
              {/* <Text style={styles.subTitle} text={`User ID: ${userId}`} />
              <View style={{ flex: 1 }} />
              <TimeAgo style={styles.timeText} time={lastTimeLogged} /> */}
            </Row>
            {/* <Row style={[styles.row, { width: '100%' }]} alignHorizontal="space-between" alignVertical="center"> */}
            {/* <Text style={styles.smallTitle}>
                App Version:
                <Text style={{ fontFamily: Assets.fonts.display.bold }} text={` ${appVersion}`} />
              </Text>

              <Text style={[styles.smallTitle, { marginRight: 10 }]}>
                CodePush Version:
                <Text style={{ fontFamily: Assets.fonts.display.bold }} text={` ${codePushVersion}`} />
              </Text> */}
            {/* </Row> */}
          </Col>
          <Image style={{ tintColor: Assets.colors.textLight }} source={Assets.images.categoryDetailsArrow} />
        </Row>
      </View>
    )
  }

  render() {
    const { searching } = this.state
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header
          statusBarProps={{ barStyle: 'dark-content' }}
          title="User List"
          titleStyle={Styles.headerTitle}
          LeftComponent={<BackButton />}
        />
        <TextInput
          ref={this._inputRef}
          LeftComponent={
            <Image
              style={{ marginHorizontal: 8, tintColor: Assets.colors.textLight }}
              source={Assets.images.searchIcon}
            />
          }
          placeholder="Search"
          style={styles.inputView}
          inputStyle={styles.inputText}
          underlineWidth={0}
          placeholderTextColor={Assets.colors.mainText}
          returnKeyType="search"
          onChangeText={this._onChangeText}
          onBlur={() => {
            console.log('**** onBlur')
            this.setState({ isSearchMode: false })
          }}
          onFocus={() => {
            console.log('**** onFocus')
            this.setState({ isSearchMode: true })
          }}
          onEndEditing={() => {
            console.log('**** onEndEditing')
            this.setState({ isSearchMode: false })
          }}
          RightComponent={<ActivityIndicator style={{ marginHorizontal: 10 }} hidesWhenStopped animating={searching} />}
        />
        <View style={{ flex: 1 }}>
          <RecyclerListView
            scrollViewProps={{
              keyboardShouldPersistTaps: 'handled',
              showsVerticalScrollIndicator: false,
              showsHorizontalScrollIndicator: false,
              decelerationRate: 'normal',
              refreshControl: <RefreshControl onRefresh={this._onRefresh} refreshing={this.state.refreshing} />,
              onMomentumScrollBegin: this._onMomentumScrollBegin,
              onMomentumScrollEnd: this._onMomentumScrollEnd
            }}
            dataProvider={this.state.dataProvider}
            onEndReached={this._onLoadMore}
            onEndReachedThreshold={1}
            layoutProvider={this._layoutProvider}
            rowRenderer={this._renderItem}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  rowItem: {
    marginHorizontal: 20,
    backgroundColor: Assets.colors.inputBg,
    borderRadius: 6,
    paddingHorizontal: 10,
    paddingVertical: 8,
    // marginTop:0,
    // marginBottom:0
  },
  title: {
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    color: Assets.colors.mainText,
    lineHeight: 24,
    marginLeft: 17,
    marginRight: 15
  },
  row: {
    marginTop: 6,
    marginLeft: 17,
    marginRight: 15
  },
  subTitle: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2,
    lineHeight: 18
  },
  smallTitle: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 10,
    color: Assets.colors.textLight,
    letterSpacing: -0.2,
    lineHeight: 18
  },
  inputView: {
    marginHorizontal: 20,
    marginBottom: 20,
    height: 36,
    backgroundColor: Assets.colors.headerSearchBarBg,
    borderRadius: 6
  },
  inputText: {
    marginRight: 10,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    marginTop: 3,
    height: 36,
    paddingVertical: 0
  },
  emptyText: {
    textAlign: 'center',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2,
    lineHeight: 18,
    marginHorizontal: 20
  },
  button: {
    marginBottom: Device.isIphoneX() ? 20 : 0,
    borderRadius: 0
  },
  timeText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 12,
    color: Assets.colors.textLight
  }
})
export default UserList
