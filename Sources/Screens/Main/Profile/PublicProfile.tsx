import React from 'react'
import { View, Image, TouchableOpacity, StyleSheet, StatusBar } from 'react-native'
import { EmptyView, BackButton, AsyncImage } from '@Components'
import { connect } from 'react-redux'
import { StoreState } from '@ReduxManager'
import { UserProfile, Item } from '@Models'
import { Header, Icon, Text, Row } from 'rn-components'
import { Api, FirebaseHelper } from '@Services'
import { Navigator } from '@Navigation'
import { LayoutAnimations, Communication, Validator, DateTime } from '@Utils'
import { NavigationProps } from '@Types'
import Assets from '@Assets'
import Styles from '@Styles'
import StarRating from 'react-native-star-rating'

import CategoryGrid from '../Home/Category/CategoryGrid'
import branch, { BranchEvent } from 'react-native-branch'
import { array } from 'prop-types'
type Props = {
  userProfile: UserProfile
  navigation: NavigationProps
}

type State = {
  otherUserProfile: UserProfile
  items: any[]
  pageIndex: number
  userId: number
  onItemAdded?: () => void
  selectedItems: Array<Item>
  selectedDates: Array<string>
  fromMultiItemScreen: boolean
}

class PublicProfile extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    const user = this.props.navigation.getParam('user', null)
    const onItemAdded = this.props.navigation.getParam('onItemAdded', null)
    const selectedItems = this.props.navigation.getParam('selectedItems', [])
    const selectedDates = this.props.navigation.getParam('selectedDates', [])
    const fromMultiItemScreen = this.props.navigation.getParam('fromMultiItemScreen', false)
    this.state = {
      otherUserProfile: null,
      items: [],
      pageIndex: 1,
      userId: user ? user.id : null,
      onItemAdded,
      selectedItems,
      selectedDates,
      fromMultiItemScreen
    }

    LayoutAnimations.enableAndroidLayoutAnimation()
  }

  componentWillMount() {
    StatusBar.setBarStyle('dark-content')
    const { userId, selectedItems } = this.state

    if (userId !== null) {
      Navigator.showLoading()
      FirebaseHelper.logEvent('ViewProfile', { user: userId })
      let user = null
      let itemList = []

      Promise.all([Api.getOtherUserDetail(userId), Api.getItemByUser({ id: userId, page: 1, per_page: 20 })])
        .then((values: any[]) => {
          const [v1, v2] = values
          user = v1.user
          itemList = v2.items ? v2.items : []

          if (selectedItems.length && itemList.length) {
            return true
          }
          return false
        })
        .then(async next => {
          if (next) {
            const items = await this._filterValidItem(itemList)
            Navigator.hideLoading()
            LayoutAnimations.setLayoutAnimation(LayoutAnimations.PresetEaseInOut)
            this.setState({ items, otherUserProfile: user, pageIndex: 1 })
          } else {
            console.log('**** itemList 3', itemList)
            Navigator.hideLoading()
            LayoutAnimations.setLayoutAnimation(LayoutAnimations.PresetEaseInOut)
            this.setState({ items: itemList, otherUserProfile: user, pageIndex: 1 })
          }
        })
        .catch(error => {
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
        })
    } else {
      Navigator.showToast('Error', '(PP - 63) Internal Error', 'Error', 3000, () => null, () => Navigator.back(), true)
    }
  }

  _filterValidItem = async itemList => {
    const { userId, selectedItems, selectedDates } = this.state
    let newItemList = itemList
    if (selectedItems.length) {
      const ids = selectedItems.map(item => item.id)
      newItemList = itemList.filter(item => {
        const unavailable_dates =
          Array.isArray(item.unavailable_dates) && item.unavailable_dates.length
            ? item.unavailable_dates.map(item => item.date)
            : []

        const found = unavailable_dates.some(r => selectedDates.includes(r))
        const notContainer = ids.includes(item.id) == false && found == false

        console.log('**** notContainer', notContainer, item)
        return notContainer
      })

      let promises = []
      newItemList.forEach(item => {
        promises.push(FirebaseHelper.getItemDetail(this.props.userProfile, item))
      })

      const values = await Promise.all(promises)

      values.forEach((value, index) => {
        if (value.size > 0) {
          let conversations: any[] = []
          value.docs.forEach(doc => {
            const data = doc.data()
            conversations.push(data)
          })

          conversations.sort((chat1, chat2) => {
            if (chat1.createdAt > chat2.createdAt) return -1
            if (chat1.createdAt < chat2.createdAt) return 1
          })

          const data = conversations[0]
          const status = data.status

          const isValid =
            status === 'inquiry' ||
            status === 'returned' ||
            status === 'reviewed' ||
            status === 'rental_complete' ||
            status === 'cancelled' ||
            status === 'declined' ||
            status === 'completed' ||
            status === 'auto_cancel'

          if (!isValid) {
            newItemList = newItemList.filter(item => item.id != data.itemId)
          }
        }
      })
    }

    return newItemList
  }

  _onTapShareItem = async () => {
    // const { otherUserProfile } = this.state;
    // if (otherUserProfile) {
    //   Navigator.showLoading();
    //   BranchHelper.createSharedUserURL(otherUserProfile)
    //     .then(url => {
    //       Navigator.hideLoading();
    //       Share.open({
    //         title: "Rent this look!",
    //         message: "",
    //         subject: "",
    //         url: url
    //       });
    //     })
    //     .catch(error => {
    //       Navigator.hideLoading();
    //       Navigator.showToast("Error", error.message, "Error");
    //     });
    // } else {
    //   Navigator.showToast("Error", "(PP - 92) Internal Error", "Error");
    // }

    Navigator.showLoading()
    const { otherUserProfile } = this.state
    const { id = '', firstname = '', lastname = '', email = '', photo = '' } = otherUserProfile
    const url = `shareProfile/${id}`
    const buo = await branch.createBranchUniversalObject(url, {
      locallyIndex: true,
      canonicalUrl: '',
      title: 'Rent this look!',
      contentImageUrl: Validator.isURL(photo) ? photo : '',
      contentMetadata: {
        customMetadata: {
          user: JSON.stringify(otherUserProfile),
          type: 'Share Profile'
        }
      }
    })
    new BranchEvent('RMW_Share_Profile', buo, { sharedBy: id, firstname, lastname, email }).logEvent()

    const { channel, completed, error } = await buo.showShareSheet(
      {
        emailSubject: '',
        messageBody: '',
        messageHeader: 'Rent this look!'
      },
      {
        feature: 'share',
        channel: 'RMW_Share_Profile'
      },
      {
        $desktop_url: 'https://itunes.apple.com/us/app/rent-my-wardrobe/id1375748670?ls=1&mt=8',
        $ios_deepview: 'branch_default'
      }
    )

    Navigator.hideLoading()
    if (error) {
      console.error('Error sharing via Branch: ' + error)
      return
    }

    console.log('Share to ' + channel + ' completed: ' + completed)
  }

  _renderProfileDetails = () => {
    const { otherUserProfile, items } = this.state
    if (!otherUserProfile) {
      return null
    }
    const { firstname, lastname, website, facebook, twitter, instagram, about_me } = otherUserProfile
    const rating = otherUserProfile.rating_renter ? otherUserProfile.rating_renter : 0

    const onPress = website ? () => Communication.web(website) : null
    const showSocial =
      website !== '' || (facebook && facebook !== '') || (instagram && instagram !== '') || (twitter && twitter !== '')
    return (
      <View style={{ marginTop: 12, marginBottom: 10 }}>
        <Row alignVertical="center">
          <AsyncImage
            style={{ marginLeft: 15, borderRadius: 35, overflow: 'hidden' }}
            source={{ uri: otherUserProfile.photo }}
            width={70}
            height={70}
            placeholderSource={Assets.images.defaultProfile}
            fallbackImageSource={Assets.images.defaultProfile}
          />
          <View style={{ marginLeft: 17 }}>
            <Text numberOfLines={1} style={styles.profileName} text={firstname + ' ' + lastname} />
            <Row alignVertical="center">
              <StarRating
                disabled={true}
                maxStars={5}
                rating={rating}
                emptyStar={Assets.images.ratingEmptyStar}
                fullStar={Assets.images.ratingFullStar}
                halfStar={Assets.images.ratingHalfStar}
                halfStarEnabled={true}
                starSize={20}
              />
              <Text style={styles.averageText} text="Avg." />
            </Row>
          </View>
        </Row>
        {about_me !== '' && <Text style={styles.bioText} text={about_me} />}
        {showSocial ? (
          <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'center', height: 32 }}>
            <View style={{ flex: 1 }}>
              <Text numberOfLines={1} style={styles.website} onPress={onPress} text={website} />
            </View>
            <View style={{ flexDirection: 'row', marginRight: 15 }}>
              {this._renderSocialMediaIcon(facebook, '', Assets.images.profile_facebook)}
              {this._renderSocialMediaIcon(instagram, 'https://www.instagram.com/', Assets.images.profile_instagram)}
              {this._renderSocialMediaIcon(twitter, 'https://twitter.com/', Assets.images.profile_twitter)}
            </View>
          </View>
        ) : null}
      </View>
    )
  }

  _renderSocialMediaIcon = (url: string, urlPrefix: string, imageIcon: any) => {
    if (!url || url === '') {
      return null
    }
    return (
      <TouchableOpacity onPress={() => Communication.web(urlPrefix + url)}>
        <View style={{ width: 40, height: 32, alignItems: 'flex-end', justifyContent: 'center' }}>
          <Image source={imageIcon} />
        </View>
      </TouchableOpacity>
    )
  }

  _keyExtractor = (item, index) => '' + item.id

  _onRefresh = () => {
    return new Promise<boolean>((resolve, reject) => {
      const { userId } = this.state
      if (!userId) {
        resolve(false)
      } else {
        const param = { id: userId, page: 1, per_page: 20 }
        Api.getItemByUser(param)
          .then((response: any) => {
            if (response.code !== 200) throw Error(response.message)

            const itemList = Array.isArray(response.items) ? response.items : []
            if (itemList.length) {
              param.page = 2
            }

            LayoutAnimations.setLayoutAnimation(LayoutAnimations.ListItem)
            this.setState({ items: itemList, pageIndex: param.page })
            resolve(true)
          })
          .catch(error => {
            reject(error)
            Navigator.showToast('Error', error.message, 'Error')
          })
      }
    })
  }

  _onLoadMore = () => {
    return new Promise<boolean>((resolve, reject) => {
      const { userId, pageIndex } = this.state
      if (!userId) {
        resolve(false)
      } else {
        let param = { id: userId, page: pageIndex + 1, per_page: 20 }
        Api.getItemByUser(param)
          .then(async (response: any) => {
            if (response.code !== 200) throw Error(response.message)

            const itemList = Array.isArray(response.items) ? response.items : []

            if (itemList.length) {
              const newItems = await this._filterValidItem(itemList)

              const items = this.state.items.concat(newItems)
              this.setState({ items, pageIndex: param.page })
            }

            resolve(true)
          })
          .catch(error => {
            reject(error)
            Navigator.showToast('Error', error.message, 'Error')
          })
      }
    })
  }

  _onItemSelected = (item: Item) => {
    const { onItemAdded } = this.state
    Navigator.navTo('Details', { item, onItemAdded })
  }

  _renderContent = () => {
    const { onItemAdded, items } = this.state
    if (items.length) {
      return (
        <CategoryGrid
          showEmptyView={false}
          data={items}
          onRefresh={this._onRefresh}
          onLoadMore={this._onLoadMore}
          emptyViewProps={{
            titleImage: Assets.images.profle_wardrobe_empty_icon,
            title: 'No wardrobe yet',
            info: 'When Eleanor adds clothes for rent, you’ll see their wardrobe here.'
          }}
          itemCellProps={{ editable: false }}
          onItemSelected={typeof onItemAdded === 'function' ? this._onItemSelected : null}
        />
      )
    }

    return (
      <EmptyView
        titleImage={Assets.images.profle_wardrobe_empty_icon}
        title="No wardrobe yet"
        info="When Eleanor adds clothes for rent, you’ll see their wardrobe here."
      />
    )
  }
  render() {
    const { otherUserProfile, fromMultiItemScreen } = this.state
    let title = ''
    if (otherUserProfile) {
      if (otherUserProfile.user_name && otherUserProfile.user_name !== '') {
        title = otherUserProfile.user_name + "'s Closet"
      } else {
        title = otherUserProfile.firstname + ' ' + otherUserProfile.lastname + "'s Closet"
      }
    }

    return (
      <View style={StyleSheet.absoluteFill}>
        <Header
          title={title}
          titleStyle={Styles.headerTitle}
          statusBarProps={{ barStyle: 'dark-content' }}
          LeftComponent={
            <BackButton iconSource={fromMultiItemScreen ? Assets.images.closeIcon : Assets.images.backIcon} />
          }
          rightContainerStyle={{ alignItems: 'flex-end' }}
          RightComponent={<Icon iconSource={Assets.images.shareIcon} onPress={this._onTapShareItem} />}
        />
        {this._renderProfileDetails()}
        {this._renderContent()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  profileName: {
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    color: Assets.colors.mainText,
    lineHeight: 24
  },

  website: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.appTheme,
    letterSpacing: -0.2,
    lineHeight: 18,
    marginTop: 6,
    marginLeft: 17,
    marginRight: 15
  },

  bioText: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2,
    lineHeight: 18,
    marginTop: 14,
    marginLeft: 15,
    marginRight: 15
  },

  averageRatingView: {
    flexDirection: 'row',
    marginLeft: 17,
    marginTop: 2,
    alignItems: 'center'
  },

  averageText: {
    marginLeft: 10,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.2,
    lineHeight: 22
  }
})

const mapStateToProps = (state: StoreState) => {
  return {
    userProfile: state.userProfile
  }
}

export default connect(mapStateToProps)(PublicProfile)
