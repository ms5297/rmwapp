import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Header, Text } from 'rn-components'
import { BackButton } from '@Components'
import { NavigationProps } from '@Types'
import Styles from '@Styles'
import Assets from '@Assets'
import { Navigator } from '@Navigation'
import { Api, StripeHelper, FirebaseHelper } from '@Services'
import { getStore, UserProfileActions, PaymentActions, PayoutActions } from '@ReduxManager'

type Props = {
  navigation: NavigationProps
}

class PayoutMethodsView extends React.Component<Props> {
  _onDelete = () => {
    Navigator.showAlert(
      'Delete bank account',
      'Are you sure you want to delete this bank account?',
      () => null,
      () => {
        Navigator.showLoading()
        const { userProfile } = getStore().getState()
        console.log('***** userProfile.payment_account_id', userProfile, userProfile.payment_account_id)
        Api.canDeleteBank()
          .then((response: any) => {
            if (!response.can_remove_bank) {
              throw Error(
                'There is a rental in progress, you can not remove this card until the rental is canceled or complete.'
              )
            }
            return StripeHelper.deleteBankAccount(userProfile.payment_account_id)
          })
          .then((response: any) => {
            if (response.error == null && response.deleted == true) {
              return Api.editUserProfile({ payment_account_id: null })
            } else {
              throw Error('Could not delete the account.')
            }
          })
          .then((response: any) => {
            if (response.code !== 200) {
              throw Error(response.message || '(PMV - 44) Internal Error')
            }
            PayoutActions.deletePayoutMethod()
            UserProfileActions.saveUserProfile(response.user)
            FirebaseHelper.logEvent('PayoutMethodDeleted')

            Navigator.hideLoading()
            Navigator.showToast(
              'Success',
              'Account has been deleted.',
              'Success',
              3000,
              () => null,
              () => {
                Navigator.back()
              },
              true
            )
          })
          .catch(error => {
            Navigator.hideLoading()
            Navigator.showToast('Error', error.message, 'Error')
          })
      },
      'CANCEL',
      'OK'
    )
  }

  render() {
    const { params } = this.props.navigation.state

    const values = [
      { title: 'Account Holder Name', value: params.data.account_holder_name },
      { title: 'Account Number', value: '*********' + params.data.last4 },
      { title: 'Routing Number', value: params.data.routing_number },
      { title: 'Country', value: params.data.country },
      { title: 'Currency', value: params.data.currency }
    ]
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header title="Bank Account" titleStyle={Styles.headerTitle} LeftComponent={<BackButton />} />
        <View style={{ flex: 1 }}>
          {values.map((data, idnex) => (
            <View style={{ height: 70 }} key={data.title}>
              <Text style={styles.keyTitleText} text={data.title} />
              <Text style={styles.keyValueText} text={data.value} />
            </View>
          ))}
          <View style={styles.seperator} />
          <Text style={styles.deleteText} text="Delete Account" onPress={this._onDelete} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    marginHorizontal: 20,
    marginTop: 20
  },
  keyTitleText: {
    fontSize: 12,
    color: Assets.colors.textLight,
    fontFamily: Assets.fonts.text.regular,
    marginTop: 15,
    marginLeft: 15
  },
  deleteText: {
    marginTop: 30,
    alignSelf: 'flex-end',
    marginRight: 15,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14
  },
  seperator: {
    marginHorizontal: 10,
    marginTop: 20,
    marginBottom: 20,
    height: 0.5,
    backgroundColor: Assets.colors.borderColor
  },
  keyValueText: {
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    marginTop: 5,
    marginLeft: 15
  }
})

export default PayoutMethodsView
