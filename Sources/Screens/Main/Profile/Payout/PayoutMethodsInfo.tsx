import React from 'react'
import { View } from 'react-native'
import { EmptyView, BackButton } from '@Components'
import { Header, Icon } from 'rn-components'
import { Navigator } from '@Navigation'
import { NavigationProps } from '@Types'
import Styles from '@Styles'
import Assets from '@Assets'

type Props = {
  navigation: NavigationProps
}
class PayoutMethodsInfo extends React.Component<Props> {
  _onPress = () => {
    const isForceUpdate = this.props.navigation.getParam('isForceUpdate', false)
    Navigator.navTo('PayoutAccountInfo', { isForceUpdate })
  }
  render() {
    const shouldShowBackIcon = this.props.navigation.getParam('isForceUpdate', false)
    const iconSource = shouldShowBackIcon ? Assets.images.backIcon : Assets.images.closeIcon
    return (
      <View>
        <Header
          LeftComponent={<BackButton iconSource={iconSource} />}
          title="Bank Transfer"
          titleStyle={Styles.headerTitle}
        />
        <EmptyView
          title="Get paid in 5-7 days"
          info="We need your checking account details to send you money. Payouts are sent 2 days after the rental checkout has been completed."
        />
        <Icon
          style={{ top: 400, right: 10, position: 'absolute' }}
          size={32}
          iconSource={Assets.images.next_round}
          onPress={this._onPress}
        />
      </View>
    )
  }
}

export default PayoutMethodsInfo
