import React from 'react'
import { View, Image, TouchableOpacity, StyleSheet } from 'react-native'
import { StackActions } from 'react-navigation'
import { connect } from 'react-redux'
import { Header, Text, ScrollView } from 'rn-components'
import { BackButton, Button } from '@Components'
import { DateTime, Device } from '@Utils'
import { NavigationProps } from '@Types'
import { StripeHelper, Api, FirebaseHelper } from '@Services'
import { Navigator } from '@Navigation'
import { UserProfileActions, PayoutActions } from '@ReduxManager'
import Styles from '@Styles'
import Assets from '@Assets'

const isIphoneX = Device.isIphoneX()

type Props = {
  navigation: NavigationProps
}
class PayoutMethodsConfirm extends React.Component<Props> {
  _onDone = () => {
    const isForceUpdate = this.props.navigation.getParam('isForceUpdate', false)
    Navigator.navTo('ActionFinal', {
      title: 'Thanks for adding your payout method',
      description:
        'Now we’re making sure your info is correct. Look out for an email with a status update in the next 2 days.',
      buttonTitle: isForceUpdate ? 'Done' : 'Manage Payout Methods',
      onActionButton: () => {
        Navigator.pop(isForceUpdate ? 5 : 4, true)
        Navigator.back()
      }
    })
  }

  _onSave = () => {
    Navigator.showLoading()
    const data = this.props.navigation.getParam('data')

    console.log('***** data', data)
    const payload = {
      type: 'custom',
      country: 'US',
      default_currency: 'usd',
      tos_acceptance: data.tos_acceptance,

      external_account: {
        account_holder_name: data.first_name + ' ' + data.last_name,
        routing_number: data.routing_number,
        account_number: data.account_number,
        currency: 'usd',
        country: 'US',
        object: 'bank_account',
        account_holder_type: 'individual'
      },
      legal_entity: {
        ssn_last_4: data.last_4_SSN,
        type: 'individual',
        first_name: data.first_name,
        last_name: data.last_name,
        dob: {
          day: DateTime.moment(data.dob).date(),
          month: DateTime.moment(data.dob).month() + 1,
          year: DateTime.moment(data.dob).year()
        },
        address: {
          city: data.city,
          country: 'US',
          line1: data.addressLine1,
          line2: data.addressLine2,
          postal_code: data.zip,
          state: data.state
        },
        personal_address: {
          city: data.city,
          country: 'US',
          line1: data.addressLine1,
          line2: data.addressLine2,
          postal_code: data.zip,
          state: data.state
        }
      }
    }

    StripeHelper.createCustomerPayoutAccount(payload)
      .then((response: any) => {
        if (response.error) {
          throw Error(response.error.message || 'Stripe Error')
        }
        return response
      })
      .then((response: any) => {
        const account = response.external_accounts.data[0]
        PayoutActions.saveAllPayoutMethods([account])
        return Api.editUserProfile({ payment_account_id: response.id })
      })
      .then((response: any) => {
        if (response.code !== 200) {
          throw Error(response.message || '(PO - 95) Internal Error')
        }

        UserProfileActions.saveUserProfile(response.user)
        FirebaseHelper.logEvent('PayoutMethodAdded')

        Navigator.hideLoading()
        Navigator.showToast(
          'Success',
          'New bank account has been added',
          'Success',
          3000,
          () => null,
          this._onDone,
          true
        )
      })
      .catch(error => {
        console.warn(error)
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  render() {
    const { params } = this.props.navigation.state
    let data = params.data
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header titleStyle={Styles.headerTitle} title="Bank Transfer" LeftComponent={<BackButton />} />
        <ScrollView style={styles.content} showsVerticalScrollIndicator={false}>
          <Text style={styles.titleText} text="Confirm account info" />
          <View style={{ height: 70 }}>
            <Text style={styles.keyTitleText} text="Name" />
            <Text style={styles.keyValueText} text={`${data.first_name} ${data.last_name}`} />
          </View>
          <View style={{ height: 70 }}>
            <Text style={styles.keyTitleText} text="Date of Birth" />
            <Text style={styles.keyValueText} text={DateTime.moment(data.dob).format('MMM-DD-YYYY')} />
          </View>
          <View style={{ height: 70 }}>
            <Text style={styles.keyTitleText} text="SSN" />
            <Text style={styles.keyValueText} text={`**** **** **** ${data.last_4_SSN}`} />
          </View>
          <View style={{ height: 70 }}>
            <Text style={styles.keyTitleText} text="Account Number" />
            <Text style={styles.keyValueText} text={data.account_number} />
          </View>
          <View style={{ height: 70 }}>
            <Text style={styles.keyTitleText} text="Routing Number" />
            <Text style={styles.keyValueText} text={data.routing_number} />
          </View>
          <View style={{ height: 70 }}>
            <Text style={styles.keyTitleText} text="Address" />
            <Text
              style={styles.keyValueText}
              text={`${data.addressLine1} ${data.addressLine2} ${data.city} ${data.state} ${data.zip}`}
            />
          </View>

          <Text
            style={styles.disclaimerText}
            text="When you add this account, a 48 hour verification process will begin. Once verified you will be able to receive payouts."
          />
        </ScrollView>
        <Button style={styles.button} textStyle={{ fontSize: 18 }} text="Confirm Account" onPress={this._onSave} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    marginHorizontal: 20,
    marginTop: 20
  },

  keyTitleText: {
    fontSize: 12,
    color: Assets.colors.textLight,
    fontFamily: Assets.fonts.text.regular,
    marginTop: 15,
    marginLeft: 15
  },
  keyValueText: {
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    marginTop: 5,
    marginLeft: 15
  },

  button: {
    borderRadius: 0,
    marginBottom: isIphoneX ? 20 : 0
  },

  disclaimerText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    textAlign: 'center',
    color: Assets.colors.textLight,
    marginVertical: 30
  },
  titleText: {
    fontSize: 24,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.display.bold,
    letterSpacing: -0.2,
    lineHeight: 28,
    marginHorizontal: 15,
    textAlign: 'center',
    marginBottom: 35
  }
})

export default PayoutMethodsConfirm
