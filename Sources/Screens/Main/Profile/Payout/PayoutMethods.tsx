import React from 'react'
import { View, Image, StyleSheet } from 'react-native'
import { EmptyView, BackButton, FlatList } from '@Components'
import { Header, Text, Col, Row } from 'rn-components'
import { connect } from 'react-redux'
import { StoreState } from '@ReduxManager'
import { StripePayoutMethods, StripePayoutMethod } from '@Models'
import { Navigator } from '@Navigation'

import Assets from '@Assets'
import Styles from '@Styles'

type Props = {
  payoutMethods: StripePayoutMethods
}

class PayoutMethods extends React.Component<Props> {
  _navigateToPaymentMethodInfo = () => Navigator.navTo('PayoutMethodsInfo', {})

  _navigateToPaymentMethodView = (data: StripePayoutMethod) => Navigator.navTo('PayoutMethodsView', { data })

  _renderListItem = ({ item }: { item: StripePayoutMethod }) => {
    const { last4 } = item
    const onPress = () => this._navigateToPaymentMethodView(item)
    let title = 'Bank Account'
    let value = '*****' + last4

    return (
      <Row onPress={onPress} style={styles.row} alignHorizontal="space-between" alignVertical="center">
        <Col>
          <Text style={styles.itemTitle} text={title} />
          <Text style={styles.accountNumber} text={value} />
        </Col>
        <Image source={Assets.images.categoryDetailsArrow} style={{ tintColor: Assets.colors.borderColor }} />
      </Row>
    )
  }
  render() {
    const { payoutMethods } = this.props
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header title="Payout Methods" titleStyle={Styles.headerTitle} LeftComponent={<BackButton />} />
        <FlatList
          style={{ flex: 1, marginTop: 40 }}
          data={payoutMethods}
          renderItem={this._renderListItem}
          ListEmptyComponent={
            <EmptyView
              title="No payout methods yet"
              info="To receive money for your rentals add your bank account."
              buttonText="Add Bank Account"
              onPress={this._navigateToPaymentMethodInfo}
            />
          }
          showsVerticalScrollIndicator={false}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    marginHorizontal: 20,
    marginTop: 20
  },

  row: {
    marginHorizontal: 25,
    paddingVertical: 20,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: Assets.colors.borderColor
  },
  itemTitle: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    color: Assets.colors.mainText
  },

  accountNumber: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    marginTop: 10
  }
})

const mapStateToProps = (state: StoreState) => {
  return {
    payoutMethods: state.payouts ? state.payouts.payoutMethods : [],
    userProfile: state.userProfile
  }
}

export default connect(mapStateToProps)(PayoutMethods)
