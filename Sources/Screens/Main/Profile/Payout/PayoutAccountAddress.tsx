import React from 'react'
import { View, Image, StyleSheet } from 'react-native'
import { Header, ScrollView, Text, Icon, InputGroup, TextInput, Row } from 'rn-components'
import { BackButton } from '@Components'
import { Navigator } from '@Navigation'
import { NavigationProps } from '@Types'
import ProgressCircle from 'react-native-progress-circle'
import Assets from '@Assets'
import Styles from '@Styles'

type Props = {
  navigation: NavigationProps
}
class PayoutAccountAddress extends React.Component<Props> {
  _inputGroupRef1 = React.createRef<InputGroup>()
  _inputGroupRef2 = React.createRef<InputGroup>()
  _inputZipCodeRef = React.createRef<TextInput>()

  componentDidMount() {
    this._inputGroupRef1.current.focus(0)
  }

  _onConfirm = () => {
    const [street, address] = this._inputGroupRef1.current.getAllText()
    const [city, state] = this._inputGroupRef2.current.getAllText()
    const zip = this._inputZipCodeRef.current.getText()

    if (street.length == 0) {
      Navigator.showToast('Error', 'Enter street address', 'Error')
      return
    }

    if (city.length == 0) {
      Navigator.showToast('Error', 'Enter city', 'Error')
      return
    }
    if (state.length == 0) {
      Navigator.showToast('Error', 'Enter state', 'Error')
      return
    }
    if (zip.length == 0) {
      Navigator.showToast('Error', 'Enter zip code', 'Error')
      return
    }
    const { params } = this.props.navigation.state

    const data = {
      addressLine1: street,
      addressLine2: address,
      city: city,
      state: state,
      zip: zip
    }
    const updatedData = Object.assign(data, params.data)
    const isForceUpdate = this.props.navigation.getParam('isForceUpdate', false)
    Navigator.navTo('PayoutMethodsConfirm', { data: updatedData, isForceUpdate })
  }

  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header titleStyle={Styles.headerTitle} title="Bank Transfer" LeftComponent={<BackButton />} />
        <ScrollView
          style={styles.content}
          keyboardShouldPersistTaps="handled"
          autoScrollNextInput
          showsVerticalScrollIndicator={false}>
          <Text style={styles.titleText} text="Address for the account" />
          <View style={{ marginHorizontal: 25 }}>
            <InputGroup
              ref={this._inputGroupRef1}
              spacing={15}
              onInputSubmit={() => this._inputGroupRef2.current.focus(0)}>
              <TextInput
                style={styles.inputContainer}
                clearButtonMode="while-editing"
                inputStyle={styles.input}
                placeholderTextColor={Assets.colors.placeholder}
                selectionColor={Assets.colors.mainText}
                autoCapitalize="words"
                placeholder="Street address"
                underlineWidth={0}
              />
              <TextInput
                style={styles.inputContainer}
                clearButtonMode="while-editing"
                inputStyle={styles.input}
                placeholderTextColor={Assets.colors.placeholder}
                selectionColor={Assets.colors.mainText}
                autoCapitalize="words"
                placeholder="Apt, Suite, etc"
                underlineWidth={0}
                returnKeyType="next"
              />
            </InputGroup>
            <InputGroup
              ref={this._inputGroupRef2}
              style={styles.inputRow}
              onInputSubmit={() => this._inputZipCodeRef.current.focus()}>
              <TextInput
                style={[styles.inputContainer, { flex: 0.65 }]}
                clearButtonMode="while-editing"
                inputStyle={styles.input}
                placeholderTextColor={Assets.colors.placeholder}
                selectionColor={Assets.colors.mainText}
                autoCapitalize="words"
                placeholder="City"
                underlineWidth={0}
              />
              <TextInput
                style={[styles.inputContainer, { flex: 0.3 }]}
                clearButtonMode="while-editing"
                inputStyle={styles.input}
                placeholderTextColor={Assets.colors.placeholder}
                selectionColor={Assets.colors.mainText}
                autoCapitalize="words"
                placeholder="State"
                underlineWidth={0}
                returnKeyType="next"
              />
            </InputGroup>
            <TextInput
              ref={this._inputZipCodeRef}
              style={[styles.inputContainer]}
              clearButtonMode="while-editing"
              inputStyle={styles.input}
              placeholderTextColor={Assets.colors.placeholder}
              selectionColor={Assets.colors.mainText}
              autoCapitalize="words"
              placeholder="Zip Code"
              underlineWidth={0}
              keyboardType="number-pad"
            />
          </View>
          <Row style={styles.footer} alignHorizontal="space-between" alignVertical="center">
            <ProgressCircle
              percent={80}
              radius={25}
              borderWidth={2}
              color={Assets.colors.appTheme}
              shadowColor={Assets.colors.borderColor}
              bgColor="#fff">
              <Image style={{ tintColor: Assets.colors.placeholder }} source={Assets.images.addCircle} />
            </ProgressCircle>
            <Row alignVertical="center">
              <Text style={styles.nextInfoText} text="Next Step: Confirmation" />
              <Icon
                iconStyle={styles.nextButtonImage}
                iconSource={Assets.images.next_round}
                onPress={this._onConfirm}
              />
            </Row>
          </Row>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    alignContent: 'center'
  },
  inputRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 15
  },
  inputContainer: {
    backgroundColor: 'white',
    borderColor: Assets.colors.borderColorLight,
    borderWidth: 0.5,
    borderRadius: 6,
    height: 44
  },
  input: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    height: 44,
    marginLeft: 10,
    paddingVertical: 0,
    marginVertical: 0
  },
  tickMarkView: {
    width: 24,
    height: 24,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
    borderWidth: 0.5,
    borderColor: Assets.colors.placeholder
  },

  titleText: {
    marginTop: 82,
    fontSize: 24,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.display.bold,
    letterSpacing: -0.2,
    lineHeight: 28,
    marginHorizontal: 15,
    textAlign: 'center',
    marginBottom: 35
  },
  contentContainer: {
    alignItems: 'center'
  },
  itemHolderView: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
    flexDirection: 'row'
  },
  footer: {
    marginHorizontal: 15,
    marginTop: 80
  },
  nextInfoText: {
    fontFamily: Assets.fonts.text.regular,
    color: Assets.colors.textLight,
    fontSize: 14,
    marginRight: 15
  },
  nextButtonImage: { height: 64, width: 64 },

  itemTitle: {
    marginLeft: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.4
  },

  seperatorView: {
    height: 1,
    width: '100%',
    backgroundColor: '#E7EAEB'
  },
  rightButton: {
    textAlign: 'right',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.appTheme,
    marginTop: 3,
    letterSpacing: -0.4
  }
})

export default PayoutAccountAddress
