import React from 'react'
import { View } from 'react-native'
import { StyleSheet } from 'react-native'
import { ScrollView, Text } from 'rn-components'
import { NavigationProps } from '@Types'
import { Navigator } from '@Navigation'
import { Button } from '@Components'
import Assets from '@Assets'

type Props = {
  navigation: NavigationProps
}
class PayoutMethodAddFinal extends React.Component<Props> {
  _onAction = () => {
    const { params } = this.props.navigation.state
    if (params !== null && typeof params.onActionButton === 'function') {
      params.onActionButton()
    } else {
      Navigator.back()
    }
  }

  render() {
    const { params } = this.props.navigation.state
    let title = ''
    let description = ''
    let buttonTitle = ''
    if (params) {
      title = params.title
      description = params.description
      buttonTitle = params.buttonTitle
    }

    return (
      <View style={StyleSheet.absoluteFill}>
        <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
          <View style={{ marginLeft: 30, marginRight: 30, marginTop: 100 }}>
            <Text style={styles.titleText} text={title} />
            <Text style={styles.infoText} text={description} />
            <Button style={{ marginTop: 70 }} text={buttonTitle} onPress={this._onAction} />
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  titleText: {
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    letterSpacing: -0.2,
    lineHeight: 40
  },

  infoText: {
    marginTop: 40,
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    letterSpacing: -0.2,
    lineHeight: 22
  }
})

export default PayoutMethodAddFinal
