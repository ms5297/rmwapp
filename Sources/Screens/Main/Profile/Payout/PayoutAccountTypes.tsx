import React from 'react'
import ProgressCircle from 'react-native-progress-circle'
import { View, Image, TouchableOpacity, StyleSheet } from 'react-native'
import { Header, Text, Icon, ScrollView, Row } from 'rn-components'
import { BackButton } from '@Components'
import { Navigator } from '@Navigation'
import Styles from '@Styles'
import Assets from '@Assets'

class PayoutAccountTypes extends React.Component {
  state = {
    selected: 'Checking'
  }

  _onPress = () => {
    Navigator.navTo('PayoutAccountInfo', {
      data: {
        type: {
          title: 'Account Type',
          value: this.state.selected
        }
      }
    })
  }

  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header titleStyle={Styles.headerTitle} title="Bank Transfer" LeftComponent={<BackButton />} />
        <ScrollView style={styles.content} autoScrollNextInput showsVerticalScrollIndicator={false}>
          <Text style={styles.titleText} text="Choose your account type" />
          {this._renderAccountTypes('Checking')}
          {this._renderAccountTypes('Savings')}
          <Row style={styles.footer} alignHorizontal="space-between" alignVertical="center">
            <ProgressCircle
              percent={30}
              radius={25}
              borderWidth={2}
              color={Assets.colors.appTheme}
              shadowColor={Assets.colors.borderColor}
              bgColor="#fff">
              <Image style={{ tintColor: Assets.colors.appTheme }} source={Assets.images.addCircle} />
            </ProgressCircle>
            <Row alignVertical="center">
              <Text style={styles.nextInfoText} text="Next Step: Account Info" />
              <Icon iconSource={Assets.images.next_round} iconStyle={styles.nextButtonImage} onPress={this._onPress} />
            </Row>
          </Row>
        </ScrollView>
      </View>
    )
  }

  _renderAccountTypes(type) {
    const onPress = () => this.setState({ selected: type })
    return (
      <View>
        <TouchableOpacity activeOpacity={0.7} onPress={onPress} style={styles.itemHolderView}>
          <Text style={styles.itemTitle} text={type} />
          <View style={{ flex: 1 }} />
          <View style={styles.tickMarkView}>
            {type === this.state.selected && (
              <Image source={Assets.images.checkboxOn} style={{ tintColor: Assets.colors.appTheme }} />
            )}
          </View>
        </TouchableOpacity>
        <View style={styles.seperatorView} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    alignContent: 'center',
    marginTop: 82
  },
  tickMarkView: {
    width: 24,
    height: 24,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
    borderWidth: 0.5,
    borderColor: Assets.colors.placeholder
  },

  titleText: {
    fontSize: 24,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.display.bold,
    letterSpacing: -0.2,
    lineHeight: 28,
    marginHorizontal: 15,
    textAlign: 'center',
    marginBottom: 35
  },

  itemHolderView: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
    flexDirection: 'row'
  },
  footer: {
    marginHorizontal: 15,
    marginTop: 100
  },
  nextInfoText: {
    fontFamily: Assets.fonts.text.regular,
    color: Assets.colors.textLight,
    fontSize: 14,
    marginRight: 15
  },
  nextButtonImage: { height: 64, width: 64 },

  itemTitle: {
    marginLeft: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.4
  },

  seperatorView: {
    height: 1,
    width: '100%',
    backgroundColor: '#E7EAEB'
  }
})

export default PayoutAccountTypes
