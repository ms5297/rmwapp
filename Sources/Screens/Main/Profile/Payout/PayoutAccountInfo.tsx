import React from 'react'
import DateTimePicker from 'react-native-modal-datetime-picker'
import ProgressCircle from 'react-native-progress-circle'
import { View, Image, StyleSheet } from 'react-native'
import { ScrollView, Header, Text, Icon, TextInput, InputGroup, Row } from 'rn-components'
import { BackButton, Section } from '@Components'
import { AppConfig, Constants, DateTime } from '@Utils'
import { NavigationProps } from '@Types'
import { Navigator } from '@Navigation'
import Assets from '@Assets'
import Styles from '@Styles'
import Axios from 'axios'

type Props = {
  navigation: NavigationProps
}

type State = {
  dob: string | null
  isDateTimePickerVisible: boolean
}
class PayoutAccountInfo extends React.Component<Props, State> {
  _inputGroupRef1 = React.createRef<InputGroup>()
  _inputGroupRef2 = React.createRef<InputGroup>()

  state: State = {
    dob: null,
    isDateTimePickerVisible: false
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true })

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false })

  _handleDatePicked = date => {
    const age = DateTime.calculateAge(date)
    if (age < 13) {
      this.setState({ isDateTimePickerVisible: false }, () => {
        Navigator.showAlert(
          'You must be over 13 years old',
          'Our platform is designed for users age 13 and up and requires parental consent for anyone under 18. Your parents must approve of you using this site to continue.',
          () => {
            this._showDateTimePicker()
          },
          null,
          'Continue'
        )
      })
    } else {
      this.setState({ dob: date })
      this._hideDateTimePicker()
    }
  }

  _onTapSelectionView(type) {
    if (type === AppConfig.fieldType.date) {
      this._showDateTimePicker()
    }
  }

  _onNext = () => {
    const [holderFirstName, holderLastName] = this._inputGroupRef1.current.getAllText()
    const [last4SSN, routingNumber, accountNumber, confirmAccountNumber] = this._inputGroupRef2.current.getAllText()
    const { dob } = this.state
    if (holderFirstName.length == 0) {
      Navigator.showToast('Error', 'Enter first name', 'Error')
      return
    }
    if (holderLastName.length == 0) {
      Navigator.showToast('Error', 'Enter last name', 'Error')
      return
    }
    if (dob == null || dob.length === 0) {
      Navigator.showToast('Error', 'Enter date of birth', 'Error')
      return
    }
    if (last4SSN.length != 4) {
      Navigator.showToast('Error', 'Enter last 4 digits of your Social Security Number.', 'Error')
      return
    }
    if (routingNumber.length == 0) {
      Navigator.showToast('Error', 'Enter routing number', 'Error')
      return
    }
    if (accountNumber.length == 0) {
      Navigator.showToast('Error', 'Enter Account Number', 'Error')
      return
    }
    if (accountNumber !== confirmAccountNumber) {
      Navigator.showToast('Error', 'The account numbers do not match, please check your account number', 'Error')
      return
    }

    Navigator.showLoading()

    Axios.get('https://api.ipify.org/?format=json')
      .then(response => {
        const { ip } = response as any
        return ip
      })
      .then(ip => {
        const data = {
          first_name: holderFirstName,
          last_name: holderLastName,
          dob,
          last_4_SSN: last4SSN,
          routing_number: routingNumber,
          account_number: accountNumber,
          tos_acceptance: {
            date: Math.floor(Date.now() / 1000),
            ip
          }
        }
        const isForceUpdate = this.props.navigation.getParam('isForceUpdate', false)
        Navigator.hideLoading(() => Navigator.navTo('PayoutAccountAddress', { data, isForceUpdate }))
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  render() {
    const { dob, isDateTimePickerVisible } = this.state

    const date = DateTime.moment(dob || new Date(), 'MMM-DD-YYYY').toDate()
    const maxDate = DateTime.moment()
      .subtract(13, 'years')
      .toDate()
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header titleStyle={Styles.headerTitle} title="Bank Transfer" LeftComponent={<BackButton />} />
        <ScrollView
          style={styles.content}
          keyboardShouldPersistTaps="handled"
          autoScrollNextInput
          showsVerticalScrollIndicator={false}>
          <Text style={styles.titleText} text="Add account info" />
          {this._renderAccountInfo()}
          <Row style={styles.footer} alignHorizontal="space-between" alignVertical="center">
            <ProgressCircle
              percent={60}
              radius={25}
              borderWidth={2}
              color={Assets.colors.appTheme}
              shadowColor={Assets.colors.borderColor}
              bgColor="#fff">
              <Image style={{ tintColor: Assets.colors.placeholder }} source={Assets.images.addCircle} />
            </ProgressCircle>

            <Row alignVertical="center">
              <Text style={styles.nextInfoText} text="Next Step: Address" />
              <Icon onPress={this._onNext} iconStyle={styles.nextButtonImage} iconSource={Assets.images.next_round} />
            </Row>
          </Row>
        </ScrollView>
        <DateTimePicker
          titleIOS="Select your date of birth"
          date={date}
          isVisible={isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
          maximumDate={maxDate}
        />
      </View>
    )
  }

  _validateInputs() {}

  _renderAccountInfo() {
    const { dob } = this.state
    const birthday = dob ? DateTime.format(dob, 'MMM-DD-YYYY') : ''
    return (
      <View style={{ marginHorizontal: 25 }}>
        <InputGroup ref={this._inputGroupRef1} style={styles.inputRow} onInputSubmit={this._showDateTimePicker}>
          <TextInput
            style={[styles.inputContainer, { flex: 0.48 }]}
            clearButtonMode="while-editing"
            inputStyle={styles.input}
            placeholderTextColor={Assets.colors.placeholder}
            selectionColor={Assets.colors.mainText}
            autoCapitalize="words"
            placeholder="First Name"
            underlineWidth={0}
          />
          <TextInput
            style={[styles.inputContainer, { flex: 0.48 }]}
            clearButtonMode="while-editing"
            inputStyle={styles.input}
            placeholderTextColor={Assets.colors.placeholder}
            selectionColor={Assets.colors.mainText}
            autoCapitalize="words"
            placeholder="Last Name"
            underlineWidth={0}
            returnKeyType="next"
          />
        </InputGroup>
        <Section
          style={{ marginTop: 15 }}
          title="Date of Birth"
          titleStyle={[styles.title]}
          value={birthday}
          valueStyle={[styles.value, { color: birthday.length ? Assets.colors.appTheme : Assets.colors.textLight }]}
          iconSource={Assets.images.calendar}
          iconStyle={{ marginRight: 15, tintColor: Assets.colors.borderColor }}
          onPress={this._showDateTimePicker}
        />
        <InputGroup ref={this._inputGroupRef2} spacing={15} onInputSubmit={this._onNext}>
          <TextInput
            style={styles.inputContainer}
            secureTextEntry
            clearButtonMode="while-editing"
            inputStyle={styles.input}
            placeholderTextColor={Assets.colors.placeholder}
            selectionColor={Assets.colors.mainText}
            autoCapitalize="words"
            placeholder="Last 4 digits of your SSN"
            maxLength={4}
            underlineWidth={0}
          />
          <TextInput
            style={styles.inputContainer}
            clearButtonMode="while-editing"
            inputStyle={styles.input}
            placeholderTextColor={Assets.colors.placeholder}
            selectionColor={Assets.colors.mainText}
            autoCapitalize="words"
            placeholder="Routing Number"
            underlineWidth={0}
          />
          <TextInput
            style={styles.inputContainer}
            clearButtonMode="while-editing"
            inputStyle={styles.input}
            placeholderTextColor={Assets.colors.placeholder}
            selectionColor={Assets.colors.mainText}
            autoCapitalize="words"
            placeholder="Account Number"
            underlineWidth={0}
          />
          <TextInput
            style={styles.inputContainer}
            clearButtonMode="while-editing"
            inputStyle={styles.input}
            placeholderTextColor={Assets.colors.placeholder}
            selectionColor={Assets.colors.mainText}
            autoCapitalize="words"
            placeholder="Confirm Account Number"
            underlineWidth={0}
          />
        </InputGroup>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  inputRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  content: {
    alignContent: 'center'
  },
  inputContainer: {
    backgroundColor: 'white',
    borderColor: Assets.colors.borderColorLight,
    borderWidth: 0.5,
    borderRadius: 6,
    height: 44
  },
  input: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    height: 44,
    marginLeft: 10
  },
  tickMarkView: {
    width: 24,
    height: 24,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
    borderWidth: 0.5,
    borderColor: Assets.colors.placeholder
  },

  titleText: {
    marginTop: 60,
    fontSize: 24,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.display.bold,
    letterSpacing: -0.2,
    lineHeight: 28,
    marginHorizontal: 15,
    textAlign: 'center',
    marginBottom: 35
  },
  contentContainer: {
    alignItems: 'center'
  },
  itemHolderView: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
    flexDirection: 'row'
  },
  sectionView: {
    height: 50,
    justifyContent: 'center',
    marginTop: 20
  },

  sectionTitle: {
    marginLeft: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 18,
    color: Assets.colors.mainText,
    letterSpacing: -0.2
  },

  selectionView: {
    width: Constants.WINDOW_WIDTH - 50,
    marginBottom: 15,
    height: 44,
    backgroundColor: '#F4F6F6',
    flexDirection: 'row',
    borderRadius: 6,
    alignItems: 'center'
  },

  titleView: {
    width: 120,
    height: 44,
    marginLeft: 15,
    justifyContent: 'center'
  },

  valueView: {
    flex: 1,
    justifyContent: 'center'
  },

  title: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.4
  },

  value: {
    marginRight: 10,
    textAlign: 'right',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2
  },
  footer: {
    marginHorizontal: 15,
    marginTop: 40
  },
  nextInfoText: {
    fontFamily: Assets.fonts.text.regular,
    color: Assets.colors.textLight,
    fontSize: 14,
    marginRight: 15
  },
  nextButtonImage: { height: 64, width: 64 },

  itemTitle: {
    marginLeft: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.4
  },

  seperatorView: {
    height: 1,
    width: '100%',
    backgroundColor: '#E7EAEB'
  },
  rightButton: {
    textAlign: 'right',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.appTheme,
    marginTop: 3,
    letterSpacing: -0.4
  }
})

export default PayoutAccountInfo
