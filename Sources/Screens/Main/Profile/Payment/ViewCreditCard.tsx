import React from 'react'
import { View, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { StoreState, PaymentActions, getStore } from '@ReduxManager'
import { Header, Text, Row } from 'rn-components'
import { BackButton, Button, Switch } from '@Components'
import { NavigationProps } from '@Types'
import { Navigator } from '@Navigation'
import { StripeHelper, Api, FirebaseHelper } from '@Services'
import { UserProfile, StripePaymentInfo, StripePaymentMethods } from '@Models'
import Styles from '@Styles'
import Assets from '@Assets'

type Props = {
  navigation: NavigationProps
}

type State = {
  card: any
  isDefault: boolean
  userProfile: UserProfile
  paymentInfo: StripePaymentInfo
  paymentMethods: StripePaymentMethods
}

class ViewCreditCard extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)

    const { params } = this.props.navigation.state
    const { payments, userProfile } = getStore().getState()
    const { card } = params
    const paymentInfo = payments ? payments.paymentInfo : null
    const paymentMethods = payments ? payments.paymentMethods : []
    const isDefault = paymentInfo ? paymentInfo.default_source === card.id : false

    console.log('***** ViewCreditCard', paymentInfo, paymentMethods, isDefault)

    console.log('***** ViewCreditCard card', card)

    this.state = {
      card,
      isDefault,
      userProfile,
      paymentInfo,
      paymentMethods
    }
  }

  _didSelectOptionItem = () => {
    this.setState({ isDefault: true }, () => {
      Navigator.showLoading()
      const { card, userProfile } = this.state
      const { id } = card

      console.log('**** setDefaultCard: ', id)
      StripeHelper.setDefaultCard(userProfile.payment_customer_id, id)
        .then((paymentInfo: any) => {
          console.log('**** setDefaultCard paymentInfo', paymentInfo)

          if (paymentInfo) {
            PaymentActions.savePaymentInfo(paymentInfo)
            let paymentMethods = []
            if (paymentInfo.sources && Array.isArray(paymentInfo.sources.data) && paymentInfo.sources.total_count) {
              paymentMethods = paymentInfo.sources.data
              PaymentActions.saveAllPaymentMethods(paymentMethods)
            }
          }

          Navigator.hideLoading()
          FirebaseHelper.logEvent('ChangedDefaultCard')
          if (paymentInfo.default_source === id) {
            Navigator.showToast(
              'Success',
              'Set payment method as default successfully.',
              'Success',
              3000,
              () => null,
              () => {
                Navigator.back()
              },
              true
            )
          } else {
            this.setState({ isDefault: false })
            Navigator.showToast('Error', 'Fail to set payment method as default', 'Error')
          }
        })
        .catch(error => {
          console.warn('**** error', error)
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
          this.setState({ isDefault: false })
        })
    })
  }

  _deletePaymentMethod = () => {
    const { card, userProfile } = this.state
    Navigator.showLoading()
    Api.canRemoveCard()
      .then((response: any) => {
        if (!response.can_remove_card) {
          throw Error(
            'There is a rental in progress, you can not remove this card until the rental is canceled or complete.'
          )
        }
        FirebaseHelper.logEvent('CardRemoved')
        return
      })
      .then(() => {
        return StripeHelper.deleteCard(userProfile.payment_customer_id, card.id)
      })
      .then((response: any) => {
        if (!response || response.error || !response.deleted)
          throw Error(response.error.message || 'Delete card failure.')
        return StripeHelper.getCustomInfo(userProfile.payment_customer_id)
      })
      .then((paymentInfo: any) => {
        console.log('****** getCustomInfo', paymentInfo)
        if (paymentInfo) {
          PaymentActions.savePaymentInfo(paymentInfo)
          let paymentMethods = []
          if (paymentInfo.sources && Array.isArray(paymentInfo.sources.data)) {
            paymentMethods = paymentInfo.sources.data
            PaymentActions.saveAllPaymentMethods(paymentMethods)
          }
        }

        Navigator.hideLoading()
        Navigator.showToast(
          'Success',
          'Delete card successfully',
          'Success',
          3000,
          () => null,
          () => {
            Navigator.back()
          },
          true
        )
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  _renderContent() {
    const { card, isDefault, paymentMethods } = this.state
    console.log('***** _renderContent', isDefault)
    if (!card) return null

    const valueText = isDefault ? 'On ' : 'Off'
    const disabled = isDefault

    return (
      <View style={{ marginTop: 25, flex: 1 }}>
        <Text style={styles.sectionHeaderText} text={`${card.brand} ending ${card.last4}`} />
        <Text style={styles.sectionDescriptionText} text={`Exp. Date ending ${card.exp_month}/${card.exp_year}`} />
        <Row style={{ marginHorizontal: 15 }} alignHorizontal="space-between" alignVertical="center">
          <Text style={styles.itemTitle} text="Set as Default" />
          <Row alignVertical="center">
            <Text style={styles.sliderValueText} text={valueText} />
            <Switch disabled={disabled} value={isDefault} onValueChange={this._didSelectOptionItem} />
          </Row>
        </Row>
        <Button
          style={{ marginTop: 120, marginHorizontal: 60 }}
          text="Delete Card"
          onPress={this._deletePaymentMethod}
        />
      </View>
    )
  }

  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header title="Credit Card" titleStyle={Styles.headerTitle} LeftComponent={<BackButton />} />
        {this._renderContent()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    marginHorizontal: 20,
    marginTop: 20
  },
  itemHolderView: {
    flex: 1,
    height: 44,
    flexDirection: 'row',
    backgroundColor: '#F4F6F6',
    alignItems: 'center',
    borderRadius: 6,
    marginBottom: 10,
    marginLeft: 15,
    marginRight: 15
  },

  itemTitle: {
    marginLeft: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.4
  },

  sectionHeaderText: {
    marginTop: 20,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 10,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    color: Assets.colors.mainText,
    lineHeight: 20
  },

  sectionDescriptionText: {
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 20,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2,
    lineHeight: 18
  },

  selItemHolderView: {
    height: 64,
    alignItems: 'center',
    marginLeft: 15,
    marginRight: 15,
    flexDirection: 'column'
  },

  seperatorView: {
    height: 1,
    backgroundColor: '#E7EAEB'
  },

  sliderValueText: {
    marginRight: 10,
    textAlign: 'right',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2
  },

  actionButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 48,
    marginTop: 60,
    borderRadius: 6,
    overflow: 'hidden'
  }
})

const mapStateToProps = (state: StoreState) => {
  return {
    paymentMethods: state.payments ? state.payments.paymentMethods : [],
    paymentInfo: state.payments ? state.payments.paymentInfo : null,
    userProfile: state.userProfile
  }
}

export default connect(mapStateToProps)(ViewCreditCard)
