import React from 'react'
import { View, Keyboard, StyleSheet } from 'react-native'
import { LiteCreditCardInput } from 'react-native-credit-card-input'
import { PaymentActions, getStore } from '@ReduxManager'
import { Header, Text } from 'rn-components'
import { BackButton, Button } from '@Components'
import { Navigator } from '@Navigation'
import { StripePaymentMethods, UserProfile } from '@Models'
import { StripeHelper, FirebaseHelper, Api } from '@Services'
import { NavigationProps } from '@Types'
import Styles from '@Styles'
import Assets from '@Assets'

type Props = {
  navigation: NavigationProps
}
type State = {
  creditCardInfo: any
  paymentMethods: StripePaymentMethods
  userProfile: UserProfile
}

class AddCreditCard extends React.Component<Props, State> {
  constructor(props) {
    super(props)
    const { userProfile, payments } = getStore().getState()
    this.state = {
      creditCardInfo: null,
      paymentMethods: payments ? payments.paymentMethods : [],
      userProfile
    }
  }

  _onChange = (form: any) => {
    this.setState({ creditCardInfo: form })
  }

  _onTapAddButton = () => {
    Keyboard.dismiss()
    const { creditCardInfo, userProfile, paymentMethods } = this.state
    if (creditCardInfo && creditCardInfo.valid) {
      Navigator.showLoading()
      const { expiry, number, cvc } = creditCardInfo.values
      const expiryParams = expiry.split('/')

      const param = {
        source: {
          object: 'card',
          number: number.replace(/ /g, ''),
          exp_month: parseInt(expiryParams[0]),
          exp_year: parseInt(expiryParams[1]),
          cvc: cvc,
          currency: 'usd'
        }
      }
      const { payment_customer_id } = userProfile
      StripeHelper.createCard(payment_customer_id, param)
        .then((response: any) => {
          FirebaseHelper.logEvent('CardAdded')
          return StripeHelper.getCustomInfo(payment_customer_id)
        })
        .then((paymentInfo: any) => {
          console.log('***** paymentInfo', paymentInfo)

          let paymentMethods = []
          if (paymentInfo.sources && Array.isArray(paymentInfo.sources.data) && paymentInfo.sources.total_count) {
            paymentMethods = paymentInfo.sources.data
            PaymentActions.saveAllPaymentMethods(paymentMethods)
          }
          PaymentActions.savePaymentInfo(paymentInfo)
          return
        })
        .then(response => {
          Navigator.hideLoading()
          Navigator.showToast(
            'Success',
            'Add payment method successfully.',
            'Success',
            3000,
            () => null,
            () => {
              const { params } = this.props.navigation.state
              if (params) {
                const { onSelectCard, onCardAdded } = params
                if (typeof onSelectCard === 'function') {
                  onSelectCard(response)
                  Navigator.popToTop()
                  Navigator.back()
                } else if (typeof onCardAdded === 'function') {
                  onCardAdded(response)
                  Navigator.back()
                } else {
                  this._onBack()
                }
              } else {
                this._onBack()
              }
            },
            true
          )
        })
        .catch(error => {
          console.warn('**** error', error)

          Navigator.hideLoading()
          Navigator.showToast('Error', 'Credit card details is not valid.', 'Error')
        })
    } else {
      Navigator.showToast('Error', 'Credit card details is not valid.', 'Error')
    }
  }

  _onBack = () => {
    const parentView = this.props.navigation.getParam('parentView', null)

    if (parentView === 'Profile') {
      Navigator.back()
      Navigator.back()
    } else {
      Navigator.back()
    }
  }
  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header
          title="Card Details"
          titleStyle={Styles.headerTitle}
          LeftComponent={<BackButton onPress={this._onBack} />}
        />
        <View style={{ flex: 1, marginTop: 30 }}>
          <Text style={styles.mainHeadingText} text="Enter your Credit Card" />
          <View style={styles.cardContainer}>
            <LiteCreditCardInput
              allowScroll
              labels={{ number: 'Card Number', expiry: 'Exp. Date', cvc: 'CVV' }}
              placeholders={{ number: '1234 5678 1234 5678', expiry: 'MM/YY', cvc: 'CVC' }}
              labelStyle={{ fontFamily: Assets.fonts.text.regular, fontSize: 14, color: Assets.colors.textLight }}
              inputStyle={{ fontFamily: Assets.fonts.text.regular, fontSize: 16, color: Assets.colors.mainText }}
              onChange={this._onChange}
            />
          </View>
          <Button
            style={{ marginTop: 120, marginHorizontal: 60 }}
            text="Add Card"
            type="solid"
            onPress={this._onTapAddButton}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    marginHorizontal: 20,
    marginTop: 20
  },
  cardContainer: {
    marginTop: 35,
    marginLeft: 15,
    marginRight: 15,
    borderColor: Assets.colors.borderColorLight,
    borderWidth: 1,
    borderRadius: 6
  },
  actionButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 48,
    marginTop: 60,
    borderRadius: 6,
    overflow: 'hidden'
  },
  actionButtonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    color: 'white'
  },

  mainHeadingText: {
    fontSize: 24,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.display.bold,
    letterSpacing: -0.2,
    lineHeight: 28,
    textAlign: 'center',
    marginLeft: 15,
    marginRight: 15
  }
})

export default AddCreditCard
