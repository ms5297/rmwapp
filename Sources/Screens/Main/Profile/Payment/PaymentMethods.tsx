import React from 'react'
import { View, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { Header, Text } from 'rn-components'
import { EmptyView, BackButton, Section, FlatList } from '@Components'
import { StoreState } from '@ReduxManager'
import { Navigator } from '@Navigation'
import { NavigationProps } from '@Types'
import { StripePaymentMethods, StripePaymentMethod } from '@Models'
import Assets from '@Assets'
import Styles from '@Styles'

type Props = {
  navigation: NavigationProps
  paymentMethods: StripePaymentMethods
}

class PaymentMethods extends React.Component<Props> {
  _onPressAddButton = () => {
    const { params } = this.props.navigation.state
    if (params && params.checkoutProcess) {
      Navigator.navTo('AddCreditCard', params)
    } else {
      Navigator.navTo('AddCreditCard')
    }
  }

  _didSelectCardItem = (item: any) => {
    const { params } = this.props.navigation.state
    if (params && params.checkoutProcess) {
      params.onSelectCard(item)
      Navigator.back()
    } else {
      Navigator.navTo('ViewCreditCard', { card: item })
    }
  }

  _keyExtractor = (item: any, index: number) => index.toString()

  _renderContent = () => {
    const { paymentMethods } = this.props
    return (
      <FlatList
        style={{ flex: 1, marginTop: 40 }}
        data={paymentMethods}
        renderItem={this._renderListItem}
        ListEmptyComponent={
          <EmptyView
            title="No payment methods yet"
            info="To rent for that next occasion you will need to add a payment method."
            buttonText="Add Payment Method"
            onPress={this._onPressAddButton}
          />
        }
        showsVerticalScrollIndicator={false}
      />
    )
  }

  _renderListItem = ({ item }: { item: StripePaymentMethod }) => {
    const { brand, last4 } = item
    const onPress = () => this._didSelectCardItem(item)
    return (
      <Section
        style={{ marginHorizontal: 20 }}
        title={brand}
        value={last4}
        iconSource={Assets.images.categoryDetailsArrow}
        onPress={onPress}
      />
    )
  }

  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header
          title="Payment Methods"
          titleStyle={Styles.headerTitle}
          LeftComponent={<BackButton />}
          RightComponent={<Text style={Styles.textHeaderRight} text="Add" onPress={this._onPressAddButton} />}
        />
        {this._renderContent()}
      </View>
    )
  }
}
const mapStateToProps = (state: StoreState) => {
  return {
    paymentMethods: state.payments ? state.payments.paymentMethods : []
  }
}

export default connect(mapStateToProps)(PaymentMethods)
