import React from 'react'
import { ImageBackground, StyleSheet, FlatList, TouchableOpacity, View, StatusBar } from 'react-native'
import { Device } from '@Utils'
import { Navigator } from '@Navigation'
import { Button } from '@Components'
import { Row, Text } from 'rn-components'
import { FacebookHelper, FirebaseHelper, Authentication } from '@Services'
import { FacebookUserInfo } from '@Models'
import Assets from '@Assets'

const slides = [
  {
    image: Assets.images.appIntro1,

    title: 'Never wear the same thing twice!',
    subTitle: 'Rent fashion locally at sustainable prices. Put cash in your pocket with what’s in your closet! ',
    description: ''
  },
  {
    image: Assets.images.appIntro2,

    title: 'Search',
    subTitle: 'Filter by size, color, location and occasion to find the perfect look.',
    description: ''
  },
  {
    image: Assets.images.appIntro3,

    title: 'Discover',
    subTitle: 'Explore new styles and rent splurge pieces you only wear once anyway.',
    description: ''
  },
  {
    image: Assets.images.appIntro4,

    title: 'Rent',
    subTitle: 'Select the available rental dates, then book and pay for the item.',
    description: "It's simple, easy and cashless. "
  },
  {
    image: Assets.images.appIntro5,

    title: 'Exchange',
    subTitle: 'Chat with the lender to schedule a local time and place for pickup and drop-off.',
    description: ''
  },
  {
    image: Assets.images.appIntro6,

    title: 'Enjoy',
    subTitle: 'Rock your rental  - without buyer’s remorse!',
    description: ''
  },
  {
    image: Assets.images.appIntro7,

    title: 'Review',
    subTitle: 'Our safe and trusting community is built on the reviews provided by each party.',
    description: ''
  },
  {
    image: Assets.images.appIntro8,

    title: 'List Items',
    subTitle: 'Make up to $500/month by renting out items from your closet. ',
    description: ''
  },
  {
    image: Assets.images.app_bg,
    title: '',
    subTitle: '',
    description: ''
  }
]

const { width: windowWidth, height: windowHeight } = Device.getScreenSize()

const isAndroid = Device.isAndroid()

const scaledRatio = isAndroid ? 0.7 : 0.8

const vs = (windowHeight / 812) * (windowHeight <= 768 ? scaledRatio : 1)

const hs = windowWidth / 375

const statusBarHeight = Device.getStatusBarHeight()

const isIphoneX = Device.isIphoneX()

class AppIntro extends React.Component {
  _flatListRef = React.createRef<FlatList<any>>()
  _autoPlayHandler = null
  _scrollLeftToRight = true
  _isDarkStyle = true

  state = {
    currentIndex: 0,
    canAutoScroll: true
  }

  componentDidMount() {
    this._autoPlayHandler = setInterval(this._handlerAutoPlay, 6250)
  }

  componentWillUnmount() {
    if (this._autoPlayHandler) {
      clearInterval(this._autoPlayHandler)
    }
  }

  _handlerAutoPlay = () => {
    const { currentIndex, canAutoScroll } = this.state
    if (this._flatListRef.current && canAutoScroll) {
      const length = slides.length
      if (currentIndex === length - 1) {
        return
      }

      let index = currentIndex + 1 * (this._scrollLeftToRight ? 1 : -1)
      if (index > length - 1) {
        this._scrollLeftToRight = false
        index = length - 1
      } else if (index < 0) {
        this._scrollLeftToRight = true
        index = 0
      }

      this._flatListRef.current.scrollToIndex({ index, animated: true })
    }
  }

  _updateStatusBar = () => {
    if (this.state.currentIndex == slides.length - 1) {
      StatusBar.setBarStyle('light-content')
      isAndroid && StatusBar.setBackgroundColor('rgba(0,0,0,0.0)', false)
      this._isDarkStyle = false
    } else {
      if (!this._isDarkStyle) {
        StatusBar.setBarStyle('dark-content')
        isAndroid && StatusBar.setBackgroundColor(Assets.colors.appTheme, false)
        this._isDarkStyle = true
      }
    }
  }

  _onSkip = () => {
    this.setState({ currentIndex: slides.length - 1 }, this._updateStatusBar)
    this._flatListRef.current.scrollToIndex({ index: slides.length - 1, animated: true })
  }

  _onNext = () => {
    const { currentIndex } = this.state
    if (this._flatListRef.current) {
      const length = slides.length
      const index = currentIndex + 1
      if (index === length) {
        this._onSkip()
        return
      }
      this._flatListRef.current.scrollToIndex({ index, animated: true })
      this._updateStatusBar()
    }
  }

  _handleFBLogin = () => {
    Navigator.showLoading()
    let facebookUserInfo: FacebookUserInfo

    FacebookHelper.loginAndGetUserData()
      .then(user => {
        return user
      })
      .then(user => {
        facebookUserInfo = user
        return FirebaseHelper.loginWithFacebook(user.token)
      })
      .then(userCredentails => {
        if (userCredentails && userCredentails.user) {
          return Authentication.loginAndCreateSession(userCredentails.user)
        } else {
          return false
        }
      })
      .then(isOK => {
        Navigator.hideLoading()
        if (isOK) {
          Navigator.navTo('Home')
        } else {
          Navigator.navTo('SignUp', {
            first_name: facebookUserInfo.first_name,
            last_name: facebookUserInfo.last_name,
            email: facebookUserInfo.email ? facebookUserInfo.email : '',
            photo: 'https://graph.facebook.com/' + facebookUserInfo.id + '/picture?type=large',
            isLoginWithFacebook: true
          })
        }
      })
      .catch(error => {
        Navigator.hideLoading()
        if (typeof error.message == 'string' && error.message != '') {
          Navigator.showToast('Error', error.message, 'Error')
        }
      })
  }

  _onSignUp = () => {
    Navigator.navTo('SignUp')
    isAndroid && StatusBar.setBackgroundColor(Assets.colors.appTheme, false)
    StatusBar.setBarStyle('dark-content')
  }

  _onLogin = () => {
    Navigator.navTo('Login')
    isAndroid && StatusBar.setBackgroundColor(Assets.colors.appTheme, false)
    StatusBar.setBarStyle('dark-content')
  }

  _renderIndicators = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {slides.map((_, index) => {
          const active = index == this.state.currentIndex
          const backgroundColor = active ? '#F06182' : '#FDE6EC'
          return <View key={index} style={[styles.dot, { backgroundColor }]} />
        })}
      </View>
    )
  }

  _renderTopControl = () => {
    const visible = this.state.currentIndex !== slides.length - 1
    return (
      <View style={styles.topContainer}>
        {this._renderIndicators()}
        {visible && <Text onPress={this._onSkip} style={styles.skipText} text="Skip" />}
      </View>
    )
  }

  _renderItem = ({ item, index }) => {
    const { image, title, subTitle, description } = item
    const textAlign = index === 0 ? 'left' : 'center'
    if (index === slides.length - 1) {
      return this._renderOnBoarding()
    }
    return (
      <ImageBackground source={image} style={styles.image} resizeMode="cover">
        <View style={styles.content}>
          <Text style={[styles.title, { textAlign }]} text={title} />
          <Text style={[styles.subTitle, { textAlign }]} text={subTitle} />
          {description !== '' && <Text style={styles.description} text={description} />}
        </View>
      </ImageBackground>
    )
  }

  _renderNextButton = () => {
    const visible = this.state.currentIndex !== slides.length - 1
    if (!visible) {
      return null
    }
    return (
      <TouchableOpacity onPress={this._onNext} style={styles.nextButton}>
        <Text style={styles.nextButtonText} text="Next" />
      </TouchableOpacity>
    )
  }

  _onMomentumScrollBegin = () => {
    this.setState({ canAutoScroll: false })
  }

  _onMomentumScrollEnd = event => {
    const { currentIndex } = this.state
    const { contentOffset } = event.nativeEvent
    const pageIndex = Math.round(contentOffset.x / windowWidth)
    if (pageIndex !== currentIndex) {
      this.setState({ currentIndex: pageIndex, canAutoScroll: true }, this._updateStatusBar)
    } else {
      this.setState({ canAutoScroll: true })
    }
  }

  _keyExtractor = (item, index) => index.toString()

  _renderOnBoarding() {
    return (
      <ImageBackground source={Assets.images.app_bg} style={styles.backgroundImage} resizeMode="cover">
        <View style={{ marginTop: 10, marginHorizontal: 30, marginBottom: 30 }}>
          <Button
            style={{ borderColor: 'white' }}
            textStyle={{ color: 'white' }}
            type="outline"
            text="Join with Facebook"
            onPress={this._handleFBLogin}
          />
          <Button
            style={{ marginTop: 25, backgroundColor: 'white' }}
            type="solid"
            textStyle={{ color: Assets.colors.appTheme }}
            text="Continue with Phone"
            onPress={this._onSignUp}
          />
          <Row style={{}} alignVertical="center">
            <Text style={styles.questionText} text="Already a member?" />
            <Text style={styles.loginText} text="Login" onPress={this._onLogin} />
          </Row>
        </View>
      </ImageBackground>
    )
  }

  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <StatusBar barStyle="dark-content" backgroundColor={Assets.colors.statusBar} translucent />
        <FlatList
          ref={this._flatListRef}
          pagingEnabled={true}
          horizontal={true}
          automaticallyAdjustContentInsets={false}
          showsHorizontalScrollIndicator={false}
          onMomentumScrollBegin={this._onMomentumScrollBegin}
          onMomentumScrollEnd={this._onMomentumScrollEnd}
          data={slides}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          bounces={false}
          alwaysBounceHorizontal={false}
        />
        {this._renderTopControl()}
        {this._renderNextButton()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  image: {
    width: windowWidth,
    height: '100%'
  },
  content: {
    marginHorizontal: 30 * hs,
    marginTop: 64 * vs + statusBarHeight
  },
  topContainer: {
    position: 'absolute',
    top: 17 * vs + statusBarHeight,
    left: 20,
    right: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 30 * vs
  },
  dot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 5
  },
  skipText: {
    fontFamily: Assets.fonts.display.bold,
    color: Assets.colors.appTheme,
    fontSize: 14
  },
  title: {
    fontFamily: Assets.fonts.display.semibold,
    color: '#262D3F',
    fontSize: 36,
    lineHeight: 40,
    letterSpacing: -0.2
  },
  subTitle: {
    marginTop: 15 * vs,
    fontFamily: Assets.fonts.display.bold,
    color: '#4D5260',
    fontSize: 14,
    lineHeight: 22,
    letterSpacing: -0.4
  },
  description: {
    marginTop: 20 * vs,
    fontFamily: Assets.fonts.display.regular,
    fontSize: 16,
    lineHeight: 22,
    letterSpacing: 0,
    textAlign: 'center',
    color: '#e84a6f'
  },
  nextButton: {
    position: 'absolute',
    right: 25,
    bottom: isIphoneX ? 34 : 15,
    width: 68,
    height: 68,
    borderRadius: 34,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center'
  },
  nextButtonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 16,
    lineHeight: 20,
    letterSpacing: 0,
    color: '#f06182'
  },
  backgroundImage: {
    width: windowWidth,
    height: '100%',
    justifyContent: 'flex-end'
  },
  questionText: {
    color: 'white',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    marginBottom: 30,
    marginTop: 25
  },
  loginText: {
    fontSize: 14,
    fontFamily: Assets.fonts.display.bold,
    color: 'white',
    marginLeft: 15,
    marginBottom: 30,
    marginTop: 25
  }
})

export default AppIntro
