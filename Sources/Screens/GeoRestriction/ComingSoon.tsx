import React from 'react'
import { View, StyleSheet, SafeAreaView } from 'react-native'
import { Header, Text } from 'rn-components'
import { Device, AppConfig } from '@Utils'
import { ComingSoonParam, EnterAccessCodeParam } from '@Models'
import { Button } from '@Components'
import { NavigationProps } from '@Types'
import Assets from '@Assets'
import { Navigator } from '@Navigation'
import { Api } from '@Services'
import { UserProfileActions, getStore, FilterActions } from '@ReduxManager'

type Props = {
  navigation: NavigationProps
}

type State = ComingSoonParam & {
  isInWaitlist: boolean
}

class ComingSoon extends React.Component<Props, State> {
  constructor(props) {
    super(props)
    const param = this.props.navigation.getParam('param', {})
    const { userProfile } = getStore().getState()
    const isInWaitlist = userProfile ? userProfile.is_in_waitlist : false
    this.state = {
      ...param,
      isInWaitlist
    }
  }

  onHaveCode = () => {
    const param: EnterAccessCodeParam = {
      onCodeAddedSuccess: this.state.onCodeAddedSuccess
    }
    Navigator.navTo('EnterAccessCode', { param })
  }

  handlePressed = () => {
    const { isHandleJoinWaitlist, onPressed } = this.state
    if (isHandleJoinWaitlist) {
      Navigator.showLoading()
      Api.addToWaitlist()
        .then((response: any) => {
          const { code, message } = response
          console.log('**** code, message', code, message)

          if (code !== 200) throw Error(message || 'Internal Error.')
          Navigator.hideLoading()

          UserProfileActions.setUserInWaitlist()
          this.setState({ isInWaitlist: true })

          Navigator.showToast(
            'Success',
            'You are added in waitlist',
            'Success',
            2000,
            () => null,
            () => {
              typeof onPressed === 'function' && onPressed()
            },
            true
          )
        })
        .catch(error => {
          Navigator.hideLoading()
          Navigator.showToast('Error', error.message, 'Error')
          console.log('**** error', error)
        })
    } else {
      typeof onPressed === 'function' && onPressed()
    }
  }

  onViewDallas = () => {
    let { filters } = getStore().getState()
    let dallasLocation = {
      longitude: AppConfig.longitude,
      latitude: AppConfig.latitude
    }
    let categoryFilterToUpdate = { ...filters.categories }
    categoryFilterToUpdate['filters'] = { 'location': dallasLocation }

    let filtersToUpdate = { ...filters.itemList.filters }
    filtersToUpdate['location'] = dallasLocation
    filtersToUpdate['locality'] = 'Dallas, TX'

    let itemListToUpdate = { ...filters.itemList }
    itemListToUpdate['filters'] = filtersToUpdate

    // FilterActions.updateCategoryFilterParam(filters.categories)
    // FilterActions.updateItemListFilterParam(filters.itemList)
    FilterActions.updateCategoryFilterParam(categoryFilterToUpdate)
    FilterActions.updateItemListFilterParam(itemListToUpdate)
    // const onGoBack = this.props.navigation.getParam('onGoBack')
    // if (typeof onGoBack === 'function') {
    //   onGoBack()
    // }
    // Navigator.back()
    const { onPressed } = this.state
    typeof onPressed === 'function' && onPressed()
  }

  render() {
    const { title, message, buttonText } = this.state
    return (
      <SafeAreaView style={StyleSheet.absoluteFill}>
        <View style={{ marginHorizontal: 30, flex: 1, marginTop: 20 }}>
          <Text style={styles.helpText} text={title} />
          <Text style={styles.infoText}>{message}</Text>
          <Button onPress={this.handlePressed} style={styles.button} textStyle={styles.buttonText} text={buttonText} />
          {this.state.isInWaitlist ? <Button
            style={styles.button}
            textStyle={styles.buttonText}
            text="Check Dallas"
            onPress={this.onViewDallas}
          /> : <View></View>}

          <View style={{ flex: 1 }} />
          <Button
            type="outline"
            text="Have a code?"
            style={styles.buttonCode}
            textStyle={styles.buttonCodeText}
            onPress={this.onHaveCode}
          />

        </View>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  helpText: {
    marginTop: 44 * Device.vs,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold
  },
  infoText: {
    marginTop: 20 * Device.vs,
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    lineHeight: 22
  },

  button: {
    marginHorizontal: 10,
    marginTop: 60 * Device.vs
  },
  buttonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: 'white'
  },
  buttonCode: {
    borderColor: Assets.colors.textLight,
    marginBottom: 20 * Device.vs,
    alignSelf: 'center'
  },
  buttonCodeText: {
    color: Assets.colors.textLight,
    marginHorizontal: 25
  }
})

export default ComingSoon
