import React from 'react'
import { StyleSheet, Modal, View, Keyboard } from 'react-native'
import { Header, Text, TextInput } from 'rn-components'
import { Navigator } from '@Navigation'
import { Api } from '@Services'
import { BackButton, Button } from '@Components'
import { NavigationProps } from '@Types'
import { Constants, Device } from '@Utils'

import Assets from '@Assets'
import Styles from '@Styles'
import { EnterAccessCodeParam } from '@Models'
import { UserProfileActions, getStore } from '@ReduxManager'

type Props = {
  navigation: NavigationProps
}

type State = EnterAccessCodeParam & {
  code: string
}
class EnterAccessCode extends React.Component<Props, State> {
  _inputRef = React.createRef<TextInput>()
  _timeoutHandler = null
  constructor(props: Props) {
    super(props)
    const param: EnterAccessCodeParam = this.props.navigation.getParam('param', {})
    this.state = {
      ...param,
      code: ''
    }
  }

  componentDidMount() {
    this._timeoutHandler = setTimeout(() => this._inputRef.current.focus(), 300)
  }

  componentWillUnmount() {
    this._timeoutHandler && clearTimeout(this._timeoutHandler)
  }

  _onAddCode = () => {
    // const accessCode = this._inputRef.current.getText()
    const { code: accessCode } = this.state
    if (accessCode === '') {
      Navigator.showToast('Error', 'Please enter an valid access code.', 'Error')
      return
    }
    Keyboard.dismiss()
    Navigator.showLoading()

    const { onCodeAddedSuccess } = this.state
    Api.applyAccessCode(accessCode)
      .then((response: any) => {
        const { code, message } = response
        if (code !== 200) throw Error(message || 'Invalid access code.')
        Navigator.hideLoading()
        Navigator.showToast(
          'Success',
          'Your access code has been applied.',
          'Success',
          2000,
          () => null,
          () => {
            if (typeof onCodeAddedSuccess === 'function') {
              onCodeAddedSuccess(accessCode)
            } else {
              const { userProfile } = getStore().getState()
              let userToUpdate = { ...userProfile }
              userToUpdate['accessibility'] = true
              UserProfileActions.saveUserProfile(userToUpdate)
              Navigator.navTo('Home')
            }
          },
          true
        )
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error', 2000)
      })
  }

  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <Header title="" titleStyle={Styles.headerTitle} LeftComponent={<BackButton />} />

        <View style={{ marginHorizontal: 20 }}>
          <Text style={styles.helpText} text="Access Code" />
          <Text style={styles.infoText} text="Have a code for exclusive access, enter it below." />
          <TextInput
            ref={this._inputRef}
            placeholder="Code"
            placeholderTextColor={Assets.colors.placeholder}
            style={styles.inputContainer}
            inputStyle={styles.input}
            onSubmitEditing={this._onAddCode}
            value={this.state.code}
            onChangeText={code => this.setState({ code: code.toUpperCase() })}
          />
          <Button text="Enter the App" style={styles.button} onPress={this._onAddCode} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 24,
    color: '#455154'
  },
  button: {
    marginHorizontal: 60,
    marginTop: Constants.WINDOW_HEIGHT / 4 - 48,
    alignSelf: 'stretch'
  },
  inputContainer: {
    alignSelf: 'stretch',
    marginTop: 40,
    backgroundColor: 'transparent',
    borderRadius: 4,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: Assets.colors.borderColor
  },
  input: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    height: 44,
    marginLeft: 10,
    textAlignVertical: 'center',
    paddingTop: 4
  },
  helpText: {
    marginTop: 10 * Device.vs,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold
  },
  infoText: {
    marginTop: 20,
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    lineHeight: 22
  }
})

export default EnterAccessCode
