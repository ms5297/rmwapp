import React from 'react'
import { View, StyleSheet, SafeAreaView, Linking, Text as RNText } from 'react-native'
import { Text } from 'rn-components'
import { Device, AppConfig } from '@Utils'
import { Button } from '@Components'
import { NavigationProps } from '@Types'
import Assets from '@Assets'
import { Navigator } from '@Navigation'
import { Api, Authentication } from '@Services'
import { UserProfileActions, getStore, FilterActions } from '@ReduxManager'

type Props = {
  navigation: NavigationProps
}

type State = {
  isInWaitlist: boolean
}
class ExistingUserComingSoon extends React.Component<Props, State> {
  _userLocality: string = ''
  constructor(props) {
    super(props)
    const { userProfile } = getStore().getState()
    this._userLocality = userProfile.locality.split(',')[0]
    const isInWaitlist = userProfile ? userProfile.is_in_waitlist : false
    this.state = {
      isInWaitlist
    }
  }
  onRequestAccessCode = async () => {
    const subject = AppConfig.requestAccessCodeSubject
    const mailTo = AppConfig.supportMail
    const link = `mailto:${mailTo}?subject=${subject}`
    await Linking.openURL(link)
  }

  onJoinWaitlist = () => {
    Navigator.showLoading()
    Api.addToWaitlist()
      .then((response: any) => {
        const { code, message } = response
        if (code !== 200) throw Error(message || 'Internal Error.')
        console.log('**** response', response)
        Navigator.hideLoading()
        Navigator.showToast('Success', 'You are added in waitlist', 'Success')
        UserProfileActions.setUserInWaitlist()
        this.setState({ isInWaitlist: true })
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
        console.log('**** error', error)
      })
  }

  onLogout = () => {
    Navigator.showLoading()
    Authentication.logout()
      .then(() => {
        Navigator.hideLoading(() => Navigator.reset('Start'))
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  onHaveCode = () => {
    Navigator.navTo('EnterAccessCode')
  }

  onViewMyCity = () => {
    //UserProfileActions.setUserAsNotInCityMsgShown()
    Navigator.back()
  }

  onViewDallas = () => {
    //UserProfileActions.setUserAsNotInCityMsgShown()
    let { filters } = getStore().getState()
    let dallasLocation = {
      longitude: AppConfig.longitude,
      latitude: AppConfig.latitude
    }
    let categoryFilterToUpdate = { ...filters.categories }
    categoryFilterToUpdate['filters'] = { 'location': dallasLocation }

    let filtersToUpdate = { ...filters.itemList.filters }
    filtersToUpdate['location'] = dallasLocation
    filtersToUpdate['locality'] = 'Dallas, TX'

    let itemListToUpdate = { ...filters.itemList }
    itemListToUpdate['filters'] = filtersToUpdate

    // FilterActions.updateCategoryFilterParam(filters.categories)
    // FilterActions.updateItemListFilterParam(filters.itemList)
    FilterActions.updateCategoryFilterParam(categoryFilterToUpdate)
    FilterActions.updateItemListFilterParam(itemListToUpdate)
    const onGoBack = this.props.navigation.getParam('onGoBack')
    if (typeof onGoBack === 'function') {
      onGoBack()
    }
    Navigator.back()

  }

  displaySearchButtons() {
    const fromCloset = this.props.navigation.getParam('fromCloset')
    if (fromCloset) {
      return (
        <View>
          <Button
            style={styles.buttonNew}
            textStyle={styles.buttonText}
            text="Explore the app"
            onPress={this.onViewMyCity}
          />
        </View>
      )
    } else {
      return (
        <View>
          <Button
            style={styles.buttonNew}
            textStyle={styles.buttonText}
            text="Search My Location"
            onPress={this.onViewMyCity}
          />
          <Button
            style={styles.buttonNew}
            textStyle={styles.buttonText}
            text="Search Dallas"
            onPress={this.onViewDallas}
          />
        </View>
      )
    }
  }

  render() {
    const { isInWaitlist } = this.state
    return (
      <SafeAreaView style={StyleSheet.absoluteFill}>
        <View style={{ marginHorizontal: 30, flex: 1, marginTop: 20 }}>
          {/* <Text style={styles.helpText} text="Thanks for trying our beta" /> */}
          <Text style={styles.helpText} text={"We are coming to " + this._userLocality + " soon!"} />
          <RNText style={styles.infoText}>
            {"Want to cash in your closet sooner? Become a " + this._userLocality + " brand advocate and get early access to inventory listings. Request an"}
            <RNText style={{ color: Assets.colors.appTheme }} onPress={this.onRequestAccessCode}>
              {' '}
              early access code.
            </RNText>
          </RNText>

          {isInWaitlist ? (
            this.displaySearchButtons()
          ) : (
              <Button
                onPress={this.onJoinWaitlist}
                disabled={isInWaitlist}
                style={[
                  styles.button,
                  { backgroundColor: isInWaitlist ? Assets.colors.seperatorColor : Assets.colors.buttonColor }
                ]}
                textStyle={styles.buttonText}
                text="Join the Waitlist"
              />
            )}

          <View style={{ flex: 1 }} />
          <Button
            type="outline"
            text="Have a code?"
            style={styles.buttonCode}
            textStyle={styles.buttonCodeText}
            onPress={this.onHaveCode}
          />
        </View>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  helpText: {
    marginTop: 22 * Device.vs,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold
  },
  infoText: {
    marginTop: 28,
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    lineHeight: 22
  },
  button: {
    marginHorizontal: 10,
    marginTop: 99 * Device.vs
  },
  buttonNew: {
    marginHorizontal: 10,
    marginTop: 50 * Device.vs
  },
  buttonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: 'white'
  },
  buttonCode: {
    borderColor: Assets.colors.textLight,
    marginBottom: 20 * Device.vs,
    alignSelf: 'center'
  },
  buttonCodeText: {
    color: Assets.colors.textLight,
    marginHorizontal: 25
  }
})

export default ExistingUserComingSoon
