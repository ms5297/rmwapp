import React from 'react'
import { Keyboard, View, TouchableHighlight, StyleSheet, ImageSourcePropType, StatusBar, TouchableOpacity, Animated, Dimensions, Image, ImageBackground } from 'react-native'
import { Header, ScrollView, InputGroup, TextInput, Text, Icon } from 'rn-components'
import { Avatar, Button, BackButton, TooltipUI, PopoverUI } from '@Components'
import { Navigator } from '@Navigation'
import { NavigationProps } from '@Types'
import { Validator, AppConfig, Device, Utils } from '@Utils'
import { FirebaseHelper, StripeHelper, Api, Authentication, BranchHelper, AppsFlyerHelper, GoogleAnalyticsHelper } from '@Services'
import { UserProfile, OTPValidationParam } from '@Models'
import Assets from '@Assets'
import Styles from '@Styles'
import Tooltip from 'react-native-walkthrough-tooltip'
import Popover from 'react-native-popover-view';
import Modal from "react-native-modal";
import { Alert } from 'rn-notifier'

type Props = {
  navigation: NavigationProps
}

type State = {
  firstname: string
  lastname: string,
  username: string,
  email: string
  phoneNumber: string
  photo: string
  source: ImageSourcePropType
  isLoginWithFacebook: boolean
  needAuthorizePhone: boolean
  toolTipVisibleFirstName: boolean
  toolTipVisibleLastName: boolean
  toolTipVisibleEmailID: boolean
  toolTipVisibleMobNo: boolean
  isVisible: boolean
  modalShow: boolean
  toolTipVisibleClose: boolean
  isVisibleGuide: boolean
  toolTipGuideAgain: boolean
  toolTipVisibleUsername: boolean
  openBulbIcon: boolean
  ref: React.RefObject<PopoverUI>
  ref1: any
  animation: any
  isOpenPopoverVisible: boolean
}

class SignUp extends React.Component<Props, State> {
  _inputGroupRef = React.createRef<InputGroup>()
  touchable: TouchableHighlight
  myRef: React.RefObject<PopoverUI>


  constructor(props: Props) {
    super(props)

    this.myRef = React.createRef();
    let firstname = ''
    let lastname = ''
    let username = ''
    let email = ''
    let phoneNumber = ''
    let source = Assets.images.user_photo_placeholder
    let photo = ''
    let isLoginWithFacebook = false
    let needAuthorizePhone = true


    const { params } = this.props.navigation.state

    if (params) {
      firstname = params.firstname ? params.firstname : ''
      lastname = params.lastname ? params.lastname : ''
      username = params.username ? params.username : ''
      email = params.email ? params.email : ''
      source = params.photo ? { uri: params.photo } : Assets.images.user_photo_placeholder
      phoneNumber = params.phone ? params.phone : ''
      photo = params.photo ? params.photo : ''
      isLoginWithFacebook =
        params.isLoginWithFacebook !== null && params.isLoginWithFacebook !== undefined
          ? params.isLoginWithFacebook
          : false
      needAuthorizePhone =
        params.needAuthorizePhone !== null && params.needAuthorizePhone !== undefined ? params.needAuthorizePhone : true
    }
    this.state = {
      firstname,
      lastname,
      username,
      email,
      phoneNumber,
      photo,
      source,
      isLoginWithFacebook,
      needAuthorizePhone,
      toolTipVisibleFirstName: false,
      toolTipVisibleLastName: false,
      toolTipVisibleEmailID: false,
      toolTipVisibleMobNo: false,
      isVisible: false,
      modalShow: false,
      toolTipVisibleClose: false,
      toolTipVisibleUsername: false,
      isVisibleGuide: false,
      toolTipGuideAgain: false,
      openBulbIcon: false,
      ref: null,
      ref1: null,
      animation: new Animated.Value(0),
      isOpenPopoverVisible: false
    }
  }
  componentDidMount() {

    setTimeout(() => {
      //
      //  this.setRef(this.refs)
      this.showPopover()
    }, 2000);
  }

  _onLogin = () => Navigator.navTo('Login')

  _onJoin = () => {
    Keyboard.dismiss()
    const [firstname, lastname, username, email, phoneNumber] = this._inputGroupRef.current.getAllText()

    if (firstname.length == 0) {
      Navigator.showToast('Error', 'Enter your first name', 'Error')
      return
    }
    if (lastname.length == 0) {
      Navigator.showToast('Error', 'Enter your last name', 'Error')
      return
    }
    if (username.length == 0) {
      Navigator.showToast('Error', 'Enter username', 'Error')
      return
    }
    if (Validator.validateEmail(email) == false) {
      Navigator.showToast('Error', 'Please enter a valid email address.', 'Error')
      return
    }
    if (Validator.validatePhoneNumber(phoneNumber) == false) {
      Navigator.showToast('Error', 'Please enter a valid phone number.', 'Error')
      return
    }

    const { isLoginWithFacebook, needAuthorizePhone } = this.state

    console.log('**** isLoginWithFacebook, needAuthorizePhone', isLoginWithFacebook, needAuthorizePhone)
    if (isLoginWithFacebook) {
      this._createNewUser()
    } else {
      if (needAuthorizePhone) {
        this._loginWithPhone()
      } else {
        this._createNewUser()
      }
    }
  }
  showPopover() {
    this.setState({ isOpenPopoverVisible: true });
    setTimeout(() => {
      // this.setState({toolTipVisibleClose: true});
    }, 1000);
  }
  setRef = (refs) => {
    this.setState({
      ref: refs,
      ref1: null
    })
  }

  setRef1 = (refs) => {
    this.setState({
      ref1: refs,
      ref: null,
    })
  }

  closePopover() {
    this.setState({ isVisible: false });
  }

  _onSourceChanged = source => {
    if (source) {
      this.setState({ source })
    } else {
      this.setState({ source: null })
    }
  }

  _onViewPrivacy = () => Navigator.navTo('WebsiteView', { url: AppConfig.privacy_url })

  _onViewTerm = () => Navigator.navTo('WebsiteView', { url: AppConfig.terms_url })

  _renderDisclaimer = () => {
    return (
      <View style={styles.disclaimerView}>
        <Text style={styles.disclaimerText} text="By signing up, you agree to our" />
        <View style={styles.disclaimerButtonView}>
          <Text style={styles.disclaimerButton} onPress={this._onViewPrivacy} text="Privacy Policy" />
          <Text style={styles.disclaimerText} text=" and " />
          <Text style={styles.disclaimerButton} onPress={this._onViewTerm} text="Terms of Service" />
        </View>
      </View>
    )
  }

  _loginWithPhone = () => {
    // const phone = this._inputGroupRef.current.getText(3)
    const phone = this._inputGroupRef.current.getText(4)
    Navigator.showLoading()
    FirebaseHelper.loginWithPhone('+1' + phone)
      .then(response => {
        const param: OTPValidationParam = {
          confirmationResult: response,
          phone,
          onShouldCreateNew: this._createNewUser
        }
        Navigator.hideLoading(() => Navigator.navTo('OTP', { param }))
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  _uploadPhoto = (callback: (url: string) => void) => {
    const { source, photo } = this.state
    const uploadedURL = source === null ? '' : typeof source === 'object' && 'uri' in source ? source.uri : photo
    if (uploadedURL.length > 0 && Validator.isURL(uploadedURL) == false) {
      FirebaseHelper.uploadUserProfilePhoto(uploadedURL)
        .then(url => {
          callback(url)
        })
        .catch(error => {
          callback(uploadedURL)
        })
    } else {
      callback(uploadedURL)
    }
  }

  _createNewUser = () => {
    const [firstname, lastname, username, email, phoneNumber] = this._inputGroupRef.current.getAllText()
    const phone = '+1' + phoneNumber
    const name = `${firstname} ${lastname}`

    console.log('**** phone, name, email', phone, name, email)
    let payload = {}
    Navigator.showLoading()
    let user: UserProfile = null
    this._uploadPhoto(url => {
      StripeHelper.createCustomerPaymentAccount(email, name, phone)
        .then((response: any) => {
          if (response === null && response.id === null) throw Error("Can't create payment account")

          return response.id
        })
        .then(id => {
          payload = {
            firstname,
            lastname,
            user_name: username,
            email,
            phone,
            photo: '',
            payment_customer_id: id
          }
          if (Validator.isURL(url)) {
            payload['photo'] = url
          }
          return Api.createUserProfile(payload)
        })
        .then((response: any) => {
          if (!response.user) throw Error(response.message)
          user = response.user
          console.log('**** user ', user)
          return Authentication.createSession()
        })
        .then(isOK => {
          if (isOK) {
            Navigator.hideLoading()
            BranchHelper.trackingRegister(user)
            AppsFlyerHelper.trackingRegister(user)
            GoogleAnalyticsHelper.trackEvent(GoogleAnalyticsHelper.EVENT_CATEGORY.MAIN, GoogleAnalyticsHelper.EVENT.REGISTER, { user_id: user.id })
            Navigator.navTo('UpdateBirthday', { param: payload })
          } else {
            throw Error('(SU - 203) Internal Error.')
          }
        })
        .catch(error => {
          Navigator.hideLoading()
          if (typeof error.message === 'string' && error.message.indexOf('The phone number already exists') !== -1) {
            Navigator.showToast(
              'Error',
              error.message,
              'Error',
              3000,
              () => null,
              () => Navigator.navTo('Login', { phoneNumber }),
              true
            )
          } else {
            Navigator.showToast('Error', error.message, 'Error')
          }
        })
    })
  }
  setModalVisible(visible) {
    this.setState({ modalShow: visible });
  }
  closeCoachmarkModal = () => {
    this.setState({ modalShow: false, openBulbIcon: true });
  }
  setModalVisibleGuid(visible) {
    this.setState({ isVisibleGuide: visible });
    setTimeout(() => {
      this.setState({ toolTipGuideAgain: true });
    }, 1000);
  }
  _renderNewModal() {
    return (
      <View style={{ flex: 1 }}>
        {/* <Button title="Show modal" onPress={this.toggleModal} /> */}
        <Modal isVisible={this.state.modalShow} style={{ backgroundColor: "#5dd19a", maxHeight: 135, paddingVertical: 15, paddingHorizontal: 15, borderRadius: 5, marginTop: 60 }}
          animationIn="slideInDown"
          animationOut="slideOutUp"
          useNativeDriver={true}
          backdropColor='transparent'
          swipeDirection='up'
          swipeThreshold={0}
          onSwipeComplete={() => this.handleCloseModal()}
        >
          <View style={{ flex: 1 }}>
            <Text style={{ fontSize: 16, color: '#fff', fontWeight: '600', marginBottom: 10 }}>If you are familiar with our app, you can turn off coachmark guide here.</Text>
            <Button
              style={[styles.negativeButton, { borderWidth: 0 }]}
              textStyle={styles.negativeButtonText}
              text="Turn guide off"
              onPress={this.closeCoachmarkModal}
            />
          </View>
        </Modal>
      </View>
    );
  }
  handleCloseModal = () => {
    this.setState({ modalShow: false })
  }

  _onBack = () => {
    Navigator.back()
    Device.isAndroid() && StatusBar.setBackgroundColor('rgba(0,0,0,0.0)', false)
    StatusBar.setBarStyle('dark-content')
  }
  closeCoachMark = () => {
    this.setState({
      toolTipVisibleClose: false,
      isVisible: false
    })
    setTimeout(() => {
      this.setModalVisible(true)
    }, 1000);

  }
  _guidModalCoachMark = () => {
    this.setState({ isVisibleGuide: false, toolTipGuideAgain: false });

  }
  _startGuidAgain = () => {
    this.setState({ openBulbIcon: false });

  }

  showGuideAgain = () => {
    this.setState({ isVisibleGuide: false })
    this.setModalVisibleGuid(true)
  }
  // _renderPopover = () =>{
  //   return(
  //     <Popover
  //           arrowStyle={{backgroundColor:'#fff', width:30, height:15,borderRadius:5}}
  //           fromView={this.state.ref}
  //           placement="top"
  //           backgroundStyle={{
  //             backgroundColor: 'rgba(0,0,0,.35)',
  //           }}
  //           isVisible={this.state.isVisible}
  //           popoverStyle={{width:200,padding:20, position:'relative',overflow:'visible',borderRadius:5,shadowColor:'transparent'}}
  //           >

  //           <Text>Enter the required information and click on Join to register.</Text>
  //           <View style={{position:'absolute',top:-15,left:-15,zIndex:99999}}>
  //             <TouchableOpacity
  //           ><Icon iconStyle={{ width:35, height:35,marginTop:-17}} iconSource={Assets.images.buttonCross} onPress={this.closeCoachMark}/></TouchableOpacity>
  //             </View>
  //             <View style={{width: 0, height: 0,backgroundColor: 'transparent',borderStyle: 'solid',borderLeftWidth: 10,
  //               borderRightWidth: 10,
  //               borderBottomWidth: 10,
  //               borderLeftColor: 'transparent',
  //               borderRightColor: 'transparent',
  //               borderBottomColor: '#fff', transform: [
  //                 {rotate: '180deg'}
  //               ], position:'absolute', bottom:-10,zIndex:9999, justifyContent:'center', alignItems:'center', flexDirection:'row', alignSelf:'center'}}
  //               ></View>
  //         </Popover>
  //   )
  // }
  handleOpen = () => {
    Animated.timing(this.state.animation, {
      toValue: 1,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };
  handleClose = () => {
    Animated.timing(this.state.animation, {
      toValue: 0,
      duration: 200,
      useNativeDriver: true,
    }).start();
  };

  _renderPopoverGuidAgain = () => {
    return (
      <Popover
        arrowStyle={{ backgroundColor: '#fff', width: 30, height: 15, borderRadius: 15 }}
        fromView={this.state.ref1}
        // animationConfig={{ duration: 250 }}
        placement="top"
        backgroundStyle={{
          backgroundColor: 'rgba(0,0,0,.35)',
        }}
        isVisible={this.state.isVisibleGuide}
        popoverStyle={{ padding: 20, position: 'relative', overflow: 'visible', borderRadius: 5, width: 300, shadowColor: 'transparent' }}
      >
        <TouchableOpacity style={{ position: 'absolute', top: -27, left: -19 }}>
          <Icon iconStyle={{ width: 35, height: 35 }} iconSource={Assets.images.buttonCross} onPress={this._guidModalCoachMark} />
        </TouchableOpacity>
        <Text style={{ fontSize: 15, lineHeight: 18 }}>To walkthrough the guide, click button.</Text>

        <Button
          // style={{ height:60,marginTop:10}}
          style={{ marginTop: 10 }}
          text="Start guide again"
          onPress={this._startGuidAgain}
        />
      </Popover>
    )
  }



  _renderItemGuid = () => {
    const screenHeight = Dimensions.get("window").height;

    const backdrop = {
      transform: [
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 0.01],
            outputRange: [screenHeight, 0],
            extrapolate: "clamp",
          }),
        },
      ],
      opacity: this.state.animation.interpolate({
        inputRange: [0.01, 0.5],
        outputRange: [0, 1],
        extrapolate: "clamp",
      }),
    };

    const slideUp = {
      transform: [
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0.01, 1],
            outputRange: [0, -1 * screenHeight],
            extrapolate: "clamp",
          }),
        },
      ],
    };

    const popupHeight = screenHeight - 60
    return (
      <Animated.View style={[StyleSheet.absoluteFill, styles.cover, backdrop]}>
        <View style={[styles.sheet]}>
          <Animated.View style={[styles.popup, slideUp, { height: popupHeight }]}>
            <View style={{ position: 'relative' }}>
              <TouchableOpacity style={{ position: 'absolute', top: 16, left: 12, zIndex: 9999999 }} onPress={this.handleClose}>
                <Icon iconStyle={{ width: 35, height: 35, position: 'absolute', left: 0 }} iconSource={Assets.images.buttonCross} onPress={this.handleClose} />
              </TouchableOpacity>
              <Image source={Assets.images.tryOnBackground} style={{ width: '100%' }} resizeMode='stretch' />
              <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 30, width: '100%', height: '100%', justifyContent: 'center', alignContent: 'center', flexDirection: 'row', alignItems: 'center' }}>
                <Image source={Assets.images.tryOn1} style={{ justifyContent: 'center', alignContent: 'center', flexDirection: 'row' }} />
              </View>
            </View>
            <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', padding: 10, paddingTop: 48 }}>
              <Text style={{ fontSize: 28, color: "#2F282A", textAlign: "center", lineHeight: 34, fontWeight: "600", marginBottom: 8 }} text="Try On" />
              <Text
                style={{ fontSize: 16, color: "#948D8F", letterSpacing: 0, textAlign: "center", lineHeight: 24 }}
                text="Don’t worry about renting an item that doesn’t fit. Message your lender to try on the item before renting."
              />
            </View>
            <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', padding: 10, flexDirection: 'row' }}>
              <Image source={Assets.images.tryOnPagination} />
            </View>

            <View style={{ paddingHorizontal: 10, marginTop: 44 }}>
              <Button style={{ marginTop: 10 }} text="Next" />
            </View>


          </Animated.View>
        </View>
      </Animated.View>

    )
  }

  render() {
    const { firstname, lastname, username, phoneNumber, email, source: _source } = this.state
    const source = _source || Assets.images.user_photo_placeholder

    return (
      <View style={StyleSheet.absoluteFill}>
        <Header
          LeftComponent={<BackButton onPress={this._onBack} />}
          titleStyle={Styles.headerTitle}
          title="Join Rent My Wardrobe"
        />
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{ paddingHorizontal: 30 }}
          keyboardShouldPersistTaps="handled">
          <Avatar
            defaultImageSource={source}
            cropperCircleOverlay
            style={styles.imageContainer}
            loadingStyle={styles.loading}
            onSourceChange={this._onSourceChanged}
          />
          <InputGroup style={{ marginTop: 30 }} ref={this._inputGroupRef} onInputSubmit={this._onJoin}>
            {/* <View> */}

            <TextInput
              clearButtonMode="while-editing"
              defaultValue={firstname}
              style={[styles.inputContainer, { position: 'relative' }]}
              inputStyle={styles.input}
              placeholderTextColor={Assets.colors.placeholder}
              selectionColor={Assets.colors.mainText}
              autoCapitalize="words"
              placeholder="First Name"
              underlineWidth={0}
              RightComponent={<TooltipUI message={'Enter first name.'} />
              }
            />
            {/* </View> */}
            <TextInput
              clearButtonMode="while-editing"
              defaultValue={lastname}
              autoCapitalize="words"
              placeholder="Last Name"
              style={styles.inputContainer}
              inputStyle={styles.input}
              selectionColor={Assets.colors.mainText}
              placeholderTextColor={Assets.colors.placeholder}
              underlineWidth={0}
              RightComponent={<TooltipUI message={'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500 when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum'} />
              }
            />
            <TextInput
              clearButtonMode="while-editing"
              defaultValue={username}
              autoCapitalize="none"
              placeholder="Username"
              style={styles.inputContainer}
              inputStyle={styles.input}
              selectionColor={Assets.colors.mainText}
              placeholderTextColor={Assets.colors.placeholder}
              underlineWidth={0}
              RightComponent={<TooltipUI message={'Lorem Ipsum is simply dummy text of the printing and typesetting.'} />}
            />
            <TextInput
              clearButtonMode="while-editing"
              defaultValue={email}
              style={styles.inputContainer}
              inputStyle={styles.input}
              autoCapitalize="none"
              keyboardType="email-address"
              placeholder="Email Address"
              selectionColor={Assets.colors.mainText}
              placeholderTextColor={Assets.colors.placeholder}
              underlineWidth={0}
              RightComponent={<TooltipUI message={'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500 when an unknown printer took a galley of type and scrambled it to make a type specimen book.'} />}
            />

            <TextInput
              LeftComponent={<Text style={styles.codeText} text="+1" />}
              clearButtonMode="while-editing"
              defaultValue={phoneNumber}
              keyboardType="phone-pad"
              placeholder="Mobile Number"
              style={styles.inputContainer}
              inputStyle={[styles.input]}
              selectionColor={Assets.colors.mainText}
              placeholderTextColor={Assets.colors.placeholder}
              underlineWidth={0}
              RightComponent={<TooltipUI message={'Enter valid mobile number.'} />}
            />
          </InputGroup>
          <View>
            {/* {this.state.openBulbIcon &&  */}
            <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'flex-end' }}>
              <TouchableOpacity
                ref={this.setRef1}
                activeOpacity={0.9}
                // onPress={this.showGuideAgain}
                onPress={this.handleOpen}
              >
                {/* {this._renderPopoverGuidAgain()} */}
                {/* {this._renderItemGuid()} */}
                <View style={{ borderRadius: 50, backgroundColor: "#fee8ef", width: 50 }}>
                  <Icon
                    //  onPress={this.showGuideAgain}
                    onPress={this.handleOpen}
                    iconStyle={{ width: 30, height: 30 }} iconSource={Assets.images.buttonBulb} />
                </View>

              </TouchableOpacity>
            </View>
            {/* } */}

          </View>
          <TouchableOpacity
            // ref={React.createRef()}
            style={{ position: 'relative' }}
            ref={this.setRef}
            activeOpacity={0.9}
            onPress={this.showGuideAgain}>
            <View style={{}}>
              <PopoverUI message={'Enter the required information and click on Join to register.'} isOpenPopoverVisible={true} ref={this.state.ref}
              />
            </View>

            <Button style={{ marginTop: 10 }} text="Join" onPress={this._onJoin} />
            {/* {this._renderPopover()} */}

          </TouchableOpacity>

          {this._renderDisclaimer()}
          {this._renderNewModal()}

          <Text style={styles.loginButtonText} text="I have an account" onPress={this._onLogin} />
          {/* <TouchableOpacity onPress={this.handleOpen}>
          <Text>Open</Text>
        </TouchableOpacity>  */}
          {this._renderItemGuid()}


        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    marginBottom: 10,
    backgroundColor: Assets.colors.inputBg,
    borderColor: 'transparent',
    borderRadius: 6
  },
  input: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    height: 44,
    marginLeft: 10,
    textAlignVertical: 'center',
    paddingTop: 4
  },

  disclaimerText: {
    fontFamily: Assets.fonts.text.regular,
    color: Assets.colors.textLight,
    fontSize: 14
  },
  disclaimerButton: {
    fontFamily: Assets.fonts.text.regular,
    color: Assets.colors.appTheme,
    fontSize: 14
  },
  disclaimerView: {
    marginTop: 20,
    alignItems: 'center'
  },
  disclaimerButtonView: {
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 5
  },
  imageContainer: {
    marginTop: 30,
    width: 100,
    height: 100,
    borderRadius: 50,
    marginBottom: 30,
    alignSelf: 'center'
  },
  loading: {
    width: 100,
    height: 100,
    borderRadius: 50
  },
  codeText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.codeColor,
    marginLeft: 10,
    marginTop: 1
  },

  userImage: {
    width: 100,
    height: 100,
    borderRadius: 50
  },

  loginButtonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 18,
    color: Assets.colors.appTheme,
    marginTop: 30,
    marginBottom: 50,
    alignSelf: 'center'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    textAlign: 'center',
    paddingTop: 30,
    backgroundColor: '#307ecc',
    padding: 16,
  },
  touchable: {
    width: '10%',
    height: 40,
    padding: 10,
    // backgroundColor: '#f5821f',
    marginTop: 30,
    // position:'absolute',
    // right:20,
    // top:0,
    // zIndex:999999
  },
  touchableText: {
    color: 'white',
    textAlign: 'center',
  },
  largeText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 20,
    marginTop: 10,
  },
  negativeButton: {
    backgroundColor: 'white',
    borderRadius: 6,
    borderWidth: 1,
    borderColor: Assets.colors.borderColor
  },
  negativeButtonText: {
    color: '#5dd19a',
    fontWeight: '600',
    fontFamily: Assets.fonts.display.regular,
    fontSize: 16,
    paddingVertical: 8
  },
  // container: {
  //   flex: 1,
  //   alignItems: "center",
  //   justifyContent: "center",
  // },
  cover: {
    backgroundColor: "rgba(0,0,0,.5)",
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    position: 'absolute',
    left: -30,
    top: -20
  },
  sheet: {
    position: "absolute",
    top: Dimensions.get("window").height,
    left: 0,
    right: 0,
    bottom: 0,
    height: "100%",
    justifyContent: "flex-end",
  },
  popup: {
    backgroundColor: "#FFF",
    marginHorizontal: 10,
    // borderTopLeftRadius: 5,
    // borderTopRightRadius: 5,
    // alignItems: "center",
    // justifyContent: "center",
    minHeight: 80,
    position: 'absolute',
    left: -11,
    width: Dimensions.get("window").width,
    borderTopStartRadius: 40,
    borderTopEndRadius: 40
  }, title: {
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.display.bold,
    marginHorizontal: 10
  },
  detailText: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2,
    lineHeight: 18
  },
})

export default SignUp
