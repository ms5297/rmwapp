import React from 'react'
import { View, Keyboard } from 'react-native'
import { Header, Text, Row, StyleSheet, ScrollView } from 'rn-components'
import { Button, BackButton } from '@Components'
import { NavigationProps } from '@Types'
import { Navigator } from '@Navigation'
import { Authentication, FirebaseHelper, AppsFlyerHelper, GoogleAnalyticsHelper } from '@Services'
import { OTPValidationParam } from '@Models'
import { getStore, AppConfigurationActions } from '@ReduxManager'
import Assets from '@Assets'
import CodeInput from 'react-native-confirmation-code-field'

type Props = {
  navigation: NavigationProps
}

type State = OTPValidationParam & {
  code: string
}
class OTPValidation extends React.Component<Props, State> {
  _inputRef = React.createRef<CodeInput>()

  constructor(props: Props) {
    super(props)
    const param: OTPValidationParam = this.props.navigation.getParam('param')
    this.state = {
      code: '',
      ...param
    }
  }

  _onFulfill = (code: string) => {
    this.setState({ code })
    this._verifyOTP(code)
  }

  _onVerifyOTP = () => {
    const { code } = this.state
    this._verifyOTP(code)
  }

  _verifyOTP = (code: string) => {
    Keyboard.dismiss()
    const { confirmationResult, phone, onShouldCreateNew } = this.state

    if (confirmationResult === null || phone === null || confirmationResult === undefined) {
      Navigator.showToast('Error', '(OT - 47) Internal Error', 'Error')
      return
    }

    Navigator.showLoading()

    confirmationResult
      .confirm(code)
      .then(user => {
        console.log('**** user', user)

        return Authentication.loginAndCreateSession(user)
      })
      .then(isOK => {
        this.setState({ code: '' })

        if (isOK) {
          Navigator.hideLoading()
          const { userProfile } = getStore().getState()
          const isAdmin = userProfile.role === 'admin'
          AppConfigurationActions.setAdminLogin(isAdmin)

          if (isAdmin) {
            Navigator.navTo('Admin')
          } else {
            // vinayak@fathomable on 04 nov 2019
            AppsFlyerHelper.trackLogin(userProfile)
            GoogleAnalyticsHelper.trackEvent(GoogleAnalyticsHelper.EVENT_CATEGORY.MAIN, GoogleAnalyticsHelper.EVENT.LOGIN, { user_id: userProfile.id })
            Navigator.navTo('Home')
          }
        } else {
          if (typeof onShouldCreateNew === 'function') {
            Navigator.back()
            onShouldCreateNew()
          } else {
            Navigator.hideLoading()
            Navigator.back()
            Navigator.navTo('SignUp', { phone, isLoginWithFacebook: false, needAuthorizePhone: false })
          }
        }
      })
      .catch(error => {
        this.setState({ code: '' })
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  _resendCode = () => {
    const { phone } = this.state

    if (phone === null) {
      Navigator.showToast('Error', '(OT - 91) Internal Error', 'Error')
      return
    }
    Navigator.showLoading()
    FirebaseHelper.resendOTP(phone)
      .then(response => {
        this.setState({ confirmationResult: response })

        Navigator.hideLoading()
        Navigator.showToast('Success', 'Your OTP code is sent.', 'Success')
      })
      .catch(error => {
        console.warn('**** error', error)
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  render() {
    const { phone } = this.state

    return (
      <View style={StyleSheet.absoluteFill}>
        <Header LeftComponent={<BackButton />} />
        <ScrollView style={styles.content} showsVerticalScrollIndicator={false}>
          <Text style={styles.helpText} text={`Enter code to\nverify phone`} />
          <Text style={styles.infoText} text={`We just texted you a 6 digit code, just\nenter it below.`} />
          <CodeInput
            containerProps={{ style: { flex: 0, marginTop: 20 } }}
            ref={this._inputRef}
            autoFocus
            cellBorderWidth={1}
            codeLength={6}
            activeColor={Assets.colors.appTheme}
            inactiveColor={Assets.colors.borderColorDark}
            onFulfill={this._onFulfill}
            cellProps={{
              style: styles.otpText
            }}
            size={50}
            ratio={0.9}
          />
          <Button
            style={{ marginTop: 60, marginHorizontal: 30 }}
            type="solid"
            text="Verify Phone"
            onPress={this._onVerifyOTP}
          />
          <Row style={{ marginTop: 21, marginHorizontal: 30 }} alignHorizontal="space-between" alignVertical="center">
            <Text style={styles.phoneInfoText} text={`sent to +1${phone}`} />
            <Text style={styles.resendButtonText} text="Resend Code" onPress={this._resendCode} />
          </Row>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    marginBottom: 20
  },
  helpText: {
    marginTop: 25,
    marginHorizontal: 30,
    fontSize: 36,
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    width: '100%'
  },
  infoText: {
    marginTop: 15,
    marginHorizontal: 30,
    fontSize: 16,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.regular,
    width: '100%'
  },
  otpText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 24,
    textAlign: 'center',
    color: Assets.colors.mainText,
    borderRadius: 4
  },
  content: {
    flex: 1,
    marginTop: 20
  },

  verifyButton: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: 48,
    marginHorizontal: 10,
    marginTop: 10,
    borderRadius: 6,
    overflow: 'hidden',
    backgroundColor: Assets.colors.buttonColor
  },

  verifyButtonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14,
    color: 'white'
  },
  phoneInfoText: {
    color: Assets.colors.textLight,
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14
  },
  resendView: {
    marginTop: 21,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  resendButtonText: {
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14
  }
})

export default OTPValidation
