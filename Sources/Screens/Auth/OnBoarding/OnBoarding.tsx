import React from 'react'
import { View, ImageBackground, StatusBar } from 'react-native'
import { Text, Row, StyleSheet } from 'rn-components'
import { Navigator } from '@Navigation'
import { Button } from '@Components'
import { Authentication, FirebaseHelper, FacebookHelper } from '@Services'
import { FacebookUserInfo } from '@Models'
import Assets from '@Assets'

class OnBoarding extends React.Component {
  _handleFBLogin = () => {
    Navigator.showLoading()
    let facebookUserInfo: FacebookUserInfo

    FacebookHelper.loginAndGetUserData()
      .then(user => {
        return user
      })
      .then(user => {
        facebookUserInfo = user
        return FirebaseHelper.loginWithFacebook(user.token)
      })
      .then(userCredentails => {
        if (userCredentails && userCredentails.user) {
          return Authentication.loginAndCreateSession(userCredentails.user)
        } else {
          return false
        }
      })
      .then(isOK => {
        Navigator.hideLoading()
        if (isOK) {
          Navigator.navTo('Home')
        } else {
          Navigator.navTo('SignUp', {
            first_name: facebookUserInfo.first_name,
            last_name: facebookUserInfo.last_name,
            email: facebookUserInfo.email ? facebookUserInfo.email : '',
            photo: 'https://graph.facebook.com/' + facebookUserInfo.id + '/picture?type=large',
            isLoginWithFacebook: true
          })
        }
      })
      .catch(error => {
        Navigator.hideLoading()
        if (typeof error.message == 'string' && error.message != '') {
          Navigator.showToast('Error', error.message, 'Error')
        }
      })
  }

  _onSignUp = () => Navigator.navTo('SignUp')

  _onLogin = () => Navigator.navTo('Login')

  render() {
    return (
      <ImageBackground source={Assets.images.app_bg} style={styles.backgroundImage} resizeMode="cover">
        <StatusBar barStyle="light-content" />
        <View style={{ marginTop: 10, marginHorizontal: 30, marginBottom: 30 }}>
          <Button
            style={{ borderColor: 'white' }}
            textStyle={{ color: 'white' }}
            type="outline"
            text="Join with Facebook"
            onPress={this._handleFBLogin}
          />
          <Button
            style={{ marginTop: 25, backgroundColor: 'white' }}
            type="solid"
            textStyle={{ color: Assets.colors.appTheme }}
            text="Continue with Phone"
            onPress={this._onSignUp}
          />
          <Row style={{ marginBottom: 30, marginTop: 25 }} alignVertical="center">
            <Text style={styles.questionText} text="Already a member?" />
            <Text style={styles.loginText} text="Login" onPress={this._onLogin} />
          </Row>
        </View>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-end'
  },
  questionText: {
    color: 'white',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14
  },
  loginText: {
    fontSize: 14,
    fontFamily: Assets.fonts.display.bold,
    color: 'white',
    marginLeft: 15
  }
})

export default OnBoarding
