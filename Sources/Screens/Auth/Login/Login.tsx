import React from 'react'
import { Keyboard, View, StatusBar } from 'react-native'
import { Header, TextInput, Text, StyleSheet, ScrollView } from 'rn-components'
import { FacebookHelper, Api, FirebaseHelper, Authentication } from '@Services'
import { BackButton, Button } from '@Components'
import { Navigator } from '@Navigation'
import { Validator, Device } from '@Utils'
import { FacebookUserInfo, OTPValidationParam } from '@Models'
import { NavigationProps } from '@Types'
import { getStore, AppConfigurationActions } from '@ReduxManager'
import Assets from '@Assets'
import Styles from '@Styles'

type Props = {
  navigation: NavigationProps
}
type State = {
  phoneNumber: string
}
class Login extends React.Component<Props, State> {
  _phoneInputRef = React.createRef<TextInput>()

  constructor(props) {
    super(props)
    const phoneNumber = this.props.navigation.getParam('phoneNumber', '')
    this.state = { phoneNumber }
  }

  _getPhoneNumber = () => {
    if (this._phoneInputRef.current) {
      return this._phoneInputRef.current.getText()
    }
    return ''
  }

  _onLogin = () => {
    const phoneNumber = this._getPhoneNumber()
    if (Validator.validatePhoneNumber(phoneNumber) == false) {
      Navigator.showToast('Error', 'Please enter a valid phone number.', 'Error')
      return
    }
    Keyboard.dismiss()

    this._handlePhoneLogin(phoneNumber)
    // const phone = '+1' + phoneNumber
    // Api.login(phone)
    //   .then((response: any) => {
    //     console.log('**** response', response)
    //     return response.data
    //   })
    //   .then(data => {
    //     if (data) {
    //       if (data.sign_in_provider === 'facebook.com') {
    //         this._handleFBLogin()
    //       }
    //     } else {
    //       this._handlePhoneLogin(phoneNumber)
    //     }
    //   })
    //   .catch(error => {
    //     Navigator.hideLoading()
    //     Navigator.showToast('Error', error.message, 'Error')
    //   })
  }

  _handleFBLogin = () => {
    let facebookUserInfo: FacebookUserInfo
    Navigator.showLoading()
    FacebookHelper.loginAndGetUserData()
      .then(user => {
        return user
      })
      .then(user => {
        facebookUserInfo = user
        return FirebaseHelper.loginWithFacebook(user.token)
      })
      .then(userCredentails => {
        if (userCredentails && userCredentails.user) {
          return Authentication.loginAndCreateSession(userCredentails.user)
        } else {
          return false
        }
      })
      .then(isOK => {
        Navigator.hideLoading()
        if (isOK) {
          const { userProfile } = getStore().getState()
          const isAdmin = userProfile.role === 'admin'
          AppConfigurationActions.setAdminLogin(isAdmin)

          if (isAdmin) {
            Navigator.navTo('Admin')
          } else {
            Navigator.navTo('Home')
          }
        } else {
          Navigator.navTo('SignUp', {
            firstname: facebookUserInfo.first_name,
            lastname: facebookUserInfo.last_name,
            email: facebookUserInfo.email ? facebookUserInfo.email : '',
            photo: 'https://graph.facebook.com/' + facebookUserInfo.id + '/picture?type=large',
            isLoginWithFacebook: true
          })
        }
      })
      .catch(error => {
        Navigator.hideLoading()
        if (typeof error.message == 'string' && error.message != '') {
          Navigator.showToast('Error', error.message, 'Error')
        }
      })
  }

  _handlePhoneLogin = (phone: string) => {
    Navigator.showLoading()
    FirebaseHelper.loginWithPhone('+1' + phone)
      .then(response => {
        Navigator.hideLoading()
        const param: OTPValidationParam = {
          confirmationResult: response,
          phone
        }
        Navigator.navTo('OTP', { param })
      })
      .catch(error => {
        console.log('**** error', error)
        Navigator.hideLoading()

        if (!error || error.message !== 'string') return

        if (error.message.indexOf('cancelled by the user') != -1) {
          Navigator.showToast('Error', error.message, 'Error')
        }
      })
  }

  _onBack = () => {
    Navigator.back()
    Device.isAndroid() && StatusBar.setBackgroundColor('rgba(0,0,0,0.0)', false)
    StatusBar.setBarStyle('dark-content')
  }

  render() {
    return (
      <View style={[StyleSheet.absoluteFill, { backgroundColor: 'white' }]}>
        <Header
          statusBarProps={{ barStyle: 'dark-content', backgroundColor: Assets.colors.statusBar }}
          LeftComponent={<BackButton onPress={this._onBack} />}
          title="Login to your account"
          titleStyle={Styles.headerTitle}
        />
        <ScrollView style={styles.content} showsVerticalScrollIndicator={false}>
          <Button
            type="outline"
            onPress={this._handleFBLogin}
            style={{ marginTop: 50 }}
            iconSource={Assets.images.logoFacebook}
            text="Login with facebook"
          />
          <Text style={styles.orText} text="OR" />
          <Text style={styles.loginInfoText} text="Login with phone" />

          <TextInput
            style={styles.inputContainer}
            inputStyle={styles.input}
            defaultValue={this.state.phoneNumber}
            ref={this._phoneInputRef}
            LeftComponent={<Text style={styles.codeText} text="+1" />}
            clearButtonMode="while-editing"
            placeholderTextColor={Assets.colors.placeholder}
            keyboardType="phone-pad"
            placeholder="Phone Number"
            underlineWidth={0}
            underlineColor="transparent"
            onSubmitEditing={this._onLogin}
          />
          <Button type="solid" onPress={this._onLogin} style={{ marginTop: 95 }} text="Login" />
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    marginHorizontal: 30,
    marginTop: 20
  },
  codeText: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: '#45515470',
    marginHorizontal: 10,
    marginTop: 1
  },
  orText: {
    marginTop: 15,
    color: Assets.colors.textLight,
    fontSize: 14,
    fontFamily: Assets.fonts.text.regular,
    alignSelf: 'center',
    textAlign: 'center'
  },
  inputContainer: {
    marginTop: 8,
    backgroundColor: Assets.colors.inputBg,
    borderColor: 'transparent',
    borderRadius: 6,
    alignItems: 'center'
  },
  input: {
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    height: 44,
    marginTop: 4,
    textAlignVertical: 'center'
  },

  loginInfoText: {
    marginTop: 10,
    width: '100%',
    color: Assets.colors.mainText,
    fontSize: 14,
    fontFamily: Assets.fonts.text.regular
  }
})

export default Login
