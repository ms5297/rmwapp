import {
  NavigationContainerComponent,
  NavigationActions,
  NavigationParams,
  StackActions,
  NavigationNavigateAction,
  DrawerActions
} from 'react-navigation'
import { StatusBarStyle } from 'react-native'
import { ToastType } from 'rn-notifier'

export type RouteName =
  | 'Admin'
  | 'Start'
  | 'Authentication'
  | 'Home'
  | 'OnBoarding'
  | 'Login'
  | 'SignUp'
  | 'Closet'
  | 'CategoryItemList'
  | 'PaymentMethods'
  | 'AdminInbox'
  | 'AdminChat'
  | 'UserList'
  | 'Settings'
  | 'EditProfile'
  | 'AddCreditCard'
  | 'OTP'
  | 'WebsiteView'
  | 'AddItem'
  | 'Details'
  | 'PayoutReminder'
  | 'PayoutMethodsInfo'
  | 'Notifications'
  | 'PayoutMethods'
  | 'PayoutMethodsConfirm'
  | 'PayoutMethodsView'
  | 'Filters'
  | 'Details'
  | 'Checkout'
  | 'CheckoutReview'
  | 'SelectionList'
  | 'Reviews'
  | 'Reporting'
  | 'PhotoViewer'
  | 'AvailabilityCalendar'
  | 'EditProfile'
  | 'EditProfileScreenModelStack'
  | 'PayoutAccountTypes'
  | 'PayoutAccountInfo'
  | 'PayoutAccountAddress'
  | 'ActionFinal'
  | 'ViewCreditCard'
  | 'Chat'
  | 'WebsiteView1'
  | 'RentalRating'
  | 'RentalRatingDetail'
  | 'RentalItemDetails'
  | 'ListerRentalRating'
  | 'ReturnItem'
  | 'ReturnItemDetail'
  | 'ReturnItemMessageLender'
  | 'ReturnItemMessageSent'
  | 'CheckoutBuyNow'
  | 'BuyNouMsg'
  | 'ReturnPhoto'
  | 'PublicProfile'
  | 'ContactLister'
  | 'PlacePicker'
  | 'MessageSuccess'
  | 'RequestSuccess'
  | 'Permissions'
  | 'LocationPermission'
  | 'PromotionCodeAdd'
  | 'PromotionCode'
  | 'PromotionCodeDetail'
  | 'PromotionCodeInfo'
  | 'UpdateInfo'
  | 'UpdateBirthday'
  | 'UpdateLocation'
  | 'UpdateLocationInfo'
  | 'UpdateSize'
  | 'TurnOnNotifications'
  | 'ForceUpdateInfo'
  | 'Inbox'
  | 'ForceUpdateBankAccount'
  | 'ChangePhoneNumber'
  | 'VerifyOTP'
  | 'CheckoutItemDetail'
  | 'RequestSize'
  | 'ComingSoon'
  | 'EnterAccessCode'
  | 'CreateAccessCode'
  | 'NotificationWarning'

type AnimationType = 'slideFromLeft' | 'slideFromBottom'

type ParamsDefaultProps = {
  animationType?: AnimationType
  tabBarVisible: boolean
  swipeBackEnabled: boolean
  drawerLockMode: 'unlocked' | 'locked-open' | 'locked-closed'
}

type ParamsProps = NavigationParams & Partial<ParamsDefaultProps>

type NavigationContainer = NavigationContainerComponent | null

class Navigator {
  private static instance = new Navigator()
  private isCheckedCodePush = false
  private rootNavigator: NavigationContainer = null

  constructor() {
    if (Navigator.instance) {
      throw new Error('Error: Instantiation failed: Use Navigation.getInstance() instead of new.')
    }
    Navigator.instance = this
  }
  public static getInstance(): Navigator {
    return Navigator.instance
  }
  public getRoot() {
    return this.rootNavigator
  }
  public setRoot = (rootNavigator: NavigationContainer | null) => {
    if (this.rootNavigator) return
    this.rootNavigator = rootNavigator
  }

  public navTo = (routeName: RouteName, params?: ParamsProps, action?: NavigationNavigateAction, key?: string) => {
    if (this.rootNavigator) {
      this.rootNavigator.dispatch(NavigationActions.navigate({ routeName, params, action, key }))
    }
  }

  public setTabbar(forScreen: string, isVisible: boolean = true) {
    const params = { tabBarVisible: isVisible }
    if (this.rootNavigator) {
      this.rootNavigator.dispatch(NavigationActions.setParams({ params, key: forScreen }))
    }
  }

  public back = (key?: string | null) => {
    if (this.rootNavigator) {
      this.rootNavigator.dispatch(NavigationActions.back({ key }))
    }
  }

  public reset = (routeName: RouteName, params?: NavigationParams, action?: NavigationNavigateAction, key?: string) => {
    if (this.rootNavigator) {
      this.rootNavigator.dispatch(
        StackActions.reset({
          index: 0,
          key: null,
          actions: [NavigationActions.navigate({ routeName, params, action, key })]
        })
      )
    }
  }

  public popToTop = (key?: RouteName, immediate?: boolean) => {
    if (this.rootNavigator) {
      this.rootNavigator.dispatch(StackActions.popToTop({ key, immediate }))
    }
  }

  public push = (routeName: RouteName, params?: NavigationParams, action?: NavigationNavigateAction, key?: string) => {
    if (this.rootNavigator) {
      this.rootNavigator.dispatch(StackActions.push({ routeName, params, action, key }))
    }
  }

  public pop = (n?: number, immediate?: boolean) => {
    if (this.rootNavigator) {
      this.rootNavigator.dispatch(StackActions.pop({ n, immediate }))
    }
  }

  public closeDrawer() {
    if (this.rootNavigator) {
      this.rootNavigator.dispatch(DrawerActions.closeDrawer())
    }
  }

  public openDrawer() {
    if (this.rootNavigator) {
      this.rootNavigator.dispatch(DrawerActions.openDrawer())
    }
  }

  public toggleDrawer() {
    if (this.rootNavigator) {
      this.rootNavigator.dispatch(DrawerActions.toggleDrawer())
    }
  }

  public getScreenProps() {
    if (this.rootNavigator) {
      return this.rootNavigator.props.screenProps
    }
    return null
  }

  public showLoading(msg?: string) {
    if (this.rootNavigator && this.rootNavigator.props.screenProps) {
      const { showLoading } = this.rootNavigator.props.screenProps
      if (showLoading) showLoading(msg)
    }
  }

  public hideLoading(onClose?: () => void) {
    if (this.rootNavigator && this.rootNavigator.props.screenProps) {
      const { hideLoading } = this.rootNavigator.props.screenProps
      if (hideLoading) hideLoading(onClose)
    }
  }
  public showAlert = (
    title: string,
    msg: string,
    onOk?: () => void,
    onCancel?: () => void,
    okButtonText?: string,
    cancelButtonText?: string
  ) => {
    if (this.rootNavigator && this.rootNavigator.props.screenProps) {
      const { showAlert } = this.rootNavigator.props.screenProps
      if (showAlert) showAlert(title, msg, onOk, onCancel, okButtonText, cancelButtonText)
    }
  }

  public showToast = (
    title: string,
    message: string,
    type?: ToastType,
    duration?: number,
    onShow?: () => void,
    onHide?: () => void,
    isDisableInteraction = false,
    activeStatusBarType: StatusBarStyle = 'light-content',
    deactiveStatusBarType: StatusBarStyle = 'dark-content'
  ) => {
    if (this.rootNavigator && this.rootNavigator.props.screenProps) {
      const { showToast } = this.rootNavigator.props.screenProps
      if (showToast)
        showToast(
          title,
          message,
          type,
          duration,
          onShow,
          onHide,
          isDisableInteraction,
          activeStatusBarType,
          deactiveStatusBarType
        )
    }
  }

  public hideToast = () => {
    if (this.rootNavigator && this.rootNavigator.props.screenProps) {
      const { hideToast } = this.rootNavigator.props.screenProps
      if (hideToast) hideToast()
    }
  }
}

export default Navigator.getInstance()
