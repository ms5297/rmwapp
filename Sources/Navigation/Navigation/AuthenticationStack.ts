import { createStackNavigator } from 'react-navigation'
import {
  Login,
  OTPValidation,
  SignUp,
  OnBoarding,
  AppIntro,
  UpdateBirthday,
  UpdateLocation,
  UpdateLocationInfo,
  UpdateSize,
  TurnOnNotifications,
  WebsiteView,
  LocationPermission,
  EnterAccessCode,
  ComingSoon
} from '@Screens'
import getSlideFromRightTransitionConfig from './transitionConfig'
import { PlacePicker, SelectionList } from '@Components'

const PlacePickerStack = createStackNavigator(
  {
    PlacePicker: PlacePicker,
    LocationPermission: LocationPermission
  },
  {
    initialRouteName: 'PlacePicker',
    mode: 'modal',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const UpdateInfoStack = createStackNavigator(
  {
    UpdateBirthday: UpdateBirthday,
    UpdateLocation: UpdateLocation,
    UpdateLocationInfo: UpdateLocationInfo,
    UpdateSize: UpdateSize,
    TurnOnNotifications: TurnOnNotifications,
    PlacePicker: PlacePickerStack,
    SelectionList: SelectionList,
    ComingSoon: ComingSoon,
    EnterAccessCode: EnterAccessCode
  },
  {
    initialRouteName: 'UpdateBirthday',
    headerMode: 'none',
    defaultNavigationOptions: {
      swipeEnabled: false,
      gesturesEnabled: false
    },
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    },
    transitionConfig: getSlideFromRightTransitionConfig
  }
)

const AuthenticationStack = createStackNavigator(
  {
    AppIntro: AppIntro,
    OnBoarding: OnBoarding,
    SignUp: SignUp,
    Login: Login,
    OTP: OTPValidation,
    UpdateInfo: UpdateInfoStack,
    WebsiteView: WebsiteView
  },
  {
    transitionConfig: getSlideFromRightTransitionConfig,
    initialRouteName: 'AppIntro',
    defaultNavigationOptions: {
      header: null,
      swipeEnabled: false,
      gesturesEnabled: false
    }
  }
)

export default AuthenticationStack
