import { AvailabilityCalendar, PhotoViewer, PlacePicker, SelectionList } from '@Components'
import {
  Permissions,
  Favorites,
  Closet,
  AddItem,
  PayoutReminder,
  Inbox,
  Chat,
  RentalItemDetails,
  RentalRating,
  ReturnItem,
  CheckoutBuyNow,
  BuyNouMsg,
  ReturnPhoto,
  AddCreditCard,
  EditProfile,
  Profile,
  PublicProfile,
  Settings,
  ViewCreditCard,
  WebsiteView,
  Notifications,
  PaymentMethods,
  AdminInbox,
  AdminChat,
  UserList,
  PayoutAccountAddress,
  PayoutAccountInfo,
  PayoutAccountTypes,
  PayoutMethodAddFinal,
  PayoutMethods,
  PayoutMethodsConfirm,
  PayoutMethodsInfo,
  PayoutMethodsView,
  CategoryItemList,
  Checkout,
  CheckoutReview,
  ContactLister,
  Details,
  Filters,
  Home,
  MessageSuccess,
  PromotionCodeDetail,
  PromotionCodeInfo,
  Reporting,
  RequestSuccess,
  Reviews,
  ChangePhoneNumber,
  PromotionCodeAdd,
  ForceUpdateInfo,
  ForceUpdateBankAccount,
  VerifyOTP,
  ReturnItemDetail,
  LocationPermission,
  ReturnItemMessageSent,
  ReturnItemMessageLender,
  RentalRatingDetail,
  RentalRatingComplete,
  CheckoutItemDetail,
  RequestSize,
  ExistingUserComingSoon,
  EnterAccessCode,
  CreateAccessCode,
  NotificationWarning
} from '@Screens'

import { createStackNavigator, createBottomTabNavigator } from 'react-navigation'
import getSlideFromRightTransitionConfig from './transitionConfig'
import { BottomTabBar } from '@Components'

const PlacePickerStack = createStackNavigator(
  {
    PlacePicker: PlacePicker,
    LocationPermission: LocationPermission
  },
  {
    initialRouteName: 'PlacePicker',
    mode: 'modal',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const MultiItemStackModal = createStackNavigator(
  {
    Details: Details,
    Reporting: Reporting,
    PhotoViewer: PhotoViewer,
    AvailabilityCalendar: AvailabilityCalendar,
    WebsiteView1: WebsiteView,
    RequestSize: RequestSize
  },
  {
    initialRouteName: 'Details',
    mode: 'modal',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const MultiItemStack = createStackNavigator(
  {
    PublicProfile: PublicProfile,
    Details: MultiItemStackModal
  },
  {
    initialRouteName: 'PublicProfile',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const CheckoutModelStack = createStackNavigator(
  {
    Checkout: Checkout,
    AddCreditCard: AddCreditCard,
    MultiItem: MultiItemStack,
    CheckoutItemDetail: CheckoutItemDetail
  },
  {
    initialRouteName: 'Checkout',
    mode: 'modal',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const PaymentMethodModelStack = createStackNavigator(
  {
    PaymentMethods: PaymentMethods,
    AddCreditCard: AddCreditCard
  },
  {
    initialRouteName: 'PaymentMethods',
    mode: 'modal',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const AdminInboxModelStack = createStackNavigator(
  {
    AdminInbox: AdminInbox,
    AdminChat: AdminChat,
    UserList: UserList
    // AddCreditCard: AddCreditCard
  },
  {
    initialRouteName: 'AdminInbox',
    mode: 'modal',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const PayoutMethodModelStack = createStackNavigator(
  {
    PayoutMethods: PayoutMethods,
    PayoutMethodsInfo: PayoutMethodsInfo
  },
  {
    initialRouteKey: 'PayoutMethods',
    mode: 'modal',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const ForceUpdateBankAccountStack = createStackNavigator(
  {
    ForceUpdateBankAccount: ForceUpdateBankAccount,
    PayoutMethods: PayoutMethods,
    PayoutAccountInfo: PayoutAccountInfo,
    PayoutMethodsInfo: PayoutMethodsInfo,
    PayoutAccountAddress: PayoutAccountAddress,
    PayoutMethodsConfirm: PayoutMethodsConfirm,
    ActionFinal: PayoutMethodAddFinal
  },
  {
    initialRouteName: 'ForceUpdateBankAccount',
    transitionConfig: getSlideFromRightTransitionConfig,
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const ComingSoonStack = createStackNavigator(
  {
    ComingSoon: ExistingUserComingSoon,
    EnterAccessCode: EnterAccessCode
  },
  {
    initialRouteName: 'ComingSoon',
    transitionConfig: getSlideFromRightTransitionConfig,
    headerMode: 'none',
    defaultNavigationOptions: {
      swipeEnabled: false,
      gesturesEnabled: false,
      header: null
    },
    navigationOptions: {
      gesturesEnabled: false
    }
  }
)

const HomeScreenModelStack = createStackNavigator(
  {
    Home: Home,
    Filters: Filters,
    Permissions: Permissions,
    AvailabilityCalendar: AvailabilityCalendar,
    ForceUpdateInfo: ForceUpdateInfo,
    ForceUpdateBankAccount: ForceUpdateBankAccountStack,
    ComingSoon: ComingSoonStack,
    NotificationWarning: NotificationWarning
  },
  {
    initialRouteName: 'Home',
    mode: 'modal',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    },
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
)

const ClosetScreenModelStack = createStackNavigator(
  {
    Closet: Closet,
    AddItem: AddItem,
    AvailabilityCalendar: AvailabilityCalendar,
    PayoutReminder: PayoutReminder
  },
  {
    initialRouteName: 'Closet',
    mode: 'modal',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const CategoryScreenModelStack = createStackNavigator(
  {
    CategoryItemList: CategoryItemList,
    Filters: Filters,
    AvailabilityCalendar: AvailabilityCalendar
  },
  {
    initialRouteName: 'CategoryItemList',
    mode: 'modal',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const RequestSizeStack = createStackNavigator(
  {
    RequestSize: RequestSize,
    AvailabilityCalendar: AvailabilityCalendar
  },
  {
    initialRouteName: 'RequestSize',
    mode: 'modal',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const DetailsScreenModelStack = createStackNavigator(
  {
    Details: Details,
    Reviews: Reviews,
    Reporting: Reporting,
    PhotoViewer: PhotoViewer,
    AvailabilityCalendar: AvailabilityCalendar,
    WebsiteView1: WebsiteView
  },
  {
    initialRouteName: 'Details',
    mode: 'modal',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const PublicProfileScreenModelStack = createStackNavigator(
  {
    PublicProfile: PublicProfile,
    Details: DetailsScreenModelStack,
    Reviews: Reviews,
    RequestSize: RequestSizeStack
  },
  {
    initialRouteName: 'PublicProfile',
    headerMode: 'none',
    transitionConfig: getSlideFromRightTransitionConfig,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const PromotionCodeStack = createStackNavigator(
  {
    PromotionCodeInfo: PromotionCodeInfo,
    PromotionCodeDetail: PromotionCodeDetail
  },
  {
    initialRouteName: 'PromotionCodeInfo',
    headerMode: 'none',
    transitionConfig: getSlideFromRightTransitionConfig,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const CheckoutReviewScreenModelStack = createStackNavigator(
  {
    CheckoutReview: CheckoutReview,
    PaymentMethods: PaymentMethodModelStack,
    WebsiteView1: WebsiteView,
    PromotionCode: PromotionCodeStack,
    PromotionCodeAdd: PromotionCodeAdd
  },
  {
    headerMode: 'none',
    transitionConfig: getSlideFromRightTransitionConfig,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const ContactListerScreenModelStack = createStackNavigator(
  {
    ContactLister: ContactLister,
    AvailabilityCalendar: AvailabilityCalendar,
    MultiItem: MultiItemStack
  },
  {
    headerMode: 'none',
    mode: 'modal',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const HomeModelStack = createStackNavigator(
  {
    Home: HomeScreenModelStack,
    CategoryItemList: CategoryScreenModelStack,
    Details: DetailsScreenModelStack,
    Checkout: CheckoutModelStack,
    CheckoutReview: CheckoutReviewScreenModelStack,
    PublicProfile: PublicProfileScreenModelStack,
    MessageSuccess: MessageSuccess,
    RequestSuccess: RequestSuccess,
    ContactLister: ContactListerScreenModelStack,
    SelectionList: SelectionList,
    PlacePicker: PlacePickerStack,
    RequestSize: RequestSizeStack
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none',
    transitionConfig: getSlideFromRightTransitionConfig,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const FavoritesModelStack = createStackNavigator(
  {
    Favorites: Favorites,
    Details: DetailsScreenModelStack,
    RequestSize: RequestSizeStack,
    Checkout: CheckoutModelStack,
    CheckoutReview: CheckoutReviewScreenModelStack,
    PublicProfile: PublicProfileScreenModelStack,
    MessageSuccess: MessageSuccess,
    RequestSuccess: RequestSuccess,
    ContactLister: ContactListerScreenModelStack
  },
  {
    initialRouteName: 'Favorites',
    headerMode: 'none',
    transitionConfig: getSlideFromRightTransitionConfig,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const ClosetModelStack = createStackNavigator(
  {
    Closet: ClosetScreenModelStack,
    Details: DetailsScreenModelStack,
    RequestSize: RequestSizeStack,
    PublicProfile: PublicProfileScreenModelStack,
    SelectionList: SelectionList,
    PlacePicker: PlacePickerStack,
    Checkout: CheckoutModelStack,
    PayoutMethodModelStack: PayoutMethodModelStack
  },
  {
    initialRouteName: 'Closet',
    headerMode: 'none',
    transitionConfig: getSlideFromRightTransitionConfig,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const RentalRatingStack = createStackNavigator(
  {
    RentalRating: RentalRating,
    RentalRatingDetail: RentalRatingDetail,
    RentalRatingComplete: RentalRatingComplete
  },
  {
    initialRouteName: 'RentalRating',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)
const ReturnItemStack = createStackNavigator(
  {
    ReturnItem: ReturnItem,
    ReturnItemDetail: ReturnItemDetail,
    ReturnPhoto: ReturnPhoto,
    ReturnItemMessageLender: ReturnItemMessageLender,
    ReturnItemMessageSent: ReturnItemMessageSent
  },
  {
    initialRouteName: 'ReturnItem',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const CheckoutBuyNowStack = createStackNavigator(
  {
    CheckoutBuyNow: CheckoutBuyNow,
    
    BuyNouMsg: BuyNouMsg,
    // ReturnItemDetail: ReturnItemDetail,
    // ReturnPhoto: ReturnPhoto,
    // ReturnItemMessageLender: ReturnItemMessageLender,
    // ReturnItemMessageSent: ReturnItemMessageSent
  },
  {
    initialRouteName: 'CheckoutBuyNow',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const ChatModelStack = createStackNavigator(
  {
    Chat: Chat,
    RentalRating: RentalRatingStack,
    ListerRentalRating: RentalRatingDetail,
    ReturnItem: ReturnItemStack,
    CheckoutBuyNow:CheckoutBuyNowStack,
    AddCreditCard: AddCreditCard,
    ForceUpdateBankAccount: ForceUpdateBankAccountStack
  },
  {
    initialRouteName: 'Chat',
    mode: 'modal',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const ActionFinalModelStack = createStackNavigator(
  {
    ActionFinal: PayoutMethodAddFinal,
    RentalRating: RentalRating
  },
  {
    initialRouteName: 'ActionFinal',
    mode: 'modal',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const InboxModelStack = createStackNavigator(
  {
    Inbox: Inbox,
    Chat: ChatModelStack,
    Details: DetailsScreenModelStack,
    RequestSize: RequestSizeStack,
    Checkout: CheckoutModelStack,
    CheckoutReview: CheckoutReviewScreenModelStack,
    ContactLister: ContactListerScreenModelStack,
    PublicProfile: PublicProfileScreenModelStack,
    ActionFinal: ActionFinalModelStack,
    RentalItemDetails: RentalItemDetails,
    MessageSuccess: MessageSuccess,
    RequestSuccess: RequestSuccess,
    WebsiteView1: WebsiteView,
    Reviews: Reviews,
    AdminChat: AdminChat,
  },
  {
    initialRouteName: 'Inbox',
    headerMode: 'none',
    transitionConfig: getSlideFromRightTransitionConfig,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const EditProfileScreenModelStack = createStackNavigator(
  {
    Profile: Profile,
    EditProfile: EditProfile,
    PlacePicker: PlacePickerStack
  },
  {
    initialRouteName: 'Profile',
    mode: 'modal',
    headerMode: 'none',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)


const ProfileModelStack = createStackNavigator(
  {
    Profile: EditProfileScreenModelStack,
    Settings: Settings,
    VerifyOTP: VerifyOTP,
    ChangePhoneNumber: ChangePhoneNumber,
    Notifications: Notifications,
    PaymentMethods: PaymentMethodModelStack,
    ViewCreditCard: ViewCreditCard,
    PayoutMethods: PayoutMethodModelStack,
    PayoutAccountTypes: PayoutAccountTypes,
    PayoutAccountInfo: PayoutAccountInfo,
    PayoutAccountAddress: PayoutAccountAddress,
    PayoutMethodsConfirm: PayoutMethodsConfirm,
    PayoutMethodsView: PayoutMethodsView,
    ActionFinal: PayoutMethodAddFinal,
    WebsiteView: WebsiteView,
    SelectionList: SelectionList,
    CreateAccessCode: CreateAccessCode,
    AdminInbox: AdminInboxModelStack
  },
  {
    initialRouteKey: 'PayoutMethods',
    headerMode: 'none',
    transitionConfig: getSlideFromRightTransitionConfig,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
)

const HomeBottomTabBar = createBottomTabNavigator(
  {
    Home: HomeModelStack,
    Favorites: FavoritesModelStack,
    Closet: ClosetModelStack,
    Inbox: InboxModelStack,
    Profile: ProfileModelStack
  },
  {
    initialRouteName: 'Home',
    tabBarComponent: BottomTabBar
  }
)

export default HomeBottomTabBar
