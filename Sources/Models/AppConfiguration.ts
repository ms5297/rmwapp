export type NotificationSettings = {
  messages_email: boolean
  messages_push: boolean
  promotion_email: boolean
  promotion_push: boolean
  account_email: boolean
  messages_push_reminder?: boolean
}

export type AppConfiguration = {
  firstStart?: boolean
  codePushVersion?: string
  firebaseToken?: any
  freshchatId?: string
  firstRegister: boolean
  isAdminLogin: boolean
}

export type PermissionsState = {
  notification: PermissionStatus
  location: PermissionStatus
}

export type PermissionStatus = 'authorized' | 'denied' | 'restricted' | 'undetermined'
