import { AppConfig } from '@Utils'
import { RNFirebaseConfirmationResult } from '@Services'
import { UserProfile } from './UserStorage'

export type RatingModelItem = {
  rental_id: string | number
  item_id?: string | number
  user_id: string | number
  accuracy: string | number
  communication: string | number
  exchange: string | number
  description: string
  return_condition?: string | number
  return_on_time?: number
  ease_of_scheduling?: string | number
  reasonable_pricing?: string | number
}

export type FirebaseUser = {
  photo: string
  token: string
  userId: number
  userName: string
  role: string
  lastTimeLogged: number
  appVersion: string
  codePushVersion: string
}

export type Location = {
  longitude: number | null
  latitude: number | null
}

export type SearchHistories = {
  keywords: Array<string>
}

export type FirebaseValueObject = {
  value: number
  title: string
  order?: number
}

export type EnumConfigKey = keyof typeof AppConfig.fieldType

export type FirebaseState = {
  chatInboxs: Array<ChatModelItem>
}

export type NotificationData = {
  approval: 'true' | 'false'
  chatID: string
  fromUser: string
  isOwner: 'true' | 'false'
  item: string | number
  message: string
  rental_id: string
  statusChange: 'true' | 'false'
  title: string
  toUser: string
  type: 'chat' | 'like' | 'should_add_bank_account' | 'admin_chat'
  chat_id: string
}

export type NotificationItem = {}

export type BranchParams = {
  rental_id: number
  item_id: number
  item_ids: number
  statusChange: boolean
  approved: boolean
  user_ids: Array<number>
  '+non_branch_link': string
  '+clicked_branch_link': string
  type:
  | 'Share Item'
  | 'Share Profile'
  | 'rental_link'
  | 'item_link'
  | 'share_item'
  | 'share_profile'
  | 'notification_setting'
  user: any
  item: any
}

export type FacebookUserInfo = {
  first_name: string
  id: string
  last_name: string
  email: string
  picture: {
    data: {
      width: number
      height: number
      is_silhouette: boolean
      url: string
    }
  }
  token: string
}

export type ItemOwner = {
  id: number
  name: string
  email: string
  phone: string
  photo: string
  location_id: null | number
  website: string
  latitude: null | number
  longitude: null | number
  locality: string
}

export type ReviewerModel = {
  id: number
  name: string
  email: string
  phone: string
  photo: string
}

export type ReviewModel = {
  accuracy: number
  communication: number
  created_at: Date
  description: string
  ease_of_scheduling: number
  exchange: number
  id: number
  reasonable_pricing: number
  reviewer: ReviewerModel
  updated_at: Date
}

export type Item = {
  avg_review: number
  cleaning_fee: number
  cleaning_option_id: number
  color_id: number | null
  condition_id: number | null
  embellishments: Array<any>
  fit_id: number | null
  hemline_id: number | null
  id: number | null
  is_favorite: boolean
  latitude: number | null
  longitude: number | null
  locality: string
  location_id: number | null
  name: string
  neckline_id: number | null
  occasions: Array<number>
  owner: ItemOwner
  photos: Array<{ id: number; url: string }>
  photo?: { id: number; url: string }
  refund_option_id: number
  rental_fee: number
  replacement_fee: number
  retail_price: number
  reviews_count: number
  size_id: number
  sleeve_id: number | null
  style_id: number | null
  tags: Array<string>
  unavailable_dates: Array<{ id: number; date: string }>
  review?: ReviewModel
  description?: string
  editable?: boolean
  monthly_discount: number
  multi_week_discount: number
  auto_smart_pricing: boolean
  additional_fee?: AdditionalFeeModel
  size_ids?: Array<number>
}

export type UserItems = Array<Item>

export type ItemSummary = {
  id: number
  items: Array<Item>
}
//Added by mahesh for item types
export type CategoryList = {
  value: number
  title: string,
  imgUrl: string
}
export type ItemType = {
  id: number
  items: string
}
export type ItemTypes = Array<ItemType>
export type CategoryLists = Array<CategoryList>
export type ItemsSummary = Array<ItemSummary>


export type ItemList = Array<Item>

export type StripePaymentMethod = {
  id: string
  object: string
  address_city: null | string
  address_country: null | string
  address_line1: null | string
  address_line1_check: null | string
  address_line2: null | string
  address_state: null | string
  address_zip: null | string
  address_zip_check: null | string
  brand: string
  country: string
  customer: string
  cvc_check: string
  dynamic_last4: null | string
  exp_month: number
  exp_year: number
  fingerprint: string
  funding: string
  last4: string
  metadata: object
  name: null | string
  tokenization_method: null | string
}

export type StripePaymentMethods = Array<StripePaymentMethod>

export type StripePaymentInfo = {
  account_balance: number
  address: string | null
  created: number
  currency: string | null
  default_source: string
  delinquent: boolean
  description: string | null
  discount: string | null
  email: string
  id: string
  invoice_prefix: string
  invoice_settings: {
    custom_fields: string | null
    default_payment_method: string | null
    footer: string | null
  }
  livemode: boolean
  metadata: object
  name: string | null
  object: string
  phone: string | null
  preferred_locales: []
  shipping: string | null
  sources: {
    object: string
    data: Array<any>
    has_more: boolean
    total_count: number
    url: string
  }
  subscriptions: {
    object: string
    data: Array<any>
    has_more: boolean
    total_count: number
    url: string
  }
  tax_exempt: 'none'
  tax_ids: {
    object: string
    data: Array<any>
    has_more: boolean
    total_count: number
    url: string
  }
  tax_info: string | null
  tax_info_verification: string | null
}

export type PaymentReduxState = {
  paymentMethods: StripePaymentMethods | []
  paymentInfo: StripePaymentInfo | null
}

export type StripePayoutMethod = {
  id: string
  object: string
  account: string
  account_holder_name: string
  account_holder_type: string
  bank_name: string
  country: string
  currency: string
  default_for_currency: boolean
  fingerprint: string
  last4: string
  metadata: any
  routing_number: string
  status: string
  isDefault: boolean
}

export type StripePayoutMethods = Array<StripePayoutMethod>

export type FilterKeyActions = 'size' | 'priceRange' | 'locationRange' | 'occasionsList' | 'availability' | FilterKey

export type FilterKey = EnumConfigKey | 'date' | 'occasion' | 'dressSize' | 'category'

export type FilterKeyValues = { [key in FilterKey]: number }

export type SortTypeModel = {
  field: 'created_at' | 'rental_fee'
  order: 'desc' | 'asc'
}

export type FilterParamsModel = {
  filters?: Partial<{
    keyword: string
    location: Location | null
    location_range: number
    min_price: number
    max_price: number
    size: Array<number>
    occasion: Array<number>
    style: Array<number>
    color: Array<number>
    // fit
    v: Array<number>
    sleeve: Array<number>
    hemline: Array<number>
    embellishment: Array<number>
    neckline: Array<number>
    availability_start: string
    availability_end: string
    locality?: string,
    itemType?: number
  }>
  sort?: SortTypeModel
  page?: number
  per_page?: number
}

export type FilterState = {
  categories: FilterParamsModel
  itemList: FilterParamsModel
  userItems: {
    id: number
    page: number
    per_page: number
  }
}

export type SelectionListParam = {
  selectedItems: Array<FirebaseValueObject>
  removedItemTitles?: Array<string>
  title: string
  onItemsSelected: (items: Array<FirebaseValueObject>) => void
  mutiple?: boolean
  maximumSelectedItems?: number
  type: EnumConfigKey
}

export type PlacePickerParam = {
  onLocationSelected: (location: Location, address: string) => void
  title?: string
}

export type CalendarParam = {
  mode: 'blocking' | 'selecting' | 'viewing'
  onDateSelected: (dates: Array<string>) => void
  selectedDates?: Array<string>
  blockedDates?: Array<string>
  bookedDates?: Array<string>
  initialRange?: Array<string>
  multiWeekDiscount?: number
  monthlyDiscount?: number
  autoSmartPricing?: boolean
}

export type AdditionalFeeModel = {
  rental_fee: number
  replacement_fee: number
  service_fee?: number
  insurance_fee: number
  cleaning_fee?: number
  insurance?: number
}

export type CheckoutParam = {
  selectedDates: Array<string>
  items?: Array<Item>
  confirming?: boolean
  selectedSizeId?: number
}

export type CheckoutReviewParam = CheckoutParam & {
  card: StripePaymentMethod
  message: string
  onItemRemoved?: (item: Item) => void
  payableFee?: number
}

export type PromotionCodeModel = {
  id: string
  object: string
  amount_off: number | null
  created: number
  currency: string | null
  duration: string
  duration_in_months: string | null
  livemode: false
  max_redemptions: string | null
  metadata: {
    description?: string
  }
  name: string
  percent_off: number
  percent_off_precise: number
  redeem_by: string | null
  times_redeemed: number
  valid: boolean
}

export type PromotionCodeParam = {
  promoCode: PromotionCodeModel
  onRemoveCode: () => void
}

export type PromotionCodeAddParam = {
  promoCodeList: Array<PromotionCodeModel>
  credits: number
  ids: Array<number>
  onAddCodeSuccess: (promoCode: string, fees: any) => void
  deliveryPoromoCodeList: Array<PromotionCodeModel>
  onDeliveryCodeSuccess: (promoCode: string) => void
}

// export type UpdateBirthdayParam = {
//   firstname: string;
//   lastname: string;
//   email: string;
//   phone: string;
//   url: string;
//   payment_customer_id: number;
//   birthday: Date | string;
// };

// export type UpdateSizeParam = {
//   birthday: string
// }
export type UpdateLocationParam = {
  birthday: string
}

// export type UpdateLocationParam = UpdateSizeParam & {
//   backup_size_id: number | null
//   dress_size_id: number | null
//   shoe_size_id: number | null
//   bra_size: string
//   height: string
//   weight: string
// }
export type UpdateSizeParam = UpdateLocationParam & {
  longitude: number | null
  latitude: number | null
  locality: string | null
  isInDallas: boolean
}
// export type UpdateNotificationParam = UpdateLocationParam & {
//   longitude: number | null
//   latitude: number | null
//   locality: string | null
//   isInDallas: boolean
// }
export type UpdateNotificationParam = UpdateSizeParam & {
  backup_size_id: number | null
  dress_size_id: number | null
  shoe_size_id: number | null
  bra_size: string
  height: string
  weight: string
}

export type OTPValidationParam = {
  phone: string
  confirmationResult: RNFirebaseConfirmationResult
  onShouldCreateNew?: () => void
}

export type VerifyOTPParam = {
  onSuccess?: (code: string) => void
  phone: string
}

export type UpdateKeys = 'date' | 'sizes' | 'shoeSizes' | 'braSizes' | 'height' | 'weight' | 'market' | 'backupSizes'

export type ForceUpdateInfoParam = {
  updateKeys: { [key in UpdateKeys]: boolean }
  onUpdateDone?: (userProfile: UserProfile) => void
}

export type PermissionsParam = {
  permissionType: 'location' | 'notification'
  onDidClosed?: () => void
  onUpdateDone?: (userProfile: UserProfile) => void
}

export type ChatUserModel = {
  id: number
  photo: string
  name: string
}

export type InboxItemProps = {
  isArchived: boolean
  index: number
  item: ChatModelItem
  onTapSwipeItem: (item: ChatModelItem, index: number) => Promise<boolean>
  onTapChatItem: (item: ChatModelItem, index: number) => void
  onTapAvatar: (user: ChatUserModel, index: number) => void
}

export type RentalStatus =
  | ''
  | 'request'
  | 'completed'
  | 'auto_cancel'
  | 'renting'
  | 'reviewed'
  | 'approved'
  | 'inquiry'
  | 'accepted'
  | 'returned'
  | 'buy'
  | 'purchased'
  | 'declined'
  | 'cancelled'
  | 'rental_complete'
  | 'cancel_without_penalty'
  | 'rent'
  | 'approve'
  | 'review'
  | 'buynow_request'
  | 'buynow_accepted'
  | 'buynow_declined'
  | 'buynow_cancel'

export type MessageModelItem = {
  image: any
  isCustom: boolean
  isImage: boolean
  messageId: string
  sender: number
  text: string
  time: number
}

export type ChatModelOrderItem = {
  itemId: number
  endDate: string
  startDate: string
  itemName: string
  firstMessage: string
  date: string
  hasCleaning: boolean
  itemSize: string
  rentalPrice: number
  status: RentalStatus
  rating: number
}

export type ChatModelItem = {
  archived: boolean
  createdAt: number
  fromUser: ChatUserModel
  id: string
  itemId: number
  itemOwner: boolean
  lastMessage: string
  lastMessageTime: number
  order: ChatModelOrderItem
  rental_id: number
  status: RentalStatus
  statusDate: number
  toUser: ChatUserModel
  unread: number
  itemIds: Array<number>
  orders: Array<ChatModelOrderItem>
  chat_id: string
}

export type ChatParam = Partial<{
  selectedDates: Array<string>
  date: Date
  message: string
  statusChange: boolean
  fromPush: boolean
  rental_id: number
  item: { id: number }
  approval: boolean
  chatID: string
  fromUser: number
  isOwner: boolean
  title: string
  toUser: number
  type: 'chat' | 'line'
}>

export type RentalRentingParam = {
  data: ChatModelItem
  order: Item
  itemOwner?: any
  onCompletion?: () => void
  onAction?: (status: RentalStatus) => void
}

export type RentalDetailFeesModel = {
  rental_fee: number
  replacement_fee: number
  cleaning_fee: number
  insurance_fee: number
  service_fee: number
  coupon_amount: number
  coupon_percentage: number
  credits: number
  payable_fee: number
  total_payout: number
}

export type RentalDetailItem = {
  avg_review: number
  description: string
  id: number
  name: string
  owner: ReviewerModel
  photo: { id: number; url: string }
  refund_option_id: number
  reviews_count: number
  size_id: number
  rental_fee: number
  replacement_fee: number
  rmw_rental_fee: number
  service_fee: number
  buy_now: boolean
}

export type RentalDetailModel = {
  fees: RentalDetailFeesModel
  lister_fees: RentalDetailFeesModel
  cancel_date: string
  cancelled_by: number | string | null
  confirm_date: string
  created_at: string
  dates: Array<string>
  decline_date: string
  dynamic_link: string
  hold_amount: number
  id: number
  inquiry_date: string
  items: Array<RentalDetailItem>
  lister: ReviewerModel & {
    location_id: null | number
    rating: number
    website: string
    items: Array<any>
  }
  renter: ReviewerModel & {
    location_id: null | number
    rating: number
    website: string
  }
  request_date: string
  return_date: string
  review_date: string
  status: RentalStatus
  updated_at: string,
  free_delivery: boolean
}

export type MessageSuccessParam = {
  items: Array<Item>
  selectedDates: Array<string>
  date: string
  additionalFee: AdditionalFeeModel
  message: string
}

export type ContactListerParam = {
  items: Array<Item>
  additionalFee: AdditionalFeeModel
}

export type RequestSuccessParam = {
  rentalId: number
  itemId: number
}

export type ReturnPhotoModel = {
  image: string
  title: string
}
export type ReturnPhotoParam = {
  order: any
  onReturnItem: (photos: Array<ReturnPhotoModel>) => void
}

export type ReturnItemParam = {
  chatData: ChatModelItem
  onReturnItem: (
    photos: Array<ReturnPhotoModel>,
    message: string,
    item: ChatModelOrderItem,
    isComplete: boolean
  ) => Promise<ChatModelItem>
  onReviewItem: (item: ChatModelOrderItem, isComplete: boolean) => Promise<ChatModelItem>
  onMessageLender: (message: string) => Promise<void>
  onRetryUpdateReturnItem: (chatData: ChatModelItem) => Promise<void>
  onRetryUpdateReviewItem: (chatData: ChatModelItem) => Promise<void>
  onCloseAction: () => void
}

export type ReturnItemDetailParam = {
  order: ChatModelOrderItem
  onReturnItem: (photos: Array<ReturnPhotoModel>, message: string) => void
  item?: Item
  title?: string
}

export type ReturnItemMessageLenderParam = {
  onMessageLender: (message: string) => Promise<void>
  onReviewItem: (item: ChatModelOrderItem, isComplete: boolean) => Promise<ChatModelItem>
  onRetryUpdateReturnItem: (chatData: ChatModelItem) => Promise<void>
  onRetryUpdateReviewItem: (chatData: ChatModelItem) => Promise<void>
  lender: string
  chatData: ChatModelItem
  items?: Item[]
}

export type ReturnItemMessageSentParam = {
  onReviewItem: (item: ChatModelOrderItem, isComplete: boolean) => Promise<ChatModelItem>
  chatData: ChatModelItem
  items?: Item[]
  onRetryUpdateReviewItem: (chatData: ChatModelItem) => Promise<void>
}

export type RentalRatingParam = {
  type?: string
  chatData: ChatModelItem
  rentalDetail: RentalDetailModel
  items?: Item[]
  onReviewItem?: (item: ChatModelOrderItem, isComplete: boolean) => Promise<ChatModelItem>
  onRetryUpdateReviewItem: (chatData: ChatModelItem) => Promise<void>
}

export type RentalRatingDetailParam = {
  chatData: ChatModelItem
  rentalDetail: RentalDetailModel
  item: Item
  onReviewed: (avgRating: number) => void
  title: string
}

export type RentalItemModel = Item & {
  onRemoved?: () => void
  onPressed?: () => void
  showTotalPrice?: boolean
  additional_fees?: AdditionalFeeModel
}

export type RentalCollectionModel = {
  items: Array<Item>
  showTotalPrice?: boolean
}

export type RentalItemDetailsParam = {
  chatData: ChatModelItem
  rentalDetail: RentalDetailModel
  onAction: (action: RentalStatus) => void,
  purchaseAction: (text: any, image: any, isCustom: any,
    sendPush: any, statusChange: any, pushMessage: any, approval: any, ) => void
}

export type RequestSizeParam = {
  item: Item
  onSizeSelected: (selectedSize: number) => void
  fromMultiItemScreen?: boolean
  selectedDates?: Array<string>
}

export type ComingSoonParam = {
  title: string
  message: string
  buttonText: string
  onPressed: () => void
  onCodeAddedSuccess: (code: string) => void
  isHandleJoinWaitlist?: boolean
}

export type EnterAccessCodeParam = {
  onCodeAddedSuccess: (code: string) => void
}
