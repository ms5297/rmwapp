export type UserProfile = {
  id: number
  name: string
  firstname: string
  lastname: string
  email: string
  phone: string
  photo: string
  token: string
  foreign_id: string
  about_me: string
  user_name: string
  birthday: string
  location_id: null | number
  height: string
  weight: string
  dress_size_id: null | number
  shoe_size_id: null | number
  backup_size_id: null | number
  bra_size: string
  payment_customer_id: string
  website: string
  payment_account_id: string
  facebook: string
  instagram: string
  twitter: string
  locality: string
  latitude: number | null
  longitude: number | null
  sign_in_provider: string
  role: 'admin' | 'user'
  should_add_bank_account: boolean
  rating_renter?: number | null
  rating?: number
  accessibility: boolean
  is_in_waitlist: boolean
}

export type UserDatabase = {
  data: any
}

export type UserToken = {
  about_me: string
  birthday: string | null
  bra_size: string | null
  dress_size_id: null | number
  email: string
  facebook: string
  firstname: string
  foreign_id: string
  height: string
  id: number
  instagram: string
  lastname: string
  latitude: number
  locality: string
  location_id: null | string
  longitude: number
  payment_account_id: string
  payment_customer_id: string
  phone: string
  photo: string
  shoe_size_id: null | number
  sign_in_provider: string
  token: string
  twitter: string
  user_name: string
  website: string
  weight: string
  is_in_waitlist: boolean
}

export type BadgeCount = {
  adminBadgeCount: number,
  userBadgeCount?: number | 0
}
