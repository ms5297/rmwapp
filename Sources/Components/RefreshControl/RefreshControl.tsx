import React from 'react'
import { RefreshControl as RNRefreshControl, RefreshControlProps, Platform } from 'react-native'
import Assets from '@Assets'

const RefreshControl: React.SFC<RefreshControlProps> = props => {
  return <RNRefreshControl {...props} />
}

RefreshControl.defaultProps = {
  ...Platform.select({
    android: { colors: [Assets.colors.appTheme] }
  })
}

export default RefreshControl
