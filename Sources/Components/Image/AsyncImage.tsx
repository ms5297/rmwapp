import React from 'React'
import { View, StyleSheet, Animated, ActivityIndicator, Image } from 'react-native'

import FastImage from 'react-native-fast-image'

const AnimatedImage = Animated.createAnimatedComponent(FastImage)

import { AsyncImageProps } from '@Types'
import { Validator } from '@Utils'

type Props = AsyncImageProps
type State = {
  loading: boolean
}

class AsyncImage extends React.Component<Props, State> {
  private animatedValue = new Animated.Value(0)
  constructor(props: Props) {
    super(props)
    this.state = {
      loading: true
    }
  }

  onLoad = () => {
    this.setState({ loading: false }, () => {
      Animated.timing(this.animatedValue, {
        toValue: 1,
        duration: 350,
        useNativeDriver: true
      }).start()
    })
  }

  render() {
    const { loading } = this.state
    const {
      style,
      source,
      loadingStyle,
      width: _width,
      height: _height,
      placeholderSource,
      fallbackImageSource,
      placeholderStyle,
      children
    } = this.props
    const animationStyle = {
      opacity: this.animatedValue
    }
    const width = (style && StyleSheet.flatten(style).width) || _width
    const height = (style && StyleSheet.flatten(style).height) || _height
    const borderRadius = (style && StyleSheet.flatten(style).borderRadius) || 0

    let displayFallbackImage =
      source === null ||
      (typeof source === 'string' && source === '') ||
      (typeof source === 'object' && (source['uri'] === '' || source['uri'] === null))

    return (
      <View style={[styles.container, style]}>
        {displayFallbackImage && fallbackImageSource !== null ? (
          <Image resizeMode="contain" source={fallbackImageSource} />
        ) : (
          <AnimatedImage
            onLoad={this.onLoad}
            style={[styles.image, { width, height, borderRadius }, animationStyle]}
            source={source}
            resizeMode="cover"
          />
        )}

        {loading && !displayFallbackImage && (
          <View style={[styles.loading, loadingStyle]}>
            {placeholderSource === undefined || placeholderSource === null ? (
              <ActivityIndicator size="small" />
            ) : (
              // @ts-ignore
              <FastImage style={placeholderStyle} source={placeholderSource} />
            )}
          </View>
        )}
        {children}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    width: '100%',
    height: '100%'
  },
  loading: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default AsyncImage
