import React from 'react'
import {
  View,
  ImageSourcePropType,
  Animated,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  Image
} from 'react-native'
import { UploadImageViewProps } from '@Types'
import ActionSheet from 'react-native-actionsheet'
import ImagePicker from 'react-native-image-crop-picker'
import Assets from '@Assets'
import { Navigator } from '@Navigation'

type Props = UploadImageViewProps

type State = {
  source?: ImageSourcePropType | null
  loading: boolean
  animatedValue: Animated.Value
}

const options = ['Cancel', 'Camera', 'Photo Library']

class UploadImageView extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    const { defaultImageSource } = props
    this.state = {
      loading: true,
      animatedValue: new Animated.Value(0),
      source: defaultImageSource || null
    }
  }

  static defaultProps: Props = {
    editable: true
  }

  static getDerivedStateFromProps(props: Props, state: State) {
    if (props.defaultImageSource !== state.source) {
      return { source: props.defaultImageSource }
    }
    return state
  }
  show = () => this.showActionSheet()

  private actionSheetRef = React.createRef<ActionSheet>()

  private onDone = () => {
    this.setState({ loading: false }, () => {
      Animated.spring(this.state.animatedValue, {
        toValue: 1,
        useNativeDriver: true,
        tension: 10,
        friction: 10
      }).start()
    })
  }

  private onStart = () => {
    this.setState({ loading: true }, () => {
      this.state.animatedValue.setValue(0)
    })
  }

  private showActionSheet = () => {
    if (this.actionSheetRef.current) {
      this.actionSheetRef.current.show()
    }
  }

  private onSelectFromCamera = () => {
    const { maxImageHeight = 400, maxImageWidth = 400 } = this.props
    ImagePicker.openCamera({
      cropping: true,
      width: maxImageWidth,
      height: maxImageHeight,
      compressImageMaxHeight: maxImageHeight,
      compressImageMaxWidth: maxImageWidth,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperCircleOverlay: this.props.cropperCircleOverlay,
      showCropGuidelines: true,
      cropperCancelText: 'Cancel',
      cropperChooseText: 'Chose',
      avoidEmptySpaceAroundImage: true,
      waitAnimationEnd: false
    })
      .then(image => {
        if (Array.isArray(image)) return
        const source = {
          uri: image.path,
          width: image.width,
          height: image.height,
          mine: image.mime
        }
        if (this.props.onSourceChange) {
          this.props.onSourceChange(source)
        }
        this.setState({ source })
      })
      .catch(error => Navigator.showToast('Error', error.message, 'Error'))
  }

  private onSelectFromAlbum = () => {
    const { maxImageHeight = 200, maxImageWidth = 200 } = this.props
    ImagePicker.openPicker({
      cropping: true,
      multiple: false,
      width: maxImageWidth,
      height: maxImageHeight,
      compressImageMaxHeight: maxImageHeight,
      compressImageMaxWidth: maxImageWidth,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperCircleOverlay: this.props.cropperCircleOverlay,
      showCropGuidelines: true,
      cropperCancelText: 'Cancel',
      cropperChooseText: 'Choose',
      avoidEmptySpaceAroundImage: true,
      waitAnimationEnd: true
    })
      .then(image => {
        if (Array.isArray(image)) return
        const source = {
          uri: image.path,
          width: image.width,
          height: image.height,
          mine: image.mime
        }

        if (this.props.onSourceChange) {
          this.props.onSourceChange(source)
        }
        this.setState({ source })
      })
      .catch(e => console.warn('error: ', e))
  }

  private onClearCurrentImage = () => {
    this.setState({ source: Assets.images.user_photo_placeholder }, () => {
      if (this.props.onSourceChange) {
        this.props.onSourceChange(undefined)
      }
      ImagePicker.clean()
    })
  }

  private onActionSheetSelect = (index: number) => {
    switch (index) {
      case 1:
        this.onSelectFromCamera()
        break
      case 2:
        this.onSelectFromAlbum()
        break

      default:
        break
    }
  }

  private renderActionSheet = () => {
    return (
      <ActionSheet
        ref={this.actionSheetRef}
        options={options}
        cancelButtonIndex={0}
        onPress={this.onActionSheetSelect}
      />
    )
  }

  private renderContentComponent = () => {
    const { ContentComponent } = this.props

    if (ContentComponent && React.isValidElement(ContentComponent))
      return <TouchableOpacity onPress={this.showActionSheet}>{ContentComponent}</TouchableOpacity>

    if (ContentComponent && typeof ContentComponent === 'function') {
      const c: Function = ContentComponent
      return <TouchableOpacity onPress={this.showActionSheet}>{c()}</TouchableOpacity>
    }

    const {
      defaultImageSource,
      editable,
      style,
      imageStyle,
      loadingStyle,
      editButtonStyle,
      fallbackImageSource = Assets.images.defaultProfile
    } = this.props
    const { source, loading, animatedValue } = this.state
    const imageSource = source ? source : defaultImageSource || Assets.images.defaultProfile
    const displayFallbackImage =
      source === null ||
      (typeof source === 'string' && source === '') ||
      (typeof source === 'object' && (source['uri'] === '' || source['uri'] === null))

    const width = (style && StyleSheet.flatten(style).width) || '100%'
    const height = (style && StyleSheet.flatten(style).height) || '100%'
    const borderRadius = (style && StyleSheet.flatten(style).borderRadius) || 0

    return (
      <TouchableOpacity style={[styles.container, style]} disabled={!editable} onPress={this.showActionSheet}>
        {imageSource && (
          <View style={styles.imageContainer}>
            {displayFallbackImage && fallbackImageSource !== null ? (
              <Image resizeMode="contain" source={fallbackImageSource} />
            ) : (
              <Animated.Image
                resizeMode="cover"
                style={[styles.imageContainer, { opacity: animatedValue, width, height, borderRadius }, imageStyle]}
                onLoadStart={this.onStart}
                onLoad={this.onDone}
                onError={this.onDone}
                source={imageSource}
              />
            )}
          </View>
        )}
        {loading && (
          <View style={[styles.loading, loadingStyle]}>
            <ActivityIndicator size="small" />
          </View>
        )}
        {editable && (
          <View style={[styles.editContainer, editButtonStyle]}>
            <Image style={{ width: 24, height: 24 }} source={Assets.images.camera_button} />
          </View>
        )}
      </TouchableOpacity>
    )
  }
  render() {
    const { style } = this.props
    return (
      <View style={[style]}>
        {this.renderContentComponent()}
        {this.renderActionSheet()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  touchable: {},
  container: {
    width: 114,
    borderRadius: 57,
    aspectRatio: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  imageContainer: {
    width: 114,
    borderRadius: 57,
    aspectRatio: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  editContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    width: 24,
    height: 24,
    borderRadius: 12,
    alignItems: 'center',
    justifyContent: 'center'
  },
  loading: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: Assets.colors.borderColor,
    borderWidth: 1,
    width: 114,
    borderRadius: 57
  }
})

export default UploadImageView
