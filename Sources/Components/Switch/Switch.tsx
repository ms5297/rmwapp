import React from 'react'
import { Switch as RNSwitch, SwitchProps } from 'react-native'
import { Device } from '@Utils'
import Assets from '@Assets'

const isAndroid = Device.isAndroid()

const Switch: React.SFC<SwitchProps> = props => {
  return (
    <RNSwitch
      trackColor={{
        true: props.disabled ? Assets.colors.textLight : Assets.colors.appTheme,
        false: Assets.colors.appTheme
      }}
      tintColor={isAndroid ? Assets.colors.borderColorDark : Assets.colors.textLight}
      onTintColor={isAndroid ? Assets.colors.borderColorDark : Assets.colors.appTheme}
      thumbColor={isAndroid ? Assets.colors.borderColorDark : 'white'}
      thumbTintColor={isAndroid ? Assets.colors.borderColorDark : 'white'}
      ios_backgroundColor={
        isAndroid ? Assets.colors.borderColorDark : props.disabled ? 'white' : Assets.colors.textLight
      }
      {...props}
    />
  )
}

export default Switch
