import React, { Component } from 'react'
import { Keyboard, View, TouchableHighlight, StyleSheet, ImageSourcePropType, StatusBar, TouchableOpacity, Animated, Dimensions, Image, ImageBackground, Text, Alert } from 'react-native'
import Popover from 'react-native-popover-view';
import { Device } from '@Utils';
import Assets from '@Assets';
import { Icon } from 'rn-components';

type Props = {
    isOpenPopoverVisible: boolean,
    message:string
    ref:any
}

type State = {
    isOpenPopoverVisible:boolean
    ref:any
}
class PopoverUI extends React.Component<Props, State> {
  constructor(props: Props){
    super(props);
    const {message, isOpenPopoverVisible, ref } = this.props
    this.state = {isOpenPopoverVisible :false, ref:null}
    // this.setState({isOpenPopoverVisible : isOpenPopoverVisible})
   console.log(this.props)
  }
  componentDidMount(){
    const {message, isOpenPopoverVisible, ref } = this.props
    setTimeout(()=>{
      this.setState({isOpenPopoverVisible : isOpenPopoverVisible , ref:ref})
    }, 1000)
    

  }
  closeCoachMark = () =>{
    this.setState({isOpenPopoverVisible: false})
  }
    render(){
      const {message, isOpenPopoverVisible} = this.props
        return(
                  <Popover
                        arrowStyle={{backgroundColor:'#fff', width:30, height:15,borderRadius:5}}
                        fromView={this.state.ref}
                        placement="bottom"
                        backgroundStyle={{
                          backgroundColor: 'rgba(0,0,0,.35)',
                        }}
                        isVisible={this.state.isOpenPopoverVisible}
                        popoverStyle={{width:200,padding:20, position:'relative',overflow:'visible',borderRadius:5,shadowColor:'transparent'}}
                        >
                           
                        <Text>{message}</Text>
                        <View style={{position:'absolute',top:-15,left:-15,zIndex:99999}}>
                          <TouchableOpacity>
                            <Icon iconStyle={{ width:35, height:35,marginTop:-17}} iconSource={Assets.images.buttonCross} 
                            onPress={this.closeCoachMark}
                            />
                        </TouchableOpacity>
                          </View>
                          <View style={{width: 0, height: 0,backgroundColor: 'transparent',borderStyle: 'solid',borderLeftWidth: 10,
                            borderRightWidth: 10,
                            borderBottomWidth: 10,
                            borderLeftColor: 'transparent',
                            borderRightColor: 'transparent',
                            borderBottomColor: '#fff', transform: [
                              {rotate: '180deg'}
                            ], position:'absolute', bottom:-10,zIndex:9999, justifyContent:'center', alignItems:'center', flexDirection:'row', alignSelf:'center'}}
                            ></View>
                      </Popover>
        )
    }
}
export default PopoverUI;