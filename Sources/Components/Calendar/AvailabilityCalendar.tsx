import React from 'react'
import { View, StyleSheet } from 'react-native'
import { CalendarList, DateObject } from 'react-native-calendars'
import { Header, Text, Row } from 'rn-components'
import { NavigationProps } from '@Types'
import { DateTime, Device, AppConfig, Formater } from '@Utils'
import { CalendarParam } from '@Models'
import { Navigator } from '@Navigation'
import Assets from '@Assets'
import BackButton from '../Button/BackButton'
import Button from '../Button/Button'
import Styles from '@Styles'

type Props = {
  theme?: { markColor: string; markTextColor: string }
  navigation?: NavigationProps
}

type State = CalendarParam & {
  isFromDatePicked: boolean
  isToDatePicked: boolean
  markedDates: {
    [key: string]: any
  }
  showCalendar: boolean
  fromDate: string | null
}

type MarkedDatesType = {
  [key: string]: Partial<{
    disabled: boolean
    booked: true
    endingDay: boolean
    selected: boolean
    marked: boolean
    dotColor: string
    activeOpacity: number
    startingDay: boolean
    color: string
    textColor: string
  }>
}

class AvailabilityCalendar extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    const param: CalendarParam = props.navigation.getParam('param')
    this.state = {
      isFromDatePicked: false,
      isToDatePicked: false,
      markedDates: {},
      showCalendar: false,
      fromDate: null,
      ...param
    }
  }

  static defaultProps: Props = {
    theme: { markColor: Assets.colors.appTheme, markTextColor: '#ffffff' }
  }

  componentWillMount() {
    Navigator.showLoading()
    const { blockedDates, selectedDates, bookedDates } = this.state
    const key = DateTime.moment().format('YYYY-MM-DD')
    let markedDates: MarkedDatesType = {
      [key]: {
        marked: true,
        dotColor: Assets.colors.appTheme,
        activeOpacity: 1,
        disabled: false,
        startingDay: false,
        endingDay: false
      }
    }

    if (selectedDates && selectedDates.length > 0) {
      for (let index = 0; index < selectedDates.length; ++index) {
        markedDates[selectedDates[index]] = {
          startingDay: index === 0 ? true : false,
          color: Assets.colors.appTheme,
          selected: true,
          textColor: '#ffffff',
          endingDay: index > 0 && index === selectedDates.length - 1 ? true : false
        }
      }
    }

    if (bookedDates && bookedDates.length > 0) {
      for (let index = 0; index < bookedDates.length; ++index) {
        markedDates[bookedDates[index]] = { booked: true }
      }
    }

    if (blockedDates && blockedDates.length > 0) {
      for (let index = 0; index < blockedDates.length; ++index) {
        markedDates[blockedDates[index]] = { disabled: true }
      }
    }

    this.setState({ markedDates })

    this._setupInitialRange()
    Navigator.hideLoading()
  }

  _setupStartMarker = (day: any) => {
    let markedDates: MarkedDatesType = {}
    const { theme } = this.props
    const { blockedDates } = this.state
    if (Array.isArray(blockedDates) && blockedDates.length > 0) {
      for (let index = 0; index < blockedDates.length; ++index) {
        if (day.dateString === blockedDates[index]) {
          return
        }
        markedDates[blockedDates[index]] = { disabled: true }
      }
    }
    markedDates[day.dateString] = {
      startingDay: true,
      selected: true,
      color: theme.markColor,
      textColor: theme.markTextColor
    }
    this.setState({
      isFromDatePicked: true,
      isToDatePicked: false,
      fromDate: day.dateString,
      markedDates: markedDates
    })
  }

  _setupMarkedDates = (fromDate: string, toDate: string, markedDates: MarkedDatesType) => {
    let newMarkedDates = { ...markedDates }
    const { theme } = this.props
    let range = DateTime.diffDays(toDate, fromDate)

    if (range >= 0) {
      if (range == 0) {
        newMarkedDates = {
          [toDate]: {
            selected: true,
            color: theme.markColor,
            textColor: theme.markTextColor
          }
        }
      } else {
        const period = Math.floor(range / 7) + (range % 7 == 0 ? 0 : 1)
        range = period * 7

        for (var i = 1; i < range; i++) {
          const tempDate = DateTime.moment(fromDate)
            .add(i, 'days')
            .format('YYYY-MM-DD')

          if (i < range - 1) {
            newMarkedDates[tempDate] = {
              color: theme.markColor,
              selected: true,
              // textColor: theme.markTextColor
              textColor: Assets.colors.appTheme
            }
          } else {
            newMarkedDates[tempDate] = {
              endingDay: true,
              selected: true,
              color: theme.markColor,
              // textColor: theme.markTextColor
              textColor: Assets.colors.appTheme
            }
          }
        }
      }
    }
    return {
      markedDates: newMarkedDates,
      range
    }
  }

  _setupInitialRange = () => {
    const { initialRange } = this.state

    if (!initialRange) return

    const [fromDate, toDate] = initialRange
    const markedDates = {
      [fromDate]: {
        startingDay: true,
        selected: true,
        color: this.props.theme.markColor,
        textColor: this.props.theme.markTextColor
      }
    }
    const { markedDates: mMarkedDates } = this._setupMarkedDates(fromDate, toDate, markedDates)
    this.setState({ markedDates: mMarkedDates ? mMarkedDates : {}, fromDate: fromDate })
  }

  _handleDoneButtonPress = () => {
    const { mode, onDateSelected, markedDates } = this.state

    switch (mode) {
      case 'viewing':
        break
      case 'blocking':
        let blockedDates: Array<string> = []

        Object.keys(markedDates).forEach(date => {
          const obj = markedDates[date]
          if (obj && obj.disabled) {
            blockedDates.push(date)
          }
        })

        if (typeof onDateSelected === 'function') {
          onDateSelected(blockedDates)
        }
        Navigator.back()
        break
      case 'selecting':
        let selectedDates: Array<string> = []
        Object.keys(markedDates).forEach(date => {
          const obj = markedDates[date]
          if (obj && obj.selected) {
            selectedDates.push(date)
          }
        })

        if (selectedDates.length < AppConfig.minimumRentDays) {
          Navigator.showToast('Error', 'Rentals are limited to minimum of one week.', 'Error')
        } else {
          if (typeof onDateSelected === 'function') {
            onDateSelected(selectedDates)
          }
          Navigator.back()
        }
        break
    }
  }

  _handleDayPress = (date: DateObject) => {
    const { isFromDatePicked, isToDatePicked, fromDate, markedDates: markedDatesAlias, mode } = this.state

    let markedDates = { ...markedDatesAlias }

    if (mode === 'blocking') {
      if (markedDates[date.dateString]) {
        let marking = markedDates[date.dateString]
        if (marking.disabled == true) {
          markedDates[date.dateString] = { disabled: false }
        } else {
          markedDates[date.dateString] = { disabled: true }
        }
      } else {
        markedDates[date.dateString] = { disabled: true }
      }
      this.setState({ markedDates })
    } else if (mode === 'selecting') {
      const { blockedDates } = this.state
      if (!isFromDatePicked || (isFromDatePicked && isToDatePicked)) {
        this._setupStartMarker(date)
      } else if (!isToDatePicked) {
        const { markedDates: _markedDates, range } = this._setupMarkedDates(fromDate, date.dateString, markedDates)
        if (Array.isArray(blockedDates) && blockedDates.length > 0) {
          Object.keys(_markedDates).forEach((key, index) => {
            if (blockedDates.includes(key)) {
              _markedDates[key] = { disabled: true }
            }
          })
        }

        if (range >= 0) {
          this.setState({
            isFromDatePicked: true,
            isToDatePicked: true,
            markedDates: _markedDates
          })
        } else {
          this._setupStartMarker(date)
        }
      }
    }
  }

  _renderWeeks = () => {
    const weeks = ['S', 'M', 'T', 'W', 'T', 'F', 'S']

    return (
      <View style={styles.weekContainer}>
        {weeks.map((week, index) => (
          <Text key={index} style={styles.weekDayText} text={week} />
        ))}
      </View>
    )
  }

  _renderDiscountText() {
    const { multiWeekDiscount, monthlyDiscount } = this.state
    if (monthlyDiscount && multiWeekDiscount) {
      const monthlyDiscountText = Formater.convertToPercent(monthlyDiscount)
      const multiWeekDiscountText = Formater.convertToPercent(multiWeekDiscount)
      return (
        <Text style={styles.descriptionText}>
          Earn a <Text style={styles.discountText}> {multiWeekDiscountText} discount </Text>for rentals over two weeks.
          Rentals over 4 weeks earn a <Text style={styles.discountText}>{monthlyDiscountText} discount</Text>.
        </Text>
      )
    } else {
      if (monthlyDiscount) {
        const monthlyDiscountText = Formater.convertToPercent(monthlyDiscount)
        return (
          <Text style={styles.descriptionText}>
            Earn a <Text style={styles.discountText}> {monthlyDiscountText} discount </Text>for rentals over 4 weeks.
          </Text>
        )
      }
      if (multiWeekDiscount) {
        const multiWeekDiscountText = Formater.convertToPercent(multiWeekDiscount)
        return (
          <Text style={styles.descriptionText}>
            Earn a <Text style={styles.discountText}> {multiWeekDiscountText} discount </Text>for rentals over two
            weeks.
          </Text>
        )
      }
    }
    return <Text style={styles.descriptionText} text="Click the day range for the rental." />
  }

  _onClearSelectedDates = () => {
    this.setState({ markedDates: [] })
  }

  render() {
    let subTitle = ''
    let buttonTitle = ''
    let title = ''
    const { mode } = this.state
    switch (mode) {
      case 'viewing':
        subTitle = 'View the available dates for this item.'
        title = 'Availability'
        break
      case 'blocking':
        subTitle = 'Tap a date to block it from renting.'
        buttonTitle = 'Block Dates'
        title = 'Tap to block dates'
        break
      case 'selecting':
        title = 'Select Rental Dates'
        subTitle = 'Earn a 10% discount for rentals over two weeks. Rentals over 4 weeks earn a 20% discount.'
        buttonTitle = 'SAVE DATES'
        break
    }

    console.log('**** this.state.markedDates', this.state.markedDates, Object.keys(this.state.markedDates).length)

    let disabled = false
    if (mode === 'selecting') {
      const { markedDates } = this.state
      let selectedDates: Array<string> = []
      Object.keys(markedDates).forEach(date => {
        const obj = markedDates[date]
        if (obj && obj.selected) {
          selectedDates.push(date)
        }
      })
      disabled = selectedDates.length < AppConfig.minimumRentDays
    }
    return (
      <View style={[{ backgroundColor: Assets.colors.componentBg }, StyleSheet.absoluteFill]}>
        <Header
          statusBarProps={{ barStyle: 'dark-content' }}
          LeftComponent={<BackButton iconSource={Assets.images.closeIcon} />}
          RightComponent={<Text style={Styles.textHeaderRight} onPress={this._onClearSelectedDates} text="Clear" />}
        />
        <Text style={styles.viewTitleLabel} text={title} />
        {mode === 'selecting' ? this._renderDiscountText() : <Text style={styles.descriptionText} text={subTitle} />}
        <View style={{ flex: 1 }}>
          {this._renderWeeks()}
          <CalendarList
            markedDates={this.state.markedDates}
            pastScrollRange={0}
            minDate={DateTime.format()}
            hideDayNames={true}
            futureScrollRange={12}
            markingType={'period'}
            pagingEnabled={false}
            style={{ borderRadius: 10, borderBottomWidth: 0, borderBottomColor: 'black' }}
            onDayPress={this._handleDayPress}
            theme={{
              backgroundColor: '#ffffff',
              calendarBackground: '#ffffff',
              textSectionTitleColor: Assets.colors.appTheme,
              selectedDayBackgroundColor: '#FDE6EC',
              selectedDayTextColor: Assets.colors.appTheme,
              todayTextColor: Assets.colors.appTheme,
              dayTextColor: Assets.colors.mainText,
              textDisabledColor: '#d9e1e8',
              dotColor: '#00adf5',
              selectedDotColor: '#ffffff',
              arrowColor: 'orange',
              monthTextColor: Assets.colors.appTheme,
              textDayFontFamily: Assets.fonts.text.medium,
              textMonthFontFamily: Assets.fonts.text.regular,
              textDayHeaderFontFamily: Assets.fonts.text.regular,
              textMonthFontWeight: 'bold',
              textDayFontSize: 18,
              textMonthFontSize: 16,
              textDayHeaderFontSize: 13
            }}
          />
        </View>
        {mode === 'selecting' ? (
          <Row style={styles.bottomView} alignHorizontal="space-between" alignVertical="center">
            <Text style={styles.bottomText} text="Rentals are limited to minimum of one week." numberOfLines={2} />
            <Button
              disabled={disabled}
              style={{ backgroundColor: disabled ? Assets.colors.seperatorColor : Assets.colors.buttonColor }}
              text={buttonTitle}
              textStyle={{ marginHorizontal: 16 }}
              onPress={this._handleDoneButtonPress}
            />
          </Row>
        ) : (
          <Button
            style={styles.button}
            text={buttonTitle}
            textStyle={{ fontSize: 18 }}
            onPress={this._handleDoneButtonPress}
          />
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  closeButton: {
    marginLeft: 6,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.appTheme,
    marginTop: 3,
    letterSpacing: -0.4
  },
  weekDayText: {
    fontFamily: Assets.fonts.display.bold,
    color: Assets.colors.mainText,
    textAlign: 'center',
    fontSize: 12,
    flex: 1
  },
  weekContainer: {
    marginHorizontal: 12,
    flexDirection: 'row',
    paddingBottom: 14,
    borderBottomColor: Assets.colors.placeholder,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  viewTitleLabel: {
    marginLeft: 15,
    marginRight: 15,
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 30,
    color: Assets.colors.mainText,
    letterSpacing: -0.2,
    lineHeight: 34
  },
  button: {
    marginBottom: Device.isIphoneX() ? 20 : 0,
    borderRadius: 0
  },
  bottomView: {
    width: '100%',
    paddingTop: 15,
    paddingBottom: Device.isIphoneX() ? 25 : 15,
    paddingHorizontal: 20,
    backgroundColor: Assets.colors.inputBg,
    borderTopColor: Assets.colors.borderColorLight,
    borderTopWidth: StyleSheet.hairlineWidth
  },
  bottomText: {
    flex: 1,
    fontSize: 16,
    marginLeft: 10,
    marginRight: 10,
    fontFamily: Assets.fonts.display.regular,
    color: Assets.colors.textLight
  },
  descriptionText: {
    marginLeft: 15,
    marginRight: 15,
    marginTop: 8,
    marginBottom: 40,
    fontFamily: Assets.fonts.display.regular,
    fontSize: 14,
    lineHeight: 18,
    textAlign: 'left',
    color: Assets.colors.mainText
  },
  discountText: {
    fontFamily: Assets.fonts.display.bold,
    color: Assets.colors.appTheme
  },
  dateContainer: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center'
  },
  dateText: {
    textAlign: 'center',
    color: 'black',
    fontSize: 18
  }
})

export default AvailabilityCalendar
