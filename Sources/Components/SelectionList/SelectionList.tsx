import React from 'react'
import { View, StyleSheet, FlatList, Image } from 'react-native'
import { Header, Text, Row } from 'rn-components'
import { FirebaseValueObject, SelectionListParam } from '@Models'
import { NavigationProps } from '@Types'
import { AppConfig } from '@Utils'
import { Navigator } from '@Navigation'
import BackButton from '../Button/BackButton'
import Styles from '@Styles'
import Assets from '@Assets'

type Props = {
  navigation: NavigationProps
}

type State = SelectionListParam & {
  items: Array<FirebaseValueObject>
}

class SelectionList extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    const param: SelectionListParam = this.props.navigation.getParam('param')
    const { type, maximumSelectedItems, mutiple = false, onItemsSelected, title = '', removedItemTitles = [] } = param
    let _items: Array<FirebaseValueObject> = AppConfig.enumConfig[type === 'backupSizes' ? 'sizes' : type]
    // let items = _items.filter((item, index) => _items.findIndex(it => it.title === item.title) === index)
    let items = _items.filter((item, index) => _items.findIndex(it => it.title === item.title) === index && item.value >= 0)


    if (Array.isArray(removedItemTitles) && removedItemTitles.length > 0) {
      const filteredItems = items.filter((item, index) => {
        return removedItemTitles.includes(item.title) == false
      })
      items = filteredItems
    }

    items.sort((a, b) => a.order - b.order)

    let selectedItems = []

    if (Array.isArray(param.selectedItems) && param.selectedItems.length > 0) {
      param.selectedItems.forEach(item => {
        if (item) {
          selectedItems.push(item)
        }
      })
    }

    if (selectedItems.length === 0) {
      selectedItems.push(items[0])
    }

    this.state = {
      selectedItems,
      items,
      maximumSelectedItems: maximumSelectedItems || items.length - 1,
      mutiple,
      onItemsSelected,
      title,
      type,
      removedItemTitles
    }
  }

  _onItemSelected = (item: FirebaseValueObject, index: number) => {
    const { selectedItems: _selectedItems, mutiple, maximumSelectedItems, items } = this.state
    const idx = _selectedItems.findIndex(it => it.title === item.title)
    let selectedItems = _selectedItems
    if (mutiple) {
      if (idx === -1) {
        if (selectedItems.length === maximumSelectedItems) {
          selectedItems.pop()
          selectedItems.push(item)
        } else {
          selectedItems.push(item)
          const allIndex = selectedItems.findIndex(item => item.title === 'All')
          if (allIndex !== -1) {
            console.log('**** all index', allIndex)
            if (selectedItems.length) {
              selectedItems.splice(allIndex, 1)
              if (item.title === 'All') {
                const idx = items.findIndex(item => item.title === 'All')
                console.log('**** ', items, idx)
                if (idx !== -1) {
                  selectedItems = [items[idx]]
                }
              }
            }
          }
        }
      } else {
        if (selectedItems.length > 1) {
          selectedItems.splice(idx, 1)
        }
      }
    } else {
      if (idx === -1) {
        selectedItems = []
        selectedItems.push(item)
      } else {
        // selectedItems = [];
      }
    }
    this.setState({ selectedItems })
  }

  _renderItem = ({ item, index }: { item: FirebaseValueObject; index: number }) => {
    const { title } = item
    const { selectedItems } = this.state
    const selected = selectedItems.includes(item)
    const alignHorizontal = selected ? 'space-between' : 'flex-start'
    const onPress = () => this._onItemSelected(item, index)

    return (
      <Row style={styles.row} alignHorizontal={alignHorizontal} alignVertical="center" onPress={onPress}>
        <Text style={styles.text} text={title} />
        <View style={{ width: 18, height: 18, marginRight: 25 }}>
          {selected && <Image style={styles.checkImage} source={Assets.images.tickmark_icon} />}
        </View>
      </Row>
    )
  }

  _keyExtractor = (item: any, index: number) => index.toString()

  _renderSeperator = () => <View style={styles.seperator} />

  _onDone = () => {
    const { selectedItems, onItemsSelected } = this.state
    if (typeof onItemsSelected === 'function') {
      onItemsSelected(selectedItems)
    }
    Navigator.back()
  }

  render() {
    const { items, title, type } = this.state
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <Header
          statusBarProps={{ barStyle: 'dark-content' }}
          title={title}
          titleStyle={Styles.headerTitle}
          LeftComponent={<BackButton onPress={this._onDone} />}
        />
        {type === 'refunds' && (
          <Text style={styles.message} text="How much will you refund a guest if they have to cancel?" />
        )}
        <FlatList
          data={items}
          renderItem={this._renderItem}
          extraData={this.state.selectedItems}
          keyExtractor={this._keyExtractor}
          contentContainerStyle={{ paddingBottom: 20 }}
          showsVerticalScrollIndicator={false}
          ItemSeparatorComponent={() => (
            <View style={{ height: StyleSheet.hairlineWidth, backgroundColor: Assets.colors.borderColor }} />
          )}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    marginTop: 20
  },
  checkImage: {
    width: 18,
    height: 18,
    tintColor: Assets.colors.appTheme
  },
  text: {
    flex: 1,
    marginLeft: 25,
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    width: '100%',
    color: 'black',
    paddingVertical: 10
  },
  row: {
    // paddingLeft: 15,
    // paddingRight: 5
  },
  seperator: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: Assets.colors.borderColor
  },
  message: {
    marginHorizontal: 12,
    marginTop: 15,
    marginBottom: 10,
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    textAlign: 'center',
    lineHeight: 18,
    letterSpacing: -0.2
  }
})

export default SelectionList
