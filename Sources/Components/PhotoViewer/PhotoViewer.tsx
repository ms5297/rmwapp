import React from 'react'
import PhotoBrowser from 'react-native-photo-browser'
import { NavigationProps } from '@Types'
import { CameraRoll } from 'react-native'
import { Navigator } from '@Navigation'
import Assets from '@Assets'

type Props = {
  navigation: NavigationProps
}

type State = {
  photos: Array<any>
  sources: Array<any>
  initialIndex: number
  name: string
  description: string
}

class PhotoViewer extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    const { params } = this.props.navigation.state
    const { initialIndex = 0, photos, name, description } = params

    const sources = photos.map(el => {
      return {
        url: String(el.photo),
        freeHeight: true,
        caption: description
      }
    })
    this.state = { sources, initialIndex, photos, name, description }
  }

  onBackButtonTap = () => {
    this.props.navigation.goBack(null)
  }

  onActionButton = (media, index) => {
    console.log('*** media', media)
    Navigator.showAlert(
      'Save this photo',
      'Are you sure that you want to save this photo.',
      () => {
        Navigator.showLoading()
        CameraRoll.saveToCameraRoll(media.photo, 'photo').then(result => {
          console.log('ssss', result)
          Navigator.showToast('Success', 'Your photo has been saved.', 'Success', 3000, () => Navigator.hideLoading())
        })
      },
      () => null
    )
  }

  render() {
    const { sources, initialIndex, photos } = this.state
    return (
      <PhotoBrowser
        onBack={this.onBackButtonTap}
        backTitle=""
        backImage={Assets.images.backIcon}
        mediaList={photos}
        sources={sources}
        initialIndex={initialIndex}
        displayNavArrows={true}
        displaySelectionButtons={false}
        displayActionButton={true}
        onActionButton={this.onActionButton}
        startOnGrid={false}
        enableGrid={true}
        useCircleProgress
        onSelectionChanged={() => {}}
        square={false}
      />
    )
  }
}

export default PhotoViewer
