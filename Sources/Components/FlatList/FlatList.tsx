import React from 'react'
import { FlatList as RNFlatList, FlatListProps } from 'react-native'
import { Navigator } from '@Navigation'

type _FlatListProps = Omit<FlatListProps<any>, 'onRefresh'>
type Props = _FlatListProps & {
  onRefresh?: () => Promise<boolean>
  onLoadMore?: () => Promise<boolean>
}

type State = {
  refreshing: boolean
  loadingMore: boolean
  loadable: boolean
}

class FlatList extends React.Component<Props, State> {
  state: State = {
    refreshing: false,
    loadingMore: false,
    loadable: false
  }

  _onRefresh = () => {
    const { onRefresh } = this.props
    if (typeof onRefresh === 'function') {
      if (this.state.refreshing) return
      Navigator.showLoading()
      this.setState({ refreshing: true })
      onRefresh()
        .then((reset: boolean) => {
          this.setState({ refreshing: false })
          Navigator.hideLoading()
        })
        .catch(error => {
          this.setState({ refreshing: false })
          Navigator.hideLoading()
          console.log('error')
        })
    }
  }

  _onLoadMore = () => {
    const { onLoadMore } = this.props
    if (typeof onLoadMore === 'function') {
      if (this.state.loadingMore || !this.state.loadable) return
      Navigator.showLoading()
      this.setState({ loadingMore: true })
      onLoadMore()
        .then((reset: boolean) => {
          this.setState({ loadingMore: false })
          Navigator.hideLoading()
        })
        .catch(error => {
          Navigator.hideLoading()
        })
    }
  }

  _onMomentumScrollBegin = () => this.setState({ loadable: true })

  _onMomentumScrollEnd = () => this.setState({ loadable: false })

  _keyExtrator = (item, index) => index.toString()

  render() {
    const { refreshing } = this.state
    const { data } = this.props
    return (
      <RNFlatList
        contentContainerStyle={{ paddingBottom: 80 }}
        data={data}
        keyExtractor={this._keyExtrator}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        onRefresh={this._onRefresh}
        refreshing={refreshing}
        initialNumToRender={10}
        onEndReachedThreshold={1}
        onEndReached={this._onLoadMore}
        scrollEnabled={data.length > 0}
        viewabilityConfig={{ viewAreaCoveragePercentThreshold: 50 }}
        onMomentumScrollBegin={this._onMomentumScrollBegin}
        onMomentumScrollEnd={this._onMomentumScrollEnd}
        {...this.props}
      />
    )
  }
}

export default FlatList
