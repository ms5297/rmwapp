import React from 'react'
import { StyleSheet, Image } from 'react-native'
import { Text, Row } from 'rn-components'
import { SectionProps } from '@Types'
import Assets from '@Assets'

type Props = SectionProps
const Section: React.SFC<Props> = props => {
  const {
    style,
    iconSource,
    iconStyle,
    titleStyle,
    title,
    value,
    valueStyle,
    onPress,
    titleContainerStyle,
    rightContainerStyle
  } = props
  return (
    <Row style={[styles.selectionView, style]} onPress={onPress} alignHorizontal="space-between" alignVertical="center">
      <Row style={[{ marginRight: 10 }, titleContainerStyle]}>
        <Text ellipsizeMode="tail" numberOfLines={1} style={[styles.title, titleStyle]} text={title} />
      </Row>
      <Row alignVertical="center" style={rightContainerStyle}>
        <Text ellipsizeMode="tail" numberOfLines={2} style={[styles.value, valueStyle]} text={value} />
        {iconSource !== null && <Image source={iconSource} style={[styles.icon, iconStyle]} />}
      </Row>
    </Row>
  )
}

const styles = StyleSheet.create({
  selectionView: {
    marginBottom: 15,
    backgroundColor: Assets.colors.inputBg,
    borderRadius: 6
  },
  title: {
    marginVertical: 12,
    marginLeft: 15,
    fontFamily: Assets.fonts.text.regular,
    fontSize: 16,
    color: Assets.colors.mainText,
    letterSpacing: -0.4
  },
  value: {
    marginRight: 10,
    textAlign: 'right',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.textLight,
    letterSpacing: -0.2
  },
  icon: {
    marginRight: 15,
    tintColor: Assets.colors.borderColor
  }
})

export default Section
