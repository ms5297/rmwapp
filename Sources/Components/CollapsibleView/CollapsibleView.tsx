import React from 'react'
import { View, LayoutChangeEvent, StyleSheet, Animated, TouchableOpacity } from 'react-native'
import { CollapsibleViewProps } from '@Types'
import { Text, Row } from 'rn-components'
import Assets from '@Assets'

type Props = CollapsibleViewProps

type State = {
  expanding: boolean
  childrenHeight: number
  animatedValue: Animated.Value
  rotateValue: Animated.Value
}

class CollapsibleView extends React.Component<Props, State> {
  static defaultProps = {
    disabled: false
  }

  constructor(props: Props) {
    super(props)
    const { disabled } = this.props
    this.state = {
      expanding: false,
      childrenHeight: -1,
      animatedValue: new Animated.Value(disabled ? 1 : 0),
      rotateValue: new Animated.Value(0)
    }
  }

  private onChildrenLayout = (event: LayoutChangeEvent) => {
    if (this.state.childrenHeight != -1) return
    const { height } = event.nativeEvent.layout
    const { paddingBottom = 0 } = this.props
    this.setState({ childrenHeight: height + 20 + 12 + paddingBottom })
  }

  private onToggle = () => {
    const { expanding, animatedValue, rotateValue } = this.state
    Animated.parallel([
      Animated.timing(animatedValue, {
        toValue: expanding ? 0 : 1,
        duration: 250
      }),
      Animated.timing(rotateValue, {
        toValue: expanding ? 0 : 1,
        duration: 250,
        useNativeDriver: true
      })
    ]).start(() => this.setState({ expanding: !expanding }))
  }

  render() {
    const {
      style,
      children,
      containerStyle,
      title,
      subTitle,
      disabled,
      hasTopDivider = false,
      RightSubComponent
    } = this.props
    const { childrenHeight, rotateValue, animatedValue, expanding } = this.state
    const source = Assets.images.chevronDown
    const rotateDeg = rotateValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '-180deg']
    })

    const rotationStyle = {
      transform: [{ rotate: rotateDeg }]
    }
    const heightStyle =
      childrenHeight == -1
        ? {}
        : {
            height: animatedValue.interpolate({
              inputRange: [0, 0.5, 1],
              outputRange: [0, childrenHeight / 3, childrenHeight]
            })
          }

    return (
      <View
        style={style}
        onStartShouldSetResponder={() => {
          if (!expanding) {
            this.onToggle()
          }
          return !expanding
        }}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={this.onToggle}
          style={[
            styles.row,
            hasTopDivider && {
              borderTopColor: Assets.colors.borderColorDark,
              borderTopWidth: 1
            }
          ]}>
          <Text style={styles.title} text={title} />
          <Row alignVertical="center">
            {RightSubComponent}
            {disabled === false && (
              <View style={styles.icon}>
                <Animated.Image style={rotationStyle} source={source} />
              </View>
            )}
          </Row>
        </TouchableOpacity>
        {subTitle !== undefined && <Text style={styles.subTitle} text={subTitle} />}

        <Animated.View style={[containerStyle, heightStyle, styles.childContainer]} onLayout={this.onChildrenLayout}>
          {children}
        </Animated.View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  childContainer: {
    flexDirection: 'column',
    borderBottomColor: Assets.colors.borderColorDark,
    borderBottomWidth: 1,
    paddingHorizontal: 15,
    paddingTop: 16,
    overflow: 'hidden'
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingTop: 15
  },
  icon: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 32
  },
  title: {
    fontSize: 16,
    fontFamily: Assets.fonts.display.regular,
    color: Assets.colors.mainText
  },
  subTitle: {
    marginTop: 2,
    fontSize: 14,
    fontFamily: Assets.fonts.text.regular,
    color: Assets.colors.textLight
  }
})

export default CollapsibleView
