import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Device } from '@Utils'
import { TagInputProps } from '@Types'
import RNTagInput from 'react-native-tag-input'
import Assets from '@Assets'
import isequal from 'lodash.isequal'

type Props = TagInputProps

type State = {
  horizontalTags: Array<string>
  horizontalText: string
}
class TagInput extends React.Component<Props, State> {
  _tagInputRef = React.createRef<RNTagInput>()
  constructor(props: Props) {
    super(props)

    this.state = {
      horizontalTags: this.props.horizontalTags || [],
      horizontalText: ''
    }
  }

  static getDerivedStateFromProps(props: Props, state) {
    if (!isequal(props.horizontalTags, state.horizontalTags)) {
      return { ...state, horizontalTags: props.horizontalTags }
    }
    return state
  }

  _onChangeHorizontalTags = horizontalTags => {
    this.setState({ horizontalTags })
  }

  _labelExtractor = tag => tag

  _onChangeHorizontalText = horizontalText => {
    this.setState({ horizontalText })

    const lastTyped = horizontalText.charAt(horizontalText.length - 1)
    const parseWhen = [',', ' ', ';', '\n']

    if (parseWhen.indexOf(lastTyped) > -1) {
      this.setState({
        horizontalTags: [...this.state.horizontalTags, this.state.horizontalText],
        horizontalText: ''
      })
      this._tagInputRef.current.scrollToEnd()
    }
  }

  _onEndEditing = () => {
    const { horizontalTags, horizontalText } = this.state
    const { onTagsSelected } = this.props
    if (horizontalText.length == 0) return
    const tags = [...horizontalTags, horizontalText]
    this.setState({
      horizontalTags: tags,
      horizontalText: ''
    })
    if (typeof onTagsSelected === 'function') {
      onTagsSelected(tags)
    }
  }

  render() {
    const { horizontalTags, horizontalText } = this.state

    const { style, onSubmitEditing } = this.props
    return (
      <View style={[styles.inputContainer, style]}>
        <RNTagInput
          ref={this._tagInputRef}
          tagContainerStyle={{ height: 32, borderRadius: 6, marginTop: 5, marginHorizontal: 5 }}
          tagTextStyle={{ fontSize: 14, fontFamily: Assets.fonts.text.regular }}
          value={horizontalTags}
          onChange={this._onChangeHorizontalTags}
          labelExtractor={this._labelExtractor}
          text={horizontalText}
          onChangeText={this._onChangeHorizontalText}
          tagColor={Assets.colors.appTheme}
          tagTextColor="white"
          inputProps={{
            keyboardType: 'default',
            returnKeyType: 'next',
            onEndEditing: this._onEndEditing,
            blurOnSubmit: true,
            placeholder: 'Type tags',
            selectionColor: Assets.colors.mainText,
            scrollEnabled: false,
            placeholderTextColor: Assets.colors.placeholder,
            onBlur: undefined,
            style: styles.input,
            onSubmitEditing: onSubmitEditing
          }}
          scrollViewProps={{ horizontal: true, showsHorizontalScrollIndicator: false }}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    marginBottom: 10,
    backgroundColor: 'white',
    borderColor: Assets.colors.borderColorLight,
    borderWidth: 0.5,
    borderRadius: 6,
    height: 50,
    justifyContent: 'center'
  },
  input: {
    fontSize: 16,
    marginLeft: 8,
    fontFamily: Assets.fonts.text.regular,
    marginTop: Device.isAndroid() ? -2 : 12
  }
})
export default TagInput
