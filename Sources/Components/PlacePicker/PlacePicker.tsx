import React from 'react'
import { View, Keyboard } from 'react-native'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'
import { Header } from 'rn-components'
import { Location, PlacePickerParam, PermissionsParam } from '@Models'
import { Navigator } from '@Navigation'
import { NavigationProps } from '@Types'
import Styles from '@Styles'
import BackButton from '../Button/BackButton'
import Assets from '@Assets'
import { Permissions } from '@Services'

type Props = {
  navigation: NavigationProps
}

type State = PlacePickerParam

class PlacePicker extends React.Component<Props, State> {
  _placePickerRef = React.createRef<GooglePlacesAutocomplete>()

  constructor(props: Props) {
    super(props)
    const param: PlacePickerParam = props.navigation.getParam('param')
    this.state = param
  }

  componentDidMount() {
    setTimeout(() => {
      this._placePickerRef.current.focus()
    }, 300)
  }

  _onCurrentLocationRowPressed = () => {
    return new Promise((resolve, reject) => {
      Permissions.check('location')
        .then(status => {
          if (status === 'authorized') {
            resolve()
          } else {
            const param: PermissionsParam = {
              permissionType: 'location',
              onDidClosed: () => {
                Permissions.check('location')
                  .then(status => {
                    if (status === 'authorized') {
                      resolve()
                    } else {
                      reject()
                    }
                  })
                  .catch(error => {
                    reject()
                  })
              }
            }
            Navigator.navTo('LocationPermission', { param })
          }
        })
        .catch(error => {
          console.log('**** error', error)
        })
    })
  }

  _onPressed = (data, details = null) => {
    const { onLocationSelected } = this.state
    let address = {
      city: '',
      state: ''
    }

    details.address_components.forEach(component => {
      if (component.types.includes('locality')) {
        address.city = component.short_name
      }
      if (address.city.length == 0 && component.types.includes('neighborhood')) {
        address.city = component.short_name
      }
      if (component.types.includes('administrative_area_level_1')) {
        address.state = component.short_name
      }
      if (address.state.length == 0 && component.types.includes('administrative_area_level_2')) {
        address.state = component.short_name
      }
    })

    let addressString = ''
    if (address.city && address.city.length && address.state && address.state.length) {
      addressString = address.city + ', ' + address.state
    } else if (address.city && address.city.length) {
      addressString = address.city
    } else if (address.state && address.state.length) {
      addressString = address.state
    }

    const location = {
      longitude: details.geometry.location.lng,
      latitude: details.geometry.location.lat
    }

    onLocationSelected(location, addressString)
    Navigator.back()
  }

  _getDefaultValue = () => ''

  render() {
    let title = 'Where is your closet located?'
    if (this.state && this.state.title) {
      title = this.state.title
    }

    const styles = {
      description: {
        fontWeight: 'bold'
      },
      predefinedPlacesDescription: {
        color: '#1faadb'
      }
    }

    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <Header
          statusBarProps={{ barStyle: 'dark-content' }}
          LeftComponent={<BackButton />}
          title={title}
          titleStyle={Styles.headerTitle}
        />
        <GooglePlacesAutocomplete
          ref={this._placePickerRef}
          currentLocation
          fetchDetails
          placeholder="Search"
          minLength={2}
          onPress={this._onPressed}
          getDefaultValue={this._getDefaultValue}
          query={{ key: 'AIzaSyAggWpOMeJ0Hkz1IMNi--UbivONTjwuN34', language: 'en' }}
          styles={styles}
          currentLocationLabel="Current location"
          // nearbyPlacesAPI="GooglePlacesSearch"
          nearbyPlacesAPI="GoogleReverseGeocoding"
          GooglePlacesSearchQuery={{ rankby: 'distance' }}
          filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}
          onCurrentLocationRowPressed={this._onCurrentLocationRowPressed}
        />
      </View>
    )
  }
}

export default PlacePicker
