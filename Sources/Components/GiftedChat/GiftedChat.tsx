import React from 'react'
import {
  Bubble,
  GiftedChat as RNGiftedChat,
  InputToolbar,
  Send,
  MessageText,
  Time,
  Composer,
  Avatar,
  SystemMessage,
  GiftedChatProps
} from 'react-native-gifted-chat'
import { View, Image } from 'react-native'
import { StyleSheet, Text } from 'rn-components'
import { Navigator } from '@Navigation'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Device } from '@Utils'
import Assets from '@Assets'
import CameraButton from '../Button/CameraButton'

const OFFSET = Device.isIphoneX() ? 20 : 0

type Props = GiftedChatProps & {
  onUploadPhoto: (url: string) => void
}

type State = {
  // messages: any;
  loadEarlier: boolean
  isLoadingEarlier: boolean
}

class GiftedChat extends React.PureComponent<Props, State> {
  state: State = {
    // messages: [],
    loadEarlier: false,
    isLoadingEarlier: false
  }

  // static getDerivedStateFromProps(props: Props, state) {
  //   if (props.messages) {
  //     return { messages: props.messages };
  //   }
  //   return state;
  // }

  static append(previousMessages, newMessages) {
    return RNGiftedChat.append(previousMessages, newMessages)
  }

  static prepend(previousMessages, newMessages) {
    return RNGiftedChat.prepend(previousMessages, newMessages)
  }

  _showPublicProfile = user => Navigator.navTo('PublicProfile', { user: { id: user._id } })

  _renderBubble = props => {
    if (props.currentMessage.isCustom) {
      return (
        <View style={styles.bubbleCustomContainer}>
          <Text style={styles.bubbleCustomText} text={props.currentMessage.text} />
        </View>
      )
    }
    return (
      <View>
        <Bubble {...props} wrapperStyle={{ right: styles.bubbleWrapperRight, left: styles.bubbleWrapperLeft }} />
      </View>
    )
  }
  _renderSend = props => {
    return (
      <Send {...props} containerStyle={{ backgroundColor: 'transparent', justifyContent: 'center' }}>
        <Image style={{ transform: [{ scale: 0.8 }], marginRight: 10 }} source={Assets.images.chat_send} />
      </Send>
    )
  }

  _renderInputToolbar = props => {
    return (
      <InputToolbar
        {...props}
        containerStyle={{
          borderTopColor: 'transparent',
          backgroundColor: '#DCDCDC70'
        }}
      />
    )
  }

  _renderMessageText = props => {
    const textStyle = {
      fontSize: 16,
      fontFamily: Assets.fonts.text.regular,
      letterSpacing: 0.25,
      lineHeight: 20,
      color: Assets.colors.mainText
    }

    console.log('**** _renderMessageText', props)
    return (
      <MessageText
        {...props}
        linkStyle={{
          left: styles.hashtag,
          right: styles.hashtag
        }}
        textStyle={{ left: textStyle, right: textStyle }}
      />
    )
  }

  _renderComposer = props => {
    return <Composer {...props} textInputStyle={styles.input} />
  }

  _renderSystemMessage = props => {
    return (
      <SystemMessage
        {...props}
        containerStyle={{ marginBottom: 15 }}
        textStyle={{ fontSize: 14, color: Assets.colors.appTheme }}
      />
    )
  }

  _renderAvatar = props => <Avatar {...props} />

  _renderActions = props => {
    return (
      <CameraButton
        size={8}
        iconSource={Assets.images.chat_camera}
        iconStyle={{ tintColor: Assets.colors.textLight }}
        onSourceChanged={this.props.onUploadPhoto}
      />
    )
  }

  _renderMessageTime(props) {
    const textStyle = {
      fontSize: 10,
      opacity: 0.7,
      fontFamily: Assets.fonts.text.regular,
      color: 'grey'
    }

    return <Time textStyle={{ left: textStyle, right: textStyle }} {...props} />
  }

  render() {
    const { messages } = this.props
    return (
      <View style={{ flex: 1 }}>
        <RNGiftedChat
          alwaysShowSend
          messages={messages}
          imageStyle={{ width: 200, height: 100 }}
          renderBubble={this._renderBubble}
          bottomOffset={0}
          onPressAvatar={this._showPublicProfile}
          renderTime={this._renderMessageTime}
          renderMessageText={this._renderMessageText}
          renderSend={this._renderSend}
          renderInputToolbar={this._renderInputToolbar}
          renderComposer={this._renderComposer}
          renderAvatar={this._renderAvatar}
          lightboxProps={{
            renderHeader: close => (
              <TouchableOpacity
                activeOpacity={0.7}
                style={{ marginTop: OFFSET, marginLeft: 5, width: 60, height: 60 }}
                onPress={close}>
                <Text text="×" style={styles.closeImageButton} />
              </TouchableOpacity>
            )
          }}
          renderActions={this._renderActions}
          loadEarlier={this.state.loadEarlier}
          isLoadingEarlier={this.state.isLoadingEarlier}
          {...this.props}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  bubbleCustomContainer: {
    flex: 1,
    marginHorizontal: 30,
    marginVertical: 5,
    backgroundColor: Assets.colors.appTheme,
    borderRadius: 6
  },
  bubbleCustomText: {
    flex: 1,
    color: 'white',
    fontSize: 12,
    fontFamily: Assets.fonts.text.regular,
    marginHorizontal: 5,
    marginVertical: 5,
    textAlign: 'center'
  },
  bubbleWrapperRight: {
    backgroundColor: 'white',
    marginVertical: 2,
    marginLeft: 100,
    borderColor: Assets.colors.borderColor,
    borderWidth: 0.5
  },
  bubbleWrapperLeft: {
    backgroundColor: '#D3D3D350',
    marginVertical: 2,
    marginRight: 100
  },
  input: {
    borderRadius: 8,
    marginHorizontal: 15,
    marginVertical: 15,
    overflow: 'hidden',
    padding: 5,
    backgroundColor: 'white'
  },
  closeImageButton: {
    fontSize: 35,
    color: 'white',
    lineHeight: 40,
    width: 40,
    textAlign: 'center'
  },
  hashtag: {
    fontSize: 16,
    fontFamily: Assets.fonts.text.regular,
    color: Assets.colors.appTheme
  }
})

export default GiftedChat
