import React from 'react'
import { StyleSheet, Platform, Animated, Easing, View, Text } from 'react-native'
import { BottomTabBarProps } from '@Types'
import { getRouteName } from '@Navigation'
import { FirebaseHelper } from '@Services'
import { StoreState, BadgeCountActions } from '@ReduxManager'
import { connect } from 'react-redux'
import { UserProfile, BadgeCount } from '@Models'
import Tab from './Tab'
import Assets from '@Assets'


type Props = BottomTabBarProps & {
  userProfile?: UserProfile
}

type State = {
  visible: boolean
  animatedValue: Animated.Value
  unread: number,
  adminUnread: number
}

class BottomTabBar extends React.Component<Props, State> {
  _conversationObserver: any = null
  _adminConversationObserver: any = null
  _adminUnreadConversationObserver: any = null
  state = {
    visible: true,
    animatedValue: new Animated.Value(1),
    unread: 0,
    adminUnread: 0
  }

  componentDidMount() {
    const { userProfile } = this.props
    // if (userProfile.role === 'admin') { this.setAdminUndreadCount() }
    this._conversationObserver = FirebaseHelper.getUserConversations(userProfile).onSnapshot(snapShoot => {
      let unread = 0
      snapShoot.forEach(child => {
        let data = child.data()
        //@ts-ignore
        if (data.unread > 0) {
          //@ts-ignore
          unread = unread + data.unread
        }
      })
      this.setState({
        unread: unread
      })
      if (userProfile.role === 'user') {
        this.getAdminConversationCountforUser()
      } else {
        this.setAdminUndreadCount()
        // FirebaseHelper.setBadge(unread)
      }

    })
  }
  getAdminConversationCountforUser = () => {
    const { userProfile } = this.props
    const { unread } = this.state;
    this._adminConversationObserver = FirebaseHelper.getAdminConversationsById(userProfile).onSnapshot(snapShoot => {
      let newUnread = unread
      snapShoot.forEach(child => {
        let data = child.data()
        //@ts-ignore
        if (data.unread > 0) {
          //@ts-ignore
          newUnread = newUnread + data.unread
        }
      })
      this.setState({
        unread: newUnread
      })

      FirebaseHelper.setBadge(newUnread)
    })
  }

  setAdminUndreadCount = () => {
    this._adminUnreadConversationObserver = FirebaseHelper.getAdminConversationsByAdminId().onSnapshot(snapShoot => {
      const { unread } = this.state;
      let newUnread = 0
      snapShoot.forEach(child => {
        let data = child.data()
        //@ts-ignore
        if (data.unread > 0) {
          //@ts-ignore
          newUnread = newUnread + data.unread
        }
      })
      this.setState({
        adminUnread: newUnread
      })
      let badgeCount: BadgeCount = { adminBadgeCount: newUnread }
      BadgeCountActions.saveBadgeCount(badgeCount)
      const newUnreadBadge = unread + newUnread;
      FirebaseHelper.setBadge(newUnreadBadge)
      //FirebaseHelper.setBadge(11)
    })
  }



  componentWillUnmount() {
    this._conversationObserver && this._conversationObserver()
    this._adminConversationObserver && this._adminConversationObserver()
    this._adminUnreadConversationObserver && this._adminUnreadConversationObserver()
  }

  static defaultProps: Props = {
    tabs: [
      {
        iconSource: Assets.images.menu_home,
        text: 'Search'
      },
      {
        iconSource: Assets.images.menu_favorites,
        text: 'Favorites'
      },
      {
        iconSource: Assets.images.menu_closet,
        text: 'My Closet'
      },
      {
        iconSource: Assets.images.menu_inbox,
        text: 'Inbox'
      },
      {
        iconSource: Assets.images.menu_profile,
        text: 'Profile'
      }
    ],
    activeColor: Assets.colors.appTheme,
    inactiveColor: Assets.colors.homeTab
  }

  componentWillReceiveProps(props: Props, state: State) {
    if (props.navigation) {
      let visible = this.isVisible(props.navigation)

      const { params } = props.navigation.state.routes[props.navigation.state.index]

      const forceVisible = params ? params.tabBarVisible : null
      if (forceVisible !== null && forceVisible !== visible && visible) {
        visible = forceVisible
      }

      const { visible: preState } = state
      if (visible !== preState) {
        Animated.timing(this.state.animatedValue, {
          toValue: visible ? 1 : 0,
          duration: 250,
          isInteraction: false,
          useNativeDriver: true,
          easing: Easing.inOut(Easing.sin)
        }).start(() => {
          this.setState({ visible })
        })
      }
    }
  }

  isVisible(navigation: any) {
    let visible = true
    const routeName = getRouteName(navigation)
    switch (routeName) {
      case 'Filters':
      case 'Details':
      case 'Checkout':
      case 'CheckoutReview':
      case 'SelectionList':
      case 'Reviews':
      case 'Reporting':
      case 'AddItem':
      case 'PhotoViewer':
      case 'AvailabilityCalendar':
      case 'EditProfile':
      case 'EditProfileScreenModelStack':
      case 'PayoutMethods':
      case 'PayoutAccountTypes':
      case 'PayoutMethodsInfo':
      case 'PayoutAccountInfo':
      case 'PayoutAccountAddress':
      case 'PayoutMethodsConfirm':
      case 'PayoutMethodsView':
      case 'ActionFinal':
      case 'AddCreditCard':
      case 'ViewCreditCard':
      case 'PaymentMethods':
      case 'AdminInbox':
      case 'AdminChat':
      case 'UserList':
      case 'Chat':
      case 'Settings':
      case 'Notifications':
      case 'WebsiteView':
      case 'WebsiteView1':
      case 'RentalRating':
      case 'RentalRatingDetail':
      case 'ListerRentalRating':
      case 'RentalItemDetails':
      case 'ReturnItem':
      case 'ReturnItemDetail':
      case 'ReturnPhoto':
      case 'ReturnItemMessageSent':
      case 'ReturnItemMessageLender':
      case 'CheckoutBuyNow':
      case 'BuyNouMsg':
      case 'PublicProfile':
      case 'ContactLister':
      case 'PlacePicker':
      case 'MessageSuccess':
      case 'RequestSuccess':
      case 'Permissions':
      case 'PromotionCodeAdd':
      case 'PromotionCode':
      case 'PromotionCodeDetail':
      case 'PromotionCodeInfo':
      case 'ForceUpdateInfo':
      case 'ForceUpdateBankAccount':
      case 'ChangePhoneNumber':
      case 'VerifyOTP':
      case 'CheckoutItemDetail':
      case 'RequestSize':
      case 'ComingSoon':
      case 'EnterAccessCode':
      case 'CreateAccessCode':
      case 'NotificationWarning':
      case 'LocationPermission':
        visible = false
        break
    }
    return visible
  }

  _renderBagde = () => {
    const { unread } = this.state
    if (unread) {
      const unreadText = unread > 10 ? '10+' : this.state.unread
      return (
        <View style={styles.badgeContainer}>
          <Text style={styles.badgeText}>{unreadText}</Text>
        </View>
      )
    }
    return null
  }
  /**added by mahesh@fathomable to show admin unread count on profile footer icon. */
  _renderProfileBagde = () => {
    const { adminUnread } = this.state
    if (adminUnread) {
      const unreadText = adminUnread > 10 ? '10+' : adminUnread
      return (
        <View style={styles.badgeContainer}>
          <Text style={styles.badgeText}>{unreadText}</Text>
        </View>
      )
    }
    return null
  }
  renderTabs = () => {
    const { jumpTo, navigation, activeColor, inactiveColor, tabs } = this.props
    if (!navigation) return null
    const { routes, index } = navigation.state
    if (routes.length !== tabs.length) return null

    const tabbar = routes.map((tab, idx) => {
      const { key, params } = tab
      const focused = index === idx

      const onPress = () => {
        if (!focused && key && jumpTo) {
          jumpTo(key)
        } else {
          const r = tab.routes[0]
          if (r && r.params) {
            const { scrollToTop } = r.params
            if (typeof scrollToTop === 'function') {
              scrollToTop()
            }
          }
        }
      }

      const { style, textStyle, iconSource, text } = tabs[idx]
      const tabActiveColor = focused ? activeColor : inactiveColor

      return (
        <Tab
          key={key}
          onPress={onPress}
          style={[styles.tab, style]}
          text={text}
          iconSource={iconSource}
          iconStyle={{ tintColor: tabActiveColor }}
          textStyle={[textStyle, { color: tabActiveColor }]}>
          {idx === 3 ? this._renderBagde() : null}
          {idx === 4 ? this._renderProfileBagde() : null}
        </Tab>
      )
    })
    return tabbar
  }

  render() {
    const { animatedValue } = this.state
    const translateY = animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [80, 0]
    })
    const animationStyle = {
      transform: [{ translateY }]
    }
    return <Animated.View style={[styles.container, animationStyle]}>{this.renderTabs()}</Animated.View>
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    maxHeight: 80,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    ...Platform.select({
      android: {
        elevation: 4
      },
      ios: {
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.2,
        shadowRadius: 9
      }
    })
  },
  tab: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    backgroundColor: 'transparent'
  },
  badgeContainer: {
    position: 'absolute',
    right: -5,
    top: 5,
    borderRadius: 9,
    height: 18,
    width: 18,
    backgroundColor: Assets.colors.appTheme,
    justifyContent: 'center',
    alignItems: 'center'
  },
  badgeText: {
    textAlign: 'center',
    fontFamily: Assets.fonts.display.regular,
    fontSize: 9,
    color: 'white'
  }
})

const mapPropsToState = (state: StoreState) => {
  return {
    userProfile: state.userProfile
  }
}
export default connect(mapPropsToState)(BottomTabBar)
