import React from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { TabProps } from '@Types'
import { Touchable, Text } from 'rn-components'
import { Device } from '@Utils'
import Assets from '@Assets'

type Props = TabProps
const Tab: React.SFC<Props> = props => {
  const { iconSource, iconStyle, text, textStyle, style, onPress, children } = props

  return (
    <Touchable activeOpacity={0.7} style={[styles.container, style]} onPress={onPress}>
      <View style={{ width: '100%', alignItems: 'center' }}>
        {iconSource && <Image source={iconSource} style={[styles.image, iconStyle]} />}
        {text && <Text text={text} style={[styles.text, textStyle]} />}
        {children}
      </View>
    </Touchable>
  )
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    padding: 0
  },
  image: {
    marginTop: 10,
    transform: [{ scale: 0.85 }]
  },
  text: {
    marginTop: 5,
    marginBottom: Device.isIphoneX() ? 15 : 10,
    fontSize: 10,
    color: 'black',
    fontFamily: Assets.fonts.text.medium
  }
})

export default Tab
