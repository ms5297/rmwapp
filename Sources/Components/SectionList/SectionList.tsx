import React from 'react'
import { SectionList as RNSectionList, SectionListProps, Dimensions, View } from 'react-native'
import { Navigator } from '@Navigation'
import { DataProvider, RecyclerListView, LayoutProvider } from 'recyclerlistview'
import { Text } from 'rn-components'
import { Constants } from '@Utils'
import ItemCell from '../ItemCell/ItemCell'
type Props = SectionListProps<any> & {
  onRefresh?: () => Promise<boolean>
  onLoadMore?: () => Promise<boolean>
}

let { width } = Dimensions.get('window')

type State = {
  refreshing: boolean
  loadingMore: boolean
  dataProvider: any
  dataRowProvider: any
}

class SectionList extends React.Component<Props, State> {
  constructor(props) {
    super(props)

    let dataProvider = new DataProvider((r1, r2) => {
      return r1 !== r2
    })

    console.log('this.props.data', this.props.data)
    const length = this.props.data.length

    let dataRowProvider = []
    for (let i = 0; i < length; i++) {
      let provider = new DataProvider((r1, r2) => {
        return r1 !== r2
      })
      const rowProvider = provider.cloneWithRows(this.props.data[i].items)
      console.log('*** roow', i, this.props.data[i].items)
      dataRowProvider.push(rowProvider)
    }

    this.state = {
      refreshing: false,
      loadingMore: false,
      dataProvider: dataProvider.cloneWithRows(this.props.data),
      dataRowProvider
    }
  }

  _onRefresh = () => {
    const { onRefresh } = this.props
    if (typeof onRefresh === 'function') {
      if (this.state.refreshing) return
      Navigator.showLoading()
      this.setState({ refreshing: true })
      onRefresh().then(() => {
        this.setState({ refreshing: false })
      })
    }
  }

  _onLoadMore = () => {
    const { onLoadMore } = this.props
    if (typeof onLoadMore === 'function') {
      if (this.state.loadingMore) return
      Navigator.showLoading()
      this.setState({ loadingMore: true })
      onLoadMore().then(() => {
        this.setState({ loadingMore: false })
      })
    }
  }

  _keyExtrator = (item, index) => index.toString()

  render() {
    return (
      <RecyclerListView
        layoutProvider={
          new LayoutProvider(
            index => {
              return 1
            },
            (type, dim) => {
              dim.width = width
              dim.height = Constants.CELL_HEIGHT
            }
          )
        }
        dataProvider={this.state.dataProvider}
        rowRenderer={(type, data, index) => {
          const provider = this.state.dataRowProvider[index]
          if (!provider) {
            return null
          }

          return (
            <View>
              <RecyclerListView
                isHorizontal
                dataProvider={this.state.dataRowProvider[index]}
                layoutProvider={
                  new LayoutProvider(
                    index => {
                      return 1
                    },
                    (type, dim) => {
                      dim.width = width / 2
                      dim.height = Constants.CELL_HEIGHT
                    }
                  )
                }
                rowRenderer={(type, data, index) => {
                  return (
                    <ItemCell editable={false} style={{ marginLeft: 10 }} item={data} width={Constants.CELL_WIDTH} />
                  )
                }}
              />
            </View>
          )
        }}
      />
    )
  }
}

export default SectionList
