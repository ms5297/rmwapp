import React from 'react'
import DeviceInfo from 'react-native-device-info'
import Assets from '@Assets'
import { Text, StyleSheet, ViewProps, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { StoreState } from '@ReduxManager'
import { Device } from '@Utils'

type Props = ViewProps & {
  codePushVersion: string
}

type State = {
  hiddenAppVersion: boolean
}
class Version extends React.Component<Props, State> {
  state = {
    hiddenAppVersion: false
  }

  toggleVersion = () => {
    const isValidCodePushVersion = this.isValidCodePushVersion()
    if (!isValidCodePushVersion) return

    this.setState({ hiddenAppVersion: !this.state.hiddenAppVersion })
  }

  isValidCodePushVersion() {
    const { codePushVersion } = this.props
    return typeof codePushVersion === 'string' && codePushVersion !== ''
  }

  render() {
    const { hiddenAppVersion } = this.state
    const { codePushVersion } = this.props
    const appVersion = Device.getAppVersion()
    const version = hiddenAppVersion ? `Code Push Version ${codePushVersion}` : `Version ${appVersion}`
    const isValidCodePushVersion = this.isValidCodePushVersion()
    return (
      <TouchableOpacity style={this.props.style} disabled={!isValidCodePushVersion} onPress={this.toggleVersion}>
        <Text style={styles.text}>{version}</Text>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    marginVertical: 20,
    textAlign: 'center',
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.appTheme,
    letterSpacing: -0.2
  }
})

const mapPropsToState = (state: StoreState) => {
  return {
    codePushVersion: state.appConfiguration.codePushVersion
  }
}
export default connect(mapPropsToState)(Version)
