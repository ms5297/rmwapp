import React from 'react'
import { View, FlatList, StyleSheet, Image } from 'react-native'
import { Constants } from '@Utils'
import { Navigator } from '@Navigation'
import { Icon, Text, Touchable } from 'rn-components'
import { HorizontalListPhotosProps } from '@Types'
import { AsyncImage } from '@Components'
import Assets from '@Assets'
import ActionSheet from 'react-native-actionsheet'
import ImagePicker from 'react-native-image-crop-picker'

type Props = HorizontalListPhotosProps

type State = {
  photos: Array<string>
  shuoldDetele: boolean
}

class HorizontalListPhotos extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      photos: Array.isArray(this.props.photos) ? this.props.photos : [],
      shuoldDetele: false
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (state && state.shuoldDetele) {
      return state
    }

    if (props && Array.isArray(props.photos) && props.photos.length > 0 && state && state.photos.length == 0) {
      return { ...state, photos: props.photos }
    }
    return state
  }

  getPhotos = () => {
    return this.state.photos
  }

  _actionSheetRef = React.createRef<ActionSheet>()

  _showActionSheet = () => this._actionSheetRef.current.show()

  _onActionSheetSelect = (index: number) => {
    switch (index) {
      case 1:
        ImagePicker.openCamera({
          width: Constants.IMAGE_WIDTH,
          height: Constants.IMAGE_HEIGHT,
          compressImageMaxWidth: Constants.IMAGE_WIDTH,
          compressImageMaxHeight: Constants.IMAGE_HEIGHT,
          compressImageQuality: 1,
          smartAlbums: ['Favorites', 'RecentlyAdded', 'UserLibrary', 'PhotoStream', 'Panoramas', 'Videos', 'Bursts'],
          cropping: false
        })
          .then(image => {
            this._pickImage(image)
          })
          .catch(err => {
            Navigator.showToast('Error', err.message, 'Error')
          })
        break
      case 2:
        ImagePicker.openPicker({
          width: Constants.IMAGE_WIDTH,
          height: Constants.IMAGE_HEIGHT,
          compressImageMaxHeight: Constants.IMAGE_HEIGHT,
          compressImageMaxWidth: Constants.IMAGE_WIDTH,
          compressImageQuality: 1,
          smartAlbums: ['Favorites', 'RecentlyAdded', 'UserLibrary', 'PhotoStream', 'Panoramas', 'Videos', 'Bursts'],
          cropping: false
        })
          .then(image => {
            this._pickImage(image)
          })
          .catch(err => {
            Navigator.showToast('Error', err.message, 'Error')
          })
        break
      case 3:
        ImagePicker.openPicker({
          multiple: true,
          width: Constants.IMAGE_WIDTH,
          height: Constants.IMAGE_HEIGHT,
          compressImageMaxHeight: Constants.IMAGE_HEIGHT,
          compressImageMaxWidth: Constants.IMAGE_WIDTH,
          compressImageQuality: 1,
          smartAlbums: ['Favorites', 'RecentlyAdded', 'UserLibrary', 'PhotoStream', 'Panoramas', 'Videos', 'Bursts']
        })
          .then(images => {
            let photos = this.state.photos.slice()
            console.log('*** images', photos)
            if (Array.isArray(images)) {
              const imagesUrl = images.map(image => image.path)
              photos = [...this.state.photos, ...imagesUrl]

              console.log('*** imagesUrl', imagesUrl, photos)
            }

            this.setState({ photos })
          })
          .catch(err => {
            Navigator.showToast('Error', err.message, 'Error')
          })
        break
    }
  }

  _pickImage(image) {
    Navigator.showLoading()
    ImagePicker.openCropper({
      path: 'file://' + image.path,
      showCropGuidelines: true,
      freeStyleCropEnabled: true,
      cropping: true,

      avoidEmptySpaceAroundImage: true,
      compressImageMaxWidth: Constants.IMAGE_WIDTH,
      compressImageMaxHeight: Constants.IMAGE_HEIGHT,
      width: Constants.IMAGE_WIDTH,
      height: Constants.IMAGE_HEIGHT,
      compressImageQuality: 1
    })
      .then(image => {
        let array = this.state.photos
        array.push(image.path)
        this.setState({ photos: array })
        Navigator.hideLoading()
      })
      .catch(error => {
        Navigator.hideLoading()
        Navigator.showToast('Error', error.message, 'Error')
      })
  }

  _deleteImage = (item, index) => {
    Navigator.showAlert(
      'Delete photo',
      'Delete this item? This cannot be undone.',
      () => null,
      () => {
        let array = [...this.state.photos]
        const idx = array.indexOf(item)
        if (idx !== -1) {
          array.splice(idx, 1)
          this.setState({ photos: array, shuoldDetele: true })
        }
      },
      'CANCEL',
      'OK'
    )
  }

  _renderItem = ({ item, index }) => {
    const onPress = () => this._deleteImage(item, index)
    return (
      <View style={styles.photoButton}>
        <AsyncImage
          resizeMode="contain"
          style={{ borderRadius: 6, width: '100%', height: '100%' }}
          source={{ uri: item }}
          fallbackImageSource={Assets.images.itemImagePlaceHolder}
        />
        <Icon
          style={styles.trashIcon}
          size={16}
          iconStyle={{ tintColor: Assets.colors.borderColor, height: 16, width: 16 }}
          iconSource={Assets.images.trash}
          onPress={onPress}
        />
      </View>
    )
  }

  _renderListEmptyComponent = () => (
    <Touchable style={[styles.photoButton, { borderColor: Assets.colors.appTheme }]} onPress={this._showActionSheet}>
      <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
        <Image style={{ tintColor: Assets.colors.appTheme }} source={Assets.images.addCircle} />
        <Text style={styles.addButtonText} text="Add Photo" />
      </View>
    </Touchable>
  )

  _keyExtractor = (item, index) => index.toString()

  render() {
    const { photos } = this.state
    const { style } = this.props
    console.log('**** photos', photos)
    return (
      <View style={style}>
        <FlatList
          scrollEnabled={photos.length > 0}
          horizontal
          keyExtractor={this._keyExtractor}
          data={photos}
          renderItem={this._renderItem}
          ListHeaderComponent={this._renderListEmptyComponent}
          showsHorizontalScrollIndicator={false}
        />
        <ActionSheet
          ref={this._actionSheetRef}
          options={['Cancel', 'Camera', 'Photo with Editing', 'Multiple Photos']}
          cancelButtonIndex={0}
          onPress={this._onActionSheetSelect}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  photoButton: {
    backgroundColor: 'white',
    borderColor: Assets.colors.textLight,
    borderWidth: 1,
    width: 130,
    height: 172,
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 15
  },
  trashIcon: {
    backgroundColor: 'white',
    position: 'absolute',
    top: 10,
    height: 32,
    width: 32,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 16,
    right: 10
  },
  addButtonText: {
    fontFamily: Assets.fonts.display.bold,
    fontSize: 12,
    color: Assets.colors.appTheme
  }
})

export default HorizontalListPhotos
