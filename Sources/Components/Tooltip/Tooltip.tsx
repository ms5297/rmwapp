import React, { Component } from 'react'
import { TouchableHighlight, StyleSheet, Text, Alert } from 'react-native'
import Tooltip from 'react-native-walkthrough-tooltip'
// import Tooltip from 'rn-tooltip';


import Assets from '@Assets';
import { Icon } from 'rn-components';

type Props = {
    message: string,
    onAction?: (fieldName: any) => void,
    fieldName?: string
    width?: number
    height?: number
    position?: string
}

type State = {
    toolTipVisible: boolean,
    isPressed: boolean
}


class TooltipUI extends React.Component<Props, State> {
    souceImage: any
    pressedSouceImage: any
    toolTipPosition: string = "bottom"
    constructor(props: Props) {
        super(props);
        this.state = { toolTipVisible: false, isPressed: false }
        const { position } = this.props
        this.souceImage = Assets.images.questionMark
        this.pressedSouceImage = Assets.images.questionMarkPink
        this.toolTipPosition = position ? position : "bottom"
    }

    _getDataForTooltip = () => {
        // const { onAction, fieldName } = this.props
        // this.setState({ toolTipVisible })
        // onAction(fieldName)
    }
    onOpen = () => {
        // this.setState({ isPressed: true })
    }
    onClose = () => {
        // this.setState({ isPressed: false })
    }
    render() {
        const { message, width, height } = this.props
        const { toolTipVisible } = this.state

        return (

            <Tooltip
                backgroundColor="transparent"
                isVisible={toolTipVisible}
                content={<Text style={[styles.textStyle]}>{message}</Text>}
                placement={this.toolTipPosition}
                tooltipStyle={{ width: '100%' }}
                contentStyle={{ backgroundColor: '#2F282A', alignSelf: 'center' }}
                onClose={() => this.setState({ toolTipVisible: false })}
            >
                <TouchableHighlight onPress={() => this.setState({ toolTipVisible: true })}>
                    <Icon iconStyle={{ width: 20, height: 20 }} iconSource={toolTipVisible ? this.pressedSouceImage : this.souceImage} onPress={() => this.setState({ toolTipVisible: true })} />
                </TouchableHighlight>
            </Tooltip>
        )
    }
}
const styles = StyleSheet.create({
    textStyle: {
        color: '#FFFFFF',
        // font-family: "GT Walsheim Pro";	
        fontSize: 14,
        letterSpacing: 0.04,
        lineHeight: 18
    }
})
export default TooltipUI;