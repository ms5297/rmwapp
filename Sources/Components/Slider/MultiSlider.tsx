import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import MultiSliderComponent from '@ptomasroos/react-native-multi-slider'
import Assets from '@Assets'

type Props = {
  values: Array<number>
  onValuesChangeStart?: () => void
  onValuesChangeFinish?: () => void
  onRangeValueChange: (values: Array<number>) => void
  min: number
  max: number
  step: number
}
type State = {
  values: Array<number>
}
class MultiSlider extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = { values: this.props.values }
  }

  componentWillReceiveProps(nextProps: Props) {
    this.setState({ values: nextProps.values })
  }

  _onValuesChangeFinish = (values: Array<number>) => {
    const { onValuesChangeFinish, onRangeValueChange } = this.props
    if (typeof onValuesChangeFinish === 'function') {
      onValuesChangeFinish()
    }
    this.setState({ values }, () => {
      onRangeValueChange(values)
    })
  }

  _onValuesChange = (values: Array<number>) => {
    this.setState({ values })
  }

  _onValuesChangeStart = () => {
    const { onValuesChangeStart } = this.props
    if (typeof onValuesChangeStart === 'function') {
      onValuesChangeStart()
    }
  }
  render() {
    let leftSideValue = ''
    let rightSideValue = ''
    if (this.state.values) {
      if (this.state.values.length == 1) {
        rightSideValue = this.state.values[0] + ' mi'
      } else {
        leftSideValue = '$' + this.state.values[0]
        let maxPriceRange = this.state.values[1]
        rightSideValue = '$' + maxPriceRange
        if (maxPriceRange == 1000) {
          rightSideValue = '$1,000'
        } else if (maxPriceRange > 1000) {
          rightSideValue = '$1,000+'
        }
      }
    }

    const { min, max, step } = this.props

    return (
      <View style={{ height: 88, alignItems: 'center', justifyContent: 'center' }}>
        <View style={{ height: 18, marginBottom: 10, flexDirection: 'row' }}>
          <Text style={styles.sliderValueText}>{leftSideValue}</Text>
          <Text style={[styles.sliderValueText, { textAlign: 'right' }]}>{rightSideValue}</Text>
        </View>
        <MultiSliderComponent
          values={this.state.values}
          onValuesChangeStart={this._onValuesChangeStart}
          onValuesChange={this._onValuesChange}
          onValuesChangeFinish={this._onValuesChangeFinish}
          selectedStyle={{ backgroundColor: Assets.colors.appTheme }}
          unselectedStyle={{ backgroundColor: '#E7EAEB' }}
          markerStyle={{
            height: 24,
            width: 24,
            borderRadius: 12,
            borderWidth: 1,
            borderColor: Assets.colors.appTheme,
            backgroundColor: Assets.colors.appTheme,
            shadowColor: '#455154',
            shadowOffset: { width: 0, height: 1 },
            shadowOpacity: 0.1,
            shadowRadius: 2
          }}
          min={min}
          max={max}
          step={step}
          allowOverlap
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  sliderValueText: {
    textAlign: 'left',
    flex: 1,
    marginLeft: 15,
    marginRight: 15,
    fontFamily: Assets.fonts.text.regular,
    fontSize: 14,
    color: Assets.colors.appTheme,
    letterSpacing: -0.2,
    lineHeight: 18
  }
})

export default MultiSlider
