import React from 'react'
import { View, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { EmptyViewProps, EmptyViewTwoButtonProps } from '@Types'
import { Text } from 'rn-components'
import Assets from '@Assets'
import Button from '../Button/Button'

type Props = EmptyViewTwoButtonProps

const EmptyViewTwoButton: React.SFC<Props> = props => {
    const {
        style,
        title,
        titleImage,
        titleStyle,
        subTitle,
        subTitleStyle,
        info,
        infoStyle,
        buttonStyle,
        buttonTextStyle,
        buttonText,
        onPress,
        secondButtonText,
        onPressSecondButton
    } = props
    return (
        <View style={[styles.container, style]}>
            {titleImage && <Image source={titleImage} style={{ marginBottom: 10 }} />}
            {title && title !== '' && <Text style={[styles.titleText, titleStyle]} text={title} />}
            {subTitle && subTitle !== '' && <Text style={[styles.subTitleText, subTitleStyle]} text={subTitle} />}
            {info && info !== '' && <Text style={[styles.infoText, infoStyle]} text={info} />}
            <View style={{ width: '100%' }} >
                {onPress && (
                    <Button
                        style={[styles.button, buttonStyle]}
                        type="solid"
                        textStyle={[styles.buttonText, buttonTextStyle]}
                        text={buttonText}
                        onPress={onPress}
                    />
                )}
                {onPressSecondButton && (
                    <Button
                        style={[styles.secondButton, buttonStyle]}
                        type="solid"
                        textStyle={[styles.buttonText, buttonTextStyle]}
                        text={secondButtonText}
                        onPress={onPressSecondButton}
                    />
                )}
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 40,
        // marginHorizontal: 40,
        // alignItems: 'center',
        marginTop: 82,
        // justifyContent: 'center',
        // alignContent: 'center',

    },
    titleText: {
        fontSize: 24,
        color: Assets.colors.mainText,
        fontFamily: Assets.fonts.display.bold,
        letterSpacing: -0.2,
        lineHeight: 28,
        textAlign: 'center'
    },

    subTitleText: {
        marginTop: 15,
        fontSize: 18,
        color: Assets.colors.mainText,
        fontFamily: Assets.fonts.display.bold,
        letterSpacing: -0.2,
        lineHeight: 28,
        textAlign: 'center'
    },

    infoText: {
        marginTop: 15,
        fontSize: 14,
        color: Assets.colors.textLight,
        fontFamily: Assets.fonts.text.regular,
        letterSpacing: -0.2,
        lineHeight: 18,
        textAlign: 'center'
    },

    button: {
        height: 48,
        marginTop: 60,
        borderRadius: 6,
        backgroundColor: Assets.colors.buttonColor
    },
    secondButton: {
        height: 48,
        marginTop: 10,
        // padding: 10,
        borderRadius: 6,
        backgroundColor: Assets.colors.buttonColor
    },
    buttonText: {
        paddingHorizontal: 30
    }
})

export default EmptyViewTwoButton
