import React from 'react'
import { View, StyleSheet, DeviceEventEmitter } from 'react-native'
import { Col, Row, Text, Icon } from 'rn-components'
import { ItemCellProps } from '@Types'
import { Constants, Device, Utils } from '@Utils'

import StarRating from 'react-native-star-rating'
import AsyncImage from '../Image/AsyncImage'
import Assets from '@Assets'
import { getStore, ItemSummaryActions, FavoritesActions, ItemListActions } from '@ReduxManager'
import { Navigator } from '@Navigation'
import { FirebaseHelper, Api } from '@Services'
import { UserProfile } from '@Models'

type Props = ItemCellProps

type State = {
  isOwner: boolean
  userProfile: UserProfile
  isFavorite: boolean
}

class ItemCell extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props)
    const { item } = this.props
    const { userProfile } = getStore().getState()
    const isOwner = item.owner.id === userProfile.id

    this.state = { isOwner, userProfile, isFavorite: item.is_favorite }
  }

  static getDerivedStateFromProps(props: Props, state: State) {
    if (props.item.is_favorite !== state.isFavorite) {
      return { ...state, isFavorite: props.item.is_favorite }
    }
    return state
  }

  _onPress = () => {
    const { item, onPress } = this.props
    if (typeof onPress === 'function') {
      onPress(item)
    } else {
      Navigator.navTo('Details', { item })
    }
  }

  _onUpdateFavorite = () => {
    const { item, onTapFavoriteItem, shouldRefreshCategoryFavorite } = this.props
    const { isFavorite } = this.state
    if (typeof onTapFavoriteItem === 'function') {
      onTapFavoriteItem(item)
    } else {
      if (isFavorite) {
        Navigator.showAlert(
          'Remove favorite item',
          'Are you sure you want to remove this favorite item?',
          () => null,
          () => {
            Navigator.showLoading()
            Api.deleteFavorites<any>(item.id)
              .then(response => {
                if (response.code !== 200) {
                  throw Error(response.message || '(IC - 63) Internal Error')
                }
                if (!shouldRefreshCategoryFavorite) {
                  ItemSummaryActions.updateItemSummaryFavorite(item.id, false)
                }

                ItemListActions.updateItemListFavorite(item.id, false)
                FavoritesActions.deleteFavorite(item.id)
                return true
              })
              .then(() => {
                this.setState({ isFavorite: false })

                Navigator.hideLoading()
                Navigator.showToast('Success', 'Your favorite item has been removed successfully.', 'Success')

                DeviceEventEmitter.emit(Constants.NEED_UPDATE_FAVORITE_ITEM_EVENT, {
                  shouldRefresh: shouldRefreshCategoryFavorite
                })
              })
              .catch(error => {
                Navigator.hideLoading()
                Navigator.showToast('Error', error.message, 'Error')
              })
          },
          'CANCEL',
          'OK'
        )
      } else {
        Navigator.showLoading()
        Api.setFavorites(item.id)
          .then(response => {
            const { userProfile } = this.state
            const { user_name, lastname, firstname } = userProfile
            let username = ''
            if (user_name && user_name.length > 0) {
              username = user_name
            } else {
              username = firstname + ' ' + lastname
            }

            let postData = {
              type: 'like',
              item: item.id,
              toUser: item.owner.id,
              fromUserName: username,
              text: username + ' just liked your rental ' + item.name
            }
            ItemSummaryActions.updateItemSummaryFavorite(item.id, true)
            ItemListActions.updateItemListFavorite(item.id, true)

            return Promise.all([Api.getFavorites(), FirebaseHelper.pushHistories(postData)])
          })
          .then((response: any) => {
            const favorites = response[0] && response[0].code === 200 ? response[0].favorites : null
            if (favorites) {
              FavoritesActions.saveFavorites(favorites)
            }

            this.setState({ isFavorite: true })
            Navigator.hideLoading()
            Navigator.showToast('Success', 'Add favorite successfully', 'Success')
            DeviceEventEmitter.emit(Constants.NEED_UPDATE_FAVORITE_ITEM_EVENT, {
              shouldRefresh: shouldRefreshCategoryFavorite
            })
          })
          .catch(error => {
            Navigator.hideLoading()
            Navigator.showToast('Error', error.message, 'Error')
          })
      }
    }
  }

  _renderOptionButton = () => {
    const { editable = false, item, onTapMoreOption } = this.props
    const { isFavorite: is_favorite, userProfile } = this.state

    const isOwner = item.owner.id === userProfile.id

    if (editable) {
      const onPress = () => {
        if (typeof onTapMoreOption === 'function') {
          onTapMoreOption(item)
        }
      }
      return (
        <Icon
          style={styles.iconContainer}
          onPress={onPress}
          size={16}
          iconSource={Assets.images.moreIcon}
          iconStyle={{ width: 15, height: 15, tintColor: Assets.colors.textLight }}
        />
      )
    }

    if (!editable && !isOwner) {
      return (
        <Icon
          onPress={this._onUpdateFavorite}
          style={styles.iconHeartContainer}
          size={16}
          iconSource={is_favorite ? Assets.images.heartIcon : Assets.images.heartOutline}
          iconStyle={{
            tintColor: is_favorite ? Assets.colors.appTheme : null,
            height: 15,
            width: 15
          }}
        />
      )
    }

    return null
  }

  render() {
    const { style, width, item, editable, ownerNameVisible = true, hiddenRating = true } = this.props
    const { photos, name, rental_fee, size_id, avg_review, owner, reviews_count } = item

    const imageWidth = width
    const imageHeight = width * Constants.IMAGE_RATIO
    const imagePreview = Array.isArray(photos) && photos.length ? photos[0].url : ''
    const rentalFee = `$${rental_fee}`
    // const size = Utils.getTitleWithValue('sizes', size_id)
    // const sizes = Utils.getSafeSizeIds(item)
    const sizeName = Utils.getNameFromSizeIds(item)

    return (
      <Col style={[style, { width }]} onPress={this._onPress}>
        <AsyncImage
          style={[styles.imageHolder, { width: imageWidth, height: imageHeight }]}
          source={{ uri: imagePreview }}
          fallbackImageSource={Assets.images.itemImagePlaceHolder}>
          {this._renderOptionButton()}
        </AsyncImage>

        <Text numberOfLines={1} ellipsizeMode="tail" text={name} style={styles.itemName} />
        <Row alignVertical="center" style={{ marginTop: 5 }}>
          <Text style={styles.price} text={rentalFee} />
          <View style={styles.detailSeperator} />
          <Text numberOfLines={1} ellipsizeMode="tail" style={styles.size} text={sizeName} />
        </Row>
        {!editable && !hiddenRating && (
          <View style={{ marginTop: 7 }}>
            <Row alignVertical="center">
              <StarRating
                disabled={true}
                maxStars={5}
                rating={avg_review}
                emptyStar={Assets.images.ratingEmptyStar}
                fullStar={Assets.images.ratingFullStar}
                halfStar={Assets.images.ratingHalfStar}
                halfStarEnabled={true}
                starSize={16}
              />
              {reviews_count > 0 && <Text style={styles.reviewCount} text={reviews_count} />}
            </Row>
          </View>
        )}
        {ownerNameVisible && <Text numberOfLines={1} style={styles.ownerName} text={owner.name} />}
      </Col>
    )
  }
}
const styles = StyleSheet.create({
  imageHolder: {
    borderRadius: 6,
    backgroundColor: '#F4F6F6',

    overflow: 'hidden'
  },

  itemName: {
    textAlign: 'left',
    fontFamily: Assets.fonts.display.bold,
    color: Assets.colors.mainText,
    marginTop: 11,
    fontSize: 16
  },

  price: {
    textAlign: 'left',
    fontFamily: Assets.fonts.display.regular,
    color: Assets.colors.mainText,
    fontSize: 14
  },

  size: {
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    color: Assets.colors.appTheme,
    fontSize: 14,
    letterSpacing: -0.2,
    flex: 1
  },

  detailSeperator: {
    marginHorizontal: Device.hs * 6,
    width: 4,
    height: 4,
    borderRadius: 2,
    backgroundColor: Assets.colors.textLight
  },

  reviewCount: {
    marginLeft: 5,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    color: Assets.colors.appTheme,
    letterSpacing: -0.2,
    fontSize: 12
  },

  ownerNameThumb: {
    marginTop: 6,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    color: Assets.colors.textLight,
    fontSize: 12,
    letterSpacing: -0.2
  },

  ownerName: {
    marginTop: 6,
    textAlign: 'left',
    fontFamily: Assets.fonts.text.regular,
    color: Assets.colors.appTheme,
    fontSize: 14,
    letterSpacing: -0.2
  },

  iconContainer: {
    position: 'absolute',
    top: 10,
    right: 10,
    width: 32,
    height: 32,
    borderRadius: 16,
    backgroundColor: 'white'
  },
  iconHeartContainer: {
    position: 'absolute',
    top: 10,
    right: 10,
    width: 32,
    height: 32,
    borderRadius: 16,
    backgroundColor: 'rgba(0,0,0,0.1)',
    borderWidth: 1,
    borderColor: 'white'
  }
})

export default ItemCell
