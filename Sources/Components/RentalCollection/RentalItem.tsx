import React from 'react'
import { StyleSheet, View, ViewProps } from 'react-native'
import { Col, Row, Icon, Text } from 'rn-components'
import { AsyncImage } from '@Components'
import { RentalItemModel } from '@Models'
import { Utils, Formater, Validator } from '@Utils'
import Assets from '@Assets'

type Props = RentalItemModel & ViewProps
const RentalItem: React.SFC<Props> = props => {
  const {
    onRemoved,
    onPressed,
    photos,
    photo,
    name,
    rental_fee,
    cleaning_fee,
    additional_fees,
    retail_price,
    size_id,
    owner,
    style,
    showTotalPrice = true,
    cleaning_option_id
  } = props

  const imagePreview =
    Array.isArray(photos) && photos.length ? photos[0].url : photo && Validator.isURL(photo.url) ? photo.url : ''

  const size = Utils.getTitleWithValue('sizes', size_id)
  console.warn(cleaning_option_id);

  const cleaning = Utils.getTitleWithValue('cleanings', cleaning_option_id)
  // const source = imagePreview === "" ? Assets.images.itemImagePlaceHolder : { uri: imagePreview };
  const closet = `${owner.name}’s Closet`

  let rentalFee = `$${rental_fee}`

  // if (additional_fees && showTotalPrice) {
  //   let totalFee = rental_fee;
  //   if (cleaning_fee) {
  //     totalFee += cleaning_fee;
  //   }

  //   const { insurance_fee, service_fee } = additional_fees;
  //   totalFee += insurance_fee + (service_fee || 0);

  //   rentalFee = `${totalFee}`;
  // }

  rentalFee = Formater.formatMoney(rentalFee)
  return (
    <Row style={[styles.container, style]} alignHorizontal="space-between">
      <AsyncImage
        style={styles.imageHolder}
        source={{ uri: imagePreview }}
        fallbackImageSource={Assets.images.cartImagePlaceHolder}
      />

      <Col flex={1} style={styles.col} alignVertical="center">
        <Text style={styles.title} text={name} />
        <Text style={styles.closet} text={closet} />
        <View style={{ flex: 1 }} />
        <Text style={styles.priceTitle}>
          Rental Price: <Text style={styles.priceValue} text={rentalFee} />
        </Text>
        <Text style={styles.priceTitle}>
          Size: <Text style={styles.priceValue} text={size} />
        </Text>
        <Text style={styles.priceTitle}>
          Cleaning: <Text style={styles.priceValue} text={cleaning} />
        </Text>
      </Col>
      <Col style={{ width: 60 }}>
        {typeof onRemoved === 'function' && <Icon iconSource={Assets.images.trashCircle} onPress={onRemoved} />}
      </Col>
    </Row>
  )
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 10
  },
  imageHolder: {
    borderRadius: 6,
    backgroundColor: '#F4F6F6',
    overflow: 'hidden',
    width: 75,
    height: 105,
    marginHorizontal: 15
  },
  col: {
    paddingTop: 8,
    paddingBottom: 10
  },
  title: {
    fontSize: 14,
    fontFamily: Assets.fonts.text.bold,
    color: Assets.colors.mainText
  },
  closet: {
    marginTop: 2,
    fontSize: 12,
    color: Assets.colors.textLight,
    fontFamily: Assets.fonts.text.bold
  },
  priceTitle: {
    fontSize: 12,
    color: Assets.colors.textLight,
    fontFamily: Assets.fonts.text.medium
  },
  priceValue: {
    fontSize: 14,
    color: Assets.colors.mainText,
    fontFamily: Assets.fonts.text.medium
  }
})

export default RentalItem
