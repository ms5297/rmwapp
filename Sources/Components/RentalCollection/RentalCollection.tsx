import React from 'react'
import { RentalCollectionModel, Item } from '@Models'
import { CollapsibleViewProps } from '@Types'
import { Text, Row } from 'rn-components'
import { View, TouchableOpacity, Animated, StyleSheet, FlatList } from 'react-native'
import Assets from '@Assets'
import RentalItem from './RentalItem'
import { Navigator } from '@Navigation'

type Props = RentalCollectionModel &
  CollapsibleViewProps & {
    onItemRemoved?: (item: Item) => void
  }

type State = {
  expanding: boolean
  childrenHeight: number
  animatedValue: Animated.Value
  rotateValue: Animated.Value
}
class RentalCollection extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    const { expanding = true } = this.props

    this.state = {
      expanding,
      childrenHeight: -1,
      animatedValue: new Animated.Value(expanding ? 1 : 0),
      rotateValue: new Animated.Value(expanding ? 1 : 0)
    }
  }

  _onItemRemoved = item => {
    this.props.onItemRemoved(item)
  }

  private onContentSizeChange = (w, h) => {
    this.setState({ childrenHeight: h })
  }

  private onToggle = () => {
    const { expanding, animatedValue, rotateValue } = this.state
    Animated.parallel([
      Animated.timing(animatedValue, {
        toValue: expanding ? 0 : 1,
        duration: 250
      }),
      Animated.timing(rotateValue, {
        toValue: expanding ? 0 : 1,
        duration: 250,
        useNativeDriver: true
      })
    ]).start(() => this.setState({ expanding: !expanding }))
  }

  renderItem = ({ item, index }) => {
    const { items, onItemRemoved, showTotalPrice = false } = this.props
    const _onRemoved = typeof onItemRemoved === 'function' && items.length > 1 ? () => this._onItemRemoved(item) : null
    const _onPressed = () => {
      Navigator.push('CheckoutItemDetail', { item, onRemoved: items.length > 1 ? this._onItemRemoved : null })
    }

    return (
      <RentalItem showTotalPrice={showTotalPrice} key={index} {...item} onRemoved={_onRemoved} onPressed={_onPressed} />
    )
  }

  keyExtractor = item => item.id.toString()

  render() {
    const { style, containerStyle, title, subTitle, highlightSubTitle, disabled, items } = this.props
    const { childrenHeight, rotateValue, animatedValue } = this.state
    const source = Assets.images.chevronDown
    const rotateDeg = rotateValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '-180deg']
    })
    const rotationStyle = {
      transform: [{ rotate: rotateDeg }]
    }

    let heightStyle = {}

    if (childrenHeight) {
      heightStyle = {
        height: animatedValue.interpolate({
          inputRange: [0, 0.5, 1],
          outputRange: [0, childrenHeight / 3, childrenHeight]
        })
      }
    }

    return (
      <View style={style}>
        <View style={styles.colContainer}>
          <View style={[styles.row]}>
            <Text style={styles.title} text={title} />
            {items.length > 0 && (
              <TouchableOpacity
                style={styles.icon}
                onPress={this.onToggle}
                hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }}>
                <Animated.Image style={rotationStyle} source={source} />
              </TouchableOpacity>
            )}
          </View>
          {typeof subTitle === 'string' && subTitle !== '' && <Text style={styles.subTitle} text={subTitle} />}
          {typeof highlightSubTitle === 'string' && highlightSubTitle !== '' && (
            <Text style={styles.highlightSubTitle} text={'Status: ' + highlightSubTitle} />
          )}
        </View>
        <Animated.View style={[containerStyle, heightStyle, { overflow: 'hidden', marginTop: 15 }]}>
          <FlatList
            scrollEnabled={false}
            onContentSizeChange={this.onContentSizeChange}
            data={items}
            extraData={items.length}
            keyExtractor={item => item.id.toString()}
            renderItem={this.renderItem}
          />
        </Animated.View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  colContainer: {
    paddingLeft: 15,
    paddingBottom: 15,
    borderBottomColor: Assets.colors.borderColorDark,
    borderBottomWidth: 1
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  icon: {
    marginTop: 10,
    marginHorizontal: 20,
    backgroundColor: Assets.colors.inputBg,
    justifyContent: 'center',
    alignItems: 'center',
    width: 32,
    height: 32,
    borderRadius: 16
  },
  title: {
    fontSize: 36,
    fontFamily: Assets.fonts.display.bold,
    color: Assets.colors.mainText
  },
  subTitle: {
    fontSize: 14,
    fontFamily: Assets.fonts.text.regular,
    color: Assets.colors.textLight
  },
  highlightSubTitle: {
    marginTop: 6,
    fontSize: 14,
    fontFamily: Assets.fonts.text.medium,
    color: Assets.colors.appTheme
  }
})

export default RentalCollection
