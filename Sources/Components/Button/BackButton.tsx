import React from 'react'
import { ImageSourcePropType } from 'react-native'
import { Navigator } from '@Navigation'
import { IconProps, Omit } from 'rn-components'
import Assets from '@Assets'
import LeftHeaderButton from './LeftHeaderButton'

type Props = Omit<IconProps, 'iconSource'> & {
  iconSource?: ImageSourcePropType
}

const BackButton: React.SFC<Props> = props => {
  const onBack = () => Navigator.back()

  return (
    <LeftHeaderButton
      iconStyle={{ tintColor: Assets.colors.appTheme }}
      iconSource={Assets.images.backIcon}
      onPress={onBack}
      {...props}
    />
  )
}

export default BackButton
