import React from 'react'
import { ImageSourcePropType, View, Image } from 'react-native'
import { Touchable, ButtonProps, StyleSheet, Text, Row } from 'rn-components'
import Assets from '@Assets'

type Props = ButtonProps & {
  type?: 'outline' | 'solid'
  color?: string
  iconSource?: ImageSourcePropType
}

const Button: React.SFC<Props> = props => {
  const { type = 'solid', style, textStyle, text, onPress, iconSource, disabled } = props
  const defaultButtonStyle = type == 'outline' ? styles.outlineButton : styles.solidButton
  const defaultTextStyle = type == 'outline' ? styles.outlineButtonText : styles.solidButtonText

  const renderChildren = () => {
    if (iconSource) {
      return (
        <Row
          style={[styles.center, [{ paddingHorizontal: 15 }]]}
          alignHorizontal="space-between"
          alignVertical="center">
          <Text style={[defaultTextStyle, textStyle]} text={text} />
          <Image source={iconSource} />
        </Row>
      )
    }
    return (
      <View style={styles.center}>
        <Text style={[defaultTextStyle, textStyle]} text={text} />
      </View>
    )
  }
  return (
    <Touchable style={[defaultButtonStyle, style]} onPress={onPress} disabled={disabled}>
      {renderChildren()}
    </Touchable>
  )
}

const styles = StyleSheet.create({
  center: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: 'transparent'
  },
  outlineButton: {
    height: 48,
    borderRadius: 6,
    backgroundColor: 'transparent',
    borderColor: Assets.colors.appTheme,
    borderWidth: 1.0
  },
  outlineButtonText: {
    color: Assets.colors.appTheme,
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14
  },
  solidButton: {
    height: 48,
    borderRadius: 6,
    backgroundColor: Assets.colors.appTheme
  },
  solidButtonText: {
    color: 'white',
    fontFamily: Assets.fonts.display.bold,
    fontSize: 14
  },
  noneBorder: {
    borderColor: 'transparent',
    borderWidth: undefined
  }
})

export default Button
