import React from 'react'
import Assets from '@Assets'
import { Icon, IconProps } from 'rn-components'

type Props = IconProps
const LeftHeaderButton: React.SFC<Props> = props => {
  return <Icon iconSource={props.iconSource} hitSlop={{ left: 10, right: 20, top: 5, bottom: 5 }} {...props} />
}

LeftHeaderButton.defaultProps = {
  iconStyle: Assets.images.backIcon
}

export default LeftHeaderButton
