import React from 'react'
import { View } from 'react-native'
import { CameraButtonProps } from '@Types'
import { Icon } from 'rn-components'
import { Constants } from '@Utils'
import ActionSheet from 'react-native-actionsheet'
import ImagePicker from 'react-native-image-crop-picker'

type Props = CameraButtonProps

const options = ['Cancel', 'Camera', 'Photo Library']

class CameraButton extends React.Component<Props> {
  _actionSheetRef = React.createRef<ActionSheet>()

  _showActionSheet = () => this._actionSheetRef.current.show()

  private onSelectFromCamera = () => {
    ImagePicker.openCamera({
      cropping: true,
      width: Constants.IMAGE_WIDTH,
      height: Constants.IMAGE_HEIGHT,
      compressImageMaxHeight: Constants.IMAGE_HEIGHT,
      compressImageMaxWidth: Constants.IMAGE_WIDTH,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperCircleOverlay: this.props.cropperCircleOverlay,
      showCropGuidelines: true,
      cropperCancelText: 'Cancel',
      cropperChooseText: 'Chose',
      avoidEmptySpaceAroundImage: true,
      waitAnimationEnd: false
    })
      .then(image => {
        if (Array.isArray(image)) return

        if (this.props.onSourceChanged) {
          this.props.onSourceChanged(image.path)
        }
      })
      .catch(e => console.warn('error: ', e))
  }

  private onSelectFromAlbum = () => {
    ImagePicker.openPicker({
      cropping: true,
      multiple: false,
      width: Constants.IMAGE_WIDTH,
      height: Constants.IMAGE_HEIGHT,
      compressImageMaxHeight: Constants.IMAGE_HEIGHT,
      compressImageMaxWidth: Constants.IMAGE_WIDTH,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperCircleOverlay: this.props.cropperCircleOverlay,
      showCropGuidelines: true,
      cropperCancelText: 'Cancel',
      cropperChooseText: 'Choose',
      avoidEmptySpaceAroundImage: true,
      waitAnimationEnd: true
    })
      .then(image => {
        if (Array.isArray(image)) return
        if (this.props.onSourceChanged) {
          this.props.onSourceChanged(image.path)
        }
      })
      .catch(e => console.warn('error: ', e))
  }

  _onActionSheetSelect = (index: number) => {
    switch (index) {
      case 1:
        this.onSelectFromCamera()
        break
      case 2:
        this.onSelectFromAlbum()
        break
      default:
        break
    }
  }

  render() {
    const { style, iconSource, iconContainerStyle, iconStyle } = this.props
    return (
      <View style={style}>
        <Icon
          iconSource={iconSource}
          iconContainerStyle={iconContainerStyle}
          iconStyle={iconStyle}
          onPress={this._showActionSheet}
        />
        <ActionSheet
          ref={this._actionSheetRef}
          options={options}
          cancelButtonIndex={0}
          onPress={this._onActionSheetSelect}
        />
      </View>
    )
  }
}

export default CameraButton
