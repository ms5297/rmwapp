import React from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { configStore, PermissionsActions } from '@ReduxManager'
import { Navigator, AppContainer } from '@Navigation'
import { StatusBarStyle, StatusBar as RN, View, Platform } from 'react-native'
import { ToastType } from 'rn-notifier'
import { Loading, Alert, Toast, NetInfo } from 'rn-notifier'
import { FreshChatHelper, FirebaseHelper, Permissions, AppsFlyerHelper, GoogleAnalyticsHelper } from '@Services'
import { Bubbles } from 'react-native-loader'
import Assets from '@Assets'
import StoreChecker from 'react-native-store-checker'

const { store, persistor } = configStore()

const isAndroid = Platform.OS === 'android'

const storeAppID = isAndroid ? '' : '1375748670'

export class App extends React.Component {
  private loadingRef = React.createRef<Loading>()
  private alertRef = React.createRef<Alert>()
  private toastRef = React.createRef<Toast>()

  componentDidMount() {
    if (!__DEV__) {
      FirebaseHelper.enableCrashlyticsCollection()
    }
    FreshChatHelper.init()
    AppsFlyerHelper.init();
  }

  showLoading = (msg?: string) => {
    if (this.loadingRef.current) this.loadingRef.current.show(msg)
  }

  hideLoading = (onClose?: () => void) => {
    if (this.loadingRef.current) this.loadingRef.current.hide(onClose)
  }

  showAlert = (
    title: string,
    msg: string,
    onOk?: () => void,
    onCancel?: () => void,
    okButtonText?: string,
    cancelButtonText?: string
  ) => {
    if (this.alertRef.current) this.alertRef.current.show(title, msg, onOk, onCancel, okButtonText, cancelButtonText)
  }

  showToast = (
    title: string,
    message: string,
    type: ToastType = 'Error',
    duration: number = 4000,
    onShow?: () => void,
    onClose?: () => void,
    isDisableInteraction = false,
    activeStatusBarType: StatusBarStyle = 'light-content',
    deactiveStatusBarType: StatusBarStyle = 'default'
  ) => {
    if (this.toastRef.current) {
      // @ts-ignore
      const backupProps = RN._currentValues
      let _deactiveStatusBarType = deactiveStatusBarType
      if (backupProps && backupProps.barStyle) {
        const { value } = backupProps.barStyle
        if (value) {
          _deactiveStatusBarType = value
        }
      }
      this.toastRef.current.show(
        title,
        message,
        type,
        duration,
        onShow,
        onClose,
        isDisableInteraction,
        activeStatusBarType,
        _deactiveStatusBarType
      )
    }
  }

  hideToast = () => {
    if (this.toastRef.current) {
      this.toastRef.current.hide()
    }
  }

  private getScreenProps() {
    return {
      showLoading: this.showLoading,
      hideLoading: this.hideLoading,
      showAlert: this.showAlert,
      showToast: this.showToast,
      hideToast: this.hideToast
    }
  }

  private setRoot = (r: any) => {
    Navigator.setRoot(r)
  }

  _renderIndicator = () => {
    return (
      <View>
        <Bubbles size={7} color="#F06182" />
      </View>
    )
  }

  _onBeforeLift = () => {
    Permissions.checkLocationAndNotification(['notification', 'location'])
      .then(status => {
        const { notification, location } = status
        console.log('**** saveAllPermissons', notification, location)
        PermissionsActions.saveAllPermissons(notification, location)
      })
      .catch(error => {
        console.warn('error: ', error)
      })
  }

  _getActiveRouteName = (navigationState) => {
    if (!navigationState) {
      return null;
    }
    const route = navigationState.routes[navigationState.index];
    // dive into nested navigators
    if (route.routes) {
      return this._getActiveRouteName(route);
    }
    return route.routeName;
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <AppContainer
            onNavigationStateChange={(prevState, currentState, action) => {
              const currentRouteName = this._getActiveRouteName(currentState);
              const previousRouteName = this._getActiveRouteName(prevState);

              if (previousRouteName !== currentRouteName) {
                GoogleAnalyticsHelper.trackScreenView(currentRouteName)
              }
            }}
            ref={this.setRoot} screenProps={this.getScreenProps()}
          />
          <Toast
            titleStyle={{
              fontFamily: Assets.fonts.text.bold,
              fontSize: 16,
              color: 'white'
            }}
            messageStyle={{
              fontFamily: Assets.fonts.text.regular,
              fontSize: 16,
              marginTop: 2,
              color: 'white'
            }}
            ref={this.toastRef}
          />
          <Loading
            ref={this.loadingRef}
            modalBackgroundColor="rgba(0,0,0,0.2)"
            IndicatorComponent={this._renderIndicator}
            style={{ justifyContent: 'flex-start' }}
            contentContainerStyle={{ justifyContent: 'flex-start' }}
            overlayStyle={{ justifyContent: 'flex-start' }}
            overlayContentStyle={{
              backgroundColor: 'transparent',
              position: 'absolute',
              top: 160
            }}
          />
          <Alert
            ref={this.alertRef}
            positiveButtonTitleStyle={{ color: Assets.colors.appTheme }}
            negativeButtonTitleStyle={{ color: Assets.colors.appTheme }}
          />
          <NetInfo />

          <StoreChecker
            storeAppID={storeAppID}
            storeAppName="rent-my-wardrobe"
            title="Update Available !"
            titleStyle={{
              fontFamily: Assets.fonts.text.bold,
              color: Assets.colors.buttonColor,
              fontSize: 20
            }}
            messageStyle={{
              fontFamily: Assets.fonts.text.regular,
              color: Assets.colors.mainText,
              fontSize: 16
            }}
            updateLaterButtonText="Later"
            updateLaterButtonStyle={{
              borderColor: Assets.colors.buttonColor,
              borderWidth: 1,
              height: 47,
              borderRadius: 6,
              backgroundColor: 'white'
            }}
            updateLaterButtonTextStyle={{
              color: Assets.colors.appTheme,
              fontFamily: Assets.fonts.display.bold
            }}
            updateNowButtonText="Update now"
            updateNowButtonStyle={{
              backgroundColor: Assets.colors.buttonColor,
              height: 48,
              borderRadius: 6
            }}
            updateNowButtonTextStyle={{
              fontFamily: Assets.fonts.display.bold
            }}
            message="The latest version of Rent My Wardrobe is available. Do you want to update now ?"
          />
        </PersistGate>
      </Provider>
    )
  }
}

export default App
